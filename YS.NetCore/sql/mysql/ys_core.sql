/*
 Navicat Premium Data Transfer

 Source Server         : 192.168.168.137
 Source Server Type    : MySQL
 Source Server Version : 50643
 Source Host           : 192.168.168.137:3306
 Source Schema         : ys_core

 Target Server Type    : MySQL
 Target Server Version : 50643
 File Encoding         : 65001

 Date: 24/05/2021 09:28:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for site_application_setting
-- ----------------------------
DROP TABLE IF EXISTS `site_application_setting`;
CREATE TABLE `site_application_setting`  (
  `SettingKey` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `Title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `Status` int(11) NULL DEFAULT NULL,
  `CreateBy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreatebyName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `CreateDate` datetime(0) NULL DEFAULT NULL,
  `LastUpdateBy` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LastUpdateByName` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `LastUpdateDate` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`SettingKey`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of site_application_setting
-- ----------------------------
INSERT INTO `site_application_setting` VALUES ('EditorType', 'EWebEditor', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('en_SiteName', '5297投资有限公司', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('EWebEditoLicence', '', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-19 13:56:26');
INSERT INTO `site_application_setting` VALUES ('ExpandAllPage', 'true', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2020-03-03 17:14:38');
INSERT INTO `site_application_setting` VALUES ('Favicon', '/upload/UploadFiles/2020-12-02/ico.ico', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'ad123', 'ad123', '2021-01-18 09:32:35');
INSERT INTO `site_application_setting` VALUES ('Logo', '/upload/UploadFiles/2020-12-02/shupai（5297LOGOzhenggao）-1.png', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'ad123', 'ad123', '2021-01-18 09:32:35');
INSERT INTO `site_application_setting` VALUES ('mail_type1', '449600885@qq.com', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-08 14:07:08');
INSERT INTO `site_application_setting` VALUES ('mail_type2', '17533394@qq.com', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-08 14:07:08');
INSERT INTO `site_application_setting` VALUES ('mail_type3', '449600885@qq.com', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-08 14:07:08');
INSERT INTO `site_application_setting` VALUES ('mail_type4', '449600885@qq.com', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-08 14:07:08');
INSERT INTO `site_application_setting` VALUES ('mail_type5', '449600885@qq.com', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'Administrator', '2020-06-08 14:07:08');
INSERT INTO `site_application_setting` VALUES ('OuterChainPicture', 'False', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'ad123', 'ad123', '2021-01-18 09:32:35');
INSERT INTO `site_application_setting` VALUES ('put_other_out_second', '120', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2020-03-03 17:14:38');
INSERT INTO `site_application_setting` VALUES ('register_redbackage', '0.01', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('sc_SiteName', 'SkyCMS 内容管理系统', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('Show_Data_Lang', 'sc', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'ad123', 'ad123', '2021-01-18 09:32:35');
INSERT INTO `site_application_setting` VALUES ('SiteMetaDescption', '', NULL, NULL, NULL, 'admin', 'admin', '2020-11-30 08:20:14', 'ad123', 'ad123', '2021-01-18 09:32:35');
INSERT INTO `site_application_setting` VALUES ('SiteMetaDescription', '', NULL, NULL, NULL, NULL, NULL, '2021-03-02 08:10:36', NULL, NULL, '2021-03-02 08:10:36');
INSERT INTO `site_application_setting` VALUES ('SiteMetaKeywords', '', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('SiteName', 'Guotai Junan International', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2020-03-03 17:14:38');
INSERT INTO `site_application_setting` VALUES ('stmp_client', '{\"Host\":\"smtp.qq.com\",\"Port\":465,\"Email\":\"977702451@qq.com\",\"PassWord\":\"\",\"EnableSsl\":false}', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('tc_SiteName', '', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');
INSERT INTO `site_application_setting` VALUES ('TplCode', 'docs', NULL, NULL, 1, 'admin', 'admin', '2020-03-03 17:14:38', 'admin', 'admin', '2021-01-26 11:43:29');

-- ----------------------------
-- Table structure for site_basedata
-- ----------------------------
DROP TABLE IF EXISTS `site_basedata`;
CREATE TABLE `site_basedata`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lang_flag` int(11) NOT NULL,
  `module` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `parent` bigint(20) NULL DEFAULT NULL,
  `parent_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chr_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `img` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `thumb` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `file_link` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `n_sort` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_id`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_basedata_type
-- ----------------------------
DROP TABLE IF EXISTS `site_basedata_type`;
CREATE TABLE `site_basedata_type`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_content
-- ----------------------------
DROP TABLE IF EXISTS `site_content`;
CREATE TABLE `site_content`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `id` bigint(20) NOT NULL COMMENT '记录ID',
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种标识：tc，en，sc',
  `lang_flag` int(11) NULL DEFAULT NULL COMMENT '1此记录只用于繁体 2表示此记录既用于繁体有用于简体s',
  `node_id` bigint(20) NULL DEFAULT NULL COMMENT '栏目ID',
  `node_code` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目代号',
  `title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接',
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '简介',
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `extend_fields_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展模型以json格式存储',
  `cover` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面',
  `atlas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '图集',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `is_home` int(11) NULL DEFAULT NULL COMMENT '是否首页',
  `is_top` int(11) NULL DEFAULT NULL COMMENT '是否置顶',
  `is_node` int(11) NULL DEFAULT NULL COMMENT '是否栏目页',
  `view_count` int(11) NULL DEFAULT NULL COMMENT '查看数',
  `status` int(11) NULL DEFAULT NULL COMMENT '状态',
  `review_status` int(11) NULL DEFAULT NULL COMMENT '审核状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `creator` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名',
  `tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `del_flag` int(11) NULL DEFAULT NULL COMMENT '删除状态',
  `down_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下载文件',
  `is_show` int(255) NULL DEFAULT NULL COMMENT '是否显示',
  `publish_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发布时间',
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 112 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '多语言内容表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_content_marks
-- ----------------------------
DROP TABLE IF EXISTS `site_content_marks`;
CREATE TABLE `site_content_marks`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `mark_id` bigint(20) NULL DEFAULT NULL COMMENT '关联留痕记录ID',
  `data_type` int(255) NULL DEFAULT NULL COMMENT '1旧数据,2新数据',
  `data_id` bigint(20) NOT NULL COMMENT '记录ID',
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种标识：tc，en，sc',
  `lang_flag` int(11) NOT NULL COMMENT '1此记录只用于繁体 2表示此记录既用于繁体有用于简体s',
  `title` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接',
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '简介',
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `extend_fields_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展模型以json格式存储',
  `cover` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面',
  `atlas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '图集',
  `is_home` int(11) NULL DEFAULT NULL COMMENT '是否首页',
  `is_top` int(11) NULL DEFAULT NULL COMMENT '是否置顶',
  `is_node` int(11) NULL DEFAULT NULL COMMENT '是否栏目页',
  `tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标签',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `down_file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '下载文件',
  `is_show` int(255) NULL DEFAULT NULL COMMENT '是否显示',
  `publish_date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2755 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '多语言内容表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_country
-- ----------------------------
DROP TABLE IF EXISTS `site_country`;
CREATE TABLE `site_country`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `id` bigint(20) NOT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lang_flag` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `country_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `n_sort` int(11) NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_doc
-- ----------------------------
DROP TABLE IF EXISTS `site_doc`;
CREATE TABLE `site_doc`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `id` bigint(20) NOT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lang_flag` int(11) NULL DEFAULT NULL,
  `module` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块分类代号',
  `node_id` bigint(20) NULL DEFAULT NULL COMMENT '栏目ID',
  `node_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目代号',
  `name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `stock_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '股票代号',
  `chr_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '说明',
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `img` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `file_link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接',
  `idusr` bigint(20) NULL DEFAULT NULL COMMENT '作者',
  `idusr_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `idusr2` bigint(20) NULL DEFAULT NULL COMMENT '作者1',
  `idusr2_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_top` int(11) NULL DEFAULT NULL COMMENT '是否置顶',
  `is_new` int(11) NULL DEFAULT NULL COMMENT '是否是新',
  `is_login` int(11) NULL DEFAULT NULL COMMENT '是否需要登录',
  `date` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日期',
  `industry` bigint(20) NULL DEFAULT NULL COMMENT '行业',
  `industry_name` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行业名称',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者用户ID',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `valid` int(11) NULL DEFAULT NULL COMMENT '是否审核',
  `is_show` int(255) NOT NULL DEFAULT 0 COMMENT '是否显示',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作者',
  `source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_doc_field_set
-- ----------------------------
DROP TABLE IF EXISTS `site_doc_field_set`;
CREATE TABLE `site_doc_field_set`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_type_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_doc_field_set_required
-- ----------------------------
DROP TABLE IF EXISTS `site_doc_field_set_required`;
CREATE TABLE `site_doc_field_set_required`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_type_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_doc_type
-- ----------------------------
DROP TABLE IF EXISTS `site_doc_type`;
CREATE TABLE `site_doc_type`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lang_flag` int(11) NOT NULL,
  `node_id` bigint(20) NULL DEFAULT NULL COMMENT '栏目ID',
  `node_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '栏目代号',
  `node_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `module` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代号',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_member
-- ----------------------------
DROP TABLE IF EXISTS `site_member`;
CREATE TABLE `site_member`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chr_account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '账号',
  `chr_pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `id_card` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号码',
  `card_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权益卡号',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '真实姓名',
  `parent_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级权益人',
  `avatar_url` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像图片',
  `gender` int(1) NULL DEFAULT NULL COMMENT '0 未知;1男;2女',
  `country` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国家',
  `province` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省份',
  `city` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '城市',
  `tel_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `open_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公众号open_id',
  `union_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开放平台union_id',
  `session_id` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录的sessionID',
  `session_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录的sessionKey',
  `comple_userinfo` int(1) NULL DEFAULT NULL COMMENT '是否需要补全信息',
  `last_comple_date` datetime(0) NULL DEFAULT NULL COMMENT '最后补全信息时间',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `tags` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员标签,t1,t2...tN',
  `review_status` int(255) NULL DEFAULT NULL COMMENT '审核状态',
  `face_picture` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '认证脸部图片',
  `card_type` int(11) NULL DEFAULT NULL COMMENT '证件类型1身份证2其他',
  `payee` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '领款人',
  `bank_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行开户行',
  `bank_no` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行卡号',
  `descption` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简介',
  `remark` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `review_time` datetime(0) NULL DEFAULT NULL COMMENT '审核时间',
  `inherit_status` int(11) NULL DEFAULT NULL COMMENT '继承状态1当前权益人2过往继承人',
  `legal_status` int(11) NULL DEFAULT NULL COMMENT '收益状态1权益人2受托继承人',
  `change_begin_time` datetime(0) NULL DEFAULT NULL COMMENT '变更时间',
  `bind_status` int(255) NULL DEFAULT NULL COMMENT '绑定状态0需要绑定1会员待绑定2会员绑定中3会员已绑定',
  `del_status` int(255) NULL DEFAULT NULL COMMENT '删除状态',
  `origin_member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收益权人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_member_tag
-- ----------------------------
DROP TABLE IF EXISTS `site_member_tag`;
CREATE TABLE `site_member_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_modify_marks
-- ----------------------------
DROP TABLE IF EXISTS `site_modify_marks`;
CREATE TABLE `site_modify_marks`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `chr_type` int(255) NULL DEFAULT NULL COMMENT '类型:1栏目,2内容',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '操作时间',
  `create_id` bigint(20) NULL DEFAULT NULL COMMENT '操作人ID',
  `create_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作人',
  `operate_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作IP',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 151 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_node
-- ----------------------------
DROP TABLE IF EXISTS `site_node`;
CREATE TABLE `site_node`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `id` bigint(20) NOT NULL,
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种标识：tc，en，sc',
  `lang_flag` int(11) NULL DEFAULT NULL COMMENT '1此记录只用于繁体 2表示此记录既用于繁体有用于简体s',
  `model_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型类型',
  `node_type` int(11) NULL DEFAULT NULL COMMENT '栏目类型 1文章,2链接',
  `out_link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `node_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目代号',
  `parent_node` bigint(20) NULL DEFAULT NULL COMMENT '父级栏目',
  `leave_path` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '级别路径',
  `node_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目名称',
  `node_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目模板',
  `list_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表模板',
  `content_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容模板',
  `node_pic` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目图片',
  `node_atlas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目图集',
  `node_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目说明',
  `node_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目备注',
  `need_review` int(11) NULL DEFAULT NULL COMMENT '是否需要审核',
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'mate关键字',
  `mate_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'mate说明',
  `show_menu` int(11) NULL DEFAULT NULL COMMENT '展示菜单',
  `show_path` int(11) NULL DEFAULT NULL COMMENT '展示导航',
  `nsort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `node_ext_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目扩展模型代号',
  `extend_fields_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展模型内容',
  `ext_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展模型代号',
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '进入安全密码',
  `down_list` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '下载文件的集合',
  `review_status` int(255) NULL DEFAULT NULL COMMENT '审核状态',
  `is_show` int(255) NULL DEFAULT NULL COMMENT '是否显示',
  `lucene_index` int(255) NULL DEFAULT NULL COMMENT '是否全文索引',
  `del_flag` int(255) NULL DEFAULT NULL COMMENT '删除状态',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '多语言栏目' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_node_field_set
-- ----------------------------
DROP TABLE IF EXISTS `site_node_field_set`;
CREATE TABLE `site_node_field_set`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 60 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_node_field_set_required
-- ----------------------------
DROP TABLE IF EXISTS `site_node_field_set_required`;
CREATE TABLE `site_node_field_set_required`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `field_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_node_marks
-- ----------------------------
DROP TABLE IF EXISTS `site_node_marks`;
CREATE TABLE `site_node_marks`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `mark_id` bigint(20) NULL DEFAULT NULL COMMENT '关联留痕记录ID',
  `data_type` int(1) NULL DEFAULT NULL COMMENT '1旧数据,2新数据',
  `data_id` bigint(20) NOT NULL COMMENT '关联数据ID',
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种标识：tc，en，sc',
  `lang_flag` int(11) NULL DEFAULT NULL COMMENT '1此记录只用于繁体 2表示此记录既用于繁体有用于简体s',
  `model_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模型类型',
  `node_type` int(11) NULL DEFAULT NULL COMMENT '栏目类型 1文章,2链接',
  `out_link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `node_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目代号',
  `parent_node` bigint(20) NULL DEFAULT NULL COMMENT '父级栏目',
  `node_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目名称',
  `node_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目模板',
  `list_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列表模板',
  `content_tpl` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容模板',
  `node_pic` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '栏目图片',
  `node_atlas` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目图集',
  `node_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目说明',
  `node_remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '栏目备注',
  `need_review` int(11) NULL DEFAULT NULL COMMENT '是否需要审核',
  `meta_keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'mate关键字',
  `mate_description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'mate说明',
  `show_menu` int(11) NULL DEFAULT NULL COMMENT '展示菜单',
  `show_path` int(11) NULL DEFAULT NULL COMMENT '展示导航',
  `extend_fields_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '扩展模型内容',
  `ext_model` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '扩展模型代号',
  `pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '进入安全密码',
  `down_list` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '下载文件的集合',
  `is_show` int(255) NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '多语言栏目' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_notice
-- ----------------------------
DROP TABLE IF EXISTS `site_notice`;
CREATE TABLE `site_notice`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chr_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `chr_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简介',
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `recipient` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '接受者',
  `read_user` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '已查看会员',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `create_user` bigint(255) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_other
-- ----------------------------
DROP TABLE IF EXISTS `site_other`;
CREATE TABLE `site_other`  (
  `row_key` bigint(255) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL COMMENT '主键自增长',
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种',
  `lang_flag` int(255) NULL DEFAULT NULL COMMENT '语言标识',
  `chr_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `chr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代号',
  `chr_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `is_show` int(255) NULL DEFAULT NULL,
  `off_status` int(255) NULL DEFAULT NULL,
  `begin_time` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `end_time` varchar(125) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey
-- ----------------------------
DROP TABLE IF EXISTS `site_survey`;
CREATE TABLE `site_survey`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `cover` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '封面图片',
  `survey_catalog` bigint(255) NULL DEFAULT NULL COMMENT '关联分类ID',
  `head_words` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '首语',
  `head_tail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束语',
  `off_status` int(255) NULL DEFAULT NULL COMMENT '是否长期有效:1 长期有效,0时间段內有效',
  `off_datetime` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `survey_role` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'public 公开不限制,all 全体会员需登录,t0...tN Tag 会员,p0,p1..pn 具体会员',
  `result_method` int(255) NULL DEFAULT NULL COMMENT '1,提交之后显示,2完成后显示,3不显示结果',
  `anonymous_type` int(255) NULL DEFAULT NULL COMMENT '1自选.2强制匿名,3不允许匿名',
  `relation_node` bigint(255) NULL DEFAULT NULL COMMENT '关联栏目',
  `post_count` int(255) NULL DEFAULT NULL COMMENT '0',
  `push_msg` int(255) NULL DEFAULT NULL COMMENT '1发送推送,2不发送推送',
  `publish_status` int(255) NULL DEFAULT NULL COMMENT '1已发布,0未发布',
  `publish_datetime` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  `create_user` bigint(255) NULL DEFAULT NULL COMMENT '创建者',
  `create_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `member_view_result` int(11) NULL DEFAULT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代号',
  `is_post_img` int(11) NULL DEFAULT NULL,
  `is_redpackage` int(11) NULL DEFAULT NULL,
  `redpackage_value` decimal(18, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey_answer
-- ----------------------------
DROP TABLE IF EXISTS `site_survey_answer`;
CREATE TABLE `site_survey_answer`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键,自增长',
  `survey_id` bigint(20) NULL DEFAULT NULL COMMENT '问卷ID',
  `member_id` bigint(20) NULL DEFAULT NULL COMMENT '会员ID',
  `member_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '会员名称',
  `anonymous_type` int(255) NULL DEFAULT NULL COMMENT '0匿名,1不匿名',
  `post_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '提交时间',
  `post_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '提交IP',
  `display_name` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `post_img` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey_answer_items
-- ----------------------------
DROP TABLE IF EXISTS `site_survey_answer_items`;
CREATE TABLE `site_survey_answer_items`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID,自增长',
  `survey_id` bigint(20) NULL DEFAULT NULL COMMENT '问卷ID',
  `answer_id` bigint(20) NULL DEFAULT NULL COMMENT '用户提交答案ID',
  `question_id` bigint(20) NULL DEFAULT NULL COMMENT '问题ID',
  `answer` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '回答,单选为选择的项ID,多选为多个项的ID以,分割;打分为项的ID,问答为具体答案,,表格填空为json格式',
  `answer_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '回答,单选为选择的项title other就是填写的值,多选为多个项的title以,分割 other 为填写的值;问答为具体答案,打分为具体份数,表格填空为json格式',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 85 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey_catalog
-- ----------------------------
DROP TABLE IF EXISTS `site_survey_catalog`;
CREATE TABLE `site_survey_catalog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `catalog_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `create_user` bigint(255) NULL DEFAULT NULL COMMENT '创建者',
  `create_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey_items
-- ----------------------------
DROP TABLE IF EXISTS `site_survey_items`;
CREATE TABLE `site_survey_items`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `survey_id` bigint(20) NULL DEFAULT NULL COMMENT '问卷ID',
  `question_id` bigint(20) NULL DEFAULT NULL COMMENT '问题ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问题标题',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '代号:如为其他则代号为other,否则为空,只有单选与多选才会出现other',
  `sort` int(255) NULL DEFAULT NULL COMMENT '排序有',
  `cover` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问题图片',
  `video` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问卷视频',
  `max_star` int(11) NULL DEFAULT NULL COMMENT '最大星数',
  `line_head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行标题',
  `column_head` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列标题',
  `relation_content` bigint(20) NULL DEFAULT NULL COMMENT '关联内容ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 425 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_survey_question
-- ----------------------------
DROP TABLE IF EXISTS `site_survey_question`;
CREATE TABLE `site_survey_question`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `survey_id` bigint(20) NULL DEFAULT NULL COMMENT '关联问卷ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问题标题',
  `cover` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问题图片',
  `video` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '问卷视频',
  `question_type` int(255) NULL DEFAULT NULL COMMENT '问卷类型 1单选,2多选,3问答,4打分,5表格',
  `required` int(255) NULL DEFAULT NULL COMMENT '1必答,0非必答',
  `min_checked` int(11) NULL DEFAULT NULL,
  `max_checked` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 166 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for site_tags
-- ----------------------------
DROP TABLE IF EXISTS `site_tags`;
CREATE TABLE `site_tags`  (
  `row_key` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '列标识,自增长,唯一标识',
  `id` bigint(20) NOT NULL COMMENT '记录ID',
  `lang` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '语种标识：tc，en，sc',
  `lang_flag` int(11) NULL DEFAULT NULL COMMENT '1此记录只用于繁体 2表示此记录既用于繁体有用于简体s',
  `first_letter` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tag_link` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `lang`) USING BTREE,
  INDEX `row_key`(`row_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_account
-- ----------------------------
DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alias_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_status` int(11) NULL DEFAULT NULL,
  `creator_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `last_login_time` datetime(0) NULL DEFAULT NULL,
  `last_login_ip` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `login_count` int(11) NULL DEFAULT NULL,
  `token` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_online_time` datetime(0) NULL DEFAULT NULL,
  `next_set_pwd` datetime(0) NULL DEFAULT NULL,
  `mobile_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chr_name` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_account
-- ----------------------------
INSERT INTO `sys_account` VALUES (1, 'admin', 'c11fe139a75d00ffd37831969133c9f6', 'admin', 'info@gtja.com', 1, 'Admin', '2020-03-03 17:14:38', '2021-05-17 15:45:10', '::ffff:192.168.168.9', 2501, 'c674c3f427354a5d8b2bfe9a3e01a50b', '2021-05-17 15:45:10', '2020-03-03 17:14:38', '', '');

-- ----------------------------
-- Table structure for sys_catalog
-- ----------------------------
DROP TABLE IF EXISTS `sys_catalog`;
CREATE TABLE `sys_catalog`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `chr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '国际化代号',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述, 备注',
  `delete_status` int(11) NOT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `enable_status` int(11) NOT NULL COMMENT '启用状态：0->禁用；1->启用',
  `status` int(11) NOT NULL COMMENT '帐号启用状态：0->禁用；1->启用',
  `last_modifier_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者id',
  `last_modifier_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者名称',
  `last_modified_time` datetime(0) NULL DEFAULT NULL COMMENT '最新修改时间',
  `creator_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者id',
  `creator_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_catalog
-- ----------------------------
INSERT INTO `sys_catalog` VALUES (2, '系统管理', 'sys_manager', 1, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_catalog` VALUES (3, '网站栏目', 'site_node', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_catalog` VALUES (4, '网站内容', 'site_content', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_catalog` VALUES (5, '模版管理', 'site_tpl', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_catalog` VALUES (7, '投票调研', 'vote_survey', 5, '', 0, 1, 1, '1', 'Administrator', '2020-10-30 15:34:39', '1', 'Administrator', '2020-10-30 15:34:39');
INSERT INTO `sys_catalog` VALUES (8, '会员管理', 'site_member', 6, '', 0, 1, 1, '1', 'Administrator', '2020-11-04 15:20:22', '1', 'Administrator', '2020-11-04 15:20:22');

-- ----------------------------
-- Table structure for sys_ext_model
-- ----------------------------
DROP TABLE IF EXISTS `sys_ext_model`;
CREATE TABLE `sys_ext_model`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_ext_model_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_ext_model_detail`;
CREATE TABLE `sys_ext_model_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `p_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `chr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `defaultvalue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sort` int(11) NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `formart_value` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `log_module` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志模块',
  `log_module_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `log_user` bigint(20) NULL DEFAULT NULL COMMENT '操作用户ID',
  `log_user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作用户名称',
  `operate_type` int(11) NULL DEFAULT NULL COMMENT '操作类型',
  `operate_type_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '操作名称',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '说明',
  `log_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `log_time` datetime(0) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1885 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_logs
-- ----------------------------
INSERT INTO `sys_logs` VALUES (1011, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-03 03:36:18');
INSERT INTO `sys_logs` VALUES (1012, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-03 07:27:02');
INSERT INTO `sys_logs` VALUES (1013, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-03 07:52:04');
INSERT INTO `sys_logs` VALUES (1014, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-03 08:19:25');
INSERT INTO `sys_logs` VALUES (1015, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-03 08:40:31');
INSERT INTO `sys_logs` VALUES (1016, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-04 05:47:07');
INSERT INTO `sys_logs` VALUES (1017, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-04 08:32:10');
INSERT INTO `sys_logs` VALUES (1018, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-04 09:08:18');
INSERT INTO `sys_logs` VALUES (1019, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-04 09:10:24');
INSERT INTO `sys_logs` VALUES (1020, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 02:23:06');
INSERT INTO `sys_logs` VALUES (1021, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-05 10:59:34');
INSERT INTO `sys_logs` VALUES (1022, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 03:15:08');
INSERT INTO `sys_logs` VALUES (1023, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 03:50:22');
INSERT INTO `sys_logs` VALUES (1024, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 06:35:40');
INSERT INTO `sys_logs` VALUES (1025, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 06:36:51');
INSERT INTO `sys_logs` VALUES (1026, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-05 14:56:11');
INSERT INTO `sys_logs` VALUES (1027, 'sys_catalog', '菜单管理', 1, 'admin', 2, '添加', '新增菜单栏目:利益分配', '::1', '2021-03-05 14:56:43');
INSERT INTO `sys_logs` VALUES (1028, 'sys_module', '模块管理', 1, 'admin', 2, '添加', '新增菜单模块:利益分配', '::1', '2021-03-05 14:56:53');
INSERT INTO `sys_logs` VALUES (1029, 'sys_resource', '资源管理', 1, 'admin', 2, '添加', '新增菜单资源利益分配,模块ID:35', '::1', '2021-03-05 14:57:03');
INSERT INTO `sys_logs` VALUES (1030, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源导入,操作:导入', '::1', '2021-03-05 14:58:07');
INSERT INTO `sys_logs` VALUES (1031, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源导出,操作:导出', '::1', '2021-03-05 14:58:35');
INSERT INTO `sys_logs` VALUES (1032, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源查看,操作:查看', '::1', '2021-03-05 14:58:43');
INSERT INTO `sys_logs` VALUES (1033, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 07:11:38');
INSERT INTO `sys_logs` VALUES (1034, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 07:14:30');
INSERT INTO `sys_logs` VALUES (1035, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 07:54:25');
INSERT INTO `sys_logs` VALUES (1036, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 08:02:55');
INSERT INTO `sys_logs` VALUES (1037, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 08:03:42');
INSERT INTO `sys_logs` VALUES (1038, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-05 08:43:56');
INSERT INTO `sys_logs` VALUES (1039, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-08 10:31:51');
INSERT INTO `sys_logs` VALUES (1040, 'sys_module', '模块管理', 1, 'admin', 2, '添加', '新增菜单模块:代办人管理', '::1', '2021-03-08 10:32:10');
INSERT INTO `sys_logs` VALUES (1041, 'sys_module', '模块管理', 1, 'admin', 2, '添加', '新增菜单模块:代办人管理', '::1', '2021-03-08 10:32:53');
INSERT INTO `sys_logs` VALUES (1042, 'sys_resource', '资源管理', 1, 'admin', 2, '添加', '新增菜单资源代办人管理,模块ID:36', '::1', '2021-03-08 10:33:21');
INSERT INTO `sys_logs` VALUES (1043, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源查看,操作:查看', '::1', '2021-03-08 10:33:29');
INSERT INTO `sys_logs` VALUES (1044, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-08 05:41:02');
INSERT INTO `sys_logs` VALUES (1045, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-08 14:53:15');
INSERT INTO `sys_logs` VALUES (1046, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-08 06:58:12');
INSERT INTO `sys_logs` VALUES (1047, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-08 06:58:53');
INSERT INTO `sys_logs` VALUES (1048, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::1', '2021-03-08 15:15:47');
INSERT INTO `sys_logs` VALUES (1049, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-08 15:16:45');
INSERT INTO `sys_logs` VALUES (1050, 'sys_roles', '角色列表', 1, 'admin', 3, '修改', '设置角色权限:系统管理员', '::1', '2021-03-08 15:17:12');
INSERT INTO `sys_logs` VALUES (1051, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::1', '2021-03-08 15:21:42');
INSERT INTO `sys_logs` VALUES (1052, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-09 02:26:03');
INSERT INTO `sys_logs` VALUES (1053, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 02:16:04');
INSERT INTO `sys_logs` VALUES (1054, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 05:58:20');
INSERT INTO `sys_logs` VALUES (1055, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 05:59:34');
INSERT INTO `sys_logs` VALUES (1056, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 05:59:47');
INSERT INTO `sys_logs` VALUES (1057, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::ffff:106.55.0.113', '2021-03-10 06:13:17');
INSERT INTO `sys_logs` VALUES (1058, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 07:04:08');
INSERT INTO `sys_logs` VALUES (1059, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 07:04:14');
INSERT INTO `sys_logs` VALUES (1060, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-10 07:04:32');
INSERT INTO `sys_logs` VALUES (1061, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-10 15:26:04');
INSERT INTO `sys_logs` VALUES (1062, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-10 08:03:10');
INSERT INTO `sys_logs` VALUES (1063, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 08:03:26');
INSERT INTO `sys_logs` VALUES (1064, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 08:03:40');
INSERT INTO `sys_logs` VALUES (1065, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 08:04:03');
INSERT INTO `sys_logs` VALUES (1066, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 08:57:38');
INSERT INTO `sys_logs` VALUES (1067, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 09:22:10');
INSERT INTO `sys_logs` VALUES (1068, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 09:30:39');
INSERT INTO `sys_logs` VALUES (1069, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-10 10:26:54');
INSERT INTO `sys_logs` VALUES (1070, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-10 10:27:38');
INSERT INTO `sys_logs` VALUES (1071, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-11 02:22:32');
INSERT INTO `sys_logs` VALUES (1072, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-11 03:43:42');
INSERT INTO `sys_logs` VALUES (1073, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史权益人:', '::ffff:192.168.168.125', '2021-03-11 08:05:46');
INSERT INTO `sys_logs` VALUES (1074, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-11 08:43:10');
INSERT INTO `sys_logs` VALUES (1075, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.125', '2021-03-11 09:38:47');
INSERT INTO `sys_logs` VALUES (1076, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-11 10:01:20');
INSERT INTO `sys_logs` VALUES (1077, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.125', '2021-03-11 10:01:31');
INSERT INTO `sys_logs` VALUES (1078, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.125', '2021-03-11 10:14:09');
INSERT INTO `sys_logs` VALUES (1079, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-12 02:05:51');
INSERT INTO `sys_logs` VALUES (1080, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-12 02:29:47');
INSERT INTO `sys_logs` VALUES (1081, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往权益人信息:', '::ffff:192.168.168.125', '2021-03-12 06:04:39');
INSERT INTO `sys_logs` VALUES (1082, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-12 06:56:17');
INSERT INTO `sys_logs` VALUES (1083, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.138', '2021-03-12 07:35:56');
INSERT INTO `sys_logs` VALUES (1084, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-12 07:36:02');
INSERT INTO `sys_logs` VALUES (1085, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-12 07:40:39');
INSERT INTO `sys_logs` VALUES (1086, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:王宇虹（天络科技）,状态为:UnPass', '::ffff:192.168.168.9', '2021-03-12 09:20:28');
INSERT INTO `sys_logs` VALUES (1087, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-15 02:33:33');
INSERT INTO `sys_logs` VALUES (1088, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-15 07:06:06');
INSERT INTO `sys_logs` VALUES (1089, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-15 08:23:25');
INSERT INTO `sys_logs` VALUES (1090, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-15 08:23:56');
INSERT INTO `sys_logs` VALUES (1091, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::ffff:106.55.0.113', '2021-03-15 08:24:26');
INSERT INTO `sys_logs` VALUES (1092, 'sys_resource', '资源管理', 1, 'admin', 2, '添加', '新增菜单资源回收站,模块ID:32', '::ffff:106.55.0.113', '2021-03-15 08:24:43');
INSERT INTO `sys_logs` VALUES (1093, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源彻底删除,操作:彻底删除', '::ffff:106.55.0.113', '2021-03-15 08:25:06');
INSERT INTO `sys_logs` VALUES (1094, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源恢复,操作:恢复', '::ffff:106.55.0.113', '2021-03-15 08:25:15');
INSERT INTO `sys_logs` VALUES (1095, 'sys_role', '', 6, '大表哥', 3, '修改', '修改角色权限', '::ffff:192.168.168.125', '2021-03-15 08:25:45');
INSERT INTO `sys_logs` VALUES (1096, 'site_recycle_member', '回收站', 6, '大表哥', 8, '恢复', '恢复删除会员:', '::ffff:192.168.168.125', '2021-03-15 08:35:11');
INSERT INTO `sys_logs` VALUES (1097, 'site_recycle_member', '回收站', 6, '大表哥', 8, '恢复', '恢复删除会员:', '::ffff:192.168.168.125', '2021-03-15 08:35:35');
INSERT INTO `sys_logs` VALUES (1098, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-15 08:37:34');
INSERT INTO `sys_logs` VALUES (1099, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-15 08:38:14');
INSERT INTO `sys_logs` VALUES (1100, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.168.125', '2021-03-15 08:39:31');
INSERT INTO `sys_logs` VALUES (1101, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-15 08:59:25');
INSERT INTO `sys_logs` VALUES (1102, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '清除会员:终南山,绑定状态', '::ffff:192.168.168.125', '2021-03-15 08:59:41');
INSERT INTO `sys_logs` VALUES (1103, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-15 09:01:56');
INSERT INTO `sys_logs` VALUES (1104, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.125', '2021-03-15 09:02:53');
INSERT INTO `sys_logs` VALUES (1105, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-15 09:04:10');
INSERT INTO `sys_logs` VALUES (1106, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.125', '2021-03-15 09:11:37');
INSERT INTO `sys_logs` VALUES (1107, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-16 02:23:02');
INSERT INTO `sys_logs` VALUES (1108, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-16 07:17:13');
INSERT INTO `sys_logs` VALUES (1109, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:王宇虹（天络科技）,状态为:UnPass', '::ffff:192.168.168.9', '2021-03-16 07:24:17');
INSERT INTO `sys_logs` VALUES (1110, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:袖雾', '::ffff:192.168.168.9', '2021-03-16 07:57:50');
INSERT INTO `sys_logs` VALUES (1111, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:For dreams', '::ffff:192.168.168.9', '2021-03-16 07:58:09');
INSERT INTO `sys_logs` VALUES (1112, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:11');
INSERT INTO `sys_logs` VALUES (1113, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:24');
INSERT INTO `sys_logs` VALUES (1114, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:26');
INSERT INTO `sys_logs` VALUES (1115, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:28');
INSERT INTO `sys_logs` VALUES (1116, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:30');
INSERT INTO `sys_logs` VALUES (1117, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:45');
INSERT INTO `sys_logs` VALUES (1118, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:47');
INSERT INTO `sys_logs` VALUES (1119, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:50');
INSERT INTO `sys_logs` VALUES (1120, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:52');
INSERT INTO `sys_logs` VALUES (1121, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:54');
INSERT INTO `sys_logs` VALUES (1122, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:55');
INSERT INTO `sys_logs` VALUES (1123, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:57');
INSERT INTO `sys_logs` VALUES (1124, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:58:59');
INSERT INTO `sys_logs` VALUES (1125, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:59:00');
INSERT INTO `sys_logs` VALUES (1126, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 07:59:02');
INSERT INTO `sys_logs` VALUES (1127, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:41');
INSERT INTO `sys_logs` VALUES (1128, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:43');
INSERT INTO `sys_logs` VALUES (1129, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:45');
INSERT INTO `sys_logs` VALUES (1130, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:46');
INSERT INTO `sys_logs` VALUES (1131, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:47');
INSERT INTO `sys_logs` VALUES (1132, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:49');
INSERT INTO `sys_logs` VALUES (1133, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:51');
INSERT INTO `sys_logs` VALUES (1134, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:52');
INSERT INTO `sys_logs` VALUES (1135, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:53');
INSERT INTO `sys_logs` VALUES (1136, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-16 08:00:55');
INSERT INTO `sys_logs` VALUES (1137, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:30');
INSERT INTO `sys_logs` VALUES (1138, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:44');
INSERT INTO `sys_logs` VALUES (1139, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:46');
INSERT INTO `sys_logs` VALUES (1140, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:49');
INSERT INTO `sys_logs` VALUES (1141, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:51');
INSERT INTO `sys_logs` VALUES (1142, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.125', '2021-03-16 08:03:53');
INSERT INTO `sys_logs` VALUES (1143, 'site_recycle_member', '回收站', 6, '大表哥', 5, '删除', '彻底删除会员:For dreams', '::ffff:192.168.168.125', '2021-03-16 08:03:56');
INSERT INTO `sys_logs` VALUES (1144, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-17 02:14:13');
INSERT INTO `sys_logs` VALUES (1145, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-17 08:38:49');
INSERT INTO `sys_logs` VALUES (1146, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:35');
INSERT INTO `sys_logs` VALUES (1147, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:40');
INSERT INTO `sys_logs` VALUES (1148, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:42');
INSERT INTO `sys_logs` VALUES (1149, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:44');
INSERT INTO `sys_logs` VALUES (1150, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:46');
INSERT INTO `sys_logs` VALUES (1151, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:48');
INSERT INTO `sys_logs` VALUES (1152, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:50');
INSERT INTO `sys_logs` VALUES (1153, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:52');
INSERT INTO `sys_logs` VALUES (1154, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:53');
INSERT INTO `sys_logs` VALUES (1155, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:43:56');
INSERT INTO `sys_logs` VALUES (1156, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:袖雾', '::ffff:192.168.168.9', '2021-03-17 08:44:32');
INSERT INTO `sys_logs` VALUES (1157, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:34');
INSERT INTO `sys_logs` VALUES (1158, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:36');
INSERT INTO `sys_logs` VALUES (1159, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:38');
INSERT INTO `sys_logs` VALUES (1160, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:40');
INSERT INTO `sys_logs` VALUES (1161, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:41');
INSERT INTO `sys_logs` VALUES (1162, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:43');
INSERT INTO `sys_logs` VALUES (1163, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:44');
INSERT INTO `sys_logs` VALUES (1164, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:46');
INSERT INTO `sys_logs` VALUES (1165, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-17 08:44:48');
INSERT INTO `sys_logs` VALUES (1166, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-17 08:47:47');
INSERT INTO `sys_logs` VALUES (1167, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-17 08:52:37');
INSERT INTO `sys_logs` VALUES (1168, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-18 03:30:58');
INSERT INTO `sys_logs` VALUES (1169, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:26');
INSERT INTO `sys_logs` VALUES (1170, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:34');
INSERT INTO `sys_logs` VALUES (1171, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:36');
INSERT INTO `sys_logs` VALUES (1172, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:38');
INSERT INTO `sys_logs` VALUES (1173, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:40');
INSERT INTO `sys_logs` VALUES (1174, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:42');
INSERT INTO `sys_logs` VALUES (1175, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:44');
INSERT INTO `sys_logs` VALUES (1176, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:46');
INSERT INTO `sys_logs` VALUES (1177, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:49');
INSERT INTO `sys_logs` VALUES (1178, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:51');
INSERT INTO `sys_logs` VALUES (1179, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:53');
INSERT INTO `sys_logs` VALUES (1180, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:55');
INSERT INTO `sys_logs` VALUES (1181, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:57');
INSERT INTO `sys_logs` VALUES (1182, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:50:59');
INSERT INTO `sys_logs` VALUES (1183, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:01');
INSERT INTO `sys_logs` VALUES (1184, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:03');
INSERT INTO `sys_logs` VALUES (1185, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:05');
INSERT INTO `sys_logs` VALUES (1186, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:06');
INSERT INTO `sys_logs` VALUES (1187, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:09');
INSERT INTO `sys_logs` VALUES (1188, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:12');
INSERT INTO `sys_logs` VALUES (1189, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:14');
INSERT INTO `sys_logs` VALUES (1190, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:16');
INSERT INTO `sys_logs` VALUES (1191, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:18');
INSERT INTO `sys_logs` VALUES (1192, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:20');
INSERT INTO `sys_logs` VALUES (1193, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:22');
INSERT INTO `sys_logs` VALUES (1194, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:23');
INSERT INTO `sys_logs` VALUES (1195, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:25');
INSERT INTO `sys_logs` VALUES (1196, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:27');
INSERT INTO `sys_logs` VALUES (1197, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:29');
INSERT INTO `sys_logs` VALUES (1198, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:30');
INSERT INTO `sys_logs` VALUES (1199, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:32');
INSERT INTO `sys_logs` VALUES (1200, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:39');
INSERT INTO `sys_logs` VALUES (1201, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:42');
INSERT INTO `sys_logs` VALUES (1202, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:45');
INSERT INTO `sys_logs` VALUES (1203, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:47');
INSERT INTO `sys_logs` VALUES (1204, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:49');
INSERT INTO `sys_logs` VALUES (1205, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:50');
INSERT INTO `sys_logs` VALUES (1206, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:52');
INSERT INTO `sys_logs` VALUES (1207, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:53');
INSERT INTO `sys_logs` VALUES (1208, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:55');
INSERT INTO `sys_logs` VALUES (1209, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:51:57');
INSERT INTO `sys_logs` VALUES (1210, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:56:42');
INSERT INTO `sys_logs` VALUES (1211, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:08');
INSERT INTO `sys_logs` VALUES (1212, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:10');
INSERT INTO `sys_logs` VALUES (1213, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:11');
INSERT INTO `sys_logs` VALUES (1214, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:13');
INSERT INTO `sys_logs` VALUES (1215, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:14');
INSERT INTO `sys_logs` VALUES (1216, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:16');
INSERT INTO `sys_logs` VALUES (1217, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:17');
INSERT INTO `sys_logs` VALUES (1218, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:19');
INSERT INTO `sys_logs` VALUES (1219, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:20');
INSERT INTO `sys_logs` VALUES (1220, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:21');
INSERT INTO `sys_logs` VALUES (1221, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:23');
INSERT INTO `sys_logs` VALUES (1222, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:25');
INSERT INTO `sys_logs` VALUES (1223, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:26');
INSERT INTO `sys_logs` VALUES (1224, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:28');
INSERT INTO `sys_logs` VALUES (1225, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:29');
INSERT INTO `sys_logs` VALUES (1226, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:31');
INSERT INTO `sys_logs` VALUES (1227, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:32');
INSERT INTO `sys_logs` VALUES (1228, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:34');
INSERT INTO `sys_logs` VALUES (1229, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:35');
INSERT INTO `sys_logs` VALUES (1230, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-03-18 03:59:37');
INSERT INTO `sys_logs` VALUES (1231, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-18 05:38:12');
INSERT INTO `sys_logs` VALUES (1232, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.9', '2021-03-18 06:26:50');
INSERT INTO `sys_logs` VALUES (1233, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-18 06:28:24');
INSERT INTO `sys_logs` VALUES (1234, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:袖雾', '::ffff:192.168.168.97', '2021-03-18 06:28:50');
INSERT INTO `sys_logs` VALUES (1235, 'site_recycle_member', '回收站', 1, 'admin', 8, '恢复', '恢复删除会员:袖雾', '::ffff:192.168.168.97', '2021-03-18 06:29:20');
INSERT INTO `sys_logs` VALUES (1236, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 06:29:26');
INSERT INTO `sys_logs` VALUES (1237, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 06:32:40');
INSERT INTO `sys_logs` VALUES (1238, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 06:41:55');
INSERT INTO `sys_logs` VALUES (1239, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 06:50:24');
INSERT INTO `sys_logs` VALUES (1240, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 07:02:47');
INSERT INTO `sys_logs` VALUES (1241, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-18 07:15:07');
INSERT INTO `sys_logs` VALUES (1242, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-18 07:15:08');
INSERT INTO `sys_logs` VALUES (1243, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-18 07:17:38');
INSERT INTO `sys_logs` VALUES (1244, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 07:22:26');
INSERT INTO `sys_logs` VALUES (1245, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 07:28:53');
INSERT INTO `sys_logs` VALUES (1246, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 07:33:51');
INSERT INTO `sys_logs` VALUES (1247, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-03-18 07:42:00');
INSERT INTO `sys_logs` VALUES (1248, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-18 07:43:33');
INSERT INTO `sys_logs` VALUES (1249, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-18 17:26:28');
INSERT INTO `sys_logs` VALUES (1250, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-18 09:28:21');
INSERT INTO `sys_logs` VALUES (1251, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-18 09:30:19');
INSERT INTO `sys_logs` VALUES (1252, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.94', '2021-03-18 09:57:27');
INSERT INTO `sys_logs` VALUES (1253, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 02:02:35');
INSERT INTO `sys_logs` VALUES (1254, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-19 02:39:10');
INSERT INTO `sys_logs` VALUES (1255, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '审核会员:曾云龙 hello yolo,状态为:Pass', '::ffff:192.168.168.125', '2021-03-19 03:22:02');
INSERT INTO `sys_logs` VALUES (1256, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-19 06:12:56');
INSERT INTO `sys_logs` VALUES (1257, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 06:28:04');
INSERT INTO `sys_logs` VALUES (1258, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.125', '2021-03-19 08:15:50');
INSERT INTO `sys_logs` VALUES (1259, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:Pass', '::ffff:192.168.168.9', '2021-03-19 08:46:45');
INSERT INTO `sys_logs` VALUES (1260, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 08:54:31');
INSERT INTO `sys_logs` VALUES (1261, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-19 09:08:58');
INSERT INTO `sys_logs` VALUES (1262, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 09:08:59');
INSERT INTO `sys_logs` VALUES (1263, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 09:19:58');
INSERT INTO `sys_logs` VALUES (1264, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-19 09:42:33');
INSERT INTO `sys_logs` VALUES (1265, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.97', '2021-03-19 09:43:00');
INSERT INTO `sys_logs` VALUES (1266, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-19 09:43:59');
INSERT INTO `sys_logs` VALUES (1267, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-19 09:48:12');
INSERT INTO `sys_logs` VALUES (1268, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-19 10:02:23');
INSERT INTO `sys_logs` VALUES (1269, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-19 10:14:37');
INSERT INTO `sys_logs` VALUES (1270, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-19 10:15:14');
INSERT INTO `sys_logs` VALUES (1271, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-19 10:20:35');
INSERT INTO `sys_logs` VALUES (1272, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 10:30:46');
INSERT INTO `sys_logs` VALUES (1273, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-19 10:31:22');
INSERT INTO `sys_logs` VALUES (1274, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 10:35:55');
INSERT INTO `sys_logs` VALUES (1275, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 10:36:28');
INSERT INTO `sys_logs` VALUES (1276, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 10:37:27');
INSERT INTO `sys_logs` VALUES (1277, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-19 10:38:01');
INSERT INTO `sys_logs` VALUES (1278, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:两宝妈', '::ffff:192.168.168.9', '2021-03-19 10:41:32');
INSERT INTO `sys_logs` VALUES (1279, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-19 10:46:39');
INSERT INTO `sys_logs` VALUES (1280, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-19 10:46:46');
INSERT INTO `sys_logs` VALUES (1281, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-19 10:46:58');
INSERT INTO `sys_logs` VALUES (1282, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 10:47:11');
INSERT INTO `sys_logs` VALUES (1283, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-19 10:47:26');
INSERT INTO `sys_logs` VALUES (1284, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-19 10:48:39');
INSERT INTO `sys_logs` VALUES (1285, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-03-19 10:48:53');
INSERT INTO `sys_logs` VALUES (1286, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-22 05:49:54');
INSERT INTO `sys_logs` VALUES (1287, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 06:09:39');
INSERT INTO `sys_logs` VALUES (1288, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-03-22 06:24:13');
INSERT INTO `sys_logs` VALUES (1289, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-03-22 06:28:31');
INSERT INTO `sys_logs` VALUES (1290, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-22 06:55:01');
INSERT INTO `sys_logs` VALUES (1291, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 06:57:01');
INSERT INTO `sys_logs` VALUES (1292, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-22 07:00:33');
INSERT INTO `sys_logs` VALUES (1293, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 07:19:54');
INSERT INTO `sys_logs` VALUES (1294, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 07:23:41');
INSERT INTO `sys_logs` VALUES (1295, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:106.55.0.113', '2021-03-22 07:31:43');
INSERT INTO `sys_logs` VALUES (1296, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 07:40:00');
INSERT INTO `sys_logs` VALUES (1297, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.168.125', '2021-03-22 08:23:52');
INSERT INTO `sys_logs` VALUES (1298, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 09:16:10');
INSERT INTO `sys_logs` VALUES (1299, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-22 09:26:20');
INSERT INTO `sys_logs` VALUES (1300, 'cms_content_g_4', '内容管理--办事流程(workProcess)', 1, 'admin', 3, '修改', '修改网站内容显示状态:测试=>禁用', '::ffff:192.168.168.9', '2021-03-22 09:42:40');
INSERT INTO `sys_logs` VALUES (1301, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-22 10:09:22');
INSERT INTO `sys_logs` VALUES (1302, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-22 10:36:46');
INSERT INTO `sys_logs` VALUES (1303, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-23 01:50:34');
INSERT INTO `sys_logs` VALUES (1304, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-23 02:23:41');
INSERT INTO `sys_logs` VALUES (1305, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-23 02:25:08');
INSERT INTO `sys_logs` VALUES (1306, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-23 02:41:18');
INSERT INTO `sys_logs` VALUES (1307, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-23 02:41:36');
INSERT INTO `sys_logs` VALUES (1308, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-03-23 03:37:10');
INSERT INTO `sys_logs` VALUES (1309, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.97', '2021-03-23 03:39:06');
INSERT INTO `sys_logs` VALUES (1310, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-23 05:46:08');
INSERT INTO `sys_logs` VALUES (1311, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::ffff:106.55.0.113', '2021-03-24 03:41:06');
INSERT INTO `sys_logs` VALUES (1312, 'site_node', '网站栏目-一级栏目', 1, 'admin', 2, '添加', '创建跟节点栏目:注册须知', '::ffff:106.55.0.113', '2021-03-24 03:41:21');
INSERT INTO `sys_logs` VALUES (1313, 'cms_content_g_17', '', 1, 'admin', 2, '添加', '新增注册须知栏目下内容:注册须知', '::ffff:106.55.0.113', '2021-03-24 03:41:51');
INSERT INTO `sys_logs` VALUES (1314, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:106.55.0.113', '2021-03-24 03:43:23');
INSERT INTO `sys_logs` VALUES (1315, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:106.55.0.113', '2021-03-24 03:46:49');
INSERT INTO `sys_logs` VALUES (1316, 'cms_content_g_17', '', 1, 'admin', 3, '修改', '修改内容:注册须知', '::ffff:106.55.0.113', '2021-03-24 03:50:45');
INSERT INTO `sys_logs` VALUES (1317, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-24 05:41:33');
INSERT INTO `sys_logs` VALUES (1318, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:106.55.0.113', '2021-03-24 06:23:48');
INSERT INTO `sys_logs` VALUES (1319, 'cms_content_g_17', '', 1, 'admin', 3, '修改', '修改内容:注册须知', '::ffff:106.55.0.113', '2021-03-24 08:04:23');
INSERT INTO `sys_logs` VALUES (1320, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-24 08:11:42');
INSERT INTO `sys_logs` VALUES (1321, 'cms_content_g_17', '', 1, 'admin', 3, '修改', '修改内容:注册须知', '::ffff:106.55.0.113', '2021-03-24 08:28:57');
INSERT INTO `sys_logs` VALUES (1322, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-03-24 08:34:51');
INSERT INTO `sys_logs` VALUES (1323, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-25 06:22:46');
INSERT INTO `sys_logs` VALUES (1324, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-26 01:49:27');
INSERT INTO `sys_logs` VALUES (1325, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.94', '2021-03-29 01:29:30');
INSERT INTO `sys_logs` VALUES (1326, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.94', '2021-03-29 01:40:51');
INSERT INTO `sys_logs` VALUES (1327, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-03-29 05:45:26');
INSERT INTO `sys_logs` VALUES (1328, 'sys_account', '账号列表', 1, 'admin', 3, '修改', '修改账号信息与密码,account:admin007,别名:大表哥', '::ffff:192.168.168.9', '2021-03-29 06:17:29');
INSERT INTO `sys_logs` VALUES (1329, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-29 06:18:17');
INSERT INTO `sys_logs` VALUES (1330, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-29 14:24:52');
INSERT INTO `sys_logs` VALUES (1331, 'sys_module', '模块管理', 1, 'admin', 2, '添加', '新增菜单模块:受益权人管理', '::1', '2021-03-29 14:25:55');
INSERT INTO `sys_logs` VALUES (1332, 'sys_resource', '资源管理', 1, 'admin', 2, '添加', '新增菜单资源受益权人,模块ID:38', '::1', '2021-03-29 14:26:08');
INSERT INTO `sys_logs` VALUES (1333, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源导入,操作:导入', '::1', '2021-03-29 14:26:21');
INSERT INTO `sys_logs` VALUES (1334, 'sys_operate', '操作管理', 1, 'admin', 2, '添加', '新增菜单资源操作,资源修改,操作:修改', '::1', '2021-03-29 14:26:31');
INSERT INTO `sys_logs` VALUES (1335, 'site_member', '会员管理', 6, '大表哥', 7, '审核', '清除会员:两宝妈,绑定状态', '::ffff:192.168.169.43', '2021-03-29 06:34:23');
INSERT INTO `sys_logs` VALUES (1336, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:UnPass', '::ffff:192.168.168.9', '2021-03-29 06:46:44');
INSERT INTO `sys_logs` VALUES (1337, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-29 07:18:32');
INSERT INTO `sys_logs` VALUES (1338, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 01:59:42');
INSERT INTO `sys_logs` VALUES (1339, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:01:43');
INSERT INTO `sys_logs` VALUES (1340, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:06:41');
INSERT INTO `sys_logs` VALUES (1341, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:09:15');
INSERT INTO `sys_logs` VALUES (1342, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:09:39');
INSERT INTO `sys_logs` VALUES (1343, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:12:06');
INSERT INTO `sys_logs` VALUES (1344, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:13:34');
INSERT INTO `sys_logs` VALUES (1345, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:16:00');
INSERT INTO `sys_logs` VALUES (1346, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:17:54');
INSERT INTO `sys_logs` VALUES (1347, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:25:09');
INSERT INTO `sys_logs` VALUES (1348, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:26:36');
INSERT INTO `sys_logs` VALUES (1349, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:31:49');
INSERT INTO `sys_logs` VALUES (1350, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:40:31');
INSERT INTO `sys_logs` VALUES (1351, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 02:48:14');
INSERT INTO `sys_logs` VALUES (1352, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 07:47:14');
INSERT INTO `sys_logs` VALUES (1353, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 07:56:46');
INSERT INTO `sys_logs` VALUES (1354, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 08:19:30');
INSERT INTO `sys_logs` VALUES (1355, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 08:39:26');
INSERT INTO `sys_logs` VALUES (1356, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 08:52:37');
INSERT INTO `sys_logs` VALUES (1357, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-30 09:08:09');
INSERT INTO `sys_logs` VALUES (1358, 'sys_role', '', 6, '大表哥', 3, '修改', '修改角色权限', '::ffff:192.168.169.43', '2021-03-30 09:31:49');
INSERT INTO `sys_logs` VALUES (1359, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 01:31:22');
INSERT INTO `sys_logs` VALUES (1360, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 03:06:59');
INSERT INTO `sys_logs` VALUES (1361, 'login', '登录', 1, 'admin', 1, '登录', 'admin登录系统', '::1', '2021-03-31 12:09:46');
INSERT INTO `sys_logs` VALUES (1362, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:叶一新', '::ffff:192.168.169.43', '2021-03-31 06:24:05');
INSERT INTO `sys_logs` VALUES (1363, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:叶一新', '::ffff:192.168.169.43', '2021-03-31 06:24:31');
INSERT INTO `sys_logs` VALUES (1364, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:叶一新', '::ffff:192.168.169.43', '2021-03-31 06:25:25');
INSERT INTO `sys_logs` VALUES (1365, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:叶一新', '::ffff:192.168.169.43', '2021-03-31 06:25:32');
INSERT INTO `sys_logs` VALUES (1366, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:叶一新', '::ffff:192.168.169.43', '2021-03-31 06:26:35');
INSERT INTO `sys_logs` VALUES (1367, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-03-31 06:29:56');
INSERT INTO `sys_logs` VALUES (1368, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-03-31 06:30:02');
INSERT INTO `sys_logs` VALUES (1369, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 06:43:07');
INSERT INTO `sys_logs` VALUES (1370, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史受益权人:', '::ffff:192.168.169.43', '2021-03-31 07:04:51');
INSERT INTO `sys_logs` VALUES (1371, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 07:26:54');
INSERT INTO `sys_logs` VALUES (1372, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 07:31:01');
INSERT INTO `sys_logs` VALUES (1373, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 07:35:06');
INSERT INTO `sys_logs` VALUES (1374, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-03-31 07:35:31');
INSERT INTO `sys_logs` VALUES (1375, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-03-31 07:47:35');
INSERT INTO `sys_logs` VALUES (1376, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-03-31 07:49:48');
INSERT INTO `sys_logs` VALUES (1377, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-03-31 07:52:52');
INSERT INTO `sys_logs` VALUES (1378, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-03-31 08:06:13');
INSERT INTO `sys_logs` VALUES (1379, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-03-31 08:06:37');
INSERT INTO `sys_logs` VALUES (1380, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 08:13:12');
INSERT INTO `sys_logs` VALUES (1381, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-03-31 08:13:30');
INSERT INTO `sys_logs` VALUES (1382, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-03-31 08:13:48');
INSERT INTO `sys_logs` VALUES (1383, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:梁伟婷', '::ffff:192.168.169.43', '2021-03-31 08:15:28');
INSERT INTO `sys_logs` VALUES (1384, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:梁伟婷', '::ffff:192.168.169.43', '2021-03-31 08:15:35');
INSERT INTO `sys_logs` VALUES (1385, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-03-31 08:20:52');
INSERT INTO `sys_logs` VALUES (1386, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史受益权人:', '::ffff:192.168.169.43', '2021-03-31 08:32:51');
INSERT INTO `sys_logs` VALUES (1387, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 08:47:17');
INSERT INTO `sys_logs` VALUES (1388, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-03-31 08:47:29');
INSERT INTO `sys_logs` VALUES (1389, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-03-31 08:53:30');
INSERT INTO `sys_logs` VALUES (1390, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史受益权人:', '::ffff:192.168.169.43', '2021-03-31 08:56:34');
INSERT INTO `sys_logs` VALUES (1391, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 09:06:12');
INSERT INTO `sys_logs` VALUES (1392, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-03-31 09:27:51');
INSERT INTO `sys_logs` VALUES (1393, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 07:12:48');
INSERT INTO `sys_logs` VALUES (1394, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 08:26:57');
INSERT INTO `sys_logs` VALUES (1395, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 08:46:55');
INSERT INTO `sys_logs` VALUES (1396, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-01 09:24:33');
INSERT INTO `sys_logs` VALUES (1397, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 09:27:23');
INSERT INTO `sys_logs` VALUES (1398, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-01 09:32:58');
INSERT INTO `sys_logs` VALUES (1399, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-01 09:35:40');
INSERT INTO `sys_logs` VALUES (1400, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 09:35:56');
INSERT INTO `sys_logs` VALUES (1401, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-01 09:39:04');
INSERT INTO `sys_logs` VALUES (1402, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-01 09:39:26');
INSERT INTO `sys_logs` VALUES (1403, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-01 09:49:55');
INSERT INTO `sys_logs` VALUES (1404, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-01 09:55:04');
INSERT INTO `sys_logs` VALUES (1405, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-01 10:01:03');
INSERT INTO `sys_logs` VALUES (1406, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-01 10:01:06');
INSERT INTO `sys_logs` VALUES (1407, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-01 10:03:10');
INSERT INTO `sys_logs` VALUES (1408, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-01 10:05:29');
INSERT INTO `sys_logs` VALUES (1409, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-02 01:32:59');
INSERT INTO `sys_logs` VALUES (1410, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 01:47:57');
INSERT INTO `sys_logs` VALUES (1411, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 01:48:01');
INSERT INTO `sys_logs` VALUES (1412, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 01:48:50');
INSERT INTO `sys_logs` VALUES (1413, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 01:49:57');
INSERT INTO `sys_logs` VALUES (1414, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 01:50:11');
INSERT INTO `sys_logs` VALUES (1415, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 01:50:34');
INSERT INTO `sys_logs` VALUES (1416, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 01:54:44');
INSERT INTO `sys_logs` VALUES (1417, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 01:54:45');
INSERT INTO `sys_logs` VALUES (1418, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 01:54:57');
INSERT INTO `sys_logs` VALUES (1419, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 01:55:51');
INSERT INTO `sys_logs` VALUES (1420, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 01:58:37');
INSERT INTO `sys_logs` VALUES (1421, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:12:06');
INSERT INTO `sys_logs` VALUES (1422, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:15:44');
INSERT INTO `sys_logs` VALUES (1423, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:16:04');
INSERT INTO `sys_logs` VALUES (1424, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:19:14');
INSERT INTO `sys_logs` VALUES (1425, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:22:08');
INSERT INTO `sys_logs` VALUES (1426, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:22:58');
INSERT INTO `sys_logs` VALUES (1427, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:24:21');
INSERT INTO `sys_logs` VALUES (1428, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:24:41');
INSERT INTO `sys_logs` VALUES (1429, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:28:29');
INSERT INTO `sys_logs` VALUES (1430, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:29:53');
INSERT INTO `sys_logs` VALUES (1431, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:31:36');
INSERT INTO `sys_logs` VALUES (1432, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:34:11');
INSERT INTO `sys_logs` VALUES (1433, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:34:18');
INSERT INTO `sys_logs` VALUES (1434, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.9', '2021-04-02 02:36:03');
INSERT INTO `sys_logs` VALUES (1435, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:37:20');
INSERT INTO `sys_logs` VALUES (1436, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 02:47:25');
INSERT INTO `sys_logs` VALUES (1437, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 02:50:42');
INSERT INTO `sys_logs` VALUES (1438, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 03:01:19');
INSERT INTO `sys_logs` VALUES (1439, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 03:02:33');
INSERT INTO `sys_logs` VALUES (1440, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:27:55');
INSERT INTO `sys_logs` VALUES (1441, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:31:29');
INSERT INTO `sys_logs` VALUES (1442, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:32:18');
INSERT INTO `sys_logs` VALUES (1443, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:32:21');
INSERT INTO `sys_logs` VALUES (1444, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:34:58');
INSERT INTO `sys_logs` VALUES (1445, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:35:00');
INSERT INTO `sys_logs` VALUES (1446, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-02 03:35:32');
INSERT INTO `sys_logs` VALUES (1447, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 03:35:37');
INSERT INTO `sys_logs` VALUES (1448, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:天络王宇虹', '::ffff:192.168.168.9', '2021-04-02 03:41:42');
INSERT INTO `sys_logs` VALUES (1449, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-02 03:42:21');
INSERT INTO `sys_logs` VALUES (1450, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-02 03:42:43');
INSERT INTO `sys_logs` VALUES (1451, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 03:47:57');
INSERT INTO `sys_logs` VALUES (1452, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 03:48:52');
INSERT INTO `sys_logs` VALUES (1453, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 03:49:15');
INSERT INTO `sys_logs` VALUES (1454, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 03:49:21');
INSERT INTO `sys_logs` VALUES (1455, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 03:49:59');
INSERT INTO `sys_logs` VALUES (1456, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 03:50:08');
INSERT INTO `sys_logs` VALUES (1457, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 03:54:37');
INSERT INTO `sys_logs` VALUES (1458, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:34:07');
INSERT INTO `sys_logs` VALUES (1459, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:34:14');
INSERT INTO `sys_logs` VALUES (1460, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:34:58');
INSERT INTO `sys_logs` VALUES (1461, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:35:28');
INSERT INTO `sys_logs` VALUES (1462, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:35:40');
INSERT INTO `sys_logs` VALUES (1463, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:35:49');
INSERT INTO `sys_logs` VALUES (1464, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:38:49');
INSERT INTO `sys_logs` VALUES (1465, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:50:00');
INSERT INTO `sys_logs` VALUES (1466, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.9', '2021-04-02 05:50:06');
INSERT INTO `sys_logs` VALUES (1467, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:51:40');
INSERT INTO `sys_logs` VALUES (1468, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:53:50');
INSERT INTO `sys_logs` VALUES (1469, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 05:54:04');
INSERT INTO `sys_logs` VALUES (1470, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:13:15');
INSERT INTO `sys_logs` VALUES (1471, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.168.9', '2021-04-02 06:20:35');
INSERT INTO `sys_logs` VALUES (1472, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.168.9', '2021-04-02 06:20:52');
INSERT INTO `sys_logs` VALUES (1473, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:34:20');
INSERT INTO `sys_logs` VALUES (1474, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:15');
INSERT INTO `sys_logs` VALUES (1475, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:17');
INSERT INTO `sys_logs` VALUES (1476, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:18');
INSERT INTO `sys_logs` VALUES (1477, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:21');
INSERT INTO `sys_logs` VALUES (1478, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:23');
INSERT INTO `sys_logs` VALUES (1479, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:27');
INSERT INTO `sys_logs` VALUES (1480, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:30');
INSERT INTO `sys_logs` VALUES (1481, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:32');
INSERT INTO `sys_logs` VALUES (1482, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:34');
INSERT INTO `sys_logs` VALUES (1483, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:36');
INSERT INTO `sys_logs` VALUES (1484, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:38');
INSERT INTO `sys_logs` VALUES (1485, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:40');
INSERT INTO `sys_logs` VALUES (1486, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:42');
INSERT INTO `sys_logs` VALUES (1487, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:44');
INSERT INTO `sys_logs` VALUES (1488, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:45');
INSERT INTO `sys_logs` VALUES (1489, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:47');
INSERT INTO `sys_logs` VALUES (1490, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:48');
INSERT INTO `sys_logs` VALUES (1491, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:50');
INSERT INTO `sys_logs` VALUES (1492, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:52');
INSERT INTO `sys_logs` VALUES (1493, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 06:44:53');
INSERT INTO `sys_logs` VALUES (1494, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:48:48');
INSERT INTO `sys_logs` VALUES (1495, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:48:55');
INSERT INTO `sys_logs` VALUES (1496, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:50:18');
INSERT INTO `sys_logs` VALUES (1497, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 06:57:28');
INSERT INTO `sys_logs` VALUES (1498, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:10:35');
INSERT INTO `sys_logs` VALUES (1499, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:12:19');
INSERT INTO `sys_logs` VALUES (1500, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:12:29');
INSERT INTO `sys_logs` VALUES (1501, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:12:42');
INSERT INTO `sys_logs` VALUES (1502, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:13:04');
INSERT INTO `sys_logs` VALUES (1503, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:15:31');
INSERT INTO `sys_logs` VALUES (1504, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:15:44');
INSERT INTO `sys_logs` VALUES (1505, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:15:49');
INSERT INTO `sys_logs` VALUES (1506, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:15:59');
INSERT INTO `sys_logs` VALUES (1507, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:16:08');
INSERT INTO `sys_logs` VALUES (1508, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:16:17');
INSERT INTO `sys_logs` VALUES (1509, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:18:18');
INSERT INTO `sys_logs` VALUES (1510, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:18:46');
INSERT INTO `sys_logs` VALUES (1511, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:18:55');
INSERT INTO `sys_logs` VALUES (1512, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:20:37');
INSERT INTO `sys_logs` VALUES (1513, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:22:26');
INSERT INTO `sys_logs` VALUES (1514, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:22:31');
INSERT INTO `sys_logs` VALUES (1515, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:22:47');
INSERT INTO `sys_logs` VALUES (1516, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-02 07:22:52');
INSERT INTO `sys_logs` VALUES (1517, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:27:36');
INSERT INTO `sys_logs` VALUES (1518, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:27:45');
INSERT INTO `sys_logs` VALUES (1519, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-02 07:28:06');
INSERT INTO `sys_logs` VALUES (1520, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:28:21');
INSERT INTO `sys_logs` VALUES (1521, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:28:47');
INSERT INTO `sys_logs` VALUES (1522, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:28:55');
INSERT INTO `sys_logs` VALUES (1523, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:29:03');
INSERT INTO `sys_logs` VALUES (1524, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:29:19');
INSERT INTO `sys_logs` VALUES (1525, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:29:31');
INSERT INTO `sys_logs` VALUES (1526, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:29:39');
INSERT INTO `sys_logs` VALUES (1527, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:30:08');
INSERT INTO `sys_logs` VALUES (1528, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:30:18');
INSERT INTO `sys_logs` VALUES (1529, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:30:26');
INSERT INTO `sys_logs` VALUES (1530, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 07:31:22');
INSERT INTO `sys_logs` VALUES (1531, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 07:31:25');
INSERT INTO `sys_logs` VALUES (1532, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 07:31:27');
INSERT INTO `sys_logs` VALUES (1533, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 07:31:29');
INSERT INTO `sys_logs` VALUES (1534, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:32:04');
INSERT INTO `sys_logs` VALUES (1535, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-02 07:32:21');
INSERT INTO `sys_logs` VALUES (1536, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:袖雾', '::ffff:192.168.168.9', '2021-04-02 07:32:58');
INSERT INTO `sys_logs` VALUES (1537, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史受益权人:', '::ffff:192.168.169.43', '2021-04-02 08:11:38');
INSERT INTO `sys_logs` VALUES (1538, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:12:39');
INSERT INTO `sys_logs` VALUES (1539, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:12:57');
INSERT INTO `sys_logs` VALUES (1540, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:13:11');
INSERT INTO `sys_logs` VALUES (1541, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:14:14');
INSERT INTO `sys_logs` VALUES (1542, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:14:24');
INSERT INTO `sys_logs` VALUES (1543, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-02 08:14:29');
INSERT INTO `sys_logs` VALUES (1544, 'site_history_member', '', 6, '大表哥', 5, '删除', '删除历史受益权人:', '::ffff:192.168.169.43', '2021-04-02 08:14:40');
INSERT INTO `sys_logs` VALUES (1545, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-02 08:32:39');
INSERT INTO `sys_logs` VALUES (1546, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-02 08:33:44');
INSERT INTO `sys_logs` VALUES (1547, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:05');
INSERT INTO `sys_logs` VALUES (1548, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:07');
INSERT INTO `sys_logs` VALUES (1549, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:08');
INSERT INTO `sys_logs` VALUES (1550, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:10');
INSERT INTO `sys_logs` VALUES (1551, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:11');
INSERT INTO `sys_logs` VALUES (1552, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:12');
INSERT INTO `sys_logs` VALUES (1553, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:14');
INSERT INTO `sys_logs` VALUES (1554, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:16');
INSERT INTO `sys_logs` VALUES (1555, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:17');
INSERT INTO `sys_logs` VALUES (1556, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:19');
INSERT INTO `sys_logs` VALUES (1557, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:21');
INSERT INTO `sys_logs` VALUES (1558, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:23');
INSERT INTO `sys_logs` VALUES (1559, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:24');
INSERT INTO `sys_logs` VALUES (1560, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:27');
INSERT INTO `sys_logs` VALUES (1561, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:28');
INSERT INTO `sys_logs` VALUES (1562, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:30');
INSERT INTO `sys_logs` VALUES (1563, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:31');
INSERT INTO `sys_logs` VALUES (1564, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:33');
INSERT INTO `sys_logs` VALUES (1565, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:35');
INSERT INTO `sys_logs` VALUES (1566, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:36');
INSERT INTO `sys_logs` VALUES (1567, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:38');
INSERT INTO `sys_logs` VALUES (1568, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:39');
INSERT INTO `sys_logs` VALUES (1569, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:41');
INSERT INTO `sys_logs` VALUES (1570, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:42');
INSERT INTO `sys_logs` VALUES (1571, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:44');
INSERT INTO `sys_logs` VALUES (1572, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:45');
INSERT INTO `sys_logs` VALUES (1573, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:47');
INSERT INTO `sys_logs` VALUES (1574, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:48');
INSERT INTO `sys_logs` VALUES (1575, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:50');
INSERT INTO `sys_logs` VALUES (1576, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:51');
INSERT INTO `sys_logs` VALUES (1577, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:53');
INSERT INTO `sys_logs` VALUES (1578, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:54');
INSERT INTO `sys_logs` VALUES (1579, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:56');
INSERT INTO `sys_logs` VALUES (1580, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:57');
INSERT INTO `sys_logs` VALUES (1581, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:47:59');
INSERT INTO `sys_logs` VALUES (1582, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-02 08:48:01');
INSERT INTO `sys_logs` VALUES (1583, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-02 08:57:29');
INSERT INTO `sys_logs` VALUES (1584, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:张廷峰', '::ffff:192.168.169.43', '2021-04-02 09:06:05');
INSERT INTO `sys_logs` VALUES (1585, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:袖雾', '::ffff:192.168.168.9', '2021-04-02 09:26:03');
INSERT INTO `sys_logs` VALUES (1586, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-02 09:30:36');
INSERT INTO `sys_logs` VALUES (1587, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:林兴家', '::ffff:192.168.169.43', '2021-04-02 09:35:08');
INSERT INTO `sys_logs` VALUES (1588, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:林兴家', '::ffff:192.168.169.43', '2021-04-02 09:35:42');
INSERT INTO `sys_logs` VALUES (1589, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:张珍珍', '::ffff:192.168.169.43', '2021-04-02 09:37:12');
INSERT INTO `sys_logs` VALUES (1590, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:张珍珍', '::ffff:192.168.169.43', '2021-04-02 09:37:17');
INSERT INTO `sys_logs` VALUES (1591, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-04-02 09:37:33');
INSERT INTO `sys_logs` VALUES (1592, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:王来', '::ffff:192.168.169.43', '2021-04-02 09:37:42');
INSERT INTO `sys_logs` VALUES (1593, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:张廷峰', '::ffff:192.168.169.43', '2021-04-02 09:38:26');
INSERT INTO `sys_logs` VALUES (1594, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.9', '2021-04-02 09:43:21');
INSERT INTO `sys_logs` VALUES (1595, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-06 05:49:27');
INSERT INTO `sys_logs` VALUES (1596, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 06:04:11');
INSERT INTO `sys_logs` VALUES (1597, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改会员信息:', '::ffff:192.168.169.43', '2021-04-06 06:06:54');
INSERT INTO `sys_logs` VALUES (1598, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-06 06:06:57');
INSERT INTO `sys_logs` VALUES (1599, 'site_member', '会员管理', 6, '大表哥', 5, '删除', '删除会员:', '::ffff:192.168.169.43', '2021-04-06 06:06:59');
INSERT INTO `sys_logs` VALUES (1600, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-06 06:08:52');
INSERT INTO `sys_logs` VALUES (1601, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:15');
INSERT INTO `sys_logs` VALUES (1602, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:16');
INSERT INTO `sys_logs` VALUES (1603, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:20');
INSERT INTO `sys_logs` VALUES (1604, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:22');
INSERT INTO `sys_logs` VALUES (1605, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:24');
INSERT INTO `sys_logs` VALUES (1606, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:25');
INSERT INTO `sys_logs` VALUES (1607, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:27');
INSERT INTO `sys_logs` VALUES (1608, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:29');
INSERT INTO `sys_logs` VALUES (1609, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:30');
INSERT INTO `sys_logs` VALUES (1610, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:32');
INSERT INTO `sys_logs` VALUES (1611, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:34');
INSERT INTO `sys_logs` VALUES (1612, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:35');
INSERT INTO `sys_logs` VALUES (1613, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:37');
INSERT INTO `sys_logs` VALUES (1614, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:39');
INSERT INTO `sys_logs` VALUES (1615, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:40');
INSERT INTO `sys_logs` VALUES (1616, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:42');
INSERT INTO `sys_logs` VALUES (1617, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:44');
INSERT INTO `sys_logs` VALUES (1618, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:45');
INSERT INTO `sys_logs` VALUES (1619, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:47');
INSERT INTO `sys_logs` VALUES (1620, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:49');
INSERT INTO `sys_logs` VALUES (1621, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:51');
INSERT INTO `sys_logs` VALUES (1622, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:52');
INSERT INTO `sys_logs` VALUES (1623, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:54');
INSERT INTO `sys_logs` VALUES (1624, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:55');
INSERT INTO `sys_logs` VALUES (1625, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:57');
INSERT INTO `sys_logs` VALUES (1626, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:09:59');
INSERT INTO `sys_logs` VALUES (1627, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:01');
INSERT INTO `sys_logs` VALUES (1628, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:02');
INSERT INTO `sys_logs` VALUES (1629, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:05');
INSERT INTO `sys_logs` VALUES (1630, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:07');
INSERT INTO `sys_logs` VALUES (1631, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:袖雾', '::ffff:192.168.168.9', '2021-04-06 06:10:11');
INSERT INTO `sys_logs` VALUES (1632, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:19');
INSERT INTO `sys_logs` VALUES (1633, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:21');
INSERT INTO `sys_logs` VALUES (1634, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:23');
INSERT INTO `sys_logs` VALUES (1635, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:袖雾', '::ffff:192.168.168.9', '2021-04-06 06:10:24');
INSERT INTO `sys_logs` VALUES (1636, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:26');
INSERT INTO `sys_logs` VALUES (1637, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:28');
INSERT INTO `sys_logs` VALUES (1638, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:30');
INSERT INTO `sys_logs` VALUES (1639, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:33');
INSERT INTO `sys_logs` VALUES (1640, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:35');
INSERT INTO `sys_logs` VALUES (1641, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:38');
INSERT INTO `sys_logs` VALUES (1642, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:40');
INSERT INTO `sys_logs` VALUES (1643, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:41');
INSERT INTO `sys_logs` VALUES (1644, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:43');
INSERT INTO `sys_logs` VALUES (1645, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:46');
INSERT INTO `sys_logs` VALUES (1646, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:48');
INSERT INTO `sys_logs` VALUES (1647, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:50');
INSERT INTO `sys_logs` VALUES (1648, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:54');
INSERT INTO `sys_logs` VALUES (1649, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:55');
INSERT INTO `sys_logs` VALUES (1650, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:57');
INSERT INTO `sys_logs` VALUES (1651, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:10:59');
INSERT INTO `sys_logs` VALUES (1652, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:02');
INSERT INTO `sys_logs` VALUES (1653, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:04');
INSERT INTO `sys_logs` VALUES (1654, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:06');
INSERT INTO `sys_logs` VALUES (1655, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:07');
INSERT INTO `sys_logs` VALUES (1656, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:10');
INSERT INTO `sys_logs` VALUES (1657, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:12');
INSERT INTO `sys_logs` VALUES (1658, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:14');
INSERT INTO `sys_logs` VALUES (1659, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:16');
INSERT INTO `sys_logs` VALUES (1660, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:19');
INSERT INTO `sys_logs` VALUES (1661, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:21');
INSERT INTO `sys_logs` VALUES (1662, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:23');
INSERT INTO `sys_logs` VALUES (1663, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:24');
INSERT INTO `sys_logs` VALUES (1664, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:26');
INSERT INTO `sys_logs` VALUES (1665, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:28');
INSERT INTO `sys_logs` VALUES (1666, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:30');
INSERT INTO `sys_logs` VALUES (1667, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:31');
INSERT INTO `sys_logs` VALUES (1668, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:33');
INSERT INTO `sys_logs` VALUES (1669, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:35');
INSERT INTO `sys_logs` VALUES (1670, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:37');
INSERT INTO `sys_logs` VALUES (1671, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:38');
INSERT INTO `sys_logs` VALUES (1672, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:40');
INSERT INTO `sys_logs` VALUES (1673, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:42');
INSERT INTO `sys_logs` VALUES (1674, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:43');
INSERT INTO `sys_logs` VALUES (1675, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:45');
INSERT INTO `sys_logs` VALUES (1676, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:47');
INSERT INTO `sys_logs` VALUES (1677, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:49');
INSERT INTO `sys_logs` VALUES (1678, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:50');
INSERT INTO `sys_logs` VALUES (1679, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:52');
INSERT INTO `sys_logs` VALUES (1680, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:54');
INSERT INTO `sys_logs` VALUES (1681, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:56');
INSERT INTO `sys_logs` VALUES (1682, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:57');
INSERT INTO `sys_logs` VALUES (1683, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:11:59');
INSERT INTO `sys_logs` VALUES (1684, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:01');
INSERT INTO `sys_logs` VALUES (1685, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:02');
INSERT INTO `sys_logs` VALUES (1686, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:05');
INSERT INTO `sys_logs` VALUES (1687, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:06');
INSERT INTO `sys_logs` VALUES (1688, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:08');
INSERT INTO `sys_logs` VALUES (1689, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:10');
INSERT INTO `sys_logs` VALUES (1690, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:13');
INSERT INTO `sys_logs` VALUES (1691, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:14');
INSERT INTO `sys_logs` VALUES (1692, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:16');
INSERT INTO `sys_logs` VALUES (1693, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:21');
INSERT INTO `sys_logs` VALUES (1694, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:23');
INSERT INTO `sys_logs` VALUES (1695, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:26');
INSERT INTO `sys_logs` VALUES (1696, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:28');
INSERT INTO `sys_logs` VALUES (1697, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:29');
INSERT INTO `sys_logs` VALUES (1698, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:31');
INSERT INTO `sys_logs` VALUES (1699, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:33');
INSERT INTO `sys_logs` VALUES (1700, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:35');
INSERT INTO `sys_logs` VALUES (1701, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:36');
INSERT INTO `sys_logs` VALUES (1702, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:38');
INSERT INTO `sys_logs` VALUES (1703, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:39');
INSERT INTO `sys_logs` VALUES (1704, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 06:12:41');
INSERT INTO `sys_logs` VALUES (1705, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:12:54');
INSERT INTO `sys_logs` VALUES (1706, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:12:56');
INSERT INTO `sys_logs` VALUES (1707, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:12:57');
INSERT INTO `sys_logs` VALUES (1708, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:12:59');
INSERT INTO `sys_logs` VALUES (1709, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:13:00');
INSERT INTO `sys_logs` VALUES (1710, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-06 06:13:02');
INSERT INTO `sys_logs` VALUES (1711, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 07:06:34');
INSERT INTO `sys_logs` VALUES (1712, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-06 07:06:40');
INSERT INTO `sys_logs` VALUES (1713, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-06 07:07:41');
INSERT INTO `sys_logs` VALUES (1714, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 07:07:44');
INSERT INTO `sys_logs` VALUES (1715, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.9', '2021-04-06 07:13:44');
INSERT INTO `sys_logs` VALUES (1716, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-06 07:22:43');
INSERT INTO `sys_logs` VALUES (1717, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 07:23:29');
INSERT INTO `sys_logs` VALUES (1718, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-06 07:35:08');
INSERT INTO `sys_logs` VALUES (1719, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 07:35:13');
INSERT INTO `sys_logs` VALUES (1720, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:两宝妈', '::ffff:192.168.168.9', '2021-04-06 07:35:21');
INSERT INTO `sys_logs` VALUES (1721, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 07:35:25');
INSERT INTO `sys_logs` VALUES (1722, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-06 07:35:46');
INSERT INTO `sys_logs` VALUES (1723, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:两宝妈', '::ffff:192.168.168.9', '2021-04-06 07:35:48');
INSERT INTO `sys_logs` VALUES (1724, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 07:37:50');
INSERT INTO `sys_logs` VALUES (1725, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 07:37:52');
INSERT INTO `sys_logs` VALUES (1726, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 08:13:42');
INSERT INTO `sys_logs` VALUES (1727, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:16:14');
INSERT INTO `sys_logs` VALUES (1728, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:16:52');
INSERT INTO `sys_logs` VALUES (1729, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:17:24');
INSERT INTO `sys_logs` VALUES (1730, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:17:33');
INSERT INTO `sys_logs` VALUES (1731, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:17:42');
INSERT INTO `sys_logs` VALUES (1732, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:18:36');
INSERT INTO `sys_logs` VALUES (1733, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:18:45');
INSERT INTO `sys_logs` VALUES (1734, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-06 08:20:23');
INSERT INTO `sys_logs` VALUES (1735, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-06 08:25:31');
INSERT INTO `sys_logs` VALUES (1736, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-06 08:25:41');
INSERT INTO `sys_logs` VALUES (1737, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:天络王宇虹', '::ffff:192.168.168.9', '2021-04-06 08:27:51');
INSERT INTO `sys_logs` VALUES (1738, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-06 08:28:08');
INSERT INTO `sys_logs` VALUES (1739, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:天络王宇虹', '::ffff:192.168.168.9', '2021-04-06 08:41:34');
INSERT INTO `sys_logs` VALUES (1740, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-06 08:41:40');
INSERT INTO `sys_logs` VALUES (1741, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-06 08:45:17');
INSERT INTO `sys_logs` VALUES (1742, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 09:10:30');
INSERT INTO `sys_logs` VALUES (1743, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-06 09:56:30');
INSERT INTO `sys_logs` VALUES (1744, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:06:27');
INSERT INTO `sys_logs` VALUES (1745, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:06:36');
INSERT INTO `sys_logs` VALUES (1746, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:06:44');
INSERT INTO `sys_logs` VALUES (1747, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:06:54');
INSERT INTO `sys_logs` VALUES (1748, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:07:07');
INSERT INTO `sys_logs` VALUES (1749, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:08:03');
INSERT INTO `sys_logs` VALUES (1750, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:08:28');
INSERT INTO `sys_logs` VALUES (1751, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:12:31');
INSERT INTO `sys_logs` VALUES (1752, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:13:13');
INSERT INTO `sys_logs` VALUES (1753, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-06 10:22:26');
INSERT INTO `sys_logs` VALUES (1754, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 01:11:31');
INSERT INTO `sys_logs` VALUES (1755, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-07 01:16:42');
INSERT INTO `sys_logs` VALUES (1756, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-07 01:18:02');
INSERT INTO `sys_logs` VALUES (1757, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-07 01:19:07');
INSERT INTO `sys_logs` VALUES (1758, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 01:24:45');
INSERT INTO `sys_logs` VALUES (1759, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 01:25:40');
INSERT INTO `sys_logs` VALUES (1760, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 01:27:10');
INSERT INTO `sys_logs` VALUES (1761, 'interest_distribution', '利益分配', 6, '大表哥', 3, '修改', '修改利益分配数据:叶晓雅,2021', '::ffff:192.168.169.43', '2021-04-07 01:27:21');
INSERT INTO `sys_logs` VALUES (1762, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-07 01:29:36');
INSERT INTO `sys_logs` VALUES (1763, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:27');
INSERT INTO `sys_logs` VALUES (1764, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:33');
INSERT INTO `sys_logs` VALUES (1765, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:35');
INSERT INTO `sys_logs` VALUES (1766, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:39');
INSERT INTO `sys_logs` VALUES (1767, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:41');
INSERT INTO `sys_logs` VALUES (1768, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 01:41:43');
INSERT INTO `sys_logs` VALUES (1769, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 03:12:29');
INSERT INTO `sys_logs` VALUES (1770, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:天络王宇虹', '::ffff:192.168.168.9', '2021-04-07 03:46:56');
INSERT INTO `sys_logs` VALUES (1771, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-07 05:44:15');
INSERT INTO `sys_logs` VALUES (1772, 'site_member', '会员管理', 1, 'admin', 3, '修改', '修改会员信息:', '::ffff:192.168.168.9', '2021-04-07 05:44:25');
INSERT INTO `sys_logs` VALUES (1773, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 05:46:53');
INSERT INTO `sys_logs` VALUES (1774, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试1', '::ffff:192.168.168.9', '2021-04-07 05:49:17');
INSERT INTO `sys_logs` VALUES (1775, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试2', '::ffff:192.168.168.9', '2021-04-07 05:53:08');
INSERT INTO `sys_logs` VALUES (1776, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试2', '::ffff:192.168.168.9', '2021-04-07 05:54:48');
INSERT INTO `sys_logs` VALUES (1777, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 05:58:44');
INSERT INTO `sys_logs` VALUES (1778, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-07 05:58:48');
INSERT INTO `sys_logs` VALUES (1779, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试1', '::ffff:192.168.168.9', '2021-04-07 06:00:22');
INSERT INTO `sys_logs` VALUES (1780, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:张廷峰', '::ffff:192.168.169.43', '2021-04-07 06:01:34');
INSERT INTO `sys_logs` VALUES (1781, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试2', '::ffff:192.168.168.9', '2021-04-07 06:02:12');
INSERT INTO `sys_logs` VALUES (1782, 'origin_member', '受益权人', 6, '大表哥', 3, '修改', '修改受益权人信息:测试2', '::ffff:192.168.169.43', '2021-04-07 06:06:48');
INSERT INTO `sys_logs` VALUES (1783, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试1', '::ffff:192.168.168.9', '2021-04-07 06:10:37');
INSERT INTO `sys_logs` VALUES (1784, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试1', '::ffff:192.168.168.9', '2021-04-07 06:11:34');
INSERT INTO `sys_logs` VALUES (1785, 'origin_member', '受益权人', 1, 'admin', 3, '修改', '修改受益权人信息:测试1', '::ffff:192.168.168.9', '2021-04-07 06:11:58');
INSERT INTO `sys_logs` VALUES (1786, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 06:12:31');
INSERT INTO `sys_logs` VALUES (1787, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-07 06:15:54');
INSERT INTO `sys_logs` VALUES (1788, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 06:18:17');
INSERT INTO `sys_logs` VALUES (1789, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 06:18:39');
INSERT INTO `sys_logs` VALUES (1790, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-07 06:21:35');
INSERT INTO `sys_logs` VALUES (1791, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 06:23:27');
INSERT INTO `sys_logs` VALUES (1792, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 06:26:06');
INSERT INTO `sys_logs` VALUES (1793, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-07 06:26:38');
INSERT INTO `sys_logs` VALUES (1794, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 06:28:34');
INSERT INTO `sys_logs` VALUES (1795, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 06:31:45');
INSERT INTO `sys_logs` VALUES (1796, 'site_member', '会员管理', 1, 'admin', 5, '删除', '删除会员:', '::ffff:192.168.168.9', '2021-04-07 06:34:51');
INSERT INTO `sys_logs` VALUES (1797, 'site_recycle_member', '回收站', 1, 'admin', 5, '删除', '彻底删除会员:', '::ffff:192.168.168.9', '2021-04-07 06:35:01');
INSERT INTO `sys_logs` VALUES (1798, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:Pass', '::ffff:192.168.168.9', '2021-04-07 06:37:47');
INSERT INTO `sys_logs` VALUES (1799, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 06:54:58');
INSERT INTO `sys_logs` VALUES (1800, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 07:01:59');
INSERT INTO `sys_logs` VALUES (1801, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:两宝妈', '::ffff:192.168.168.9', '2021-04-07 07:05:27');
INSERT INTO `sys_logs` VALUES (1802, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 07:06:30');
INSERT INTO `sys_logs` VALUES (1803, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 07:16:08');
INSERT INTO `sys_logs` VALUES (1804, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:16:46');
INSERT INTO `sys_logs` VALUES (1805, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:16:48');
INSERT INTO `sys_logs` VALUES (1806, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:20:03');
INSERT INTO `sys_logs` VALUES (1807, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 07:23:41');
INSERT INTO `sys_logs` VALUES (1808, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:37:04');
INSERT INTO `sys_logs` VALUES (1809, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:37:12');
INSERT INTO `sys_logs` VALUES (1810, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 07:37:14');
INSERT INTO `sys_logs` VALUES (1811, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 07:52:29');
INSERT INTO `sys_logs` VALUES (1812, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:14:17');
INSERT INTO `sys_logs` VALUES (1813, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 08:19:11');
INSERT INTO `sys_logs` VALUES (1814, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:20:48');
INSERT INTO `sys_logs` VALUES (1815, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 08:21:00');
INSERT INTO `sys_logs` VALUES (1816, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 08:33:20');
INSERT INTO `sys_logs` VALUES (1817, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:33:27');
INSERT INTO `sys_logs` VALUES (1818, 'site_member', '会员管理', 6, '大表哥', 3, '修改', '修改过往受益权人信息:', '::ffff:192.168.169.43', '2021-04-07 08:33:40');
INSERT INTO `sys_logs` VALUES (1819, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-07 08:38:33');
INSERT INTO `sys_logs` VALUES (1820, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:48:34');
INSERT INTO `sys_logs` VALUES (1821, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-04-07 08:50:58');
INSERT INTO `sys_logs` VALUES (1822, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:52:20');
INSERT INTO `sys_logs` VALUES (1823, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 08:54:47');
INSERT INTO `sys_logs` VALUES (1824, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 09:00:59');
INSERT INTO `sys_logs` VALUES (1825, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 09:08:42');
INSERT INTO `sys_logs` VALUES (1826, 'site_history_member', '', 1, 'admin', 5, '删除', '删除历史受益权人:', '::ffff:192.168.168.9', '2021-04-07 09:11:55');
INSERT INTO `sys_logs` VALUES (1827, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 09:13:15');
INSERT INTO `sys_logs` VALUES (1828, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 09:19:50');
INSERT INTO `sys_logs` VALUES (1829, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-07 09:23:00');
INSERT INTO `sys_logs` VALUES (1830, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-07 09:42:39');
INSERT INTO `sys_logs` VALUES (1831, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-07 09:45:01');
INSERT INTO `sys_logs` VALUES (1832, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 09:46:37');
INSERT INTO `sys_logs` VALUES (1833, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-07 09:56:08');
INSERT INTO `sys_logs` VALUES (1834, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-08 01:15:19');
INSERT INTO `sys_logs` VALUES (1835, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-08 01:55:01');
INSERT INTO `sys_logs` VALUES (1836, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-08 01:58:08');
INSERT INTO `sys_logs` VALUES (1837, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-08 02:57:40');
INSERT INTO `sys_logs` VALUES (1838, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-04-08 06:45:53');
INSERT INTO `sys_logs` VALUES (1839, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-08 08:53:36');
INSERT INTO `sys_logs` VALUES (1840, 'cms_content_g_17', '内容管理--注册须知(registration_agreement)', 1, 'admin', 3, '修改', '修改内容:注册须知', '::ffff:192.168.168.9', '2021-04-08 09:03:23');
INSERT INTO `sys_logs` VALUES (1841, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-09 02:28:16');
INSERT INTO `sys_logs` VALUES (1842, 'Login', '系统后台登录', 6, '大表哥', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-09 06:02:39');
INSERT INTO `sys_logs` VALUES (1843, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-09 06:47:50');
INSERT INTO `sys_logs` VALUES (1844, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-04-13 06:05:12');
INSERT INTO `sys_logs` VALUES (1845, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-04-13 06:05:26');
INSERT INTO `sys_logs` VALUES (1846, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.97', '2021-04-13 06:12:51');
INSERT INTO `sys_logs` VALUES (1847, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.97', '2021-04-13 06:13:20');
INSERT INTO `sys_logs` VALUES (1848, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.97', '2021-04-14 06:36:01');
INSERT INTO `sys_logs` VALUES (1849, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:UnPass', '::ffff:192.168.168.97', '2021-04-14 06:56:47');
INSERT INTO `sys_logs` VALUES (1850, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.97', '2021-04-14 06:59:37');
INSERT INTO `sys_logs` VALUES (1851, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.97', '2021-04-14 07:06:23');
INSERT INTO `sys_logs` VALUES (1852, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.97', '2021-04-14 07:07:48');
INSERT INTO `sys_logs` VALUES (1853, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:袖雾,绑定状态', '::ffff:192.168.168.97', '2021-04-14 07:08:41');
INSERT INTO `sys_logs` VALUES (1854, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:袖雾,状态为:Pass', '::ffff:192.168.168.97', '2021-04-14 07:11:38');
INSERT INTO `sys_logs` VALUES (1855, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:106.55.0.113', '2021-04-16 07:05:34');
INSERT INTO `sys_logs` VALUES (1856, 'site_notice', '通知管理', 1, 'admin', 3, '修改', '新增通知:来开会', '::ffff:106.55.0.113', '2021-04-16 07:05:59');
INSERT INTO `sys_logs` VALUES (1857, 'site_notice', '通知管理', 1, 'admin', 3, '修改', '新增通知:来开会', '::ffff:106.55.0.113', '2021-04-16 07:07:10');
INSERT INTO `sys_logs` VALUES (1858, 'site_notice', '通知管理', 1, 'admin', 3, '修改', '新增通知:快点来开会', '::ffff:106.55.0.113', '2021-04-16 07:07:54');
INSERT INTO `sys_logs` VALUES (1859, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-16 08:37:51');
INSERT INTO `sys_logs` VALUES (1860, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:天络王宇虹,绑定状态', '::ffff:192.168.168.9', '2021-04-16 08:38:08');
INSERT INTO `sys_logs` VALUES (1861, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:Pass', '::ffff:192.168.168.9', '2021-04-16 08:41:59');
INSERT INTO `sys_logs` VALUES (1862, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:Pass', '::ffff:192.168.168.9', '2021-04-16 08:44:08');
INSERT INTO `sys_logs` VALUES (1863, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-16 09:05:35');
INSERT INTO `sys_logs` VALUES (1864, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-16 09:05:53');
INSERT INTO `sys_logs` VALUES (1865, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-16 09:08:45');
INSERT INTO `sys_logs` VALUES (1866, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:两宝妈,状态为:Pass', '::ffff:192.168.168.9', '2021-04-16 09:08:59');
INSERT INTO `sys_logs` VALUES (1867, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:106.55.0.113', '2021-04-19 07:17:40');
INSERT INTO `sys_logs` VALUES (1868, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-23 08:46:26');
INSERT INTO `sys_logs` VALUES (1869, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-23 08:54:24');
INSERT INTO `sys_logs` VALUES (1870, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-23 08:59:15');
INSERT INTO `sys_logs` VALUES (1871, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-25 02:23:58');
INSERT INTO `sys_logs` VALUES (1872, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.169.43', '2021-04-25 06:53:17');
INSERT INTO `sys_logs` VALUES (1873, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-25 07:25:47');
INSERT INTO `sys_logs` VALUES (1874, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-25 07:49:40');
INSERT INTO `sys_logs` VALUES (1875, 'site_member', '会员管理', 1, 'admin', 7, '审核', '清除会员:两宝妈,绑定状态', '::ffff:192.168.168.9', '2021-04-25 08:13:35');
INSERT INTO `sys_logs` VALUES (1876, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-25 08:28:03');
INSERT INTO `sys_logs` VALUES (1877, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-25 08:41:10');
INSERT INTO `sys_logs` VALUES (1878, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.169.43', '2021-04-25 08:43:22');
INSERT INTO `sys_logs` VALUES (1879, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-25 08:51:01');
INSERT INTO `sys_logs` VALUES (1880, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.125', '2021-04-26 05:46:33');
INSERT INTO `sys_logs` VALUES (1881, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-26 07:12:10');
INSERT INTO `sys_logs` VALUES (1882, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-04-28 09:02:18');
INSERT INTO `sys_logs` VALUES (1883, 'site_member', '会员管理', 1, 'admin', 7, '审核', '审核会员:天络王宇虹,状态为:UnPass', '::ffff:192.168.168.9', '2021-04-28 09:17:50');
INSERT INTO `sys_logs` VALUES (1884, 'Login', '系统后台登录', 1, 'admin', 1, '登录', '登录系统', '::ffff:192.168.168.9', '2021-05-17 07:45:11');

-- ----------------------------
-- Table structure for sys_module
-- ----------------------------
DROP TABLE IF EXISTS `sys_module`;
CREATE TABLE `sys_module`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `p_catalog` bigint(20) NOT NULL,
  `module_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `chr_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `is_sys` int(11) NULL DEFAULT NULL COMMENT '是否未系统栏模块 0 ->不是 1->是系统',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述, 备注',
  `delete_status` int(11) NOT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `enable_status` int(11) NOT NULL COMMENT '启用状态：0->禁用；1->启用',
  `status` int(11) NOT NULL COMMENT '帐号启用状态：0->禁用；1->启用',
  `last_modifier_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者id',
  `last_modifier_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者名称',
  `last_modified_time` datetime(0) NULL DEFAULT NULL COMMENT '最新修改时间',
  `creator_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者id',
  `creator_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_module
-- ----------------------------
INSERT INTO `sys_module` VALUES (2, 2, 'sys_account', '系统账号管理', 1, 0, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (3, 2, 'sys_menus', '菜单管理', 2, 0, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (4, 2, 'sys_roles', '角色管理', 3, 0, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (5, 3, 'site_node', '网站栏目', 1, 0, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (6, 4, 'site_content', '网站内容', 1, 0, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (7, 4, 'site_extmodel', '扩展模型', 2, 0, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:10', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (8, 5, 'site_tpl', '模版管理', 1, 0, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (9, 2, 'sys_setting', '系统设置', 4, 0, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (11, 4, 'site_tag', '标签管理', 3, 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (15, 4, 'site_doc', '栏目文件', 6, 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:22', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (16, 4, 'site_doc_type', '栏目文件分类', 7, 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:27', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (17, 2, 'sys_strategy', '安全策略', 8, 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (18, 2, 'sys_log', '操作日志', 9, 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_module` VALUES (20, 3, 'site_node_recycle', '栏目回收站', 3, 0, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:05:11', '1', 'Administrator', '2020-03-04 16:05:11');
INSERT INTO `sys_module` VALUES (21, 3, 'site_node_review', '栏目审核', 2, 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:06:54', '1', 'Administrator', '2020-03-04 16:06:54');
INSERT INTO `sys_module` VALUES (22, 4, 'site_content_review', '内容审核', 5, 0, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:18', '1', 'Administrator', '2020-03-04 16:12:58');
INSERT INTO `sys_module` VALUES (23, 4, 'site_content_recycle', '内容回收站', 4, 0, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:14:35', '1', 'Administrator', '2020-03-04 16:14:23');
INSERT INTO `sys_module` VALUES (24, 4, 'site_other', '附加内容', 8, 0, '', 0, 1, 1, '1', 'Administrator', '2020-03-10 09:57:56', '1', 'Administrator', '2020-03-10 09:57:56');
INSERT INTO `sys_module` VALUES (28, 3, 'node_mark', '栏目留痕', 4, 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-31 13:48:20', '1', 'Administrator', '2020-03-31 13:48:20');
INSERT INTO `sys_module` VALUES (29, 4, 'content_mark', '内容留痕', 10, 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-31 13:49:20', '1', 'Administrator', '2020-03-31 13:49:20');
INSERT INTO `sys_module` VALUES (30, 7, 'survey', '调研管理', 1, 0, '', 0, 1, 1, '1', 'Administrator', '2020-10-30 15:34:55', '1', 'Administrator', '2020-10-30 15:34:55');
INSERT INTO `sys_module` VALUES (31, 7, 'vote', '投票管理', 2, 0, '', 0, 1, 1, '1', 'Administrator', '2020-10-30 15:35:08', '1', 'Administrator', '2020-10-30 15:35:08');
INSERT INTO `sys_module` VALUES (32, 8, 'site_member', '会员管理', 1, 0, '', 0, 1, 1, '1', 'Administrator', '2020-11-04 15:20:37', '1', 'Administrator', '2020-11-04 15:20:37');
INSERT INTO `sys_module` VALUES (33, 8, 'site_member_tag', '会员标签管理', 2, 0, '', 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:38', '1', 'Administrator', '2020-11-04 15:21:38');
INSERT INTO `sys_module` VALUES (34, 4, 'site_notice', '通知管理', 10, 0, '', 0, 1, 1, '1', 'Administrator', '2020-11-16 16:03:43', '1', 'Administrator', '2020-11-16 16:03:43');

-- ----------------------------
-- Table structure for sys_operate
-- ----------------------------
DROP TABLE IF EXISTS `sys_operate`;
CREATE TABLE `sys_operate`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `p_module` bigint(20) NULL DEFAULT NULL,
  `p_module_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `p_res` bigint(20) NOT NULL,
  `p_res_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `operate_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `operate_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `api_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT 'API地址',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述, 备注',
  `is_menu` int(11) NULL DEFAULT NULL,
  `delete_status` int(11) NOT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `enable_status` int(11) NOT NULL COMMENT '启用状态：0->禁用；1->启用',
  `status` int(11) NOT NULL COMMENT '帐号启用状态：0->禁用；1->启用',
  `last_modifier_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者id',
  `last_modifier_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者名称',
  `last_modified_time` datetime(0) NULL DEFAULT NULL COMMENT '最新修改时间',
  `creator_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者id',
  `creator_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18483 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_operate
-- ----------------------------
INSERT INTO `sys_operate` VALUES (10, 2, 'sys_account', 2, 'sys_account', 'add', '添加', 'account/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (11, 2, 'sys_account', 2, 'sys_account', 'view', '查看', 'account/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (12, 2, 'sys_account', 2, 'sys_account', 'list', '列表', 'account/list', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (13, 2, 'sys_account', 2, 'sys_account', 'change_status', '禁用/启用', 'account/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (14, 2, 'sys_account', 2, 'sys_account', 'modify', '修改', 'account/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (15, 2, 'sys_account', 3, 'sys_account_recycle', 'delete', '彻底删除', 'account_recycle/delete/:id', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (16, 2, 'sys_account', 3, 'sys_account_recycle', 'recovery', '恢复', 'account_recycle/recovery/:id', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (17, 2, 'sys_account', 2, 'sys_account', 'delete', '删除', 'account/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (18, 2, 'sys_account', 3, 'sys_account_recycle', 'list', '列表', 'account_recycle/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (19, 2, 'sys_account', 2, 'sys_account', 'set_role', '设置账号角色', 'account/set_roles/:id', 7, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (20, 3, 'sys_menus', 4, 'sys_catalog', 'add', '添加', 'syscatalog/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (21, 3, 'sys_menus', 4, 'sys_catalog', 'view', '查看', 'syscatalog/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (22, 3, 'sys_menus', 4, 'sys_catalog', 'list', '列表', 'syscatalog/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (23, 3, 'sys_menus', 4, 'sys_catalog', 'change_status', '禁用/启用', 'syscatalog/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (24, 3, 'sys_menus', 4, 'sys_catalog', 'modify', '修改', 'syscatalog/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (25, 3, 'sys_menus', 4, 'sys_catalog', 'delete', '删除', 'syscatalog/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (26, 3, 'sys_menus', 5, 'sys_module', 'add', '添加', 'sysmodule/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (27, 3, 'sys_menus', 5, 'sys_module', 'view', '查看', 'sysmodule/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (28, 3, 'sys_menus', 5, 'sys_module', 'list', '列表', 'sysmodule/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (29, 3, 'sys_menus', 5, 'sys_module', 'change_status', '禁用/启用', 'sysmodule/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (30, 3, 'sys_menus', 5, 'sys_module', 'modify', '修改', 'sysmodule/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (31, 3, 'sys_menus', 5, 'sys_module', 'delete', '删除', 'sysmodule/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (32, 3, 'sys_menus', 6, 'sys_resource', 'add', '添加', 'sysresource/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (33, 3, 'sys_menus', 6, 'sys_resource', 'view', '查看', 'sysresource/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (34, 3, 'sys_menus', 6, 'sys_resource', 'list', '列表', 'sysresource/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (35, 3, 'sys_menus', 6, 'sys_resource', 'change_status', '禁用/启用', 'sysresource/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (36, 3, 'sys_menus', 6, 'sys_resource', 'modify', '修改', 'sysresource/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (37, 3, 'sys_menus', 6, 'sys_resource', 'delete', '删除', 'sysresource/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (38, 3, 'sys_menus', 7, 'sys_operate', 'add', '添加', 'sysoperate/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (39, 3, 'sys_menus', 7, 'sys_operate', 'view', '查看', 'sysoperate/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (40, 3, 'sys_menus', 7, 'sys_operate', 'list', '列表', 'sysoperate/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (41, 3, 'sys_menus', 7, 'sys_operate', 'change_status', '禁用/启用', 'sysoperate/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (42, 3, 'sys_menus', 7, 'sys_operate', 'modify', '修改', 'sysoperate/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (43, 3, 'sys_menus', 7, 'sys_operate', 'delete', '删除', 'sysoperate/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (44, 3, 'sys_menus', 4, 'sys_catalog', 'data_catalog_key_value', '获取栏目键值', 'syscatalog/data_key_value/:delete_status', 7, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (45, 3, 'sys_menus', 5, 'sys_module', 'data_module_key_value', '获取模块键值', 'sysmodule/data_key_value/:delete_status', 7, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (46, 3, 'sys_menus', 6, 'sys_resource', 'data_resource_key_value', '获取资源键值', 'sysresource/data_key_value/:delete_status', 7, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (47, 3, 'sys_menus', 7, 'sys_operate', 'data_module_key_value', '获取操作键值', 'sysoperate/data_key_value/:delete_status', 7, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (48, 4, 'sys_roles', 8, 'sys_roles', 'add', '添加', 'sysroles/add', 1, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (49, 4, 'sys_roles', 8, 'sys_roles', 'view', '查看', 'sysroles/:id', 2, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (50, 4, 'sys_roles', 8, 'sys_roles', 'list', '列表', 'sysroles/list', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (51, 4, 'sys_roles', 8, 'sys_roles', 'change_status', '禁用/启用', 'sysroles/change_status', 4, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (52, 4, 'sys_roles', 8, 'sys_roles', 'modify', '修改', 'sysroles/update/:id', 5, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (53, 4, 'sys_roles', 8, 'sys_roles', 'delete', '删除', 'sysroles/delete/:id', 6, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (55, 4, 'sys_roles', 8, 'sys_roles', 'data_role_key_value', '获取角色键值', 'sysroles/data_key_value/:delete_status', 8, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (56, 2, 'sys_account', 2, 'sys_account', 'viewrole', '查看账号角色', 'account_role/:id', 9, '', 1, 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (57, 5, 'site_node', 9, 'site_node', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (58, 5, 'site_node', 9, 'site_node', 'mod', '修改', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (59, 5, 'site_node', 9, 'site_node', 'del', '删除', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (60, 5, 'site_node', 9, 'site_node', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (61, 6, 'site_content', 10, 'site_content', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (62, 6, 'site_content', 10, 'site_content', 'mod', '修改', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (63, 6, 'site_content', 10, 'site_content', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (64, 6, 'site_content', 10, 'site_content', 'sort', '排序', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (65, 7, 'site_extmodel', 11, 'site_extmodel', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (66, 7, 'site_extmodel', 11, 'site_extmodel', 'mod', '修改', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (67, 7, 'site_extmodel', 11, 'site_extmodel', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (68, 7, 'site_extmodel', 11, 'site_extmodel', 'change_status', '状态改变', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (70, 6, 'site_content', 10, 'site_content', 'view', '查看', '', 5, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (71, 7, 'site_extmodel', 11, 'site_extmodel', 'view', '查看', '', 5, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (72, 8, 'site_tpl', 12, 'site_tpl', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (73, 8, 'site_tpl', 12, 'site_tpl', 'mod', '修改', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (74, 8, 'site_tpl', 12, 'site_tpl', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (75, 8, 'site_tpl', 12, 'site_tpl', 'change_status', '状态改变', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (76, 8, 'site_tpl', 12, 'site_tpl', 'view', '查看', '', 5, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (77, 9, 'sys_setting', 13, 'sys_setting', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (78, 9, 'sys_setting', 13, 'sys_setting', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (79, 8, 'site_tpl', 12, 'site_tpl', 'addfile', '添加文件', '', 6, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (607, 8, 'site_tpl', 131, 'site_res', 'use', '使用', 'single/module', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10869, 11, 'site_tag', 10190, 'site_tag', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10870, 11, 'site_tag', 10190, 'site_tag', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10871, 11, 'site_tag', 10190, 'site_tag', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10872, 11, 'site_tag', 10190, 'site_tag', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10873, 12, 'site_author', 10191, 'site_author', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10874, 12, 'site_author', 10191, 'site_author', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10875, 12, 'site_author', 10191, 'site_author', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10876, 12, 'site_author', 10191, 'site_author', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10877, 13, 'site_dept', 10192, 'site_dept', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10878, 13, 'site_dept', 10192, 'site_dept', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10879, 13, 'site_dept', 10192, 'site_dept', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10880, 13, 'site_dept', 10192, 'site_dept', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10881, 14, 'site_industry', 10193, 'site_industry', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10882, 14, 'site_industry', 10193, 'site_industry', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10883, 14, 'site_industry', 10193, 'site_industry', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10884, 14, 'site_industry', 10193, 'site_industry', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10885, 15, 'site_doc', 10194, 'site_doc', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10886, 15, 'site_doc', 10194, 'site_doc', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10887, 15, 'site_doc', 10194, 'site_doc', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10888, 15, 'site_doc', 10194, 'site_doc', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10889, 16, 'site_doc_type', 10195, 'site_doc_type', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10890, 16, 'site_doc_type', 10195, 'site_doc_type', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10891, 16, 'site_doc_type', 10195, 'site_doc_type', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10892, 16, 'site_doc_type', 10195, 'site_doc_type', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10893, 17, 'sys_strategy', 10196, 'sys_strategy', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10894, 17, 'sys_strategy', 10196, 'sys_strategy', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10895, 17, 'sys_strategy', 10196, 'sys_strategy', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10896, 17, 'sys_strategy', 10196, 'sys_strategy', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10897, 18, 'sys_log', 10197, 'sys_log', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_operate` VALUES (10906, 21, 'site_node_review', 10201, 'site_node_review', 'review', '审核', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:07:14', '1', 'Administrator', '2020-03-04 16:07:14');
INSERT INTO `sys_operate` VALUES (10907, 20, 'site_node_recycle', 10202, 'site_node_recycle', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:07:46', '1', 'Administrator', '2020-03-04 16:07:46');
INSERT INTO `sys_operate` VALUES (10908, 20, 'site_node_recycle', 10202, 'site_node_recycle', 'delete', '彻底删除', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:08:04', '1', 'Administrator', '2020-03-04 16:08:04');
INSERT INTO `sys_operate` VALUES (10909, 20, 'site_node_recycle', 10202, 'site_node_recycle', 'recovery', '恢复', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:08:24', '1', 'Administrator', '2020-03-04 16:08:24');
INSERT INTO `sys_operate` VALUES (10910, 21, 'site_node_review', 10201, 'site_node_review', 'view', '查看', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:11:37', '1', 'Administrator', '2020-03-04 16:11:37');
INSERT INTO `sys_operate` VALUES (10911, 22, 'site_content_review', 10203, 'site_content_review', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:49', '1', 'Administrator', '2020-03-04 16:13:49');
INSERT INTO `sys_operate` VALUES (10912, 22, 'site_content_review', 10203, 'site_content_review', 'review', '审核', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:59', '1', 'Administrator', '2020-03-04 16:13:59');
INSERT INTO `sys_operate` VALUES (10913, 23, 'site_content_recycle', 10204, 'site_content_recycle', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:15:03', '1', 'Administrator', '2020-03-04 16:15:03');
INSERT INTO `sys_operate` VALUES (10914, 23, 'site_content_recycle', 10204, 'site_content_recycle', 'delete', '彻底删除', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:15:17', '1', 'Administrator', '2020-03-04 16:15:17');
INSERT INTO `sys_operate` VALUES (10915, 23, 'site_content_recycle', 10204, 'site_content_recycle', 'recovery', '恢复', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-04 16:15:38', '1', 'Administrator', '2020-03-04 16:15:38');
INSERT INTO `sys_operate` VALUES (12116, 24, 'site_other', 10505, 'site_other', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-10 09:58:27', '1', 'Administrator', '2020-03-10 09:58:27');
INSERT INTO `sys_operate` VALUES (12117, 24, 'site_other', 10505, 'site_other', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-10 09:58:36', '1', 'Administrator', '2020-03-10 09:58:36');
INSERT INTO `sys_operate` VALUES (12118, 24, 'site_other', 10505, 'site_other', 'del', '删除', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-10 09:58:47', '1', 'Administrator', '2020-03-10 09:58:47');
INSERT INTO `sys_operate` VALUES (12119, 24, 'site_other', 10505, 'site_other', 'view', '查看', '查看', 4, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-10 09:59:00', '1', 'Administrator', '2020-03-10 09:59:00');
INSERT INTO `sys_operate` VALUES (14238, 28, 'node_mark', 11021, 'node_mark', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-31 13:48:48', '1', 'Administrator', '2020-03-31 13:48:48');
INSERT INTO `sys_operate` VALUES (14239, 29, 'content_mark', 11022, 'content_mark', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-03-31 13:49:49', '1', 'Administrator', '2020-03-31 13:49:49');
INSERT INTO `sys_operate` VALUES (17995, 5, 'site_node', 11859, 'cms_node_g_1', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (17996, 5, 'site_node', 11859, 'cms_node_g_1', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (17997, 5, 'site_node', 11859, 'cms_node_g_1', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (17998, 5, 'site_node', 11859, 'cms_node_g_1', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (17999, 5, 'site_node', 11859, 'cms_node_g_1', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (18000, 6, 'site_content', 11860, 'cms_content_g_1', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (18001, 6, 'site_content', 11860, 'cms_content_g_1', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (18002, 6, 'site_content', 11860, 'cms_content_g_1', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (18003, 6, 'site_content', 11860, 'cms_content_g_1', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_operate` VALUES (18265, 21, 'site_node_review', 10201, 'site_node_review', 'delete', '删除', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-29 17:15:14', '1', 'Administrator', '2020-10-29 17:15:14');
INSERT INTO `sys_operate` VALUES (18266, 22, 'site_content_review', 10203, 'site_content_review', 'delete', '删除', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-29 17:15:37', '1', 'Administrator', '2020-10-29 17:15:37');
INSERT INTO `sys_operate` VALUES (18267, 30, 'survey', 11919, 'survey', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:36:55', '1', 'Administrator', '2020-10-30 15:36:55');
INSERT INTO `sys_operate` VALUES (18268, 30, 'survey', 11919, 'survey', 'delete', '删除', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:37:35', '1', 'Administrator', '2020-10-30 15:37:03');
INSERT INTO `sys_operate` VALUES (18269, 30, 'survey', 11919, 'survey', 'view', '查看', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:37:31', '1', 'Administrator', '2020-10-30 15:37:31');
INSERT INTO `sys_operate` VALUES (18270, 30, 'survey', 11919, 'survey', 'publish', '发布', '', 4, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:37:53', '1', 'Administrator', '2020-10-30 15:37:53');
INSERT INTO `sys_operate` VALUES (18271, 30, 'survey', 11919, 'survey', 'mod', '修改', '', 5, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:38:02', '1', 'Administrator', '2020-10-30 15:38:02');
INSERT INTO `sys_operate` VALUES (18272, 30, 'survey', 11920, 'survey_catalog', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:38:18', '1', 'Administrator', '2020-10-30 15:38:18');
INSERT INTO `sys_operate` VALUES (18273, 30, 'survey', 11920, 'survey_catalog', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:38:29', '1', 'Administrator', '2020-10-30 15:38:29');
INSERT INTO `sys_operate` VALUES (18274, 30, 'survey', 11920, 'survey_catalog', 'delete', '删除', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:38:37', '1', 'Administrator', '2020-10-30 15:38:37');
INSERT INTO `sys_operate` VALUES (18275, 30, 'survey', 11920, 'survey_catalog', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', 'Administrator', '2020-10-30 15:38:48', '1', 'Administrator', '2020-10-30 15:38:48');
INSERT INTO `sys_operate` VALUES (18294, 32, 'site_member', 11923, 'site_member', 'mod', '修改', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:20:59', '1', 'Administrator', '2020-11-04 15:20:59');
INSERT INTO `sys_operate` VALUES (18295, 32, 'site_member', 11923, 'site_member', 'review', '审核', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:06', '1', 'Administrator', '2020-11-04 15:21:06');
INSERT INTO `sys_operate` VALUES (18296, 32, 'site_member', 11923, 'site_member', 'view_all', '查看所有会员', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:12', '1', 'Administrator', '2020-11-04 15:21:12');
INSERT INTO `sys_operate` VALUES (18297, 32, 'site_member', 11923, 'site_member', 'del', '删除', '', 4, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:20', '1', 'Administrator', '2020-11-04 15:21:20');
INSERT INTO `sys_operate` VALUES (18298, 33, 'site_member_tag', 11924, 'site_member_tag', 'del', '删除', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:58', '1', 'Administrator', '2020-11-04 15:21:58');
INSERT INTO `sys_operate` VALUES (18299, 33, 'site_member_tag', 11924, 'site_member_tag', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:22:03', '1', 'Administrator', '2020-11-04 15:22:03');
INSERT INTO `sys_operate` VALUES (18300, 33, 'site_member_tag', 11924, 'site_member_tag', 'add', '添加', '', 3, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:22:11', '1', 'Administrator', '2020-11-04 15:22:11');
INSERT INTO `sys_operate` VALUES (18301, 33, 'site_member_tag', 11924, 'site_member_tag', 'view', '查看', '', 4, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-04 15:22:16', '1', 'Administrator', '2020-11-04 15:22:16');
INSERT INTO `sys_operate` VALUES (18302, 32, 'site_member', 11923, 'site_member', 'data_tag_1', '查看[集团本部]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-04 16:04:15', '1', 'Administrator', '2020-11-04 16:04:15');
INSERT INTO `sys_operate` VALUES (18303, 5, 'site_node', 11925, 'cms_node_g_2', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18304, 5, 'site_node', 11925, 'cms_node_g_2', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18305, 5, 'site_node', 11925, 'cms_node_g_2', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18306, 5, 'site_node', 11925, 'cms_node_g_2', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18307, 5, 'site_node', 11925, 'cms_node_g_2', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18308, 6, 'site_content', 11926, 'cms_content_g_2', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18309, 6, 'site_content', 11926, 'cms_content_g_2', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18310, 6, 'site_content', 11926, 'cms_content_g_2', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18311, 6, 'site_content', 11926, 'cms_content_g_2', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_operate` VALUES (18312, 5, 'site_node', 11927, 'cms_node_g_3', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18313, 5, 'site_node', 11927, 'cms_node_g_3', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18314, 5, 'site_node', 11927, 'cms_node_g_3', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18315, 5, 'site_node', 11927, 'cms_node_g_3', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18316, 5, 'site_node', 11927, 'cms_node_g_3', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18317, 6, 'site_content', 11928, 'cms_content_g_3', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18318, 6, 'site_content', 11928, 'cms_content_g_3', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18319, 6, 'site_content', 11928, 'cms_content_g_3', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18320, 6, 'site_content', 11928, 'cms_content_g_3', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_operate` VALUES (18321, 5, 'site_node', 11929, 'cms_node_g_4', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18322, 5, 'site_node', 11929, 'cms_node_g_4', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18323, 5, 'site_node', 11929, 'cms_node_g_4', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18324, 5, 'site_node', 11929, 'cms_node_g_4', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18325, 5, 'site_node', 11929, 'cms_node_g_4', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18326, 6, 'site_content', 11930, 'cms_content_g_4', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18327, 6, 'site_content', 11930, 'cms_content_g_4', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18328, 6, 'site_content', 11930, 'cms_content_g_4', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18329, 6, 'site_content', 11930, 'cms_content_g_4', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_operate` VALUES (18330, 5, 'site_node', 11931, 'cms_node_g_5', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18331, 5, 'site_node', 11931, 'cms_node_g_5', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18332, 5, 'site_node', 11931, 'cms_node_g_5', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18333, 5, 'site_node', 11931, 'cms_node_g_5', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18334, 5, 'site_node', 11931, 'cms_node_g_5', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18335, 6, 'site_content', 11932, 'cms_content_g_5', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18336, 6, 'site_content', 11932, 'cms_content_g_5', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18337, 6, 'site_content', 11932, 'cms_content_g_5', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18338, 6, 'site_content', 11932, 'cms_content_g_5', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_operate` VALUES (18339, 5, 'site_node', 11933, 'cms_node_g_6', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18340, 5, 'site_node', 11933, 'cms_node_g_6', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18341, 5, 'site_node', 11933, 'cms_node_g_6', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18342, 5, 'site_node', 11933, 'cms_node_g_6', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18343, 5, 'site_node', 11933, 'cms_node_g_6', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18344, 6, 'site_content', 11934, 'cms_content_g_6', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18345, 6, 'site_content', 11934, 'cms_content_g_6', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18346, 6, 'site_content', 11934, 'cms_content_g_6', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18347, 6, 'site_content', 11934, 'cms_content_g_6', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_operate` VALUES (18348, 5, 'site_node', 11935, 'cms_node_g_7', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18349, 5, 'site_node', 11935, 'cms_node_g_7', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18350, 5, 'site_node', 11935, 'cms_node_g_7', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18351, 5, 'site_node', 11935, 'cms_node_g_7', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18352, 5, 'site_node', 11935, 'cms_node_g_7', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18353, 6, 'site_content', 11936, 'cms_content_g_7', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18354, 6, 'site_content', 11936, 'cms_content_g_7', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18355, 6, 'site_content', 11936, 'cms_content_g_7', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18356, 6, 'site_content', 11936, 'cms_content_g_7', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_operate` VALUES (18357, 5, 'site_node', 11937, 'cms_node_g_8', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18358, 5, 'site_node', 11937, 'cms_node_g_8', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18359, 5, 'site_node', 11937, 'cms_node_g_8', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18360, 5, 'site_node', 11937, 'cms_node_g_8', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18361, 5, 'site_node', 11937, 'cms_node_g_8', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18362, 6, 'site_content', 11938, 'cms_content_g_8', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18363, 6, 'site_content', 11938, 'cms_content_g_8', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18364, 6, 'site_content', 11938, 'cms_content_g_8', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18365, 6, 'site_content', 11938, 'cms_content_g_8', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_operate` VALUES (18366, 5, 'site_node', 11939, 'cms_node_g_9', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18367, 5, 'site_node', 11939, 'cms_node_g_9', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18368, 5, 'site_node', 11939, 'cms_node_g_9', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18369, 5, 'site_node', 11939, 'cms_node_g_9', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18370, 5, 'site_node', 11939, 'cms_node_g_9', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18371, 6, 'site_content', 11940, 'cms_content_g_9', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18372, 6, 'site_content', 11940, 'cms_content_g_9', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18373, 6, 'site_content', 11940, 'cms_content_g_9', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18374, 6, 'site_content', 11940, 'cms_content_g_9', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_operate` VALUES (18375, 5, 'site_node', 11941, 'cms_node_g_10', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18376, 5, 'site_node', 11941, 'cms_node_g_10', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18377, 5, 'site_node', 11941, 'cms_node_g_10', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18378, 5, 'site_node', 11941, 'cms_node_g_10', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18379, 5, 'site_node', 11941, 'cms_node_g_10', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18380, 6, 'site_content', 11942, 'cms_content_g_10', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18381, 6, 'site_content', 11942, 'cms_content_g_10', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18382, 6, 'site_content', 11942, 'cms_content_g_10', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18383, 6, 'site_content', 11942, 'cms_content_g_10', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_operate` VALUES (18384, 5, 'site_node', 11943, 'cms_node_g_11', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18385, 5, 'site_node', 11943, 'cms_node_g_11', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18386, 5, 'site_node', 11943, 'cms_node_g_11', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18387, 5, 'site_node', 11943, 'cms_node_g_11', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18388, 5, 'site_node', 11943, 'cms_node_g_11', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18389, 6, 'site_content', 11944, 'cms_content_g_11', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18390, 6, 'site_content', 11944, 'cms_content_g_11', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18391, 6, 'site_content', 11944, 'cms_content_g_11', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18392, 6, 'site_content', 11944, 'cms_content_g_11', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_operate` VALUES (18393, 5, 'site_node', 11945, 'cms_node_g_12', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18394, 5, 'site_node', 11945, 'cms_node_g_12', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18395, 5, 'site_node', 11945, 'cms_node_g_12', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18396, 5, 'site_node', 11945, 'cms_node_g_12', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18397, 5, 'site_node', 11945, 'cms_node_g_12', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18398, 6, 'site_content', 11946, 'cms_content_g_12', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18399, 6, 'site_content', 11946, 'cms_content_g_12', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18400, 6, 'site_content', 11946, 'cms_content_g_12', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18401, 6, 'site_content', 11946, 'cms_content_g_12', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_operate` VALUES (18402, 34, 'site_notice', 11947, 'site_notice', 'add', '添加', '', 1, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-16 16:03:59', '1', 'Administrator', '2020-11-16 16:03:59');
INSERT INTO `sys_operate` VALUES (18403, 34, 'site_notice', 11947, 'site_notice', 'view', '查看', '', 2, '', 1, 0, 1, 1, '1', 'Administrator', '2020-11-16 16:04:06', '1', 'Administrator', '2020-11-16 16:04:06');
INSERT INTO `sys_operate` VALUES (18404, 32, 'site_member', 11923, 'site_member', 'data_tag_2', '查看[劳务公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:55:55', '1', 'Administrator', '2020-11-20 03:55:55');
INSERT INTO `sys_operate` VALUES (18405, 32, 'site_member', 11923, 'site_member', 'data_tag_3', '查看[集团工会]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:04', '1', 'Administrator', '2020-11-20 03:56:04');
INSERT INTO `sys_operate` VALUES (18406, 32, 'site_member', 11923, 'site_member', 'data_tag_4', '查看[生物公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:15', '1', 'Administrator', '2020-11-20 03:56:15');
INSERT INTO `sys_operate` VALUES (18407, 32, 'site_member', 11923, 'site_member', 'data_tag_5', '查看[晨光加工部]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:24', '1', 'Administrator', '2020-11-20 03:56:24');
INSERT INTO `sys_operate` VALUES (18408, 32, 'site_member', 11923, 'site_member', 'data_tag_6', '查看[晨光乳业公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:35', '1', 'Administrator', '2020-11-20 03:56:35');
INSERT INTO `sys_operate` VALUES (18409, 32, 'site_member', 11923, 'site_member', 'data_tag_7', '查看[牛奶公司本部]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:43', '1', 'Administrator', '2020-11-20 03:56:43');
INSERT INTO `sys_operate` VALUES (18410, 32, 'site_member', 11923, 'site_member', 'data_tag_8', '查看[北山牛场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:56:52', '1', 'Administrator', '2020-11-20 03:56:52');
INSERT INTO `sys_operate` VALUES (18411, 32, 'site_member', 11923, 'site_member', 'data_tag_9', '查看[圳美牛场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:02', '1', 'Administrator', '2020-11-20 03:57:02');
INSERT INTO `sys_operate` VALUES (18412, 32, 'site_member', 11923, 'site_member', 'data_tag_10', '查看[新坡头牛场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:10', '1', 'Administrator', '2020-11-20 03:57:10');
INSERT INTO `sys_operate` VALUES (18413, 32, 'site_member', 11923, 'site_member', 'data_tag_11', '查看[圳美果场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:18', '1', 'Administrator', '2020-11-20 03:57:18');
INSERT INTO `sys_operate` VALUES (18414, 32, 'site_member', 11923, 'site_member', 'data_tag_12', '查看[宝明洁公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:28', '1', 'Administrator', '2020-11-20 03:57:28');
INSERT INTO `sys_operate` VALUES (18415, 32, 'site_member', 11923, 'site_member', 'data_tag_13', '查看[迳口果场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:44', '1', 'Administrator', '2020-11-20 03:57:44');
INSERT INTO `sys_operate` VALUES (18416, 32, 'site_member', 11923, 'site_member', 'data_tag_14', '查看[碧眼果场]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:57:52', '1', 'Administrator', '2020-11-20 03:57:52');
INSERT INTO `sys_operate` VALUES (18417, 32, 'site_member', 11923, 'site_member', 'data_tag_15', '查看[畜牧公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:00', '1', 'Administrator', '2020-11-20 03:58:00');
INSERT INTO `sys_operate` VALUES (18418, 32, 'site_member', 11923, 'site_member', 'data_tag_16', '查看[光明鸽公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:08', '1', 'Administrator', '2020-11-20 03:58:08');
INSERT INTO `sys_operate` VALUES (18419, 32, 'site_member', 11923, 'site_member', 'data_tag_17', '查看[光侨公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:14', '1', 'Administrator', '2020-11-20 03:58:14');
INSERT INTO `sys_operate` VALUES (18420, 32, 'site_member', 11923, 'site_member', 'data_tag_18', '查看[运输公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:23', '1', 'Administrator', '2020-11-20 03:58:23');
INSERT INTO `sys_operate` VALUES (18421, 32, 'site_member', 11923, 'site_member', 'data_tag_19', '查看[机械公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:30', '1', 'Administrator', '2020-11-20 03:58:30');
INSERT INTO `sys_operate` VALUES (18422, 32, 'site_member', 11923, 'site_member', 'data_tag_20', '查看[华建公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:38', '1', 'Administrator', '2020-11-20 03:58:38');
INSERT INTO `sys_operate` VALUES (18423, 32, 'site_member', 11923, 'site_member', 'data_tag_21', '查看[经发公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:47', '1', 'Administrator', '2020-11-20 03:58:47');
INSERT INTO `sys_operate` VALUES (18424, 32, 'site_member', 11923, 'site_member', 'data_tag_22', '查看[农科园公司]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:58:55', '1', 'Administrator', '2020-11-20 03:58:55');
INSERT INTO `sys_operate` VALUES (18425, 32, 'site_member', 11923, 'site_member', 'data_tag_23', '查看[退管办]标签会员', '', 1, '', 0, 0, 1, 1, '1', 'Administrator', '2020-11-20 03:59:02', '1', 'Administrator', '2020-11-20 03:59:02');
INSERT INTO `sys_operate` VALUES (18427, 5, 'site_node', 11949, 'cms_node_g_13', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18428, 5, 'site_node', 11949, 'cms_node_g_13', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18429, 5, 'site_node', 11949, 'cms_node_g_13', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18430, 5, 'site_node', 11949, 'cms_node_g_13', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18431, 5, 'site_node', 11949, 'cms_node_g_13', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18432, 6, 'site_content', 11950, 'cms_content_g_13', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18433, 6, 'site_content', 11950, 'cms_content_g_13', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18434, 6, 'site_content', 11950, 'cms_content_g_13', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18435, 6, 'site_content', 11950, 'cms_content_g_13', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_operate` VALUES (18436, 5, 'site_node', 11951, 'cms_node_g_14', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18437, 5, 'site_node', 11951, 'cms_node_g_14', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18438, 5, 'site_node', 11951, 'cms_node_g_14', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18439, 5, 'site_node', 11951, 'cms_node_g_14', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18440, 5, 'site_node', 11951, 'cms_node_g_14', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18441, 6, 'site_content', 11952, 'cms_content_g_14', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18442, 6, 'site_content', 11952, 'cms_content_g_14', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18443, 6, 'site_content', 11952, 'cms_content_g_14', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18444, 6, 'site_content', 11952, 'cms_content_g_14', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_operate` VALUES (18445, 5, 'site_node', 11953, 'cms_node_g_15', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18446, 5, 'site_node', 11953, 'cms_node_g_15', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18447, 5, 'site_node', 11953, 'cms_node_g_15', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18448, 5, 'site_node', 11953, 'cms_node_g_15', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18449, 5, 'site_node', 11953, 'cms_node_g_15', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18450, 6, 'site_content', 11954, 'cms_content_g_15', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18451, 6, 'site_content', 11954, 'cms_content_g_15', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18452, 6, 'site_content', 11954, 'cms_content_g_15', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18453, 6, 'site_content', 11954, 'cms_content_g_15', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_operate` VALUES (18454, 34, 'site_notice', 11947, 'site_notice', 'delete', '删除', '', 3, '', 1, 0, 1, 1, '1', 'admin', '2020-11-26 02:28:52', '1', 'admin', '2020-11-26 02:28:52');
INSERT INTO `sys_operate` VALUES (18455, 5, 'site_node', 11955, 'cms_node_g_16', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18456, 5, 'site_node', 11955, 'cms_node_g_16', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18457, 5, 'site_node', 11955, 'cms_node_g_16', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18458, 5, 'site_node', 11955, 'cms_node_g_16', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18459, 5, 'site_node', 11955, 'cms_node_g_16', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18460, 6, 'site_content', 11956, 'cms_content_g_16', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18461, 6, 'site_content', 11956, 'cms_content_g_16', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18462, 6, 'site_content', 11956, 'cms_content_g_16', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18463, 6, 'site_content', 11956, 'cms_content_g_16', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_operate` VALUES (18464, 35, 'interest_distribution', 11957, 'interest_distribution', 'import', '导入', '', 1, '', 1, 0, 1, 1, '1', 'admin', '2021-03-05 14:58:07', '1', 'admin', '2021-03-05 14:58:07');
INSERT INTO `sys_operate` VALUES (18465, 35, 'interest_distribution', 11957, 'interest_distribution', 'export', '导出', '', 1, '', 1, 0, 1, 1, '1', 'admin', '2021-03-05 14:58:35', '1', 'admin', '2021-03-05 14:58:35');
INSERT INTO `sys_operate` VALUES (18466, 35, 'interest_distribution', 11957, 'interest_distribution', 'view', '查看', '', 3, '', 1, 0, 1, 1, '1', 'admin', '2021-03-05 14:58:43', '1', 'admin', '2021-03-05 14:58:43');
INSERT INTO `sys_operate` VALUES (18467, 36, 'member_agency', 11958, 'member_agency', 'view', '查看', '', 1, '', 1, 0, 1, 1, '1', 'admin', '2021-03-08 10:33:29', '1', 'admin', '2021-03-08 10:33:29');
INSERT INTO `sys_operate` VALUES (18468, 32, 'site_member', 11959, 'site_recycle_member', 'del', '彻底删除', '', 1, '', 1, 0, 1, 1, '1', 'admin', '2021-03-15 08:25:06', '1', 'admin', '2021-03-15 08:25:06');
INSERT INTO `sys_operate` VALUES (18469, 32, 'site_member', 11959, 'site_recycle_member', 'recovery', '恢复', '', 2, '', 1, 0, 1, 1, '1', 'admin', '2021-03-15 08:25:15', '1', 'admin', '2021-03-15 08:25:15');
INSERT INTO `sys_operate` VALUES (18470, 5, 'site_node', 11960, 'cms_node_g_17', 'add', '添加', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18471, 5, 'site_node', 11960, 'cms_node_g_17', 'update', '修改', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18472, 5, 'site_node', 11960, 'cms_node_g_17', 'delete', '删除', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18473, 5, 'site_node', 11960, 'cms_node_g_17', 'sort', '本级子栏目排序', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18474, 5, 'site_node', 11960, 'cms_node_g_17', 'view', '查看', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18475, 6, 'site_content', 11961, 'cms_content_g_17', 'add', '添加内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18476, 6, 'site_content', 11961, 'cms_content_g_17', 'update', '修改内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18477, 6, 'site_content', 11961, 'cms_content_g_17', 'delete', '删除内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18478, 6, 'site_content', 11961, 'cms_content_g_17', 'view', '查看内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18479, 6, 'site_content', 11961, 'cms_content_g_17', 'copy', '复制内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18480, 6, 'site_content', 11961, 'cms_content_g_17', 'move', '移动内容', '', 1, '', 1, 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_operate` VALUES (18481, 38, 'origin_member', 11962, 'origin_member', 'import', '导入', '', 1, '', 1, 0, 1, 1, '1', 'admin', '2021-03-29 14:26:21', '1', 'admin', '2021-03-29 14:26:21');
INSERT INTO `sys_operate` VALUES (18482, 38, 'origin_member', 11962, 'origin_member', 'mod', '修改', '', 2, '', 1, 0, 1, 1, '1', 'admin', '2021-03-29 14:26:31', '1', 'admin', '2021-03-29 14:26:31');

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `p_module` bigint(20) NOT NULL,
  `res_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `res_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名称',
  `uri` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '前端资源路径',
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '连接地址',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述, 备注',
  `delete_status` int(11) NOT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `enable_status` int(11) NOT NULL COMMENT '启用状态：0->禁用；1->启用',
  `status` int(11) NOT NULL COMMENT '帐号启用状态：0->禁用；1->启用',
  `last_modifier_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者id',
  `last_modifier_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者名称',
  `last_modified_time` datetime(0) NULL DEFAULT NULL COMMENT '最新修改时间',
  `creator_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者id',
  `creator_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11963 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES (2, 2, 'sys_account', '账号列表', '/emp/list', '/emp/list', 1, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (3, 2, 'sys_account_recycle', '账号回收站', '/emp/list', '/emp/list', 1, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (4, 3, 'sys_catalog', '菜单管理', '/emp/list', '/emp/list', 1, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (5, 3, 'sys_module', '模块管理', '/emp/list', '/emp/list', 2, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (6, 3, 'sys_resource', '资源管理', '/emp/list', '/emp/list', 3, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (7, 3, 'sys_operate', '操作管理', '/emp/list', '/emp/list', 4, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (8, 4, 'sys_roles', '角色列表', '/emp/list', '/emp/list', 5, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (9, 5, 'site_node', '网站栏目-一级栏目', '', '', 6, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10, 6, 'site_content', '网站内容', '', '', 7, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (11, 7, 'site_extmodel', '扩展模型', '', '', 8, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (12, 8, 'site_tpl', '模版管理', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (13, 9, 'sys_setting', '系统设置', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (131, 8, 'site_res', '网站资源', '/emp/list', '/emp/list', 2, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10190, 11, 'site_tag', '标签管理', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10194, 15, 'site_doc', '栏目文件', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10195, 16, 'site_doc_type', '栏目文件分类', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10196, 17, 'sys_strategy', '安全策略', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10197, 18, 'sys_log', '操作日志', '', '', 1, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');
INSERT INTO `sys_resource` VALUES (10201, 21, 'site_node_review', '栏目审核', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:07:06', '1', 'Administrator', '2020-03-04 16:07:06');
INSERT INTO `sys_resource` VALUES (10202, 20, 'site_node_recycle', '栏目回收站', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:07:36', '1', 'Administrator', '2020-03-04 16:07:36');
INSERT INTO `sys_resource` VALUES (10203, 22, 'site_content_review', '内容审核', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:13:42', '1', 'Administrator', '2020-03-04 16:13:42');
INSERT INTO `sys_resource` VALUES (10204, 23, 'site_content_recycle', '内容回收站', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-04 16:14:54', '1', 'Administrator', '2020-03-04 16:14:54');
INSERT INTO `sys_resource` VALUES (10505, 24, 'site_other', '附加内容', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-10 09:58:16', '1', 'Administrator', '2020-03-10 09:58:16');
INSERT INTO `sys_resource` VALUES (11021, 28, 'node_mark', '栏目留痕', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-31 13:48:39', '1', 'Administrator', '2020-03-31 13:48:39');
INSERT INTO `sys_resource` VALUES (11022, 29, 'content_mark', '内容留痕', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-03-31 13:49:38', '1', 'Administrator', '2020-03-31 13:49:38');
INSERT INTO `sys_resource` VALUES (11859, 5, 'cms_node_g_1', '栏目管理--首页(index)', '', '', 1, '', 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_resource` VALUES (11860, 6, 'cms_content_g_1', '内容管理--首页(index)', '', '', 1, '', 0, 1, 1, '0', '', '2020-05-27 11:14:32', '0', '', '2020-05-27 11:14:32');
INSERT INTO `sys_resource` VALUES (11919, 30, 'survey', '调研管理', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-10-30 15:35:26', '1', 'Administrator', '2020-10-30 15:35:26');
INSERT INTO `sys_resource` VALUES (11920, 30, 'survey_catalog', '调研分类', '', '', 2, '', 0, 1, 1, '1', 'Administrator', '2020-10-30 15:35:51', '1', 'Administrator', '2020-10-30 15:35:51');
INSERT INTO `sys_resource` VALUES (11923, 32, 'site_member', '会员管理', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-11-04 15:20:51', '1', 'Administrator', '2020-11-04 15:20:51');
INSERT INTO `sys_resource` VALUES (11924, 33, 'site_member_tag', '会员标签管理', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-11-04 15:21:49', '1', 'Administrator', '2020-11-04 15:21:49');
INSERT INTO `sys_resource` VALUES (11925, 5, 'cms_node_g_2', '栏目管理--理事会介绍(councilItd)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_resource` VALUES (11926, 6, 'cms_content_g_2', '内容管理--理事会介绍(councilItd)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 02:59:51', '0', '', '2020-11-11 02:59:51');
INSERT INTO `sys_resource` VALUES (11927, 5, 'cms_node_g_3', '栏目管理--理事会架构(councilItdFm)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_resource` VALUES (11928, 6, 'cms_content_g_3', '内容管理--理事会架构(councilItdFm)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:00:41', '0', '', '2020-11-11 03:00:41');
INSERT INTO `sys_resource` VALUES (11929, 5, 'cms_node_g_4', '栏目管理--办事流程(workProcess)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_resource` VALUES (11930, 6, 'cms_content_g_4', '内容管理--办事流程(workProcess)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:01:57', '0', '', '2020-11-11 03:01:57');
INSERT INTO `sys_resource` VALUES (11931, 5, 'cms_node_g_5', '栏目管理--项目动态(ProjectTrends)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_resource` VALUES (11932, 6, 'cms_content_g_5', '内容管理--项目动态(ProjectTrends)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:03:19', '0', '', '2020-11-11 03:03:19');
INSERT INTO `sys_resource` VALUES (11933, 5, 'cms_node_g_6', '栏目管理--制度法规(systemStatute)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_resource` VALUES (11934, 6, 'cms_content_g_6', '内容管理--制度法规(systemStatute)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:04:28', '0', '', '2020-11-11 03:04:28');
INSERT INTO `sys_resource` VALUES (11935, 5, 'cms_node_g_7', '栏目管理--政策法规(policyStatute)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_resource` VALUES (11936, 6, 'cms_content_g_7', '内容管理--政策法规(policyStatute)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:06:29', '0', '', '2020-11-11 03:06:29');
INSERT INTO `sys_resource` VALUES (11937, 5, 'cms_node_g_8', '栏目管理--制度文件(systemDoc)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_resource` VALUES (11938, 6, 'cms_content_g_8', '内容管理--制度文件(systemDoc)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:08:11', '0', '', '2020-11-11 03:08:11');
INSERT INTO `sys_resource` VALUES (11939, 5, 'cms_node_g_9', '栏目管理--投票调查(VotingSurvey)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_resource` VALUES (11940, 6, 'cms_content_g_9', '内容管理--投票调查(VotingSurvey)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:09:32', '0', '', '2020-11-11 03:09:32');
INSERT INTO `sys_resource` VALUES (11941, 5, 'cms_node_g_10', '栏目管理--个人中心(PersonalCenter)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_resource` VALUES (11942, 6, 'cms_content_g_10', '内容管理--个人中心(PersonalCenter)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:11:18', '0', '', '2020-11-11 03:11:18');
INSERT INTO `sys_resource` VALUES (11943, 5, 'cms_node_g_11', '栏目管理--区域管理人员(Rglmanage)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_resource` VALUES (11944, 6, 'cms_content_g_11', '内容管理--区域管理人员(Rglmanage)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:17:18', '0', '', '2020-11-11 03:17:18');
INSERT INTO `sys_resource` VALUES (11945, 5, 'cms_node_g_12', '栏目管理--消息列表(Messagelist)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_resource` VALUES (11946, 6, 'cms_content_g_12', '内容管理--消息列表(Messagelist)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-11 03:18:12', '0', '', '2020-11-11 03:18:12');
INSERT INTO `sys_resource` VALUES (11947, 34, 'site_notice', '通知管理', '', '', 1, '', 0, 1, 1, '1', 'Administrator', '2020-11-16 16:03:52', '1', 'Administrator', '2020-11-16 16:03:52');
INSERT INTO `sys_resource` VALUES (11949, 5, 'cms_node_g_13', '栏目管理--开发测试栏目(tets_site)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_resource` VALUES (11950, 6, 'cms_content_g_13', '内容管理--开发测试栏目(tets_site)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-24 09:11:59', '0', '', '2020-11-24 09:11:59');
INSERT INTO `sys_resource` VALUES (11951, 5, 'cms_node_g_14', '栏目管理--开发测试栏目(kaifa)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_resource` VALUES (11952, 6, 'cms_content_g_14', '内容管理--开发测试栏目(kaifa)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-24 09:13:31', '0', '', '2020-11-24 09:13:31');
INSERT INTO `sys_resource` VALUES (11953, 5, 'cms_node_g_15', '栏目管理--测试(cesi)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_resource` VALUES (11954, 6, 'cms_content_g_15', '内容管理--测试(cesi)', '', '', 1, '', 0, 1, 1, '0', '', '2020-11-25 05:54:33', '0', '', '2020-11-25 05:54:33');
INSERT INTO `sys_resource` VALUES (11955, 5, 'cms_node_g_16', '栏目管理--11111111(jakdada)', '', '', 1, '', 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_resource` VALUES (11956, 6, 'cms_content_g_16', '内容管理--11111111(jakdada)', '', '', 1, '', 0, 1, 1, '0', '', '2020-12-15 03:01:37', '0', '', '2020-12-15 03:01:37');
INSERT INTO `sys_resource` VALUES (11957, 35, 'interest_distribution', '利益分配', '', '', 1, '', 0, 1, 1, '1', 'admin', '2021-03-05 14:57:03', '1', 'admin', '2021-03-05 14:57:03');
INSERT INTO `sys_resource` VALUES (11958, 36, 'member_agency', '代办人管理', '', '', 1, '', 0, 1, 1, '1', 'admin', '2021-03-08 10:33:21', '1', 'admin', '2021-03-08 10:33:21');
INSERT INTO `sys_resource` VALUES (11959, 32, 'site_recycle_member', '回收站', '', '', 2, '', 0, 1, 1, '1', 'admin', '2021-03-15 08:24:43', '1', 'admin', '2021-03-15 08:24:43');
INSERT INTO `sys_resource` VALUES (11960, 5, 'cms_node_g_17', '栏目管理--注册须知(registration_agreement)', '', '', 1, '', 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_resource` VALUES (11961, 6, 'cms_content_g_17', '内容管理--注册须知(registration_agreement)', '', '', 1, '', 0, 1, 1, '0', '', '2021-03-24 03:41:21', '0', '', '2021-03-24 03:41:21');
INSERT INTO `sys_resource` VALUES (11962, 38, 'origin_member', '受益权人', '', '', 1, '', 0, 1, 1, '1', 'admin', '2021-03-29 14:26:08', '1', 'admin', '2021-03-29 14:26:08');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_sys` int(11) NULL DEFAULT NULL COMMENT '是否未系统角色 0 ->不是 1->是系统',
  `n_sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `description` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '描述, 备注',
  `delete_status` int(11) NOT NULL COMMENT '删除状态：0->未删除；1->已删除',
  `enable_status` int(11) NOT NULL COMMENT '启用状态：0->禁用；1->启用',
  `status` int(11) NOT NULL COMMENT '帐号启用状态：0->禁用；1->启用',
  `last_modifier_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者id',
  `last_modifier_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最新修改者名称',
  `last_modified_time` datetime(0) NULL DEFAULT NULL COMMENT '最新修改时间',
  `creator_id` varchar(38) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建者id',
  `creator_name` varchar(76) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者名称',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '系统管理员', 1, 0, '', 0, 1, 1, '1', 'admin', '2020-03-03 17:14:38', '1', 'admin', '2020-03-03 17:14:38');
INSERT INTO `sys_role` VALUES (4, '副管理员', 1, 0, '', 0, 1, 1, '1', '超级管理员', '2020-03-03 17:14:38', '1', '超级管理员', '2020-03-03 17:14:38');

-- ----------------------------
-- Table structure for sys_role_operate_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_operate_relation`;
CREATE TABLE `sys_role_operate_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `module_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `res_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `operate_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2071 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_operate_relation
-- ----------------------------
INSERT INTO `sys_role_operate_relation` VALUES (472, 4, 'site_node', 'cms_node_g_1', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (473, 4, 'site_node', 'cms_node_g_1', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (474, 4, 'site_node', 'cms_node_g_1', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (475, 4, 'site_node', 'cms_node_g_1', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (476, 4, 'site_node', 'cms_node_g_1', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (477, 4, 'site_content', 'cms_content_g_1', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (478, 4, 'site_content', 'cms_content_g_1', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (479, 4, 'site_content', 'cms_content_g_1', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (480, 4, 'site_content', 'cms_content_g_1', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (481, 4, 'sys_account', 'sys_account', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (482, 4, 'sys_account', 'sys_account', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (483, 4, 'sys_account', 'sys_account', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (484, 4, 'sys_account', 'sys_account', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (485, 4, 'sys_account', 'sys_account', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (486, 4, 'sys_account', 'sys_account', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (487, 4, 'sys_account', 'sys_account', 'set_role');
INSERT INTO `sys_role_operate_relation` VALUES (488, 4, 'sys_account', 'sys_account', 'viewrole');
INSERT INTO `sys_role_operate_relation` VALUES (489, 4, 'sys_account', 'sys_account_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (490, 4, 'sys_account', 'sys_account_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (491, 4, 'sys_account', 'sys_account_recycle', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (492, 4, 'site_node', 'site_node', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (493, 4, 'site_node', 'site_node', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (494, 4, 'site_node', 'site_node', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (495, 4, 'site_node', 'site_node', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (496, 4, 'site_content', 'site_content', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (497, 4, 'site_content', 'site_content', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (498, 4, 'site_content', 'site_content', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (499, 4, 'site_content', 'site_content', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (500, 4, 'site_content', 'site_content', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (501, 4, 'site_tpl', 'site_tpl', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (502, 4, 'site_tpl', 'site_tpl', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (503, 4, 'site_tpl', 'site_tpl', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (504, 4, 'site_tpl', 'site_tpl', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (505, 4, 'site_tpl', 'site_tpl', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (506, 4, 'site_tpl', 'site_tpl', 'addfile');
INSERT INTO `sys_role_operate_relation` VALUES (507, 4, 'site_tpl', 'site_res', 'use');
INSERT INTO `sys_role_operate_relation` VALUES (508, 4, 'survey', 'survey', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (509, 4, 'survey', 'survey', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (510, 4, 'survey', 'survey', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (511, 4, 'survey', 'survey', 'publish');
INSERT INTO `sys_role_operate_relation` VALUES (512, 4, 'survey', 'survey', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (513, 4, 'survey', 'survey_catalog', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (514, 4, 'survey', 'survey_catalog', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (515, 4, 'survey', 'survey_catalog', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (516, 4, 'survey', 'survey_catalog', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (517, 4, 'site_member', 'site_member', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (518, 4, 'site_member', 'site_member', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (519, 4, 'site_member', 'site_member', 'view_all');
INSERT INTO `sys_role_operate_relation` VALUES (520, 4, 'site_member', 'site_member', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (521, 4, 'sys_menus', 'sys_catalog', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (522, 4, 'sys_menus', 'sys_catalog', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (523, 4, 'sys_menus', 'sys_catalog', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (524, 4, 'sys_menus', 'sys_catalog', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (525, 4, 'sys_menus', 'sys_catalog', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (526, 4, 'sys_menus', 'sys_catalog', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (527, 4, 'sys_menus', 'sys_catalog', 'data_catalog_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (528, 4, 'sys_menus', 'sys_module', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (529, 4, 'sys_menus', 'sys_module', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (530, 4, 'sys_menus', 'sys_module', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (531, 4, 'sys_menus', 'sys_module', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (532, 4, 'sys_menus', 'sys_module', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (533, 4, 'sys_menus', 'sys_module', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (534, 4, 'sys_menus', 'sys_module', 'data_module_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (535, 4, 'sys_menus', 'sys_resource', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (536, 4, 'sys_menus', 'sys_resource', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (537, 4, 'sys_menus', 'sys_resource', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (538, 4, 'sys_menus', 'sys_resource', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (539, 4, 'sys_menus', 'sys_resource', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (540, 4, 'sys_menus', 'sys_resource', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (541, 4, 'sys_menus', 'sys_resource', 'data_resource_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (542, 4, 'sys_menus', 'sys_operate', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (543, 4, 'sys_menus', 'sys_operate', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (544, 4, 'sys_menus', 'sys_operate', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (545, 4, 'sys_menus', 'sys_operate', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (546, 4, 'sys_menus', 'sys_operate', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (547, 4, 'sys_menus', 'sys_operate', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (548, 4, 'sys_menus', 'sys_operate', 'data_module_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (549, 4, 'site_extmodel', 'site_extmodel', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (550, 4, 'site_extmodel', 'site_extmodel', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (551, 4, 'site_extmodel', 'site_extmodel', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (552, 4, 'site_extmodel', 'site_extmodel', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (553, 4, 'site_extmodel', 'site_extmodel', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (554, 4, 'site_node_review', 'site_node_review', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (555, 4, 'site_node_review', 'site_node_review', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (556, 4, 'site_node_review', 'site_node_review', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (557, 4, 'site_member_tag', 'site_member_tag', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (558, 4, 'site_member_tag', 'site_member_tag', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (559, 4, 'site_member_tag', 'site_member_tag', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (560, 4, 'site_member_tag', 'site_member_tag', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (561, 4, 'sys_roles', 'sys_roles', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (562, 4, 'sys_roles', 'sys_roles', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (563, 4, 'sys_roles', 'sys_roles', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (564, 4, 'sys_roles', 'sys_roles', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (565, 4, 'sys_roles', 'sys_roles', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (566, 4, 'sys_roles', 'sys_roles', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (567, 4, 'sys_roles', 'sys_roles', 'data_role_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (568, 4, 'site_tag', 'site_tag', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (569, 4, 'site_tag', 'site_tag', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (570, 4, 'site_tag', 'site_tag', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (571, 4, 'site_tag', 'site_tag', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (572, 4, 'site_node_recycle', 'site_node_recycle', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (573, 4, 'site_node_recycle', 'site_node_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (574, 4, 'site_node_recycle', 'site_node_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (575, 4, 'sys_setting', 'sys_setting', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (576, 4, 'sys_setting', 'sys_setting', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (577, 4, 'site_content_recycle', 'site_content_recycle', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (578, 4, 'site_content_recycle', 'site_content_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (579, 4, 'site_content_recycle', 'site_content_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (580, 4, 'node_mark', 'node_mark', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (581, 4, 'site_content_review', 'site_content_review', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (582, 4, 'site_content_review', 'site_content_review', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (583, 4, 'site_content_review', 'site_content_review', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (584, 4, 'site_doc', 'site_doc', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (585, 4, 'site_doc', 'site_doc', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (586, 4, 'site_doc', 'site_doc', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (587, 4, 'site_doc', 'site_doc', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (588, 4, 'site_doc_type', 'site_doc_type', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (589, 4, 'site_doc_type', 'site_doc_type', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (590, 4, 'site_doc_type', 'site_doc_type', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (591, 4, 'site_doc_type', 'site_doc_type', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (592, 4, 'sys_strategy', 'sys_strategy', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (593, 4, 'sys_strategy', 'sys_strategy', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (594, 4, 'sys_strategy', 'sys_strategy', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (595, 4, 'sys_strategy', 'sys_strategy', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (596, 4, 'site_other', 'site_other', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (597, 4, 'site_other', 'site_other', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (598, 4, 'site_other', 'site_other', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (599, 4, 'site_other', 'site_other', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (600, 4, 'sys_log', 'sys_log', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (601, 4, 'content_mark', 'content_mark', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1848, 1, 'site_node', 'cms_node_g_1', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1849, 1, 'site_node', 'cms_node_g_1', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1850, 1, 'site_node', 'cms_node_g_1', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1851, 1, 'site_node', 'cms_node_g_1', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1852, 1, 'site_node', 'cms_node_g_1', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1853, 1, 'site_node', 'cms_node_g_6', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1854, 1, 'site_node', 'cms_node_g_6', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1855, 1, 'site_node', 'cms_node_g_6', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1856, 1, 'site_node', 'cms_node_g_6', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1857, 1, 'site_node', 'cms_node_g_6', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1858, 1, 'site_node', 'cms_node_g_5', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1859, 1, 'site_node', 'cms_node_g_5', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1860, 1, 'site_node', 'cms_node_g_5', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1861, 1, 'site_node', 'cms_node_g_5', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1862, 1, 'site_node', 'cms_node_g_5', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1863, 1, 'site_node', 'cms_node_g_9', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1864, 1, 'site_node', 'cms_node_g_9', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1865, 1, 'site_node', 'cms_node_g_9', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1866, 1, 'site_node', 'cms_node_g_9', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1867, 1, 'site_node', 'cms_node_g_9', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1868, 1, 'site_node', 'cms_node_g_10', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1869, 1, 'site_node', 'cms_node_g_10', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1870, 1, 'site_node', 'cms_node_g_10', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1871, 1, 'site_node', 'cms_node_g_10', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1872, 1, 'site_node', 'cms_node_g_10', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1873, 1, 'site_node', 'cms_node_g_11', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1874, 1, 'site_node', 'cms_node_g_11', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1875, 1, 'site_node', 'cms_node_g_11', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1876, 1, 'site_node', 'cms_node_g_11', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1877, 1, 'site_node', 'cms_node_g_11', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1878, 1, 'site_node', 'cms_node_g_12', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1879, 1, 'site_node', 'cms_node_g_12', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1880, 1, 'site_node', 'cms_node_g_12', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1881, 1, 'site_node', 'cms_node_g_12', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1882, 1, 'site_node', 'cms_node_g_12', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1883, 1, 'site_node', 'cms_node_g_14', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1884, 1, 'site_node', 'cms_node_g_14', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1885, 1, 'site_node', 'cms_node_g_14', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1886, 1, 'site_node', 'cms_node_g_14', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1887, 1, 'site_node', 'cms_node_g_14', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1888, 1, 'site_node', 'cms_node_g_13', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1889, 1, 'site_node', 'cms_node_g_13', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1890, 1, 'site_node', 'cms_node_g_13', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1891, 1, 'site_node', 'cms_node_g_13', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1892, 1, 'site_node', 'cms_node_g_13', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1893, 1, 'site_node', 'cms_node_g_15', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1894, 1, 'site_node', 'cms_node_g_15', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1895, 1, 'site_node', 'cms_node_g_15', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1896, 1, 'site_node', 'cms_node_g_15', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1897, 1, 'site_node', 'cms_node_g_15', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1898, 1, 'site_content', 'cms_content_g_1', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1899, 1, 'site_content', 'cms_content_g_1', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1900, 1, 'site_content', 'cms_content_g_1', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1901, 1, 'site_content', 'cms_content_g_1', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1902, 1, 'site_content', 'cms_content_g_6', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1903, 1, 'site_content', 'cms_content_g_6', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1904, 1, 'site_content', 'cms_content_g_6', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1905, 1, 'site_content', 'cms_content_g_6', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1906, 1, 'site_content', 'cms_content_g_5', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1907, 1, 'site_content', 'cms_content_g_5', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1908, 1, 'site_content', 'cms_content_g_5', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1909, 1, 'site_content', 'cms_content_g_5', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1910, 1, 'site_content', 'cms_content_g_9', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1911, 1, 'site_content', 'cms_content_g_9', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1912, 1, 'site_content', 'cms_content_g_9', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1913, 1, 'site_content', 'cms_content_g_9', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1914, 1, 'site_content', 'cms_content_g_10', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1915, 1, 'site_content', 'cms_content_g_10', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1916, 1, 'site_content', 'cms_content_g_10', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1917, 1, 'site_content', 'cms_content_g_10', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1918, 1, 'site_content', 'cms_content_g_11', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1919, 1, 'site_content', 'cms_content_g_11', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1920, 1, 'site_content', 'cms_content_g_11', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1921, 1, 'site_content', 'cms_content_g_11', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1922, 1, 'site_content', 'cms_content_g_12', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1923, 1, 'site_content', 'cms_content_g_12', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1924, 1, 'site_content', 'cms_content_g_12', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1925, 1, 'site_content', 'cms_content_g_12', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1926, 1, 'site_content', 'cms_content_g_14', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1927, 1, 'site_content', 'cms_content_g_14', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1928, 1, 'site_content', 'cms_content_g_14', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1929, 1, 'site_content', 'cms_content_g_14', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1930, 1, 'site_content', 'cms_content_g_13', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1931, 1, 'site_content', 'cms_content_g_13', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1932, 1, 'site_content', 'cms_content_g_13', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1933, 1, 'site_content', 'cms_content_g_13', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1934, 1, 'site_content', 'cms_content_g_15', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1935, 1, 'site_content', 'cms_content_g_15', 'update');
INSERT INTO `sys_role_operate_relation` VALUES (1936, 1, 'site_content', 'cms_content_g_15', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1937, 1, 'site_content', 'cms_content_g_15', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1938, 1, 'sys_account', 'sys_account', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1939, 1, 'sys_account', 'sys_account', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1940, 1, 'sys_account', 'sys_account', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (1941, 1, 'sys_account', 'sys_account', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (1942, 1, 'sys_account', 'sys_account', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (1943, 1, 'sys_account', 'sys_account', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1944, 1, 'sys_account', 'sys_account', 'set_role');
INSERT INTO `sys_role_operate_relation` VALUES (1945, 1, 'sys_account', 'sys_account', 'viewrole');
INSERT INTO `sys_role_operate_relation` VALUES (1946, 1, 'sys_account', 'sys_account_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1947, 1, 'sys_account', 'sys_account_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (1948, 1, 'sys_account', 'sys_account_recycle', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (1949, 1, 'site_node', 'site_node', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1950, 1, 'site_node', 'site_node', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1951, 1, 'site_node', 'site_node', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (1952, 1, 'site_node', 'site_node', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1953, 1, 'site_content', 'site_content', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1954, 1, 'site_content', 'site_content', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1955, 1, 'site_content', 'site_content', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (1956, 1, 'site_content', 'site_content', 'sort');
INSERT INTO `sys_role_operate_relation` VALUES (1957, 1, 'site_content', 'site_content', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1958, 1, 'site_tpl', 'site_tpl', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1959, 1, 'site_tpl', 'site_tpl', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1960, 1, 'site_tpl', 'site_tpl', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (1961, 1, 'site_tpl', 'site_tpl', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (1962, 1, 'site_tpl', 'site_tpl', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1963, 1, 'site_tpl', 'site_tpl', 'addfile');
INSERT INTO `sys_role_operate_relation` VALUES (1964, 1, 'site_tpl', 'site_res', 'use');
INSERT INTO `sys_role_operate_relation` VALUES (1965, 1, 'survey', 'survey', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1966, 1, 'survey', 'survey', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1967, 1, 'survey', 'survey', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1968, 1, 'survey', 'survey', 'publish');
INSERT INTO `sys_role_operate_relation` VALUES (1969, 1, 'survey', 'survey', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1970, 1, 'survey', 'survey_catalog', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1971, 1, 'survey', 'survey_catalog', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1972, 1, 'survey', 'survey_catalog', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1973, 1, 'survey', 'survey_catalog', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1974, 1, 'site_member', 'site_member', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (1975, 1, 'site_member', 'site_member', 'data_tag_1');
INSERT INTO `sys_role_operate_relation` VALUES (1976, 1, 'site_member', 'site_member', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (1977, 1, 'site_member', 'site_member', 'view_all');
INSERT INTO `sys_role_operate_relation` VALUES (1978, 1, 'site_member', 'site_member', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (1979, 1, 'site_member', 'site_recycle_member', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (1980, 1, 'site_member', 'site_recycle_member', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (1981, 1, 'interest_distribution', 'interest_distribution', 'import');
INSERT INTO `sys_role_operate_relation` VALUES (1982, 1, 'interest_distribution', 'interest_distribution', 'export');
INSERT INTO `sys_role_operate_relation` VALUES (1983, 1, 'interest_distribution', 'interest_distribution', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1984, 1, 'sys_menus', 'sys_catalog', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1985, 1, 'sys_menus', 'sys_catalog', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1986, 1, 'sys_menus', 'sys_catalog', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (1987, 1, 'sys_menus', 'sys_catalog', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (1988, 1, 'sys_menus', 'sys_catalog', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (1989, 1, 'sys_menus', 'sys_catalog', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1990, 1, 'sys_menus', 'sys_catalog', 'data_catalog_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (1991, 1, 'sys_menus', 'sys_module', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1992, 1, 'sys_menus', 'sys_module', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (1993, 1, 'sys_menus', 'sys_module', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (1994, 1, 'sys_menus', 'sys_module', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (1995, 1, 'sys_menus', 'sys_module', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (1996, 1, 'sys_menus', 'sys_module', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (1997, 1, 'sys_menus', 'sys_module', 'data_module_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (1998, 1, 'sys_menus', 'sys_resource', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (1999, 1, 'sys_menus', 'sys_resource', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2000, 1, 'sys_menus', 'sys_resource', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (2001, 1, 'sys_menus', 'sys_resource', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (2002, 1, 'sys_menus', 'sys_resource', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (2003, 1, 'sys_menus', 'sys_resource', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2004, 1, 'sys_menus', 'sys_resource', 'data_resource_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (2005, 1, 'sys_menus', 'sys_operate', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2006, 1, 'sys_menus', 'sys_operate', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2007, 1, 'sys_menus', 'sys_operate', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (2008, 1, 'sys_menus', 'sys_operate', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (2009, 1, 'sys_menus', 'sys_operate', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (2010, 1, 'sys_menus', 'sys_operate', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2011, 1, 'sys_menus', 'sys_operate', 'data_module_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (2012, 1, 'site_extmodel', 'site_extmodel', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2013, 1, 'site_extmodel', 'site_extmodel', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2014, 1, 'site_extmodel', 'site_extmodel', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2015, 1, 'site_extmodel', 'site_extmodel', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (2016, 1, 'site_extmodel', 'site_extmodel', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2017, 1, 'site_node_review', 'site_node_review', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (2018, 1, 'site_node_review', 'site_node_review', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2019, 1, 'site_node_review', 'site_node_review', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2020, 1, 'site_member_tag', 'site_member_tag', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2021, 1, 'site_member_tag', 'site_member_tag', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2022, 1, 'site_member_tag', 'site_member_tag', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2023, 1, 'site_member_tag', 'site_member_tag', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2024, 1, 'sys_roles', 'sys_roles', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2025, 1, 'sys_roles', 'sys_roles', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2026, 1, 'sys_roles', 'sys_roles', 'list');
INSERT INTO `sys_role_operate_relation` VALUES (2027, 1, 'sys_roles', 'sys_roles', 'change_status');
INSERT INTO `sys_role_operate_relation` VALUES (2028, 1, 'sys_roles', 'sys_roles', 'modify');
INSERT INTO `sys_role_operate_relation` VALUES (2029, 1, 'sys_roles', 'sys_roles', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2030, 1, 'sys_roles', 'sys_roles', 'data_role_key_value');
INSERT INTO `sys_role_operate_relation` VALUES (2031, 1, 'site_tag', 'site_tag', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2032, 1, 'site_tag', 'site_tag', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2033, 1, 'site_tag', 'site_tag', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2034, 1, 'site_tag', 'site_tag', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2035, 1, 'site_node_recycle', 'site_node_recycle', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2036, 1, 'site_node_recycle', 'site_node_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2037, 1, 'site_node_recycle', 'site_node_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (2038, 1, 'member_agency', 'member_agency', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2039, 1, 'sys_setting', 'sys_setting', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2040, 1, 'sys_setting', 'sys_setting', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2041, 1, 'site_content_recycle', 'site_content_recycle', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2042, 1, 'site_content_recycle', 'site_content_recycle', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2043, 1, 'site_content_recycle', 'site_content_recycle', 'recovery');
INSERT INTO `sys_role_operate_relation` VALUES (2044, 1, 'node_mark', 'node_mark', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2045, 1, 'site_content_review', 'site_content_review', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2046, 1, 'site_content_review', 'site_content_review', 'review');
INSERT INTO `sys_role_operate_relation` VALUES (2047, 1, 'site_content_review', 'site_content_review', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2048, 1, 'site_doc', 'site_doc', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2049, 1, 'site_doc', 'site_doc', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2050, 1, 'site_doc', 'site_doc', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2051, 1, 'site_doc', 'site_doc', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2052, 1, 'site_doc_type', 'site_doc_type', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2053, 1, 'site_doc_type', 'site_doc_type', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2054, 1, 'site_doc_type', 'site_doc_type', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2055, 1, 'site_doc_type', 'site_doc_type', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2056, 1, 'sys_strategy', 'sys_strategy', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2057, 1, 'sys_strategy', 'sys_strategy', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2058, 1, 'sys_strategy', 'sys_strategy', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2059, 1, 'sys_strategy', 'sys_strategy', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2060, 1, 'site_other', 'site_other', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2061, 1, 'site_other', 'site_other', 'mod');
INSERT INTO `sys_role_operate_relation` VALUES (2062, 1, 'site_other', 'site_other', 'del');
INSERT INTO `sys_role_operate_relation` VALUES (2063, 1, 'site_other', 'site_other', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2064, 1, 'sys_log', 'sys_log', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2065, 1, 'content_mark', 'content_mark', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2066, 1, 'site_notice', 'site_notice', 'add');
INSERT INTO `sys_role_operate_relation` VALUES (2067, 1, 'site_notice', 'site_notice', 'view');
INSERT INTO `sys_role_operate_relation` VALUES (2068, 1, 'site_notice', 'site_notice', 'delete');
INSERT INTO `sys_role_operate_relation` VALUES (2069, 1, 'origin_member', 'origin_member', 'import');
INSERT INTO `sys_role_operate_relation` VALUES (2070, 1, 'origin_member', 'origin_member', 'mod');

-- ----------------------------
-- Table structure for sys_strategy
-- ----------------------------
DROP TABLE IF EXISTS `sys_strategy`;
CREATE TABLE `sys_strategy`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `chr_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `chr_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型:ip,time,power_pwd,mod_pwd,login_cooling',
  `begin_condition` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开始条件',
  `end_condition` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '结束条件',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_strategy
-- ----------------------------
INSERT INTO `sys_strategy` VALUES (1, 'IP限制策略', 'ip', '127.0.0.1', '127.0.0.1');
INSERT INTO `sys_strategy` VALUES (2, '登录时间策略', 'time', '08:00:00', '22:00:00');
INSERT INTO `sys_strategy` VALUES (4, '密码强度策略', 'power_pwd', '10', '');
INSERT INTO `sys_strategy` VALUES (5, '密码修改策略', 'mod_pwd', '14', '');
INSERT INTO `sys_strategy` VALUES (6, '连续登录策略', 'login_cooling', '2', '');

-- ----------------------------
-- Table structure for sys_tableid
-- ----------------------------
DROP TABLE IF EXISTS `sys_tableid`;
CREATE TABLE `sys_tableid`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `max_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_tableid
-- ----------------------------
INSERT INTO `sys_tableid` VALUES (2, 'site_node', 17);
INSERT INTO `sys_tableid` VALUES (3, 'site_content', 34);
INSERT INTO `sys_tableid` VALUES (4, 'site_other', 0);
INSERT INTO `sys_tableid` VALUES (5, 'site_history_member', 500000043);

-- ----------------------------
-- Table structure for sys_templates
-- ----------------------------
DROP TABLE IF EXISTS `sys_templates`;
CREATE TABLE `sys_templates`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tpl_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tpl_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tpl_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `tpl_thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_templates
-- ----------------------------
INSERT INTO `sys_templates` VALUES (1, '网站模板', 'tpl1', '网站模板', '/upload/tpl/20200423092720.png');

-- ----------------------------
-- Table structure for sys_user_pwd_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_pwd_log`;
CREATE TABLE `sys_user_pwd_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `new_pwd` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '新密码',
  `mod_time` datetime(0) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_pwd_log
-- ----------------------------
INSERT INTO `sys_user_pwd_log` VALUES (1, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-02-26 14:36:30');
INSERT INTO `sys_user_pwd_log` VALUES (2, 1, '828b4118e05aed6bf3355d820b0553da', '2021-02-26 14:40:18');
INSERT INTO `sys_user_pwd_log` VALUES (3, 4, '828b4118e05aed6bf3355d820b0553da', '2020-04-01 14:40:18');
INSERT INTO `sys_user_pwd_log` VALUES (4, 55, 'c0c3ccfcafce1d1cdcd3c57dafba56cc', '2020-06-16 18:36:45');
INSERT INTO `sys_user_pwd_log` VALUES (5, 3, 'b4e6a5bf11d8a549150bc8a33eb3f3c7', '2020-11-24 06:36:33');
INSERT INTO `sys_user_pwd_log` VALUES (6, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-11-24 06:41:49');
INSERT INTO `sys_user_pwd_log` VALUES (7, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-12-02 08:32:31');
INSERT INTO `sys_user_pwd_log` VALUES (8, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-12-02 08:35:14');
INSERT INTO `sys_user_pwd_log` VALUES (9, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-12-02 08:36:04');
INSERT INTO `sys_user_pwd_log` VALUES (10, 4, 'd61b6f8ecb46673318d6fd2d53d958c2', '2020-12-04 02:10:01');
INSERT INTO `sys_user_pwd_log` VALUES (11, 1, 'c11fe139a75d00ffd37831969133c9f6', '2020-12-04 06:52:42');
INSERT INTO `sys_user_pwd_log` VALUES (12, 5, '97f731eddf0c8ae3831c00eeb5249daa', '2020-12-04 07:30:58');
INSERT INTO `sys_user_pwd_log` VALUES (13, 6, '1828b5248df291914bb50aea10e61192', '2020-12-04 07:36:11');
INSERT INTO `sys_user_pwd_log` VALUES (14, 4, 'b2df806c83968411900f587551bb5a07', '2020-12-15 02:59:34');
INSERT INTO `sys_user_pwd_log` VALUES (15, 6, 'c3b7a5391a49a96b2688e60c6c7076df', '2021-03-29 06:17:29');

-- ----------------------------
-- Table structure for sys_user_role_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role_relation`;
CREATE TABLE `sys_user_role_relation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role_relation
-- ----------------------------
INSERT INTO `sys_user_role_relation` VALUES (6, 0, 53);
INSERT INTO `sys_user_role_relation` VALUES (9, 1, 55);
INSERT INTO `sys_user_role_relation` VALUES (17, 5, 3);
INSERT INTO `sys_user_role_relation` VALUES (25, 4, 2);
INSERT INTO `sys_user_role_relation` VALUES (30, 1, 1);
INSERT INTO `sys_user_role_relation` VALUES (33, 1, 5);
INSERT INTO `sys_user_role_relation` VALUES (35, 1, 4);
INSERT INTO `sys_user_role_relation` VALUES (36, 1, 6);

-- ----------------------------
-- Table structure for sys_user_strategy
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_strategy`;
CREATE TABLE `sys_user_strategy`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL,
  `strategy_id` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_strategy
-- ----------------------------
INSERT INTO `sys_user_strategy` VALUES (2, 55, 0);
INSERT INTO `sys_user_strategy` VALUES (10, 3, 0);
INSERT INTO `sys_user_strategy` VALUES (18, 2, 0);
INSERT INTO `sys_user_strategy` VALUES (23, 1, 0);

SET FOREIGN_KEY_CHECKS = 1;
