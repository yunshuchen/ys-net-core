﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Utils.Mvc.Resource;
using YS.Utils.Mvc.Route;
using Microsoft.Extensions.DependencyInjection;

using YS.Net;

namespace YS.NetCore
{
    public class Builder : PluginBase
    {
        #region Base
        public override IEnumerable<AdminMenu> AdminMenu()
        {
            return null;
        }

        public override void ConfigureServices(IServiceCollection serviceCollection)
        {

        }

        public override IEnumerable<RouteDescriptor> RegistRoute()
        {
            return null;
        }

        protected override void InitScript(Func<string, ResourceHelper> script)
        {

        }

        protected override void InitStyle(Func<string, ResourceHelper> style)
        {

        }
        #endregion

        public override void ConfigureMVC(IMvcBuilder mvcBuilder)
        {
            mvcBuilder.AddRazorRuntimeCompilation();
        }
    }
}
