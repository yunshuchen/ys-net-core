﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.Razor;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using YS.Net.Setting;
using YS.Net.Common;
using YS.Net;
using YS.Net.Services;
using System.Dynamic;
using YS.Net.Models;
using System.Linq.Expressions;
using Microsoft.Extensions.Localization;
using YS.Utils;


namespace YS.NetCore.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRazorViewEngine viewRender;
        private readonly ViewResultExecutor executor;
        private readonly ITempDataProvider _tempDataProvider;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IApplicationContextAccessor _httpContextAccessor;
        private readonly IApplicationSettingService AppSite;
        private readonly ISiteFileProvider SiteFile;
        private readonly ISiteNodeService CMSNode;
        private readonly ISiteContentService CMSContent;
        public HomeController(ILogger<HomeController> logger,
            IRazorViewEngine _viewRender,
            IActionResultExecutor<ViewResult> _executor,
            ITempDataProvider tempDataProvider,
            IWebHostEnvironment hostingEnvironment,
            IApplicationContextAccessor httpContextAccessor,
            IApplicationSettingService _AppSite,
            ISiteFileProvider _SiteFile, ISiteNodeService _CMSNode,
            ISiteContentService _CMSContent)
        {
            AppSite = _AppSite;
            _logger = logger;
            viewRender = _viewRender;
            executor = _executor as ViewResultExecutor ?? throw new ArgumentNullException("executor");
            _tempDataProvider = tempDataProvider;
            _hostingEnvironment = hostingEnvironment;
            _httpContextAccessor = httpContextAccessor;
            SiteFile = _SiteFile;
            CMSNode = _CMSNode;
            CMSContent = _CMSContent;
        }



        /// <summary>
        /// 首页
        /// path==""||path=="index"||path=="default"
        /// </summary>
        /// <returns></returns>
        public IActionResult Index([FromServices] Shyjus.BrowserDetection.IBrowserDetector browserDetector)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();
            string TplBasePath = SiteFile.TemplateRelativeBasePath;


            
            string NodeCode = RouteData.Values["NodeCode"].MyToString();
            if (NodeCode == "")
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }

            if (lang == "")
            {
                lang = "sc";
            }

            Expression<Func<site_node, bool>> predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && a.lang == lang;

            if (lang == "sc")
            {
                predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var Node = CMSNode.Get("id asc", predicate);

            if (Node == null)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["node_info"] = Node;
            base.ViewData["meta_keywords"] = Node.meta_keywords;
            base.ViewData["mate_description"] = Node.mate_description;
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = Node.node_name;

            string tpl_code = "tpl1";
            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {
                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = Node.node_tpl;
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/" + SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });

            return View();
        }

        /// <summary>
        /// 错误
        /// </summary>
        /// <returns></returns>
        public IActionResult Error([FromServices] Shyjus.BrowserDetection.IBrowserDetector browserDetector, [FromServices] IStringLocalizerFactory localizerFactory)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();
   
            //string TplBasePath = SiteFile.MultiSiteTemplateBasePath(site_id);
            string TplBasePath = SiteFile.TemplateRelativeBasePath;

            var _ = localizerFactory.Create(null);
            if (lang == "")
            {
                lang = "sc";
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["lang"] = lang;
            base.ViewData["node_info"] = new site_node();
            base.ViewData["node_name"] = _["Site.Error.Title"];

            string tpl_code = "tpl1";

            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {
                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = "error/index.cshtml";
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/"+ SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });

            //TplBasePath = Path.Combine(TplBasePath, AppSite.Get("TplCode", "tpl1")) + "/";
            //string actualViewPath = "error/index.cshtml";
            //var view = viewRender.GetView(TplBasePath, actualViewPath, true)?.View;
            //if (view != null)
            //{
            //    using (view as IDisposable)
            //    {
            //        dynamic dataShapedObject = new ExpandoObject();
            //        dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            //        dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            //        dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/" + _httpContextAccessor.Current.site_id + "/" + SiteFile.StaticContentDirectory + "/";
            //        base.ViewData["SiteFile"] = dataShapedObject;
            //        await executor.ExecuteAsync(ControllerContext, view, base.ViewData, base.TempData, "text/html; charset=utf-8", 200);
            //    }
            //    return new EmptyResult();
            //}
            return View();
        }
        /// <summary>
        /// 全站检索
        /// </summary>
        /// <returns></returns>
        public IActionResult Search([FromServices] Net.MyLucene.ISearchService searcher,
            [FromServices]Shyjus.BrowserDetection.IBrowserDetector browserDetector,
            [FromServices] IStringLocalizerFactory localizerFactory)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();
            int page = RouteData.Values["page"].MyToInt();
           
            //string TplBasePath = SiteFile.MultiSiteTemplateBasePath(site_id);
            string TplBasePath = SiteFile.TemplateRelativeBasePath;
            if (lang == "")
            {
                lang = "sc";
            }
            
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            var _ = localizerFactory.Create(null);
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = _["Site.Search"];
            base.ViewData["node_info"] = new site_node();
            string key_words = Request.Query["q"].MyToString();
            if (key_words.Trim().Length <= 0)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            base.ViewData["key_words"] = key_words;
            base.ViewData["page"] = page;

            var result = searcher.Search(lang, new Net.MyLucene.Models.SearchRequest() { iPage = page, PageSize = 10, Query = key_words });

            base.ViewData["search_data"] = result;

            string tpl_code = "tpl1";
            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {
                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = "search/index.cshtml";
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/" + SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });
            //TplBasePath = Path.Combine(TplBasePath, AppSite.Get("TplCode", "tpl1")) + "/";
            //string actualViewPath = "search/index.cshtml";
            //var view = viewRender.GetView(TplBasePath, actualViewPath, true)?.View;
            //if (view != null)
            //{
            //    using (view as IDisposable)
            //    {
            //        dynamic dataShapedObject = new ExpandoObject();
            //        dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            //        dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            //        dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/" + _httpContextAccessor.Current.site_id + "/" + SiteFile.StaticContentDirectory + "/";
            //        base.ViewData["SiteFile"] = dataShapedObject;
            //        base.ViewData["RequestMethod"] = Request.Method.ToUpper();
            //        var query = Request.Query;
            //        Dictionary<string, string> query_dic = new Dictionary<string, string>();
            //        foreach (var item in query)
            //        {
            //            query_dic.TryAdd(item.Key, item.Value);
            //        }
            //        base.ViewData["RequestQuery"] = query_dic;
            //        if (Request.Method.ToUpper() == "POST")
            //        {

            //        }
            //        await executor.ExecuteAsync(ControllerContext, view, base.ViewData, base.TempData, "text/html; charset=utf-8", 200);
            //    }
            //    return new EmptyResult();
            //}
            return View();
        }


        /// <summary>
        /// 具体代号
        /// /栏目代号
        /// </summary>
        /// <returns></returns>
        public IActionResult Node([FromServices] Shyjus.BrowserDetection.IBrowserDetector browserDetector)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();

            //string TplBasePath = SiteFile.MultiSiteTemplateBasePath(site_id);
            string TplBasePath = SiteFile.TemplateRelativeBasePath;
            string NodeCode = RouteData.Values["NodeCode"].MyToString();
            if (NodeCode == "")
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }

            }
            if (lang == "")
            {
                lang = "sc";
            }
            Expression<Func<site_node, bool>> predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && a.lang == lang;
            if (lang == "sc")
            {
                predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var Node = CMSNode.Get("id asc", predicate);
            if (Node == null)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["node_info"] = Node;
            base.ViewData["meta_keywords"] = Node.meta_keywords;
            base.ViewData["mate_description"] = Node.mate_description;
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = Node.node_name;
            if (RouteData.Values["Params"].MyToInt() <= 0)
            {
                base.ViewData["page"] = 1;
            }
            else
            {
                base.ViewData["page"] = RouteData.Values["Params"].MyToInt();
            }
            string tpl_code = "tpl1";
            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {
                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = Node.node_tpl;
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/"  + SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });


           

            return View();
        }

        /// <summary>
        /// 列表页
        /// 栏目代号/list
        /// 栏目代号/list/page
        /// </summary>
        /// <returns></returns>
        public IActionResult List([FromServices] Shyjus.BrowserDetection.IBrowserDetector browserDetector)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();
  
            //string TplBasePath = SiteFile.MultiSiteTemplateBasePath(site_id);
            string TplBasePath = SiteFile.TemplateRelativeBasePath;
            string NodeCode = RouteData.Values["NodeCode"].MyToString();
            if (NodeCode == "")
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            if (lang == "")
            {
                lang = "sc";
            }
            Expression<Func<site_node, bool>> predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && a.lang == lang;
            if (lang == "sc")
            {
                predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var Node = CMSNode.Get("id asc", predicate);
            if (Node == null)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["node_info"] = Node;
            base.ViewData["meta_keywords"] = Node.meta_keywords;
            base.ViewData["mate_description"] = Node.mate_description;
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = Node.node_name;
            if (RouteData.Values["Params"].MyToInt() <= 0)
            {
                base.ViewData["page"] = 1;
            }
            else
            {
                base.ViewData["page"] = RouteData.Values["Params"].MyToInt();
            }

            string tpl_code = "tpl1";
            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {
                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = Node.list_tpl;
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/"  + SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });
           
            return View();
        }
        public JsonResult JsonDetail()
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();

            string TplBasePath = SiteFile.TemplateBasePath;
            string NodeCode = RouteData.Values["NodeCode"].MyToString();
            if (NodeCode == "")
            {
                return Json(new { code = -1, msg = "404" });
            }
            if (lang == "")
            {
                lang = "sc";
            }

            Expression<Func<site_node, bool>> predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && a.lang == lang;

            if (lang == "sc")
            {
                predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var Node = CMSNode.Get("id asc", predicate);

            if (Node == null)
            {
                return Json(new { code = -1, msg = "404" });
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["node_info"] = Node;
            base.ViewData["meta_keywords"] = Node.meta_keywords;
            base.ViewData["mate_description"] = Node.mate_description;
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = Node.node_name;
            Expression<Func<site_content, bool>> predicate1 = a => a.node_id == Node.id && a.lang == lang;

            if (lang == "sc")
            {
                predicate1 = a => a.node_id == Node.id && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var RowData = CMSContent.Get("n_sort asc", predicate1);

            return Json(new { code = 1, msg = "", data = RowData });
        }

        /// <summary>
        /// 列表页
        /// 栏目代号/detail/DataId
        /// </summary>
        /// <returns></returns>
        public IActionResult Detail([FromServices] Shyjus.BrowserDetection.IBrowserDetector browserDetector)
        {
            string lang = RouteData.Values["lang"].MyToString().ToLower();

            //string TplBasePath = SiteFile.MultiSiteTemplateBasePath(site_id);
            string TplBasePath = SiteFile.TemplateRelativeBasePath;
            string NodeCode = RouteData.Values["NodeCode"].MyToString();
            if (NodeCode == "")
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            if (lang == "")
            {
                lang = "sc";
            }

            Expression<Func<site_node, bool>> predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && a.lang == lang;

            if (lang == "sc")
            {
                predicate = a => a.node_code.ToLower() == NodeCode.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            var Node = CMSNode.Get("id asc", predicate);

            if (Node == null)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            base.ViewData["req_path"] = Request.Path.Value.MyToString();
            base.ViewData["node_info"] = Node;
            base.ViewData["meta_keywords"] = Node.meta_keywords;
            base.ViewData["mate_description"] = Node.mate_description;
            base.ViewData["lang"] = lang;
            base.ViewData["node_name"] = Node.node_name;


            long DataId = RouteData.Values["Params"].MyToLong();
            base.ViewData["data_id"] = DataId;
            Expression<Func<site_content, bool>> predicate1 = (a) => a.node_id == Node.id && a.lang == lang;
            if (lang == "sc")
            {
                predicate1 = a => a.node_id == Node.id && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
                if (DataId > 0)
                {
                    predicate1 = (a) => a.node_id == Node.id && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) && a.id == DataId;
                }
            }
            else
            {
                predicate1 = a => a.node_id == Node.id && a.lang == lang;
            }
            if (DataId > 0)
            {
                predicate1 = (a) => a.node_id == Node.id && a.lang == lang && a.id == DataId;

            }
            var RowData = CMSContent.Get("n_sort asc", predicate1);

            if (RowData == null || RowData.id <= 0)
            {
                if (_httpContextAccessor.Current.MultiLanguage)
                {
                    return Redirect($"/{lang}/error");
                }
                else
                {
                    return Redirect($"/error");
                }
            }
            CMSContent.Modify(a => a.id == RowData.id, a => new site_content()
            {
                view_count = a.view_count + 1
            });



            base.ViewData["row_data"] = RowData;

            string tpl_code = "tpl1";
            tpl_code = AppSite.Get("TplCode", "tpl1");
            if (_httpContextAccessor.Current.MobileDiffTpl)
            {

                if (browserDetector.Browser.DeviceType == "Mobile" || browserDetector.Browser.DeviceType == "Tablet")
                {
                    tpl_code = AppSite.Get("MobileTplCode", "tpl1");
                }
            }
            string actualViewPath = Node.content_tpl;
            TplBasePath = $"{TplBasePath}{tpl_code}/{actualViewPath}";
            dynamic dataShapedObject = new ExpandoObject();
            dataShapedObject.StaticDirectory = SiteFile.StaticDirectory;
            dataShapedObject.StaticContentDirectory = SiteFile.StaticContentDirectory;
            dataShapedObject.StaticUploadsDirectory = "/" + SiteFile.StaticDirectory + "/" + SiteFile.StaticContentDirectory + "/";
            base.ViewData["SiteFile"] = dataShapedObject;
            return View(Utils.Mvc.ViewComponentExtensions.GetViewPath(this.HttpContext, TplBasePath), new { });

            
            return View();
        }




    }
}
