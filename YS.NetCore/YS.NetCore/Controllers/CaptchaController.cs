﻿using System;
using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CaptchaController : Net.Mvc.BasicController<sys_account, ISysAccountService>
    {
        IHttpContextAccessor _httpContextAccessor;

        ISysAccountService sysaccount;

        public CaptchaController(IHttpContextAccessor httpContextAccessor
            , IApplicationContextAccessor _applicationContextAccessor,
            ISysAccountService _sysaccount
            ) : base(_sysaccount, _applicationContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            sysaccount = _sysaccount;
        }
        [Route("/{lang}/captcha")]
        [AllowAnonymous]
        public ActionResult GetValidateCode()
        {
            _httpContextAccessor.HttpContext.Session.SetString("email_fail", "true");
            _httpContextAccessor.HttpContext.Session.SetString("alert_danger", "");
            _httpContextAccessor.HttpContext.Session.SetString("alert_success", "");
            Response.Clear();

            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            var randValue = rand.Next(0, 10000);
            Bitmap image;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            string code = "";
            string code_resilt = "";
            image = Net.Common.ValidateImg.CreateImageCode(code);
            var rdm = randValue %2;
            if (rdm == 0)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateMyImageCode(code);
            }
            else
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            image.Save(ms, ImageFormat.Png);
            _httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.SiteUserPostSafeCodeKey, code_resilt.ToLower());
            return File(ms.ToArray(), @"image/png");
        }
    }
}