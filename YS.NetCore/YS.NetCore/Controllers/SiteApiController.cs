﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class SiteApiController : Controller
    {

        [Route("/{lang}/site/api")]
        public JsonResult Index(string lang, IFormCollection data)
        {
            
            
            string action = data["action"].MyToString();
            return Json(new { code = -1, msg = "404" });
        }
    }
}