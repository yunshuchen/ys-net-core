﻿using YS.Utils;
using Microsoft.OpenApi.Models;
using IDocumentFilter = Swashbuckle.AspNetCore.SwaggerGen.IDocumentFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.NetCore
{
    public class ControllerTagDescriptions : IDocumentFilter
    {


        public void Apply(OpenApiDocument swaggerDoc, Swashbuckle.AspNetCore.SwaggerGen.DocumentFilterContext context)
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly(); // 获取当前程序集 
            var classes = assembly.GetTypes()
                .Where(item => Attribute.IsDefined(item, typeof(CommentControllerAttribute)));

            foreach (var t in classes)
            {
                CommentControllerAttribute attribute = (CommentControllerAttribute)t.GetCustomAttributes(typeof(CommentControllerAttribute), false).FirstOrDefault();
                swaggerDoc.Tags.Add(new OpenApiTag()
                {
                    Name = t.Name.Replace("Controller",""),
                    Description = attribute.Comment,
                });

            }
        }
    }
}
