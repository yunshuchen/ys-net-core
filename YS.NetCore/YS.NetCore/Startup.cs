using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using YS.Net;
using YS.Net.CustomMessageHandler;
using YS.Net.JsonLocalizer;
using YS.Net.Options;
using YS.Net.Quartz;
using YS.Net.WxOpenMessageHandler;
using YS.Utils;
using YS.Utils.Mvc.Plugin;
using YS.Utils.Mvc.Resource;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Senparc.CO2NET;
using Senparc.CO2NET.AspNet;
using Senparc.CO2NET.Utilities;
using Senparc.NeuChar.MessageHandlers;
using Senparc.Weixin;
using Senparc.Weixin.Cache.CsRedis;
using Senparc.Weixin.Entities;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.MessageHandlers.Middleware;
using Senparc.Weixin.Open;
using Senparc.Weixin.Open.ComponentAPIs;
using Senparc.Weixin.RegisterServices;
using Senparc.Weixin.WxOpen;
using Senparc.Weixin.WxOpen.MessageHandlers.Middleware;
using YS.Utils.DI;
using UEditorNetCore;
using Microsoft.EntityFrameworkCore;
using YS.Utils.Repositorys;
using YS.Net.Securitys;
using NLog.Extensions.Logging;
using YS.Net.Mvc.Middlewares;
//using UEditorNetCore;




namespace YS.NetCore
{
    public class Startup
    {
        // public static IContainer AutofacContainer;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        // private DatabaseOption databaseOption;
        public void ConfigureServices(IServiceCollection services)
        {

            #region 加载非appsetings.json的配置文件
            IConfiguration _configuration;
            var builder = new ConfigurationBuilder();
            // 方式1
            _configuration = builder
                .AddJsonFile("configs/upload_config.json", false, true)
                .AddJsonFile("configs/cors_origins.json", false, true)
                .AddJsonFile("configs/json_lang_config.json", false, true)
                .AddJsonFile("configs/survey_setting_config.json", false, true)
                .AddJsonFile("configs/website_config.json", false, true)
                .AddJsonFile("configs/database.json", false, true)
                .AddJsonFile("configs/cache_providor.json", false, true)
                .AddJsonFile("configs/weixin_mp_config.json", false, true)
                //.AddJsonFile("configs/database_master_slave.json", false, true)
                .Build();

            #region 注入config
            //json 多语种相关配置
            services.Configure<JsonLocalizerOptions>(_configuration.GetSection(nameof(JsonLocalizerOptions)));
            services.Configure<AllowFileOptions>(_configuration.GetSection(nameof(AllowFileOptions)));
            services.Configure<SurveySettingOption>(_configuration.GetSection(nameof(SurveySettingOption)));
            services.Configure<Utils.Options.DatabaseOption>(_configuration.GetSection("Database"));
            services.Configure<Utils.Options.SiteSetting>(_configuration.GetSection("WebSite"));
            services.Configure<CacheOptions>(_configuration.GetSection("CacheProvider"));

            services.Configure<SenparcSetting>(_configuration.GetSection(nameof(SenparcSetting)));
            services.Configure<SenparcWeixinSetting>(_configuration.GetSection(nameof(SenparcWeixinSetting)));
            #endregion
            #endregion
            //启用 GB2312（按需）
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            services.ConfigureResource<DefaultResourceManager>();
            services.TryAddEnumerable(ServiceDescriptor.Singleton<IConfigureOptions<MvcRazorRuntimeCompilationOptions>, CompilationOptionsSetup>());
            string Deploy = Configuration.GetSection("Deploy").Get<string>();

            //单一数据库
            services.UseDataBase(_configuration.GetSection("Database").Get<Utils.Options.DatabaseOption>());

            if (Deploy.ToLower() == "linux")
            {
                //如果部署在linux系统上，需要加上下面的配置：
                services.Configure<KestrelServerOptions>(options => options.AllowSynchronousIO = true);
            }
            else
            {
                //如果部署在IIS上，需要加上下面的配置：
                services.Configure<IISServerOptions>(options => options.AllowSynchronousIO = true);
            }

            Type mvcBuilderType = typeof(Builder);
            //插件加载
            YS.Utils.Mvc.Plugin.PluginActivtor.LoadedPlugins.Add(new PluginDescriptor
            {
                Assembly = mvcBuilderType.Assembly,
                PluginType = mvcBuilderType
            });
            services.AddUEditorService();
            services.UseJF(Configuration);



            #region JWT认证
            services.AddSingleton<IAuthorizationHandler, Net.Mvc.Authorize.PermissionCheckPolicyHandler>();
            services.AddSingleton<IAuthorizationHandler, Net.Mvc.Authorize.MemberCheckPolicyHandler>();


            var JWTSecret = Configuration.GetSection("JWTSecret").Get<string>();
            var key = System.Text.Encoding.ASCII.GetBytes(JWTSecret);
            services.AddAuthorization(options =>
            {
                // 1、后台权限验证机制
                options.AddPolicy("Permission",
                policy => policy.Requirements.Add(new Net.Mvc.Authorize.PermissionCheckPolicyRequirement()));

                // 1、会员验证机制
                options.AddPolicy("SiteMember",
                policy => policy.Requirements.Add(new Net.Mvc.Authorize.MemberCheckPolicyRequirement()));
            }).AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })


           .AddJwtBearer(x =>
           {
               x.RequireHttpsMetadata = false;
               x.SaveToken = true;
               x.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(key),
                   ValidateIssuer = false,
                   ValidateAudience = false
               };

               x.Events = new JwtBearerEvents
               {
                   //此处为权限验证失败后触发的事件
                   OnChallenge = context =>
                   {

                       //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
                       context.HandleResponse();

                       //自定义自己想要返回的数据结果，我这里要返回的是Json对象，通过引用Newtonsoft.Json库进行转换
                       var payload = JsonHelper.ToJson(new { code = -403, msg = "认证不通过/Status401Unauthorized" });
                       //自定义返回的数据类型
                       context.Response.ContentType = "application/json";
                       //自定义返回状态码，默认为401 我这里改成 200
                       context.Response.StatusCode = StatusCodes.Status200OK;
                       //context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                       //输出Json数据结果
                       context.Response.WriteAsync(payload);
                       return Task.FromResult(0);
                   },
                   OnAuthenticationFailed = context =>
                   {
                       //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
                       //context.HandleResponse();

                       //自定义自己想要返回的数据结果，我这里要返回的是Json对象，通过引用Newtonsoft.Json库进行转换
                       var payload = JsonHelper.ToJson(new { code = -403, msg = "认证不通过/Status401Unauthorized" });
                       //自定义返回的数据类型
                       context.Response.ContentType = "application/json";
                       //自定义返回状态码，默认为401 我这里改成 200
                       context.Response.StatusCode = StatusCodes.Status200OK;
                       //context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                       //输出Json数据结果
                       context.Response.WriteAsync(payload);
                       return Task.FromResult(0);
                   },
                   OnForbidden = context =>
                   {
                       //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
                       //context.HandleResponse();

                       //自定义自己想要返回的数据结果，我这里要返回的是Json对象，通过引用Newtonsoft.Json库进行转换
                       var payload = JsonHelper.ToJson(new { code = -403, msg = "认证不通过/Status401Unauthorized" });
                       //自定义返回的数据类型
                       context.Response.ContentType = "application/json";
                       //自定义返回状态码，默认为401 我这里改成 200
                       context.Response.StatusCode = StatusCodes.Status200OK;
                       //context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                       //输出Json数据结果
                       context.Response.WriteAsync(payload);
                       return Task.FromResult(0);
                   }
               };
           });
            #endregion


            #region Swagger
            services.AddApiVersioning(option =>
            {

                // 可选，为true时API返回支持的版本信息
                option.ReportApiVersions = true;
                // 不提供版本时，默认为1.0
                option.AssumeDefaultVersionWhenUnspecified = true;
                // 请求中未指定版本时默认为1.0
                option.DefaultApiVersion = new ApiVersion(1, 0);
            })
          .AddVersionedApiExplorer(option =>
          {
              // 版本名的格式：v+版本号
              option.GroupNameFormat = "'v'V";
              option.AssumeDefaultVersionWhenUnspecified = true;
          });

            services.AddSwaggerGen(c =>
            {
              //  c.DocumentFilter<ControllerTagDescriptions>();
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                foreach (var item in provider.ApiVersionDescriptions)
                {

                    c.SwaggerDoc(item.GroupName, new Microsoft.OpenApi.Models.OpenApiInfo
                    {

                        Title = "YS.Core API",
                        Version = item.GroupName,
                        Description = "YS.Core API with NET 5"
                    });

                }
                c.IgnoreObsoleteProperties();
                c.AddSecurityDefinition("Bearer",
                       new OpenApiSecurityScheme
                       {
                           Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer {token}",
                           Name = "Authorization",
                           In = ParameterLocation.Header,
                           Type = SecuritySchemeType.ApiKey,
                           BearerFormat = "JWT",
                           Scheme = "Bearer"
                       });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });

                // 为 Swagger JSON and UI设置xml文档注释路径
                var basePath = Path.GetDirectoryName(typeof(Program).Assembly.Location);//获取应用程序所在目录（绝对，不受工作目录影响，建议采用此方法获取路径）
                var xmlPath = Path.Combine(basePath, "YS.NetCore.xml");
                c.IncludeXmlComments(xmlPath);


            });
            #endregion



            services.RegisterGzip();


            #region 跨域设置

            List<string> Origins = _configuration.GetSection("cors_origins").Get<List<string>>();

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .AllowAnyMethod()
                    .WithOrigins(Origins.ToArray());
                });
            });
            #endregion

            services.AddResponseCaching();



            services.AddSingleton<IStringLocalizerFactory, JsonStringLocalizerFactory>();


            #region 缓存
            CacheOptions cacheOptions = _configuration.GetSection("CacheProvider").Get<CacheOptions>();
            if (cacheOptions != null && cacheOptions.CacheType.ToLower() == "redis" && cacheOptions.CacheConnectionString != "")
            {
                var csredis = new CSRedis.CSRedisClient(cacheOptions.CacheConnectionString);
                RedisHelper.Initialization(csredis);
                services.AddSingleton<IDistributedCache>(new Microsoft.Extensions.Caching.Redis.CSRedisCache(RedisHelper.Instance));
                //var keys = RedisHelper.Keys("member:report:user_id:*");
                //RedisHelper.Del(keys);

            }
            else
            {

                services.AddDistributedMemoryCache(a => a.SizeLimit = 50000);
                //services.AddSingleton(typeof(IDistributedCache), typeof(MemoryCache));
            }

            #endregion

            services.AddSenparcWeixinServices(Configuration);//Senparc.Weixin 注册（必须）
            //主从数据
            //services.AddEntityFrameworkProxies();
        }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        private readonly Action<IEndpointRouteBuilder> GetRoutes =
            endpoints =>
            {

#if blazor
                //blazor
                //endpoints.MapControllerRoute(name: "default",pattern: "{controller=Home}/{action=Index}/{id?}");
#else
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
#endif

                //endpoints.MapDynamicControllerRoute<Net.Mvc.Routing.HomeRouteTCTransformer>("tc/{*path}");
                //endpoints.MapDynamicControllerRoute<Net.Mvc.Routing.HomeRouteSCTransformer>("sc/{*path}");
                //endpoints.MapDynamicControllerRoute<Net.Mvc.Routing.HomeRouteENTransformer>("en/{*path}");
                //endpoints.MapDynamicControllerRoute<Net.Mvc.Routing.HomeRouteTransformer>("{*path}");


                endpoints.MapAreaControllerRoute(
                  name: "areas", "areas",
                  pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
#if blazor
                //blazor
                //endpoints.MapBlazorHub();
                endpoints.MapFallbackToFile("index.html");
#endif
                endpoints.MapRazorPages();
            };

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="provider"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="senparcSetting"></param>
        /// <param name="senparcWeixinSetting"></param>
        public void Configure(IApplicationBuilder app
            , IWebHostEnvironment env
            , ILoggerFactory loggerFactory
            , IApiVersionDescriptionProvider provider
            , IHttpContextAccessor httpContextAccessor
            , IOptions<SenparcSetting> senparcSetting
            , IOptions<SenparcWeixinSetting> senparcWeixinSetting
            //, IHostApplicationLifetime hostlifetime
            //, IScheduler scheduler
            )
        {

            app.UseRequestResponseLogging();
            app.UseDeveloperExceptionPage();
#if blazor
            //blazor
            app.UseWebAssemblyDebugging();
#endif
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Error");
            //    app.UseStatusCodePagesWithReExecute("/Error");
            //    app.UseHsts();
            //}
            var options = new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(
                    new CultureInfo("zh-CN")),
                SupportedCultures = new CultureInfo[]{
                    new CultureInfo("zh-hk"),
                    new CultureInfo("en-US"),
                    new CultureInfo("zh-CN")
                },
                SupportedUICultures = new CultureInfo[]{
                    new CultureInfo("zh-HK"),
                    new CultureInfo("en-US"),
                    new CultureInfo("zh-CN")
                }
            };


            options.RequestCultureProviders.Insert(0, new RouteValueRequestCultureProvider() { Options = options });
            app.UseRequestLocalization(options);

            var CookieSecure = Configuration.GetSection("CookieSecure").Get<bool>();
            var cookie_opt = new CookiePolicyOptions
            {
                MinimumSameSitePolicy = SameSiteMode.None,
                HttpOnly = HttpOnlyPolicy.Always,
                Secure = CookieSecurePolicy.SameAsRequest
            };
            if (CookieSecure)
            {
                cookie_opt.Secure = CookieSecurePolicy.Always;
            }
            app.UseCookiePolicy(cookie_opt);
            //启用Session管道
            app.UseSession();
            //app.Use(async (context, next) => {
            //    const int durationInSeconds = 60 * 60 * 24;
            //    context.Response.GetTypedHeaders().CacheControl = new Microsoft.Net.Http.Headers.CacheControlHeaderValue() { Public = true, MaxAge = TimeSpan.FromSeconds(durationInSeconds) };
            //    context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] = new string[] { "Accept-Encoding" };
            //    await next();
            //});
            app.UseSecurityHeadersMiddleware(new SecurityHeadersBuilder().AddDefaultSecurePolicy());
            // app.UseHttpsRedirection();
#if blazor
            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                DefaultContentType = "application/octet-stream",
                //设置不限制content-type
                ServeUnknownFileTypes = true,
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, @"Resources")),
                RequestPath = new PathString("/Resources"),
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                },
                HttpsCompression = Microsoft.AspNetCore.Http.Features.HttpsCompressionMode.Compress
            });
#endif
            app.UseStaticFiles(new StaticFileOptions
            {
                DefaultContentType = "application/octet-stream",
                //设置不限制content-type
                ServeUnknownFileTypes = true,
                FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath, @"upload")),
                RequestPath = new PathString("/upload"),
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24;
                    ctx.Context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.CacheControl] = "public,max-age=" + durationInSeconds;
                },
                HttpsCompression = Microsoft.AspNetCore.Http.Features.HttpsCompressionMode.Compress
            });
            app.UseRouting();
            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseResponseCompression();
            app.UseEndpoints(GetRoutes);
            app.UseResponseCaching();


            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                foreach (var item in provider.ApiVersionDescriptions)
                {
                    c.SwaggerEndpoint($"/swagger/{item.GroupName}/swagger.json", "YS.Core API V" + item.ApiVersion);
                }

            });

            loggerFactory.UseFileLog(env, app.ApplicationServices.GetService<IHttpContextAccessor>());
            /*使用NLog*/
            loggerFactory.AddNLog();
            app.UseJF(env, httpContextAccessor);
            #region 微信相关注册
                        // 启动 CO2NET 全局注册
                        var registerService = app.UseSenparcGlobal(env, senparcSetting.Value, globalRegister =>
                        {
            #region CO2NET 全局配置

            #region 全局缓存配置

                                        globalRegister.ChangeDefaultCacheNamespace("DefaultCO2NETCache");

            #region 配置和使用 Redis
                                        if (UseRedis(senparcSetting.Value, out string redisConfigurationStr))//这里为了方便不同环境的开发者进行配置，做成了判断的方式，实际开发环境一般是确定的，这里的if条件可以忽略
                                        {
                                Senparc.CO2NET.Cache.CsRedis.Register.SetConfigurationOption(redisConfigurationStr);
                                Senparc.CO2NET.Cache.CsRedis.Register.UseKeyValueRedisNow();
                            }
            #endregion

            #endregion

            #region 注册日志（按需，建议）
                                        globalRegister.RegisterTraceLog(ConfigTraceLog);//配置TraceLog
            #endregion

            #region APM 系统运行状态统计记录配置
                                        ////测试APM缓存过期时间（默认情况下可以不用设置）
                                        //Senparc.CO2NET.APM.Config.EnableAPM = true;//默认已经为开启，如果需要关闭，则设置为 false
                                        //Senparc.CO2NET.APM.Config.DataExpire = TimeSpan.FromMinutes(60);
            #endregion

            #endregion
                                    }, true)
                            //使用 Senparc.Weixin SDK
                            .UseSenparcWeixin(senparcWeixinSetting.Value, weixinRegister =>
                            {
            #region 微信相关配置

            #region 微信缓存

                                            //微信的 Redis 缓存，如果不使用则注释掉
                                            if (UseRedis(senparcSetting.Value, out _))
                                {
                                    weixinRegister.UseSenparcWeixinCacheCsRedis();
                                }

            #endregion
            #region 注册公众号或小程序（按需）

                                            weixinRegister
                                        //注册公众号（可注册多个）
                                        .RegisterMpAccount(senparcWeixinSetting.Value, "公众号")


                                        //注册多个公众号或小程序（可注册多个）
                                        .RegisterWxOpenAccount(senparcWeixinSetting.Value, "小程序")
                                        //除此以外，仍然可以在程序任意地方注册公众号或小程序：
                                        //AccessTokenContainer.Register(appId, appSecret, name);//命名空间：Senparc.Weixin.MP.Containers
            #endregion
            #region 注册微信第三方平台（按需）

                                        //注册第三方平台（可注册多个）
                                        .RegisterOpenComponent(senparcWeixinSetting.Value,
                                            //getComponentVerifyTicketFunc
                                            async componentAppId =>
                                            {
                                                var dir = Path.Combine(ServerUtility.ContentRootMapPath("~/App_Data/OpenTicket"));
                                                if (!Directory.Exists(dir))
                                                {
                                                    Directory.CreateDirectory(dir);
                                                }

                                                var file = Path.Combine(dir, string.Format("{0}.txt", componentAppId));
                                                using (var fs = new FileStream(file, FileMode.Open))
                                                {
                                                    using (var sr = new StreamReader(fs))
                                                    {
                                                        var ticket = await sr.ReadToEndAsync();
                                                        sr.Close();
                                                        return ticket;
                                                    }
                                                }
                                            },

                                            //getAuthorizerRefreshTokenFunc
                                            async (componentAppId, auhtorizerId) =>
                                            {
                                                var dir = Path.Combine(ServerUtility.ContentRootMapPath("~/App_Data/AuthorizerInfo/" + componentAppId));
                                                if (!Directory.Exists(dir))
                                                {
                                                    Directory.CreateDirectory(dir);
                                                }

                                                var file = Path.Combine(dir, string.Format("{0}.bin", auhtorizerId));
                                                if (!File.Exists(file))
                                                {
                                                    return null;
                                                }

                                                using (Stream fs = new FileStream(file, FileMode.Open))
                                                {
                                                    var binFormat = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                                                    var result = (RefreshAuthorizerTokenResult)binFormat.Deserialize(fs);
                                                    fs.Close();
                                                    return result.authorizer_refresh_token;
                                                }
                                            },

                                            //authorizerTokenRefreshedFunc
                                            (componentAppId, auhtorizerId, refreshResult) =>
                                            {
                                                var dir = Path.Combine(ServerUtility.ContentRootMapPath("~/App_Data/AuthorizerInfo/" + componentAppId));
                                                if (!Directory.Exists(dir))
                                                {
                                                    Directory.CreateDirectory(dir);
                                                }

                                                var file = Path.Combine(dir, string.Format("{0}.bin", auhtorizerId));
                                                using (Stream fs = new FileStream(file, FileMode.Create))
                                                {
                                                                //这里存了整个对象，实际上只存RefreshToken也可以，有了RefreshToken就能刷新到最新的AccessToken
                                                                var binFormat = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                                                    binFormat.Serialize(fs, refreshResult);
                                                    fs.Flush();
                                                    fs.Close();
                                                }
                                            }, "开放平台")
            #endregion
                        ;
            #endregion
                                        });
            #region 使用 MessageHadler 中间件，用于取代创建独立的 Controller
                        //MessageHandler 中间件介绍：https://www.cnblogs.com/szw/p/Wechat-MessageHandler-Middleware.html
                        app.UseMessageHandlerForMp("/weixin/WeixinAsync", CustomMessageHandler.GenerateMessageHandler, options =>
                        {
            #region 配置 SenparcWeixinSetting 参数，以自动提供 Token、EncodingAESKey 等参数
                                        options.AccountSettingFunc = context =>
                                senparcWeixinSetting.Value;
            #endregion
                                        options.DefaultMessageHandlerAsyncEvent = DefaultMessageHandlerAsyncEvent.SelfSynicMethod;
                            options.AggregateExceptionCatch = ex =>
                            {
                                return false;
                            };
                        });
                        app.UseMessageHandlerForWxOpen("/weixin/WxOpenAsync", CustomWxOpenMessageHandler.GenerateMessageHandler, options =>
                        {
                            options.DefaultMessageHandlerAsyncEvent = DefaultMessageHandlerAsyncEvent.SelfSynicMethod;
                            options.AccountSettingFunc = context => senparcWeixinSetting.Value;
                        });


            #endregion

            #endregion


            #region 定时任务
                        //hostlifetime.ApplicationStopping.Register(() =>
                        //{


                        //});
                        //hostlifetime.ApplicationStarted.Register(() =>
                        //{


                        //    scheduler.Start();
                        //}, true);
            #endregion

        }
        /// <summary>
        /// Autofac 自动注入
        /// </summary>
        /// <param name="builder"></param>
        public void ConfigureContainer(ContainerBuilder builder)
        {
            //数据库
            builder.Register(context => new NopObjectContext(context.Resolve<DbContextOptions<NopObjectContext>>()))
                .As<IDbContext>().InstancePerLifetimeScope();

            //主从
            //builder.Register(context => new MSNopObjectContext(WriteAndRead.Write))
            //    .As<IDbContext>().InstancePerLifetimeScope();
            //IOC
            builder.RegisterGeneric(typeof(EfRepository<>)).As(typeof(IRepository<>)).InstancePerLifetimeScope();

            // builder.RegisterType(typeof(Net.Quartz.Jobs.CheckSurveyJob)).SingleInstance();
            var _assably = AppDomain.CurrentDomain.GetAssemblies();
            var _needImpl = _assably.Where(c => c.FullName.StartsWith("YS.")).ToArray();

            builder.RegisterAssemblyTypes(_needImpl)
                         .Where(t => t.GetCustomAttribute<ScopedDIAttribute>() != null)
                         .AsImplementedInterfaces()
                        .InstancePerLifetimeScope();
            builder.RegisterAssemblyTypes(_needImpl)
                       .Where(t => t.GetCustomAttribute<TransientDIAttribute>() != null)
                       .AsImplementedInterfaces()
                      .InstancePerDependency();

            builder.RegisterAssemblyTypes(_needImpl)
                        .Where(t => t.GetCustomAttribute<SingletonDIAttribute>() != null)
                        .AsImplementedInterfaces()
                       .SingleInstance();

            // var list = _needImpl[1].GetTypes().Where(item => Attribute.IsDefined(item, typeof(SingletonDIAttribute)));


#region 注入没有接口的类


            var list = _needImpl.Select(a => a.GetTypes().Where(item => Attribute.IsDefined(item, typeof(ClassTypeDIAttribute)))).ToList();

            foreach (var item in list)
            {
                foreach (var job in item)
                {
                    if (Attribute.IsDefined(job, typeof(SingletonTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .SingleInstance();
                    }
                    else if (Attribute.IsDefined(job, typeof(TransientTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .InstancePerDependency();
                    }
                    else if (Attribute.IsDefined(job, typeof(ScopedTypeDIAttribute)))
                    {
                        builder.RegisterType(job)
                                  .InstancePerLifetimeScope();
                    }
                }
            }

            builder.RegisterBuildCallback(lifetimeScope =>
            {
                var _container = (IContainer)lifetimeScope;
                //var site_manager = _container.Resolve<Net.Services.IPlatformSiteManagerService>();
                ContainerManager.SetContainer(_container);
                Console.WriteLine("全部注入成功");
                // ServiceProviderInstance.Container = lifetimeScope as IContainer;
            });
#endregion


#region 方法1   Load 适用于无接口注入
            //var assemblysServices = Assembly.Load("Exercise.Services");

            //containerBuilder.RegisterAssemblyTypes(assemblysServices)
            //          .AsImplementedInterfaces()
            //          .InstancePerLifetimeScope();

            //var assemblysRepository = Assembly.Load("Exercise.Repository");

            //containerBuilder.RegisterAssemblyTypes(assemblysRepository)
            //          .AsImplementedInterfaces()
            //          .InstancePerLifetimeScope();

#endregion

#region 方法2  选择性注入 与方法1 一样
            //            Assembly Repository = Assembly.Load("Exercise.Repository");
            //            Assembly IRepository = Assembly.Load("Exercise.IRepository");
            //            containerBuilder.RegisterAssemblyTypes(Repository, IRepository)
            //.Where(t => t.Name.EndsWith("Repository"))
            //.AsImplementedInterfaces().PropertiesAutowired();

            //            Assembly service = Assembly.Load("Exercise.Services");
            //            Assembly Iservice = Assembly.Load("Exercise.IServices");
            //            containerBuilder.RegisterAssemblyTypes(service, Iservice)
            //.Where(t => t.Name.EndsWith("Service"))
            //.AsImplementedInterfaces().PropertiesAutowired();
#endregion

#region 方法3  使用 LoadFile 加载服务层的程序集  将程序集生成到bin目录 实现解耦 不需要引用
            //获取项目路径
            //var basePath = Microsoft.DotNet.PlatformAbstractions.ApplicationEnvironment.ApplicationBasePath;
            //var ServicesDllFile = Path.Combine(basePath, "Exercise.Services.dll");//获取注入项目绝对路径
            //var assemblysServices = Assembly.LoadFile(ServicesDllFile);//直接采用加载文件的方法
            //containerBuilder.RegisterAssemblyTypes(assemblysServices).AsImplementedInterfaces();

            //var RepositoryDllFile = Path.Combine(basePath, "Exercise.Repository.dll");
            //var RepositoryServices = Assembly.LoadFile(RepositoryDllFile);//直接采用加载文件的方法
            //containerBuilder.RegisterAssemblyTypes(RepositoryServices).AsImplementedInterfaces();

            //获取方式
            //[FromServices] Autofac.IComponentContext componentContext
            //    var service = componentContext.Resolve<ISiteSurveyAnswerService>();
#endregion
        }
        private void ConfigTraceLog()
        {

            Senparc.CO2NET.Trace.SenparcTrace.SendCustomLog("系统日志", "系统启动");

            //全局自定义日志记录回调
            Senparc.CO2NET.Trace.SenparcTrace.OnLogFunc = () =>
            {
                //加入每次触发Log后需要执行的代码
            };

            //当发生基于WeixinException的异常时触发
            WeixinTrace.OnWeixinExceptionFunc = async ex =>
            {
                //加入每次触发WeixinExceptionLog后需要执行的代码

            };
        }
        /// <summary>
        /// 判断当前配置是否满足使用 Redis（根据是否已经修改了默认配置字符串判断）
        /// </summary>
        /// <param name="senparcSetting"></param>
        /// <returns></returns>
        private bool UseRedis(SenparcSetting senparcSetting, out string redisConfigurationStr)
        {
            redisConfigurationStr = senparcSetting.Cache_Redis_Configuration;
            var useRedis = !string.IsNullOrEmpty(redisConfigurationStr) && redisConfigurationStr != "#{Cache_Redis_Configuration}#"/*默认值，不启用*/;
            return useRedis;
        }
    }

}
