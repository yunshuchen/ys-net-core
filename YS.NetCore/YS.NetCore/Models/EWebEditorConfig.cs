﻿using YS.Utils.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace YS.NetCore.Models
{
    public class EWebEditorConfig
    {
        private string sConfig = "";
        public EWebEditorConfig()
        {
			sConfig = ExtFile.ReadFile("./EWebEditorConfig.txt");
        }

		public string getConfigString(string s_Key)
		{
			string text = "'" + s_Key + " = \"(.*)\"";
			MatchCollection matchCollection = Regex.Matches(this.sConfig, text);
			string result = "";

			foreach (object obj in matchCollection)
			{
				Match match = (Match)obj;
				result = match.Groups[1].Value.ToString();
			}

			return result;
		}


		public List<string> getConfigArray(string s_Key)
		{
			string text = "'" + s_Key + " = \"(.*)\"";
			MatchCollection matchCollection = Regex.Matches(this.sConfig, text);
			int num = 0;

			List<string> array = new List<string>(); ;

			foreach (object obj in matchCollection)
			{
				Match match = (Match)obj;
				array.Add(match.Groups[1].Value.ToString());
			}


			return array;

		}

	}
}
