﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysStrategyController : Net.Mvc.BasicController<sys_strategy, ISysStrategyService>
    {

        public SysStrategyController(ISysStrategyService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_strategy", resource_code = "sys_strategy", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {

            sys_strategy tc_model = new sys_strategy();

            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong());

            }

            return View(tc_model);
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult Save(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string chr_title = data["chr_title"].MyToString();
            string chr_type = data["chr_type"].MyToString();


            string begin_condition = data["begin_condition"].MyToString();
            string end_condition = data["end_condition"].MyToString();

            if (chr_title == "")
            {
                return Json(new { code = -1, msg = "名称不能为空" });
            }
            if (chr_type == "")
            {
                return Json(new { code = -1, msg = "请选择策略分类" });
            }
            if (chr_type == "ip")
            {
                string pattern = @"^(([1-9]\d?)|(1\d{2})|(2[01]\d)|(22[0-3]))(\.((1?\d\d?)|(2[04]/d)|(25[0-5]))){3}$";
                if (begin_condition == "" || end_condition == "")
                {
                    return Json(new { code = -1, msg = "限制允许IP不能为空" });

                }
                if (!Regex.Match(begin_condition, pattern).Success)
                {

                    return Json(new { code = -1, msg = "IP限制开始IP格式不正确" });
                }
                if (!Regex.Match(end_condition, pattern).Success)
                {

                    return Json(new { code = -1, msg = "IP限制结束IP格式不正确" });
                }
            }

            if (chr_type == "time")
            {

                string pattern = @"^((20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d)$";
                if (begin_condition == "" || end_condition == "")
                {

                    return Json(new { code = -1, msg = "允许登录时间段不能为空" });
                }
                if (!Regex.Match(begin_condition, pattern).Success)
                {

                    return Json(new { code = -1, msg = "允许登录的开始时间不能为空" });
                }
                if (!Regex.Match(end_condition, pattern).Success)
                {

                    return Json(new { code = -1, msg = "允许登录的结束时间不能为空" });
                }
            }

            if (chr_type == "power_pwd" || chr_type == "mod_pwd" || chr_type == "login_cooling")
            {
                if (begin_condition == "")
                {

                    return Json(new { code = -1, msg = "参数错误" });
                }
            }
            if (chr_type == "power_pwd")
            {
                if (!Regex.Match(begin_condition, @"^[1-9]\d*$").Success)
                {

                    return Json(new { code = -1, msg = "请输入正确的密码长度" });
                }
            }
            if (chr_type == "mod_pwd" || chr_type == "login_cooling")
            {
                if (!Regex.Match(begin_condition, @"^\d+$").Success)
                {

                    return Json(new { code = -1, msg = "请输入正确的非负数" });
                }
            }

            if (id <= 0)
            {
                var flag = Service.Insert(new sys_strategy()
                {
                    chr_title = chr_title,
                    chr_type = chr_type,
                    begin_condition = begin_condition,
                    end_condition = end_condition
                });

                if (flag.id > 0)
                {
                    AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Add, $"新增安全策略:{chr_title},类型:{chr_type}");
                    return Json(new { code = 1, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else {
                var flag=Service.Modify(a => a.id == id, a => new sys_strategy()
                {
                    chr_title = chr_title,
                    chr_type = chr_type,
                    begin_condition = begin_condition,
                    end_condition = end_condition
                });
                if (flag > 0)
                {
                    AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Update, $"修改安全策略:{chr_title},类型:{chr_type}");
                    return Json(new { code = 1, msg = "修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }
            }

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_strategy", resource_code = "sys_strategy", operate_code = "del")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;
                
                if (deleted)
                {
                    AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Update, $"删除安全策略:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();

            List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>>();

            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.chr_title.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }


             
    }
}