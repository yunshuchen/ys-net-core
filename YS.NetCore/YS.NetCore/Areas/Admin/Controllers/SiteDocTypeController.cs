﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteDocTypeController : Net.Mvc.BasicController<site_doc_type, ISiteDocTypeService>
    {

        public SiteDocTypeController(ISiteDocTypeService service, IApplicationContextAccessor _httpContextAccessor) : base(service,_httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_doc_type", resource_code = "site_doc_type", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {

            site_doc_type tc_model = new site_doc_type();
            site_doc_type sc_model = new site_doc_type();
            site_doc_type en_model = new site_doc_type();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_doc_type();
            }
            if (sc_model == null)
            {
                sc_model = new site_doc_type();
            }
            if (en_model == null)
            {
                en_model = new site_doc_type();
            }

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public  JsonResult Save(
             [FromServices] IApplicationContext app_context
            ,[FromServices] ISiteNodeService node_service
            ,[FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            long node_id = data["node_id"].MyToLong();
            string module = data["module"].MyToString();
            string tc_title = data["tc_title"].MyToString();
            string sc_title = data["sc_title"].MyToString();
            string en_title = data["en_title"].MyToString();
            int n_sort = data["n_sort"].MyToInt();
            if (node_id <= 0)
            {
                return Json(new { code = -1, msg = "请关联栏目" });
            }
            if (module.Trim() == "")
            {
                return Json(new { code = -1, msg = "请输入代号" });
            }
            if (tc_title.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            long _node_id = 0;
            string _node_code = "";
            string _node_name = "";
            site_node node = null;
            if (node_id > 0)
            {
                node = node_service.Get("", a => a.id == node_id && a.lang == "tc");
            }
            if (node != null)
            {
                _node_id = node.id;
                _node_code = node.node_code;
            }
            if (id <= 0)
            {
                int lang_flag = 1;

                long new_id = tableid.getNewTableId<site_doc_type>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                var result = new site_doc_type();
                if (tc_title != "")
                {
                    result = base.Service.Insert(new site_doc_type()
                    {
                        lang = "tc",
                        lang_flag = lang_flag,
                        title = tc_title,
                        module = module,
                        id = new_id,
                        create_id = app_context.CurrentUser.id,
                        create_name = app_context.CurrentUser.alias_name,
                        create_time = DateTime.Now,
                        node_code = _node_code,
                        node_id = _node_id,
                        node_name = _node_name,
                        n_sort = n_sort
                    });
                }
                if (sc_title != "")
                {
                    result = base.Service.Insert(new site_doc_type()
                    {
                        lang = "sc",
                        lang_flag = 1,
                        title = sc_title,
                        module = module,
                        id = new_id,
                        create_id = app_context.CurrentUser.id,
                        create_name = app_context.CurrentUser.alias_name,
                        create_time = DateTime.Now,
                        node_code = _node_code,
                        node_id = _node_id,
                        node_name = _node_name,
                        n_sort = n_sort
                    });
                }
                if (sc_title != "")
                {
                    result = base.Service.Insert(new site_doc_type()
                    {
                        lang = "en",
                        lang_flag = 1,
                        title = en_title,
                        module = module,
                        id = new_id,
                        create_id = app_context.CurrentUser.id,
                        create_name = app_context.CurrentUser.alias_name,
                        create_time = DateTime.Now,
                        node_code = _node_code,
                        node_id = _node_id,
                        node_name = _node_name,
                        n_sort = n_sort
                    });
                }
                if (result.id > 0)
                {
                    AddLog("site_doc_type", LogsOperateTypeEnum.Add, $"新增栏目文件分类:{sc_title}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                var result = 0;

                if (tc_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "tc");
                    if (count > 0)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "tc", a => new site_doc_type()
                        {
                            lang_flag = lang_flag,
                            title = tc_title,
                            node_code = _node_code,
                            node_id = _node_id,
                            node_name = _node_name,
                            n_sort = n_sort
                        });
                    }
                    else
                    {
                        base.Service.Insert(new site_doc_type()
                        {
                            lang = "tc",
                            lang_flag = 1,
                            title = tc_title,
                            module = module,
                            id = id,
                            create_id = app_context.CurrentUser.id,
                            create_name = app_context.CurrentUser.alias_name,
                            create_time = DateTime.Now,
                            node_code = _node_code,
                            node_name = _node_name,
                            node_id = _node_id,
                            n_sort = n_sort
                        });
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "tc");
                }

                if (sc_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");
                    if (count > 0)
                    {
                        result = base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_doc_type()
                        {
                            lang_flag = 1,
                            title = sc_title,
                            node_code = _node_code,
                            node_id = _node_id,
                            node_name = _node_name,
                            n_sort = n_sort
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_doc_type()
                        {
                            lang = "sc",
                            lang_flag = 1,
                            title = sc_title,
                            module = module,
                            id = id,
                            create_id = app_context.CurrentUser.id,
                            create_name = app_context.CurrentUser.alias_name,
                            create_time = DateTime.Now,
                            node_code = _node_code,
                            node_name = _node_name,
                            node_id = _node_id,
                            n_sort = n_sort
                        });
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "sc");
                }

                if (en_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_doc_type()
                        {

                            lang_flag = 1,
                            title = en_title,
                            node_code = _node_code,
                            node_name = _node_name,
                            node_id = _node_id,
                            n_sort = n_sort
                        });
                    }
                    else
                    {
                        base.Service.Insert(new site_doc_type()
                        {
                            lang = "en",
                            lang_flag = 1,
                            title = en_title,
                            module = module,
                            id = id,
                            create_id = app_context.CurrentUser.id,
                            create_name = app_context.CurrentUser.alias_name,
                            create_time = DateTime.Now,
                            node_code = _node_code,
                            node_name = _node_name,
                            node_id = _node_id,
                            n_sort = n_sort
                        });
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "en");
                }

                if (result > 0)
                {
                    AddLog("site_doc_type", LogsOperateTypeEnum.Update, $"修改栏目文件分类:{sc_title}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_doc_type", resource_code = "site_doc_type", operate_code = "del")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;
                
                if (deleted)
                {
                    AddLog("site_doc_type", LogsOperateTypeEnum.Delete, $"删除栏目文件分类:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();

            List<System.Linq.Expressions.Expression<Func<site_doc_type, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_doc_type, bool>>>();
            condition.Add(a => a.lang == "sc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }


             
    }
}