﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteBaseDataController : Net.Mvc.BasicController<site_basedata, ISiteBasedataService>
    {
        public SiteBaseDataController(ISiteBasedataService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        { 
        
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_author", resource_code = "site_author", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {

            site_basedata tc_model = new site_basedata();
            site_basedata sc_model = new site_basedata();
            site_basedata en_model = new site_basedata();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_basedata();
            }
            if (sc_model == null)
            {
                sc_model = new site_basedata();
            }
            if (en_model == null)
            {
                en_model = new site_basedata();
            }

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult Save(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string tc_title = data["tc_title"].MyToString();

            long tc_parent = data["tc_parent"].MyToLong();
            long sc_parent = data["sc_parent"].MyToLong();
            long en_parent = data["en_parent"].MyToLong();


            string tc_parent_name = data["tc_parent_name"].MyToString();
            string sc_parent_name = data["sc_parent_name"].MyToString();
            string en_parent_name = data["en_parent_name"].MyToString();
            if (tc_title.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string sc_title = data["sc_title"].MyToString();
            string en_title = data["en_title"].MyToString();

            if (id <= 0)
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                long node_id = tableid.getNewTableId<site_basedata>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                var result = base.Service.Insert(new site_basedata()
                {
                    lang = "tc",
                    lang_flag = lang_flag,
                    module= "author_user",
                    title = tc_title,
                    parent=tc_parent,
                    parent_name=tc_parent_name,
                    n_sort=1,
                    create_time=DateTime.Now,
                    id = node_id
                });
                if (sc_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "sc",
                        lang_flag = 1,
                        module = "author_user",
                        title = sc_title,
                        parent = sc_parent,
                        parent_name = sc_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }

                if (en_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "en",
                        lang_flag = 1,
                        module = "author_user",
                        title = en_title,
                        parent = en_parent,
                        parent_name = en_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }
                if (result.id > 0)
                {

                    AddLog("site_author", Utils.LogsOperateTypeEnum.Add, $"新增作者:{tc_title}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                var result = base.Service.Modify(a => a.id == id && a.lang == "tc", a => new site_basedata()
                {

                    lang_flag = lang_flag,
                    title = tc_title,
                    parent = tc_parent,
                    parent_name=tc_parent_name
                });
                if (sc_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");


                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_basedata()
                        {
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "sc",
                            lang_flag = 1,
                            module = "author_user",
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }

                if (en_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_basedata()
                        {
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "en",
                            lang_flag = 1,
                            module = "author_user",
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }
                if (result > 0)
                {
                    AddLog("site_author", Utils.LogsOperateTypeEnum.Update, $"修改作者:{tc_title}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_author", resource_code = "site_author", operate_code = "del")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;

                if (deleted)
                {
                    AddLog("site_author", Utils.LogsOperateTypeEnum.Delete, $"删除作者:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_basedata, bool>>>();
            condition.Add(a => a.lang == "tc");
            condition.Add(a => a.module == "author_user");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id asc", condition);
            return PartialView("gv_List", list);
        }




        ///////////////////////////////////////////////////////////
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Dept()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_dept", resource_code = "site_dept", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddDept(string id)
        {

            site_basedata tc_model = new site_basedata();
            site_basedata sc_model = new site_basedata();
            site_basedata en_model = new site_basedata();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_basedata();
            }
            if (sc_model == null)
            {
                sc_model = new site_basedata();
            }
            if (en_model == null)
            {
                en_model = new site_basedata();
            }

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult SaveDept(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string tc_title = data["tc_title"].MyToString();

            long tc_parent = 0;
            long sc_parent = 0;
            long en_parent = 0;


            string tc_parent_name = "";
            string sc_parent_name ="";
            string en_parent_name = "";
            if (tc_title.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string sc_title = data["sc_title"].MyToString();
            string en_title = data["en_title"].MyToString();

            if (id <= 0)
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                long node_id = tableid.getNewTableId<site_basedata>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                var result = base.Service.Insert(new site_basedata()
                {
                    lang = "tc",
                    lang_flag = lang_flag,
                    module = "department",
                    title = tc_title,
                    parent = tc_parent,
                    parent_name = tc_parent_name,
                    n_sort = 1,
                    create_time = DateTime.Now,
                    id = node_id
                });
                if (sc_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "sc",
                        lang_flag = 1,
                        module = "department",
                        title = sc_title,
                        parent = sc_parent,
                        parent_name = sc_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }

                if (en_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "en",
                        lang_flag = 1,
                        module = "department",
                        title = en_title,
                        parent = en_parent,
                        parent_name = en_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }
                if (result.id > 0)
                {
                    AddLog("site_dept", Utils.LogsOperateTypeEnum.Add, $"新增部门:{tc_title}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                var result = base.Service.Modify(a => a.id == id && a.lang == "tc", a => new site_basedata()
                {

                    lang_flag = lang_flag,
                    title = tc_title,
                    parent = tc_parent,
                    parent_name = tc_parent_name
                });
                if (sc_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");


                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_basedata()
                        {
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "sc",
                            lang_flag = 1,
                            module = "department",
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }

                if (en_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_basedata()
                        {
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "en",
                            lang_flag = 1,
                            module = "department",
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }
                if (result > 0)
                {
                    AddLog("site_dept", Utils.LogsOperateTypeEnum.Update, $"修改部门:{tc_title}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_dept", resource_code = "site_dept", operate_code = "del")]
        public JsonResult DeleteDept(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;

                if (deleted)
                {
                    AddLog("site_dept", Utils.LogsOperateTypeEnum.Delete, $"删除部门:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List_Dept(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_basedata, bool>>>();
            condition.Add(a => a.module == "department");
            condition.Add(a => a.lang == "tc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id asc", condition);
            return PartialView("gv_List_Dept", list);
        }
        /////////////////////////////////////////////////////////////////////
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Industry()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_industry", resource_code = "site_industry", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddIndustry(string id)
        {

            site_basedata tc_model = new site_basedata();
            site_basedata sc_model = new site_basedata();
            site_basedata en_model = new site_basedata();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_basedata();
            }
            if (sc_model == null)
            {
                sc_model = new site_basedata();
            }
            if (en_model == null)
            {
                en_model = new site_basedata();
            }

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult SaveIndustry(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string tc_title = data["tc_title"].MyToString();

            long tc_parent = 0;
            long sc_parent = 0;
            long en_parent = 0;


            string tc_parent_name = "";
            string sc_parent_name = "";
            string en_parent_name = "";
            if (tc_title.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string sc_title = data["sc_title"].MyToString();
            string en_title = data["en_title"].MyToString();

            if (id <= 0)
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                long node_id = tableid.getNewTableId<site_basedata>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                var result = base.Service.Insert(new site_basedata()
                {
                    lang = "tc",
                    lang_flag = lang_flag,
                    module = "industry",
                    title = tc_title,
                    parent = tc_parent,
                    parent_name = tc_parent_name,
                    n_sort = 1,
                    create_time = DateTime.Now,
                    id = node_id
                });
                if (sc_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "sc",
                        lang_flag = 1,
                        module = "industry",
                        title = sc_title,
                        parent = sc_parent,
                        parent_name = sc_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }

                if (en_title != "")
                {
                    base.Service.Insert(new site_basedata()
                    {
                        lang = "en",
                        lang_flag = 1,
                        module = "industry",
                        title = en_title,
                        parent = en_parent,
                        parent_name = en_parent_name,
                        n_sort = 1,
                        create_time = DateTime.Now,
                        id = node_id
                    });
                }
                if (result.id > 0)
                {
                    AddLog("site_industry", Utils.LogsOperateTypeEnum.Add, $"新增行业:{tc_title}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int lang_flag = 1;
                if (sc_title == "")
                {
                    lang_flag = 2;
                }

                var result = base.Service.Modify(a => a.id == id && a.lang == "tc", a => new site_basedata()
                {

                    lang_flag = lang_flag,
                    title = tc_title,
                    parent = tc_parent,
                    parent_name = tc_parent_name
                });
                if (sc_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");


                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_basedata()
                        {
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "sc",
                            lang_flag = 1,
                            module = "industry",
                            title = sc_title,
                            parent = sc_parent,
                            parent_name = sc_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }

                if (en_title != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_basedata()
                        {
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_basedata()
                        {
                            lang = "en",
                            lang_flag = 1,
                            module = "industry",
                            title = en_title,
                            parent = en_parent,
                            parent_name = en_parent_name,
                            n_sort = 1,
                            create_time = DateTime.Now,
                            id = id
                        });
                    }
                }
                if (result > 0)
                {
                    AddLog("site_industry", Utils.LogsOperateTypeEnum.Update, $"修改行业:{tc_title}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_industry", resource_code = "site_industry", operate_code = "del")]
        public JsonResult DeleteIndustry(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;

                if (deleted)
                {
                    AddLog("site_industry", Utils.LogsOperateTypeEnum.Delete, $"删除行业:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List_Industry(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_basedata, bool>>>();
            condition.Add(a => a.module == "industry");
            condition.Add(a => a.lang == "tc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id asc", condition);
            return PartialView("gv_List_Industry", list);
        }

    }
}