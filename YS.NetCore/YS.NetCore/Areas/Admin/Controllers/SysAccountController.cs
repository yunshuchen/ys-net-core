﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysAccountController : Net.Mvc.BasicController<sys_account, ISysAccountService>
    {
        private ISysUserRoleRelationService accountrole;
        private ISysRoleService role;
        IMd5Hash md5Hash;
        public SysAccountController(ISysAccountService userService
            , IMd5Hash _md5Hash
            , ISysUserRoleRelationService _accountrole
            , ISysRoleService _role, IApplicationContextAccessor _httpContextAccessor) : base(userService, _httpContextAccessor)
        {
            accountrole = _accountrole;
            role = _role;
            md5Hash = _md5Hash;
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account")]
        public IActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account",operate_code = "add,modify")]
        public IActionResult AddAccount(
            [FromServices]ISysStrategyService sys_strategy
            , [FromServices]ISysUserStrategyService sys_user_strategy
            , string id)
        {

            sys_account model = new sys_account();
            var user_strategy_condition = new List<System.Linq.Expressions.Expression<Func<sys_user_strategy, bool>>>();


                if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne(a => a.id == id.MyToLong());

                user_strategy_condition.Add(a => a.user_id == id.MyToLong());
            }


            var strategys = sys_strategy.GetList("", new List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>>());

        
            var user_strategy = sys_user_strategy.GetList("", user_strategy_condition) ;


            List<object> strategys_list = new List<object>();
            foreach (var item in strategys)
            {
                strategys_list.Add(new
                {
                    strategy_id = item.id,
                    strategy_name = item.chr_title,
                    selected = user_strategy.Where(b => b.strategy_id == item.id).Count()
                });
            }
            
  
            var list = role.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>() {
                  a=> a.status ==StatusEnum.Legal
                });
            List<object> result = new List<object>();
            if (id.MyToLong() > 0)
            {
                var UserRole = accountrole.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==id.MyToLong()
                });
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        Checked = UserRole.Where(a => a.role_id == item.id).Count()
                    });
                }
            }
            else
            {
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        Checked = 0
                    });
                }
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { model = model, list = result, strategys_list })));
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account")]
        [HttpPost]
        public PartialViewResult gv_AccountList(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string Account = data["acc"].MyToString();
            string IP = data["ip"].MyToString();


            List<System.Linq.Expressions.Expression<Func<sys_account, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_account, bool>>>();
            if (Account.Trim() != "")
            {
                condition.Add(a => a.account.Contains(Account));
            }
            if (IP.Trim() != "")
            {
                condition.Add(a => a.last_login_ip.Contains(IP));
            }



            var list = Service.GetPageList(page_index, page_size, "id desc", condition);

            return PartialView("gv_AccountList", list);

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account", operate_code = "add,modify")]
        [HttpPost]
        public JsonResult Save([FromServices]ISysUserStrategyService sys_user_strateg, [FromServices]ISysUserPwdLogService sys_user_pwd
            , IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string account = data["account"].MyToString();
            string chr_name = data["chr_name"].MyToString();
            string pwd = data["pwd"].MyToString();
            string email = data["email"].MyToString();
            string mobile_phone = data["mobile_phone"].MyToString();
            
            int status = data["status"].MyToInt();
            string roles = data["roles"].MyToString();
            string strategy = data["strategy"].MyToString();
            
            if (account == "" || chr_name == "" || (status != 0 && status != 1))
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            long new_id = id;
            if (id <= 0)
            {

                if (pwd.Trim().Length <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                var check = Service.GetByOne("", a => a.account == account);
                if (check != null)
                {
                    return Json(new { code = -1, msg = "账号已存在" });
                }

                bool flag = false;
                var new_md5 = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(pwd));
                var model = Service.Insert(new sys_account()
                {
                    account = account,
                    password = new_md5,
                    alias_name = chr_name,
                    user_status = (StatusEnum)status,
                    create_time = DateTime.Now,
                    login_count = 0,
                    last_login_ip = "",
                    creator_name = "",
                    email= email,
                    mobile_phone= mobile_phone,
                    last_login_time = DateTime.MinValue,
                    token = "",
                    last_online_time = DateTime.MinValue,
                    next_set_pwd = DateTime.MinValue
                });
                new_id = model.id;
                flag = new_id > 0;
                sys_user_pwd.Insert(new sys_user_pwd_log()
                {
                    mod_time = DateTime.Now,
                    new_pwd = model.password,
                    user_id = model.id
                });
                AddLog("sys_account", Utils.LogsOperateTypeEnum.Add, $"新增账号,account:{account},别名:{chr_name}");
            }
            else
            {
                bool flag = false;

                if (pwd.Trim().Length > 0)
                {
                    var new_md5 = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(pwd));
                    flag = Service.Modify(a => a.id == id, a => new sys_account()
                    {
                        alias_name = chr_name,
                        email=email,
                        mobile_phone= mobile_phone,
                        user_status = (StatusEnum)status,
                        password = new_md5
                    }) > 0;
                    var model = Service.GetByOne("id asc", a => a.id == id);
                    sys_user_pwd.Insert(new sys_user_pwd_log()
                    {
                        mod_time = DateTime.Now,
                        new_pwd = model.password,
                        user_id = id
                    });
                    AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改账号信息与密码,account:{account},别名:{chr_name}");
                }
                else
                {
                    flag = Service.Modify(a => a.id == id, a => new sys_account()
                    {
                        email= email,
                        alias_name = chr_name,
                        user_status = (StatusEnum)status

                    }) > 0;
                    AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改账号信息,account:{account},别名:{chr_name}");
                }

            }

            if (new_id > 0)
            {
                //删除以前的
                accountrole.Delete(a => a.user_id == new_id);
                var roles_arr = roles.Split(',');
                List<sys_user_role_relation_entity> b_insert = new List<sys_user_role_relation_entity>();
                foreach (var item in roles_arr)
                {
                    b_insert.Add(new sys_user_role_relation_entity()
                    {
                        user_id = new_id,
                        role_id = item.MyToLong()
                    });
                }
                accountrole.BulkInsert(b_insert);
                sys_user_strateg.Delete(a => a.user_id == new_id);
                var strategy_arr = strategy.Split(',');
                List<sys_user_strategy> s_insert = new List<sys_user_strategy>();
                foreach (var item in strategy_arr)
                {
                    s_insert.Add(new sys_user_strategy()
                    {
                        user_id = new_id,
                        strategy_id = item.MyToLong()
                    });
                }
                sys_user_strateg.BulkInsert(s_insert);

                return Json(new { code = 0, msg = "操作成功" });

            }
            return Json(new { code = -1, msg = "操作失败" });
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account", operate_code = "delete")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string account = data["account"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {

                //用户踢线
                //string key = "##" + company_id + "##" + id + "##";
                // Caches.OnLineAdminUser.LoginOutAccount(key);
                //AdminLogs.WriteLogs(company_id, TypeConverters.IP(), CurrentUser.account, "系统账号管理", "修改系统账号[" + account + "]状态");

                AddLog("sys_account", Utils.LogsOperateTypeEnum.Delete, $"删除账号account:{account}");
                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_account", resource_code = "sys_account", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string account = data["account"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_account()
            {
                user_status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改账号状态,account:{account},状态:{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}