



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    [DefaultAuthorize]
    public class SysResourceController : Net.Mvc.BasicController<sys_resource_entity, ISysResourceService>
    {

        private readonly IApplicationContextAccessor applicationContextAccessor;
        public SysResourceController(IApplicationContextAccessor _applicationContextAccessor, ISysResourceService userService) : base(userService,_applicationContextAccessor)
        {
            applicationContextAccessor = _applicationContextAccessor;
        }
        [DefaultAuthorize]
        public IActionResult Index(string catalog_id)
        {
            return View("Index", catalog_id);
        }

        public IActionResult Add(string module_id, string id)
        {

            sys_resource_entity model = new sys_resource_entity();
            if (!string.IsNullOrEmpty(module_id) && module_id.MyToLong() > 0)
            {
                model.p_module = module_id.MyToLong();
            }
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne(a => a.id == id.MyToLong());
            }
            return View(model);
        }
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();

            long catalog_id = data["catalog_id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            long opt_catalog_id = data["opt_catalog_id"].MyToLong();


            List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>();

            if (catalog_id > 0)
            {
                condition.Add(a => a.p_module == catalog_id);
            }
            else
            {
                if (opt_catalog_id > 0)
                {
                    condition.Add(a => a.p_module == catalog_id);
                }
            }

            if (chr_name.Trim().Length > 0)
            {
                condition.Add(a => a.res_name.Contains(chr_name));
            }


            var list = Service.GetPageList(page_index, page_size, "n_sort asc", condition);
            return PartialView("gv_List", list);
        }

        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            bool deleted = Service.Modify(a => a.id == id, a => new sys_resource_entity()
            {
                delete_status = DeleteEnum.Delete
            }) > 0;
            if (deleted)
            {
                AddLog("sys_resource", Utils.LogsOperateTypeEnum.Delete, $"删除菜单资源{chr_name}");
                Service.ReloadCahce();
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }
        [DefaultAuthorize]
        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {

            long id = data["id"].MyToLong();
            long p_module = data["p_module"].MyToLong();
            string res_code = data["res_code"].MyToString();
            string res_name = data["res_name"].MyToString();

            string uri = data["uri"].MyToString();
            string url = data["url"].MyToString();

            string description = data["description"].MyToString();

            int n_sort = data["n_sort"].MyToInt();
            StatusEnum status = (StatusEnum)data["status"].MyToInt();



            bool flag = false;

            if (id <= 0)
            {

                var check = Service.GetByOne(a => a.res_code == res_code && a.p_module == p_module);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该模块下模块代号已经存在" });
                }
                var user = applicationContextAccessor.Current.CurrentUser;
                flag=Service.Insert(new sys_resource_entity() {

                    p_module = p_module,
                    res_code = res_code,
                    res_name = res_name,
                    uri= uri,
                    url=url,
                    n_sort = n_sort,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now

                }).id>0;
                if (flag)
                {
                    Service.ReloadCahce();
                    AddLog("sys_resource", Utils.LogsOperateTypeEnum.Add, $"新增菜单资源{res_name},模块ID:{p_module}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {

                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                var check = Service.GetByOne(a => a.res_code == res_code 
                && a.p_module == p_module
                &&a.id!=id);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该模块下模块代号已经存在" });
                }
                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Modify(a=>a.id==id,a=>new sys_resource_entity()
                {

                    p_module = p_module,
                    res_code = res_code,
                    res_name = res_name,
                    uri = uri,
                    url = url,
                    n_sort = n_sort,
                    description = description,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now

                })> 0;
                if (flag)
                {
                    AddLog("sys_resource", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源{res_name},模块ID:{p_module}");
                    Service.ReloadCahce();
                    return Json(new { code = 0, msg = "修改成功" });
                }
                else
                {

                    return Json(new { code = -1, msg = "修改失败" });
                }
            }


        }
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_resource_entity()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("sys_resource", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源状态{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                Service.ReloadCahce();
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}
