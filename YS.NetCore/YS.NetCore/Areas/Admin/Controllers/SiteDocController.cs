﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteDocController : Net.Mvc.BasicController<site_doc, ISiteDocService>
    {
        public SiteDocController(ISiteDocService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string pid, string id)
        {

            site_doc tc_model = new site_doc();
            site_doc sc_model = new site_doc();
            site_doc en_model = new site_doc();

            tc_model.module = pid;
            sc_model.module = pid;
            en_model.module = pid;

            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");
            }


            if (tc_model == null)
            {
                tc_model = new site_doc();
                tc_model.module = pid;
            }
            if (sc_model == null)
            {
                sc_model = new site_doc();
                sc_model.module = pid;
            }
            if (en_model == null)
            {
                en_model = new site_doc();
                en_model.module = pid;
            }


            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }


        [HttpPost]
        public JsonResult Delete([FromServices] Net.MyLucene.ISearchService searcher, IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["NodeName"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            var model = Service.GetByOne(a => a.id == id && a.lang == "sc");
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                searcher.DeleteDocIndex(model);
                AddLog("site_doc", LogsOperateTypeEnum.Delete, $"删除栏目文件资源:{chr_name}");
                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult Save(
            [FromServices] IApplicationContext app_context
             , [FromServices] ISiteDocTypeService doc_tye
            , [FromServices]ISysTableidService tableid
            , [FromServices] Net.MyLucene.ISearchService searcher
            , IFormCollection data)
        {

            long id = data["id"].MyToLong();
            string module = data["module"].MyToString();
            if (module == "")
            {
                return Json(new
                {
                    code = -1,
                    msg = "参数错误"
                });
            }
            string tc_name = data["tc_name"].MyToString();
            if (tc_name == "")
            {
                return Json(new
                {
                    code = -1,
                    msg = "参数错误"
                });
            }

            var doc_type_model = doc_tye.GetByOne(a => a.module.ToLower() == module.ToLower());

            if (doc_type_model == null)
            {
                return Json(new
                {
                    code = -1,
                    msg = "该分类不存在"
                });
            }
            long node_id = doc_type_model.node_id;
            string node_code = doc_type_model.node_code;
            StatusEnum is_top = (StatusEnum)data["is_top"].MyToInt();
            StatusEnum is_new = (StatusEnum)data["is_new"].MyToInt();
            StatusEnum is_login = (StatusEnum)data["login"].MyToInt();
            StatusEnum is_show = (StatusEnum)data["is_show"].MyToInt();
            string date = data["date"].MyToString();

            int n_sort = data["n_sort"].MyToInt();


            string tc_desc = data["tc_desc"].MyToString();
            string tc_img = data["tc_img"].MyToString();
            string tc_file_link = data["tc_file_link"].MyToString();
            string tc_link = data["tc_link"].MyToString();

            



            string tc_author = data["tc_author"].MyToString();

            string tc_source = data["tc_source"].MyToString();
            string tc_chr_content = data["tc_chr_content"].MyToString();
            string sc_name = data["sc_name"].MyToString();
            string sc_desc = data["sc_desc"].MyToString();
            string sc_img = data["sc_img"].MyToString();
            string sc_file_link = data["sc_file_link"].MyToString();
            string sc_link = data["sc_link"].MyToString();

            string sc_author = data["sc_author"].MyToString();

            string sc_source = data["sc_source"].MyToString();
            string sc_chr_content = data["sc_chr_content"].MyToString();
            string en_name = data["en_name"].MyToString();
            string en_desc = data["en_desc"].MyToString();
            string en_img = data["en_img"].MyToString();
            string en_file_link = data["en_file_link"].MyToString();
            string en_link = data["en_link"].MyToString();
            string en_author = data["enc_author"].MyToString();
            string en_source = data["enc_source"].MyToString();
            string en_chr_content = data["en_chr_content"].MyToString();
            if (id <= 0)
            {
                var new_id = tableid.getNewTableId<site_doc>();
                int lang_flag = 1;
                var doc = new site_doc();
                if (tc_name != "")
                {


                    doc = Service.Insert(new site_doc()
                    {
                        module = module,
                        is_top = is_top,
                        is_new = is_new,
                
                        is_login = is_login,
                        n_sort = n_sort,
                        date = date,
   
                        name = tc_name,
                        chr_desc = tc_desc,
                        img = tc_img,
                        file_link = tc_file_link,
                        link = tc_link,
      
                        author = tc_author.MyToString(),
                        source = tc_source.MyToString(),
                  
                        chr_content = tc_chr_content,
                        create_id = app_context.CurrentUser.id,
                        create_time = DateTime.Now,
                        create_name = app_context.CurrentUser.alias_name,
                        lang = "tc",
                        node_code = node_code,
                        node_id = node_id,
                        lang_flag = lang_flag,
                        valid = ReViewStatusEnum.Pass,
                        id = new_id,
                        is_show = is_show
                    });
                }
                if (doc.id > 0)
                {


                    searcher.InsertDocIndex(doc);



                }

                if (sc_name != "")
                {
                    doc = Service.Insert(new site_doc()
                    {
                        module = module,
                        is_top = is_top,
                        is_new = is_new,
     
                        is_login = is_login,
                        n_sort = n_sort,
                        date = date,
        
                        name = sc_name,
                        chr_desc = sc_desc,
                        img = sc_img,
                        file_link = sc_file_link,
                        link = sc_link,
                        author = sc_author.MyToString(),
                        source = sc_source.MyToString(),
                        chr_content = sc_chr_content,
                        create_id = app_context.CurrentUser.id,
                        create_time = DateTime.Now,
                        create_name = app_context.CurrentUser.alias_name,
                        lang = "sc",
                        node_code = node_code,
                        node_id = node_id,
                        lang_flag = 1,
                        valid = ReViewStatusEnum.Pass,
                        id = new_id,
                        is_show = is_show
                    });

                    if (doc.id > 0)
                    {
                        searcher.InsertDocIndex(doc);
                    }
                }
                if (en_name != "")
                {

                    doc = Service.Insert(new site_doc()
                    {
                        module = module,
                        is_top = is_top,
                        is_new = is_new,
     
                        is_login = is_login,
                        n_sort = n_sort,
                        date = date,
              
                        name = en_name,
                        chr_desc = en_desc,
                        img = en_img,
                        file_link = en_file_link,
                        link = en_link,
                        author = en_author.MyToString(),
                        source = en_source.MyToString(),
                        chr_content = en_chr_content,
                        create_id = app_context.CurrentUser.id,
                        create_time = DateTime.Now,
                        create_name = app_context.CurrentUser.alias_name,
                        lang = "en",
                        node_code = node_code,
                        node_id = node_id,
                        lang_flag = 1,
                        valid = ReViewStatusEnum.Pass,
                        id = new_id,
                        is_show = is_show
                    });
                    if (doc.id > 0)
                    {
                        searcher.InsertDocIndex(doc);
                    }
                }

                AddLog("site_doc", Utils.LogsOperateTypeEnum.Add, $"新增栏目文件资源:{tc_name}");
                return Json(new { code = 1, msg = "保存成功" });
            }
            else
            {
                int lang_flag = 1;
                if (sc_name == "")
                {

                    lang_flag = 2;
                }
                int result = 0;
                if (tc_name != "")
                {
                    var doc = Service.GetByOne(a => a.id == id && a.lang == "tc");
                    if (doc != null)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "tc", a => new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                      
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
                  
                            name = tc_name,
                            chr_desc = tc_desc,
                            img = tc_img,
                            file_link = tc_file_link,
                            link = tc_link,
                            author = tc_author.MyToString(),
                            source = tc_source.MyToString(),
                            chr_content = tc_chr_content,
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = lang_flag,
                            is_show = is_show
                        });
                        if (result > 0)
                        {
                            searcher.UpdateDocIndex(doc);
                        }
                    }
                    else
                    {
                        var doc1 = Service.Insert(new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                          
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
                
                            name = tc_name,
                            chr_desc = tc_desc,
                            img = tc_img,
                            file_link = tc_file_link,
                            link = tc_link,
                            author = tc_author.MyToString(),
                            source = tc_source.MyToString(),
                            chr_content = tc_chr_content,
                            create_id = app_context.CurrentUser.id,
                            create_time = DateTime.Now,
                            create_name = app_context.CurrentUser.alias_name,
                            lang = "tc",
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = lang_flag,
                            valid = ReViewStatusEnum.Pass,
                            id = id,
                            is_show = is_show
                        });
                        if (doc.id > 0)
                        {
                            searcher.InsertDocIndex(doc1);
                        }
                    } 
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "tc");
                }
        
                
                if (sc_name != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");
                    if (count > 0)
                    {
                        result=Service.Modify(a => a.id == id && a.lang == "sc", a => new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                         
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
                 
                            name = sc_name,
                            chr_desc = sc_desc,
                            img = sc_img,
                            file_link = sc_file_link,
                            link = sc_link,
                            author = sc_author.MyToString(),
                            source = sc_source.MyToString(),
                            chr_content = sc_chr_content,
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = lang_flag,
                            is_show = is_show
                        });

                        var doc = Service.GetByOne(a => a.id == id && a.lang == "sc");
                        if (result > 0)
                        {
                            searcher.UpdateDocIndex(doc);
                        }
                    }
                    else
                    {

                         var i_doc= Service.Insert(new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                  
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
                            name = sc_name,
                            chr_desc = sc_desc,
                            img = sc_img,
                            file_link = sc_file_link,
                            link = sc_link,
                             author = tc_author.MyToString(),
                             source = tc_source.MyToString(),
                             chr_content = sc_chr_content,
                            create_id = app_context.CurrentUser.id,
                            create_time = DateTime.Now,
                            create_name = app_context.CurrentUser.alias_name,
                            lang = "sc",
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = 1,
                            valid = ReViewStatusEnum.Pass,
                            id = id,
                            is_show = is_show
                        });
                        if (i_doc.id > 0)
                        {
                            searcher.UpdateDocIndex(i_doc);
                        }
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "sc");
                }


                if (en_name != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");

                    if (count > 0)
                    {
                        result=Service.Modify(a => a.id == id && a.lang == "en", a => new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                      
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
                
                            name = en_name,
                            chr_desc = en_desc,
                            img = en_img,
                            file_link = en_file_link,
                            link = en_link,
                            author = en_author.MyToString(),
                            source = en_source.MyToString(),
                            chr_content = en_chr_content,
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = lang_flag,
                            is_show = is_show
                        });

                        var doc = Service.GetByOne(a => a.id == id && a.lang == "en");
                        if (result > 0)
                        {
                            searcher.UpdateDocIndex(doc);
                        }
                    }
                    else
                    {
                       var i_doc= Service.Insert(new site_doc()
                        {
                            module = module,
                            is_top = is_top,
                            is_new = is_new,
                    
                            is_login = is_login,
                            n_sort = n_sort,
                            date = date,
         
                            name = en_name,
                            chr_desc = en_desc,
                            img = en_img,
                            file_link = en_file_link,
                            link = en_link,
                           author = en_author.MyToString(),
                           source = en_source.MyToString(),
                           chr_content = en_chr_content,
                            create_id = app_context.CurrentUser.id,
                            create_time = DateTime.Now,
                            create_name = app_context.CurrentUser.alias_name,
                            lang = "en",
                            node_code = node_code,
                            node_id = node_id,
                            lang_flag = 1,
                            valid = ReViewStatusEnum.Pass,
                            id = id,
                            is_show = is_show
                        });

                        if (i_doc.id > 0)
                        {
                            searcher.UpdateDocIndex(i_doc);
                        }
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "en");
                }
                if (result > 0)
                {
                    AddLog("site_doc", Utils.LogsOperateTypeEnum.Update, $"修改栏目文件资源:{tc_name}");
                    return Json(new { code = 1, msg = "修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }
            }


        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string pid = data["pid"].MyToString();

            string lang = data["lang"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_doc, bool>>>();

            if (lang == "sc")
            {
                condition.Add(a => (a.lang == "sc" || ((a.lang == "tc" && a.lang_flag == 2))) && a.module == pid);
            }
            else
            {
                condition.Add(a => a.lang == lang && a.module == pid);
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }


        public async Task<JsonResult> GetDocTypeTreeAsync([FromServices]IDynamicPermissionCheck PermissionCheck, [FromServices] ISiteDocTypeService doc_tye, IFormCollection data)
        {

            string lang = data["lang"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_doc_type, bool>>>();
            if (lang == "sc")
            {
                condition.Add(a => a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            else
            {
                condition.Add(a => a.lang == lang);
            }
            var list = doc_tye.GetList("id asc", condition);





            SiteNodesTree result = new SiteNodesTree();
            result.children = new List<SiteNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.title = "根节点";
            result.view_disable = true;
            result.add_disable = true;

            foreach (var a in list)
            {

                if (await PermissionCheck.CheckAsync(base.httpContextAccessor.Current.HttpContextAccessor.HttpContext, "site_content", $"cms_content_g_{a.node_id}"))
                {
                    SiteNodesTree child = new SiteNodesTree();
                    child.children = new List<SiteNodesTree>();
                    child.id = a.id;
                    child.need_review = StatusEnum.UnLegal;
                    child.pid = 0;
                    child.node_code = a.module;
                    result.spread = true;
                    child.title = a.title;
                    result.view_disable = false;
                    result.add_disable = false;
                    result.children.Add(child);
                }

            }
            List<SiteNodesTree> rs = new List<SiteNodesTree>();
            rs.Add(result);
            return Json(rs);
        }



    }
}