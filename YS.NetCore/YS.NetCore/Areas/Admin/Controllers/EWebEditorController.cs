﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using YS.NetCore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json.Linq;
using UEditorNetCore;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class EWebEditorController : BasicController<sys_template, ISysTemplateService>
    {

        private readonly ILogger<EWebEditorController> logger;
        private ISiteFileProvider siteProvider;
        private readonly IMyCache myCache;
        public EWebEditorController(IWebHostEnvironment environment,
           ISiteFileProvider _siteProvider, ISysTemplateService ser,
           IMyCache _myCache,
        ILogger<EWebEditorController> _logger,IApplicationContextAccessor _httpContextAccessor) : base(ser, _httpContextAccessor)
        {
            logger = _logger;
            siteProvider = _siteProvider;
            myCache = _myCache;
            this.environment = environment;

        }

        private IWebHostEnvironment environment { get; set; }

        private EWebEditorConfig mConfig;
        private string sLicense = "";
        [AllowAnonymous]
        [RequestSizeLimit(100000000)]
        [Route("/Admin/EWebEditor")]
        [Route("/Admin/EWebEditor/index")]
        [Route("/admin/aspx/i.aspx")]
        public ActionResult Index([FromServices] IHostingEnvironment _hostingEnvironment, IFormCollection data)
        {
            mConfig = new EWebEditorConfig();
            sLicense = mConfig.getConfigString("License");
            var action = Request.Query["action"].MyToString().ToLower();
            if (action == "config")
            {
                return ShowConfig();
            }
            if (action == "license")
            {

                return Content(ShowLicense());
            }
            if (action == "wordeqlicense")
            {

                return Content(ShowWordEqLicense());
            }
            return Json(new { });
        }
        private string ShowLicense()
        {

            //string text = this.Request.Query["r"];
            //if (text.Length < 10)
            //{
            //    return "";
            //}
            //sLicense = mConfig.getConfigString("License");
            //if (sLicense == "")
            //{
            //    return "";
            //}
            //string domain = this.GetDomain();
            //if (domain == "127.0.0.1" || domain == "localhost")
            //{
            //    return "";
            //}
            //string text2 = "";
            //string[] array = sLicense.Split(';');
            //int num = 0;
            //int num2 = array.Length - 1;

            //for (int i = num; i <= num2; i++)
            //{
            //    char text3 = ',';
            //    if (array[i].IndexOf(",") >= 0)
            //    {
            //        text3 = ',';
            //    }
            //    else
            //    {
            //        text3 = ':';
            //    }
            //    string[] array2 = array[i].Split(text3);
            //    if (array2.Length == 8 && array2[7].MyToInt() == 32)
            //    {
            //        bool flag = false;
            //        string text4 = array2[0];
            //        flag = true;
            //        if (flag)
            //        {
            //            int num3 = 0;
            //            do
            //            {
            //                text2 = text2 + array2[num3] + text3;
            //                num3++;
            //            }
            //            while (num3 <= 6);
            //            text2 = text2 + GetMD5(Left(array2[7], 16) + text, 16) + GetMD5(Right(array2[7], 16) + text, 16);
            //            break;
            //        }
            //    }
            //}
            //return text2;

            string text = this.Request.Query["r"];
            if (text.Length < 10)
            {
                return "";
            }

            sLicense = mConfig.getConfigString("License");
            if (sLicense == "")
            {
                return "";
            }
            string domain = this.GetDomain();
            if (domain == "127.0.0.1" || domain == "localhost")
            {
                return "";
            }
            string text2 = "";
            string[] array = sLicense.Split(';');
            int num = 0;
            int num2 = sLicense.Length - 1;
            checked
            {
                for (int i = num; i <= num2; i++)
                {
                    char text3 = ':';
                    if (array[i].IndexOf(',') > 0)
                    {
                        text3 = ',';
                    }
                    string[] array2 = array[i].Split(text3);
                    if (array2.Length == 8 && array2[7].Length == 32)
                    {
                        bool flag = false;
                        string text4 = array2[0];
                        if (text4 == "3")
                        {
                            if (array2[6] == domain || Right(domain, array2[6].Length + 1) == "." + array2[6])
                            {
                                flag = true;
                            }
                        }
                        else if (array2[6] == domain || "www." + array2[6] == domain)
                        {
                            flag = true;
                        }
                        if (flag)
                        {
                            int num3 = 0;
                            do
                            {
                                text2 = text2 + array2[num3] + text3;
                                num3++;
                            }
                            while (num3 <= 6);
                            text2 = text2 + GetMD5(Left(array2[7], 16) + text, 16) + GetMD5(Right(array2[7], 16) + text, 16);
                            break;
                        }
                    }
                }
                return text2;

            }

        }


        private string ShowWordEqLicense()
        {
            string text = this.Request.Query["r"].MyToString();
            if (text.Length < 10)
            {
                return "";
            }

            string configString = mConfig.getConfigString("WordEqLicense");
            if (configString == "")
            {
                return "";
            }
            string domain = GetDomain();
            if (domain == "127.0.0.1" || domain == "localhost")
            {
                return "";
            }
            string text2 = "";
            string[] array = configString.Split(';');
            int num = 0;
            int num2 = array.Length - 1;

            for (int i = num; i <= num2; i++)
            {
                char text3 = ':';
                if (array[i].IndexOf(',') >= 0)
                {
                    text3 = ',';
                }
                string[] array2 = array[i].Split(text3);
                if (array2.Length == 4 && array2[3].Length == 32)
                {

                    int num3 = 0;
                    do
                    {
                        text2 = text2 + array2[num3] + text3;
                        num3++;
                    }
                    while (num3 <= 2);
                    text2 = text2 + GetMD5(Left(array2[3], 16) + text, 16) + GetMD5(Right(array2[3], 16) + text, 16);
                    break;

                }
            }
            return text2;
        }

        public static string Left(string sSource, int iLength)
        {
            return sSource.Substring(0, iLength > sSource.Length ? sSource.Length : iLength);
        }
        public static string Right(string sSource, int iLength)
        {
            return sSource.Substring(iLength > sSource.Length ? 0 : sSource.Length - iLength);
        }

        public static string Mid(string sSource, int iLength)
        {

            return sSource.Substring(0, iLength > sSource.Length ? sSource.Length : iLength);
        }
        public static string Mid(string sSource, int iStart, int iLength)
        {
            int iStartPoint = iStart > sSource.Length ? sSource.Length : iStart;
            return sSource.Substring(iStartPoint, iStartPoint + iLength > sSource.Length ? sSource.Length - iStartPoint : iLength);
        }
        public static string GetHideInputHtml(string s_Name, string s_Value)
        {
            return string.Concat(new string[]
            {
                "<input type=\"hidden\" name=\"",
                s_Name,
                "\" id=\"",
                s_Name,
                "\" value=\"",
                (s_Value),
                "\">"
            });
        }


        private List<string> sAllowExt = new List<string>();

        private string sAction = "";
        private long nAllowSize = 2048 * 1024;
        [AllowAnonymous]
        [RequestSizeLimit(100000000)]

        [Route("/Admin/EWebEditor/Upload")]
        [Route("/Admin/aspx/Upload.aspx")]
        public IActionResult Upload([FromServices] IHostingEnvironment _hostingEnvironment, IFormCollection data)
        {
            mConfig = new EWebEditorConfig();
            sAction = Request.Query["action"].MyToString().ToLower();
            sStyleName = Request.Query["style"].MyToString().ToLower();
            this.sType = Request.Query["type"].MyToString().Trim().ToLower();

            ////aStyle = mConfig.getConfigArray("Style");

            ////List<string> array = new List<string>();
            ////int num2 = aStyle.Count() - 1;
            ////for (int i = 0; i <= num2; i++)
            ////{
            ////    array = aStyle[i].Split(new string[] { "|||" }, StringSplitOptions.None).ToList();
            ////    if (array[0].ToLower()==sStyleName)
            ////    {
            ////        break;
            ////    }
            ////}
            #region init Allow
            if (sType == "remote")
            {
                this.sAllowExt = new List<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp", ".ico" };
                this.nAllowSize = 20 * 1024;

            }
            else if (sType == "file")
            {

                this.sAllowExt = new List<string>() {
                    ".png",
                    ".jpg",
                    ".jpeg",
                    ".gif",
                    ".bmp",
                    ".icon",
                    ".webp",
                    ".flv",
                    ".swf",
                    ".mkv",
                    ".avi",
                    ".rm",
                    ".rmvb",
                    ".mpeg",
                    ".mpg",
                    ".ogg",
                    ".ogv",
                    ".mov",
                    ".wmv",
                    ".mp4",
                    ".webm",
                    ".mp3",
                    ".wav",
                    ".mid",
                    ".rar",
                    ".zip",
                    ".tar",
                    ".gz",
                    ".7z",
                    ".bz2",
                    ".cab",
                    ".iso", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml" };
                this.nAllowSize = 2048 * 1024;


            }
            else if (sType == "media")

            {
                this.sAllowExt = new List<string>() { ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid" };
                this.nAllowSize = 2048 * 1024;

            }
            else if (sType == "flash")
            {
                this.sAllowExt = new List<string>()
                        { ".swf"};
                this.nAllowSize = 20 * 1024;

            }
            else if (sType == "local")

            {
                this.sAllowExt = new List<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp", ".ico" };
                this.nAllowSize = 20 * 1024;
            }
            else
            {
                this.sAllowExt = new List<string>() {
                    ".png",
                    ".jpg",
                    ".jpeg",
                    ".gif",
                    ".bmp",
                    ".icon",
                    ".webp",
                    ".flv",
                    ".swf",
                    ".mkv",
                    ".avi",
                    ".rm",
                    ".rmvb",
                    ".mpeg",
                    ".mpg",
                    ".ogg",
                    ".ogv",
                    ".mov",
                    ".wmv",
                    ".mp4",
                    ".webm",
                    ".mp3",
                    ".wav",
                    ".mid",
                    ".rar",
                    ".zip",
                    ".tar",
                    ".gz",
                    ".7z",
                    ".bz2",
                    ".cab",
                    ".iso", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml" };
                this.nAllowSize = 2048 * 1024;
            }
            #endregion



            switch (sAction)
            {
                case "save":
                    {

                        string BasePath = siteProvider.UploadStaticContentPath;
                        string content_build_path = siteProvider.ContentUploadBuildPath;
                        BasePath = Path.Combine(BasePath, content_build_path);
                        if (!Directory.Exists(BasePath))
                        {
                            Directory.CreateDirectory(BasePath);
                        }

                        string file_name = data.Files[0].FileName;
                        string ext = file_name.GetFileExt();
                        file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                        string newfile_yinpin = file_name.ToPinYingGuid();

                        string new_name = newfile_yinpin + ext;
                        while (true)
                        {
                            if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                            {
                                break;
                            }
                            else
                            {
                                new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                            }
                        }
                        // string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                        string save_path = Path.Combine(BasePath, new_name);

                        data.Files[0].SaveAs(save_path);
                        string sOriginalFileName = data.Files[0].FileName;


                        string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                        string s_FormItem = GetHideInputHtml("d_ori", sOriginalFileName) + GetHideInputHtml("d_save", NodePicPath) + GetHideInputHtml("d_thumb", "");
                        string s_Script = "parent.UploadSaved(document.getElementById('d_ori').value, document.getElementById('d_save').value, document.getElementById('d_thumb').value);";


                        string content = $"<html><head><title>eWebEditor</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'></head><body>{s_FormItem}\r\n<script type='text/javascript'>{s_Script}</script></body></html>";
                        return Content(content, "text/html");

                    }
                    break;
                case "local":
                    {
                        string BasePath = siteProvider.UploadStaticContentPath;
                        string content_build_path = siteProvider.ContentUploadBuildPath;
                        BasePath = Path.Combine(BasePath, content_build_path);
                        if (!Directory.Exists(BasePath))
                        {
                            Directory.CreateDirectory(BasePath);
                        }

                        string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                        string save_path = Path.Combine(BasePath, new_name);

                        data.Files[0].SaveAs(save_path);
                        string sOriginalFileName = data.Files[0].FileName;


                        string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;

                        return Content(NodePicPath);
                    }
                    break;
                case "remote":
                    {
                        string text1 = string.Concat(data["eWebEditor_UploadText"], "");

                        string text = System.Uri.UnescapeDataString(text1);
                        text = ReplaceRemoteUrl(text, "gif|jpg|jpeg|bmp|png|webp|ico");

                        string s_FormItem = GetHideInputHtml("d_content", text.HtmlEncode()) +
                            GetHideInputHtml("d_ori", sOriginalFileName) +
                            GetHideInputHtml("d_save", sPathFileName);
                        string s_Script = "parent.setHTML(document.getElementById('d_content').value); try{parent.addUploadFiles(document.getElementById('d_ori').value, document.getElementById('d_save').value);} catch(e){} parent.remoteUploadOK();";
                        return OutScript("uploadremote", s_FormItem, s_Script);
                    }
                    break;
                case "mfu":
                    {
                        var re = InitUpload();
                        if (!re.Item1)
                        {
                            return re.Item2;
                        }
                        return DOMFU1(data);
                    }
                    break;
                default:
                    break;
            }
            return Json(new { });
        }
        

        private int CInt(string str)
        {
            return str.MyToInt();
        }

        private bool IsInt(string str)
        {

            if (str == "")
            {
                return false;
            }

            for (int i = 0; i < str.Length; i++)
            {
                if (!IsNumeric(Mid(str, i, 1)))
                {
                    return false;
                }
            }
            return true;
        }
        public bool IsNumeric(string str)
        {
            System.Text.RegularExpressions.Regex reg1
            = new System.Text.RegularExpressions.Regex(@"^[-]?\d+[.]?\d*$");
            return reg1.IsMatch(str);
        }

        private bool IsOkSParams(string s_SParams, string s_EncryptKey)
        {

            if (s_SParams == "") return false;

            int n = 0; string s1 = ""; string s2 = "";
            n = s_SParams.IndexOf("|");
            if (n < 0) return false;

            s1 = Left(s_SParams, n - 1);
            s2 = Mid(s_SParams, n + 1);

            if (GetMD5(s_EncryptKey + s2, 16) != s1) return false;

            return true;
        }
        private bool IsFileNameFormat(string s_Name)
        {
            bool _IsFileNameFormat = false;
            if (Left(s_Name, 1) == ".") return _IsFileNameFormat;
            if (Regex.IsMatch(s_Name, @"[\/\\\:\*\?\""\<\>\|\r\n\t]+ ")) return _IsFileNameFormat;
            _IsFileNameFormat = true;
            return _IsFileNameFormat;

        }
        private string Trim(string str)
        {
            return str.MyToString().ToUpper();
        }
        private string GetSafeUrl(string str)
        {
            if (Regex.IsMatch(str, @"[\<\>\""\'\;\(\)\?\%\&\r\n\t]+"))
                return "";
            else
                return str;
        }
        #region 大文件上传

        bool _InitUpload = false;
        //string sStyleName = "";
        //string sCusDir = "";
        string sParamSYFlag = "";
        string sParamRnd = "";
        private string sParamBlockFile = "";
        private string sParamBlockFlag = "";
        private string sSpacePath = "";
        private string sBaseUrl = "";
        private int nUploadObject = 0;
        private string sAutoDir = "";
        string sUploadDir = "";
        string sContentPath = "";

        private string sSetContentPath = "";

        int nSLTFlag = 0;
        int nSLTMode = 0;
        int nSLTCheckFlag = 0;
        int nSLTMinSize = 0;
        int nSLTOkSize = 0;
        int nSYWZFlag = 0;
        string sSYText = "";
        string sSYFontColor = "";
        int nSYFontSize = 0;
        string sSYFontName = "";
        string sSYPicPath = "";
        int nSLTSYObject = 0;
        string sSLTSYExt = "";
        int nSYWZMinWidth = 0;
        string sSYShadowColor = "";
        int nSYShadowOffset = 0;


        int nSYWZMinHeight = 0;
        int nSYWZPosition = 0;
        int nSYWZTextWidth = 0;
        int nSYWZTextHeight = 0;
        int nSYWZPaddingH = 0;
        int nSYWZPaddingV = 0;
        int nSYTPFlag = 0;
        int nSYTPMinWidth = 0;
        int nSYTPMinHeight = 0;
        int nSYTPPosition = 0;
        int nSYTPPaddingH = 0;
        int nSYTPPaddingV = 0;
        int nSYTPImageWidth = 0;
        int nSYTPImageHeight = 0;
        float nSYTPOpacity = 0.8f;
        string sSpaceSize = "";

        string sMFUMode = "";
        string sFileNameMode = "";
        string sFileNameSameFix = "";
        string sAutoDirOrderFlag = "";
        string sSYValidNormal = "";
        string sSYValidLocal = "";
        string sSYValidRemote = "";

        string sSaveFileName = "";
        private (bool, IActionResult) InitUpload()
        {
            _InitUpload = false;
            sWSRootUrl = GetSafeUrl(DeCode9193(Request.Query["ws"].MyToString().Trim()));

            sType = Request.Query["type"].MyToString().Trim().ToUpper();
            sStyleName = Trim(Request.Query["style"]);
            sCusDir = Trim(Request.Query["cusdir"]);
            sParamSYFlag = Trim(Request.Query["syflag"]);
            sParamRnd = Trim(Request.Query["rnd"]);

            string s_SKey = "";
            string s_SParams = "";

            s_SKey = Trim(Request.Query["skey"]);
            s_SParams = DeCode9193(Trim(Request.Query["sparams"]));

            sParamBlockFile = Trim(Request.Query["blockfile"]).ToLower() ;
            sParamBlockFlag = Trim(Request.Query["blockflag"]).ToLower();

            if (sParamBlockFile != "")
            {
                if (!IsFileNameFormat(sParamBlockFile))
                {

                    return (_InitUpload, OutError("blockfile"));
                }

            }
            mConfig = new EWebEditorConfig();
            aStyle = mConfig.getConfigArray("Style");
            List<string> aStyleConfig = new List<string>();

            bool bValidStyle = false;


            int num = 0;
            int num2 = aStyle.Count() - 1;


            for (int i = num; i <= num2; i++)
            {
                aStyleConfig = aStyle[i].Split(new string[] { "|||" }, StringSplitOptions.None).ToList();
                if (sStyleName.ToLower() == aStyleConfig[0].ToLower())
                {
                    bValidStyle = true;
                    break;
                }
            }
            if (!bValidStyle)
            {
                return (_InitUpload, OutError("style"));

            }
            if (sWSRootUrl != "" && aStyleConfig[112] == "")
            {
                return (_InitUpload, OutError("ws"));
            }

            if (aStyleConfig[61] != "1")
                sCusDir = "";
            string ss_FileSize = "";
            string ss_FileBrowse = "";
            string ss_SpaceSize = "";
            string ss_SpacePath = "";
            string ss_PathMode = "";
            string ss_PathUpload = "";
            string ss_PathCusDir = "";
            string ss_PathCode = "";
            string ss_PathView = "";
            if ((aStyleConfig[61] == "2") && (s_SKey != ""))
            {
                if (IsOkSParams(s_SParams, aStyleConfig[70]))
                {
                    List<string> a_SParams = new List<string>();
                    a_SParams = s_SParams.Split(',').ToList();
                    ss_FileSize = a_SParams[1];
                    ss_FileBrowse = a_SParams[2];
                    ss_SpaceSize = a_SParams[3];
                    ss_SpacePath = a_SParams[4];
                    ss_PathMode = a_SParams[5];
                    ss_PathUpload = a_SParams[6];
                    ss_PathCusDir = a_SParams[7];
                    ss_PathCode = a_SParams[8];
                    ss_PathView = a_SParams[9];
                }
                else
                {
                    ss_FileSize = GetSAPIvalue(s_SKey, "FileSize");
                    ss_FileBrowse = GetSAPIvalue(s_SKey, "FileBrowse");
                    ss_SpaceSize = GetSAPIvalue(s_SKey, "SpaceSize");
                    ss_SpacePath = GetSAPIvalue(s_SKey, "SpacePath");
                    ss_PathMode = GetSAPIvalue(s_SKey, "PathMode");
                    ss_PathUpload = GetSAPIvalue(s_SKey, "PathUpload");
                    ss_PathCusDir = GetSAPIvalue(s_SKey, "PathCusDir");
                    ss_PathCode = GetSAPIvalue(s_SKey, "PathCode");
                    ss_PathView = GetSAPIvalue(s_SKey, "PathView");
                }

                if (IsNumeric(ss_SpaceSize))
                {
                    aStyleConfig[11] = ss_FileSize;
                    aStyleConfig[12] = ss_FileSize;
                    aStyleConfig[13] = ss_FileSize;
                    aStyleConfig[14] = ss_FileSize;
                    aStyleConfig[15] = ss_FileSize;
                    aStyleConfig[45] = ss_FileSize;
                }
                else
                {
                    ss_FileSize = "";
                }

                if (ss_FileBrowse == "0" || ss_FileBrowse == "1")
                {
                    aStyleConfig[43] = ss_FileBrowse;
                }
                else
                {
                    ss_FileBrowse = "";
                }

                if (IsNumeric(ss_SpaceSize))
                    aStyleConfig[78] = ss_SpaceSize;
                else
                    ss_SpaceSize = "";

                if (ss_PathMode != "")
                    aStyleConfig[19] = ss_PathMode;


                if (ss_PathUpload != "")
                    aStyleConfig[3] = ss_PathUpload;

                if (ss_PathCode != "")
                    aStyleConfig[23] = ss_PathCode;

                if (ss_PathView != "")
                    aStyleConfig[22] = ss_PathView;
                sCusDir = ss_PathCusDir;
                sSpacePath = ss_SpacePath;


            }
            else
            {
                sSpacePath = "";
            }
            sBaseUrl = aStyleConfig[19].Trim();
            nUploadObject = (aStyleConfig[20]).MyToInt();
            sAutoDir = aStyleConfig[71];

            sUploadDir = aStyleConfig[3];
            if (sBaseUrl != "3")
            {
                if (Left(sUploadDir, 1) != "/")
                    sUploadDir = "../" + sUploadDir;

            }
            switch (sBaseUrl)
            {
                case "0":
                case "3":
                    sContentPath = aStyleConfig[23];
                    break;
                case "1":
                    //sContentPath = RelativePath2RootPath(sUploadDir);
                    break;
                case "2":
                    //sContentPath = RootPath2DomainPath(RelativePath2RootPath(sUploadDir));
                    break;
                default:
                    break;
            }

            sSetContentPath = sContentPath;

            if (sBaseUrl != "3")
            {
                //sUploadDir = Server.MapPath(sUploadDir);
            }

            if (Right(sUploadDir, 1) != @"\")
                sUploadDir = sUploadDir + @"\";

            nSLTFlag = CInt(aStyleConfig[29]);
            nSLTMode = CInt(aStyleConfig[69]);
            nSLTCheckFlag = CInt(aStyleConfig[77]);
            nSLTMinSize = CInt(aStyleConfig[30]);
            nSLTOkSize = CInt(aStyleConfig[31]);
            nSYWZFlag = CInt(aStyleConfig[32]);
            sSYText = aStyleConfig[33];
            sSYFontColor = aStyleConfig[34];
            nSYFontSize = CInt(aStyleConfig[35]);
            sSYFontName = aStyleConfig[36];
            sSYPicPath = aStyleConfig[37];
            nSLTSYObject = CInt(aStyleConfig[38]);
            sSLTSYExt = aStyleConfig[39];
            nSYWZMinWidth = CInt(aStyleConfig[40]);
            sSYShadowColor = aStyleConfig[41];
            nSYShadowOffset = CInt(aStyleConfig[42]);
            nSYWZMinHeight = CInt(aStyleConfig[46]);
            nSYWZPosition = CInt(aStyleConfig[47]);
            nSYWZTextWidth = CInt(aStyleConfig[48]);
            nSYWZTextHeight = CInt(aStyleConfig[49]);
            nSYWZPaddingH = CInt(aStyleConfig[50]);
            nSYWZPaddingV = CInt(aStyleConfig[51]);
            nSYTPFlag = CInt(aStyleConfig[52]);
            nSYTPMinWidth = CInt(aStyleConfig[53]);
            nSYTPMinHeight = CInt(aStyleConfig[54]);
            nSYTPPosition = CInt(aStyleConfig[55]);
            nSYTPPaddingH = CInt(aStyleConfig[56]);
            nSYTPPaddingV = CInt(aStyleConfig[57]);
            nSYTPImageWidth = CInt(aStyleConfig[58]);
            nSYTPImageHeight = CInt(aStyleConfig[59]);
            nSYTPOpacity = System.Convert.ToSingle(aStyleConfig[60]);
            sSpaceSize = aStyleConfig[78];
            sMFUMode = aStyleConfig[79];
            sFileNameMode = aStyleConfig[68];
            sFileNameSameFix = aStyleConfig[87];
            sAutoDirOrderFlag = aStyleConfig[88];
            sSYValidNormal = aStyleConfig[99];
            sSYValidLocal = aStyleConfig[100];
            sSYValidRemote = aStyleConfig[101];
            if (((sAction == "save" || sAction == "mfu") && sSYValidNormal != "1") || (sAction == "local" && sSYValidLocal != "1") || (sAction == "remote" && sSYValidRemote != "1"))
            {
                nSYWZFlag = 0;
                nSYTPFlag = 0;
            }
            if (nSYWZFlag == 2)
            {
                if (sParamSYFlag == "1")
                {
                    nSYWZFlag = 1;
                }
                else
                {
                    nSYWZFlag = 0;
                }
            }

            if (nSYTPFlag == 2)
            {
                if (sParamSYFlag == "1")
                {
                    nSYTPFlag = 1;
                }
                else
                {
                    nSYTPFlag = 0;
                }
            }

            if (!IsInt(sParamRnd))
            {
                sParamRnd = "";
            }
            if (sCusDir != "")
                sCusDir = sCusDir.Replace(@"\", "/");
            if (Left(sCusDir, 1) == "/" || sCusDir.IndexOf("//") >= 0 || Regex.IsMatch(sCusDir, @"[\.\;\,\:\*\?\""\|\<\>\r\n\t]+ "))
            {
                sCusDir = "";
            }
            else
            {
                if (Right(sCusDir, 1) != "/")
                    sCusDir = sCusDir + "/";
            }




            return (true, null);
        }
        private string temp_sParamBlockFile = "";
        public IActionResult DOMFU1(IFormCollection data)
        {

            if (sParamBlockFlag == "cancel")
            {
                string DelFilename = "";
                if (sParamBlockFile == "")
                {
                    DelFilename = myCache.GetString(temp_sParamBlockFile);
                }
                else
                {
                    string key = "";
                    if (sParamBlockFile.IndexOf(".") >= 0)
                    {
                        key = sParamBlockFile.Substring(0, sParamBlockFile.LastIndexOf("."));
                    }
                    else
                    {
                        key = sParamBlockFile;
                    }
                    DelFilename = myCache.GetString(key);
                }

           
                DelFile(DelFilename);
                return Content("ok");
            }
            bool b = false;
            var res = DoUpload_ASPDotNet1(data);
            b = res.Item1;
            if (!b)
            {
                return res.Item2;
            }

            if (sParamBlockFlag == "end")
            {
                myCache.Remove(sParamBlockFile);
                return Content(res.Item3 + "::" + res.Item3);
            }
            else
            {
                if (sParamBlockFile != "")
                {
                    if (sParamBlockFile.IndexOf(".") >= 0)
                    {
                        return Content(sParamBlockFile.Substring(0, sParamBlockFile.LastIndexOf(".")));
                    }
                    else {
                        return Content(sParamBlockFile);
                    }
                }
                else
                {
                    return Content(temp_sParamBlockFile);
                }

            }
        }

        private (bool, IActionResult,string) DoUpload_ASPDotNet1(IFormCollection oFiles)
        {
            bool result = false;
            var postedFile = oFiles.Files[0];
            sOriginalFileName = postedFile.FileName;
            if (sOriginalFileName.IndexOf(".") > 0)
            {
                sOriginalFileName = sOriginalFileName.Substring(0, sOriginalFileName.LastIndexOf('.'));
            }

            sOriginalFileName = sOriginalFileName.ToPinYingEWebeditor();
            if (sOriginalFileName == "")
            {
                if (sParamBlockFile == "")
                {
                    sOriginalFileName = temp_sParamBlockFile;
                }
                else {
                    sOriginalFileName = sParamBlockFile;
                }
            }
            string sFileExt = System.IO.Path.GetExtension(postedFile.FileName);
            string BasePath = siteProvider.UploadStaticContentPath;
            string content_build_path = "mfu";
            BasePath = Path.Combine(BasePath, content_build_path, sParamRnd);
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }
            sSaveFileName = sOriginalFileName + sFileExt;
            string save_path = Path.Combine(BasePath, sSaveFileName);
            string str_Mappath = save_path;
            var res = MFU_GetSavePath(str_Mappath);
            if (res.Item1 == "")
            {
                return (false, res.Item2,"");
            }
            postedFile.SaveAs(res.Item1);

            string s_Path = str_Mappath;
            string relative_path = "";
            if (sAction == "mfu")
            {
                if (sParamBlockFile != "")
                {
                    MFU_DoMergeFile_Normal(s_Path);
                    if (sParamBlockFlag == "end")
                    {
                        string content_build_path1 = siteProvider.ContentUploadBuildPath;
                        string BasePath1 = siteProvider.UploadStaticContentPath;
                         BasePath1 = Path.Combine(BasePath1, content_build_path1);
                        if (!Directory.Exists(BasePath1))
                        {
                            Directory.CreateDirectory(BasePath1);
                        }
                        string sOriginalFileName1 = postedFile.FileName;
                        if (sOriginalFileName1.IndexOf(".") > 0)
                        {
                            sOriginalFileName1 = sOriginalFileName1.Substring(0, sOriginalFileName1.LastIndexOf('.'));
                        }
                        sOriginalFileName1 = sOriginalFileName1.ToPinYingEWebeditor();
                        if (sOriginalFileName1 == "")
                        {
                            if (sParamBlockFile == "")
                            {
                                sOriginalFileName1 = temp_sParamBlockFile;
                            }
                            else
                            {
                                sOriginalFileName1 = sParamBlockFile;
                            }
                        }
                        string new_name = sOriginalFileName1 + sFileExt;
                        while (true)
                        {
                            if (!System.IO.File.Exists(Path.Combine(BasePath1, new_name)))
                            {
                                break;
                            }
                            else
                            {
                                new_name = sOriginalFileName1 + "_" + DateTime.Now.GetTimestamp() + sFileExt;
                            }
                        }

                        string move_save_path = Path.Combine(BasePath1, new_name);
                        relative_path = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path1.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                        //rename
                        System.IO.File.Move(string.Concat(s_Path, ".", sParamBlockFile, ".tmp1"), move_save_path);
                    }
                }

            }
            return (true, null, relative_path);
        }
        private void MFU_DoMergeFile_Normal(string base_path)
        {

            string s_File1 = string.Concat(base_path, ".", sParamBlockFile, ".tmp1");
            string s_File2 = string.Concat(base_path, ".", sParamBlockFile, ".tmp2");

            var fs1 = new FileStream(s_File1, FileMode.Append);
            var fw1 = new BinaryWriter(fs1);

            var fs2 = new FileStream(s_File2, FileMode.Open);
            var fw2 = new BinaryReader(fs2);
            fw1.Write(fw2.ReadBytes(fs2.Length.MyToInt()));
            fw2.Close();
            fs2.Close();
            fw1.Close();
            fs1.Close();
            DelFile(s_File2);
        }

        private (string, IActionResult) MFU_GetSavePath(string s_Path)
        {

            string s_Ret = "";
            if (sParamBlockFile == "")
            {
                //new
                if (sParamBlockFlag == "end")
                    s_Ret = s_Path;
                else
                {
                    temp_sParamBlockFile = Guid.NewGuid().ToString("N").ToLower();
                    string SaveFileName= string.Concat(s_Path, ".", temp_sParamBlockFile, ".tmp1");
                    myCache.SetString(temp_sParamBlockFile, SaveFileName);
                    s_Ret = string.Concat(s_Path, ".", temp_sParamBlockFile, ".tmp1");
                }
            }
            else
            {

                if (!System.IO.File.Exists(string.Concat(s_Path, ".", sParamBlockFile, ".tmp1")))
                    return ("", OutError("file"));
                s_Ret = string.Concat(s_Path, ".", sParamBlockFile, ".tmp2");
            }
            return (s_Ret, null);
        }
        private void DelFile(string s_MapFile)
        {
            if (s_MapFile != "")
            {
                try
                {
                    System.IO.File.Delete(s_MapFile);
                }
                catch (Exception)
                {
                }


            }
        }
        #endregion





        string sOriginalFileName = "";
        string sPathFileName = "";
        private string ReplaceRemoteUrl(string sHTML, string sExt)
        {
            sOriginalFileName = "";
            sPathFileName = "";
            //string reg = "((http|https|ftp|rtsp|mms):(\\/\\/|\\\\\\\\){1}(([A-Za-z0-9_-])+[.]){1,}([A-Za-z0-9]{1,5})(:[0-9]+?)?\\/(\\S+\\.(" + sExt + ")))";
            //MatchCollection matchCollection = Regex.Matches(sHTML, reg);

            List<string> array = sHTML.GetImgs().ToList();

            //List<string> array = new List<string>();

            //foreach (object obj in matchCollection)
            //{
            //    Match match = (Match)obj;


            //    array.Add(match.Value.ToString());


            //}

            int num4 = 0;
            int num5 = array.Count() - 1;
            List<string> old_filenames = new List<string>();
            List<string> new_filenames = new List<string>();
            Crawler[] crawlers = array.Select(x => new Crawler(x.HtmlDecode(), siteProvider).Fetch()).ToArray();

            foreach (var item in crawlers)
            {
                sHTML = sHTML.Replace(item.SourceUrl, item.ServerUrl);

                old_filenames.Add(item.SourceUrl);
                new_filenames.Add(item.ServerUrl);
            }
            //for (int i = num4; i <= num5; i++)
            //{
            //    string url_temp = array[i].HtmlDecode();

            //    //Crawler[] crawlers = Sources.Select(x => new Crawler(x, siteProvider, httpContextAccessor.Current.site_id).Fetch()).ToArray();
            //    var request = HttpWebRequest.Create(url_temp) as HttpWebRequest;
            //    using (var response = request.GetResponseAsync().Result as HttpWebResponse)
            //    {
            //        string file_name = array[i].Substring(array[i].LastIndexOf("/"));
            //        string fileExt = Path.GetExtension(file_name).ToLower();
            //        int len = fileExt.LastIndexOf('?');

            //        if (len != -1)
            //        {
            //            file_name = file_name.Substring(0, len);
            //        }
            //        string BasePath = siteProvider.MultiSiteUploadStaticContentPath(httpContextAccessor.Current.site_id);
            //        string content_build_path = siteProvider.ContentUploadBuildPath;
            //        BasePath = Path.Combine(BasePath, content_build_path);

            //        if (!Directory.Exists(BasePath))
            //        {
            //            Directory.CreateDirectory(BasePath);
            //        }


            //        if (len != -1)
            //        {
            //            fileExt = fileExt.Substring(0, len);
            //        }
            //        string new_name = Guid.NewGuid().ToString("N") + fileExt;
            //        string save_path = Path.Combine(BasePath, new_name);

            //        //string BasePath = siteProvider.MultiSiteUploadStaticContentPath(httpContextAccessor.Current.site_id);
            //        string ServerUrl = string.Concat("/", siteProvider.StaticDirectory,"/", httpContextAccessor.Current.site_id, "/" , siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;

            //        try
            //        {
            //            var stream = response.GetResponseStream();
            //            var reader = new BinaryReader(stream);
            //            byte[] bytes;
            //            using (var ms = new MemoryStream())
            //            {
            //                byte[] buffer = new byte[4096];
            //                int count;
            //                while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
            //                {
            //                    ms.Write(buffer, 0, count);
            //                }
            //                bytes = ms.ToArray();
            //            }
            //            System.IO.File.WriteAllBytes(save_path, bytes);
            //            sHTML = sHTML.Replace(array[i], ServerUrl);

            //            old_filenames.Add(file_name);
            //            new_filenames.Add(ServerUrl);

            //        }
            //        catch (Exception e)
            //        {
            //            continue;
            //        }
            //    }
            //}

            sOriginalFileName = string.Join('|', old_filenames);
            sPathFileName = string.Join('|', new_filenames);
            return sHTML;

        }
        string sWSRootUrl = "";
        string sType = "";
        string sStyleName = "";
        string sCusDir = "";
        string nAllowBrowse = "";


        [AllowAnonymous]
        [RequestSizeLimit(100000000)]
        [Route("/Admin/EWebEditor/Browse")]
        [Route("/Admin/aspx/Browse.aspx")]
        public IActionResult Browse([FromServices] IHostingEnvironment _hostingEnvironment, IFormCollection data)
        {
            mConfig = new EWebEditorConfig();
            sWSRootUrl = Request.Query["ws"].MyToString().Trim();
            sType = Request.Query["type"].MyToString().Trim();
            sStyleName = Request.Query["style"].MyToString().Trim();
            sCusDir = Request.Query["cusdir"].MyToString().Trim();

            aStyle = mConfig.getConfigArray("Style");

            string sAction = this.Request.Query["action"].MyToString().ToLower();
            if (sAction == "file")
            {
                return OutData("browsefile", this.GetFileList());
            }
            else if (sAction == "folder")
            {
                return OutData("browsefolder", this.GetFolderList());

            }
            return Json(new { });
        }
        int nTreeIndex = 0;
        private string GetFolderList()
        {
            nTreeIndex = 0;
            string BasePath = siteProvider.SiteWebRootPath;
            string folderTree = this.GetFolderTree(BasePath, "", 1);

            string text = this.sType;
            string text2 = "";
            string text3 = "";
            string text4 = "";
            string text5 = "";
            if (text.ToUpper() == "FILE")
            {
                this.nTreeIndex = 0;
                text2 = this.GetFolderTree(BasePath, "", 1);
            }

            return string.Concat(new string[]
        {
                folderTree,
                "|||",
                text2,
                "|||",
                text3,
                "|||",
                text4,
                "|||",
                text5
        });
        }

        private string GetFolderTree(string s_Dir, string s_Flag, int n_Indent)
        {
            string[] directories = Directory.GetDirectories(s_Dir);
            int length = directories.Length;
            string text = "";
            int num = 0;
            checked
            {
                foreach (string text2 in directories)
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(text2);
                    num++;
                    string text3;
                    if (num < length)
                    {
                        text3 = "0";
                    }
                    else
                    {
                        text3 = "1";
                    }
                    string name = directoryInfo.Name;
                    if (nTreeIndex > 0)
                    {
                        text += "||";
                    }
                    text = string.Concat(new string[]
                    {
                        text,
                        name,
                        "|",
                        n_Indent.ToString(),
                        "|",
                        text3
                    });
                    this.nTreeIndex++;
                    text += this.GetFolderTree(Path.Combine(s_Dir, name), s_Flag, n_Indent + 1);
                }
                if (text == "")
                {
                    text = "0";
                }
                return text;
            }
        }
        private string GetFileList()
        {

            string safeStr = Request.Query["returnflag"].MyToString().Trim();
            string foldertype = Request.Query["foldertype"].MyToString().Trim();
            string dir = Request.Query["dir"].MyToString().Trim();
            List<string> imageManagerAllowFiles = new List<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp", ".ico" };
            if (sType.ToLower() == "media")
            {
                imageManagerAllowFiles = new List<string>() { ".rm", ".flv", ".wmv", ".asf", ".mov", ".mpg", ".mpeg", ".avi", ".mp3", ".mp4", ".wav", ".mid", ".midi", ".ra", ".wma" };
            }
            if (sType.ToLower() == "flash")
            {
                imageManagerAllowFiles = new List<string>() { ".swf", };
            }

            if (sType.ToLower() == "file")
            {
                imageManagerAllowFiles = new List<string>() { ".rar", ".zip", ".pdf", ".doc", ".xls", ".ppt", ".chm", ".hlp" };
            }
            string BasePath = siteProvider.SiteWebRootPath;
            if (dir.StartsWith("/") ||
                           dir.StartsWith(".") ||
                             dir.EndsWith(".") ||
                             dir.IndexOf("./") >= 0 ||
                             dir.IndexOf("/.") >= 0 ||
                             dir.IndexOf("//") >= 0 ||
                             dir.IndexOf("..") >= 0)
            {
                dir = "";
            }
            //相对路径
            string text5 = dir.Replace("\\", "/");
            if (dir != "")
            {
                var post_dir = text5.Split('/');

                if (Directory.Exists(Path.Combine(BasePath, Path.Combine(post_dir))))
                {
                    dir = Path.Combine(BasePath, Path.Combine(post_dir));
                }
                else
                {
                    dir = BasePath;
                }
            }
            string result = "";
            if (!Directory.Exists(BasePath))
            {
                result = string.Concat(new string[]
                    {
                        safeStr,
                        "|||",
                        foldertype,
                        "|||",
                        text5,
                        "|||0"
                    });
            }
            else
            {
                string text6 = "";
                if (dir != "")
                {
                    string[] files = Directory.GetFiles(dir);

                    int num = -1;
                    foreach (string text7 in files)
                    {
                        FileInfo fileInfo = new FileInfo(text7);
                        string name = fileInfo.Name;
                        if (imageManagerAllowFiles.Contains(name.GetFileExt()))
                        {
                            num++;
                            if (num > 0)
                            {
                                text6 += "||";
                            }
                            text6 = string.Concat(new string[]
                            {
                                text6,
                                name,
                                "|",
                                this.GetSizeUnit(fileInfo.Length),
                                "|",
                                fileInfo.LastWriteTime.MyToDateTime()
                            });
                        }
                    }

                    if (text6 == "")
                    {
                        text6 = "0";
                    }
                    text6 = string.Concat(new string[]
                        {
                        safeStr,
                        "|||",
                        foldertype,
                        "|||",
                        text5,
                        "|||",
                        text6
                        });
                }
                result = text6;
            }
            return result;
        }

        private string GetSizeUnit(long n_Size)
        {
            return (n_Size / 1024).ToString("G18") + "K";
        }
        public IActionResult OutError(string s_ErrCode)
        {
            return OutData("browseerr", s_ErrCode);
        }
        private IActionResult OutData(string s_Action, string s_Data)
        {
            string hideInputHtml = GetHideInputHtml("d_data", s_Data);

            string obj = "var d=document.getElementById(\"d_data\").value; ";
            if (s_Action == "browsefile")
            {
                obj = obj + "parent.setFileList(d);";
            }
            else if (s_Action == "browsefolder")
            {
                obj = obj + "parent.setFolderList(d);";
            }
            else if (s_Action == "browseerr")
            {
                obj = obj + "parent.ServerError(d);";
            }

            return OutScript(s_Action, hideInputHtml, obj);
        }


        // Token: 0x06000051 RID: 81 RVA: 0x0000285C File Offset: 0x0000185C
        public IActionResult OutScript(string s_Action, string s_FormItem, string s_Script)
        {
            string text = "";
            string text2 = "";
            if (sWSRootUrl != "")
            {
                text = string.Concat(new string[]
                {
                    "<form name=\"myform\" action=\"",
                    sWSRootUrl,
                    "aspx/fs.aspx?act=",
                    s_Action,
                    "\" method=\"post\" target=\"_self\">"
                });
                text2 = "</form>";
                s_Script = "document.myform.submit();";
            }


            return Content(string.Concat(new string[]
            {
                "<html><head><title>eWebEditor</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=gb2312\">\r\n<script type=\"text/javascript\">\r\nwindow.onload = _Onload; var bRun = false; function _Onload(){if(bRun){return;}; bRun = true; ",
                s_Script,
                "}\r\n</script>\r\n</head><body>\r\n",
                text,
                "\r\n",
                s_FormItem,
                "\r\n",
                text2,
                "\r\n</body></html>"
            }), "text/html; charset=gb2312");

        }


        List<string> aStyle = new List<string>();
        List<string> aToolbar = new List<string>();



        public string GetSAPIvalue(string s_SessionKey, string s_ParamName)
        {
            return httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.GetString("eWebEditor_" + s_SessionKey + "_" + s_ParamName);
        }
        public string GetMD5(string s, int n_Len)
        {
            string text = "gb2312";
            MD5 md = new MD5CryptoServiceProvider();
            byte[] array = md.ComputeHash(Encoding.GetEncoding(text).GetBytes(s));
            StringBuilder stringBuilder = new StringBuilder(32);
            int num = 0;
            checked
            {
                int num2 = array.Length - 1;
                for (int i = num; i <= num2; i++)
                {
                    stringBuilder.Append(array[i].ToString("x").PadLeft(2, '0'));
                }
                if (n_Len == 16)
                {
                    return stringBuilder.ToString().ToLower().Substring(8, 16);
                }
                return stringBuilder.ToString().ToLower();
            }
        }
        // eWebEditorServer.i_aspx
        // Token: 0x0600003D RID: 61 RVA: 0x00003794 File Offset: 0x00002794
        private ActionResult ShowConfig()
        {

            aStyle = mConfig.getConfigArray("Style");
            aToolbar = mConfig.getConfigArray("Toolbar");
            sLicense = mConfig.getConfigString("License");
            string configString = mConfig.getConfigString("WordEqVersion");
            string domain = GetDomain();
            string text = "ok";
            //if (this.CheckLicense(domain))
            //{
            //    text = "";
            //}
            //else
            //{
            //    text = "";
            //}
            string text2 = this.Request.Query["style"].MyToString();
            bool flag = false;
            int num = 0;
            int num2 = aStyle.Count() - 1;

            List<string> array = new List<string>();
            int num3 = 0;
            for (int i = num; i <= num2; i++)
            {
                array = aStyle[i].Split(new string[] { "|||" }, StringSplitOptions.None).ToList();

                if (array[0].Trim().ToLower() == text2.Trim().ToLower())
                {
                    num3 = i;
                    flag = true;
                    break;
                }
            }
            if (!flag)
            {
                return Content("");
            }
            string text3 = this.Request.Query["skey"].MyToString();
            string text4 = "";
            string text5 = "";
            if (array[61] == "2" && text3 != "")
            {
                string text6 = GetSAPIvalue(text3, "FileSize");
                string text7 = GetSAPIvalue(text3, "FileBrowse");
                string text8 = GetSAPIvalue(text3, "SpaceSize");
                string sapivalue = GetSAPIvalue(text3, "SpacePath");
                string sapivalue2 = GetSAPIvalue(text3, "PathMode");
                string sapivalue3 = GetSAPIvalue(text3, "PathUpload");
                text5 = GetSAPIvalue(text3, "PathCusDir");
                string sapivalue4 = GetSAPIvalue(text3, "PathCode");
                string sapivalue5 = GetSAPIvalue(text3, "PathView");
                if (text6.MyToInt() > 0)
                {
                    array[11] = text6;
                    array[12] = text6;
                    array[13] = text6;
                    array[14] = text6;
                    array[15] = text6;
                    array[45] = text6;
                }
                else
                {
                    text6 = "";
                }
                if (text7 == "0" || text7 == "1")
                {
                    array[43] = text7;
                }

                else
                {
                    text7 = "";
                }
                if (text8.MyToInt() > 0)
                {
                    array[78] = text8;
                }
                else
                {
                    text8 = "";
                }
                if (sapivalue2 == "")
                {
                    array[19] = sapivalue2;
                }
                if (sapivalue3 != "")
                {
                    array[3] = sapivalue3;
                }
                if (sapivalue4 != "")
                {
                    array[23] = sapivalue4;
                }
                if (sapivalue5 != "")
                {
                    array[22] = sapivalue5;
                }
                text4 = string.Concat(new string[]
                {
                text6,
                ",",
                text7,
                ",",
                text8,
                ",",
                sapivalue,
                ",",
                sapivalue2,
                ",",
                sapivalue3,
                ",",
                text5,
                ",",
                sapivalue4,
                ",",
                sapivalue5
                });
                text4 = GetMD5(array[70] + text4, 16) + "," + text4;
            }
            object obj = array[112];

            string text9;
            if (array[114] == "2")
            {
                text9 = "";
            }
            else
            {
                text9 = array[114];
            }
            string text10 = "";
            text10 = text10 + "config.FixWidth = \"" + array[1] + "\";\r\n";
            if (array[19] == "3")
            {
                text10 = text10 + "config.UploadUrl = \"" + "/upload/" /*array[23]*/ + "\";\r\n";
            }
            else
            {
                text10 = text10 + "config.UploadUrl = \"" + "/upload/" /*array[3]*/ + "\";\r\n";
            }
            text10 = text10 + "config.InitMode = \"" + array[18] + "\";\r\n";
            text10 = text10 + "config.AutoDetectPaste = \"" + array[17] + "\";\r\n";
            text10 = text10 + "config.BaseUrl = \"" + array[19] + "\";\r\n";
            text10 = text10 + "config.BaseHref = \"" + array[22] + "\";\r\n";
            text10 = text10 + "config.AutoRemote = \"" + array[24] + "\";\r\n";
            text10 = text10 + "config.ShowBorder = \"" + array[25] + "\";\r\n";
            text10 = text10 + "config.StateFlag = \"" + array[16] + "\";\r\n";
            text10 = text10 + "config.SBCode = \"" + array[62] + "\";\r\n";
            text10 = text10 + "config.SBEdit = \"" + array[63] + "\";\r\n";
            text10 = text10 + "config.SBText = \"" + array[64] + "\";\r\n";
            text10 = text10 + "config.SBView = \"" + array[65] + "\";\r\n";
            text10 = text10 + "config.SBSize = \"" + array[76] + "\";\r\n";
            text10 = text10 + "config.EnterMode = \"" + array[66] + "\";\r\n";
            text10 = text10 + "config.Skin = \"" + array[2] + "\";\r\n";
            text10 = text10 + "config.AllowBrowse = \"" + array[43] + "\";\r\n";
            text10 = text10 + "config.AllowImageSize = \"" + 20 * 1024/*array[13]*/ + "\";\r\n";
            text10 = text10 + "config.AllowFlashSize = \"" + 20 * 1024/*array[12]*/ + "\";\r\n";
            text10 = text10 + "config.AllowMediaSize = \"" + 2048 * 1024/*array[14]*/ + "\";\r\n";
            text10 = text10 + "config.AllowFileSize = \"" + 2048 * 1024/*array[11]*/ + "\";\r\n";
            text10 = text10 + "config.AllowRemoteSize = \"" + 20 * 1024/*array[15]*/ + "\";\r\n";
            text10 = text10 + "config.AllowLocalSize = \"" + 20 * 1024/*array[45]*/ + "\";\r\n";
            text10 = text10 + "config.AllowImageExt = \"" + "gif|jpg|jpeg|bmp|png|webp|ico"/*array[8]*/ + "\";\r\n";
            text10 = text10 + "config.AllowFlashExt = \"" + array[7] + "\";\r\n";
            text10 = text10 + "config.AllowMediaExt = \"" + array[9] + "\";\r\n";
            text10 = text10 + "config.AllowFileExt = \"" + array[6] + "\";\r\n";
            text10 = text10 + "config.AllowRemoteExt = \"" + "gif|jpg|jpeg|bmp|png|webp|ico"/* array[10]*/ + "\";\r\n";
            text10 = text10 + "config.AllowLocalExt = \"" + "gif|jpg|jpeg|bmp|png|webp|ico"/*array[44]*/ + "\";\r\n";
            text10 = text10 + "config.AreaCssMode = \"" + array[67] + "\";\r\n";
            text10 = text10 + "config.SLTFlag = \"" + array[29] + "\";\r\n";
            text10 = text10 + "config.SLTMinSize = \"" + array[30] + "\";\r\n";
            text10 = text10 + "config.SLTOkSize = \"" + array[31] + "\";\r\n";
            text10 = text10 + "config.SLTMode = \"" + array[69] + "\";\r\n";
            text10 = text10 + "config.SLTCheckFlag = \"" + array[77] + "\";\r\n";
            text10 = text10 + "config.SYWZFlag = \"" + array[32] + "\";\r\n";
            text10 = text10 + "config.SYTPFlag = \"" + array[52] + "\";\r\n";
            text10 = text10 + "config.FileNameMode = \"" + array[68] + "\";\r\n";
            text10 = text10 + "config.PaginationMode = \"" + array[72] + "\";\r\n";
            text10 = text10 + "config.PaginationKey = \"" + array[73] + "\";\r\n";
            text10 = text10 + "config.PaginationAutoFlag = \"" + array[74] + "\";\r\n";
            text10 = text10 + "config.PaginationAutoNum = \"" + array[75] + "\";\r\n";
            text10 = text10 + "config.SParams = \"" + text4.Replace("\\", "\\\\") + "\";\r\n";
            text10 = text10 + "config.SpaceSize = \"" + array[78] + "\";\r\n";
            text10 = text10 + "config.MFUBlockSize = \"" + array[80] + "\";\r\n";
            text10 = text10 + "config.MFUEnable = \"" + array[81] + "\";\r\n";
            text10 = text10 + "config.CodeFormat = \"" + array[82] + "\";\r\n";
            text10 = text10 + "config.TB2Flag = \"" + array[83] + "\";\r\n";
            text10 = text10 + "config.TB2Code = \"" + array[84] + "\";\r\n";
            text10 = text10 + "config.TB2Max = \"" + array[85] + "\";\r\n";
            text10 = text10 + "config.ShowBlock = \"" + array[86] + "\";\r\n";
            text10 = text10 + "config.WIIMode = \"" + array[95] + "\";\r\n";
            text10 = text10 + "config.QFIFontName = \"" + array[96] + "\";\r\n";
            text10 = text10 + "config.QFIFontSize = \"" + array[97] + "\";\r\n";
            text10 = text10 + "config.UIMinHeight = \"" + array[98] + "\";\r\n";
            text10 = text10 + "config.SYVNormal = \"" + array[99] + "\";\r\n";
            text10 = text10 + "config.SYVLocal = \"" + array[100] + "\";\r\n";
            text10 = text10 + "config.SYVRemote = \"" + array[101] + "\";\r\n";
            text10 = text10 + "config.AutoDonePasteWord = \"" + array[102] + "\";\r\n";
            text10 = text10 + "config.AutoDonePasteExcel = \"" + array[103] + "\";\r\n";
            text10 = text10 + "config.AutoDoneQuickFormat = \"" + array[104] + "\";\r\n";
            text10 = text10 + "config.WIAPI = \"" + array[105] + "\";\r\n";
            text10 = text10 + "config.AutoDonePasteOption = \"" + array[106] + "\";\r\n";
            text10 = text10 + "config.TB2Edit = \"" + array[107] + "\";\r\n";
            text10 = text10 + "config.TB2Text = \"" + array[108] + "\";\r\n";
            text10 = text10 + "config.TB2View = \"" + array[109] + "\";\r\n";
            text10 = text10 + "config.Cookie = \"" + text9 + "\";\r\n";
            text10 = text10 + "config.CertIssuer = \"\";\r\n";
            text10 = text10 + "config.CertSubject = \"\";\r\n";
            text10 = text10 + "config.WordEqVersion = \"" + "2.2.0.0.t"/*configString*/ + "\";\r\n";
            text10 = text10 + "config.WordEqImport = \"" + array[110] + "\";\r\n";
            text10 = text10 + "config.ResMovePath = \"" + array[111] + "\";\r\n";
            text10 = text10 + "config.FSPath = \"\";\r\n";
            text10 = text10 + "config.FSL = \"\";\r\n";
            text10 = text10 + "config.UseProxy = \"" + array[113] + "\";\r\n";
            text10 = text10 + "config.L = \"" + text + "\";\r\n";
            text10 += "config.ServerExt = \"aspx\";\r\n";
            text10 += "config.UploadPath = \"/admin/EWebEditor/Upload\";\r\n";
            text10 += "config.BrowsePath = \"/admin/EWebEditor/Browse\";\r\n";
            text10 += "config.Charset = \"gb2312\";\r\n";
            if (text5 == "")
            {
                text10 = text10 + "config.CusDir = \"" + text5 + "\";\r\n";
            }
            text10 += "\r\n";
            text10 += "config.Toolbars = [\r\n";
            string text11 = "";
            string text12 = "";
            int num4 = 0;
            int num5 = aToolbar.Count() - 1;
            for (int j = num4; j <= num5; j++)
            {
                if (aToolbar[j] != "")
                {
                    List<string> array2 = aToolbar[j].Split(new string[] { "|||" }, StringSplitOptions.None).ToList();

                    if (array2[0].MyToInt() == num3)
                    {
                        if (text12 != "")
                        {
                            text12 += "|";
                            text11 += "|";
                        }
                        text12 += j;
                        text11 += array2[3];
                    }
                }
            }
            if (text12 != "")
            {
                string[] array3 = text12.Split('|');

                string[] array4 = text11.Split('|');
                int num6 = 0;
                int num7 = array3.Length - 1;
                for (int j = num6; j <= num7; j++)
                {
                    List<string> array2 = aToolbar[array3[j].MyToInt()].Split(new string[] { "|||" }, StringSplitOptions.None).ToList();

                    string[] array5 = array2[1].Split('|');
                    if (j > 0)
                    {
                        text10 += ",\r\n";
                    }
                    text10 = text10 + "\t" + "[";
                    int num8 = 0;
                    int num9 = array5.Length - 1;

                    for (int i = num8; i <= num9; i++)
                    {
                        if (i > 0)
                        {
                            text10 += ", ";
                        }
                        text10 = text10 + "\"" + array5[i] + "\"";
                    }
                    text10 += "]";
                }
            }
            text10 += "\r\n];\r\n";
            return Content(text10, "application/x-javascript");


        }

        private bool CheckLicense(string s_Domain)
        {
            bool result = false;
            checked
            {
                if (s_Domain.Contains("127.0.0.1") || s_Domain.Contains("localhost") || s_Domain.Contains("192.168."))
                {
                    result = true;
                }
                else if (sLicense != "")
                {
                    string[] array = sLicense.Split(':');

                    foreach (var item in array)
                    {
                        List<string> sub_items = new List<string>();
                        if (item.IndexOf(",") >= 0)
                            sub_items = item.Split(',').ToList();
                        else
                            sub_items = item.Split(':').ToList();
                        if (sub_items.Count() == 7)
                        {
                            if (sub_items[7].MyToInt() == 32)
                            {
                                switch (sub_items[0])
                                {
                                    case "3":
                                        result = true;
                                        break;
                                    default:
                                        result = true;
                                        break;
                                }
                            }
                        }
                    }
                }
                return result;
            }
        }


        private string GetDomain()
        {
            string text = DeCode9193(Request.Query["h"].MyToString().ToLower());
            if (text.Trim() == "")
            {
                text = httpContextAccessor.Current.HttpContextAccessor.ServerName();
                if (text.IndexOf(":") > 0 & !text.StartsWith("["))
                {
                    text = "[" + text + "]";
                }
            }
            return text;
        }
        public static string DeCode9193(string s)
        {
            string text = s.Replace(";91;", "[");
            text = text.Replace(";93;", "]");
            return text + "";
        }
    }
}
