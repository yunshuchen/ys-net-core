﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Authorize;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace YS.NetCore.Areas.Admin.Controllers
{
    public class RolesController : Net.Mvc.BasicController<sys_role_entity, ISysRoleService>
    {
        private readonly IApplicationContextAccessor applicationContextAccessor;
        private ISysRoleOperateRelationService role_config;
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;


        private ISysModuleService module;
        private ISysResourceService resource;
        private ISysOperateService Operate;
        public RolesController(ISysRoleService userService,
            ISysRoleOperateRelationService _role_config,
            ISysResourceService _resource,
            ISysOperateService _Operate,
            ISysModuleService _module,
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> _role_operate_cacheManager,
        IApplicationContextAccessor _applicationContextAccessor) : base(userService, _applicationContextAccessor)
        {
       
            module = _module;
            role_config = _role_config;
            resource = _resource;
            Operate = _Operate;
            applicationContextAccessor = _applicationContextAccessor;

            //角色操作关系表
            role_operate_cahce = _role_operate_cacheManager.GetOrAdd(Net.SessionKeyProvider.SysRoleOperateRelation, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles")]
        // GET: Platform/AdminRole
        public ActionResult Index()
        {

            return View();

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles",operate_code = "add,modify")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddRole(string id)
        {

            sys_role_entity model = new sys_role_entity();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                model = Service.GetByOne(a => a.id == id.MyToLong());

            }
            return View(model);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "add,modify")]
        /// <summary>
        /// 设置角色权限控制器
        /// </summary>
        /// <param name="roleid"></param>
        /// <returns></returns>
        public ActionResult RoleOpSetting(string roleid)
        {

            if (roleid.MyToLong() <= 0)
            {
                return new EmptyResult();
            }

            //
            var configList = role_config.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                a=>a.role_id==roleid.MyToLong()
            });
            var module_list=module.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>() {


            a=>a.delete_status==DeleteEnum.UnDelete
            });
            var resList = resource.GetList("n_sort asc,id asc", new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>() {
                a=>a.delete_status==DeleteEnum.UnDelete&&a.res_code.StartsWith("cms_node_g_")==false&&a.res_code.StartsWith("cms_content_g_")==false
            });

            var OpList = Operate.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>>() {
            a=>a.delete_status==DeleteEnum.UnDelete
            });
            List<object> result = new List<object>();

            foreach (var module_item in module_list)
            {
                List<object> res_list = new List<object>();

                var _resList = resList.Where(a => a.p_module == module_item.id).ToList();
                foreach (var item in _resList)
                {
                    var Operat = OpList.Where(a => a.p_res == item.id).ToList().Select(a =>
                    {
                        return new
                        {
                            Op_Code = a.operate_code,
                            Op_Name = a.operate_name,
                            Checked = configList.Where(b => b.res_code == a.p_res_code && b.operate_code == a.operate_code&&b.module_code==module_item.module_code).Count()
                        };
                    });
                    var res = new
                    {
                        Res_Code = item.res_code,
                        Res_Name = item.res_name,
                        Op_List = Operat
                    };
                    res_list.Add(res);
                }

                var module = new
                {
                    Module_Code = module_item.module_code,
                    Module_Name = module_item.chr_name,
                    Res_List = res_list
                };
                result.Add(module);
            }
           
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { roleid = roleid, list = result })));
        }
        class OpClass
        {

            public long id { get; set; }
            public long pid { get; set; }
            public string name { get; set; }
            public string module_code { get; set; }
            public string res_code { get; set; }
            public string op_code { get; set; }
            public int check { get; set; }

            public List<OpClass> operate { get; set; }

            public List<OpClass> children { get; set; }
        }
        public JsonResult GetNodeContentOperate([FromServices] ISiteNodeService node, long roleid, string type)
        {
            var result = node.GetAllLevelData(new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.lang=="sc"
            });
            var configList = role_config.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                a=>a.role_id==roleid.MyToLong()
            });
            List<OpClass> list = new List<OpClass>();

            var node_codes = type == "node" ? new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
            } : new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
            long before = type == "node" ? 10000 : 20000;

            foreach (var item in result)
            {
                var res_one = new OpClass();
                res_one.id = before+ item.id;
                res_one.module_code = $"site_{type}";
                res_one.res_code = $"cms_{type}_g_{item.id}";
                res_one.op_code = "";
                res_one.name = item.title;
                res_one.pid = 0;
                res_one.operate = new List<OpClass>();
                res_one.check = 0;
                res_one.children = new List<OpClass>();
                foreach (var op in node_codes)
                {
                    res_one.operate.Add(new OpClass
                    {
                        id = item.id,
                        module_code = $"site_{type}",
                        res_code = $"cms_{type}_g_{item.id}",
                        op_code = op.Key,
                        name = op.Value,
                        pid = 0,
                        check = configList.Where(b => b.res_code == $"cms_{type}_g_{item.id}" && b.operate_code == op.Key && b.module_code == $"site_{type}").Count()
                    });

                }
                ProcssChildren(res_one, item, node_codes, configList, type);

                list.Add(res_one);
            }

            return Json(list);
        }

        private void ProcssChildren(OpClass parent, SiteNodesTree currnt_nodes, Dictionary<string, string> node_codes, List<sys_role_operate_relation_entity> configList, string type)
        {
            long before = type == "node" ? 10000 : 20000;
            foreach (var item in currnt_nodes.children)
            {
                var res_one = new OpClass();
                res_one.id = before + item.id;
                res_one.module_code = $"site_{type}";
                res_one.res_code = $"cms_{type}_g_{item.id}";
                res_one.op_code = "";
                res_one.name = item.title;
                res_one.pid = before + item.pid;
                res_one.operate = new List<OpClass>();
                res_one.check = 0;
                res_one.children = new List<OpClass>();
                foreach (var op in node_codes)
                {
                    res_one.operate.Add(new OpClass
                    {
                        id = item.id,
                        module_code = $"site_{type}",
                        res_code = $"cms_{type}_g_{item.id}",
                        op_code = op.Key,
                        name = op.Value,
                        pid = 0,
                        check = configList.Where(b => b.res_code == $"cms_{type}_g_{item.id}" && b.operate_code == op.Key && b.module_code == $"site_{type}").Count()
                    });

                }
                ProcssChildren(res_one, item, node_codes, configList, type);

                parent.children.Add(res_one);
            }
        }
        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "add,modify")]
        public JsonResult Save(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            string description = data["description"].MyToString();
            
            int status = data["status"].MyToInt();
            int is_sys = data["is_sys"].MyToInt();
            if (chr_name == "")
            {
                return Json(new { error = -1, msg = "参数错误" });
            }
            if (status != 1 && status != 0)
            {
                return Json(new { error = -1, msg = "参数错误" });
            }
            if (id <= 0)
            {
                var user = applicationContextAccessor.Current.CurrentUser;
                Service.Insert(new sys_role_entity()
                {

                    role_name = chr_name,
                    status = (StatusEnum)status,
                    is_sys = (StatusEnum)is_sys,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now
                });

                AddLog("sys_roles", LogsOperateTypeEnum.Add, $"新增角色:{chr_name}");
            }
            else
            {
                var user = applicationContextAccessor.Current.CurrentUser;
                Service.Modify(a => a.id == id, a => new sys_role_entity()
                {
                    role_name = chr_name,
                    status = (StatusEnum)status,
                    is_sys = (StatusEnum)is_sys,
                    description = description,

                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now


                });

                AddLog("sys_roles", LogsOperateTypeEnum.Update, $"修改角色:{chr_name}");
            }

            return Json(new { error = 1, msg = "保存成功" });

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {

            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { error = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { error = -1, msg = "参数错误" });
            }

            Service.Modify(a => a.id == id, a => new sys_role_entity()
            {
                status = (StatusEnum)status
            });

            AddLog("sys_roles", LogsOperateTypeEnum.Update, $"修改角色状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
            return Json(new { result = 0, msg = "操作成功" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "add,modify")]
        [HttpPost]
        public JsonResult SaveOp(IFormCollection data)
        {

            if (data["role_id"].MyToLong() <= 0)
            {
                return Json(new { result = false, msg = "参数错误" });
            }

            long Role_id = data["role_id"].MyToLong();

            var  model=Service.GetByOne(a => a.id == Role_id);
            string ModuleOpCodes = data["mc"].MyToString().Trim();
            if (ModuleOpCodes.Length <= 0)
            {
                return Json(new { result = false, msg = "请选择具体操作权限" });
            }


            role_config.Delete(a => a.role_id == Role_id);


            List<sys_role_operate_relation_entity> list = new List<sys_role_operate_relation_entity>();
            var Modules = ModuleOpCodes.Split(',');
            foreach (var item in Modules)
            {
                string[] MO = item.Split('|');
                if (MO.Length == 3)
                {
                    list.Add(new sys_role_operate_relation_entity()
                    {
                        role_id = Role_id,
                        module_code = MO[0],
                        res_code = MO[1],
                        operate_code = MO[2]
                    });
                }

            }
            role_config.BulkInsert(list);



            var new_list = role_config.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                         c=>c.role_id==Role_id
                    });
            role_operate_cahce.TryUpdate(Role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return new_list; });


            AddLog("sys_roles", LogsOperateTypeEnum.Update, $"设置角色权限:{model.role_name}");
            return Json(new { result = true, msg = "设置成功" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "delete")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { error = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;
                role_config.Delete(a => a.role_id == id);
                if (deleted)
                {
                    List<sys_role_operate_relation_entity> cache_list = null;
                    role_operate_cahce.TryRemove(id.ToString(), out cache_list);

                    AddLog("sys_roles", LogsOperateTypeEnum.Delete, $"删除角色:{chr_name}");
                    return Json(new { error = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { error = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { error = false, msg = ex.Message });
            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_roles", resource_code = "sys_roles")]
        [HttpPost]
        public PartialViewResult gv_RoleList(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();


            List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>();
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.role_name.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_RoleList", list);

        }


    }
}