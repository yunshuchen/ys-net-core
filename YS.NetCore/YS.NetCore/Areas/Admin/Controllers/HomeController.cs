﻿
using System;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Models;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{

    public class HomeController : Net.Mvc.BasicController<sys_account, ISysAccountService>
    {
        private readonly IApplicationContext app_content;
        public HomeController(ISysAccountService service, IApplicationContext _app_conte, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {
            app_content = _app_conte;
        }
        [DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Upload()
        {
            return View();
        }
        [DefaultAuthorize]
        [HttpPost]
        public JsonResult Refresh([FromServices]IOnlineUserCache online_user_cache, [FromServices]IHttpContextAccessor httpContextAccessor)
        {

           var currentUser= httpContextAccessor.HttpContext.Session.Get<sys_account>(Net.SessionKeyProvider.AdminLoginLoginUser);
            //用户不存在
            if (currentUser == null || currentUser.id <= 0 || currentUser.token.MyToString() == "")
            {
                return Json(new { code = -2 });
            }
            string cache_key = string.Format("#{0}#{1}#", currentUser.token, currentUser.id);
            //更新缓存
            var user = online_user_cache.GetOnlineUser(cache_key);
            var last_refresh_s = user.last_refresh.MyToString();
            var last_refresh = DateTime.MinValue;
            DateTime.TryParse(last_refresh_s, out last_refresh);
            DateTime Now = DateTime.Now;
            //已经60秒没有刷新了
            if ((Now - last_refresh).TotalSeconds >= 60)
            {
                return Json(new { code = -2 });
            }
            var put_out_login_s = user.put_out_login.MyToString();

            var put_out_login = DateTime.MinValue;
            DateTime.TryParse(put_out_login_s, out put_out_login);
            if (Now >= put_out_login)
            {
                return Json(new { code = -2 });
            }
            user.last_refresh = Now;
            online_user_cache.UpdateOnlineUser(cache_key, user);
            return Json(new { code = 0 });
        }
    }
}