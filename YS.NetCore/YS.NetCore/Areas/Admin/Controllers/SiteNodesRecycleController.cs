﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteNodesRecycleController : Net.Mvc.BasicController<site_node, ISiteNodeService>
    {

        public SiteNodesRecycleController(ISiteNodeService service, IApplicationContextAccessor _httpContextAccessor) : base(service,_httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_node_recycle", resource_code = "site_node_recycle", operate_code = "review")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {
            site_node model = new site_node();
            site_node sc_model = new site_node();
            site_node en_model = new site_node();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null)
            {
                model = new site_node();
            }
            if (sc_model == null)
            {
                sc_model = new site_node();
            }
            if (en_model == null)
            {
                en_model = new site_node();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_node_recycle", resource_code = "site_node_recycle", operate_code = "view")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(string id)
        {
            site_node model = new site_node();
            site_node sc_model = new site_node();
            site_node en_model = new site_node();

            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null)
            {
                model = new site_node();
            }
            if (sc_model == null)
            {
                sc_model = new site_node();
            }
            if (en_model == null)
            {
                en_model = new site_node();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }



        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            List<System.Linq.Expressions.Expression<Func<site_node, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>();
            condition.Add(a => a.lang == "sc"&&a.del_flag==DeleteEnum.Delete);
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.node_name.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc,create_time desc", condition);
            return PartialView("gv_List", list);
        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_node_recycle", resource_code = "site_node_recycle", operate_code = "delete")]
        public JsonResult Delete([FromServices]ISysRoleOperateRelationService role_operate
            ,[FromServices] Net.MyLucene.ISearchService searcher
            , [FromServices]ISysResourceService sys_resource
            , [FromServices]ISysOperateService sys_operate

            , IFormCollection data)
        {


            long Id = data["Id"].MyToLong();

            long parent_node = data["node_id"].MyToLong();

            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string NodeName = data["chr_name"].MyToString();
            var check = Service.GetList("", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.parent_node==Id
            });




            if (check.Count() > 0)
            {
                return Json(new { code = -1, msg = "存在下级栏目无法删除" });
            }
            var model = Service.Get("", a => a.id == Id&&a.lang=="sc");
            int flag = Service.Delete(a => a.id == Id);
            if (flag > 0)
            {
                ////更新同一级之下排序号
                Service.Modify(a => a.parent_node == model.parent_node && a.nsort > model.nsort, a => new site_node()
                {
                    nsort = a.nsort + 1
                });
                //删除相关权限
                sys_operate.Delete(a => a.p_module == 5 && a.p_res_code == $"cms_node_g_{Id}");
                sys_resource.Delete(a => a.p_module == 5 && a.res_code == $"cms_node_g_{Id}");
                sys_operate.Delete(a => a.p_module == 6 && a.p_res_code == $"cms_content_g_{Id}");
                sys_resource.Delete(a => a.p_module == 6 && a.res_code == $"cms_content_g_{Id}");
                role_operate.Delete(a => a.res_code == $"cms_content_g_{Id}");

                searcher.DeleteNodeIndex(model);
                AddLog("site_node_recycle", Utils.LogsOperateTypeEnum.Delete, $"彻底删除栏目:{NodeName}");

                return Json(new { code = 0, msg = "删除成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "删除失败" });
            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_node_recycle", resource_code = "site_node_recycle", operate_code = "recovery")]
        public JsonResult Recovery(IFormCollection data)
        {
            long Id = data["Id"].MyToLong();
            long parent_node = data["node_id"].MyToLong();
            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string NodeName = data["chr_name"].MyToString();
            int flag = Service.Modify(a => a.id == Id, a => new site_node() { del_flag = DeleteEnum.UnDelete });
            if (flag > 0)
            {
                AddLog("site_node_recycle", Utils.LogsOperateTypeEnum.Recovery, $"恢复放入回收站的栏目:{NodeName}");
                return Json(new { code = 0, msg = "恢复成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "恢复失败" });
            }
        }


        


    }
}