﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteOtherController : Net.Mvc.BasicController<site_other, ISiteOtherService>
    {

        public SiteOtherController(ISiteOtherService service, IApplicationContextAccessor _httpContextAccessor) : base(service,_httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_other", resource_code = "site_other", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {

            site_other tc_model = new site_other();
            site_other sc_model = new site_other();
            site_other en_model = new site_other();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_other();
            }
            if (sc_model == null)
            {
                sc_model = new site_other();
            }
            if (en_model == null)
            {
                en_model = new site_other();
            }
            
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public  JsonResult Save(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string chr_code = data["chr_code"].MyToString();
            string chr_title = data["chr_title"].MyToString();
            StatusEnum is_show =(StatusEnum)data["is_show"].MyToInt();
            string tc_content = data["tc_content"].MyToString();
            string sc_content = data["sc_content"].MyToString();
            string en_content = data["en_content"].MyToString();


            string begin_time = data["begin_time"].MyToString();
            string end_time = data["end_time"].MyToString();
            StatusEnum off_status = (StatusEnum)data["off_status"].MyToInt();


            if (chr_code.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (chr_title.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (sc_content == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (off_status == StatusEnum.Legal)
            {
                if (begin_time == "")
                {

                    return Json(new { code = -1, msg = "有效开始时间不能为空" });
                }
                if (end_time == "")
                {
                    return Json(new { code = -1, msg = "有效结束时间不能为空" });
                }
            }
            if (id <= 0)
            {
                var  count =Service.Count(a => a.chr_code.ToLower() == chr_code.ToLower());
                if (count > 0)
                {
                    return Json(new { code = -1, msg = "该代号的附加内容已经存在" });
                }

                int lang_flag = 1;

                long node_id = tableid.getNewTableId<site_other>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                var result =new site_other();
                if (tc_content != "")
                {
                    result = base.Service.Insert(new site_other()
                    {
                        chr_code = chr_code,
                        chr_title = chr_title,
                        chr_content = tc_content,
                        lang = "tc",
                        lang_flag = lang_flag,
                        id = node_id,
                        is_show = is_show
                        ,off_status=off_status
                        ,begin_time=begin_time
                        ,end_time=end_time
                        
                    });
                }
                if (sc_content != "")
                {
                    result = base.Service.Insert(new site_other()
                    {
                        chr_code = chr_code,
                        chr_title = chr_title,
                        chr_content = sc_content,
                        lang = "sc",
                        lang_flag = 1,
                        id = node_id,
                        is_show = is_show 
                        ,off_status=off_status
                        ,begin_time=begin_time
                        ,end_time=end_time

                    });
                }

                if (en_content != "")
                {
                    result = base.Service.Insert(new site_other()
                    {
                        chr_code = chr_code,
                        chr_title = chr_title,
                        chr_content = en_content,
                        lang = "en",
                        lang_flag = 1,
                        id = node_id,
                        is_show = is_show
                        ,
                        off_status = off_status
                        ,
                        begin_time = begin_time
                        ,
                        end_time = end_time
                    });
                }
                if (result.id > 0)
                {
                    AddLog("site_other", Utils.LogsOperateTypeEnum.Add, $"新增标签:{chr_title}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {

                
                int lang_flag = 1;

                var result = 0;

                if (tc_content != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "tc");
                    if (count > 0)
                    {
                        result = base.Service.Modify(a => a.id == id && a.lang == "tc", a => new site_other()
                        {

                            lang_flag = lang_flag,
                            chr_title = chr_title,
                            chr_content = tc_content,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        });
                    }
                    else
                    {
                        result = base.Service.Insert(new site_other()
                        {
                            chr_code = chr_code,
                            chr_title = chr_title,
                            chr_content = tc_content,
                            lang = "tc",
                            lang_flag = 1,
                            id = id,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        }).id.MyToInt();
                    }
                }
                else {
                    Service.Delete(a => a.id == id && a.lang == "tc");
                }
                if (sc_content != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");


                    if (count > 0)
                    {
                        result = base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_other()
                        {

                            lang_flag = 1,
                            chr_code = chr_code,
                            chr_title = chr_title,
                            chr_content = sc_content,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        });
                    }
                    else
                    {

                        result = base.Service.Insert(new site_other()
                        {
                            chr_code = chr_code,
                            chr_title = chr_title,
                            chr_content = sc_content,
                            lang = "sc",
                            lang_flag = 1,
                            id = id,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        }).id.MyToInt();
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "sc");
                }

                if (en_content != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        result = base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_other()
                        {

                            lang_flag = 1,
                            chr_title = chr_title,
                            chr_content = en_content,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        });
                    }
                    else
                    {

                        result = base.Service.Insert(new site_other()
                        {
                            chr_code = chr_code,
                            chr_title = chr_title,
                            chr_content = en_content,
                            lang = "en",
                            lang_flag = 1,
                            id = id,
                            is_show = is_show
                        ,
                            off_status = off_status
                        ,
                            begin_time = begin_time
                        ,
                            end_time = end_time
                        }).id.MyToInt();
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "en");
                }

                if (result > 0)
                {
                    AddLog("site_other", Utils.LogsOperateTypeEnum.Update, $"修改附加内容:{chr_title}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_other", resource_code = "site_other", operate_code = "del")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;
                
                if (deleted)
                {
                    AddLog("site_other", Utils.LogsOperateTypeEnum.Delete, $"删除附加内容:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            string chr_code = data["chr_code"].MyToString();
            List<System.Linq.Expressions.Expression<Func<site_other, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_other, bool>>>();
            condition.Add(a => a.lang == "sc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.chr_title.Contains(ChrName));
            }

            if (chr_code.Trim() != "")
            {
                condition.Add(a => a.chr_code.Contains(chr_code));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public  JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
           
            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag = Service.Modify(a => a.id == id, a => new site_other()
            {
                is_show = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"site_other", Utils.LogsOperateTypeEnum.Update, $"修改网站附加内容状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}