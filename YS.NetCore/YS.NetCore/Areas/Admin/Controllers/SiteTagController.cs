﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteTagController : Net.Mvc.BasicController<site_tags, ISiteTagsService>
    {

        public SiteTagController(ISiteTagsService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tag", resource_code = "site_tag", operate_code = "add,mod")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Add(string id)
        {

            site_tags tc_model = new site_tags();
            site_tags sc_model = new site_tags();
            site_tags en_model = new site_tags();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                tc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.GetByOne(a => a.id == id.MyToLong() && a.lang == "en");

            }

            if (tc_model == null)
            {
                tc_model = new site_tags();
            }
            if (sc_model == null)
            {
                sc_model = new site_tags();
            }
            if (en_model == null)
            {
                en_model = new site_tags();
            }

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = tc_model, sc = sc_model, en = en_model }
                )));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public JsonResult Save(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {


            long id = data["id"].MyToLong();
            string tc_chr_name = data["tc_chr_name"].MyToString();

            string tc_tag_link = data["tc_tag_link"].MyToString();
            string sc_tag_link = data["sc_tag_link"].MyToString();
            string en_tag_link = data["en_tag_link"].MyToString();
            string sc_chr_name = data["sc_chr_name"].MyToString();
            string en_chr_name = data["en_chr_name"].MyToString();
            if (sc_chr_name.Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }


            if (id <= 0)
            {
                int lang_flag = 1;
                long node_id = tableid.getNewTableId<site_tags>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                var result = new site_tags();
                if (tc_chr_name != "")
                {
                    result = Service.Insert(new site_tags()
                    {
                        first_letter = "",
                        lang = "tc",
                        lang_flag = lang_flag,
                        name = tc_chr_name,
                        tag_link = tc_tag_link,
                        id = node_id
                    });
                }
                if (sc_chr_name != "")
                {
                    base.Service.Insert(new site_tags()
                    {
                        first_letter = "",
                        lang = "sc",
                        lang_flag = 1,
                        name = sc_chr_name,
                        tag_link = sc_tag_link,
                        id = node_id
                    });
                }

                if (en_chr_name != "")
                {
                    base.Service.Insert(new site_tags()
                    {
                        first_letter = "",
                        lang = "en",
                        lang_flag = 1,
                        name = en_chr_name,
                        tag_link = en_tag_link,
                        id = node_id
                    });
                }
                if (result.id > 0)
                {
                    AddLog("site_tag", Utils.LogsOperateTypeEnum.Add, $"新增标签:{sc_chr_name}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int lang_flag = 1;
                int result = 0;
                if (tc_chr_name != "")
                {

                    int count = Service.Count(a => a.id == id && a.lang == "tc");
                    if (count > 0)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "tc", a => new site_tags()
                        {
                            lang_flag = lang_flag,
                            name = tc_chr_name,
                            tag_link = tc_tag_link,
                        });
                    }
                    else
                    {
                        Service.Insert(new site_tags()
                        {
                            first_letter = "",
                            lang = "tc",
                            lang_flag = 1,
                            name = tc_chr_name,
                            tag_link = tc_tag_link,
                            id = id
                        });
                    }
                }
                else
                {
                    Service.Delete<site_tags>(a => a.id == id && a.lang == "tc");
                }
                if (sc_chr_name != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "sc");


                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "sc", a => new site_tags()
                        {

                            lang_flag = 1,
                            name = sc_chr_name,
                            tag_link = sc_tag_link,
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_tags()
                        {
                            first_letter = "",
                            lang = "sc",
                            lang_flag = 1,
                            name = sc_chr_name,
                            tag_link = sc_tag_link,
                            id = id
                        });
                    }
                }
                else {
                    Service.Delete<site_tags>(a => a.id == id && a.lang == "sc");
                }

                if (en_chr_name != "")
                {
                    int count = Service.Count(a => a.id == id && a.lang == "en");
                    if (count > 0)
                    {
                        base.Service.Modify(a => a.id == id && a.lang == "en", a => new site_tags()
                        {

                            lang_flag = 1,
                            name = en_chr_name,
                            tag_link = en_tag_link,
                        });
                    }
                    else
                    {

                        base.Service.Insert(new site_tags()
                        {
                            first_letter = "",
                            lang = "en",
                            lang_flag = 1,
                            name = en_chr_name,
                            tag_link = en_tag_link,
                            id = id
                        });
                    }
                }
                else {
                    Service.Delete<site_tags>(a => a.id == id && a.lang == "en");
                }

                if (result > 0)
                {
                    AddLog("site_tag", Utils.LogsOperateTypeEnum.Update, $"修改标签:{sc_chr_name}");
                    return Json(new { code = 0, msg = $"修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }


            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tag", resource_code = "site_tag", operate_code = "del")]
        public JsonResult Delete(IFormCollection data)
        {

            try
            {
                long id = data["id"].MyToLong();
                string chr_name = data["chr_name"].MyToString();
                if (id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                bool deleted = Service.Delete(a => a.id == id) > 0;

                if (deleted)
                {
                    AddLog("site_tag", Utils.LogsOperateTypeEnum.Delete, $"删除标签:{chr_name}");
                    return Json(new { code = 1, msg = "删除成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "删除失败" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { code = false, msg = ex.Message });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();

            List<System.Linq.Expressions.Expression<Func<site_tags, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_tags, bool>>>();
            condition.Add(a => a.lang == "sc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.name.Contains(ChrName));
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }

        [HttpPost]
        public IActionResult GetTagList(IFormCollection data)
        {
            string lang = data["lang"].MyToString();

            string tc_selected = data["tc_selected"].MyToString();
            string sc_selected = data["sc_selected"].MyToString();
            string en_selected = data["en_selected"].MyToString();
            var list = Service.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<site_tags, bool>>>()); ;
            var tc_selected_list = tc_selected.Split(',');
            var sc_selected_list = sc_selected.Split(',');
            var en_selected_list = en_selected.Split(',');
            var tc_result = list.Where(a => a.lang == "tc").Select(a => new
            {
                name = a.name,
                value = a.id,
                selected = tc_selected_list.Contains(a.id.MyToString())
            });

            var sc_result = list.Where(a => a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)).Select(a => new
            {
                name = a.name,
                value = a.id,
                selected = sc_selected_list.Contains(a.id.MyToString())
            });

            var en_result = list.Where(a => a.lang == "en").Select(a => new
            {
                name = a.name,
                value = a.id,
                selected = en_selected_list.Contains(a.id.MyToString())
            });

            return Json(new { tc = tc_result, sc = sc_result, en = en_result });

        }

    }
}