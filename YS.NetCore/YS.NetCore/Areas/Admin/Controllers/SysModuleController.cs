﻿



using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysModuleController : Net.Mvc.BasicController<sys_module_entity, ISysModuleService>
    {

        private readonly IApplicationContextAccessor applicationContextAccessor;

        private readonly IApplicationContext app;
        public SysModuleController(
            IApplicationContextAccessor _applicationContextAccessor
            , ISysModuleService userService
            , IApplicationContext _app) : base(userService, _applicationContextAccessor)
        {
            app = _app;
            applicationContextAccessor = _applicationContextAccessor;
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "")]
        public IActionResult Index(string catalog_id)
        {
            return View("Index", catalog_id);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "add,modify")]
        public IActionResult Add(string catalog_id, string id)
        {
            sys_module_entity model = new sys_module_entity();
            if (!string.IsNullOrEmpty(catalog_id) && catalog_id.MyToLong() > 0)
            {
                model.p_catalog = catalog_id.MyToLong();
            }
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne(a => a.id == id.MyToLong());
            }
            return View(model);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "")]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();

            long catalog_id = data["catalog_id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            long opt_catalog_id = data["opt_catalog_id"].MyToLong();

            List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>();
            if (catalog_id > 0)
            {
                condition.Add(a => a.p_catalog == catalog_id);
            }
            else
            {
                if (opt_catalog_id > 0)
                {
                    condition.Add(a => a.p_catalog == catalog_id);
                }
            }

            if (chr_name.Trim().Length > 0)
            {
                condition.Add(a => a.chr_name.Contains(chr_name));
            }
            var list = Service.GetPageList(page_index, page_size, "n_sort asc", condition);
            return PartialView("gv_List", list);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "delete")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["account"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            bool deleted = Service.Modify(a => a.id == id,a=>new sys_module_entity() { 
                delete_status=DeleteEnum.Delete
            }) > 0;
            if (deleted)
            {
                AddLog("sys_module", Utils.LogsOperateTypeEnum.Delete, $"删除菜单模块:{chr_name}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "add,modify")]

        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {

            long id = data["id"].MyToLong();
            long p_catalog = data["catalog_id"].MyToLong();
            string module_code = data["module_code"].MyToString();
            string chr_name = data["chr_name"].MyToString();
            int n_sort = data["n_sort"].MyToInt();
            StatusEnum is_sys = (StatusEnum)data["is_sys"].MyToInt();
            string description = data["description"].MyToString();

            StatusEnum status = (StatusEnum)data["status"].MyToInt();

            bool flag = false;

            if (id <= 0)
            {
                var check = Service.GetByOne(a => a.module_code == module_code && a.p_catalog == p_catalog);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该栏目下模块代号已经存在" });
                }


                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Insert(new sys_module_entity()
                {
                    p_catalog = p_catalog,
                    module_code = module_code,
                    chr_name = chr_name,
                    n_sort = n_sort,
                    is_sys = is_sys,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now
                }).id > 0;
                if (flag)
                {
                    AddLog("sys_module", Utils.LogsOperateTypeEnum.Add, $"新增菜单模块:{chr_name}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                var check = Service.GetByOne(a => a.module_code == module_code 
                && a.p_catalog == p_catalog
                &&a.id!=id);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该栏目下模块代号已经存在" });
                }

                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Modify(a=>a.id==id,a=>new sys_module_entity()
                {
                    p_catalog = p_catalog,
                    module_code = module_code,
                    chr_name = chr_name,
                    n_sort = n_sort,
                    is_sys = is_sys,
                    description = description,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
              
                }) > 0;
                if (flag)
                {
                    AddLog("sys_module", Utils.LogsOperateTypeEnum.Update, $"修改菜单模块:{chr_name}");
                    return Json(new { code = 0, msg = "修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }
            }


        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_module", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["account"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_module_entity()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("sys_module", Utils.LogsOperateTypeEnum.Update, $"修改菜单模块状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}
