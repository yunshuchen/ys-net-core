﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysContentsReviewController : Net.Mvc.BasicController<site_content, ISiteContentService>
    {

        public SysContentsReviewController(ISiteContentService service, IApplicationContextAccessor _httpContextAccessor) : base(service,_httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_review", resource_code = "site_content_review", operate_code = "review")]
        public ActionResult Add(string id)
        {
            site_content model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null)
            {
                model = new site_content();
             
            }
            if (sc_model == null)
            {
                sc_model = new site_content();
            }
            if (en_model == null)
            {
                en_model = new site_content();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_review", resource_code = "site_content_review", operate_code = "view")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(string id)
        {




            site_content model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();



            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null)
            {
                model = new site_content();

            }
            if (sc_model == null)
            {
                sc_model = new site_content();
            }
            if (en_model == null)
            {
                en_model = new site_content();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_review", resource_code = "site_content_review", operate_code = "review")]
        public JsonResult Save(
                [FromServices]ISysTableidService tableid, IFormCollection data)
        {
            long id = data["id"].MyToLong();

            int review_status = data["review_status"].MyToInt();
            if (id <= 0 || review_status <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (review_status != 1 && review_status != 2)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            ReViewStatusEnum status = (ReViewStatusEnum)data["review_status"].MyToInt();

            var model = Service.Get("row_key asc", a => a.id == id && a.lang == "sc");
            var flag = Service.Modify(a => a.id == id, a => new site_content()
            {
                review_status = status
            });
            if (flag > 0)
            {
                if (status == ReViewStatusEnum.Pass)
                {
                    AddLog("site_content_review", LogsOperateTypeEnum.ReView, $"审核通过网站内容:{model.title}");
                }
                else {
                    AddLog("site_content_review", LogsOperateTypeEnum.ReView, $"审核不通过网站内容:{model.title}");
                }
                return Json(new { code = 1, msg = "审核成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "审核失败" });
            }

        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString();
            int review_status = data["review_status"].MyToInt();
            List<System.Linq.Expressions.Expression<Func<site_content, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>();
            condition.Add(a => a.lang == "sc");
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            if (review_status == -1)
            {
                List<ReViewStatusEnum> r_s = new List<ReViewStatusEnum>() { ReViewStatusEnum.UnPass, ReViewStatusEnum.UnReview };
                condition.Add(a => r_s.Contains(a.review_status));
            }
            else {
                condition.Add(a => a.review_status==(ReViewStatusEnum)review_status);
            }
            var result = Service.GetNodeContentList(page_index, page_size, 0, condition);
            return PartialView("gv_List", Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(result)));
          
        }

             
    }
}