﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json.Linq;
using UEditorNetCore;

namespace YS.NetCore.Areas.Admin.Controllers
{
    class JsFolder
    {
        public string id { get; set; }
        public string folder_name { get; set; }
        public string description { get; set; }
        public string port_level { get; set; }
        public string parent_id { get; set; }
        public string is_directory { get; set; }
        public string upd_username { get; set; }
        public string upd_date { get; set; }
        public string upd_time { get; set; }
        public string crt_username { get; set; }
        public string crt_date { get; set; }
        public string crt_time { get; set; }
    }
    enum ResultState
    {
        Success,
        InvalidParam,
        AuthorizError,
        IOError,
        PathNotFound
    }

    [AllowAnonymous]
    public class UEditorController : Net.Mvc.BasicController<sys_template, ISysTemplateService>
    {
        private UEditorService ue;
        private ISiteFileProvider siteProvider;
        public UEditorController(UEditorService _ue, IWebHostEnvironment environment,
           ISiteFileProvider _siteProvider, ISysTemplateService ser, IApplicationContextAccessor _httpContextAccessor) : base(ser, _httpContextAccessor)
        {
            siteProvider = _siteProvider;
            ue = _ue;
            this.environment = environment;

        }

        private IWebHostEnvironment environment { get; set; }
        private string GetStateString()
        {
            switch (State)
            {
                case ResultState.Success:
                    return "SUCCESS";
                case ResultState.InvalidParam:
                    return "参数不正确";
                case ResultState.PathNotFound:
                    return "路径不存在";
                case ResultState.AuthorizError:
                    return "文件系统权限不足";
                case ResultState.IOError:
                    return "文件系统读取错误";
            }
            return "未知错误";
        }
        private int Start;
        private int Size;
        private int Total;
        private ResultState State;

        private String[] FileList;
        private ActionResult WriteResult()
        {
            return Write(new
            {
                state = GetStateString(),
                list = FileList == null ? null : FileList.Select(x => new { url = x }),
                start = Start,
                size = Size,
                total = Total
            });
        }

        private bool CheckFileType(List<string> AllowExts, string filename)
        {
            var fileExtension = Path.GetExtension(filename).ToLower();
            return AllowExts.Select(x => x.ToLower()).Contains(fileExtension);
        }

            private bool CheckFileSize(double SizeLimit, long size)
        {
            return size < SizeLimit;
        }

        private ActionResult Write(object data)
        {
            return Content(JsonHelper.ToJson(data), "text/json");
        }

        [AllowAnonymous]
        [RequestSizeLimit(100000000)]
        [Route("/Admin/UEditor")]
        [Route("/Admin/UEditor/index")]

        public ActionResult Index([FromServices]IHostingEnvironment _hostingEnvironment,IFormCollection data)
        {
            var action = Request.Query["action"].MyToString().ToLower();
            if (action == "config")
            {
                string contentType = "application/json";
                HttpContext.Response.ContentType = contentType;

                var root_path = _hostingEnvironment.ContentRootPath;
                string path = Path.Combine(root_path, "wwwroot", "lib", "UEditor", "config.json");//@"wwwroot\lib\UEditor\config.json"; // 注意哦, 不要像我这样直接使用客户端的值来拼接 path, 危险的 
                FileStream objStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                return File(objStream, "text/json");
                //return File(path, contentType);
            }


            //上传图片
            if (action == "uploadimage")
            {
                #region 上传图片

                string data_id = data["data_id"];//数据ID
                string data_type = data["data_type"];//数据类型
                if (data.Files.Count <= 0)
                {

                    return Write(new
                    {
                        state = "Failed"
                    });
                }

                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }
                string file_name = data.Files[0].FileName;
                string ext = file_name.GetFileExt();


                List<string> fileAllowFiles = new List<string>() {
                    ".png",
                    ".jpg",
                    ".jpeg",
                    ".gif",
                    ".bmp",
                    ".webp",
                    ".ico"
                };

                if (!fileAllowFiles.Contains(ext.ToLower()))
                {
                    return Write(new
                    {
                        state = "TypeNotAllow",
                        error = "不允许的文件格式"
                    });
                }
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();

                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                // string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                string save_path = Path.Combine(BasePath, new_name);

                data.Files[0].SaveAs(save_path);
                if (System.IO.File.Exists(save_path))
                {
                    string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + 
                        
                        content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;

                    return Write(new
                    {
                        state = "SUCCESS",
                        url = NodePicPath,
                        title = "",
                        original = "",
                        data_id = data_id
                    });
                }
                else
                {
                    return Write(new
                    {
                        state = "Failed"
                    });
                }
                #endregion
            }

            //获取列表图片
            if (action == "listimage")
            {

                #region 获取图片
                List<string> imageManagerAllowFiles = new List<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp",".ico" };
                var SearchExtensions = imageManagerAllowFiles.Select(x => x.ToLower()).ToArray();

                string BasePath = siteProvider.SiteWebRootPath;

                try
                {
                    Start = String.IsNullOrEmpty(Request.Query["start"]) ? 0 : Convert.ToInt32(Request.Query["start"]);
                    Size = String.IsNullOrEmpty(Request.Query["size"]) ? Config.GetInt("imageManagerListSize") : Convert.ToInt32(Request.Query["size"]);
                }
                catch (FormatException)
                {
                    State = ResultState.InvalidParam;
                    return WriteResult();
                }
                var buildingList = new List<String>();

                string static_path = siteProvider.StaticDirectory;
                try
                {
                    var localPath = Path.Combine(BasePath);
                    buildingList.AddRange(Directory.GetFiles(localPath, "*", SearchOption.AllDirectories)
                    .Where(x => SearchExtensions.Contains(Path.GetExtension(x).ToLower()))
                    .Select(x => string.Concat("/", static_path, "/",x.Substring(localPath.Length + 1).Replace("\\", "/").Replace(@"\", "/"))));
                    Total = buildingList.Count;
                    FileList = buildingList.OrderBy(x => x).Skip(Start).Take(Size).ToArray();
                }

                catch (UnauthorizedAccessException)
                {
                    State = ResultState.AuthorizError;
                }
                catch (DirectoryNotFoundException)
                {
                    State = ResultState.PathNotFound;
                }
                catch (IOException)
                {
                    State = ResultState.IOError;
                }

                return WriteResult();

                #endregion
            }
            //上传视频
            if (action == "uploadvideo")
            {

                #region 上传视频
                List<string> AllowExtensions = new List<string>() { ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid" };
                double SizeLimit = 102400000;

                if (data.Files.Count <= 0)
                {

                    return Write(new
                    {
                        state = "Failed"
                    });
                }

                var file = data.Files[0];
                string uploadFileName = file.FileName;

                if (!CheckFileType(AllowExtensions, uploadFileName))
                {

                    return Write(new
                    {
                        state = "TypeNotAllow",
                        error = "不允许的文件格式"
                    });
                }
                if (!CheckFileSize(SizeLimit, file.Length))
                {
                    return Write(new
                    {
                        state = "TypeNotAllow",
                        error = "不允许的文件格式"
                    });
                }

                byte[] uploadFileBytes = null;

                uploadFileBytes = new byte[file.Length];
                try
                {
                    file.OpenReadStream().Read(uploadFileBytes, 0, (int)file.Length);
                }
                catch (Exception)
                {
                    return Write(new
                    {
                        state = "NetworkError",
                        error = "网络错误"
                    });
                }

                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }
                string file_name = data.Files[0].FileName;
                string ext = file_name.GetFileExt();
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();

                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                // string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                string save_path = Path.Combine(BasePath, new_name);

                data.Files[0].SaveAs(save_path);
                if (System.IO.File.Exists(save_path))
                {
                    string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;

                    return Write(new
                    {
                        state = "SUCCESS",
                        url = NodePicPath,
                        title = "",
                        original = uploadFileName
                    });
                }
                else
                {
                    return Write(new
                    {
                        state = "Failed"
                    });
                }
                #endregion
            }


            if (action == "init_dir")
            {
                var dir_type = Request.Query["dir_type"].MyToString().ToLower();

                string root_id = "764bf6648e2b42d78d29e1e7a417836b";
                //path=相对于根目录的path
                //my_id=1 //自己的ID
                //parent_id=1 //父级的ID
                //max_id=0
                string id = data["id"].MyToString();
                string BasePath = siteProvider.StaticBasePath;
                if (dir_type == "static")
                {
                    BasePath = siteProvider.WwwrootsePath;
                }
                //else if (dir_type == "upload")
                //{
                //    BasePath = siteProvider.UploadStaticContentPath;
                //}
                string relative_path = id == root_id ? "" : id.Replace('$', System.IO.Path.DirectorySeparatorChar);

                string CurrentRootPath = Path.Combine(BasePath, relative_path);
                if (!Directory.Exists(CurrentRootPath))
                {
                    return Write(new RestfulData(-1, "目录不存在"));
                }
                List<JsFolder> rows = new List<JsFolder>();
                //如果查询第一级
                if (id == root_id)
                {

                    rows.Add(new JsFolder()
                    {
                        id = root_id,
                        folder_name = "根目录",
                        description = "根目录",
                        port_level = "1",
                        crt_username = "",
                        crt_date = "",
                        crt_time = "",
                        is_directory = "1",
                        parent_id = "0",
                        upd_date = "",
                        upd_time = "",
                        upd_username = ""
                    });

                    //目录下的文件、文件夹集合
                    string[] diArr = System.IO.Directory.GetDirectories(CurrentRootPath, "*", System.IO.SearchOption.TopDirectoryOnly);
                    string[] rootfileArr = System.IO.Directory.GetFiles(CurrentRootPath);
                    foreach (var dirs in diArr)
                    {

                        string fileName = Path.GetFileNameWithoutExtension(dirs);
                        rows.Add(new JsFolder()
                        {
                            id = fileName,
                            folder_name = fileName,
                            description = fileName,
                            port_level = "1",
                            crt_username = "",
                            crt_date = "",
                            crt_time = "",
                            is_directory = "1",
                            parent_id = root_id,
                            upd_date = "",
                            upd_time = "",
                            upd_username = ""
                        });


                    }
                    //文件直接添加
                    foreach (string var in rootfileArr)
                    {
                        DirectoryInfo info = new DirectoryInfo(var);
                        string file_name = Path.GetFileName(var);
                        rows.Add(new JsFolder()
                        {
                            id = file_name,
                            folder_name = file_name,
                            description = file_name,
                            port_level = "1",
                            crt_username = "",
                            crt_date = "",
                            crt_time = "",
                            is_directory = "0",
                            parent_id = root_id,
                            upd_date = "",
                            upd_time = "",
                            upd_username = ""
                        });
                    }
                }
                else
                {
                    GetFileAndDirects(CurrentRootPath, rows, id, BasePath);
                }
                string url_base_path = "/upload/";
                if (dir_type == "static")
                {
                    url_base_path = "/";
                }
                else if (dir_type == "upload")
                {
                    url_base_path = "/upload/";
                }
                return Write(new RestfulData<object>()
                {
                    code = 0,
                    msg = "ok",
                    data = new
                    {
                        list = rows,
                        base_path = url_base_path
                    }
                });
            }
            //媒体库上传文件
            if (action == "imagelibuplocas")
            {
                #region 文件库上传文件

                string data_id = data["id"];//目录名称2019$10 
                string relative_path = "";
                if (data_id != "764bf6648e2b42d78d29e1e7a417836b")
                {
                    relative_path = data_id.Replace('$', System.IO.Path.DirectorySeparatorChar);
                }


                if (data.Files.Count <= 0)
                {

                    return Write(new RestfulData(-1, "请选择上传文件"));
                }
                var dir_type = Request.Query["dir_type"].MyToString().ToLower();
                string root_path = siteProvider.StaticBasePath;
                if (dir_type == "static")
                {
                    root_path = siteProvider.WwwrootsePath;
                }
                //else if (dir_type == "upload")
                //{
                //    root_path = siteProvider.UploadStaticContentPath;
                //}
                string BasePath = Path.Combine(root_path, relative_path);

                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }

                string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                string save_path = Path.Combine(BasePath, new_name);

                data.Files[0].SaveAs(save_path);
                if (System.IO.File.Exists(save_path))
                {
                    string id = save_path.Replace(root_path, "", StringComparison.CurrentCultureIgnoreCase)
                        .Replace(System.IO.Path.DirectorySeparatorChar, '$');

                    if (id.StartsWith("$"))
                    {
                        id = id.Substring(1);
                    }
                    string file_name = Path.GetFileName(save_path);
                    return Write(new RestfulData<JsFolder>()
                    {
                        code = 0,
                        msg = "ok",
                        data = new JsFolder()
                        {
                            id = id,
                            folder_name = file_name,
                            description = file_name,
                            port_level = "1",
                            crt_username = "",
                            crt_date = "",
                            crt_time = "",
                            is_directory = "0",
                            parent_id = data_id,
                            upd_date = "",
                            upd_time = "",
                            upd_username = ""
                        }
                    });
                }
                else
                {
                    return Write(new RestfulData(-1, "上传失败"));
                }
                #endregion
            }

            //创建目录

            if (action == "create_dir")
            {
                #region 文件库上传文件

                string data_id = data["id"];//目录名称2019$10 
                string create_dir_name = data["create_dir_name"];//目录名


                if (create_dir_name == "")
                {
                    return Write(new RestfulData(-1, "参数错误"));
                }


                string relative_path = "";
                if (data_id != "764bf6648e2b42d78d29e1e7a417836b")
                {
                    relative_path = data_id.Replace('$', System.IO.Path.DirectorySeparatorChar);
                }

                var dir_type = Request.Query["dir_type"].MyToString().ToLower();

                string root_path = siteProvider.StaticBasePath;
                if (dir_type == "static")
                {
                    root_path = siteProvider.WwwrootsePath;
                }
                //else if (dir_type == "upload")
                //{
                //    root_path = siteProvider.UploadStaticContentPath;
                //}

                string BasePath = Path.Combine(root_path, relative_path, create_dir_name);

                if (Directory.Exists(BasePath))
                {
                    return Write(new RestfulData(-1, "该目录已存在"));
                }

                try
                {
                    Directory.CreateDirectory(BasePath);

                    string id = BasePath.Replace(root_path, "", StringComparison.CurrentCultureIgnoreCase)
                      .Replace(System.IO.Path.DirectorySeparatorChar, '$');

                    if (id.StartsWith("$"))
                    {
                        id = id.Substring(1);
                    }
                    return Write(new RestfulData<JsFolder>()
                    {
                        code = 0,
                        msg = "ok",
                        data = new JsFolder()
                        {
                            id = id,
                            folder_name = create_dir_name,
                            description = create_dir_name,
                            port_level = "1",
                            crt_username = "",
                            crt_date = "",
                            crt_time = "",
                            is_directory = "1",
                            parent_id = data_id,
                            upd_date = "",
                            upd_time = "",
                            upd_username = ""
                        }
                    });
                }
                catch (Exception ex)
                {

                    return Write(new RestfulData(-1, $"创建失败{ex.Message}"));
                }


                #endregion
            }
            //抓取远程图片
            if (action == "catchimage")
            {
                #region 抓取远程图片
                string[] Sources = data["source[]"];


                if (Sources == null || Sources.Length == 0)
                {
                    return Write(new
                    {
                        state = "参数错误：没有指定抓取源"
                    });
                }

                Crawler[] crawlers = Sources.Select(x => new Crawler(x, siteProvider).Fetch()).ToArray();
                return Write(new
                {
                    state = "SUCCESS",
                    list = crawlers.Select(x => new
                    {
                        state = x.State,
                        source = x.SourceUrl,
                        url = x.ServerUrl
                    })
                });
                #endregion
            }
            //上传涂鸦
            if (action == "uploadscrawl")
            {
                #region 上传涂鸦
                var source = data["upfile"];
                string uploadFileName = "scrawl.png";
                byte[] uploadFileBytes = Convert.FromBase64String(source);

                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }

                string new_name = Guid.NewGuid().ToString("N") + uploadFileName.GetFileExt();
                string save_path = Path.Combine(BasePath, new_name);
                System.IO.File.WriteAllBytes(save_path, uploadFileBytes);
                if (System.IO.File.Exists(save_path))
                {
                    string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;

                    return Write(new
                    {
                        state = "SUCCESS",
                        url = NodePicPath,
                        title = "",
                        original = ""
                    });
                }
                else
                {
                    return Write(new
                    {
                        state = "Failed"
                    });
                }
                #endregion
            }
            //上传文件
            if (action == "uploadfile")
            {
                #region 上传文件


                List<string> fileAllowFiles = new List<string>() {
                    ".png",
                    ".jpg",
                    ".jpeg",
                    ".gif",
                    ".bmp",
                    ".webp",
                    ".flv",
                    ".swf",
                    ".mkv",
                    ".avi",
                    ".rm",
                    ".rmvb",
                    ".mpeg",
                    ".mpg",
                    ".ogg",
                    ".ogv",
                    ".mov",
                    ".wmv",
                    ".mp4",
                    ".webm",
                    ".mp3",
                    ".wav",
                    ".mid",
                    ".rar",
                    ".zip",
                    ".tar",
                    ".gz",
                    ".7z",
                    ".bz2",
                    ".cab",
                    ".iso", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml" };
                double fileMaxSize = 512000000;

                if (data.Files.Count <= 0)
                {

                    return Write(new
                    {
                        state = "Failed",
                        error = "请选择文件后上传"
                    });
                }

                var file = data.Files[0];
                string uploadFileName = file.FileName;

                if (!CheckFileType(fileAllowFiles, uploadFileName))
                {

                    return Write(new
                    {
                        state = "TypeNotAllow",
                        error = "不允许的文件格式"
                    });
                }
                if (!CheckFileSize(fileMaxSize, file.Length))
                {
                    return Write(new
                    {
                        state = "TypeNotAllow",
                        error = "不允许的文件格式"
                    });
                }

                byte[] uploadFileBytes = new byte[file.Length];
                try
                {
                    file.OpenReadStream().Read(uploadFileBytes, 0, (int)file.Length);
                }
                catch (Exception)
                {
                    return Write(new
                    {
                        state = "NetworkError",
                        error = "网络错误"
                    });
                }
                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }

                string file_name = data.Files[0].FileName;
                string ext = file_name.GetFileExt();
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();

                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                // string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
                string save_path = Path.Combine(BasePath, new_name);

                data.Files[0].SaveAs(save_path);
                if (System.IO.File.Exists(save_path))
                {
                    string NodePicPath = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                    return Write(new
                    {
                        state = "SUCCESS",
                        url = NodePicPath,
                        title = uploadFileName,
                        original = uploadFileName
                    });
                }
                else
                {
                    return Write(new
                    {
                        state = "Failed",
                        error = "文件保存失败"
                    });
                }
                #endregion

            }
            //文件列表
            if (action == "listfile")
            {
                #region 文件列表


                List<string> fileManagerAllowFiles = new List<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp", ".flv", ".swf", ".mkv", ".avi", ".rm", ".rmvb", ".mpeg", ".mpg", ".ogg", ".ogv", ".mov", ".wmv", ".mp4", ".webm", ".mp3", ".wav", ".mid", ".rar", ".zip", ".tar", ".gz", ".7z", ".bz2", ".cab", ".iso", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".pdf", ".txt", ".md", ".xml" };
                var SearchExtensions = fileManagerAllowFiles.Select(x => x.ToLower()).ToArray();

                string BasePath = siteProvider.SiteWebRootPath;

                try
                {
                    Start = String.IsNullOrEmpty(Request.Query["start"]) ? 0 : Convert.ToInt32(Request.Query["start"]);
                    Size = String.IsNullOrEmpty(Request.Query["size"]) ? Config.GetInt("imageManagerListSize") : Convert.ToInt32(Request.Query["size"]);
                }
                catch (FormatException)
                {
                    State = ResultState.InvalidParam;
                    return WriteResult();
                }
                var buildingList = new List<String>();
                try
                {
                    string static_path = siteProvider.StaticDirectory;
                    var localPath = Path.Combine(BasePath);
                    buildingList.AddRange(Directory.GetFiles(localPath, "*", SearchOption.AllDirectories)
                    .Where(x => SearchExtensions.Contains(Path.GetExtension(x).ToLower()))
                    .Select(x => string.Concat("/", static_path, "/", x.Substring(localPath.Length + 1).Replace("\\", "/").Replace(@"\", "/"))));
                    Total = buildingList.Count;
                    FileList = buildingList.OrderBy(x => x).Skip(Start).Take(Size).ToArray();
                }

                catch (UnauthorizedAccessException)
                {
                    State = ResultState.AuthorizError;
                }
                catch (DirectoryNotFoundException)
                {
                    State = ResultState.PathNotFound;
                }
                catch (IOException)
                {
                    State = ResultState.IOError;
                }

                return WriteResult();
                #endregion
            }

            if (action == "jsfolder")
            {


            }
            return Write(new
            {
                state = "action is empty or action not supperted."
            });
        }
        /// <summary>
        /// 获取子目录所有文件及目录
        /// </summary>
        /// <param name="path">父级目录</param>
        /// <param name="result">返回值</param>
        /// <param name="parent_path">父级目录_id</param>
        private void GetFileAndDirects(string path, List<JsFolder> result, string parent_path, string base_path)
        {
            if (!Directory.Exists(path)) return;
            //目录下的文件、文件夹集合
            string[] diArr = System.IO.Directory.GetDirectories(path, "*", System.IO.SearchOption.TopDirectoryOnly);
            string[] rootfileArr = System.IO.Directory.GetFiles(path);

            foreach (var dirs in diArr)
            {

                string fileName = Path.GetFileNameWithoutExtension(dirs);
                string id = dirs.Replace(base_path, "", StringComparison.CurrentCultureIgnoreCase).Replace(System.IO.Path.DirectorySeparatorChar, '$');
                if (id.StartsWith("$"))
                {
                    id = id.Substring(1);
                }
                result.Add(new JsFolder()
                {
                    id = id,
                    folder_name = fileName,
                    description = fileName,
                    port_level = "1",
                    crt_username = "",
                    crt_date = "",
                    crt_time = "",
                    is_directory = "1",
                    parent_id = parent_path,
                    upd_date = "",
                    upd_time = "",
                    upd_username = ""
                });


            }
            //文件直接添加
            foreach (string var in rootfileArr)
            {
                DirectoryInfo info = new DirectoryInfo(var);
                string file_name = Path.GetFileName(var);

                string id = var.Replace(base_path, "", StringComparison.CurrentCultureIgnoreCase)
                    .Replace(System.IO.Path.DirectorySeparatorChar, '$');
                if (id.StartsWith("$"))
                {
                    id = id.Substring(1);
                }
                result.Add(new JsFolder()
                {
                    id = id,
                    folder_name = file_name,
                    description = file_name,
                    port_level = "1",
                    crt_username = "",
                    crt_date = "",
                    crt_time = "",
                    is_directory = "0",
                    parent_id = parent_path,
                    upd_date = "",
                    upd_time = "",
                    upd_username = ""
                });
            }

        }



        #region  文件上传(包含切片)

        /// <summary>
        /// 获取指定文件的已上传的最大文件块
        /// </summary>
        /// <param name="md5">文件唯一值</param>
        /// <param name="ext">文件后缀</param>
        /// <returns></returns>
        [AllowAnonymous]
        [RequestSizeLimit(100000000)]
        [HttpGet("/Admin/WebUpload/GetMaxChunk")]
        public CommResult GetMaxChunk(string md5, string ext)
        {
            try
            {
                var result = JResult.Success();




                // upload/web_uploader_temp
                string import_temp_path = siteProvider.UploadStaticContentPath;
                string userPath = Path.Combine(import_temp_path, "web_uploader_temp");
                //判断目录
                if (!Directory.Exists(userPath))
                {
                    Directory.CreateDirectory(userPath);
                }

                var md5Folder = GetFileMd5Folder(userPath, md5);
                if (!Directory.Exists(md5Folder))
                {
                    DicCreate(md5Folder);
                    return result;
                }

                var fileName = md5 + "." + ext;
                string targetPath = Path.Combine(md5Folder, fileName);
                // 文件已经存在，则可能存在问题，直接删除，重新上传
                if (System.IO.File.Exists(targetPath))
                {
                    System.IO.File.Delete(targetPath);
                    return result;
                }

                var dicInfo = new DirectoryInfo(md5Folder);
                var files = dicInfo.GetFiles();
                var chunk = files.Count();
                if (chunk > 1)
                {
                    #region 计算切片中途是否有缺失
                    /*   var returnMax = 0;
                       var fileIntList = files.Select(x => Convert.ToInt32(x.Name)).OrderBy(x => x).ToList();
                       if (fileIntList == null || fileIntList.Count == 0)
                           return Write(JResult.Success(), JsonRequestBehavior.AllowGet);
                       var maxFileInt = fileIntList.Max();
                       var minFileInt = fileIntList.Min();
                       if (minFileInt != 1)
                           return Write(JResult.Success());
                       foreach (var item in fileIntList)
                       {
                           var nextFile = item + 1;
                           if (maxFileInt == nextFile)
                           {
                               returnMax = maxFileInt;
                               break;
                           }
                           if (!fileIntList.Where(x => x == nextFile).Any())
                           {
                               returnMax = item;
                               break;
                           }
                       }*/

                    #endregion
                    //当文件上传中时，页面刷新，上传中断，这时最后一个保存的块的大小可能会有异常，所以这里直接删除最后一个块文件                  
                    result.data = (chunk - 1);
                    return result;
                }

                return result;
            }
            catch (Exception ex)
            {
                var errMsg = ex.Message;
                return JResult.Error(errMsg);
            }
        }

        /// <summary>
        /// 文件分块上传
        /// </summary>
        /// <param name="forms">表单
        /// { 
        /// id: WU_FILE_1
        /// name: My97DatePicker.zip
        /// type: application/x-zip-compressed
        /// lastModifiedDate: Thu Nov 07 2019 10:46:00 GMT 0800 (中国标准时间)
        /// size: 39235 
        /// md5: 4bd4d8fc0dca561f4b17bb5bf2be75d6
        /// file: (binary)
        /// } </param>
        /// <returns></returns>
        [HttpPost("/Admin/WebUpload/ChunkUpload")]
        public async Task<CommResult> ChunkUploadAsync(IFormCollection forms/*IFormFile file, string md5, int? chunk, int chunks = 0*/)
        {
            try
            {
                IFormFile file = forms.Files.GetFile("file");
                string md5 = forms["md5"].MyToString();
                int chunk = forms["chunk"].MyToInt();
                int chunks = forms["chunks"].MyToInt();
                var result = JResult.Success();
                // upload/web_uploader_temp
                string import_temp_path = siteProvider.UploadStaticContentPath;
                string userPath = Path.Combine(import_temp_path, "web_uploader_temp");
                //判断目录
                if (!Directory.Exists(userPath))
                {
                    Directory.CreateDirectory(userPath);
                }
                var md5Folder = GetFileMd5Folder(userPath, md5);
                var filePath = "";  // 要保存的文件路径
                if (!Directory.Exists(md5Folder))
                {
                    Directory.CreateDirectory(md5Folder);
                }
                filePath = Path.Combine(md5Folder, chunk.MyToString());
                result.code = chunk;
                if (chunks == chunk)
                {
                    result.message = "chunked";
                }
                // 写入文件
                using (var addFile = new FileStream(filePath, FileMode.OpenOrCreate))
                {
                    await file.CopyToAsync(addFile);
                }

                return result;
            }
            catch (Exception ex)
            {
                return JResult.Error(ex.Message);
            }
        }




        /// <summary>
        /// 合并文件
        /// </summary>
        /// <param name="data">
        ///json
        /// {"md5":"4bd4d8fc0dca561f4b17bb5bf2be75d6","filename":"My97DatePicker.zip","fileTotalSize":39235}
        /// </param>
        /// <returns></returns>
        [HttpPost("/Admin/WebUpload/MergeFiles")]
        public RestfulData MergeFiles([FromBody]JObject data)
        {
            try
            {
                string md5 = data["md5"].MyToString();
                long fileTotalSize = data["fileTotalSize"].MyToLong();
                string clientFileName = data["filename"].MyToString();

                // upload/web_uploader_temp
                string import_temp_path = siteProvider.UploadStaticContentPath;
                string userPath = Path.Combine(import_temp_path, "web_uploader_temp");
                //判断目录
                if (!Directory.Exists(userPath))
                {
                    Directory.CreateDirectory(userPath);
                }
                //源数据文件夹
                string sourcePath = GetFileMd5Folder(userPath, md5);



                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }

                string new_name = md5 + Path.GetExtension(clientFileName);
                //合并后的文件路径
                string targetFilePath = Path.Combine(BasePath, new_name);
                // 目标文件不存在，则需要合并
                if (!System.IO.File.Exists(targetFilePath))
                {
                    //判断分片上传目录是否存在
                    if (!Directory.Exists(sourcePath))
                    {
                        return new RestfulData(-1, "未找到对应的文件片");
                    }
                    //合并文件
                    MergeDiskFile(sourcePath, targetFilePath);
                }
                var valid = VaildMergeFile(md5, fileTotalSize, clientFileName, targetFilePath);
                DeleteFolder(sourcePath);
                if (!valid.result)
                {
                    return new RestfulData(-1, "文件损坏,请重新上传");
                }
                string File_Name = clientFileName;
                if (clientFileName != "" && clientFileName.IndexOf('.') > 0)
                {
                    File_Name = clientFileName.Substring(0, clientFileName.LastIndexOf('.'));
                }
                string web_site_path = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                return new RestfulData<object>(0, "ok",
                    new
                    {
                        new_name = new_name,
                        client_name = clientFileName,
                        file_name = File_Name,
                        web_path= web_site_path
                    });
            }
            catch (Exception ex)
            {
                return new RestfulData(-1, ex.Message);
            }
        }



        /// <summary>
        /// 获得文件MD5文件夹
        /// </summary>
        /// <returns></returns>
        private string GetFileMd5Folder(string basepath, string identifier)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                throw new Exception("缺少文件MD5值");
            }
            return Path.Combine(basepath, identifier);
        }

        /// <summary>
        /// 将磁盘上的切片源合并成一个文件
        /// <returns>返回所有切片文件的字节总和</returns>
        /// </summary>
        /// <param name="sourcePath">磁盘上的切片源</param>
        /// <param name="targetPath">目标文件路径</param>
        private int MergeDiskFile(string sourcePath, string targetPath)
        {
            FileStream addFile = null;
            BinaryWriter addWriter = null;
            try
            {
                var streamTotalSize = 0;
                addFile = new FileStream(targetPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
                //addFile = new FileStream(targetPath, FileMode.Append, FileAccess.Write);
                addWriter = new BinaryWriter(addFile);
                // 获取目录下所有的切片文件块
                FileInfo[] files = new DirectoryInfo(sourcePath).GetFiles();
                // 按照文件名(数字)进行排序
                var orderFileInfoList = files.OrderBy(f => int.Parse(f.Name));
                foreach (FileInfo diskFile in orderFileInfoList)
                {
                    //获得上传的分片数据流 
                    Stream stream = diskFile.Open(FileMode.Open);
                    BinaryReader tempReader = new BinaryReader(stream);
                    var streamSize = (int)stream.Length;
                    //将上传的分片追加到临时文件末尾
                    addWriter.Write(tempReader.ReadBytes(streamSize));
                    streamTotalSize += streamSize;
                    //关闭BinaryReader文件阅读器
                    tempReader.Close();
                    stream.Close();

                    tempReader.Dispose();
                    stream.Dispose();
                }
                addWriter.Close();
                addFile.Close();
                addWriter.Dispose();
                addFile.Dispose();
                return streamTotalSize;
            }
            catch (Exception ex)
            {
                if (addFile != null)
                {
                    addFile.Close();
                    addFile.Dispose();
                }

                if (addWriter != null)
                {
                    addWriter.Close();
                    addWriter.Dispose();
                }

                throw ex;
            }
        }

        /// <summary>
        /// 校验合并后的文件
        /// <para>1.是否没有漏掉块(chunk)</para>
        /// <para>2.检测文件大小是否跟客户端一样</para>
        /// <para>3.检查文件的MD5值是否一致</para>
        /// </summary>
        /// <param name="targetPath"></param>
        /// <returns></returns>
        private CommResult VaildMergeFile(string identifier, long fileTotalSize, string clientFileName, string targetPath)
        {

            var targetFile = new FileInfo(targetPath);
            var streamTotalSize = targetFile.Length;
            try
            {
                if (streamTotalSize != fileTotalSize)
                {
                    throw new Exception("[" + clientFileName + "]文件上传时发生损坏，请重新上传");
                }
                var fileMd5 = GetMD5HashFromFile(targetPath);
                if (!fileMd5.Equals(identifier))
                {
                    throw new Exception("[" + clientFileName + "],文件MD5值不对等");
                }
                return JResult.Success();
            }
            catch (Exception ex)
            {
                // 删除本地错误文件
                System.IO.File.Delete(targetPath);
                return JResult.Error(ex.Message);
            }

        }

        /// <summary>
        /// C#获取文件MD5值方法
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetMD5HashFromFile(string fileName)
        {
            try
            {
                //开辟临时缓存内存
                byte[] byteArrayRead = new byte[5 * 1024 * 1024]; // 1字节*1024 = 1k 1k*1024 = 1M内存


                StringBuilder sb = new StringBuilder();
                using (FileStream file = new FileStream(fileName, FileMode.Open))
                {
                    if (file.Length >= byteArrayRead.Length)
                    {
                        //readCount 这个是保存真正读取到的字节数
                        int readCount = file.Read(byteArrayRead, 0, byteArrayRead.Length);
                    }
                    else
                    {
                        byteArrayRead = new byte[(int)file.Length]; // 1字节*1024 = 1k 1k*1024 = 1M内存
                        //readCount 这个是保存真正读取到的字节数
                        int readCount = file.Read(byteArrayRead, 0, (int)file.Length);
                    }

                    System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    byte[] retVal = md5.ComputeHash(byteArrayRead);

                    for (int i = 0; i < retVal.Length; i++)
                    {
                        sb.Append(retVal[i].ToString("x2"));
                    }
                    return sb.ToString();

                }

            }
            catch (Exception ex)
            {
                throw new Exception("GetMD5HashFromFile() fail,error:" + ex.Message);
            }
        }

        /// <summary>
        /// 删除文件夹及其内容
        /// <para>附带删除超过一个月的文件以及文件夹</para>
        /// </summary>
        /// <param name="strPath"></param>
        private void DeleteFolder(string strPath)
        {
            if (Directory.Exists(strPath))
                Directory.Delete(strPath, true);

            #region 删除一个月以前的临时文件夹与文件
            var chunkTemp = Path.GetDirectoryName(strPath);
            DirectoryInfo dir = new DirectoryInfo(chunkTemp);
            DirectoryInfo[] dii = dir.GetDirectories();
            // 超过一个月的文件夹和文件
            var expireDate = DateTime.Now.AddMonths(-1);
            var deleteExpire = dii.Where(t => t.LastWriteTime < expireDate).ToList();
            if (deleteExpire.Any())
            {
                foreach (var item in deleteExpire)
                {
                    Directory.Delete(Path.Combine(chunkTemp, item.Name), true);
                }
            }

            var deleteExpireFile = dir.GetFiles().Where(t => t.LastWriteTime < expireDate).ToList();
            if (deleteExpireFile.Any())
            {
                foreach (var item in deleteExpireFile)
                {
                    System.IO.File.Delete(Path.Combine(chunkTemp, item.Name));
                }
            }
            #endregion
        }

        /// <summary>
        /// 文件目录如果不存在，就创建一个新的目录
        /// </summary>
        /// <param name="path"></param>
        private void DicCreate(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        #endregion





    }

    public class Crawler
    {
        /// <summary>
        /// 源地址
        /// </summary>
        public string SourceUrl { get; set; }
        /// <summary>
        /// 本地路径
        /// </summary>
        public string ServerUrl { get; set; }
        public string State { get; set; }

        ISiteFileProvider siteProvider;
        public Crawler(string sourceUrl, ISiteFileProvider _siteProvider)
        {
            this.SourceUrl = sourceUrl;
            // this.Server = server;
            siteProvider = _siteProvider;
        }

        public Crawler Fetch()
        {
            if (!IsExternalIPAddress(this.SourceUrl))
            {
                State = "INVALID_URL";
                return this;
            }
            var request = HttpWebRequest.Create($"{this.SourceUrl}") as HttpWebRequest;
            Uri uri = new Uri(this.SourceUrl);
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36";
            request.Referer = this.SourceUrl;
            using (var response = request.GetResponseAsync().Result as HttpWebResponse)
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    State = "Url returns " + response.StatusCode + ", " + response.StatusDescription;
                    return this;
                }
                if (response.ContentType.IndexOf("image") == -1)
                {
                    State = "Url is not an image";
                    return this;
                }
                String fileExt = Path.GetExtension(this.SourceUrl).ToLower();
                int len = fileExt.LastIndexOf('?');


                string BasePath = siteProvider.UploadStaticContentPath;
                string content_build_path = siteProvider.ContentUploadBuildPath;
                BasePath = Path.Combine(BasePath, content_build_path);
                if (!Directory.Exists(BasePath))
                {
                    Directory.CreateDirectory(BasePath);
                }


                if (len != -1)
                {
                    fileExt = fileExt.Substring(0, len);
                }
                string new_name = Guid.NewGuid().ToString("N") + fileExt;
                string save_path = Path.Combine(BasePath, new_name);

                ServerUrl = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;



                if (!Directory.Exists(Path.GetDirectoryName(save_path)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(save_path));
                }
                try
                {
                    var stream = response.GetResponseStream();
                    var reader = new BinaryReader(stream);
                    byte[] bytes;
                    using (var ms = new MemoryStream())
                    {
                        byte[] buffer = new byte[4096];
                        int count;
                        while ((count = reader.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            ms.Write(buffer, 0, count);
                        }
                        bytes = ms.ToArray();
                    }
                    File.WriteAllBytes(save_path, bytes);
                    State = "SUCCESS";
                }
                catch (Exception e)
                {
                    State = "抓取错误：" + e.Message;
                }
                return this;
            }
        }

        private bool IsExternalIPAddress(string url)
        {
            var uri = new Uri(url);
            switch (uri.HostNameType)
            {
                case UriHostNameType.Dns:
                    var ipHostEntry = Dns.GetHostEntryAsync(uri.DnsSafeHost).Result;
                    foreach (IPAddress ipAddress in ipHostEntry.AddressList)
                    {
                        byte[] ipBytes = ipAddress.GetAddressBytes();
                        if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            if (!IsPrivateIP(ipAddress))
                            {
                                return true;
                            }
                        }
                    }
                    break;

                case UriHostNameType.IPv4:
                    return !IsPrivateIP(IPAddress.Parse(uri.DnsSafeHost));
            }
            return false;
        }

        private bool IsPrivateIP(IPAddress myIPAddress)
        {
            if (IPAddress.IsLoopback(myIPAddress)) return true;
            if (myIPAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                byte[] ipBytes = myIPAddress.GetAddressBytes();
                // 10.0.0.0/24 
                if (ipBytes[0] == 10)
                {
                    return true;
                }
                // 172.16.0.0/16
                else if (ipBytes[0] == 172 && ipBytes[1] == 16)
                {
                    return true;
                }
                // 192.168.0.0/16
                else if (ipBytes[0] == 192 && ipBytes[1] == 168)
                {
                    return true;
                }
                // 169.254.0.0/16
                else if (ipBytes[0] == 169 && ipBytes[1] == 254)
                {
                    return true;
                }
            }
            return false;
        }
    }
}