﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace YS.NetCore.Areas.Admin.Controllers
{
    public class ExtModelController : Net.Mvc.BasicController<sys_ext_model, ISysExtModelService>
    {


        private ISysExtModelDetailesService extdetailrservice;

        public ExtModelController(ISysExtModelService extservice
            , ISysExtModelDetailesService _extdetailrservice
            , IApplicationContextAccessor _httpContextAccessor)
            : base(extservice, _httpContextAccessor)
        {
            extdetailrservice = _extdetailrservice;

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel")]
        public IActionResult Index()
        {
            return View();
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel")]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();

            string chr_name = data["chr_name"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<sys_ext_model, bool>>>();
            if (chr_name.Trim() != "")
            {
                condition.Add(a => a.chr_name.Contains(chr_name));


            }
            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", result);
        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel",operate_code ="add,mod")]
        public IActionResult Add(string id)
        {

            sys_ext_model model = new sys_ext_model();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {

                model = Service.GetByOne(a => a.id == id.MyToLong());

            }
            return View(model);

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "mod")]
        public IActionResult DSet(string p_code)
        {
            if (string.IsNullOrEmpty(p_code))
            {
                return new EmptyResult();
            }

            var list = extdetailrservice.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() {
                a=>a.p_code.ToLower()==p_code.ToLower()
            });

            return View(new PageOf<sys_ext_model_detail>()
            {
                list = list,
                page_index = 1,
                PagePostUrl = p_code,
                total = 1,
                page_size = 1,
            });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "mod")]
        [HttpPost]
        public JsonResult SaveDSet(string data,string p_code)
        {
            var list = JsonHelper.ToObject<List<sys_ext_model_detail>>(data);

            if (p_code.MyToString().Trim() == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            extdetailrservice.Delete(a => a.p_code.ToLower() == p_code.ToLower());
            list=list.Select(a =>
            {
                a.value = "";
                return a;
            }).ToList();


            extdetailrservice.BulkInsert(list);
            return Json(new { code = 1, msg = "保存成功" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "del")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            string chr_code = data["chr_code"].MyToString();

            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (chr_code == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            var check = Service.GetByOne<site_content>(a => a.ext_model_code.ToLower() == chr_code.ToLower());
            if (check != null && check.id > 0)
            {
                return Json(new { code = 1, msg = "已有栏目使用此模型无法删除" });
            }
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                AddLog("site_extmodel", LogsOperateTypeEnum.Delete, $"删除扩展模型:{chr_name}");
                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string account = data["account"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_ext_model()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("site_extmodel", LogsOperateTypeEnum.Update, $"修改扩展模型状态:{account}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");

                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "add,mod")]
        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {
            long id = data["id"].MyToLong();

            string chr_name = data["chr_name"].MyToString();
            string chr_code = data["chr_code"].MyToString();
            int status = data["status"].MyToInt();

            if (chr_name == "" || (status != 0 && status != 1))
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (id <= 0)
            {


                var check = Service.GetByOne("", a => a.chr_code == chr_code);
                if (check != null)
                {
                    return Json(new { code = -1, msg = "该代号的扩展模型已存在" });
                }

                bool flag = false;
                flag = Service.Insert(new sys_ext_model()
                {
                    chr_code = chr_code,
                    chr_name = chr_name,
                    status = (StatusEnum)status
                }).id > 0;
                if (flag)
                {

                    AddLog("site_extmodel", LogsOperateTypeEnum.Add, $"新增扩展模型:{chr_name}");
                    return Json(new { code = 1, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                bool flag = false;

                var check = Service.GetByOne("", a => a.chr_code == chr_code&&a.id!=id);
                if (check != null)
                {
                    return Json(new { code = -1, msg = "该代号的扩展模型已存在" });
                }
                flag = Service.Modify(a => a.id == id, a => new sys_ext_model()
                {

                    chr_code = chr_code,
                    chr_name = chr_name,
                    status = (StatusEnum)status

                }) > 0;

                if (flag)
                {

                    AddLog("site_extmodel", LogsOperateTypeEnum.Update, $"修改扩展模型:{chr_name}");
                    return Json(new { code = 1, msg = "修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改修改" });
                }

            }
        }
    }
}