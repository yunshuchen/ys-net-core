﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YS.Net;
using YS.Net.Models;
using YS.Utils.Extend;
using YS.Net.Services;
using YS.Net.Common;
using System.IO;
using Microsoft.Extensions.Logging;
using YS.Utils.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class TemplatesController : Net.Mvc.BasicController<sys_template, ISysTemplateService>
    {
        ISysTemplateService TplService;

        ISiteFileProvider siteProvider;
        private readonly ILogger<TemplatesController> _logger;
        public TemplatesController(ISysTemplateService _TplService,
            ISiteFileProvider _siteProvider, ILogger<TemplatesController> logger, IApplicationContextAccessor _httpContextAccessor) :base(_TplService, _httpContextAccessor)
        {
            TplService = _TplService;
            siteProvider = _siteProvider;
            _logger = logger;
        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl")]
        public IActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string TplName = data["TplName"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<sys_template, bool>>>();

            if (TplName.IsNotNullAndWhiteSpace())
            {
                condition.Add(a => a.tpl_name.Contains(TplName));
            }

            var result = TplService.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", result);
        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "change_status")]
        [HttpPost]
        public JsonResult ChangeStatus(IFormCollection data)
        {
            int Status = data["Status"].MyToInt();
            long id = data["Id"].MyToLong();
            string TplName = data["TplName"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (Status < 0 || Status > 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            
            return Json(new { code = -1, msg = "修改失败" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "del")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data) {
            long id = data["id"].MyToLong();
            string tplname = data["tplname"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag=TplService.Delete(a => a.id == id);
            if (flag > 0)
            {

                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Delete, $"删除模版:{tplname}");
                return Json(new { code = 0, msg = "删除成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "删除失败" });
            }



        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl")]
        [Route("/Admin/Templates/TplDetails/{tpl}")]
        [Route("/Admin/Templates/TplDetails/{tpl}/{pathname}")]
        public IActionResult TplDetails(string tpl, string pathname = "")
        {
            if (tpl == "")
            {
                return RedirectToAction("Error", "Code", new { returnurl = "~/Admin/Home" });
            }

            pathname = pathname.Replace('$', Path.DirectorySeparatorChar);
            template_file model = new template_file();
            model.tplPathCode = tpl;
            model.pathName = string.Join('$', pathname.Split(Path.DirectorySeparatorChar));
            model.Node = new tpl_directory_info();
            model.CurrentPath = pathname.MyToString() == "" ? tpl : pathname;
            model.Node.Nodes = new List<tpl_directory_info>();
            model.PrePath = pathname == "" ? "" : string.Join('$', pathname.Split(Path.DirectorySeparatorChar).SkipLast(1));
            string BasePath = siteProvider.TemplateBasePath;
            string rootPath = Path.Combine(BasePath, tpl + Path.DirectorySeparatorChar);
            string CurrentRootPath = rootPath;
            if (pathname != "")
            {
                CurrentRootPath = Path.Combine(rootPath, pathname);
            }
            GetFileAndDirects(CurrentRootPath, model.Node, rootPath);
            
            return View(model);
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl",operate_code ="del")]
        [HttpPost]
        public JsonResult DeleteFileOrFolder(IFormCollection data)
        {

            string tpl = data["tpl"].MyToString();
            int chr_type = data["chr_type"].MyToInt();//1文件 2目录
            string relative_path = data["relative_path"].MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);
            string BasePath = siteProvider.TemplateBasePath;
            string rootPath = Path.Combine(BasePath, tpl);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, relative_path);
            if (chr_type == 1)
            {
                if (!System.IO.File.Exists(CurrentRootPath))
                {
                    return Json(new { code = -1, msg = "文件不存在" });
                }
                try
                {
     
                    System.IO.File.Delete(CurrentRootPath);
                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Delete, $"删除模版文件:{relative_path}");
                    return Json(new { code = 0, msg = "ok" });
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "删除模板文件失败");
                    return Json(new { code = -1, msg = "删除失败" });
                }
            }
            else
            {
                if (!Directory.Exists(CurrentRootPath))
                {
                    return Json(new { code = -1, msg = "目录不存在" });
                }
                try
                {

                    System.IO.Directory.Delete(CurrentRootPath, true);

                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Delete, $"删除模版文件目录:{relative_path}");
                    return Json(new { code = 0, msg = "ok" });
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "删除模板目录失败");
                    return Json(new { code = -1, msg = "删除失败" });
                }
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "add,mod")]
        [Route("/Admin/Templates/EditTemplateFile/{relative_path}/{tpl}")]
        public IActionResult EditTemplateFile(string relative_path, string tpl)
        {
            tpl = tpl.MyToString();
            string new_relative_path = relative_path.MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);

            string BasePath = siteProvider.TemplateBasePath;
            string rootPath = Path.Combine(BasePath, tpl);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, new_relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }

            FileInfo fi = new FileInfo(CurrentRootPath);
            var source = System.IO.File.ReadAllBytes(CurrentRootPath);
            string str = System.Text.Encoding.Default.GetString(source);


            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new
            {
                chr_name=fi.Name,
                tpl_name = tpl,
                relative_path = relative_path,
                file_conent = str,
                content_type = fi.Extension
            })));
        }


        #region 网站资源
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "add,mod")]
        [Route("/Admin/Templates/EditStaticFile/{relative_path}")]
        public IActionResult EditStaticFile(string relative_path)
        {

            string new_relative_path = relative_path.MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);

            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, new_relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }

            FileInfo fi = new FileInfo(CurrentRootPath);
            var source = System.IO.File.ReadAllBytes(CurrentRootPath);
            string str = System.Text.Encoding.Default.GetString(source);


            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new
            {
                chr_name = fi.Name,
                relative_path = relative_path,
                file_conent = str,
                content_type = fi.Extension
            })));
        }
        [HttpPost]
        public JsonResult SaveStatic(IFormCollection data)
        {

            string content = data["content"].MyToString();
            string relative_path = data["relative_path"].MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);





            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "文件不存在" });
            }

            try
            {
                ExtFile.WriteFile(CurrentRootPath, content, System.Text.Encoding.UTF8);

                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"修改模版文件:{relative_path}");
                return Json(new { code = 0, msg = "ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "修改模板保存失败");
                return Json(new { code = -1, msg = "保存失败" });
            }


        }
        #endregion

        #region 上传资源
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "add,mod")]
        [Route("/Admin/Templates/EditUploadFile/{relative_path}")]
        public IActionResult EditUploadFile(string relative_path)
        {

            string new_relative_path = relative_path.MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);

            string BasePath = siteProvider.StaticBasePath;
            string rootPath = Path.Combine(BasePath);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, new_relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }

            FileInfo fi = new FileInfo(CurrentRootPath);
            var source = System.IO.File.ReadAllBytes(CurrentRootPath);
            string str = System.Text.Encoding.Default.GetString(source);


            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new
            {
                chr_name = fi.Name,
                relative_path = relative_path,
                file_conent = str,
                content_type = fi.Extension
            })));
        }

        [HttpPost]
        public JsonResult SaveUpload([FromServices] Microsoft.Extensions.Caching.Distributed.IDistributedCache cache, IFormCollection data)
        {

            string content = data["content"].MyToString();
            string relative_path = data["relative_path"].MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);
            string BasePath = siteProvider.StaticBasePath;
            string rootPath = Path.Combine(BasePath);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "文件不存在" });
            }


            try
            {
                ExtFile.WriteFile(CurrentRootPath, content, System.Text.Encoding.UTF8);
                if (relative_path.ToLower().IndexOf("localization") >= 0)

                {
                    string new_font_cache_key = cache.GetString("new_font_cache_key").MyToString();

                    cache.SetString("new_font_cache_key", Guid.NewGuid().ToString("N"));
                    cache.SetString("old_font_cache_key", new_font_cache_key);
                }
                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"修改模版文件:{relative_path}");
                return Json(new { code = 0, msg = "ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "修改模板保存失败");
                return Json(new { code = -1, msg = "保存失败" });
            }


        }

        #endregion
        private void GetFileAndDirects(string path, tpl_directory_info node, string rootpath)
        {
            if (!Directory.Exists(path)) return;
            //目录下的文件、文件夹集合
            string[] diArr = System.IO.Directory.GetDirectories(path, "*", System.IO.SearchOption.TopDirectoryOnly);
            string[] rootfileArr = System.IO.Directory.GetFiles(path);
            //文件夹递归
            for (int i = 0; i < diArr.Length; i++)
            {
                //增加父节点
                string fileName = Path.GetFileNameWithoutExtension(diArr[i]);
                DirectoryInfo info = new DirectoryInfo(diArr[i]);
                node.Nodes.Add(new tpl_directory_info()
                {
                    ParentPath = info.Parent.Parent.Name,
                    ChrName = fileName,
                    ChrType = 2,
                    Nodes = new List<tpl_directory_info>(),
                    FullName = diArr[i].Replace(rootpath, "", StringComparison.CurrentCultureIgnoreCase)
                });
                //GetFileAndDirects(diArr[i], node.Nodes[i]);
            }

            //文件直接添加
            foreach (string var in rootfileArr)
            {
                DirectoryInfo info = new DirectoryInfo(var);
                node.Nodes.Add(new tpl_directory_info()
                {
                    ChrName = Path.GetFileName(var),
                    ParentPath = info.Parent.Parent.Name,
                    ChrType = 1,
                    Nodes = new List<tpl_directory_info>(),
                    FullName = var.Replace(rootpath, "", StringComparison.CurrentCultureIgnoreCase)
                });
            }

        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "add,mod")]
        [HttpPost]
        public JsonResult SaveTemplate(IFormCollection data)
        {

            string tpl = data["tpl_name"].MyToString();
            string content = data["content"].MyToString();
            string relative_path = data["relative_path"].MyToString().Replace('$', System.IO.Path.DirectorySeparatorChar);





            string BasePath = siteProvider.TemplateBasePath;
            string rootPath = Path.Combine(BasePath, tpl);
            if (!Directory.Exists(rootPath))
            {
                return Json(new { code = -1, msg = "目录不存在" });
            }
            string CurrentRootPath = Path.Combine(rootPath, relative_path);

            if (!System.IO.File.Exists(CurrentRootPath))
            {
                return Json(new { code = -1, msg = "文件不存在" });
            }

            try
            {
                ExtFile.WriteFile(CurrentRootPath, content, System.Text.Encoding.UTF8);

                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"修改模版文件:{relative_path}");
                return Json(new { code = 0, msg = "ok" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "修改模板保存失败");
                return Json(new { code = -1, msg = "保存失败" });
            }


        }
   

        

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "addfile")]
        [Route("/Admin/Templates/AddFile")] 
        public IActionResult AddFile(string tpl,string path,string chr_type)
        {
            return View("AddFile",Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tpl= tpl, path= path, chr_type= chr_type })));
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_tpl", resource_code = "site_tpl", operate_code = "addfile")]
        public JsonResult SaveFile(IFormCollection data)
        {
            string tpl = data["tpl"].MyToString();
            string path = data["path"].MyToString();
            int chr_type = data["chr_type"].MyToInt();
            string chr_name = data["chr_name"].MyToString();
            string BasePath = siteProvider.TemplateBasePath;
            string rootPath = Path.Combine(BasePath, tpl, path.Replace('$', System.IO.Path.DirectorySeparatorChar));

            if (chr_name == "")
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (chr_type == 1)
            {
                string create_path = Path.Combine(rootPath, chr_name);
                if (Directory.Exists(create_path))
                {
                    return Json(new { code = -1, msg = "该目录已经存在" });
                }
                try
                {

                    Directory.CreateDirectory(create_path);


                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"创建文件目录:{path.Replace('$', System.IO.Path.DirectorySeparatorChar)}");
                    return Json(new { code = 1, msg = $"创建${chr_name}成功" });
                }
                catch (Exception)
                {

                    return Json(new { code = -1, msg = "创建失败" });
                }
            }
            else {
                string create_path = Path.Combine(rootPath, chr_name);

                if (System.IO.File.Exists(create_path))
                {
                    return Json(new { code = -1, msg = "该文件已经存在" });
                }
                try
                {

                   
                    ExtFile.WriteFile(create_path, "", System.Text.Encoding.UTF8);
                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"创建文件:{path.Replace('$', System.IO.Path.DirectorySeparatorChar)}");
                    return Json(new { code = 1, msg = $"创建${chr_name}成功" });
                }
                catch (Exception)
                {

                    return Json(new { code = -1, msg = "创建失败" });
                }
        
            }


        }


        

    }
}