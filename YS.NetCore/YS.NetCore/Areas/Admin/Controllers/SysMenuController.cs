﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysMenuController : Net.Mvc.BasicController<sys_catalog_entity, ISysCatalogService>
    {
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog")]
        public IActionResult Index()
        {
            return View();
        }


        private readonly IApplicationContextAccessor applicationContextAccessor;
        public SysMenuController(IApplicationContextAccessor _applicationContextAccessor, ISysCatalogService userService) : base(userService, _applicationContextAccessor)
        {
            applicationContextAccessor = _applicationContextAccessor;
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog",operate_code ="add,modify")]
        public IActionResult Add(string id)
        {

            sys_catalog_entity model = new sys_catalog_entity();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne(a => a.id == id.MyToLong());

            }
            return View(model);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog")]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>();
            var list = Service.GetPageList(page_index, page_size, "n_sort asc", condition);
            return PartialView("gv_List", list);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog",operate_code ="delete")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            bool deleted = Service.Delete(a => a.id == id) > 0;
            if (deleted)
            {

                AddLog("sys_catalog", Utils.LogsOperateTypeEnum.Delete, $"删除菜单栏目:{chr_name}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "add,modify")]
        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {

            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            string chr_code = data["chr_code"].MyToString();
            int n_sort = data["n_sort"].MyToInt();
            string description = data["description"].MyToString();
            StatusEnum status = (StatusEnum)data["status"].MyToInt();


            bool flag = false;

            if (id <= 0)
            {
                var check = Service.GetByOne(a => a.chr_code == chr_code);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "栏目代号已经存在" });
                }
                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Insert(new sys_catalog_entity()
                {

                    chr_name = chr_name,
                    chr_code = chr_code,
                    n_sort = n_sort,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now
                }).id > 0;
                if (flag)
                {

                    AddLog("sys_catalog", Utils.LogsOperateTypeEnum.Add, $"新增菜单栏目:{chr_name}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                var check = Service.GetByOne(a => a.chr_code == chr_code && a.id != id);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "栏目代号已经存在" });
                }
                flag = Service.Modify(a => a.id == id, a => new sys_catalog_entity()
                {
                    chr_name = chr_name,
                    chr_code = chr_code,
                    n_sort = n_sort,
                    description = description,
                    status = status,
                }) > 0;
                if (flag)
                {
                    AddLog("sys_catalog", Utils.LogsOperateTypeEnum.Update, $"修改菜单栏目:{chr_name}");
                    return Json(new { code = 0, msg = "修改成功" });
                }
                else
                {

                    return Json(new { code = -1, msg = "修改失败" });
                }
            }
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["account"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag = Service.Modify(a => a.id == id, a => new sys_catalog_entity()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("sys_catalog", Utils.LogsOperateTypeEnum.Update, $"修改菜单栏目状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}