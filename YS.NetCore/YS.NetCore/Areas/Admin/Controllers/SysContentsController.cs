﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysContentsController : Net.Mvc.BasicController<site_content, ISiteContentService>
    {
        private readonly ISiteFileProvider siteProvider;
        private readonly ISiteNodeService CmsNodes;
        private readonly ILogger<SysContentsController> _logger;
        private readonly ISiteContentService SysContent;
        IDynamicPermissionCheck PermissionCheck;


        public SysContentsController(
        ILogger<SysContentsController> logger,
        ISiteNodeService _CmsNodes,
        ISiteContentService _SysContent,
        IDynamicPermissionCheck _PermissionCheck,

               ISiteFileProvider _iteProvider, IApplicationContextAccessor _httpContextAccessor) : base(_SysContent, _httpContextAccessor)
        {
            PermissionCheck = _PermissionCheck;
            siteProvider = _iteProvider;
            _logger = logger;
            SysContent = _SysContent;
            CmsNodes = _CmsNodes;
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        public IActionResult Index()
        {
            return View();
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            long ParentNode = data["pid"].MyToLong();
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string lang = data["lang"].MyToString();
            var result = SysContent.GetNodeContentList(page_index, page_size, ParentNode, new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);
            return PartialView("gv_List", Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(result)));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [Route("/Admin/SysContents/Edit/{id}/{pid}")]
        public IActionResult Edit([FromServices] ISiteNodeFieldSetService site_node_fidld,string id = "", string pid = "0")
        {

            site_content model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();

            model.node_id = pid.MyToLong();
            sc_model.node_id = pid.MyToLong();
            en_model.node_id = pid.MyToLong();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = SysContent.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = SysContent.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = SysContent.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            else
            {
                model.node_id = pid.MyToLong();
            }
            if (model == null || model.id <= 0)
            {
                model = new site_content();
                model.node_id = pid.MyToLong();
            }
            var node = CmsNodes.Get("", a => a.id == model.node_id);

            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_content();
                sc_model.node_id = pid.MyToLong();
            }

            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_content();
                en_model.node_id = pid.MyToLong();
            }

            model.ext_model_code = "";
            if (node != null)
            {
                model.ext_model_code = node.ext_model.MyToString();
                sc_model.ext_model_code = node.ext_model.MyToString();
                en_model.ext_model_code = node.ext_model.MyToString();
            }

            var site_node_fields = site_node_fidld.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<site_node_field_set, bool>>>() {
                a=>a.node_code==node.node_code
            });
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(
                new { tc = model, sc = sc_model, en = en_model,field_list= site_node_fields }
                )));
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> SaveAsync(
            [FromServices]ISysTableidService tableid
            , [FromServices] Net.MyLucene.ISearchService searcher
            , [FromServices] ISiteModifyMarksService modify_mark
            , [FromServices] ISiteContentMarksService content_mark
            , [FromServices]IApplicationContextAccessor accessor

            , IFormCollection data)
        {
            long id = data["id"].MyToLong();
            long NodeId = data["NodeId"].MyToLong();
            StatusEnum IsHome = (StatusEnum)data["IsHome"].MyToInt();
            StatusEnum IsTop = (StatusEnum)data["IsTop"].MyToInt();
            StatusEnum IsNode = (StatusEnum)data["IsNode"].MyToInt();
            StatusEnum is_show = (StatusEnum)data["is_show"].MyToInt();
            int n_sort = data["n_sort"].MyToInt();
            int view_count = data["view_count"].MyToInt();
            string publish_date = data["publish_date"].MyToString();
            string tc_ChrTitle = data["tc_ChrTitle"].MyToString();
            string tc_link = data["tc_link"].MyToString();
            string tc_Summary = data["tc_Summary"].MyToString();
            string tc_ChrContent = data["tc_ChrContent"].MyToString();
            string tc_author = data["tc_author"].MyToString();
            string tc_source = data["tc_source"].MyToString();
            string tc_tag = data["tc_tag"].MyToString();
            string tc_cover = data["tc_cover"].MyToString();
            string tc_atlas = data["tc_atlas"].MyToString();
            string tc_down_file = data["tc_down_file"].MyToString();

            string sc_ChrTitle = data["sc_ChrTitle"].MyToString();
            string sc_link = data["sc_link"].MyToString();
            string sc_Summary = data["sc_Summary"].MyToString();
            string sc_ChrContent = data["sc_ChrContent"].MyToString();
            string sc_author = data["sc_author"].MyToString();
            string sc_source = data["sc_source"].MyToString();
            string sc_tag = data["sc_tag"].MyToString();
            string sc_cover = data["sc_cover"].MyToString();
            string sc_atlas = data["sc_atlas"].MyToString();
            string sc_down_file = data["sc_down_file"].MyToString();

            string en_ChrTitle = data["en_ChrTitle"].MyToString();
            string en_link = data["en_link"].MyToString();
            string en_Summary = data["en_Summary"].MyToString();
            string en_ChrContent = data["en_ChrContent"].MyToString();
            string en_author = data["en_author"].MyToString();
            string en_source = data["en_source"].MyToString();
            string en_tag = data["en_tag"].MyToString();
            string en_cover = data["en_cover"].MyToString();
            string en_atlas = data["en_atlas"].MyToString();
            string en_down_file = data["en_down_file"].MyToString();

            string tc_ExtendFieldsJson = data["tc_extends_field"].MyToString();
            string sc_ExtendFieldsJson = data["sc_extends_field"].MyToString();
            string en_ExtendFieldsJson = data["en_extends_field"].MyToString();

            if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + NodeId, "add,update"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            if (sc_ChrTitle == "" || NodeId <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (id <= 0)
            {
                int lang_flag = 1;

                long content_id = tableid.getNewTableId<site_content>();
                if (content_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }

                if (n_sort <= 0)
                {
                    int count = SysContent.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>()
                    {
                        a=>a.node_id==NodeId&&a.lang=="sc"
                    }).Count();
                    n_sort = count + 1;
                }


                var SiteNode = CmsNodes.Get("", a => a.id == NodeId && a.lang == "sc");
                if (SiteNode == null)
                {
                    return Json(new { code = -1, msg = "该栏目已不存在" });
                }

                site_content result = new site_content();
                if (tc_ChrTitle.Trim() != "")
                {
                    result = SysContent.Add(new site_content()
                    {

                        id = content_id,
                        lang = "tc",
                        lang_flag = lang_flag,
                        node_id = NodeId,
                        node_code = SiteNode.node_code,
                        title = tc_ChrTitle,
                        link = tc_link,
                        summary = tc_Summary,
                        chr_content = tc_ChrContent,
                        extend_fields_json = tc_ExtendFieldsJson,
                        cover = tc_cover,
                        atlas = tc_atlas,
                        n_sort = n_sort,
                        is_home = IsHome,
                        is_top = IsTop,
                        is_node = IsNode,
                        view_count = view_count,
                        status = StatusEnum.Legal,
                        review_status = SiteNode.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        create_time = DateTime.Now,
                        creator = accessor.Current.CurrentUser.id,
                        creator_name = accessor.Current.CurrentUser.alias_name,
                        tag = tc_tag,
                        author = tc_author,
                        source = tc_source,
                        del_flag = DeleteEnum.UnDelete,
                        is_show = is_show,
                        down_file = tc_down_file,
                        publish_date = publish_date
                    });

                    if (result.id > 0)
                    {
                        var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(result.lang, result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, result);

                            if (lang_flag == 2)
                            {
                                result.lang = "sc";
                                searcher.InsertContentIndex(up_lucene_model.node_code, result);
                            }
                        }
                    }
                }
                if (sc_ChrTitle.Trim() != "")
                {
                    result = SysContent.Add(new site_content()
                    {

                        id = content_id,
                        lang = "sc",
                        lang_flag = 1,
                        node_id = NodeId,
                        node_code = SiteNode.node_code,
                        title = sc_ChrTitle,
                        link = sc_link,
                        summary = sc_Summary,
                        chr_content = sc_ChrContent,
                        extend_fields_json = sc_ExtendFieldsJson,
                        cover = sc_cover,
                        atlas = sc_atlas,
                        n_sort = n_sort,
                        is_home = IsHome,
                        is_top = IsTop,
                        is_node = IsNode,
                        view_count = view_count,
                        status = StatusEnum.Legal,
                        review_status = SiteNode.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        create_time = DateTime.Now,
                        creator = accessor.Current.CurrentUser.id,
                        creator_name = accessor.Current.CurrentUser.alias_name,
                        tag = sc_tag,
                        author = sc_author,
                        source = sc_source,
                        del_flag = DeleteEnum.UnDelete,
                        is_show = is_show,
                        down_file = sc_down_file,
                        publish_date = publish_date
                    });
                    if (result.id > 0)
                    {
                        var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(result.lang, result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (en_ChrTitle.Trim() != "")
                {
                    result = SysContent.Add(new site_content()
                    {

                        id = content_id,
                        lang = "en",
                        lang_flag = 1,
                        node_id = NodeId,
                        node_code = SiteNode.node_code,
                        title = en_ChrTitle,
                        link = en_link,
                        summary = en_Summary,
                        chr_content = en_ChrContent,
                        extend_fields_json = en_ExtendFieldsJson,
                        cover = en_cover,
                        atlas = en_atlas,
                        n_sort = n_sort,
                        is_home = IsHome,
                        is_top = IsTop,
                        is_node = IsNode,
                        view_count = view_count,
                        status = StatusEnum.Legal,
                        review_status = SiteNode.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        create_time = DateTime.Now,
                        creator = accessor.Current.CurrentUser.id,
                        creator_name = accessor.Current.CurrentUser.alias_name,
                        tag = en_tag,
                        author = en_author,
                        source = en_source,
                        del_flag = DeleteEnum.UnDelete,
                        down_file = en_down_file,
                        is_show = is_show,
                        publish_date = publish_date
                    });

                    if (result.id > 0)
                    {
                        var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(result.lang, result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (result.id > 0)
                {
                    AddLog($"cms_content_g_{NodeId}", Utils.LogsOperateTypeEnum.Add, $"新增{SiteNode.node_name}栏目下内容:{sc_ChrTitle}");

                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                var SiteNode = CmsNodes.Get("", a => a.id == NodeId && a.lang == "sc");
                if (SiteNode == null)
                {
                    return Json(new { code = -1, msg = "该栏目已不存在" });
                }
                int lang_flag = 1;

                var modify = modify_mark.Insert(new site_modify_marks()
                {
                    chr_type = 2,
                    create_id = httpContextAccessor.Current.CurrentUser.id,
                    operate_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                    create_time = DateTime.Now,
                    create_name = httpContextAccessor.Current.CurrentUser.alias_name
                });
                int result = 0;

                var db_content_model = SysContent.GetByOne("row_key asc", a => a.id == id);
                #region 繁体修改
                //繁体修改
                if (tc_ChrTitle.Trim() != "")
                {

                    var tc_model = SysContent.Get("row_key asc", a => a.id == id && a.lang == "tc");
                    if (tc_model != null)
                    {
                        result = SysContent.Modify(a => a.id == id && a.lang == "tc", a => new site_content()
                        {
                            node_id= NodeId,
                            node_code= SiteNode.node_code,
                            lang_flag = lang_flag,
                            title = tc_ChrTitle,
                            link = tc_link,
                            summary = tc_Summary,
                            chr_content = tc_ChrContent,
                            extend_fields_json = tc_ExtendFieldsJson,
                            cover = tc_cover,
                            atlas = tc_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            tag = tc_tag,
                            author = tc_author,
                            source = tc_source,
                            down_file = tc_down_file,
                            is_show = is_show,
                            publish_date = publish_date

                        });
                        //更新索引
                        if (result > 0)
                        {
                            tc_model = SysContent.Get("row_key asc", a => a.id == id && a.lang == "tc");
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(tc_model.lang, tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateContentIndex(up_lucene_model.node_code, tc_model);
                                if (lang_flag == 2)
                                {
                                    tc_model.lang = "sc";
                                    searcher.UpdateContentIndex(up_lucene_model.node_code, tc_model);
                                }
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "tc",
                                lang_flag = lang_flag,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,

                                title = tc_model.title,
                                link = tc_model.link,
                                summary = tc_model.summary,
                                chr_content = tc_model.title,
                                extend_fields_json = tc_model.title,
                                cover = tc_model.title,
                                atlas = tc_model.title,
                                is_home = tc_model.is_home,
                                is_top = tc_model.is_top,
                                is_node = tc_model.is_node,
                                tag = tc_model.tag,
                                author = tc_model.author,
                                source = tc_model.source,
                                down_file = tc_model.down_file,
                                is_show = tc_model.is_show,
                                publish_date = tc_model.publish_date
                            });
                            //插入新数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                title = tc_ChrTitle,
                                link = tc_link,
                                summary = tc_Summary,
                                chr_content = tc_ChrContent,
                                extend_fields_json = tc_ExtendFieldsJson,
                                cover = tc_cover,
                                atlas = tc_atlas,
                                is_home = IsHome,
                                is_top = IsTop,
                                is_node = IsNode,
                                tag = tc_tag,
                                author = tc_author,
                                source = tc_source,
                                lang_flag = lang_flag,
                                down_file = tc_down_file,
                                is_show = is_show,
                                publish_date = publish_date
                            });
                        }
                    }
                    else
                    {
                        //插入繁体数据
                        var mod_tc_model= SysContent.Add(new site_content()
                        {

                            id = id,
                            lang = "tc",
                            lang_flag = lang_flag,
                            node_id = NodeId,
                            node_code = SiteNode.node_code,
                            title = tc_ChrTitle,
                            link = tc_link,
                            summary = tc_Summary,
                            chr_content = tc_ChrContent,
                            extend_fields_json = tc_ExtendFieldsJson,
                            cover = tc_cover,
                            atlas = tc_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            status = StatusEnum.Legal,
                            review_status = db_content_model.review_status,
                            create_time = DateTime.Now,
                            creator = accessor.Current.CurrentUser.id,
                            creator_name = accessor.Current.CurrentUser.alias_name,
                            tag = tc_tag,
                            author = tc_author,
                            source = tc_source,
                            del_flag = db_content_model.del_flag,
                            is_show = is_show,
                            down_file = tc_down_file,
                            publish_date = publish_date
                        });
                        if (mod_tc_model.id > 0)
                        {
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(mod_tc_model.lang, mod_tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertContentIndex(up_lucene_model.node_code, mod_tc_model);
                            }
                        }
                    }
                }
                else
                {
                    SysContent.Delete(a => a.id == id && a.lang == "tc");
                }
                #endregion

                //简体修改
                #region 简体修改
                if (sc_ChrTitle.Trim() != "")
                {
                    var sc_model = SysContent.GetByOne("row_key asc", a => a.id == id && a.lang == "sc");
                    if (sc_model != null)
                    {
                        //数据修改
                        result = SysContent.Modify(a => a.id == id && a.lang == "sc", a => new site_content()
                        {
                            node_id = NodeId,
                            node_code = SiteNode.node_code,
                            title = sc_ChrTitle,
                            link = sc_link,
                            summary = sc_Summary,
                            chr_content = sc_ChrContent,
                            extend_fields_json = sc_ExtendFieldsJson,
                            cover = sc_cover,
                            atlas = sc_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            tag = sc_tag,
                            author = sc_author,
                            source = sc_source,
                            down_file = sc_down_file,
                            is_show = is_show,
                            publish_date = publish_date
                        });
                        //更新索引
                        if (result > 0)
                        {
                            sc_model = SysContent.GetByOne("row_key asc", a => a.id == id && a.lang == "sc");
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(sc_model.lang, sc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateContentIndex(up_lucene_model.node_code, sc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "sc",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,

                                title = sc_model.title,
                                link = sc_model.link,
                                summary = sc_model.summary,
                                chr_content = sc_model.title,
                                extend_fields_json = sc_model.title,
                                cover = sc_model.title,
                                atlas = sc_model.title,
                                is_home = sc_model.is_home,
                                is_top = sc_model.is_top,
                                is_node = sc_model.is_node,
                                tag = sc_model.tag,
                                author = sc_model.author,
                                source = sc_model.source,
                                down_file = sc_model.down_file,
                                is_show = sc_model.is_show,
                                publish_date = sc_model.publish_date
                            });
                            //插入新数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                title = sc_ChrTitle,
                                link = sc_link,
                                summary = sc_Summary,
                                chr_content = sc_ChrContent,
                                extend_fields_json = sc_ExtendFieldsJson,
                                cover = sc_cover,
                                atlas = sc_atlas,
                                is_home = IsHome,
                                is_top = IsTop,
                                is_node = IsNode,
                                tag = sc_tag,
                                author = sc_author,
                                source = sc_source,
                                lang_flag = 1,
                                down_file = sc_down_file,
                                is_show = is_show,
                                publish_date = publish_date
                            });
                        }
                    }
                    else
                    {
                        var mod_sc_model = SysContent.Add(new site_content()
                        {
                            id = id,
                            lang = "sc",
                            lang_flag = 1,
                            node_id = NodeId,
                            node_code = SiteNode.node_code,
                            title = sc_ChrTitle,
                            link = sc_link,
                            summary = sc_Summary,
                            chr_content = sc_ChrContent,
                            extend_fields_json = sc_ExtendFieldsJson,
                            cover = sc_cover,
                            atlas = sc_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            status = StatusEnum.Legal,
                            review_status = db_content_model.review_status,
                            create_time = DateTime.Now,
                            creator = accessor.Current.CurrentUser.id,
                            creator_name = accessor.Current.CurrentUser.alias_name,
                            tag = sc_tag,
                            author = sc_author,
                            source = sc_source,
                            del_flag = db_content_model.del_flag,
                            is_show = is_show,
                            down_file = sc_down_file,
                            publish_date = publish_date
                        });
                        if (mod_sc_model.id > 0)
                        {
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(mod_sc_model.lang, mod_sc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertContentIndex(up_lucene_model.node_code, mod_sc_model);
                            }
                        }
                        if (modify.id > 0)
                        {
                            //插入新数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                title = sc_ChrTitle,
                                link = sc_link,
                                summary = sc_Summary,
                                chr_content = sc_ChrContent,
                                extend_fields_json = sc_ExtendFieldsJson,
                                cover = sc_cover,
                                atlas = sc_atlas,
                                is_home = IsHome,
                                is_top = IsTop,
                                is_node = IsNode,
                                tag = sc_tag,
                                author = sc_author,
                                source = sc_source,
                                lang_flag = 1,
                                down_file = sc_down_file,
                                is_show = is_show,
                                publish_date = publish_date
                            });
                        }
                    }
                }
                else {
                    //删除数据
                    SysContent.Delete(a => a.id == id && a.lang == "sc");
                }
                #endregion

                #region 英文修改
                if (en_ChrTitle.Trim() != "")
                {
                    var en_model = SysContent.GetByOne("row_key asc", a => a.id == id && a.lang == "en");
                    if (en_model != null)
                    {
                        //修改
                        result = SysContent.Modify(a => a.id == id && a.lang == "en", a => new site_content()
                        {
                            node_id = NodeId,
                            node_code = SiteNode.node_code,
                            title = en_ChrTitle,
                            link = en_link,
                            summary = en_Summary,
                            chr_content = en_ChrContent,
                            extend_fields_json = en_ExtendFieldsJson,
                            cover = en_cover,
                            atlas = en_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            tag = en_tag,
                            author = en_author,
                            source = en_source,
                            down_file = en_down_file,
                            is_show = is_show,
                            publish_date = publish_date
                        });
                        //索引
                        if (result > 0)
                        {
                            en_model = SysContent.GetByOne("row_key asc", a => a.id == id && a.lang == "en");
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(en_model.lang, en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateContentIndex(up_lucene_model.node_code, en_model);

                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,

                                title = en_model.title,
                                link = en_model.link,
                                summary = en_model.summary,
                                chr_content = en_model.title,
                                extend_fields_json = en_model.title,
                                cover = en_model.title,
                                atlas = en_model.title,
                                is_home = en_model.is_home,
                                is_top = en_model.is_top,
                                is_node = en_model.is_node,
                                tag = en_model.tag,
                                author = en_model.author,
                                source = en_model.source,
                                down_file = en_model.down_file,
                                is_show = en_model.is_show,
                                publish_date = en_model.publish_date
                            });
                            //插入新数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                title = en_ChrTitle,
                                link = en_link,
                                summary = en_Summary,
                                chr_content = en_ChrContent,
                                extend_fields_json = en_ExtendFieldsJson,
                                cover = en_cover,
                                atlas = en_atlas,
                                is_home = IsHome,
                                is_top = IsTop,
                                is_node = IsNode,
                                tag = en_tag,
                                author = en_author,
                                source = en_source,
                                lang_flag = 1,
                                down_file = en_down_file,
                                is_show = is_show,
                                publish_date = publish_date
                            });
                        }
                    }
                    else
                    {
                        var mod_en_model = SysContent.Add(new site_content()
                        {
                            id = id,
                            lang = "en",
                            lang_flag = 1,
                            node_id = NodeId,
                            node_code = SiteNode.node_code,
                            title = en_ChrTitle,
                            link = en_link,
                            summary = en_Summary,
                            chr_content = en_ChrContent,
                            extend_fields_json = en_ExtendFieldsJson,
                            cover = en_cover,
                            atlas = en_atlas,
                            n_sort = n_sort,
                            is_home = IsHome,
                            is_top = IsTop,
                            is_node = IsNode,
                            view_count = view_count,
                            status = StatusEnum.Legal,
                            review_status = db_content_model.review_status,
                            create_time = DateTime.Now,
                            creator = accessor.Current.CurrentUser.id,
                            creator_name = accessor.Current.CurrentUser.alias_name,
                            tag = en_tag,
                            author = en_author,
                            source = en_source,
                            del_flag = db_content_model.del_flag,
                            is_show = is_show,

                            down_file = en_down_file,
                            publish_date = publish_date
                        });
                        if (mod_en_model.id > 0)
                        {
                            var up_lucene_model = CmsNodes.GetUpLeaveLuceneNode(mod_en_model.lang, mod_en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertContentIndex(up_lucene_model.node_code, mod_en_model);
                            }
                        }
                        if (modify.id > 0)
                        {
                            //插入新数据
                            content_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                title = en_ChrTitle,
                                link = en_link,
                                summary = en_Summary,
                                chr_content = en_ChrContent,
                                extend_fields_json = en_ExtendFieldsJson,
                                cover = en_cover,
                                atlas = en_atlas,
                                is_home = IsHome,
                                is_top = IsTop,
                                is_node = IsNode,
                                tag = en_tag,
                                author = en_author,
                                source = en_source,
                                lang_flag = 1,
                                down_file = en_down_file,
                                is_show = is_show,
                                publish_date = publish_date
                            });
                        }
                    }
                }
                else {
                    //删除数据
                    SysContent.Delete(a => a.id == id && a.lang == "en");
                }
                #endregion

                if (result > 0)
                {
                    AddLog($"cms_content_g_{NodeId}", Utils.LogsOperateTypeEnum.Update, $"修改{SiteNode.node_name}栏目下内容:{sc_ChrTitle}");
                    return Json(new { code = 0, msg = "修改成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }
            }

        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public JsonResult DoSort([FromBody]JObject data)
        {
            var value = data["data"];
            foreach (var item in value)
            {
                if (item["id"].MyToLong() > 0)
                {
                    SysContent.Modify(a => a.id == item["id"].MyToLong(), a => new site_content()
                    {
                        n_sort = item["sort"].MyToInt()
                    });
                }
            }
            AddLog($"cms_content", Utils.LogsOperateTypeEnum.Update, $"栏目内容排序");
            return Json(new { code = 0, msg = "ok" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public JsonResult Sort(IFormCollection data)
        {
            long Id = data["id"].MyToLong();
            int type = data["type"].MyToInt();//1 up 2down
            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            var model = SysContent.Get("", a => a.id == Id);
            if (type == 1)
            {
                if (model.n_sort <= 1)
                {
                    return Json(new { code = -1, msg = "已经是第一无需再继续排序" });
                }
            }
            //非自己的其他同级
            var check = SysContent.GetList("", new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>() {
                a=>a.node_id==model.node_id&&a.id!=model.id
            });

            if (check.Count() <= 0)
            {
                return Json(new { code = -1, msg = "只存在一条同级记录无需排序" });
            }
            if (type == 2)
            {
                if (model.n_sort == check.Count() + 1)
                {
                    return Json(new { code = -1, msg = "已经是最后一条无需再继续排序" });
                }
            }
            if (type == 1)
            {
                var pre = check.Where(a => a.n_sort < model.n_sort).OrderBy(a => a.n_sort).LastOrDefault();
                if (pre != null)
                {
                    //前面一个往下面移动
                    SysContent.Modify(a => a.id == pre.id, a => new site_content()
                    {
                        n_sort = a.n_sort + 1
                    });

                    //自己往上面移动
                    SysContent.Modify(a => a.id == model.id, a => new site_content()
                    {
                        n_sort = a.n_sort - 1
                    });
                }
            }
            else
            {
                var next = check.Where(a => a.n_sort > model.n_sort).OrderBy(a => a.n_sort).FirstOrDefault();
                if (next != null)
                {
                    //下面一个往上面移动
                    SysContent.Modify(a => a.id == next.id, a => new site_content()
                    {
                        n_sort = a.n_sort - 1
                    });
                    //自己往下面移动
                    SysContent.Modify(a => a.id == model.id, a => new site_content()
                    {
                        n_sort = a.n_sort + 1
                    });
                }
            }
            return Json(new { code = 0, msg = "ok" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> DeleteAsync(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string account = data["NodeName"].MyToString();

            long Node_id = data["Node_id"].MyToLong();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            var model = SysContent.GetByOne(a => a.id == id && a.lang == "sc");
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + model.node_id, "delete"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            int flag = SysContent.Modify(a => a.id == id, a => new site_content() { del_flag = DeleteEnum.Delete });
            if (flag > 0)
            {

                AddLog($"cms_content_g_{Node_id}", Utils.LogsOperateTypeEnum.Delete, $"删除栏目内容:{account}");
                return Json(new { code = 1, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> changeAsync(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();

            long node_id = data["node_id"].MyToLong();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + node_id, "update"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag = Service.Modify(a => a.id == id, a => new site_content()
            {
                is_show = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_content_g_{id}", Utils.LogsOperateTypeEnum.Update, $"修改网站内容显示状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}