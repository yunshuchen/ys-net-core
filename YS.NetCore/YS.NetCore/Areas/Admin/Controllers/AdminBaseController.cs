﻿using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.NetCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("Admin/[controller]/{action=Index}")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class AdminBaseController : Controller
    {
      
    }
}
