using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysOperateController : Net.Mvc.BasicController<sys_operate_entity, ISysOperateService>
    {
        private readonly ISysResourceService sys_resource;
        private readonly ISysModuleService sys_module;

        private readonly IApplicationContextAccessor applicationContextAccessor;
        public SysOperateController(IApplicationContextAccessor _applicationContextAccessor
            , ISysResourceService _sys_resource
            , ISysModuleService _sys_module
            , ISysOperateService userService) : base(userService, _applicationContextAccessor)
        {
            sys_resource = _sys_resource;
            sys_module = _sys_module;
            applicationContextAccessor = _applicationContextAccessor;
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "")]
        public IActionResult Index(string catalog_id)
        {
            return View("Index", catalog_id);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "add,modify")]
        public IActionResult Add(string res_id, string id)
        {

            sys_operate_entity model = new sys_operate_entity();

            if (!string.IsNullOrEmpty(res_id) && res_id.MyToLong() > 0)
            {
                model.p_res = res_id.MyToLong();
            }
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne(a => a.id == id.MyToLong());

            }
            return View(model);
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "")]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();

            long catalog_id = data["catalog_id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            long opt_catalog_id = data["opt_catalog_id"].MyToLong();


            List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>>();


            if (catalog_id > 0)
            {
                condition.Add(a => a.p_res == catalog_id);
            }
            else
            {
                if (opt_catalog_id > 0)
                {
                    condition.Add(a => a.p_res == catalog_id);
                }
            }

            if (chr_name.Trim().Length > 0)
            {
                condition.Add(a => a.operate_name.Contains(chr_name));
            }

            var list = Service.GetPageList(page_index, page_size, "n_sort asc", condition);
            return PartialView("gv_List", list);
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "delete")]
        [HttpPost]
        public JsonResult Delete(IFormCollection data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            bool deleted = Service.Modify(a => a.id == id, a => new sys_operate_entity()
            {
                delete_status = DeleteEnum.Delete
            }) > 0;
            if (deleted)
            {
                AddLog("sys_operate", Utils.LogsOperateTypeEnum.Delete, $"删除菜单操作:{chr_name}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }

        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "add,modify")]
        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {

            long id = data["id"].MyToLong();
            long p_module = data["p_module"].MyToLong();
            string res_code = data["res_code"].MyToString();
            string res_name = data["res_name"].MyToString();
            string api_url = data["api_url"].MyToString();
   
            string description = data["description"].MyToString();
            int n_sort = data["n_sort"].MyToInt();
            StatusEnum status = (StatusEnum)data["status"].MyToInt();
            StatusEnum is_menu = (StatusEnum)data["is_menu"].MyToInt();
            bool flag=false;
            
            if (id <= 0)
            {
                var check = Service.GetByOne(a => a.operate_code == res_code
                && a.p_res == p_module);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该资源下该操作代号已经存在" });
                }

                var  res=sys_resource.GetByOne("", a => a.id == p_module);
                if (res == null)
                {
                    return Json(new { code = -1, msg = "该资源已不存在" });
                }
                var module = sys_module.GetByOne("", a => a.id == res.p_module);
                if (module == null)
                {
                    return Json(new { code = -1, msg = "该模块已不存在" });
                }
                

                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Insert(new sys_operate_entity()
                {

                    p_res = p_module,
                    operate_code = res_code,
                    p_res_code= res.res_code,
                    p_module= module.id,
                    p_module_code= module.module_code,
                    operate_name = res_name,
                    is_menu=is_menu,
                    api_url= api_url,
                    n_sort = n_sort,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now

                }).id > 0;
                if (flag)
                {
                    AddLog("sys_operate", Utils.LogsOperateTypeEnum.Add, $"新增菜单资源操作,资源{res_name},操作:{res_name}");
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {

                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                var check = Service.GetByOne(a => a.operate_code == res_code
                && a.p_res == p_module&&a.id!=id);

                if (check != null)
                {
                    return Json(new { code = -1, msg = "该资源下该操作代号已经存在" });
                }
                var user = applicationContextAccessor.Current.CurrentUser;
                flag = Service.Modify(a => a.id == id, a => new sys_operate_entity()
                {

                    p_res = p_module,
                    operate_code = res_code,
                    operate_name = res_name,
                    is_menu = is_menu,
                    api_url = api_url,
                    n_sort = n_sort,
                    description = description,
                    status = status,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now

                }) > 0;
                if (flag)
                {
                    AddLog("sys_operate", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源操作,资源{res_name},操作:{res_name}");
                    return Json(new { code = 0, msg = "修改成功" });
                }
                else
                {

                    return Json(new { code = -1, msg = "修改失败" });
                }
            }

    
        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "change_status")]
        [HttpPost]
        public JsonResult change(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_operate_entity()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {

                AddLog("sys_operate", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源操作状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }
    }
}
