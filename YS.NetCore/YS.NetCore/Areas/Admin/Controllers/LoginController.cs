﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;



namespace YS.NetCore.Areas.Admin.Controllers
{

    public class LoginController : Net.Mvc.BasicController<sys_account, ISysAccountService>
    {
        IHttpContextAccessor _httpContextAccessor;

        ISysAccountService sysaccount;
        IMd5Hash md5Hash;
        public LoginController(IHttpContextAccessor httpContextAccessor
            , IApplicationContextAccessor _applicationContextAccessor,
            ISysAccountService _sysaccount,
            IMd5Hash _md5Hash
            ):base(_sysaccount, _applicationContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            sysaccount = _sysaccount;
            md5Hash = _md5Hash;
        }
        [AllowAnonymous]
        // GET: Login
        public ActionResult Index()
        {
            var admin_lang = "";
            Request.Cookies.TryGetValue("admin_lang", out admin_lang);
            string culture = admin_lang == "" ? "sc" : admin_lang;
            return View("index1", culture);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult CaptchaImg([FromServices]ISiteFileProvider siteProvider)
        {

            var guid = Guid.NewGuid().ToString("N");
            _httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, guid.ToLower());
            List<string> imageManagerAllowFiles = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
            var SearchExtensions = imageManagerAllowFiles.Select(x => x.ToLower()).ToArray();


            string BasePath = Path.Combine(siteProvider.StaticBasePath, "SliderCaptchaImgages");

            var buildingList = new List<String>();

            buildingList.AddRange(Directory.GetFiles(BasePath, "*", SearchOption.AllDirectories)
           .Where(x => SearchExtensions.Contains(Path.GetExtension(x).ToLower()))
           .Select(x => x.Substring(BasePath.Length + 1).Replace("\\", "/").Replace(@"\", "/")));

            Random rand = new Random();
            var rd = rand.Next(buildingList.Count);
            if (rd <= buildingList.Count - 1)
            {
                var rd_img = buildingList[rd];

                string file_path = Path.Combine(siteProvider.StaticBasePath, "SliderCaptchaImgages", rd_img);
                if (!System.IO.File.Exists(file_path))
                {
                    return new NotFoundResult();
                }

                string mime = MimeMapping.MimeUtility.GetMimeMapping(file_path);
                FileStream objStream = new FileStream(file_path, FileMode.Open, FileAccess.Read, FileShare.Read);
                return File(objStream, mime);
            }
            else
            {
                string code = Net.Common.ValidateImg.CreateVerifyCode();

                Bitmap image = Net.Common.ValidateImg.CreateImageCode(code);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, ImageFormat.Png);
                return File(ms.ToArray(), @"image/jpeg");
            }

        }


        /// <summary>
        /// 服务器端滑块验证方法
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Verify([FromBody]List<int> datas)
        {
            var sum = datas.Sum();
            var avg = sum * 1.0 / datas.Count;
            var stddev = datas.Select(v => Math.Pow(v - avg, 2)).Sum() / datas.Count;


            var flag = stddev > 0;
            var guid = "";
            if (flag)
            {
                guid = Guid.NewGuid().ToString("N");
                _httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, guid.ToLower());
            }
            else
            {
                _httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, "");
            }
            return Json(new
            {
                code = flag ? 0 : -1,
                msg = "",
                data = new
                {
                    captcha = guid.ToLower()
                }
            });
        }
        [AllowAnonymous]
        public ActionResult GetValidateCode()
        {

            Response.Clear();

            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            var randValue = rand.Next(0, 10000);
            Bitmap image;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            string code = "";
            string code_resilt = "";


            image = Net.Common.ValidateImg.CreateImageCode(code);

            var rdm = randValue % 3;
            if (rdm == 0)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateMyImageCode(code);
            }
            else if (rdm == 1)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            else
            {
                var result = Net.Common.ValidateImg.CreateVerifyExpression();
                code = result.Item1;
                code_resilt = result.Item2;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            image.Save(ms, ImageFormat.Png);
            _httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, code_resilt.ToLower());
            return File(ms.ToArray(), @"image/jpeg");
        }

        [AllowAnonymous]
        [HttpPost]
        public  JsonResult SetLang(IFormCollection data)
        {
            string lang = data["lang"].MyToString().Trim();

            HttpContext.Response.Cookies.Append("admin_lang", lang);

            return new JsonResult(new { code = 0 });
        }
        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> InAsync([FromServices] IMyCache cache, IFormCollection data)
        {
            string account = data["account"].MyToString().Trim();
            string pwd = data["pwd"].MyToString().Trim();
            string safecode = data["code"].MyToString().Trim();
            string lang = data["lang"].MyToString().Trim();
            var result = sysaccount.UserLogin(account, pwd, safecode);

            if (result.GetModelValue<int>("code") == 0)
            {
                var login_account = result.GetModelValue<sys_account>("user");
                login_account.AuthenticationType = DefaultAuthorizeAttribute.DefaultAuthenticationScheme;
                var identity = new ClaimsIdentity(login_account);
                identity.AddClaim(new Claim(ClaimTypes.Name, login_account.account));
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, login_account.id.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Hash, login_account.token.ToString()));
                await HttpContext.SignInAsync(DefaultAuthorizeAttribute.DefaultAuthenticationScheme, new ClaimsPrincipal(identity));;
                AddLog("login", LogsOperateTypeEnum.Login, $"{account}登录系统", "登录", login_account.id, login_account.alias_name);
                HttpContext.Response.Cookies.Append("admin_lang", lang);
                cache.Set<sys_account>(string.Format(CacheKey.UserLoginInfoCackeKey, login_account.id, login_account.token), login_account);
                httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.Set<sys_account>(Net.SessionKeyProvider.AdminLoginLoginUser, login_account);
            }
            return new JsonResult(result);
        }
        [AllowAnonymous]
        public async Task<ActionResult> Out([FromServices]IApplicationContext app_content, [FromServices]IOnlineUserCache online_user_cache,string returnurl)
        {
            IUser currentUser = null;
            if (app_content.CurrentUser != null)
            {
                currentUser = app_content.CurrentUser;
            }
            bool remove = false;
            //用户不存在
            if (currentUser == null || currentUser.id <= 0 || currentUser.token.MyToString() == "")
            {
                remove = true;
            }
            if (!remove)
            {
                string cache_key = string.Format("#{0}#{1}#", currentUser.token, currentUser.id);
                online_user_cache.RemoveOnlineUser(cache_key);
            }
            await HttpContext.SignOutAsync(DefaultAuthorizeAttribute.DefaultAuthenticationScheme);
            return Redirect(returnurl ?? "~/Admin/Login");
        }

    }
}