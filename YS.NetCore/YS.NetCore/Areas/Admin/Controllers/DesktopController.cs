﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace YS.NetCore.Areas.Admin.Controllers
{
    public class DesktopController : Net.Mvc.BasicController<site_basedata, ISiteBasedataService>
    {
        public DesktopController(ISiteBasedataService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        // GET: Desktop
        public ActionResult Index()
        {
            return View();
        }

        [Route("get/{doc_type}")]
        public ActionResult Details(string doc_type)
        {
            

            return View();
        }
       
     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Desktop/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Desktop/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Desktop/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Desktop/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}