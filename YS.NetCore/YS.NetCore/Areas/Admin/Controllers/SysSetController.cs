﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysSetController : Net.Mvc.BasicController<ApplicationSetting, IApplicationSettingService>
    {


        IApplicationSettingService AppSite;
        ISiteFileProvider siteProvider;
        private readonly ILogger<SysSetController> _logger;

        ISiteNodeService CmsNodes;

        private readonly ISysResourceService sys_resource;
        private readonly ISysOperateService sys_operate;
        public SysSetController(
            YS.Net.Setting.IApplicationSettingService _AppSite,
            ISiteNodeService _CmsNodes,
                 ISysResourceService _sys_resource,
            ISysOperateService _sys_operate,
            ISiteFileProvider _siteProvider, ILogger<SysSetController> logger
            , IApplicationContextAccessor _httpContextAccessor) : base(_AppSite, _httpContextAccessor)
        {
            CmsNodes = _CmsNodes;
            siteProvider = _siteProvider;
            _logger = logger;
            AppSite = _AppSite;

            sys_resource = _sys_resource;
            sys_operate = _sys_operate;
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_setting", resource_code = "sys_setting")]
        public IActionResult Index()
        {
            return View();
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_setting", resource_code = "sys_setting", operate_code = "mod")]
        [HttpPost]
        public JsonResult UploadFile(IFormCollection data)
        {
            string type = data["type"].MyToString();
            string module = data["module"].MyToString();
            if (data.Files.Count <= 0)
            {
                return Json(new { code = -1, msg = "请选择上传文件" });
            }
            string old_file_name = AppSite.Get(type, "");
            string BasePath = siteProvider.StaticBasePath;
            string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
            string save_path = Path.Combine(BasePath, new_name);
            data.Files[0].SaveAs(save_path);
            int flag = AppSite.Modify(type, "/" + siteProvider.StaticDirectory + "/" + new_name);
            if (flag > 0)
            {
                if (old_file_name.Trim() != "")
                {
                    try
                    {
                        System.IO.File.Delete(Path.Combine(BasePath, old_file_name));
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "上传文件时删除旧文件失败");
                    }
                }
                return Json(new { code = 0, msg = "上传成功", path = "/" + siteProvider.StaticDirectory + "/" + new_name });
            }
            return Json(new { code = -1, msg = "上传失败" });
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "sys_setting", resource_code = "sys_setting", operate_code = "mod")]
        [HttpPost]
        public JsonResult SaveSiteSet(IFormCollection data)
        {
            string tc_SiteName = data["tc_SiteName"].MyToString();
            string sc_SiteName = data["sc_SiteName"].MyToString();
            string en_SiteName = data["en_SiteName"].MyToString();
            string TplCode = data["TplCode"].MyToString();
            string SiteMetaKeywords = data["SiteMetaKeywords"].MyToString();
            string EditorType = data["EditorType"].MyToString(); 


            AppSite.Modify("tc_SiteName", tc_SiteName);
            AppSite.Modify("sc_SiteName", sc_SiteName);
            AppSite.Modify("en_SiteName", en_SiteName);
            AppSite.Modify("TplCode", TplCode);
            AppSite.Modify("EditorType", EditorType);
            



            var stmp_client_str = AppSite.Get("stmp_client", "");

            var client = JsonHelper.ToObject<YS.Utils.SMTP.SmtpSetting>(stmp_client_str);

            if (client == null)
            {

                client = new YS.Utils.SMTP.SmtpSetting()
                {
                    Email = "",
                    EnableSsl = false,
                    Host = "",
                    PassWord = "",
                    Port = 0
                };
            }

            string Email_Host = data["Email_Host"].MyToString();
            int Email_Port = data["Email_Port"].MyToInt();
            string Email_Email = data["Email_Email"].MyToString();
            string Email_PassWord = data["Email_PassWord"].MyToString();
            int Email_EnableSsl = data["Email_EnableSsl"].MyToInt();
           
            client.Email = Email_Email;
            client.PassWord = Email_PassWord;
            client.Port = Email_Port;
            client.EnableSsl = Email_EnableSsl == 1;
            client.Host = Email_Host;
            AppSite.Modify("stmp_client", JsonHelper.ToJson(client));
            AppSite.Modify("SiteMetaKeywords", SiteMetaKeywords);

            
            AddLog("sys_setting", Utils.LogsOperateTypeEnum.Update, $"修改菜系统设置:网站名:{tc_SiteName},模版代号:{TplCode}");
            return Json(new { code = 0, msg = "修改成功" });
        }


        [HttpPost]
        public JsonResult InitPermission(IFormCollection data)
        {
            var list = CmsNodes.GetList();

            foreach (var a in list)
            {
                string res_code = $"cms_node_g_{a.id}";
                var opeart = sys_resource.GetByOne(b => b.p_module == 5 && b.res_code == res_code);
                if (opeart == null)
                {
                    #region 栏目相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 5;
                        string module_code = "site_node";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_node_g_{a.id}",
                            res_name = $"栏目管理--{a.node_name}({a.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion

                    #region 内容相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 6;
                        string module_code = "site_content";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_content_g_{a.id}",
                            res_name = $"内容管理--{a.node_name}({a.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion
                }
            }



            return Json(new { code = 0, msg = "修改成功" });
        }


        [HttpPost]
        public JsonResult InitLuceneIndexs([FromServices]ISiteNodeService _CmsNodes, [FromServices] Net.MyLucene.ISearchService searcher, IFormCollection data)
        {

            List<LuceneModel> tc_list = new List<LuceneModel>();
            List<LuceneModel> sc_list = new List<LuceneModel>();
            List<LuceneModel> en_list = new List<LuceneModel>();
            var node_list = Service.GetList<site_node>("row_key desc", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() { });
            foreach (var node in node_list)
            {
                var up_lucene_model = _CmsNodes.GetUpLeaveLuceneNode(node.lang, node.node_code);
                if (up_lucene_model != null)
                {

                    if (node.lang == "tc")
                    {
                        tc_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Node,
                            chr_content = node.node_desc,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.node_remark,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_list,
                            publish_date = node.create_time,
                            title = node.node_name
                        });

                        if (node.lang_flag == 2)
                        {
                            sc_list.Add(new LuceneModel()
                            {
                                lang = "sc",
                                chr_type = LuceneContentType.Node,
                                chr_content = node.node_desc,
                                code = node.node_code,
                                node_code = up_lucene_model.node_code,
                                desc = node.node_remark,
                                id = node.id,
                                row_key = node.row_key,
                                file_path = node.down_list,
                                publish_date = node.create_time,

                            });
                        }

                    }
                    if (node.lang == "sc")
                    {
                        sc_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Node,
                            chr_content = node.node_desc,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.node_remark,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_list,
                            publish_date = node.create_time,
                            title = node.node_name
                        });
                    }

                    if (node.lang == "en")
                    {
                        en_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Node,
                            chr_content = node.node_desc,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.node_remark,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_list,
                            publish_date = node.create_time,
                            title = node.node_name
                        });
                    }

                }
            }





            var content_list = Service.GetList<site_content>("row_key desc", new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>() { });

            foreach (var node in content_list)
            {
                var up_lucene_model = _CmsNodes.GetUpLeaveLuceneNode(node.lang, node.node_code);
                if (up_lucene_model != null)
                {

                    DateTime publish_date = DateTime.Now;
                    DateTime.TryParse(node.publish_date, out publish_date);
                    if (node.lang == "tc")
                    {
                        tc_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Content,
                            chr_content = node.chr_content,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.summary,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_file,
                            publish_date = publish_date,
                            title = node.title
                        });

                        if (node.lang_flag == 2)
                        {
                            sc_list.Add(new LuceneModel()
                            {
                                lang = "sc",
                                chr_type = LuceneContentType.Content,
                                chr_content = node.chr_content,
                                code = node.node_code,
                                node_code = up_lucene_model.node_code,
                                desc = node.summary,
                                id = node.id,
                                row_key = node.row_key,
                                file_path = node.down_file,
                                publish_date = publish_date,
                                title = node.title
                            });
                        }
                    }
                    if (node.lang == "sc")
                    {
                        sc_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Content,
                            chr_content = node.chr_content,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.summary,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_file,
                            publish_date = publish_date,
                            title = node.title
                        });
                    }
                    if (node.lang == "en")
                    {
                        en_list.Add(new LuceneModel()
                        {
                            lang = node.lang,
                            chr_type = LuceneContentType.Content,
                            chr_content = node.chr_content,
                            code = node.node_code,
                            node_code = up_lucene_model.node_code,
                            desc = node.summary,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.down_file,
                            publish_date = publish_date,
                            title = node.title
                        });

                    }

                }
            }
            var doc_list = Service.GetList<site_doc>("row_key desc", new List<System.Linq.Expressions.Expression<Func<site_doc, bool>>>() { });


            foreach (var node in doc_list)
            {
                DateTime publish_date = DateTime.Now;
                DateTime.TryParse(node.date, out publish_date);
                if (node.lang == "tc")
                {
                    tc_list.Add(new LuceneModel()
                    {
                        lang = node.lang,
                        chr_type = LuceneContentType.Doc,
                        chr_content = node.chr_content,
                        code = node.module,
                        node_code = node.node_code,
                        desc = node.chr_desc,
                        id = node.id,
                        row_key = node.row_key,
                        file_path = node.file_link,
                        publish_date = publish_date,
                        title = node.name
                    });
                    if (node.lang_flag == 2)
                    {
                        sc_list.Add(new LuceneModel()
                        {
                            lang = "sc",
                            chr_type = LuceneContentType.Doc,
                            chr_content = node.chr_content,
                            code = node.module,
                            node_code = node.node_code,
                            desc = node.chr_desc,
                            id = node.id,
                            row_key = node.row_key,
                            file_path = node.file_link,
                            publish_date = publish_date,
                            title = node.name
                        });

                    }
                }
                else if (node.lang == "sc")
                {
                    sc_list.Add(new LuceneModel()
                    {
                        lang = node.lang,
                        chr_type = LuceneContentType.Doc,
                        chr_content = node.chr_content,
                        code = node.module,
                        node_code = node.node_code,
                        desc = node.chr_desc,
                        id = node.id,
                        row_key = node.row_key,
                        file_path = node.file_link,
                        publish_date = publish_date,
                        title = node.name
                    });
                }
                else if (node.lang == "en")
                {
                    en_list.Add(new LuceneModel()
                    {
                        lang = node.lang,
                        chr_type = LuceneContentType.Doc,
                        chr_content = node.chr_content,
                        code = node.module,
                        node_code = node.node_code,
                        desc = node.chr_desc,
                        id = node.id,
                        row_key = node.row_key,
                        file_path = node.file_link,
                        publish_date = publish_date,
                        title = node.name
                    });
                }

            }




            searcher.InitBuildIndex("tc", tc_list);
            searcher.InitBuildIndex("sc", sc_list);
            searcher.InitBuildIndex("en", en_list);
            return Json(new { code = 0, msg = "ok" });

        }
        
    }
}