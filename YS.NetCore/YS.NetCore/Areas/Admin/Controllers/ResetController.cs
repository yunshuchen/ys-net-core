﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils.Encrypt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{

    public class ResetController : Net.Mvc.BasicController<sys_account, ISysAccountService>
    {
        public ResetController(ISysAccountService service, IApplicationContextAccessor _httpContextAccessor) :
                   base(service, _httpContextAccessor)
        {

        }
        // GET: Login
        public ActionResult Index(string account)
        {
            return View("Index", account);
        }
        [Route("/admin/resetPwd")]
        [Net.Mvc.Authorize.DefaultAuthorize]
        // GET: Login
        public ActionResult resetPwd()
        {
            string account = httpContextAccessor.Current.CurrentUser.account;
            return View("resetPwd", account);
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult CaptchaImg([FromServices] ISiteFileProvider siteProvider)
        {

            var guid = Guid.NewGuid().ToString("N");
            httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, guid.ToLower());
            List<string> imageManagerAllowFiles = new List<string>() { ".png", ".jpg", ".jpeg", ".gif" };
            var SearchExtensions = imageManagerAllowFiles.Select(x => x.ToLower()).ToArray();

            string BasePath = Path.Combine(siteProvider.StaticBasePath, "SliderCaptchaImgages");

            var buildingList = new List<String>();

            buildingList.AddRange(Directory.GetFiles(BasePath, "*", SearchOption.AllDirectories)
           .Where(x => SearchExtensions.Contains(Path.GetExtension(x).ToLower()))
           .Select(x => x.Substring(BasePath.Length + 1).Replace("\\", "/").Replace(@"\", "/")));

            Random rand = new Random();
            var rd = rand.Next(buildingList.Count);
            if (rd <= buildingList.Count - 1)
            {
                var rd_img = buildingList[rd];

                string file_path = Path.Combine(siteProvider.StaticBasePath, "SliderCaptchaImgages", rd_img);
                if (!System.IO.File.Exists(file_path))
                {
                    return new NotFoundResult();
                }

                string mime = MimeMapping.MimeUtility.GetMimeMapping(file_path);
                FileStream objStream = new FileStream(file_path, FileMode.Open, FileAccess.Read, FileShare.Read);
                return File(objStream, mime);
            }
            else
            {
                string code = Net.Common.ValidateImg.CreateVerifyCode();

                Bitmap image = Net.Common.ValidateImg.CreateImageCode(code);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, ImageFormat.Png);
                return File(ms.ToArray(), @"image/jpeg");
            }

        }


        /// <summary>
        /// 服务器端滑块验证方法
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]

        public ActionResult Verify([FromBody] List<int> datas)
        {
            var sum = datas.Sum();
            var avg = sum * 1.0 / datas.Count;
            var stddev = datas.Select(v => Math.Pow(v - avg, 2)).Sum() / datas.Count;


            var flag = stddev > 0;
            var guid = "";
            if (flag)
            {
                guid = Guid.NewGuid().ToString("N");
                httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, guid.ToLower());
            }
            else
            {
                httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, "");
            }
            return Json(new
            {
                code = flag ? 0 : -1,
                msg = "",
                data = new
                {
                    captcha = guid.ToLower()
                }
            });
        }
        [AllowAnonymous]
        public ActionResult GetValidateCode()
        {

            Response.Clear();

            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            var randValue = rand.Next(0, 10000);
            Bitmap image;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            string code = "";
            string code_resilt = "";


            image = Net.Common.ValidateImg.CreateImageCode(code);

            var rdm = randValue % 3;
            if (rdm == 0)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateMyImageCode(code);
            }
            else if (rdm == 1)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            else
            {
                var result = Net.Common.ValidateImg.CreateVerifyExpression();
                code = result.Item1;
                code_resilt = result.Item2;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            image.Save(ms, ImageFormat.Png);
            httpContextAccessor.Current.HttpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminRestPwdSafeCodeKey, code_resilt.ToLower());
            return File(ms.ToArray(), @"image/jpeg");
        }
        [HttpPost]
        public JsonResult Save(IFormCollection data)
        {
            string account = data["account"].MyToString().Trim();
            string old_pwd = data["old_pwd"].MyToString().Trim();
            string new_pwd = data["new_pwd"].MyToString().Trim();
            string safecode = data["code"].MyToString().Trim();
            var result = Service.UserModifyPwd(account, old_pwd, new_pwd, safecode);
            return new JsonResult(result);


        }


    }
}