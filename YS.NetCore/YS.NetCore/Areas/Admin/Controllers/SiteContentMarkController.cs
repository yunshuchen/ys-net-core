﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteContentMarkController : Net.Mvc.BasicController<site_modify_marks, ISiteModifyMarksService>
    {

        public SiteContentMarkController(ISiteModifyMarksService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult ViewData(string id)
        {
            site_content_marks model = new site_content_marks();
            site_content_marks sc_model = new site_content_marks();
            site_content_marks en_model = new site_content_marks();


            site_content_marks af_model = new site_content_marks();
            site_content_marks af_sc_model = new site_content_marks();
            site_content_marks af_en_model = new site_content_marks();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "tc" && a.data_type == 1);
                sc_model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "sc" && a.data_type == 1);
                en_model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "en" && a.data_type == 1);

                af_model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "tc" && a.data_type == 2);
                af_sc_model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "sc" && a.data_type == 2);
                af_en_model = Service.GetByOne<site_content_marks>("id asc", a => a.mark_id == id.MyToLong() && a.lang == "en" && a.data_type == 2);
            }
            if (model == null || model.id <= 0)
            {
                model = new site_content_marks();
            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_content_marks();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_content_marks();
            }

            if (af_model == null || af_model.id <= 0)
            {
                af_model = new site_content_marks();
            }
            if (af_sc_model == null || af_sc_model.id <= 0)
            {
                af_sc_model = new site_content_marks();

            }
            if (af_en_model == null || af_en_model.id <= 0)
            {
                af_en_model = new site_content_marks();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new
            {
                tc = model,
                sc = sc_model,
                en = en_model,
                af_tc = af_model,
                af_sc = af_sc_model,
                af_en = af_en_model
            })));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string title = data["title"].MyToString();
            string modiftor = data["modiftor"].MyToString();
            string lang = data["lang"].MyToString();
            List<Expression<Func<site_modify_marks, bool>>> predicate = new List<Expression<Func<site_modify_marks, bool>>>();
            if (modiftor != "")
            {
                predicate.Add(a => a.create_name.Contains(modiftor));
            }
            var result = Service.GetContentModifyMarkPage(page_index, page_size, predicate, title, lang);
            return PartialView("gv_List", Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(result)));
        }


    }
}