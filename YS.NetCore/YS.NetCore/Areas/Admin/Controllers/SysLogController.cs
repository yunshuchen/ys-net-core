﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysLogController : Net.Mvc.BasicController<sys_logs, ISysLogsService>
    {

        public SysLogController(ISysLogsService service, IApplicationContextAccessor _httpContextAccessor) : base(service,_httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string user_name = data["user_name"].MyToString();
            string module_name = data["module_name"].MyToString();
            int operate_type = data["operate_type"].MyToInt();
            List<System.Linq.Expressions.Expression<Func<sys_logs, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_logs, bool>>>();
            if (user_name.Trim() != "")
            {
                condition.Add(a => a.log_user_name.Contains(user_name));
            }
            if (module_name.Trim() != "")
            {
                condition.Add(a => a.log_module_name.Contains(module_name));
            }
            if (operate_type > 0)
            {
                condition.Add(a => a.operate_type == (Utils.LogsOperateTypeEnum)operate_type);
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return PartialView("gv_List", list);
        }

       
             
    }
}