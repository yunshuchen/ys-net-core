﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SiteNodesController : Net.Mvc.BasicController<site_node, ISiteNodeService>
    {
        IApplicationSettingService AppSite;
        ISiteFileProvider siteProvider;
        ISiteNodeService CmsNodes;
        private readonly ILogger<SiteNodesController> _logger;
        private readonly ISysResourceService sys_resource;
        private readonly ISysOperateService sys_operate;
        private readonly ISiteNodeFieldSetService site_node_fidld;
        IDynamicPermissionCheck PermissionCheck;
        public SiteNodesController(
            YS.Net.Setting.IApplicationSettingService _AppSite,
            ISiteNodeService _CmsNodes
            , IDynamicPermissionCheck _PermissionCheck
            , ISiteFileProvider _siteProvider
            , ILogger<SiteNodesController> logger
            , ISysResourceService _sys_resource
            , ISiteNodeFieldSetService _site_node_fidld
            , ISysOperateService _sys_operate, IApplicationContextAccessor _httpContextAccessor
            ) : base(_CmsNodes, _httpContextAccessor)
        {
            siteProvider = _siteProvider;
            _logger = logger;
            AppSite = _AppSite;
            CmsNodes = _CmsNodes;
            PermissionCheck = _PermissionCheck;
            sys_resource = _sys_resource;
            sys_operate = _sys_operate;
            site_node_fidld = _site_node_fidld;
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        public IActionResult Index()
        {
            return View();
        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        public IActionResult FieldSet(string node_code)
        {
            if (string.IsNullOrWhiteSpace(node_code))
            {
                return new EmptyResult();
            }
            var site_node_fields = site_node_fidld.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<site_node_field_set, bool>>>() {
                a=>a.node_code==node_code
            });

            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { node_code = node_code, list = site_node_fields })));
            
        }
        [HttpPost]
        [Net.Mvc.Authorize.DefaultAuthorize]
        public IActionResult SaveFieldSet([FromBody] JObject data)
        {

            string node_code = data["node_code"].MyToString();
            var field_list = (data["field_list"] as JArray);

            site_node_fidld.Delete(a => a.node_code.ToLower() == node_code.ToLower());

            if (field_list != null)
            {
                List<site_node_field_set> list = new List<site_node_field_set>();

                foreach (var item in field_list)
                {
                    list.Add(new site_node_field_set() {
                        node_code= node_code,
                        field_code= item["value"].MyToString(),
                        field_name = item["title"].MyToString()
                    });
                }
                site_node_fidld.BulkInsert(list);

            }
            return new JsonResult(new { code = 0, msg = "ok" });
        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [Route("/Admin/SiteNodes/Edit/{id}/{pid}")]
        public async Task<IActionResult> EditAsync(
            [FromServices]IApplicationContextAccessor app_accessor, string id = "", string pid = "0")
        {
            if (id.MyToLong() > 0)
            {
                if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + id, "add,update"))
                {
                    return new RedirectResult("/admin/error/forbidden");
                }
            }
            else
            {
                if (pid.MyToLong() > 0)
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + pid, "add"))
                    {
                        return new RedirectResult("/admin/error/forbidden");
                    }
                }
                else
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "site_node", "add,update"))
                    {
                        return new RedirectResult("/admin/error/forbidden");
                    }

                }
            }
            site_node model = new site_node();
            site_node sc_model = new site_node();
            site_node en_model = new site_node();


            model.parent_node = pid.MyToLong();
            sc_model.parent_node = pid.MyToLong();
            en_model.parent_node = pid.MyToLong();
            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = CmsNodes.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = CmsNodes.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = CmsNodes.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null || model.id <= 0)
            {
                model = new site_node();
                model.parent_node = pid.MyToLong();
            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_node();
                sc_model.parent_node = pid.MyToLong();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_node();
                en_model.parent_node = pid.MyToLong();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> GetNodeTreeAsync(IFormCollection data)
        {
            string chr_type = data["chr_type"].MyToString();
            string lang = data["lang"].MyToString();
            var result = await CmsNodes.GetTreeDataAsync(chr_type, new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);
            return Json(result);
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {
            long ParentNode = data["pid"].MyToLong();
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string lang = data["lang"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>();
            condition.Add(a => a.parent_node == ParentNode || a.id == ParentNode);
            if (lang == "sc")
            {
                condition.Add(a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete);
            }
            else
            {
                condition.Add(a => a.lang == lang && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete);
            }
            var result = CmsNodes.GetPageList(page_index, 10000, "parent_node asc,nsort asc,create_time asc", condition);
            result.PagePostUrl = ParentNode.ToString();
            return PartialView("gv_List", result);
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public JsonResult UploadFile(IFormCollection data)
        {
            string type = data["type"].MyToString();
            string module = data["module"].MyToString();
            long id = data["id"].MyToLong();
            if (data.Files.Count <= 0)
            {
                return Json(new { code = -1, msg = "请选择上传文件" });
            }
            string BasePath = siteProvider.UploadStaticContentPath;
            string content_build_path = siteProvider.ContentUploadBuildPath;
            BasePath = Path.Combine(BasePath, content_build_path);
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }
            string new_name = Guid.NewGuid().ToString("N") + data.Files[0].FileName.GetFileExt();
            string save_path = Path.Combine(BasePath, new_name);
            data.Files[0].SaveAs(save_path);
            if (System.IO.File.Exists(save_path))
            {
                string NodePicPath = content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                if (id > 0)
                {
                    CmsNodes.Modify(a => a.id == id, a => new site_node()
                    {
                        node_pic = NodePicPath
                    });
                }
                return Json(new { code = 0, msg = "上传成功", path = NodePicPath });
            }
            else
            {
                return Json(new { code = -1, msg = "上传失败" });
            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public async Task<JsonResult> SaveAsync([FromServices]ISysTableidService tableid
            , [FromServices] ISiteModifyMarksService modify_mark
            , [FromServices] ISiteNodeMarksService node_mark
            , [FromServices] Net.MyLucene.ISearchService searcher
            , IFormCollection data)
        {


            long id = data["id"].MyToLong();
            NodeTypeEnum NodeType = (NodeTypeEnum)data["NodeType"].MyToInt();
            string ModelType = data["ModelType"].MyToString();
            string NodeCode = data["NodeCode"].MyToString();
            long ParentNode = data["ParentNode"].MyToLong();

            string ParentNode_Name = data["ParentNode_Name"].MyToString();
            if (id > 0)
            {
                if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + id, "add,update"))
                {
                    return Json(new { code = -403, msg = "forbidden" });
                }
            }
            else
            {
                if (ParentNode > 0)
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + ParentNode, "add"))
                    {
                        return Json(new { code = -403, msg = "forbidden" });
                    }
                }
                else
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "site_node", "add"))
                    {
                        return Json(new { code = -403, msg = "forbidden" });
                    }

                }
            }
            string ExtModel = data["ExtModel"].MyToString();
            string NodeExtModel = data["NodeExtModel"].MyToString();

            string NodePwd = data["NodePwd"].MyToString();
            StatusEnum ShowOnMenu = (StatusEnum)data["ShowOnMenu"].MyToInt();
            StatusEnum ShowOnPath = (StatusEnum)data["ShowOnPath"].MyToInt();
            StatusEnum NeedReview = (StatusEnum)data["NeedReview"].MyToInt();
            StatusEnum is_show = (StatusEnum)data["is_show"].MyToInt();
            StatusEnum lucene_index = (StatusEnum)data["lucene_index"].MyToInt();
            int nsort = data["nsort"].MyToInt();
            string tc_NodeName = data["tc_NodeName"].MyToString();
            string tc_out_link = data["tc_out_link"].MyToString();
            string tc_NodePic = data["tc_NodePic"].MyToString();
            string tc_node_atlas = data["tc_node_atlas"].MyToString();
            string tc_NodeDesc = data["tc_NodeDesc"].MyToString();
            string tc_NodeRemark = data["tc_NodeRemark"].MyToString();
            string tc_MetaKeywords = data["tc_MetaKeywords"].MyToString();
            string tc_MetaDescription = data["tc_MetaDescription"].MyToString();
            string tc_NodeTpl = data["tc_NodeTpl"].MyToString();
            string tc_ListTpl = data["tc_ListTpl"].MyToString();
            string tc_ContentTpl = data["tc_ContentTpl"].MyToString();
            string tc_extends_field_json = data["tc_extends_field_json"].MyToString();
            string sc_NodeName = data["sc_NodeName"].MyToString();
            string sc_out_link = data["sc_out_link"].MyToString();
            string sc_NodePic = data["sc_NodePic"].MyToString();
            string sc_node_atlas = data["sc_node_atlas"].MyToString();
            string sc_NodeDesc = data["sc_NodeDesc"].MyToString();
            string sc_NodeRemark = data["sc_NodeRemark"].MyToString();
            string sc_MetaKeywords = data["sc_MetaKeywords"].MyToString();
            string sc_MetaDescription = data["sc_MetaDescription"].MyToString();
            string sc_NodeTpl = data["sc_NodeTpl"].MyToString();
            string sc_ListTpl = data["sc_ListTpl"].MyToString();
            string sc_ContentTpl = data["sc_ContentTpl"].MyToString();
            string sc_extends_field_json = data["sc_extends_field_json"].MyToString();
            string en_NodeName = data["en_NodeName"].MyToString();
            string en_out_link = data["en_out_link"].MyToString();
            string en_NodePic = data["en_NodePic"].MyToString();
            string en_node_atlas = data["en_node_atlas"].MyToString();
            string en_NodeDesc = data["en_NodeDesc"].MyToString();
            string en_NodeRemark = data["en_NodeRemark"].MyToString();
            string en_MetaKeywords = data["en_MetaKeywords"].MyToString();
            string en_MetaDescription = data["en_MetaDescription"].MyToString();
            string en_NodeTpl = data["en_NodeTpl"].MyToString();
            string en_ListTpl = data["en_ListTpl"].MyToString();
            string en_ContentTpl = data["en_ContentTpl"].MyToString();
            string en_extends_field_json = data["en_extends_field_json"].MyToString();
            if (sc_NodeName == "" || NodeCode == "" || ModelType == "" || ParentNode < 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (id <= 0)
            {
                int check_count = CmsNodes.Count(a => a.node_code.ToLower() == NodeCode.ToLower());
                if (check_count > 0)
                {
                    return Json(new { code = -1, msg = "保存失败，已存在相同代号的栏目" });
                }
                int count = nsort;
                if (nsort <= 0)
                {
                    count = CmsNodes.Count(a => a.parent_node == ParentNode);
                    count = count + 1;
                }
                string parentLeavepath = "0";
                if (ParentNode != 0)
                {
                    var parent = CmsNodes.Get("", a => a.id == ParentNode);

                    if (parent != null)
                    {
                        parentLeavepath = parent.leave_path + "/" + parent.id;
                    }
                }
                int lang_flag = 1;
                long node_id = tableid.getNewTableId<site_node>();
                if (node_id <= 0)
                {
                    return Json(new { code = -1, msg = "参数错误" });
                }
                site_node result = new site_node();
                site_node sc_result = new site_node();
                if (sc_NodeName != "")
                {
                    sc_result = CmsNodes.Add(new site_node()
                    {
                        id = node_id,
                        model_type = ModelType,
                        node_type = NodeType,
                        node_code = NodeCode,
                        parent_node = ParentNode,
                        node_name = sc_NodeName,
                        node_tpl = sc_NodeTpl,
                        list_tpl = sc_ListTpl,
                        content_tpl = sc_ContentTpl,
                        node_pic = sc_NodePic,
                        node_desc = sc_NodeDesc,
                        node_remark = sc_NodeRemark,
                        meta_keywords = sc_MetaKeywords,
                        show_menu = ShowOnMenu,
                        show_path = ShowOnPath,
                        need_review = NeedReview,
                        nsort = count,
                        leave_path = parentLeavepath,
                        ext_model = ExtModel,
                        node_ext_model = NodeExtModel,
                        extend_fields_json = sc_extends_field_json,
                        mate_description = sc_MetaDescription,
                        out_link = sc_out_link,
                        node_atlas = sc_node_atlas,
                        pwd = NodePwd,
                        lang = "sc",
                        lang_flag = 1,
                        review_status = ReViewStatusEnum.UnReview,
                        down_list = "",
                        is_show = is_show,
                        lucene_index = lucene_index,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });

                    if (sc_result.id > 0)
                    {
                        var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", sc_result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertNodeIndex(up_lucene_model.node_code, sc_result);
                        }
                    }
                }
                if (tc_NodeName != "")
                {
                    result = CmsNodes.Add(new site_node()
                    {
                        id = node_id,
                        model_type = ModelType,
                        node_type = NodeType,
                        node_code = NodeCode,
                        parent_node = ParentNode,
                        node_name = tc_NodeName,
                        node_tpl = tc_NodeTpl,
                        list_tpl = tc_ListTpl,
                        content_tpl = tc_ContentTpl,
                        node_pic = tc_NodePic,
                        node_desc = tc_NodeDesc,
                        node_remark = tc_NodeRemark,
                        meta_keywords = tc_MetaKeywords,
                        show_menu = ShowOnMenu,
                        show_path = ShowOnPath,
                        need_review = NeedReview,
                        nsort = count,
                        leave_path = parentLeavepath,
                        ext_model = ExtModel,
                        node_ext_model = NodeExtModel,
                        extend_fields_json = tc_extends_field_json,
                        mate_description = tc_MetaDescription,
                        out_link = tc_out_link,
                        node_atlas = tc_node_atlas,
                        pwd = NodePwd,
                        lang = "tc",
                        lang_flag = lang_flag,
                        review_status = ReViewStatusEnum.UnReview,
                        down_list = "",
                        is_show = is_show,
                        lucene_index = lucene_index,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0)
                    {
                        var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertNodeIndex(up_lucene_model.node_code, result);

                            if (lang_flag == 2)
                            {
                                result.lang = "sc";
                                searcher.InsertNodeIndex(up_lucene_model.node_code, result);
                            }
                        }
                    }
                }

                if (en_NodeName != "")
                {
                    result = CmsNodes.Add(new site_node()
                    {
                        id = node_id,
                        model_type = ModelType,
                        node_type = NodeType,
                        node_code = NodeCode,
                        parent_node = ParentNode,
                        node_name = en_NodeName,
                        node_tpl = en_NodeTpl,
                        list_tpl = en_ListTpl,
                        content_tpl = en_ContentTpl,
                        node_pic = en_NodePic,
                        node_desc = en_NodeDesc,
                        node_remark = en_NodeRemark,
                        meta_keywords = en_MetaKeywords,
                        show_menu = ShowOnMenu,
                        show_path = ShowOnPath,
                        need_review = NeedReview,
                        nsort = count,
                        leave_path = parentLeavepath,
                        ext_model = ExtModel,
                        node_ext_model = NodeExtModel,
                        extend_fields_json = en_extends_field_json,
                        mate_description = en_MetaDescription,
                        out_link = en_out_link,
                        node_atlas = en_node_atlas,
                        pwd = NodePwd,
                        lang = "en",
                        lang_flag = 1,
                        review_status = ReViewStatusEnum.UnReview,
                        down_list = "",
                        is_show = is_show,
                        lucene_index = lucene_index,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0)
                    {
                        var up_lucene_model = Service.GetUpLeaveLuceneNode("en", result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertNodeIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (sc_result.id > 0)
                {

                    #region 栏目相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 5;
                        string module_code = "site_node";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_node_g_{sc_result.id}",
                            res_name = $"栏目管理--{sc_result.node_name}({sc_result.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion

                    #region 内容相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 6;
                        string module_code = "site_content";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_content_g_{sc_result.id}",
                            res_name = $"内容管理--{sc_result.node_name}({sc_result.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion
                    if (ParentNode == 0)
                    {
                        AddLog("site_node", Utils.LogsOperateTypeEnum.Add, $"创建跟节点栏目:{sc_result.node_name}");
                    }
                    else
                    {
                        AddLog($"cms_node_g_{ParentNode}", Utils.LogsOperateTypeEnum.Add, $"在{ParentNode_Name}栏目下创建子栏目:{sc_result.node_name}");
                    }
                    return Json(new { code = 0, msg = "新增成功" });
                }
                else
                {
                    return Json(new { code = -1, msg = "新增失败" });
                }
            }
            else
            {
                int check_count = CmsNodes.Count(a => a.node_code.ToLower() == NodeCode.ToLower() && a.id != id);
                if (check_count > 0)
                {
                    return Json(new { code = -1, msg = "修改失败，已存在相同代号的栏目" });
                }
                if (nsort <= 0)
                {
                    nsort = CmsNodes.Count(a => a.parent_node == ParentNode);
                    nsort = nsort + 1;
                }
                int lang_flag = 1;
                var result = 0;
                var modify = modify_mark.Insert(new site_modify_marks()
                {
                    chr_type = 1,
                    create_id = httpContextAccessor.Current.CurrentUser.id,
                    operate_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                    create_time = DateTime.Now,
                    create_name = httpContextAccessor.Current.CurrentUser.alias_name
                });
                var node_model = CmsNodes.Get("row_key asc", a => a.id == id);
                //繁体修改该
                if (tc_NodeName != "")
                {
                    var tc_model = CmsNodes.Get("row_key asc", a => a.id == id && a.lang == "tc");
                    if (tc_model != null)
                    {
                        #region 繁体修改
                        result = CmsNodes.Modify(a => a.id == id && a.lang == "tc", a => new site_node()
                        {

                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = tc_NodeName,
                            node_tpl = tc_NodeTpl,
                            list_tpl = tc_ListTpl,
                            content_tpl = tc_ContentTpl,
                            node_pic = tc_NodePic,
                            node_desc = tc_NodeDesc,
                            node_remark = tc_NodeRemark,
                            meta_keywords = tc_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = tc_extends_field_json,
                            mate_description = tc_MetaDescription,
                            out_link = tc_out_link,
                            node_atlas = tc_node_atlas,
                            pwd = NodePwd,
                            lang_flag = lang_flag,
                            down_list = "",
                            is_show = is_show,
                            lucene_index = lucene_index
                        });
                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, tc_model);
                                if (lang_flag == 2)
                                {
                                    tc_model.lang = "sc";
                                    searcher.UpdateNodeIndex(up_lucene_model.node_code, tc_model);
                                }
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                lang_flag = lang_flag,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                down_list = "",
                                model_type = tc_model.model_type,
                                node_type = tc_model.node_type,
                                node_code = tc_model.node_code,
                                parent_node = tc_model.parent_node,
                                node_name = tc_model.node_name,
                                node_tpl = tc_model.node_tpl,
                                list_tpl = tc_model.list_tpl,
                                content_tpl = tc_model.content_tpl,
                                node_pic = tc_model.node_pic,
                                node_desc = tc_model.node_desc,
                                node_remark = tc_model.node_remark,
                                meta_keywords = tc_model.meta_keywords,
                                show_menu = tc_model.show_menu,
                                show_path = tc_model.show_path,
                                need_review = tc_model.need_review,
                                ext_model = tc_model.ext_model,
                                extend_fields_json = tc_model.extend_fields_json,
                                mate_description = tc_model.mate_description,
                                out_link = tc_model.out_link,
                                node_atlas = tc_model.node_atlas,
                                pwd = tc_model.model_type,
                                is_show = tc_model.is_show
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = tc_NodeName,
                                node_tpl = tc_NodeTpl,
                                list_tpl = tc_ListTpl,
                                content_tpl = tc_ContentTpl,
                                node_pic = tc_NodePic,
                                node_desc = tc_NodeDesc,
                                node_remark = tc_NodeRemark,
                                meta_keywords = tc_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
       
                                extend_fields_json = tc_extends_field_json,
                                mate_description = tc_MetaDescription,
                                out_link = tc_out_link,
                                node_atlas = tc_node_atlas,
                                pwd = NodePwd,
                                lang_flag = lang_flag,
                                down_list = "",
                                is_show = is_show
                            });
                        }
                        #endregion
                    }
                    else
                    {
                        #region 新插入
                        //重新插入
                        string parentLeavepath = "0";
                        if (ParentNode != 0)
                        {
                            var parent = CmsNodes.Get("", a => a.id == ParentNode);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                        var mod_tc_model = CmsNodes.Add(new site_node()
                        {
                            id = id,
                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = tc_NodeName,
                            node_tpl = tc_NodeTpl,
                            list_tpl = tc_ListTpl,
                            content_tpl = tc_ContentTpl,
                            node_pic = tc_NodePic,
                            node_desc = tc_NodeDesc,
                            node_remark = tc_NodeRemark,
                            meta_keywords = tc_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = tc_extends_field_json,
                            mate_description = tc_MetaDescription,
                            out_link = tc_out_link,
                            node_atlas = tc_node_atlas,
                            pwd = NodePwd,
                            lang = "tc",
                            lang_flag = 1,
                            review_status = node_model.review_status,
                            del_flag = node_model.del_flag,
                            down_list = "",
                            is_show = is_show,
                            lucene_index = lucene_index,
                            create_time = DateTime.Now
                        });

                        if (mod_tc_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", mod_tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_tc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = tc_NodeName,
                                node_tpl = tc_NodeTpl,
                                list_tpl = tc_ListTpl,
                                content_tpl = tc_ContentTpl,
                                node_pic = tc_NodePic,
                                node_desc = tc_NodeDesc,
                                node_remark = tc_NodeRemark,
                                meta_keywords = tc_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
                
                                extend_fields_json = tc_extends_field_json,
                                mate_description = tc_MetaDescription,
                                out_link = tc_out_link,
                                node_atlas = tc_node_atlas,
                                pwd = NodePwd,
                                lang_flag = 1,
                                down_list = "",
                                is_show = is_show
                            });
                        }

                        #endregion
                    }
                }
                else
                {
                    CmsNodes.Delete(a => a.id == id && a.lang == "tc");
                }
                if (sc_NodeName != "")
                {
                    var sc_model = CmsNodes.Get("row_key asc", a => a.id == id && a.lang == "sc");
                    if (sc_model != null)
                    {
                        result = CmsNodes.Modify(a => a.id == id && a.lang == "sc", a => new site_node()
                        {
                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = sc_NodeName,
                            node_tpl = sc_NodeTpl,
                            list_tpl = sc_ListTpl,
                            content_tpl = sc_ContentTpl,
                            node_pic = sc_NodePic,
                            node_desc = sc_NodeDesc,
                            node_remark = sc_NodeRemark,
                            meta_keywords = sc_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = sc_extends_field_json,
                            mate_description = sc_MetaDescription,
                            out_link = sc_out_link,
                            node_atlas = sc_node_atlas,
                            pwd = NodePwd,
                            lang_flag = lang_flag,
                            is_show = is_show,
                            lucene_index = lucene_index
                        });
                        sc_model = CmsNodes.Get("row_key asc", a => a.id == id && a.lang == "sc");
                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, node_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                down_list = "",
                                model_type = sc_model.model_type,
                                node_type = sc_model.node_type,
                                node_code = sc_model.node_code,
                                parent_node = sc_model.parent_node,
                                node_name = sc_model.node_name,
                                node_tpl = sc_model.node_tpl,
                                list_tpl = sc_model.list_tpl,
                                content_tpl = sc_model.content_tpl,
                                node_pic = sc_model.node_pic,
                                node_desc = sc_model.node_desc,
                                node_remark = sc_model.node_remark,
                                meta_keywords = sc_model.meta_keywords,
                                show_menu = sc_model.show_menu,
                                show_path = sc_model.show_path,
                                need_review = sc_model.need_review,
                                ext_model = sc_model.ext_model,
                                extend_fields_json = sc_model.extend_fields_json,
                                mate_description = sc_model.mate_description,
                                out_link = sc_model.out_link,
                                node_atlas = sc_model.node_atlas,
                                pwd = sc_model.model_type,
                                is_show = sc_model.is_show
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = sc_NodeName,
                                node_tpl = sc_NodeTpl,
                                list_tpl = sc_ListTpl,
                                content_tpl = sc_ContentTpl,
                                node_pic = sc_NodePic,
                                node_desc = sc_NodeDesc,
                                node_remark = sc_NodeRemark,
                                meta_keywords = sc_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
         
                                extend_fields_json = sc_extends_field_json,
                                mate_description = sc_MetaDescription,
                                out_link = sc_out_link,
                                node_atlas = sc_node_atlas,
                                pwd = NodePwd,
                                lang_flag = 1,
                                down_list = "",
                                is_show = is_show
                            });
                        }
                    }
                    else
                    {
                        string parentLeavepath = "0";
                        if (ParentNode != 0)
                        {
                            var parent = CmsNodes.Get("", a => a.id == ParentNode);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                        var mod_sc_model = CmsNodes.Add(new site_node()
                        {
                            id = id,
                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = sc_NodeName,
                            node_tpl = sc_NodeTpl,
                            list_tpl = sc_ListTpl,
                            content_tpl = sc_ContentTpl,
                            node_pic = sc_NodePic,
                            node_desc = sc_NodeDesc,
                            node_remark = sc_NodeRemark,
                            meta_keywords = sc_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = sc_extends_field_json,
                            mate_description = sc_MetaDescription,
                            out_link = sc_out_link,
                            node_atlas = sc_node_atlas,
                            pwd = NodePwd,
                            lang = "sc",
                            lang_flag = 1,
                            review_status = node_model.review_status,
                            del_flag = node_model.del_flag,
                            down_list = "",
                            is_show = is_show,
                            lucene_index = lucene_index,
                            create_time = DateTime.Now
                        });
                        if (mod_sc_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", mod_sc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_sc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = sc_NodeName,
                                node_tpl = sc_NodeTpl,
                                list_tpl = sc_ListTpl,
                                content_tpl = sc_ContentTpl,
                                node_pic = sc_NodePic,
                                node_desc = sc_NodeDesc,
                                node_remark = sc_NodeRemark,
                                meta_keywords = sc_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
          
                                extend_fields_json = sc_extends_field_json,
                                mate_description = sc_MetaDescription,
                                out_link = sc_out_link,
                                node_atlas = sc_node_atlas,
                                pwd = NodePwd,
                                lang_flag = 1,
                                down_list = "",
                                is_show = is_show
                            });
                        }
                    }
                }
                else
                {
                    CmsNodes.Delete(a => a.id == id && a.lang == "sc");
                }

                if (en_NodeName != "")
                {
                    var en_model = CmsNodes.Get("row_key asc", a => a.id == id && a.lang == "en");
                    if (en_model != null)
                    {
                        result = CmsNodes.Modify(a => a.id == id && a.lang == "en", a => new site_node()
                        {

                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = en_NodeName,
                            node_tpl = en_NodeTpl,
                            list_tpl = en_ListTpl,
                            content_tpl = en_ContentTpl,
                            node_pic = en_NodePic,
                            node_desc = en_NodeDesc,
                            node_remark = en_NodeRemark,
                            meta_keywords = en_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = en_extends_field_json,
                            mate_description = en_MetaDescription,
                            out_link = en_out_link,
                            node_atlas = en_node_atlas,
                            pwd = NodePwd,
                            lang_flag = lang_flag,
                            is_show = is_show
                        });


                        en_model = CmsNodes.Get("row_key asc", a => a.id == id && a.lang == "en");

                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("en", en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                down_list = "",
                                model_type = en_model.model_type,
                                node_type = en_model.node_type,
                                node_code = en_model.node_code,
                                parent_node = en_model.parent_node,
                                node_name = en_model.node_name,
                                node_tpl = en_model.node_tpl,
                                list_tpl = en_model.list_tpl,
                                content_tpl = en_model.content_tpl,
                                node_pic = en_model.node_pic,
                                node_desc = en_model.node_desc,
                                node_remark = en_model.node_remark,
                                meta_keywords = en_model.meta_keywords,
                                show_menu = en_model.show_menu,
                                show_path = en_model.show_path,
                                need_review = en_model.need_review,
                                ext_model = en_model.ext_model,
                                extend_fields_json = en_model.extend_fields_json,
                                mate_description = en_model.mate_description,
                                out_link = en_model.out_link,
                                node_atlas = en_model.node_atlas,
                                pwd = en_model.model_type,
                                is_show = en_model.is_show
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = en_NodeName,
                                node_tpl = en_NodeTpl,
                                list_tpl = en_ListTpl,
                                content_tpl = en_ContentTpl,
                                node_pic = en_NodePic,
                                node_desc = en_NodeDesc,
                                node_remark = en_NodeRemark,
                                meta_keywords = en_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
                       
                                extend_fields_json = en_extends_field_json,
                                mate_description = en_MetaDescription,
                                out_link = en_out_link,
                                node_atlas = en_node_atlas,
                                pwd = NodePwd,
                                lang_flag = 1,
                                down_list = "",
                                is_show = is_show
                            });
                        }
                    }
                    else
                    {
                        string parentLeavepath = "0";
                        if (ParentNode != 0)
                        {
                            var parent = CmsNodes.Get("", a => a.id == ParentNode);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                        var mod_en_model = CmsNodes.Add(new site_node()
                        {
                            id = id,
                            model_type = ModelType,
                            node_type = NodeType,
                            node_code = NodeCode,
                            parent_node = ParentNode,
                            node_name = en_NodeName,
                            node_tpl = en_NodeTpl,
                            list_tpl = en_ListTpl,
                            content_tpl = en_ContentTpl,
                            node_pic = en_NodePic,
                            node_desc = en_NodeDesc,
                            node_remark = en_NodeRemark,
                            meta_keywords = en_MetaKeywords,
                            show_menu = ShowOnMenu,
                            show_path = ShowOnPath,
                            need_review = NeedReview,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ExtModel,
                            node_ext_model = NodeExtModel,
                            extend_fields_json = en_extends_field_json,
                            mate_description = en_MetaDescription,
                            out_link = en_out_link,
                            node_atlas = en_node_atlas,
                            pwd = NodePwd,
                            lang = "en",
                            lang_flag = 1,
                            down_list = "",
                            is_show = is_show,
                            lucene_index = lucene_index,
                            review_status = node_model.review_status,
                            del_flag = node_model.del_flag,
                            create_time = DateTime.Now
                        });
                        if (mod_en_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("en", mod_en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = ModelType,
                                node_type = NodeType,
                                node_code = NodeCode,
                                parent_node = ParentNode,
                                node_name = en_NodeName,
                                node_tpl = en_NodeTpl,
                                list_tpl = en_ListTpl,
                                content_tpl = en_ContentTpl,
                                node_pic = en_NodePic,
                                node_desc = en_NodeDesc,
                                node_remark = en_NodeRemark,
                                meta_keywords = en_MetaKeywords,
                                show_menu = ShowOnMenu,
                                show_path = ShowOnPath,
                                need_review = NeedReview,
                                ext_model = ExtModel,
                       
                                extend_fields_json = en_extends_field_json,
                                mate_description = en_MetaDescription,
                                out_link = en_out_link,
                                node_atlas = en_node_atlas,
                                pwd = NodePwd,
                                lang_flag = 1,
                                down_list = "",
                                is_show = is_show
                            });
                        }
                    }
                }
                if (result > 0)
                {
                    string res_code = $"cms_node_g_{id}";
                    var opeart = sys_resource.GetByOne(a => a.p_module == 5 && a.res_code == res_code);
                    string append_msg = "";
                    if (opeart == null)
                    {
                        #region 栏目相关权限
                        {
                            //一级分类不用创建==parent=0:
                            long module_id = 5;
                            string module_code = "site_node";


                            sys_resource_entity resource = new sys_resource_entity()
                            {
                                p_module = module_id,
                                url = $"",
                                uri = "",
                                enable_status = StatusEnum.Legal,
                                created_time = DateTime.Now,
                                creator_id = "0",
                                creator_name = "",
                                delete_status = DeleteEnum.UnDelete,
                                description = "",
                                last_modified_time = DateTime.Now,
                                last_modifier_id = "0",
                                last_modifier_name = "",
                                n_sort = 1,
                                res_code = $"cms_node_g_{id}",
                                res_name = $"栏目管理--{tc_NodeName}({NodeCode})",
                                status = StatusEnum.Legal
                            };
                            resource = sys_resource.Insert(resource);
                            if (resource.id > 0)
                            {
                                List<sys_operate_entity> operates = new List<sys_operate_entity>();
                                var operate_codes = new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
                            };
                                foreach (var item in operate_codes)
                                {
                                    var operate_model = new sys_operate_entity()
                                    {
                                        is_menu = StatusEnum.Legal,
                                        creator_id = "0",
                                        creator_name = "",
                                        delete_status = DeleteEnum.UnDelete,
                                        description = "",
                                        last_modified_time = DateTime.Now,
                                        last_modifier_id = "0",
                                        last_modifier_name = "",
                                        n_sort = 1,
                                        operate_code = item.Key,
                                        operate_name = item.Value,
                                        p_module = module_id,
                                        api_url = $"",
                                        created_time = DateTime.Now,
                                        enable_status = StatusEnum.Legal,
                                        p_module_code = module_code,
                                        p_res = resource.id,
                                        p_res_code = resource.res_code,
                                        status = StatusEnum.Legal
                                    };
                                    operates.Add(operate_model);
                                }
                                //批量插入权限
                                sys_operate.BulkInsert(operates);
                            }
                        }
                        #endregion
                        #region 内容相关权限
                        {
                            //一级分类不用创建==parent=0:
                            long module_id = 6;
                            string module_code = "site_content";


                            sys_resource_entity resource = new sys_resource_entity()
                            {
                                p_module = module_id,
                                url = $"",
                                uri = "",
                                enable_status = StatusEnum.Legal,
                                created_time = DateTime.Now,
                                creator_id = "0",
                                creator_name = "",
                                delete_status = DeleteEnum.UnDelete,
                                description = "",
                                last_modified_time = DateTime.Now,
                                last_modifier_id = "0",
                                last_modifier_name = "",
                                n_sort = 1,
                                res_code = $"cms_content_g_{id}",
                                res_name = $"内容管理--{tc_NodeName}({NodeCode})",
                                status = StatusEnum.Legal
                            };
                            resource = sys_resource.Insert(resource);
                            if (resource.id > 0)
                            {
                                List<sys_operate_entity> operates = new List<sys_operate_entity>();
                                var operate_codes = new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                                foreach (var item in operate_codes)
                                {
                                    var operate_model = new sys_operate_entity()
                                    {
                                        is_menu = StatusEnum.Legal,
                                        creator_id = "0",
                                        creator_name = "",
                                        delete_status = DeleteEnum.UnDelete,
                                        description = "",
                                        last_modified_time = DateTime.Now,
                                        last_modifier_id = "0",
                                        last_modifier_name = "",
                                        n_sort = 1,
                                        operate_code = item.Key,
                                        operate_name = item.Value,
                                        p_module = module_id,
                                        api_url = $"",
                                        created_time = DateTime.Now,
                                        enable_status = StatusEnum.Legal,
                                        p_module_code = module_code,
                                        p_res = resource.id,
                                        p_res_code = resource.res_code,
                                        status = StatusEnum.Legal
                                    };
                                    operates.Add(operate_model);
                                }
                                //批量插入权限
                                sys_operate.BulkInsert(operates);
                            }
                        }
                        #endregion
                        append_msg = ",并添加该栏目的权限,请权限用户进行授权!";
                    }
                    if (ParentNode == 0)
                    {
                        AddLog("site_node", Utils.LogsOperateTypeEnum.Update, $"修改跟节点栏目:{sc_NodeName}");
                    }
                    else
                    {
                        AddLog($"cms_node_g_{ParentNode}", Utils.LogsOperateTypeEnum.Update, $"修改在{ParentNode_Name}栏目下的子栏目:{sc_NodeName}");
                    }
                    return Json(new { code = 0, msg = $"修改成功{append_msg}" });
                }
                else
                {
                    return Json(new { code = -1, msg = "修改失败" });
                }
            }

        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> DeleteAsync([FromServices]ISysRoleOperateRelationService role_operate, IFormCollection data)
        {


            long Id = data["Id"].MyToLong();

            long parent_node = data["parent_node"].MyToLong();

            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string NodeName = data["NodeName"].MyToString();
            var check = CmsNodes.GetList("", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.parent_node==Id
            });
            if (check.Count() > 0)
            {
                return Json(new { code = -1, msg = "存在下级栏目无法删除" });
            }
            var model = CmsNodes.Get("", a => a.id == Id);
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + model.id, "delete"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            int flag = CmsNodes.Modify(a => a.id == Id, a => new site_node() { del_flag = DeleteEnum.Delete });
            if (flag > 0)
            {

                if (parent_node == 0)
                {
                    AddLog("site_node", Utils.LogsOperateTypeEnum.Delete, $"修改跟节点栏目:{NodeName}");
                }
                else
                {
                    AddLog($"cms_node_g_{parent_node}", Utils.LogsOperateTypeEnum.Delete, $"删除栏目:{NodeName}");
                }
                return Json(new { code = 0, msg = "删除成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "删除失败" });
            }
        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public JsonResult Sort(IFormCollection data)
        {
            long Id = data["id"].MyToLong();
            int type = data["type"].MyToInt();//1 up 2down
            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            var model = CmsNodes.Get("", a => a.id == Id);
            if (type == 1)
            {
                if (model.nsort <= 1)
                {
                    return Json(new { code = -1, msg = "已经是第一无需再继续排序" });
                }
            }
            //非自己的其他同级
            var check = CmsNodes.GetList("", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.parent_node==model.parent_node&&a.id!=model.id
            });

            if (check.Count() <= 0)
            {
                return Json(new { code = -1, msg = "只存在一条同级记录无需排序" });
            }
            if (type == 2)
            {
                if (model.nsort == check.Count() + 1)
                {
                    return Json(new { code = -1, msg = "已经是最后一条无需再继续排序" });
                }
            }
            if (type == 1)
            {
                var pre = check.Where(a => a.nsort < model.nsort).OrderBy(a => a.nsort).LastOrDefault();
                if (pre != null)
                {
                    //前面一个往下面移动
                    CmsNodes.Modify(a => a.id == pre.id, a => new site_node()
                    {
                        nsort = a.nsort + 1
                    });
                    //自己往上面移动
                    CmsNodes.Modify(a => a.id == model.id, a => new site_node()
                    {
                        nsort = a.nsort - 1
                    });
                }
            }
            else
            {
                var next = check.Where(a => a.nsort > model.nsort).OrderBy(a => a.nsort).FirstOrDefault();
                if (next != null)
                {
                    //下面一个往上面移动
                    CmsNodes.Modify(a => a.id == next.id, a => new site_node()
                    {
                        nsort = a.nsort - 1
                    });
                    //自己往下面移动
                    CmsNodes.Modify(a => a.id == model.id, a => new site_node()
                    {
                        nsort = a.nsort + 1
                    });
                }
            }


            AddLog("site_node", Utils.LogsOperateTypeEnum.Update, $"修改栏目排序");

            return Json(new { code = 0, msg = "ok" });
        }


        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public JsonResult DoSort([FromBody]JObject data)
        {
            var value = data["data"];
            foreach (var item in value)
            {
                if (item["id"].MyToLong() > 0)
                {

                    CmsNodes.Modify(a => a.id == item["id"].MyToLong(), a => new site_node()
                    {
                        nsort = item["sort"].MyToInt()
                    });
                }
            }
            AddLog("site_node", Utils.LogsOperateTypeEnum.Update, $"修改栏目排序");
            return Json(new { code = 0, msg = "ok" });
        }




        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> changeAsync(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + id, "update"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag = Service.Modify(a => a.id == id, a => new site_node()
            {
                is_show = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_node_g_{id}", Utils.LogsOperateTypeEnum.Update, $"修改网站栏目显示状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }



        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public async Task<JsonResult> LuceneAsync(IFormCollection data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();
            string chr_name = data["chr_name"].MyToString();
            if (status < 0 || id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + id, "update"))
            {
                return Json(new { code = -403, msg = "forbidden" });
            }
            if (status != 0 && status != 1)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            int flag = Service.Modify(a => a.id == id, a => new site_node()
            {
                lucene_index = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_node_g_{id}", Utils.LogsOperateTypeEnum.Update, $"修改网站栏目索引状态:{chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return Json(new { code = 0, msg = "操作成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "操作失败" });
            }
        }

    }
}