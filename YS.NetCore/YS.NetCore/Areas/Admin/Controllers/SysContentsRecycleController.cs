﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace YS.NetCore.Areas.Admin.Controllers
{
    public class SysContentsRecycleController : Net.Mvc.BasicController<site_content, ISiteContentService>
    {

        public SysContentsRecycleController(ISiteContentService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        [Net.Mvc.Authorize.DefaultAuthorize]
        public ActionResult Index()
        {
            return View();
        }


        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_recycle", resource_code = "site_content_recycle", operate_code = "view")]
        /// <summary>
        /// 添加角色的控制器
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult View(string id)
        {




            site_content model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();



            if (!string.IsNullOrEmpty(id) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null)
            {
                model = new site_content();

            }
            if (sc_model == null)
            {
                sc_model = new site_content();
            }
            if (en_model == null)
            {
                en_model = new site_content();
            }
            return View(Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(new { tc = model, sc = sc_model, en = en_model })));
        }

        [Net.Mvc.Authorize.DefaultAuthorize]
        [HttpPost]
        public PartialViewResult gv_List(IFormCollection data)
        {


            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string ChrName = data["chr_name"].MyToString(); ;
            List<System.Linq.Expressions.Expression<Func<site_content, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>();
            condition.Add(a => a.lang == "sc" && a.del_flag == DeleteEnum.Delete);
            if (ChrName.Trim() != "")
            {
                condition.Add(a => a.title.Contains(ChrName));
            }
            var result = Service.GetNodeContentList(page_index, page_size, 0, condition);
            return PartialView("gv_List", Newtonsoft.Json.JsonConvert.DeserializeObject(JsonHelper.ToJson(result)));

        }

        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_recycle", resource_code = "site_content_recycle", operate_code = "delete")]
        public JsonResult Delete([FromServices] Net.MyLucene.ISearchService searcher,IFormCollection data)
        {


            long Id = data["Id"].MyToLong();

            long parent_node = data["node_id"].MyToLong();

            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string chr_name = data["chr_name"].MyToString();
            string node_name = data["node_name"].MyToString();
            var model = Service.Get("", a => a.id == Id&&a.lang=="sc");
            int flag = Service.Delete(a => a.id == Id);
            if (flag > 0)
            {
                ////更新同一级之下排序号
                Service.Modify(a => a.node_id == model.node_id && a.n_sort > model.n_sort, a => new site_content()
                {
                    n_sort = a.n_sort + 1
                });
                searcher.DeleteContentIndex(model);
                AddLog("site_content_recycle", Utils.LogsOperateTypeEnum.Delete, $"彻底删除“{node_name}”栏目下内容:{chr_name}");

                return Json(new { code = 0, msg = "删除成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "删除失败" });
            }
        }
        [Net.Mvc.Authorize.DefaultAuthorize(module_code = "site_content_recycle", resource_code = "site_content_recycle", operate_code = "recovery")]
        public JsonResult Recovery(IFormCollection data)
        {
            long Id = data["Id"].MyToLong();
            long parent_node = data["node_id"].MyToLong();
            if (Id <= 0)
            {
                return Json(new { code = -1, msg = "参数错误" });
            }
            string node_name = data["node_name"].MyToString();
            string chr_name = data["chr_name"].MyToString();
            int flag = Service.Modify(a => a.id == Id, a => new site_content() { del_flag = DeleteEnum.UnDelete });
            if (flag > 0)
            {
                AddLog("site_content_recycle", Utils.LogsOperateTypeEnum.Recovery, $"恢复放入回收站“{node_name}”栏目下内容“{chr_name}”");
                return Json(new { code = 0, msg = "恢复成功" });
            }
            else
            {
                return Json(new { code = -1, msg = "恢复失败" });
            }
        }

    }
}