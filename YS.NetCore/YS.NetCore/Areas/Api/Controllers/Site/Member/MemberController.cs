﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("前端使用--会员相关")]
    public class MemberController : ApiBasicController<site_member, ISiteMemberService>
    {

        public MemberController(ISiteMemberService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {

        }
        /// <summary>
        /// 数据接口,只验证登录不验证权限,可用于其他模板下拉或选择使用
        /// 例如:
        ///
        /// - 在问卷选择哪标签的人可以见 value值为code
        /// - 会员修改/会员审核时的标签 value值为code
        /// - 返回[{"id":1,"code"="t1",tag_name="标签名称"}]
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("data/list")]
        public RestfulData GetDataList()
        {

            var condition = new List<System.Linq.Expressions.Expression<Func<site_member_tag, bool>>>();
            var result = Service.GetList("id desc", condition);
            return new RestfulData<object>(0, "ok", result.Select(a => new { code = $"t{a.id}", a.tag_name }));
        }
        /// <summary>
        /// 绑定用户
        /// </summary>
        /// <param name="Configuration"></param>
        /// <param name="cache"></param>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id_card":"身份证号码",
        ///     "card_no":"权益卡号",
        ///     "real_name:"姓名",
        ///     "tel_no":"电话号码",
        ///     "tag":"标签 从api/v2/membertag/data/list 获取下来传入code",
        ///     "code":"标识换取凭证,code2login接口返回code=2时data的code"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [HttpPost("BindUser")]
        [AllowAnonymous]
        public RestfulData BindUser([FromServices] IConfiguration Configuration, [FromServices] IMyCache cache, JObject data)
        {


            string code = data["code"].MyToString();

            string id_card = data["id_card"].MyToString();
            string card_no = data["card_no"].MyToString();
            string real_name = data["real_name"].MyToString();
            string tel_no = data["tel_no"].MyToString();
            string tag = data["tag"].MyToString();
            if (id_card == "" || card_no == "" || real_name == "" || tel_no == "" || tag == "")
            {
                return new RestfulData(-1, "参数错误");
            }
            if (code == "")
            {
                return new RestfulData(-1, "参数错误");
            }
            var model = cache.Get<site_member>(string.Format(WeChatConstants.WeChatMPAutoLoginKey, code));
            if (model == null)
            {
                return new RestfulData(-1, "非法进入,请重新授权登录");
            }

            Service.Modify(a => a.id == model.id, a => new site_member()
            {
                id_card = id_card,
                card_no = card_no,
                real_name = real_name,
                tel_no = tel_no,
                tags = tag
            });

            return new RestfulData(0, "提交成功");

        }

        /// <summary>
        /// 获取区域管理人员
        /// </summary>
        /// <returns></returns>
        [HttpGet("areamaner")]
        [SiteMemberCheck]
        public RestfulData GetAreaManager([FromServices]ISysRoleOperateRelationService sysRoleOperateRelationService,
            [FromServices] ISysUserRoleRelationService sysUserRoleRelationService
            , [FromServices] ISysAccountService sysAccountService
            )
        {
            var member = httpContextAccessor.Current.MemberCustomer;

            List<long> role_ids = new List<long>();
            if (member.review_status == ReViewStatusEnum.Pass && member.tags != "")
            {
                var tags_array = member.tags.MySplit(',');
                var predicate = PredicateBuilder2.False<sys_role_operate_relation_entity>();
                predicate = predicate.Or(a => a.module_code == "site_member" && a.res_code == "site_member" && a.operate_code == "view_all");
                foreach (var item in tags_array)
                {
                    if (item.StartsWith("t"))
                    {
                        var tag_id = item.Replace("t", "");
                        string op_code = "data_tag_" + tag_id;
                        predicate = predicate.Or(a => a.module_code == "site_member" && a.res_code == "site_member" && a.operate_code == op_code);

                    }
                }
                var condtions = new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() { };
                condtions.Add(predicate);
                var role_list = sysRoleOperateRelationService.GetList("id asc", condtions);

                if (role_list.Count() > 0)
                {
                    role_ids.AddRange(role_list.Select(a => a.role_id));
                }
                List<long> accountd_ids = new List<long>();
                if (role_ids.Count() > 0)
                {
                    var list = sysUserRoleRelationService.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_user_role_relation_entity, bool>>>() {
                        a=>role_ids.Contains(a.role_id)
                    });
                    if (list.Count() > 0)
                    {
                        accountd_ids.AddRange(list.Select(a => a.user_id));
                    }
                }
                if (accountd_ids.Count() > 0)
                {
                    var account_list = sysAccountService.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_account, bool>>>() {
                        a=>accountd_ids.Contains(a.id)
                    });
                    return new RestfulArray<sys_account>(0, "ok", account_list);

                }
            }

            return new RestfulArray<object>(0, "ok", new List<object>());
        }



     

    }
}
