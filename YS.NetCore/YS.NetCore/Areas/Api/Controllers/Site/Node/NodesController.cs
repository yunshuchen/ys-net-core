﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.CO2NET.Extensions;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.WxOpen.AdvancedAPIs.Sns;
using Senparc.Weixin.WxOpen.Containers;
using Senparc.Weixin.WxOpen.Entities;
using Senparc.Weixin.WxOpen.Helpers;

namespace YS.NetCore.Areas.Api.Controllers.Site
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("前端使用--网站栏目相关")]

    public class NodesController : ApiBasicController<site_node, ISiteNodeService>
    {


        public NodesController(ISiteNodeService service,
            IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }

        /// <summary>
        /// 获取栏目树形结构
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"语种:sc简体,tc繁体,en英文"
        ///} 
        ///```
        /// </param>
        /// <returns></returns>
        [HttpPost("tree")]
        public async Task<RestfulData> GetNodeTreeAsync(JObject data)
        {
            string lang = data["lang"].MyToString();
            var result = await Service.GetSiteTreeDataAsync("node", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);
            return new RestfulData<object>(0, "ok", result.FirstOrDefault()?.children);
        }



        /// <summary>
        /// 获取某栏目下所有的子栏目
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"*语种:sc简体,tc繁体,en英文",
        ///     "node_code":"*栏目代号",
        ///     "show_menu":"是否菜单显示:1显示,0不显示,-1全部",
        ///     "show_path":"是否导航显示:1显示,0不显示,-1全部",
        ///     "is_show":"是否显示:1显示,0不显示,-1全部"
        ///} 
        ///```
        /// - 说明
        /// 获取node_code 下面所有子栏目
        /// </param>
        /// <returns></returns>
        [HttpPost("AllChildrenNodes")]
        public RestfulData GetAllChildrenNodes(JObject data)
        {
            string lang = data["lang"].MyToString();
            string node_code = data["node_code"].MyToString().ToLower();

            int show_menu = data["show_menu"].MyToInt(-1);
            int show_path = data["show_path"].MyToInt(-1);
            int is_show = data["is_show"].MyToInt(-1);
            List<Expression<Func<site_node, bool>>> predicat = new List<Expression<Func<site_node, bool>>>();
            predicat.Add(a => a.del_flag == DeleteEnum.UnDelete && a.review_status == ReViewStatusEnum.Pass);
            if (show_menu != -1)
            {
                predicat.Add(a => a.show_menu == (StatusEnum)show_menu);
            }
            if (show_path != -1)
            {
                predicat.Add(a => a.show_path == (StatusEnum)show_path);
            }
            if (is_show != -1)
            {
                predicat.Add(a => a.is_show == (StatusEnum)is_show);
            }
            var result = Service.GetPathChildNodes(node_code, lang, predicat);
            return new RestfulData<object>(0, "ok", result.FirstOrDefault()?.children);
        }

        /// <summary>
        /// 获取栏目的直接下级栏目
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"*语种:sc简体,tc繁体,en英文",
        ///     "node_code":"*栏目代号"
        ///} 
        ///```
        /// </param>
        /// <returns></returns>
        [HttpPost("ChildrenNodes")]
        public RestfulData GetChildrenNodes(JObject data)
        {
            string lang = data["lang"].MyToString();
            string node_code = data["node_code"].MyToString().ToLower();
            var model = Service.GetByOne(a => a.node_code.ToLower() == node_code && a.lang == lang);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }

            List<Expression<Func<site_node, bool>>> predicat = new List<Expression<Func<site_node, bool>>>();
            predicat.Add(a => a.del_flag == DeleteEnum.UnDelete && a.review_status == ReViewStatusEnum.Pass);
            predicat.Add(a => a.is_show == StatusEnum.Legal);
            predicat.Add(a => a.lang == lang);
            predicat.Add(a => a.parent_node == model.id);
            var result = Service.GetList("nsort asc", predicat);
            return new RestfulData<object>(0, "ok", new { node = model, children = result });
        }


        /// <summary>
        /// 获取单个栏目信息
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id":"内容ID",
        ///     "node_code":"栏目代号",
        ///     "lang":"*语种:sc,tc,en"
        /// }
        /// ```
        /// - 说明
        /// id与node_code 不能同时为空
        /// </param>
        /// <returns></returns>
        [HttpPost("single")]
        public RestfulData SingleData(JObject data)
        {
            long id = data["id"].MyToLong();
            string node_code = data["node_code"].MyToString().ToLower() ;
            string lang = data["lang"].MyToString();
            if (lang == "")
            {
                lang = "sc";
            }
            if (node_code == "" && id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }
            Expression<Func<site_node, bool>> predicate = a => a.id == id;
            if (node_code != "")
            {
                predicate = a => a.node_code.ToLower() == node_code;
            }
            var model = Service.GetByOne("id asc", predicate);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            if (model == null)
            {
                return null;
            }
            return new RestfulData<site_node>(0, "ok", model);
        }

        /// <summary>
        /// 获取父级栏目
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"*语种:sc简体,tc繁体,en英文",
        ///     "node_code":"*栏目代号"
        ///} 
        ///```
        /// </param>
        /// <returns></returns>
        [HttpPost("ParentNodes")]
        public RestfulData GetParentNodes(JObject data)
        {
            string lang = data["lang"].MyToString();
            string node_code = data["node_code"].MyToString().ToLower();
            var model = Service.GetAllLevelParentNode( lang, node_code);
            

           
            return new RestfulData<object>(0, "ok", model);
        }
    }
}
