﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("站点配置获取")]
    public class SettingController : ApiBasicController<sys_account, ISysAccountService>
    {
        public SettingController(ISysAccountService service
                , IApplicationContextAccessor _httpContextAccessor)
            : base(service, _httpContextAccessor)
        {

        }

        /// <summary>
        /// 获取站点设置
        /// "editor_type": "使用的编辑器:UEditor/EWebEditor",
        /// "en_sitename": "英文站点名称[multi_language=false则忽略]",
        /// "favicon": "favicon",
        /// "logo": "站点logo",
        /// "outer_chain_picture": "是否允许外链",
        /// "sc_sitename": "简体站点名称",
        /// "show_data_lang": "默认数据显示语种",
        /// "site_meta_keywords": "meta 关键字",
        /// "tc_site_name": "繁体站点名称[multi_language=false则忽略]",
        /// "tpl_code": "模版代号",
        /// "multi_language": "是否为多语种站点:false单语种只有sc,true多语种包含:sc,en,tc"
        /// </summary>
        /// <returns>
        /// </returns>
        //[PermissionCheck]
        [HttpGet("setting")]
        public RestfulData GetSiteSetting([FromServices] IApplicationSettingService applicationSettingService)
        {

            string EditorType = applicationSettingService.Get("EditorType", "");
            string en_SiteName = applicationSettingService.Get("en_SiteName", "");

            string Favicon = applicationSettingService.Get("Favicon", "");
            string Logo = applicationSettingService.Get("Logo", "");
            bool OuterChainPicture = applicationSettingService.Get("OuterChainPicture", "true").MyToBool();
            string sc_SiteName = applicationSettingService.Get("sc_SiteName", "");
            string Show_Data_Lang = applicationSettingService.Get("Show_Data_Lang", "");
            string SiteMetaKeywords = applicationSettingService.Get("SiteMetaKeywords", "");

            string tc_SiteName = applicationSettingService.Get("tc_SiteName", "");
            string TplCode = applicationSettingService.Get("TplCode", "");

            bool MultiLanguage = httpContextAccessor.Current.MultiLanguage;

            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = new
                {
                    editor_type = EditorType,
                    en_sitename = en_SiteName,
                    favicon = Favicon,
                    logo = Logo,
                    outer_chain_picture = OuterChainPicture,
                    sc_sitename = sc_SiteName,
                    show_data_lang = Show_Data_Lang,
                    site_meta_keywords = SiteMetaKeywords,
                    tc_site_name = tc_SiteName,
                    tpl_code = TplCode,
                    multi_language = MultiLanguage
                }
            };
        }
        /// <summary>
        /// 实验动态条件
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "conditions":[
        ///         {
        ///             "condtion":
        ///             {
        ///                 "fields":[
        ///                  {
        ///                     "field_name":"字段名称",
        ///                     "operate":"操作:1等于,2大于,3小于,4大于等于,5小于等于,6包含Contains",
        ///                     "field_value":"条件值:string,int"
        ///                  }
        ///                  ],
        ///                 "field_operator":"运算符:1And,2Or"
        ///             },
        ///             "condition_operator":"运算符:1And,2Or"
        ///         }
        ///     ]
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [HttpPost("Check")]
        public RestfulData Check(JObject data)
        {
            List<DynamicConditons> result = new List<DynamicConditons>() {
                new DynamicConditons(){
                    condtion= new List<DynamicConditionItem>() {
                        new DynamicConditionItem(){
                            field_operator=1,
                            property_fields=new List<FieldItem>(){ 
                                new FieldItem(){ 
                                    field_name="parent_node",
                                    operate=1,
                                    field_value="1"
                                },
                                  new FieldItem(){
                                    field_name="lucene_index",
                                    operate=1,
                                    field_value="1"
                                }
                            }
                        }
                    },
                    condition_operator =1
                }
            };

            string json = JsonHelper.ToJson(result);

            var node = Activator.CreateInstance<site_node>();
            var answer_items = JsonHelper.ToObject<List<DynamicConditons>>(data["conditions"].MyToString());
            var predicate_all = LinqKit.PredicateBuilder.New<site_node>(true);

            foreach (var item in answer_items)
            {
                bool condition_defaultExpression = true;
                if (item.condition_operator == 2)
                {
                    condition_defaultExpression = false;
                }
                var predicate_item = LinqKit.PredicateBuilder.New<site_node>(condition_defaultExpression);
                var condtion = item.condtion;
                foreach (var condtion_item in condtion)
                {
                    bool defaultExpression = true;
                    if (condtion_item.field_operator == 2)
                    {
                        defaultExpression = false;
                    }
                    var predicate_codition = LinqKit.PredicateBuilder.New<site_node>(defaultExpression);
                    var fields = condtion_item.property_fields;
                    foreach (var field_item in fields)
                    {
                        var temp = GetSitePredicate(node, field_item.field_name, field_item.operate, field_item.field_value);
                        if (temp != null)
                        {
                            if (condtion_item.field_operator == 1)
                                predicate_codition = predicate_codition.And(temp);
                            else
                                predicate_codition = predicate_codition.Or(temp);
                        }
                    }
                    if (item.condition_operator == 1)
                    {
                        predicate_item = predicate_item.And(predicate_codition);
                    }
                    else
                    {
                        predicate_item = predicate_item.Or(predicate_codition);
                    }
                }

                predicate_all = predicate_all.And(predicate_item);
            }

            var model = Service.GetByOne<site_node>(predicate_all);
            //var predicate1 = LinqKit.PredicateBuilder.New<site_node>(true);
            //predicate1.And()
            //var node = Activator.CreateInstance<site_node>();
            //node.GetType().GetProperty("")

            return new RestfulData(1, "ok");
        }


        public Expression<Func<T1, bool>> GetSitePredicate<T1>(T1 t, string field_name, int operate, object field_value)
        {

            var pro = t.GetType().GetProperty(field_name);
            if (pro != null)
            {

                // var predicate1 = LinqKit.PredicateBuilder.New<T1>(true);
                var p = Expression.Parameter(typeof(T1), "x");
                //string lambda_field = field_name + "_parameter";
                //var field_name_parmeter = Expression.Parameter(pro.PropertyType, lambda_field);

                //1等于,2大于,3小于,4大于等于,5小于等于,6包含Contains
                string lambda = "";
                switch (operate)
                {
                    case 2:
                        lambda = $"x.{field_name} > {field_value}";
                        break;
                    case 3:
                        lambda = $"x.{field_name} < {field_value}";
                        break;
                    case 4:
                        lambda = $"x.{field_name} >= {field_value}";
                        break;
                    case 5:
                        lambda = $"x.{field_name} <= {field_value}";
                        break;
                    case 6:
                        lambda = $"x.{field_name}.Contains(\"{field_value}\")";
                        break;
                    default:
                        if(pro.PropertyType==typeof(int)|| pro.PropertyType == typeof(long) )
                            lambda = $"x.{field_name} = {field_value}";
                        else if (pro.PropertyType.BaseType == typeof(Enum))
                            lambda = $"Convert.ToInt32(x.{field_name}) = {field_value}";
                        else
                            lambda = $"x.{field_name} = \"{field_value}\"";
                        break;
                }
                var e = (Expression)DynamicExpressionParser.ParseLambda(new[] { p }, null, lambda);

                var typedExpression = (Expression<Func<T1, bool>>)e;

                return typedExpression;




            }
            return null;

        }
    }

    public class DynamicConditons
    {
        public List<DynamicConditionItem> condtion { get; set; }
        public int condition_operator { get; set; }


    }
    public class DynamicConditionItem
    {
        public List<FieldItem> property_fields { get; set; }
        public int field_operator { get; set; }
    }
    public class FieldItem
    {
        public string field_name { get; set; }
        public int operate { get; set; }
        public string field_value { get; set; }
    }
}
