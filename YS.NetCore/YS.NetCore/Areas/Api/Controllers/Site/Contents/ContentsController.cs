﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Extend;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.CO2NET.Extensions;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.WxOpen.AdvancedAPIs.Sns;
using Senparc.Weixin.WxOpen.Containers;
using Senparc.Weixin.WxOpen.Entities;
using Senparc.Weixin.WxOpen.Helpers;

namespace YS.NetCore.Areas.Api.Controllers.Site
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("前端使用--网站内容相关")]
    public class SiteContentsController : ApiBasicController<site_content, ISiteContentService>
    {


        public SiteContentsController(ISiteContentService service,
            IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }



        /// <summary>
        /// 获取栏内容列表
        /// node_code:分类代号
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "ipage":"页码",
        ///     "page_size":"页大小",
        ///     "condition":{"node_id":"关联栏目的ID,部根据此条件则此条件传0或者不传","node_code":"关联栏目的代号","lang":"*语种:sc,tc,en"}
        /// }
        /// ```
        /// 说明
        /// - 条件说明
        /// condition里面的条件除加*号的必传,其他的如不跟据此条件查询则可不传入
        /// </param>
        /// <returns></returns>
        [HttpPost("list")]
        
        public RestfulData LoadItemData(JObject data)
        {
            int ipage = data["ipage"].MyToInt();
            int page_size = data["page_size"].MyToInt();
            string node_code = "";
            string lang = "sc";
            long node_id = 0;
            if (data.ContainsKey("condition"))
            {
                node_id = data["condition"]["node_id"].MyToLong();
                node_code = data["condition"]["node_code"].MyToString().ToLower();
                lang = data["condition"]["lang"].MyToString();
            }
            if (lang == "")
            {
                lang = "sc";
            }

            var store_condition = new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>() {
                    a=>a.review_status==ReViewStatusEnum.Pass
                    &&a.is_show==StatusEnum.Legal
                    &&a.del_flag==DeleteEnum.UnDelete&&a.lang==lang
            };

            if (node_code != "")
            {
                store_condition.Add(a => a.node_code.ToLower() == node_code);
            }
            if (node_id>0)
            {
                store_condition.Add(a => a.node_id == node_id);
            }
            var spot_list = Service.GetPageList(ipage, page_size, "is_top desc,n_sort desc", store_condition);
            return new RestfulPageData<site_content>(0, "ok", spot_list);
        }


        /// <summary>
        /// 获取单个内容详细页面
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id":"内容ID",
        ///     "lang":"语种:sc,tc,en"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [HttpPost("single")]
        public RestfulData SingleData(JObject data)
        {
            long id = data["id"].MyToLong();
            string lang = data["lang"].MyToString();

            var model = Service.GetByOne("id asc", a => a.id == id&&a.lang==lang);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            if (model == null)
            {
                return null;
            }
            return new RestfulData<site_content>(0, "ok", model);
        }



        /// <summary>
        /// 获取栏目内容一条数据,is_top desc,n_sort desc
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "node_id":"栏目ID/代号必须传入一个",
        ///     "node_code":"栏目代号/栏目ID必须传入一个",
        ///     "lang":"语种:sc,tc,en"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [HttpPost("NodeContentSingle")]
        public RestfulData NodeContentSingleData(JObject data)
        {
            long node_id = data["node_id"].MyToLong();
            string node_code = data["node_code"].MyToString().ToLower();
            string lang = data["lang"].MyToString();
            if (node_id <= 0 && node_code == "")
            {

                return new RestfulData(-1, "node_id/node_code必须传入其中一个");
            }
            if (lang == "")
            {
                lang = "sc";
            }
            System.Linq.Expressions.Expression<Func<site_content, bool>> condtion = a => a.lang == lang&&a.is_show==StatusEnum.Legal&&a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete;
            if (node_id > 0 && node_code == "")
            {
                condtion = a => a.lang == lang && a.node_id == node_id && a.is_show == StatusEnum.Legal && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete;
            }
            else if (node_id == 0 && node_code != "")
            {
                condtion = a => a.lang == lang && a.node_code.ToLower() == node_code && a.is_show == StatusEnum.Legal && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete;
            }
            else
            {
                condtion = a => a.lang == lang && a.node_code.ToLower() == node_code && a.node_id == node_id && a.is_show == StatusEnum.Legal && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete;
            }
            var model = Service.GetByOne("is_top desc,n_sort desc", condtion);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            return new RestfulData<site_content>(0, "ok", model);
        }


    }
}
