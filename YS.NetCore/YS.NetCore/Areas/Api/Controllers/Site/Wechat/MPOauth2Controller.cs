﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Options;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.CO2NET.Extensions;
using Senparc.Weixin;
using Senparc.Weixin.Exceptions;
using Senparc.Weixin.MP;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.OAuth;

namespace YS.NetCore.Areas.Api.Controllers.Site
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("微信公众号认证相关")]
    public class MPOauth2Controller : ApiBasicController<site_member, ISiteMemberService>
    {


        public MPOauth2Controller(ISiteMemberService service,
            IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
        
        //下面换成账号对应的信息，也可以放入web.config等地方方便配置和更换
        public readonly string appId = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppId;//与微信公众账号后台的AppId设置保持一致，区分大小写。
        private readonly string appSecret = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppSecret;//与微信公众账号后台的AppId设置保持一致，区分大小写。
        //[HttpGet("test")]
        //public RestfulData Text([FromServices] Autofac.IComponentContext componentContext)
        //{
        //    using (var scope = Startup..BeginLifetimeScope())
        //    {
        //        IConfiguration config = scope.Resolve<IConfiguration>();
        //        IHostingEnvironment env = scope.Resolve<IHostingEnvironment>();
        //    }
        //    var service = componentContext.Resolve<ISiteSurveyAnswerService>();
        //    return new RestfulData(0, nameof(service));

        //}
        /// <summary>
        /// 微信公众号网页授权获取授权地址
        /// </summary>
        /// <param name="fileProvider"></param>
        /// <param name="routerKey">路由标志最后决定跳转去那里</param>
        /// <returns></returns>
        [HttpGet("oauth")]
        [AllowAnonymous]
        public IActionResult Index([FromServices] Microsoft.Extensions.Options.IOptions<SurveySettingOption> options, [FromServices] ISiteFileProvider fileProvider, [FromServices] IMyCache cache,string routerKey)
        {
           
            var SurveySetting = options.Value;
            //弹出授权页面，可通过openid拿到昵称、性别、所在地。并且，即使在未关注的情况下，只要用户授权，也能获取其信息
            var returnUrl = SurveySetting.OAuthBack;
            string oauth_url = OAuthApi.GetAuthorizeUrl(appId, returnUrl, routerKey, SurveySetting.Scope == "snsapi_userinfo" ? OAuthScope.snsapi_userinfo : OAuthScope.snsapi_base);
            return Redirect(oauth_url);
        }


        /// <summary>
        /// OAuthScope.snsapi_userinfo方式回调
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <param name="returnUrl">用户最初尝试进入的页面</param>
        /// <returns></returns>
        [HttpGet("userinfo")]
        [AllowAnonymous]
        public IActionResult UserInfoCallback([FromServices]IOptions<SurveySettingOption> options,[FromServices] ISiteFileProvider fileProvider, [FromServices] IMyCache cache, string code, string state, string returnUrl)
        {
            if (string.IsNullOrEmpty(code))
            {
                return Content("您拒绝了授权！");
            }
            Senparc.Weixin.MP.AdvancedAPIs.OAuth.OAuthAccessTokenResult result = null;
            //通过，用code换取access_token
            try
            {
                result = OAuthApi.GetAccessToken(appId, appSecret, code);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
            if (result.errcode != ReturnCode.请求成功)
            {
                return Content("错误：" + result.errmsg);
            }
            try
            {
               
                OAuthUserInfo userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                //使用SessionContainer管理登录信息（推荐）
                var unionId = userInfo.unionid;
                var openId = userInfo.openid;
                if (unionId.MyToString() == "")
                {
                    unionId = openId;
                }
                var model = Service.GetByOne("id asc", a => a.open_id == openId);
                bool need_add = false;
                if (model == null)
                {
                    need_add = true;
                }
                if (need_add)
                {
                    model = Service.Insert(new site_member()
                    {
                        last_comple_date = DateTime.MinValue,
                        avatar_url = userInfo.headimgurl,
                        city = userInfo.city,
                        comple_userinfo = Utils.StatusEnum.Legal,
                        country = userInfo.country,
                        gender = (GenderEnum)userInfo.sex,
                        nick_name = userInfo.nickname,
                        open_id = openId,
                        province = userInfo.province,
                        session_id = "",
                        session_key = "",
                        tel_no = "",
                        union_id = unionId,
                        last_login_time = DateTime.Now,
                        chr_account="",
                        chr_pwd="",
                        comple_status=StatusEnum.UnLegal,
                        tags="",
                        token="",
                        review_status=ReViewStatusEnum.UnReview
                    });
                }
                else
                {
                    //更新
                    Service.Modify(a => a.id == model.id, a => new site_member()
                    {
                        last_login_time = DateTime.Now
                    });
                }
                var SurveySetting = options.Value;//fileProvider.GetAppSettingValue<SurveySettingOption>("SurveySetting");
                string key = Guid.NewGuid().ToString("N");
                cache.SetAsync<site_member>(string.Format(WeChatConstants.WeChatMPAutoLoginKey, key), model);

                return Redirect(string.Format(SurveySetting.SuccessRedirect, key, state));
            }
            catch (ErrorJsonResultException ex)
            {
                return Content(ex.Message);
            }
        }

        /// <summary>
        /// OAuthScope.snsapi_base方式回调
        /// </summary>
        /// <param name="code"></param>
        /// <param name="state"></param>
        /// <param name="returnUrl">用户最初尝试进入的页面</param>
        /// <returns></returns>
        [HttpGet("baseuserinfo")]
        [AllowAnonymous]
        public IActionResult BaseCallback([FromServices] IOptions<SurveySettingOption> options, [FromServices] ISiteFileProvider fileProvider, [FromServices] IMyCache cache, string code, string state, string returnUrl)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                {
                    return Content("您拒绝了授权");
                }
                //通过，用code换取access_token
                var result = OAuthApi.GetAccessToken(appId, appSecret, code);
                if (result.errcode != ReturnCode.请求成功)
                {
                    return Content($"错误:{result.errmsg}");
                }
                //因为这里还不确定用户是否关注本微信，所以只能试探性地获取一下
                OAuthUserInfo userInfo = null;
                try
                {
                    //已关注，可以得到详细信息
                    userInfo = OAuthApi.GetUserInfo(result.access_token, result.openid);
                    //使用SessionContainer管理登录信息（推荐）
                    var unionId = userInfo.unionid;
                    var openId = userInfo.openid;
                    if (unionId.MyToString() == "")
                    {
                        unionId = openId;
                    }
                    var model = Service.GetByOne("id asc", a => a.open_id == openId);
                    bool need_add = false;
                    if (model == null)
                    {
                        need_add = true;
                    }
                    if (need_add)
                    {
                        model = Service.Insert(new site_member()
                        {
                            last_comple_date = DateTime.MinValue,
                            avatar_url = "",
                            city = "",
                            comple_userinfo = Utils.StatusEnum.Legal,
                            country = "",
                            gender = GenderEnum.Unknown,
                            nick_name = "",
                            open_id = openId,
                            province = "",
                            session_id = "",
                            session_key = "",
                            tel_no = "",
                            union_id = unionId,
                            last_login_time = DateTime.Now,
                            chr_account = "",
                            chr_pwd = "",
                            comple_status = StatusEnum.UnLegal,
                            tags = "",
                            token = "",
                            review_status = ReViewStatusEnum.UnReview
                        });
                    }
                    else
                    {
                        //更新
                        Service.Modify(a => a.id == model.id, a => new site_member()
                        {
                            last_login_time = DateTime.Now
                        });
                    }
                    var SurveySetting = options.Value;
                    string key = Guid.NewGuid().ToString("N");
                    cache.SetAsync<site_member>(string.Format(WeChatConstants.WeChatMPAutoLoginKey, key), model);
                    return Redirect(string.Format(SurveySetting.SuccessRedirect, key));
                }
                catch (ErrorJsonResultException ex)
                {
                    return Content($"用户已授权，授权Token：{result}");
                }
            }
            catch (Exception ex)
            {
                return Content($"发生错误,{ex.ToString()}");
            }
        }

        /// <summary>
        /// 认证之后拿querystring 的code 调用此接口换取自动登录
        /// </summary>
        /// <param name="Configuration"></param>
        /// <param name="data">
        /// ```json
        ///     {
        ///         "code":"querystring里面的code参数值"
        ///     }
        /// ```
        /// 说明
        /// - result.code=0 标识登录成功已成功换取到token
        /// - result.code=2 需要路由到绑定页面 并带入参数code等参数
        /// </param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("code2login")]
        public RestfulData Code2Login([FromServices] IConfiguration Configuration, [FromServices] IMyCache cache, JObject data)
        {

            string code = data["code"].MyToString();

            if (code == "")
            {
                return new RestfulData(-1, "参数错误");
            }
            var model1 = cache.Get<site_member>(string.Format(WeChatConstants.WeChatMPAutoLoginKey, code));

            if (model1 == null)
            {
                return new RestfulData(-1, "非法进入,请重新授权登录");
            }
            if (model1.id <= 0)
            {
                return new RestfulData(-1, "非法进入,请重新授权登录");
            }
            var model = Service.GetByOne(a => a.id == model1.id);
            if (model.review_status != ReViewStatusEnum.Pass)
            {
                return new RestfulData<object>(2, "需要绑定账号", new
                {
                    model.real_name,
                    code = code,
                    model.tags,
                    model.tel_no,
                    model.id_card,
                    model.card_no
                });
            }
        
            //jwt 登陆
            var JWTSecret = Configuration.GetSection("JWTSecret").Get<string>();
            var key = Encoding.ASCII.GetBytes(JWTSecret);
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(System.Security.Claims.ClaimTypes.NameIdentifier,model.id.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.GivenName,model.nick_name.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Name, model.open_id.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.MobilePhone,model.tel_no.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Hash,model.session_id.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.UserData,"member")
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string g_Token = tokenHandler.WriteToken(token);
            model.token = g_Token;

            return new RestfulData<object>(0, "ok", model);
        }
    }
}
