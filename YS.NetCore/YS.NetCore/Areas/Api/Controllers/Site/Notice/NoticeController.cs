﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("2.0")]
    [CommentControllerAttribute("前端使用--通知管理")]

    public class NoticeController : ApiBasicController<site_notice, ISiteNoticeService>
    {

        public NoticeController(ISiteNoticeService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }
        /// <summary>
        /// 我获取的通知列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "page_size":"页大小",
        ///     "page_index","页码"
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [SiteMemberCheck]
        [HttpPost("list")]
        public async Task<RestfulData> GetListAsync(JObject data)
        {



            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();


            List<string> MyTags = new List<string>();
            var member = httpContextAccessor.Current.MemberCustomer;


            MyTags.AddRange(member.Tag_Codes);
            var predicate = PredicateBuilder2.False<site_notice>();
            foreach (var item in MyTags)
            {
                predicate = predicate.Or(a => ("," + a.recipient + ",").Contains(("," + item + ",")));
            }


            var condition = new List<System.Linq.Expressions.Expression<Func<site_notice, bool>>>();
            condition.Add(predicate);


            var result = Service.GetPageList(page_index, page_size, "id desc", condition);

            var list = result.list.ToList();

            list.ForEach(a =>
            {
                if (("," + a.read_user + ",").Contains(("," + member.id + ",")))
                {
                    a.is_new = 0;
                }
                else
                {
                    a.is_new = 1;
                }
            });



            return new RestfulPageData<site_notice>(0, "ok", new PageOf<site_notice>()
            {
                list = list,
                page_index = result.page_index,
                page_size = result.page_size,
                total = result.total
            });
        }


        /// <summary>
        /// 获取单个通知详细
        /// </summary>
        /// <param name="id">通知ID</param>
        /// <returns></returns>
        [SiteMemberCheck]
        [HttpGet("single/{id}")]
        public RestfulData GetSingleSurvey(string id)
        {


            long id_long = id.MyToLong();
            if (id.IsNullOrEmpty() || id_long <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }
            var model = Service.GetByOne(a => a.id == id_long);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }

            var member = httpContextAccessor.Current.MemberCustomer;

            Service.Modify(a => a.id == model.id && !("," + a.read_user + ",").Contains(("," + member.id + ",")), a => new site_notice()
            {
                read_user = a.read_user + "," + member.id
            });

            return new RestfulData<site_notice>(0, "ok", model);
        }






    }
}
