﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.BackgroundService;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Options;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.Weixin;
using Senparc.Weixin.Entities.TemplateMessage;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("通知管理")]
    public class SiteNoticeController : ApiBasicController<site_notice, ISiteNoticeService>
    {

        public SiteNoticeController(ISiteNoticeService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }

        /// <summary>
        /// 删除通知
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"删除的通知ID"
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_notice", resource_code = "site_notice",operate_code ="delete")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long id = data["id"].MyToLong();
            var flag=Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                return new RestfulData(0, "删除成功");
            }
            else {
                return new RestfulData(-1, "删除失败");
            }

        }
            /// <summary>
            /// 获取通知列表
            /// </summary>
            /// <param name="data">
            ///```json
            /// {
            ///     "page_size":"页大小",
            ///     "page_index","页码",
            ///     "condition":{"chr_title":"标题,模糊检索,可不传"}
            /// }
            ///```
            /// </param>
            /// <returns></returns>
            [Net.Mvc.Authorize.PermissionCheck(module_code = "site_notice", resource_code = "site_notice")]
        [HttpPost("list")]
        public async Task<RestfulData> GetListAsync(JObject data)
        {



            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string chr_title = "";



            if (data.ContainsKey("condition"))
            {
                chr_title = data["condition"]["chr_title"].MyToString();
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<site_notice, bool>>>();
            if (chr_title.Trim() != "")
            {
                condition.Add(a => a.chr_title.Contains(chr_title));
            }

            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<site_notice>(0, "ok", result);
        }



        public static readonly string Token = Senparc.Weixin.Config.SenparcWeixinSetting.Token;//与微信小程序后台的Token设置保持一致，区分大小写。
        public static readonly string EncodingAESKey = Senparc.Weixin.Config.SenparcWeixinSetting.EncodingAESKey;//与微信小程序后台的EncodingAESKey设置保持一致，区分大小写。
        public static readonly string WxAppId = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppId;//与微信小程序后台的AppId设置保持一致，区分大小写。
        public static readonly string WxAppSecret = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppSecret;//与微信小程序账号后台的AppId设置保持一致，区分大小写。
        /// <summary>
        /// 新增通知信息[不能修改]
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "chr_title":"标题",
        /// "chr_desc":"简介",
        /// "chr_content":"内容富文本编辑器",
        /// "recipient":"接受者"
        /// }
        ///```
        /// 说明
        /// -  小徐做过这个
        /// - all 全体会员需登录
        /// - member 已审核会员
        /// - 具体标签t0,t1...tN 从接口/api/v1/sitemembertag/data/list获取,值为code多个用＂,＂分割
        /// - 具体会员p0,p1..pN从接口/api/v1/sitemember/data/list获取,值为code多个用＂,＂分割
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_notice", resource_code = "site_notice", operate_code = "add")]
        [HttpPost("save")]
        public async Task<RestfulData> SaveAsync([FromServices] Microsoft.Extensions.Options.IOptions<SurveySettingOption> options, [FromServices] ISiteFileProvider fileProvider,[FromServices] IMediator mediator, JObject data)
        {

            string chr_title = data["chr_title"].MyToString();
            string chr_desc = data["chr_desc"].MyToString();
            string chr_content = data["chr_content"].MyToString();
            string recipient = data["recipient"].MyToString();

            var user = httpContextAccessor.Current.CurrentUser;
            var model = Service.Insert(new site_notice()
            {
                chr_title = chr_title,
                chr_desc = chr_desc,
                chr_content = chr_content,
                recipient = recipient,
                read_user = "",
                create_time = DateTime.Now,
                create_user = user.id,
                create_username = user.alias_name
            });
            if (model.id > 0)
            {


             

                if (recipient != "public")
                {
                    //标题 {{thing1.DATA}} 发布时间 {{character_string5.DATA}} {{thing2.DATA}}
                    var SurveySetting = options.Value;
                    if (SurveySetting.WxTemplateMsgIds != "")
                    {
                        var testData = new //TestTemplateData()
                        {
                            thing1 = new TemplateDataItem(model.chr_title),
                            character_string5 = new TemplateDataItem(model.create_time.MyToDateTime()),
                            thing2 = new TemplateDataItem(model.chr_desc)
                        };
                        string NoticeDetaileRouterKey = $"/index/noticeDetail/?id={model.id}";
                        var savedJob = await mediator.Send(new JobAddContext()
                        {
                            action_type = "send_notice",
                            data = new { WxAppId, template_id = SurveySetting.WxTemplateMsgIds, msg_data = testData, notice = model, url = string.Format(SurveySetting.OAuthPage, NoticeDetaileRouterKey) }
                        });
                    }
                    //var send_result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(WxAppId,);
                    //if (send_result.errcode == ReturnCode.请求成功)
                    //{

                    //}
                }



                AddLog("site_notice", Utils.LogsOperateTypeEnum.Update, $"新增通知:{model.chr_title}");
                return new RestfulData(0, "新增成功");
            }
            else
            {
                return new RestfulData(-1, "新增失败");
            }
        }
        /// <summary>
        /// 获取单个通知详细
        /// </summary>
        /// <param name="id">通知ID</param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_notice", resource_code = "site_notice")]
        [HttpGet("single/{id}")]
        public RestfulData GetSingleSurvey(string id)
        {
            long id_long = id.MyToLong();
            if (id.IsNullOrEmpty() || id_long <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }
            var model = Service.GetByOne(a => a.id == id_long);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }

           
            return new RestfulData<site_notice>(0, "ok", model);
        }
        /// <summary>
        /// 查看通知阅读情况
        /// </summary>
        /// <param name="memberService"></param>
        /// <param name="data">
        ///```json
        /// {
        ///     "page_size":"页大小",
        ///     "page_index","页码",
        ///     "id":"通知ID"
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpPost("report")]
        public RestfulData ReportNotice([FromServices] ISiteMemberService memberService, JObject data)
        {
            string id = data["id"].MyToString();
            long id_long = id.MyToLong();
            if (id.IsNullOrEmpty() || id_long <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }
            var model = Service.GetByOne(a => a.id == id_long);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            if (model.recipient == "public")
            {
                return new RestfulData(-1, "数据设置接受者为公开无法统计");
            }

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string recipient = model.recipient;
            string read_user = string.Concat(",", model.read_user, ",");
            var condition = new List<System.Linq.Expressions.Expression<Func<site_member, bool>>>();
            
            if (recipient == "all")
            {
            }
            else if (recipient == "member")
            {
                condition.Add(a => a.review_status == ReViewStatusEnum.Pass);
            }
            else
            {
                var tags = recipient.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                var predicate = PredicateBuilder2.False<site_member>();
                foreach (var item in tags)
                {
                    if (item.StartsWith("t"))
                    {
                        predicate = predicate.Or(a => ("," + a.tags + ",").Contains("," + item + ","));
                    }
                    else if (item.StartsWith("p"))
                    {
                        long user_id = item.Replace("p", "").MyToLong();
                        if (user_id > 0)
                        {
                            predicate = predicate.Or(a => a.id == user_id);
                        }
                    }
                }
                condition.Add(predicate);
            }
            var result = memberService.GetList("id desc", condition);

            var list=  result.Select(a => new { 
                a.nick_name,
                is_read= read_user.Contains(string.Concat(",",a.id,","))
            });
            var result_list = list.OrderBy(a => a.is_read).Skip((page_index - 1) * page_size).Take(page_size);


            return new RestfulData<object>(0, "ok", new {
                list = result_list,
                total = list.Count()
            });


        }
    }
}
