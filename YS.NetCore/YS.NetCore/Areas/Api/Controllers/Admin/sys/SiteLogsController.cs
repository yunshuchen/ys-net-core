﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.BackgroundService;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Options;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.Weixin;
using Senparc.Weixin.Entities.TemplateMessage;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("操作日志")]
    public class SiteLogsController : ApiBasicController<sys_logs, ISysLogsService>
    {

        public SiteLogsController(ISysLogsService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }



        /// <summary>
        /// 获取操作日志列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "page_size":"页大小",
        ///     "page_index","页码",
        ///     "condition":{"log_user_name":"用户,模糊检索,可不传","log_module_name":"操作模块名称,模糊检索,可不传","operate_type":"操作类型:-1全部,其他可从data/list接口获取"}
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "sys_log", resource_code = "sys_log")]
        [HttpPost("list")]
        public RestfulData GetList(JObject data)
        {



            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string log_user_name = "";

            string log_module_name = "";

            int operate_type = -1;
            if (data.ContainsKey("condition"))
            {
                log_user_name = data["condition"]["log_user_name"].MyToString();
                log_module_name = data["condition"]["log_module_name"].MyToString();
                operate_type = data["condition"]["operate_type"].MyToInt(-1);
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<sys_logs, bool>>>();
            if (log_user_name.Trim() != "")
            {
                condition.Add(a => a.log_user_name.Contains(log_user_name));
            }
            if (log_module_name.Trim() != "")
            {
                condition.Add(a => a.log_module_name.Contains(log_module_name));
            }
            if (operate_type > 0)
            {
                condition.Add(a => a.operate_type == (Utils.LogsOperateTypeEnum)operate_type);
            }
            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<sys_logs>(0, "ok", result);
        }
        /// <summary>
        /// 获取日志操作类型
        /// </summary>
        /// <returns></returns>
        [HttpGet("data/list")]
        [Net.Mvc.Authorize.PermissionCheck]
        public async Task<RestfulData> GetNodeListAsync()
        {

            var list = EnumExt.GetEnumByValueDescription<LogsOperateTypeEnum>();


            return new RestfulData<object>(0, "ok", list);

        }

    }
}
