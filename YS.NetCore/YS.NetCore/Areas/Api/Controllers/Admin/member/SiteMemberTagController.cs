﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("会员标签,分组管理")]
    public class SiteMemberTagController : ApiBasicController<site_member_tag, ISiteMemberTagService>
    {
   
        public SiteMemberTagController(ISiteMemberTagService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {

            

        }
        /// <summary>
        /// 获取会员标签列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "page_size":"页大小",
        /// "page_index","页码",
        /// "condition":{"tag_name":"标签名称,模糊检索"}
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member_tag", resource_code = "site_member_tag")]
        [HttpPost("list")]
        public RestfulData GetList(JObject data)
        {

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string tag_name = "";

            if (data.ContainsKey("condition"))
            {
                tag_name = data["condition"]["tag_name"].MyToString();
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<site_member_tag, bool>>>();
            if (tag_name.Trim() != "")
            {
                condition.Add(a => a.tag_name.Contains(tag_name));
            }
            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<site_member_tag>(0, "ok", result);
        }

        /// <summary>
        /// 添加会员标签
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"会员标签ID,传入0则表示新增,非0则表示修改此ID的会员标签",
        /// "tag_name","标签名称"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member_tag", resource_code = "site_member_tag", operate_code ="add,mod")]
        [HttpPost("save")]
        public RestfulData Save([FromServices] ISysModuleService sys_module, [FromServices] ISysResourceService sys_resource
       , [FromServices] ISysOperateService sys_operate,JObject data)
        {

            long id = data["id"].MyToLong();

            string tag_name = data["tag_name"].MyToString();
            if (tag_name == "" )
            {
                return new RestfulData(){ code = -1, msg = "参数错误" };
            }
            if (id <= 0)
            {

                bool flag = false;
                var model = Service.Insert(new site_member_tag()
                {
                    tag_name = tag_name
                });
                flag = model.id>0;
                if (flag)
                {
                    AddLog("site_member_tag", LogsOperateTypeEnum.Add, $"新增会员标签:{tag_name}");

                    var module = sys_module.GetByOne("id asc", a => a.module_code == "site_member");

                    if (module != null)
                    {
                        var resource = sys_resource.GetByOne("id asc", a => a.res_code == "site_member");
                        if (resource != null)
                        {
                                sys_operate.Insert(new sys_operate_entity() {
                                p_module= module.id,
                                p_module_code= module.module_code,
                                p_res=resource.id,
                                p_res_code= resource.res_code,
                                operate_code=$"data_tag_{model.id}",
                                operate_name=$"查看[{tag_name}]标签会员",
                                api_url="",
                                n_sort=1,
                                description="",
                                is_menu=StatusEnum.UnLegal,
                                delete_status=DeleteEnum.UnDelete,
                                enable_status=StatusEnum.Legal,
                                status=StatusEnum.Legal,
                                last_modifier_id="1",
                                last_modifier_name="Administrator",
                                last_modified_time=DateTime.Now,
                                creator_id="1",
                                creator_name="Administrator",
                                created_time=DateTime.Now
                            });
                        }
                    }
                    //18301   33  site_member_tag 11924   site_member_tag
                    return new RestfulData() { code = 0, msg = "新增成功" };
                }
                else
                {
                    return new RestfulData() { code = -1, msg = "新增失败" };
                }
            }
            else
            {
                bool flag = false;
                flag = Service.Modify(a => a.id == id, a => new site_member_tag()
                {
                    tag_name = tag_name
                }) > 0;

                if (flag)
                {
                    var module = sys_module.GetByOne("id asc", a => a.module_code == "site_member");
                    if (module != null)
                    {
                        var resource = sys_resource.GetByOne("id asc", a => a.res_code == "site_member");
                        if (resource != null)
                        {
                            string operate_code = $"data_tag_{id}";
                            sys_operate.Modify(a => a.p_module == module.id && a.p_res == resource.id && a.operate_code == operate_code, a => new sys_operate_entity()
                            {
                                operate_name = $"查看[{tag_name}]标签会员",

                            });
                        }
                    }
                    AddLog("site_member_tag", LogsOperateTypeEnum.Update, $"修改会员标签:{tag_name}");
                    return new RestfulData() { code = 0, msg = "修改成功" };
                }
                else
                {
                    return new RestfulData() { code = 0, msg = "修改修改" };
                }

            }
        }


        /// <summary>
        /// 删除会员标签
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"扩展模型ID"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member_tag", resource_code = "site_member_tag", operate_code = "del")]
        [HttpPost("delete")]
        public RestfulData Delete([FromServices]ISiteMemberService siteMemberService, [FromServices] ISysModuleService sys_module, [FromServices] ISysResourceService sys_resource, [FromServices] ISysOperateService sys_operate, JObject data)
        {
            long id = data["id"].MyToLong();
            if (id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在,删除失败" };
            }
            var  count=siteMemberService.Count(a => ("," + a.tags + ",").Contains(",t" + id + ","));
            if (count > 0)
            {
                return new RestfulData() { code = -1, msg = "标签正在使用,无法删除" };
            }
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                AddLog("site_member_tag", LogsOperateTypeEnum.Delete, $"删除扩展模型:{model.tag_name}");

                var module = sys_module.GetByOne("id asc", a => a.module_code == "site_member");
                if (module != null)
                {
                    string operate_code = $"data_tag_{id}";
                    var resource = sys_resource.GetByOne("id asc", a => a.res_code == "site_member");
                    if (resource != null)
                    {
                        sys_operate.Delete(a => a.p_module == module.id && a.p_res == resource.id && a.operate_code == operate_code);
                        sys_operate.Delete<sys_role_operate_relation_entity>(a => a.module_code == "site_member" && a.res_code == "site_member" && a.operate_code == operate_code);
                    }
                }
                return new RestfulData() { code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }

        }



        /// <summary>
        /// 数据接口,只验证登录不验证权限,可用于其他模板下拉或选择使用
        /// 例如:
        ///
        /// - 在问卷选择哪标签的人可以见 value值为code
        /// - 会员修改/会员审核时的标签 value值为code
        /// - 返回[{"id":1,"code"="t1",tag_name="标签名称"}]
        /// </summary>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpGet("data/list")]
        public RestfulData GetDataList()
        {
            var condition = new List<System.Linq.Expressions.Expression<Func<site_member_tag, bool>>>();
            var result = Service.GetList("id desc", condition);
            return new RestfulData<object>(0, "ok", result.Select(a => new {code=$"t{a.id}", a.tag_name}));
        }
       
    }
}
