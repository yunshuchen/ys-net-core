﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Options;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("会员管理")]
    public class SiteMemberController : ApiBasicController<site_member, ISiteMemberService>
    {

        public SiteMemberController(ISiteMemberService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }
        /// <summary>
        /// 获取会员列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "page_size":"页大小",
        /// "page_index","页码",
        /// "condition":{"nick_name":"昵称,模糊检索","review_status":"审核状态-1全部,0待审核,1审核通过,2审核不通过","tags":"会员标签tags t0,t1...tN 从接口/api/v1/sitemembertag/data/list获取,值为code"}
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member", resource_code = "site_member")]
        [HttpPost("list")]
        public async Task<RestfulData> GetListAsync([FromServices] IDynamicPermissionCheck PermissionCheck, [FromServices] ISiteMemberTagService siteMemberTagService, JObject data)
        {


            
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string nick_name = "";
            int review_status = -1;
            string tags = "";


            if (data.ContainsKey("condition"))
            {
                nick_name = data["condition"]["nick_name"].MyToString();
                tags = data["condition"]["tags"].MyToString();
                review_status = data["condition"]["review_status"].MyToInt();
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<site_member, bool>>>();


            if (!await PermissionCheck.CheckAsync(HttpContext, "site_member", "site_member", "view_all"))
            {
                var tag_list = siteMemberTagService.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<site_member_tag, bool>>>());
                var predicate = PredicateBuilder2.False<site_member>();
                foreach (var item in tag_list)
                {
                    if (await PermissionCheck.CheckAsync(HttpContext, "site_member", "site_member", $"data_tag_{item.id}"))
                    {
                        predicate = predicate.Or(a => ("," + a.tags + ",").Contains(",t" + item.id + ","));
                    }
                }
                condition.Add(predicate);
            }
            if (nick_name.Trim() != "")
            {
                condition.Add(a => a.nick_name.Contains(nick_name));
            }

            if (review_status != -1)
            {
                condition.Add(a => a.review_status == (ReViewStatusEnum)review_status);
            }
            if (tags != "")
            {
                condition.Add(a => a.tags == tags);
            }
            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<site_member>(0, "ok", result);
        }
        public static readonly string Token = Senparc.Weixin.Config.SenparcWeixinSetting.Token;//与微信小程序后台的Token设置保持一致，区分大小写。
        public static readonly string EncodingAESKey = Senparc.Weixin.Config.SenparcWeixinSetting.EncodingAESKey;//与微信小程序后台的EncodingAESKey设置保持一致，区分大小写。
        public static readonly string WxAppId = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppId;//与微信小程序后台的AppId设置保持一致，区分大小写。
        public static readonly string WxAppSecret = Senparc.Weixin.Config.SenparcWeixinSetting.WeixinAppSecret;//与微信小程序账号后台的AppId设置保持一致，区分大小写。

        /// <summary>
        /// 审核待审核会员
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "id":"*会员ID",
        /// "review_status","*1审核通过2审核不通过",
        /// "tags":"/api/v1/sitemembettag/data/list 接口的ID,多个用,分割,可以传空"
        /// }
        ///```
        /// 说明
        /// - 
        /// - tags t0,t1...tN 从接口/api/v1/sitemembertag/data/list获取,值为code多个用＂,＂分割
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member", resource_code = "site_member", operate_code = "review")]
        [HttpPost("Review")]
        public RestfulData Review([FromServices] Microsoft.Extensions.Options.IOptions<SurveySettingOption> options, [FromServices] ISiteFileProvider fileProvider, JObject data)
        {
            long id = data["id"].MyToLong();
            int review_status = data["review_status"].MyToInt();
            string tags = data["tags"].MyToString();

            if (id <= 0 || (review_status != 1 && review_status != 2))
            {
                return new RestfulData(-1, "参数错误");
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            var flag = Service.Modify(a => a.id == id, a => new site_member()
            {
                review_status = (ReViewStatusEnum)review_status,
                tags = tags
            });
            if (flag > 0)
            {
                /***
                 * 
 标题：审核结果通知
详细内容：
  {{first.DATA}}
  审核类型：{{keyword1.DATA}}
  审核结果：{{keyword2.DATA}}
  审核时间：{{keyword3.DATA}}
  {{remark.DATA}}


标题：审核结果通知
详细内容：
  您的会员信息已审核，具体结果如下：
  审核类型：会员审核
  审核结果：通过/未通过
  审核时间：2020年11月27日 16:13
  如需协助，请联系区域管理人员！
                 * */
                var SurveySetting = options.Value;
                if (SurveySetting.WxReviewMsgId != "")
                {
                    if (review_status == 1)
                    {



                        var testData = new //TestTemplateData()
                        {
                            first = new TemplateDataItem("您的会员信息已审核，具体结果如下："),
                            keyword1 = new TemplateDataItem("会员审核"),
                            keyword2 = new TemplateDataItem("审核通过"),
                            keyword3 = new TemplateDataItem(DateTime.Now.MyToDateTime("yyyy年MM月dd日 HH:mm")),
                            remark = "如需协助，请联系区域管理人员！"
                        };
                        string url = string.Format(SurveySetting.OAuthPage, "/index/personal/?node_code=Rglmanage");
                        var send_result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(WxAppId, model.open_id, SurveySetting.WxReviewMsgId, url, testData);


                        Console.WriteLine($"发送模板消息{JsonHelper.ToJson(send_result)}");

                    }
                    else
                    {
                        var testData = new
                        {
                            first = new TemplateDataItem("您的会员信息已审核，具体结果如下："),
                            keyword1 = new TemplateDataItem("会员审核"),
                            keyword2 = new TemplateDataItem("审核不通过"),
                            keyword3 = new TemplateDataItem(DateTime.Now.MyToDateTime("yyyy年MM月dd日 HH:mm")),
                            remark = "如需协助，请联系区域管理人员！"
                        };
                        string url = string.Format(SurveySetting.OAuthPage, "index/userbind");
                        var send_result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessage(WxAppId, model.open_id, SurveySetting.WxReviewMsgId, url, testData);

                        Console.WriteLine($"发送模板消息{JsonHelper.ToJson(send_result)}");

                    }
                }
                AddLog("site_member", Utils.LogsOperateTypeEnum.ReView, $"审核会员:{model.nick_name},状态为:{EnumExt.GetDescriptionByEnum((ReViewStatusEnum)review_status)}");
                return new RestfulData(0, "审核成功");
            }
            else
            {
                return new RestfulData(-1, "审核失败");
            }
        }


        /// <summary>
        /// 修改会员信息
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "id":"*会员ID,必传",
        /// "id_card":"身份证号码",
        /// "card_no":"卡号",
        /// "real_name":"真实姓名",
        /// "tags":"/api/v1/sitemembettag/data/list 接口的code,多个用,分割,可以传空"
        /// }
        ///```
        /// 说明
        /// - 
        /// - tags t0,t1...tN 从接口/api/v1/sitemembertag/data/list获取,值为code多个用＂,＂分割
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_member", resource_code = "site_member", operate_code = "mod")]
        [HttpPost("save")]
        public RestfulData Modify(JObject data)
        {
            long id = data["id"].MyToLong();

            string tags = data["tags"].MyToString();
            string id_card = data["id_card"].MyToString();
            string card_no = data["card_no"].MyToString();
            string real_name = data["real_name"].MyToString();
            if (id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }

            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData(-1, "数据不存在");
            }
            var flag = Service.Modify(a => a.id == id, a => new site_member()
            {
                tags = tags,
                id_card = id_card,
                card_no = card_no,
                real_name = real_name
            });
            if (flag > 0)
            {
                AddLog("site_member", Utils.LogsOperateTypeEnum.Update, $"修改会员信息:{model.nick_name}");
                return new RestfulData(0, "修改成功");
            }
            else
            {
                return new RestfulData(-1, "修改失败");
            }
        }


        /// <summary>
        /// 数据接口,只验证登录不验证权限,可用于其他模板下拉或选择使用
        /// 例如:
        ///
        /// - 在问卷选择哪具体会员可以见value值为code
        /// - 返回[{"id":1,"code"="p1","nick_name"="会员名称","real_name":"真实姓名"}]
        /// </summary>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpGet("data/list")]
        public RestfulData GetDataList()
        {

            var condition = new List<System.Linq.Expressions.Expression<Func<site_member, bool>>>();
            var result = Service.GetList("id desc", condition);
            return new RestfulData<object>(0, "ok", result.Select(a => new { code = $"p{a.id}", a.nick_name,a.real_name }));
        }

      
        /// <summary>
        /// 首页新会员列表Top10
        /// </summary>
        /// <param name="PermissionCheck"></param>
        /// <param name="siteMemberTagService"></param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpGet("index/newmember")]
        public async Task<RestfulData> GetListAsync([FromServices] IDynamicPermissionCheck PermissionCheck, [FromServices] ISiteMemberTagService siteMemberTagService)
        {



            var condition = new List<System.Linq.Expressions.Expression<Func<site_member, bool>>>();
            condition.Add(a => a.review_status == ReViewStatusEnum.Pass);
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_member", "site_member", "view_all"))
            {
                var tag_list = siteMemberTagService.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<site_member_tag, bool>>>());
                var predicate = PredicateBuilder2.False<site_member>();
                foreach (var item in tag_list)
                {
                    if (await PermissionCheck.CheckAsync(HttpContext, "site_member", "site_member", $"data_tag_{item.id}"))
                    {
                        predicate = predicate.Or(a => ("," + a.tags + ",").Contains(",t" + item.id + ","));
                    }
                }
                condition.Add(predicate);
            }
            var result = Service.GetPageList(1,10,"id desc", condition);
            return new RestfulArray<site_member>(0, "ok", result.list);
        }


    }

}
