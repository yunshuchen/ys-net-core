﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("附件相关")]
    public class AttachmentController : ApiBasicController<sys_account, ISysAccountService>
    {
        private readonly IWebHostEnvironment environment;
        private readonly ISiteFileProvider siteProvider;
        public AttachmentController(ISysAccountService service
                , IApplicationContextAccessor _httpContextAccessor ,IWebHostEnvironment _environment,
           ISiteFileProvider _siteProvider)
            : base(service, _httpContextAccessor)
        {
            siteProvider = _siteProvider;
            this.environment = _environment;
        }

        /// <summary>
        /// 文件上传图片(.png,.jpg,.jpeg,.gif,.bmp,webp,.ico,.svg)
        /// </summary>
        /// <param name="data">
        /// ```json
        /// 表单方式提交,只接收文件类型为:.png,.jpg,.jpeg,.gif,.bmp,webp,.ico,.svg
        /// ```
        /// </param>
        /// <returns></returns>
        [RequestSizeLimit(100*1024*1024)]
        [HttpPost("upload/image")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData UploadImg([FromServices] Microsoft.Extensions.Options.IOptions<Net.Options.AllowFileOptions> _options, IFormCollection data)
        {
            var sAllowExt = _options.Value.ImageAllowFiles;
            if (data.Files.Count <= 0)
            {
                return new RestfulData(-1, "请选择要上传的文件");
            }

            string BasePath = siteProvider.UploadStaticContentPath;
            string content_build_path = siteProvider.ContentUploadBuildPath;
            BasePath = Path.Combine(BasePath, content_build_path);
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }

            List<object> list = new();
            foreach (var item in data.Files)
            {
                string file_name = item.FileName;
                string ext = file_name.GetFileExt();
                if (!sAllowExt.Contains(ext))
                {
                    continue;                
                }
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();

                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                string save_path = Path.Combine(BasePath, new_name);
                item.SaveAs(save_path);
                string relative_path = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                list.Add(new { path = relative_path, alt = file_name });
            }
            if (list.Count() == 1)
            {

                return new RestfulData<object>(0, "ok", list[0]);
            }
            else if (list.Count() > 1)
            {
                return new RestfulData<object>(0, "ok", list);
            }
            else
            {
                return new RestfulData<object>(-1, "文件上传失败", list);
            }
        }


        /// <summary>
        /// 多媒体文件上传(.flv,.swf,.mkv,.avi,.rm,.rmvb,.mpeg,.mpg,.ogg,.ogv,.mov,.wmv,.mp4,.webm,.mp3,.wav,.mid,.3gp)
        /// </summary>
        /// <param name="data">
        /// ```json
        /// 表单方式提交,只接收文件类型为:.flv,.swf,.mkv,.avi,.rm,.rmvb,.mpeg,.mpg,.ogg,.ogv,.mov,.wmv,.mp4,.webm,.mp3,.wav,.mid,.3gp
        /// ```
        /// </param>
        /// <returns></returns>
        [RequestSizeLimit(1024 * 1024 * 1024)]
        [HttpPost("upload/media")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData UploadMedia([FromServices] Microsoft.Extensions.Options.IOptions<Net.Options.AllowFileOptions> _options, IFormCollection data)
        {
            var sAllowExt = _options.Value.MediaAllowFiles;
            if (data.Files.Count <= 0)
            {
                return new RestfulData(-1, "请选择要上传的文件");
            }

            string BasePath = siteProvider.UploadStaticContentPath;
            string content_build_path = siteProvider.ContentUploadBuildPath;
            BasePath = Path.Combine(BasePath, content_build_path);
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }

            List<object> list = new();
            foreach (var item in data.Files)
            {
                string file_name = item.FileName;
                string ext = file_name.GetFileExt();
                if (!sAllowExt.Contains(ext))
                {
                    continue;
                }
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();
                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                string save_path = Path.Combine(BasePath, new_name);
                item.SaveAs(save_path);
                string relative_path = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                list.Add(new { path = relative_path, alt = file_name });
            }
            if (list.Count() == 1)
            {

                return new RestfulData<object>(0, "ok", list[0]);
            }
            else if (list.Count() > 1)
            {
                return new RestfulData<object>(0, "ok", list);
            }
            else
            {
                return new RestfulData<object>(-1, "文件上传失败", list);
            }
        }



        /// <summary>
        /// 文件上传(.png,.jpg,.jpeg,.gif,.bmp,.icon,.webp,.flv,.swf,.mkv,.avi,.rm,.rmvb,.mpeg,.mpg,.ogg,.ogv,.mov,.wmv,.mp4,.webm,.mp3,.wav,.mid,.rar,.zip,.tar,.gz,.7z,.bz2,.cab,.iso,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.pdf,.txt,.md,.xml,.3pg
        /// </summary>
        /// <param name="data">
        /// ```json
        /// 表单方式提交,只接收文件类型为:
        /// ```
        /// </param>
        /// <returns></returns>
        [RequestSizeLimit(1024*1024*1024)]
        [HttpPost("upload/file")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData UploadFile([FromServices] Microsoft.Extensions.Options.IOptions<Net.Options.AllowFileOptions> _options, IFormCollection data)
        {
            var sAllowExt = _options.Value.FileAllowFiles;
            if (data.Files.Count <= 0)
            {
                return new RestfulData(-1, "请选择要上传的文件");
            }

            string BasePath = siteProvider.UploadStaticContentPath;
            string content_build_path = siteProvider.ContentUploadBuildPath;
            BasePath = Path.Combine(BasePath, content_build_path);
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }

            List<object> list = new();
            foreach (var item in data.Files)
            {
                string file_name = item.FileName;
                string ext = file_name.GetFileExt();
                if (!sAllowExt.Contains(ext))
                {
                    continue;
                }
                file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
                string newfile_yinpin = file_name.ToPinYingGuid();

                string new_name = newfile_yinpin + ext;
                while (true)
                {
                    if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                    {
                        break;
                    }
                    else
                    {
                        new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                    }
                }
                string save_path = Path.Combine(BasePath, new_name);
                item.SaveAs(save_path);
                string relative_path = string.Concat("/", siteProvider.StaticDirectory, "/" + siteProvider.StaticContentDirectory, "/") + content_build_path.Replace("\\", "/").Replace(@"\", "/") + "/" + new_name;
                list.Add(new { path = relative_path, alt = file_name });
            }
            if (list.Count() == 1)
            {

                return new RestfulData<object>(0, "ok", list[0]);
            }
            else if (list.Count() > 1)
            {
                return new RestfulData<object>(0, "ok", list);
            }
            else
            {
                return new RestfulData<object>(-1, "文件上传失败", list);
            }
        }
    }
}
