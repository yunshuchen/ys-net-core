﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("扩展模型管理")]
    public class ExtModelController : ApiBasicController<sys_ext_model, ISysExtModelService>
    {
   
        public ExtModelController(ISysExtModelService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {

            

        }
        /// <summary>
        /// 获取扩展模型分页数据
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        /// "page_size":"页大小",
        /// "page_index","页码",
        /// "condition":{"chr_name":"模型名称","chr_code":"代号","status":"状态:-1全部,1启用,0禁用"}
        /// }
        ///```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel")]
        [HttpPost("list")]
        public RestfulData GetList(JObject data)
        {

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string chr_name = "";
            string chr_code = "";
            int status = -1;
            if (data.ContainsKey("condition"))
            {
                chr_name = data["condition"]["chr_name"].MyToString();
                chr_code = data["condition"]["chr_code"].MyToString();
                status = data["condition"]["status"].MyToInt(-1);
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<sys_ext_model, bool>>>();
            if (chr_name.Trim() != "")
            {
                condition.Add(a => a.chr_name.Contains(chr_name));
            }
            if (chr_code.Trim() != "")
            {
                condition.Add(a => a.chr_code.Contains(chr_code));
            }
            if (status != -1)
            {
                condition.Add(a => a.status==(StatusEnum)status);
            }
            var result = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<sys_ext_model>(0, "ok", result);
        }

        /// <summary>
        /// 添加扩展模型
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"扩展模型ID,传入0则表示新增,非0则表示修改此ID的扩展模型",
        /// "chr_name","名称",
        /// "chr_code":"代号,可以英文+数字+下划线但不能以数字开头,位数要求:3-12",
        /// "status":"1启用,2禁用"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code ="add,mod")]
        [HttpPost("save")]
        public RestfulData Save(JObject data)
        {

            long id = data["id"].MyToLong();

            string chr_name = data["chr_name"].MyToString();
            string chr_code = data["chr_code"].MyToString();
            int status = data["status"].MyToInt();
            if (chr_name == "" || (status != 0 && status != 1))
            {
                return new RestfulData(){ code = -1, msg = "参数错误" };
            }
            if (id <= 0)
            {


                var check = Service.GetByOne("", a => a.chr_code == chr_code);
                if (check != null)
                {
        
                    return new RestfulData() { code = -1, msg = "该代号的扩展模型已存在" };
                }

                bool flag = false;
                flag = Service.Insert(new sys_ext_model()
                {
                    chr_code = chr_code,
                    chr_name = chr_name,
                    status = (StatusEnum)status
                }).id > 0;
                if (flag)
                {

                    AddLog("site_extmodel", LogsOperateTypeEnum.Add, $"新增扩展模型:{chr_name}");


                    return new RestfulData() { code = 0, msg = "新增成功" };
                }
                else
                {
                    return new RestfulData() { code = -1, msg = "新增失败" };
                }
            }
            else
            {
                bool flag = false;

                var check = Service.GetByOne("", a => a.chr_code == chr_code && a.id != id);
                if (check != null)
                {

                    return new RestfulData() { code = -1, msg = "该代号的扩展模型已存在" };
                }
                flag = Service.Modify(a => a.id == id, a => new sys_ext_model()
                {

                    //chr_code = chr_code,
                    chr_name = chr_name,
                    status = (StatusEnum)status

                }) > 0;

                if (flag)
                {

                    AddLog("site_extmodel", LogsOperateTypeEnum.Update, $"修改扩展模型:{chr_name}");
                    
                    return new RestfulData() { code = 0, msg = "修改成功" };
                }
                else
                {
                    return new RestfulData() { code = 0, msg = "修改修改" };
                }

            }
        }


        /// <summary>
        /// 修改扩展模型状态
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"扩展模型ID",
        /// "status":"1启用,0禁用"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "mod")]
        [HttpPost("change_status")]
        public RestfulData change(JObject data)
        {
            int status = data["status"].MyToInt();
            long id = data["id"].MyToLong();

            if (status < 0 || id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }

            if (status != 0 && status != 1)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model=Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在,修改失败" };
            }


            int flag = Service.Modify(a => a.id == id, a => new sys_ext_model()
            {
                status = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog("site_extmodel", LogsOperateTypeEnum.Update, $"修改扩展模型状态:{model.chr_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");


                return new RestfulData() { code = 0, msg = "操作成功" };
            }
            else
            {
                return new RestfulData() { code =-1, msg = "操作失败" };
            }
        }

        /// <summary>
        /// 删除扩展模型
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"扩展模型ID"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "del")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long id = data["id"].MyToLong();


            if (id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在,删除失败" };
            }

            var check = Service.GetByOne<site_node>(a => a.ext_model.ToLower() == model.chr_code.ToLower()||a.node_ext_model==model.chr_code.ToLower());
            if (check != null && check.id > 0)
            {
                return new RestfulData() { code = -1, msg = "已有栏目使用此模型无法删除" };
            }
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                AddLog("site_extmodel", LogsOperateTypeEnum.Delete, $"删除扩展模型:{model.chr_name}");
                
                return new RestfulData() { code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }

        }

        /// <summary>
        /// 保存扩展模型字段
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///"model_id":"父级扩展模型ID", 
        ///"items":
        ///	[
        ///		{
        ///			"key":"代号,可以英文+数字+下划线但不能以数字开头,位数要求:3-12",
        ///			"chr_name":"标头",
        ///			"type":"控件类型,text/textarea/upload/uploadlist/datetime/time/date/editor/select/radio/checkbox",
        ///			"sort":"排序",
        ///			"formart_value":"选项值:只有在select/radio/checkbox 生效;格式:项与项之间以 |#| 分割,value与name以 |,| 分割",
        ///			"remark":"说明" 
        ///        } 
        ///	] 
        ///} 
        ///```
        ///说明:
        ///
        ///- upload 单文件上传
        ///- uploadlist 多文件上传多个文件路径以; 分割
        ///- datetime 格式:yyyy-MM-dd HH:mm:ss
        ///- time 格式: HH:mm:ss
        ///- date 格式: yyyy-MM-dd
        ///- select/radio/checkbox:必须填写formart_value
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel", operate_code = "mod,add")]
        [HttpPost("SaveField")]
        public RestfulData SaveDSet([FromServices] ISysExtModelDetailesService extdetailrservice,JObject data)
        {

            long id = data["model_id"].MyToLong();
            if (id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "扩展模型不存在" };
            }

            var list = JsonHelper.ToObject<List<sys_ext_model_detail>>(data["items"].ToString());

            string pcode = model.chr_code;
            if (pcode.MyToString().Trim() == "")
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            extdetailrservice.Delete(a => a.p_code.ToLower() == pcode.ToLower());

            list = list.Select(a =>
            {
                a.value = "";
                a.p_code = pcode;
                return a;
            }).ToList();


            extdetailrservice.BulkInsert(list);
            return new RestfulData() { code = 0, msg = "保存成功" };
        }



        /// <summary>
        /// 数据接口,只验证登录不显峥权限,可用于其他模板下拉或选择使用
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        /// "condition":{"status":"状态:-1全部,1启用,0禁用"}
        /// }
        /// ```
        ///说明:
        ///
        /// - 数据接口获取数据不验证权限,只验证登录
        ///
        ///例如:
        ///
        /// - 栏目使用下拉选择栏目使用的扩展模型
        /// - 栏目关联内容使用的扩展模型
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("data/list")]
        public RestfulData GetDataList(JObject data)
        {


                int status = -1;
            if (data.ContainsKey("condition"))
            {
                status = data["condition"]["status"].MyToInt(-1);
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<sys_ext_model, bool>>>();
            
            if (status != -1)
            {
                condition.Add(a => a.status == (StatusEnum)status);
            }
            var result = Service.GetList("id desc", condition);
            return new RestfulData<object>(0, "ok", result.Select(a => new { a.id, a.chr_code, a.chr_name }));
        }
        /// <summary>
        /// 获取单个扩展模型详细
        /// </summary>
        /// <param name="extdetailrservice"></param>
        /// <param name="data">
        /// ```json
        /// {
        /// "id":"扩展模型ID"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_extmodel", resource_code = "site_extmodel")]
        [HttpPost("single")]
        public RestfulData GetSingleExtModel([FromServices] ISysExtModelDetailesService extdetailrservice, JObject data)
        {
            long id = data["id"].MyToLong();


            if (id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在" };
            }
            var fields = extdetailrservice.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() { a => a.p_code == model.chr_code });
            return new RestfulData<object>(0, "ok", new { model, fields });

        }
    }
}
