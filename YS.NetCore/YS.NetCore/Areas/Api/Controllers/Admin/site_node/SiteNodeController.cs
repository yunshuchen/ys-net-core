﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("栏目管理")]
    public class SiteNodeController : ApiBasicController<site_node, ISiteNodeService>
    {

        public SiteNodeController(ISiteNodeService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }
        /// <summary>
        /// 获取栏目树形结构
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"语种:sc简体,tc繁体,en英文"
        ///} 
        ///```
        ///权限判断:
        ///
        /// - add_disable true则没有添加权限,false则有添加权限
        /// - mod_disable true则没有修改权限,false则有修改权限
        /// - del_disable true则没有删除权限,false则有删除权限
        /// - 点击树形结构时,根据点击节点的add_disable判断此点击节点是否有添加按钮的出现
        /// - 此节点加载的列表(list接口)数据的ID匹配tree节点的ID匹配判断mod_disable/del_disable判断修改/删除按钮的出现
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("tree")]
        public async Task<RestfulData> GetNodeTreeAsync(JObject data)
        {
            string lang = data["lang"].MyToString();
            var result = await Service.GetTreeDataAsync("node", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);
            return new RestfulData<object>(0, "ok", result);
        }

        /// <summary>
        /// 获取栏目列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "pid":"父级栏目的ID,即点击tree节点的ID",
        ///     "lang":"语种:sc,tc,en"
        /// }
        ///```
        ///
        ///权限判断:
        ///
        /// - add_disable true则没有添加权限,false则有添加权限
        /// - mod_disable true则没有修改权限,false则有修改权限
        /// - del_disable true则没有删除权限,false则有删除权限
        /// - 点击树形结构时,根据点击节点的add_disable判断此点击节点是否有添加按钮的出现
        /// - 此节点加载的列表数据的ID匹配tree节点的ID匹配判断mod_disable/del_disable判断修改/删除按钮的出现
        /// - 当pid=0时返回的res.data 为site_node集合,否则为{model:"当前栏目","children":"子栏目"}
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("list")]
        public RestfulData getNodeList(JObject data)
        {

            long pid = 0;
            string lang = "sc";
            pid = data["pid"].MyToLong();
            lang = data["lang"].MyToString();
            var condition = new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>();
            condition.Add(a => a.parent_node == pid);

            condition.Add(a => a.lang == lang && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete);
            var result = Service.GetList("parent_node asc,nsort asc,create_time asc", condition);
            object obj = null;
            if (pid != 0)
            {
                var model = Service.GetByOne(a => a.id == pid && a.lang == lang);
                obj = new { model, children = result };
            }
            else
            {
                obj = result;
            }
            return new RestfulData<object>(0, "ok", obj);
        }

        /// <summary>
        /// 获取单条栏目数据
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 在修改绑定页面时公用字段包括:parent_node/node_ext_model/ext_model/node_code/need_review/show_menu/show_path/nsort/lucene_index/pwd/is_show 直接使用sc.data的数据即可
        /// - 详细字段请看首页
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("single")]
        public async Task<RestfulData> GetSingleNodeAsync([FromServices] IDynamicPermissionCheck PermissionCheck, [FromServices] ISysExtModelDetailesService ext, JObject data)
        {
            
            long id = data["id"].MyToLong();
     
            site_node tc_model = new site_node();
            site_node sc_model = new site_node();
            site_node en_model = new site_node();


            if (!string.IsNullOrEmpty(id.MyToString()) && id.MyToLong() > 0)
            {
                tc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (tc_model == null || tc_model.id <= 0)
            {
                tc_model = new site_node();

            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_node();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_node();
            }

            List<sys_ext_model_detail> tc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> sc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> en_list = new List<sys_ext_model_detail>();
            if (sc_model.node_ext_model.MyToString() != "")
            {
                sc_list = ext.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() {
                a=>a.p_code==sc_model.node_ext_model
                });

                tc_list = sc_list.Clone();
                en_list = sc_list.Clone();
                if (Convert.ToInt64(string.Concat(sc_model.id, "")) > 0)
                {
                    {
                        if (sc_model.extend_fields != null)
                        {
                            var ext_sc = new List<sys_ext_model_detail>();
                            foreach (var property in sc_model.extend_fields)
                            {
                                ext_sc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.MyToString(),
                                    value = property.value.MyToString()
                                });
                            }
                            sc_list.ForEach(a =>
                            {
                                var db = ext_sc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }

                    {
                        if (tc_model.extend_fields != null)
                        {
                            var ext_tc = new List<sys_ext_model_detail>();
                        
                            foreach (var property in tc_model.extend_fields)
                            {
                                ext_tc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.MyToString(),
                                    value = property.value.MyToString()
                                });
                            }
                            tc_list.ForEach(a =>
                            {
                                var db = ext_tc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                        
                    }

                    {
                        if (en_model.extend_fields != null)
                        {
                            var ext_en = new List<sys_ext_model_detail>();

                            foreach (var property in en_model.extend_fields)
                            {
                                ext_en.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.MyToString(),
                                    value = property.value.MyToString()
                                });
                            }

                            en_list.ForEach(a =>
                            {
                                var db = ext_en.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }
                }
            }
            return new RestfulData<object>(0, "ok", new { sc = new { data = sc_model, ext_model = sc_list }, tc = new { data = tc_model, ext_model = tc_list }, en = new { data = en_model, ext_model = en_list } });
        }

        /// <summary>
        /// 新增保存/修改保存
        /// </summary>
        /// <param name="tableid"></param>
        /// <param name="modify_mark"></param>
        /// <param name="node_mark"></param>
        /// <param name="searcher"></param>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"栏目ID,为0时则表示新增,否则则表示修改此栏目ID"
        ///     "parent_node":"*父级栏目ID",
        ///     "node_ext_model":"栏目使用的扩展模型代号:/api/extmodel/data/list 接口下拉的chr_code",
        ///     "ext_model":"栏目关联使用的扩展模型代号代号:/api/extmodel/data/list 接口下拉的chr_code",
        ///     "node_code":"*栏目代号:可英文,数字,下划线组合但不能以数字开头,栏目代号不能修改",
        ///     "show_menu":"是否菜单处显示:1菜单处显示,0菜单处不显示",
        ///     "show_path":"是否导航处显示:1导航处显示,0导航处不显示",
        ///     "is_show":"是否显示:1显示,0不显示",
        ///     "nsort":"排序号,可不填,后台自动生成",
        ///     "lucene_index":"是否全文索引:1全文索引,0不全文索引",
        ///     "pwd":"栏目访问安全密码",
        ///     "need_review":"是否关联内容需要审核:1需要,0不需要"
        ///     "sc":
        ///     {
        ///         "node_name":"*栏目名称",
        ///         "out_link":"外链地址",
        ///         "node_tpl":"栏目页模版路径",
        ///         "list_tpl":"列表页模板路径",
        ///         "content_tpl":"详细页模板路径",
        ///         "node_pic":"栏目图片",
        ///         "node_atlas":"栏目图集,多张以;分割",
        ///         "node_desc":"栏目说明",
        ///         "node_remark":"栏目备注",
        ///         "meta_keywords":"栏目mate关键字",
        ///         "mate_description":"栏目mate说明",
        ///         "down_list":"栏目下载文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        ///     "tc":
        ///     {
        ///         "node_name":"栏目名称",
        ///         "out_link":"外链地址",
        ///         "node_tpl":"栏目页模版路径",
        ///         "list_tpl":"列表页模板路径",
        ///         "content_tpl":"详细页模板路径",
        ///         "node_pic":"栏目图片",
        ///         "node_atlas":"栏目图集,多张以;分割",
        ///         "node_desc":"栏目说明",
        ///         "node_remark":"栏目备注",
        ///         "meta_keywords":"栏目mate关键字",
        ///         "mate_description":"栏目mate说明",
        ///         "down_list":"栏目下载文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        ///     "en":
        ///     {
        ///         "node_name":"栏目名称",
        ///         "out_link":"外链地址",
        ///         "node_tpl":"栏目页模版路径",
        ///         "list_tpl":"列表页模板路径",
        ///         "content_tpl":"详细页模板路径",
        ///         "node_pic":"栏目图片",
        ///         "node_atlas":"栏目图集,多张以;分割",
        ///         "node_desc":"栏目说明",
        ///         "node_remark":"栏目备注",
        ///         "meta_keywords":"栏目mate关键字",
        ///         "mate_description":"栏目mate说明",
        ///         "down_list":"栏目下载文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        /// }
        ///```
        ///
        ///说明:
        /// - 详细字段请看首页
        /// - post 的 sc/tc/en 结构必须提交,就算是单语言也必须提交
        /// </param>
        /// <returns></returns>
        [HttpPost("save")]
        [Net.Mvc.Authorize.PermissionCheck]
        public async Task<RestfulData> SaveAsync(
            [FromServices] IDynamicPermissionCheck PermissionCheck
       , [FromServices] ISysTableidService tableid
       , [FromServices] ISiteModifyMarksService modify_mark
       , [FromServices] ISiteNodeMarksService node_mark
       , [FromServices] ISysResourceService sys_resource
       , [FromServices] ISysOperateService sys_operate
                   , [FromServices] ISiteContentService site_content_service
            , [FromServices] ISiteDocTypeService site_doc_type_service
            , [FromServices] ISiteDocService site_doc_service
       , [FromServices] Net.MyLucene.ISearchService searcher
       , JObject data)
        {
            long id = data["id"].MyToLong();
            long parent_node = data["parent_node"].MyToLong();

            string node_ext_model = data["node_ext_model"].MyToString();
            string ext_model = data["ext_model"].MyToString();
            string node_code = data["node_code"].MyToString();
            if (node_code == "")
            {
                return new RestfulData(-1, "栏目代号不能为空");
            }
            StatusEnum lucene_index = (StatusEnum)data["lucene_index"].MyToInt();
            StatusEnum is_show = (StatusEnum)data["is_show"].MyToInt();
            StatusEnum show_menu = (StatusEnum)data["show_menu"].MyToInt();
            StatusEnum show_path = (StatusEnum)data["show_path"].MyToInt();
            int nsort = data["nsort"].MyToInt();
            string pwd = data["pwd"].MyToString();
            StatusEnum need_review = (StatusEnum)data["need_review"].MyToInt();

            if (!data.ContainsKey("sc") || !data.ContainsKey("tc") || !data.ContainsKey("en"))
            {
                return new RestfulData(-1, "参数错误");
            }


            if (id > 0)
            {
                if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + id, "add,update"))
                {
                    return new RestfulData(-403, "认证不通过/Status401Unauthorized");
                }
            }
            else
            {
                if (parent_node > 0)
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + parent_node, "add"))
                    {
                        return new RestfulData(-403, "认证不通过/Status401Unauthorized");
                    }
                }
                else
                {
                    if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "site_node", "add"))
                    {
                        return new RestfulData(-403, "认证不通过/Status401Unauthorized");
                    }

                }
            }
            string sc_node_name = data["sc"]["node_name"].MyToString();
            string sc_out_link = data["sc"]["out_link"].MyToString();
            string sc_node_tpl = data["sc"]["node_tpl"].MyToString();
            string sc_list_tpl = data["sc"]["list_tpl"].MyToString();
            string sc_content_tpl = data["sc"]["content_tpl"].MyToString();
            string sc_node_pic = data["sc"]["node_pic"].MyToString();
            string sc_node_atlas = data["sc"]["node_atlas"].MyToString();
            string sc_node_desc = data["sc"]["node_desc"].MyToString();
            string sc_node_remark = data["sc"]["node_remark"].MyToString();
            string sc_meta_keywords = data["sc"]["meta_keywords"].MyToString();
            string sc_mate_description = data["sc"]["mate_description"].MyToString();
            string sc_down_list = data["sc"]["down_list"].MyToString();

            string sc_extend_fields_json = data["sc"]["ext_model"].MyToString();
            if (sc_extend_fields_json == "")
            {
                sc_extend_fields_json = "[]";
            }


            string tc_node_name = data["tc"]["node_name"].MyToString();
            string tc_out_link = data["tc"]["out_link"].MyToString();
            string tc_node_tpl = data["tc"]["node_tpl"].MyToString();
            string tc_list_tpl = data["tc"]["list_tpl"].MyToString();
            string tc_content_tpl = data["tc"]["content_tpl"].MyToString();
            string tc_node_pic = data["tc"]["node_pic"].MyToString();
            string tc_node_atlas = data["tc"]["node_atlas"].MyToString();
            string tc_node_desc = data["tc"]["node_desc"].MyToString();
            string tc_node_remark = data["tc"]["node_remark"].MyToString();
            string tc_meta_keywords = data["tc"]["meta_keywords"].MyToString();
            string tc_mate_description = data["tc"]["mate_description"].MyToString();
            string tc_down_list = data["tc"]["down_list"].MyToString();

            string tc_extend_fields_json = data["tc"]["ext_model"].MyToString();
            if (tc_extend_fields_json == "")
            {
                tc_extend_fields_json = "[]";
            }

            string en_node_name = data["en"]["node_name"].MyToString();
            string en_out_link = data["en"]["out_link"].MyToString();
            string en_node_tpl = data["en"]["node_tpl"].MyToString();
            string en_list_tpl = data["en"]["list_tpl"].MyToString();
            string en_content_tpl = data["en"]["content_tpl"].MyToString();
            string en_node_pic = data["en"]["node_pic"].MyToString();
            string en_node_atlas = data["en"]["node_atlas"].MyToString();
            string en_node_desc = data["en"]["node_desc"].MyToString();
            string en_node_remark = data["en"]["node_remark"].MyToString();
            string en_meta_keywords = data["en"]["meta_keywords"].MyToString();
            string en_mate_description = data["en"]["mate_description"].MyToString();
            string en_down_list = data["en"]["down_list"].MyToString();

            string en_extend_fields_json = data["en"]["ext_model"].MyToString();
            if (en_extend_fields_json == "")
            {
                en_extend_fields_json = "[]";
            }
            if (sc_node_name == "")
            {
                return new RestfulData(-1, "参数错误,简体栏目名称必填");
            }
            if (id <= 0)
            {

                int check_count = Service.Count(a => a.node_code.ToLower() == node_code.ToLower());
                if (check_count > 0)
                {
                    return new RestfulData(-1, "保存失败，已存在相同代号的栏目");

                }
                int count = nsort;
                if (nsort <= 0)
                {
                    count = Service.Count(a => a.parent_node == parent_node);
                    count = count + 1;
                }
                string parentLeavepath = "0";
                if (parent_node != 0)
                {
                    var parent = Service.Get("", a => a.id == parent_node);

                    if (parent != null)
                    {
                        parentLeavepath = parent.leave_path + "/" + parent.id;
                    }
                }
                int lang_flag = 1;
                long node_id = tableid.getNewTableId<site_node>();
                if (node_id <= 0)
                {
                    return new RestfulData(-1, "参数错误");
                }
                site_node result = new site_node();
                site_node sc_result = new site_node();

                sc_result = Service.Add(new site_node()
                {
                    id = node_id,
                    model_type = "",
                    node_type = NodeTypeEnum.ContentNode,
                    node_code = node_code,
                    parent_node = parent_node,
                    node_name = sc_node_name,
                    node_tpl = sc_node_tpl,
                    list_tpl = sc_list_tpl,
                    content_tpl = sc_content_tpl,
                    node_pic = sc_node_pic,
                    node_desc = sc_node_desc,
                    node_remark = sc_node_remark,
                    meta_keywords = sc_meta_keywords,
                    show_menu = show_menu,
                    show_path = show_path,
                    need_review = need_review,
                    nsort = count,
                    leave_path = parentLeavepath,
                    ext_model = ext_model,
                    node_ext_model = node_ext_model,
                    extend_fields_json = sc_extend_fields_json,
                    mate_description = sc_mate_description,
                    out_link = sc_out_link,
                    node_atlas = sc_node_atlas,
                    pwd = pwd,
                    lang = "sc",
                    lang_flag = 1,
                    review_status = ReViewStatusEnum.UnReview,
                    down_list = sc_down_list,
                    is_show = is_show,
                    lucene_index = lucene_index,
                    del_flag = DeleteEnum.UnDelete,
                    create_time = DateTime.Now
                });

                if (sc_result.id > 0)
                {
                    var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", sc_result.node_code);
                    if (up_lucene_model != null)
                    {
                        searcher.InsertNodeIndex(up_lucene_model.node_code, sc_result);
                    }
                }

                if (tc_node_name != "")
                {
                    result = Service.Add(new site_node()
                    {
                        id = node_id,
                        model_type = "",
                        node_type = NodeTypeEnum.ContentNode,
                        node_code = node_code,
                        parent_node = parent_node,
                        node_name = tc_node_name,
                        node_tpl = tc_node_tpl,
                        list_tpl = tc_list_tpl,
                        content_tpl = tc_content_tpl,
                        node_pic = tc_node_pic,
                        node_desc = tc_node_desc,
                        node_remark = tc_node_remark,
                        meta_keywords = tc_meta_keywords,
                        show_menu = show_menu,
                        show_path = show_path,
                        need_review = need_review,
                        nsort = count,
                        leave_path = parentLeavepath,
                        ext_model = ext_model,
                        node_ext_model = node_ext_model,
                        extend_fields_json = tc_extend_fields_json,
                        mate_description = tc_mate_description,
                        out_link = tc_out_link,
                        node_atlas = tc_node_atlas,
                        pwd = pwd,
                        lang = "tc",
                        lang_flag = 1,
                        review_status = ReViewStatusEnum.UnReview,
                        down_list = tc_down_list,
                        is_show = is_show,
                        lucene_index = lucene_index,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0)
                    {
                        var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertNodeIndex(up_lucene_model.node_code, result);

                        }
                    }
                }

                if (en_node_name != "")
                {
                    result = Service.Add(new site_node()
                    {
                        id = node_id,
                        model_type = "",
                        node_type = NodeTypeEnum.ContentNode,
                        node_code = node_code,
                        parent_node = parent_node,
                        node_name = en_node_name,
                        node_tpl = en_node_tpl,
                        list_tpl = en_list_tpl,
                        content_tpl = en_content_tpl,
                        node_pic = en_node_pic,
                        node_desc = en_node_desc,
                        node_remark = en_node_remark,
                        meta_keywords = en_meta_keywords,
                        show_menu = show_menu,
                        show_path = show_path,
                        need_review = need_review,
                        nsort = count,
                        leave_path = parentLeavepath,
                        ext_model = ext_model,
                        node_ext_model = node_ext_model,
                        extend_fields_json = en_extend_fields_json,
                        mate_description = en_mate_description,
                        out_link = en_out_link,
                        node_atlas = en_node_atlas,
                        pwd = pwd,
                        lang = "en",
                        lang_flag = 1,
                        review_status = ReViewStatusEnum.UnReview,
                        down_list = en_down_list,
                        is_show = is_show,
                        lucene_index = lucene_index,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0)
                    {
                        var up_lucene_model = Service.GetUpLeaveLuceneNode("en", result.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertNodeIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (sc_result.id > 0)
                {

                    #region 栏目相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 5;
                        string module_code = "site_node";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_node_g_{sc_result.id}",
                            res_name = $"栏目管理--{sc_result.node_name}({sc_result.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion

                    #region 内容相关权限
                    {
                        //一级分类不用创建==parent=0:
                        long module_id = 6;
                        string module_code = "site_content";


                        sys_resource_entity resource = new sys_resource_entity()
                        {
                            p_module = module_id,
                            url = $"",
                            uri = "",
                            enable_status = StatusEnum.Legal,
                            created_time = DateTime.Now,
                            creator_id = "0",
                            creator_name = "",
                            delete_status = DeleteEnum.UnDelete,
                            description = "",
                            last_modified_time = DateTime.Now,
                            last_modifier_id = "0",
                            last_modifier_name = "",
                            n_sort = 1,
                            res_code = $"cms_content_g_{sc_result.id}",
                            res_name = $"内容管理--{sc_result.node_name}({sc_result.node_code})",
                            status = StatusEnum.Legal
                        };
                        resource = sys_resource.Insert(resource);
                        if (resource.id > 0)
                        {
                            List<sys_operate_entity> operates = new List<sys_operate_entity>();
                            var operate_codes = new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                            foreach (var item in operate_codes)
                            {
                                var operate_model = new sys_operate_entity()
                                {
                                    is_menu = StatusEnum.Legal,
                                    creator_id = "0",
                                    creator_name = "",
                                    delete_status = DeleteEnum.UnDelete,
                                    description = "",
                                    last_modified_time = DateTime.Now,
                                    last_modifier_id = "0",
                                    last_modifier_name = "",
                                    n_sort = 1,
                                    operate_code = item.Key,
                                    operate_name = item.Value,
                                    p_module = module_id,
                                    api_url = $"",
                                    created_time = DateTime.Now,
                                    enable_status = StatusEnum.Legal,
                                    p_module_code = module_code,
                                    p_res = resource.id,
                                    p_res_code = resource.res_code,
                                    status = StatusEnum.Legal
                                };
                                operates.Add(operate_model);
                            }
                            //批量插入权限
                            sys_operate.BulkInsert(operates);
                        }
                    }
                    #endregion
                    if (parent_node == 0)
                    {
                        AddLog("site_node", Utils.LogsOperateTypeEnum.Add, $"创建跟节点栏目:{sc_result.node_name}");
                    }
                    else
                    {
                        AddLog($"cms_node_g_{parent_node}", Utils.LogsOperateTypeEnum.Add, $"创建栏目:{sc_result.node_name}");
                    }

                    return new RestfulData(0, "新增成功");
                }
                else
                {
                    return new RestfulData(-1, "新增失败");
                }
            }
            else {

                int check_count = Service.Count(a => a.node_code.ToLower() == node_code.ToLower() && a.id != id);
                if (check_count > 0)
                {
                    return new RestfulData(-1, "修改失败，已存在相同代号的栏目");
                }
                if (nsort <= 0)
                {
                    nsort = Service.Count(a => a.parent_node == parent_node);
                    nsort = nsort + 1;
                }
                int lang_flag = 1;
                var result = 0;
                var modify = modify_mark.Insert(new site_modify_marks()
                {
                    chr_type = 1,
                    create_id = httpContextAccessor.Current.CurrentUser.id,
                    operate_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                    create_time = DateTime.Now,
                    create_name = httpContextAccessor.Current.CurrentUser.alias_name
                });
                var node_model = Service.Get("row_key asc", a => a.id == id);


             
                    var sc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "sc");
                    if (sc_model != null)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "sc", a => new site_node()
                        {
                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = sc_node_name,
                            node_tpl = sc_node_tpl,
                            list_tpl = sc_list_tpl,
                            content_tpl = sc_content_tpl,
                            node_pic = sc_node_pic,
                            node_desc = sc_node_desc,
                            node_remark = sc_node_remark,
                            meta_keywords = sc_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = sc_extend_fields_json,
                            mate_description = sc_mate_description,
                            out_link = sc_out_link,
                            node_atlas = sc_node_atlas,
                            pwd = pwd,
                            lang_flag = 1,
                            nsort=nsort,

                            down_list=sc_down_list,
                            is_show = is_show,
                            lucene_index = lucene_index
                        });
                        sc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "sc");
                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, node_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                model_type = sc_model.model_type,
                                node_type = sc_model.node_type,
                                node_code = sc_model.node_code,
                                parent_node = sc_model.parent_node,
                                node_name = sc_model.node_name,
                                node_tpl = sc_model.node_tpl,
                                list_tpl = sc_model.list_tpl,
                                content_tpl = sc_model.content_tpl,
                                node_pic = sc_model.node_pic,
                                node_desc = sc_model.node_desc,
                                node_remark = sc_model.node_remark,
                                meta_keywords = sc_model.meta_keywords,
                                show_menu = sc_model.show_menu,
                                show_path = sc_model.show_path,
                                need_review = sc_model.need_review,
                                ext_model = sc_model.ext_model,
                                extend_fields_json = sc_model.extend_fields_json,
                                mate_description = sc_model.mate_description,
                                out_link = sc_model.out_link,
                                node_atlas = sc_model.node_atlas,
                                pwd = sc_model.model_type,
                                is_show = sc_model.is_show,
                                down_list = sc_model.down_list,
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = sc_model.model_type,
                                node_type = sc_model.node_type,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name = sc_node_name,
                                node_tpl = sc_node_tpl,
                                list_tpl = sc_list_tpl,
                                content_tpl = sc_content_tpl,
                                node_pic = sc_node_pic,
                                node_desc = sc_node_desc,
                                node_remark = sc_node_remark,
                                meta_keywords = sc_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,
                                extend_fields_json = sc_extend_fields_json,
                                mate_description = sc_mate_description,
                                out_link = sc_out_link,
                                node_atlas = sc_node_atlas,
                                pwd = pwd,
                                lang_flag = 1,
                                down_list = sc_down_list,
                                is_show = is_show
                            });
                        }
                    }
                    else
                    {
                        string parentLeavepath = "0";
                        if (parent_node != 0)
                        {
                            var parent = Service.Get("", a => a.id == parent_node);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                        var mod_sc_model  = Service.Add(new site_node()
                        {
                            id = id,
                            model_type = "",
                            node_type = NodeTypeEnum.ContentNode,
                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = sc_node_name,
                            node_tpl = sc_node_tpl,
                            list_tpl = sc_list_tpl,
                            content_tpl = sc_content_tpl,
                            node_pic = sc_node_pic,
                            node_desc = sc_node_desc,
                            node_remark = sc_node_remark,
                            meta_keywords = sc_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = sc_extend_fields_json,
                            mate_description = sc_mate_description,
                            out_link = sc_out_link,
                            node_atlas = sc_node_atlas,
                            pwd = pwd,
                            lang = "sc",
                            lang_flag = 1,
                            review_status = ReViewStatusEnum.UnReview,
                            down_list = sc_down_list,
                            is_show = is_show,
                            lucene_index = lucene_index,
                            del_flag = DeleteEnum.UnDelete,
                            create_time = DateTime.Now
                        });
                    if (mod_sc_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("sc", mod_sc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_sc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "sc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = "",
                                node_type = NodeTypeEnum.ContentNode,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name = sc_node_name,
                                node_tpl = sc_node_tpl,
                                list_tpl = sc_list_tpl,
                                content_tpl = sc_content_tpl,
                                node_pic = sc_node_pic,
                                node_desc = sc_node_desc,
                                node_remark = sc_node_remark,
                                meta_keywords = sc_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,
                                extend_fields_json = sc_extend_fields_json,
                                mate_description = sc_mate_description,
                                out_link = sc_out_link,
                                node_atlas = sc_node_atlas,
                                pwd = pwd,
                                lang_flag = 1,
                                down_list = sc_down_list,
                                is_show = is_show
                            });
                        }
                    }
                
                //繁体修改该
                if (tc_node_name != "")
                {
                    var tc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "tc");
                    if (tc_model != null)
                    {
                        #region 繁体修改
                        result = Service.Modify(a => a.id == id && a.lang == "tc", a => new site_node()
                        {


                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = tc_node_name,
                            node_tpl = tc_node_tpl,
                            list_tpl = tc_list_tpl,
                            content_tpl = tc_content_tpl,
                            node_pic = tc_node_pic,
                            node_desc = tc_node_desc,
                            node_remark = tc_node_remark,
                            meta_keywords = tc_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = tc_extend_fields_json,
                            mate_description = tc_mate_description,
                            out_link = tc_out_link,
                            node_atlas = tc_node_atlas,
                            pwd = pwd,
                            nsort = nsort,
                            down_list =tc_down_list,
                            lang_flag = 1,
                            is_show = is_show,
                            lucene_index = lucene_index
                        });
                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, tc_model);
                                if (lang_flag == 2)
                                {
                                    tc_model.lang = "sc";
                                    searcher.UpdateNodeIndex(up_lucene_model.node_code, tc_model);
                                }
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                lang_flag = lang_flag,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                down_list = "",
                                model_type = tc_model.model_type,
                                node_type = tc_model.node_type,
                                node_code = tc_model.node_code,
                                parent_node = tc_model.parent_node,
                                node_name = tc_model.node_name,
                                node_tpl = tc_model.node_tpl,
                                list_tpl = tc_model.list_tpl,
                                content_tpl = tc_model.content_tpl,
                                node_pic = tc_model.node_pic,
                                node_desc = tc_model.node_desc,
                                node_remark = tc_model.node_remark,
                                meta_keywords = tc_model.meta_keywords,
                                show_menu = tc_model.show_menu,
                                show_path = tc_model.show_path,
                                need_review = tc_model.need_review,
                                ext_model = tc_model.ext_model,
                                extend_fields_json = tc_model.extend_fields_json,
                                mate_description = tc_model.mate_description,
                                out_link = tc_model.out_link,
                                node_atlas = tc_model.node_atlas,
                                pwd = tc_model.model_type,
                                is_show = tc_model.is_show
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = "",
                                node_type = NodeTypeEnum.ContentNode,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name = tc_node_name,
                                node_tpl = tc_node_tpl,
                                list_tpl = tc_list_tpl,
                                content_tpl = tc_content_tpl,
                                node_pic = tc_node_pic,
                                node_desc = tc_node_desc,
                                node_remark = tc_node_remark,
                                meta_keywords = tc_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,
                                extend_fields_json = tc_extend_fields_json,
                                mate_description = tc_mate_description,
                                out_link = tc_out_link,
                                node_atlas = tc_node_atlas,
                                pwd = pwd,
                                lang_flag = lang_flag,
                                down_list = tc_down_list,
                                is_show = is_show
                            });
                        }
                        #endregion
                    }
                    else
                    {
                        #region 新插入
                        //重新插入
                        string parentLeavepath = "0";
                        if (parent_node != 0)
                        {
                            var parent = Service.Get("", a => a.id == parent_node);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                       var mod_tc_model= Service.Add(new site_node()
                        {
                            id = id,
                            model_type = "",
                            node_type = NodeTypeEnum.ContentNode,
                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = tc_node_name,
                            node_tpl = tc_node_tpl,
                            list_tpl = tc_list_tpl,
                            content_tpl = tc_content_tpl,
                            node_pic = tc_node_pic,
                            node_desc = tc_node_desc,
                            node_remark = tc_node_remark,
                            meta_keywords = tc_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = tc_extend_fields_json,
                            mate_description = tc_mate_description,
                            out_link = tc_out_link,
                            node_atlas = tc_node_atlas,
                            pwd = pwd,
                            lang = "tc",
                            lang_flag = 1,
                            review_status = ReViewStatusEnum.UnReview,
                            down_list = tc_down_list,
                            is_show = is_show,
                            lucene_index = lucene_index,
                            del_flag = DeleteEnum.UnDelete,
                            create_time = DateTime.Now
                        });
               

                        if (mod_tc_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("tc", mod_tc_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_tc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = "",
                                node_type = NodeTypeEnum.ContentNode,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name =tc_node_name,
                                node_tpl = tc_node_tpl,
                                list_tpl = tc_list_tpl,
                                content_tpl = tc_content_tpl,
                                node_pic = tc_node_pic,
                                node_desc = tc_node_desc,
                                node_remark = tc_node_remark,
                                meta_keywords = tc_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,

                                extend_fields_json = tc_extend_fields_json,
                                mate_description = tc_mate_description,
                                out_link = tc_out_link,
                                node_atlas = tc_node_atlas,
                                pwd = pwd,
                                lang_flag = 1,
                                down_list = tc_down_list,
                                is_show = is_show
                            });
                        }

                        #endregion
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "tc");
                }


                if (en_node_name != "")
                {
                    var en_model = Service.Get("row_key asc", a => a.id == id && a.lang == "en");
                    if (en_model != null)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "en", a => new site_node()
                        {

                            model_type = "",
                            node_type = NodeTypeEnum.ContentNode,
                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = en_node_name,
                            node_tpl = en_node_tpl,
                            list_tpl = en_list_tpl,
                            content_tpl = en_content_tpl,
                            node_pic = en_node_pic,
                            node_desc = en_node_desc,
                            node_remark = en_node_remark,
                            meta_keywords = en_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = en_extend_fields_json,
                            mate_description = en_mate_description,
                            out_link = en_out_link,
                            node_atlas = en_node_atlas,
                            pwd = pwd,
                            nsort = nsort,
                            down_list =en_down_list,
                            lang_flag = 1,
                            is_show = is_show
                        });


                        en_model = Service.Get("row_key asc", a => a.id == id && a.lang == "en");

                        if (result > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("en", en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateNodeIndex(up_lucene_model.node_code, en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,
                                down_list = "",
                                model_type = en_model.model_type,
                                node_type = en_model.node_type,
                                node_code = en_model.node_code,
                                parent_node = en_model.parent_node,
                                node_name = en_model.node_name,
                                node_tpl = en_model.node_tpl,
                                list_tpl = en_model.list_tpl,
                                content_tpl = en_model.content_tpl,
                                node_pic = en_model.node_pic,
                                node_desc = en_model.node_desc,
                                node_remark = en_model.node_remark,
                                meta_keywords = en_model.meta_keywords,
                                show_menu = en_model.show_menu,
                                show_path = en_model.show_path,
                                need_review = en_model.need_review,
                                ext_model = en_model.ext_model,
                                extend_fields_json = en_model.extend_fields_json,
                                mate_description = en_model.mate_description,
                                out_link = en_model.out_link,
                                node_atlas = en_model.node_atlas,
                                pwd = en_model.model_type,
                                is_show = en_model.is_show
                            });
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = "",
                                node_type = NodeTypeEnum.ContentNode,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name = en_node_name,
                                node_tpl = en_node_tpl,
                                list_tpl = en_list_tpl,
                                content_tpl = en_content_tpl,
                                node_pic = en_node_pic,
                                node_desc = en_node_desc,
                                node_remark = en_node_remark,
                                meta_keywords = en_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,

                                extend_fields_json = en_extend_fields_json,
                                mate_description = en_mate_description,
                                out_link = en_out_link,
                                node_atlas = en_node_atlas,
                                pwd = pwd,
            
                                lang_flag = 1,
                                down_list = en_down_list,
                                is_show = is_show
                            });
                        }
                    }
                    else
                    {
                        string parentLeavepath = "0";
                        if (parent_node != 0)
                        {
                            var parent = Service.Get("", a => a.id == parent_node);
                            if (parent != null)
                            {
                                parentLeavepath = parent.leave_path + "/" + parent.id;
                            }
                        }
                        var mod_en_model = Service.Add(new site_node()
                        {
                            id = id,
                            model_type = "",
                            node_type = NodeTypeEnum.ContentNode,
                            node_code = node_code,
                            parent_node = parent_node,
                            node_name = en_node_name,
                            node_tpl = en_node_tpl,
                            list_tpl = en_list_tpl,
                            content_tpl = en_content_tpl,
                            node_pic = en_node_pic,
                            node_desc = en_node_desc,
                            node_remark = en_node_remark,
                            meta_keywords = en_meta_keywords,
                            show_menu = show_menu,
                            show_path = show_path,
                            need_review = need_review,
                            nsort = nsort,
                            leave_path = parentLeavepath,
                            ext_model = ext_model,
                            node_ext_model = node_ext_model,
                            extend_fields_json = en_extend_fields_json,
                            mate_description = en_mate_description,
                            out_link = en_out_link,
                            node_atlas = en_node_atlas,
                            pwd = pwd,
                            lang = "en",
                            lang_flag = 1,
                            review_status = ReViewStatusEnum.UnReview,
                            down_list = en_down_list,
                            is_show = is_show,
                            lucene_index = lucene_index,
                            del_flag = DeleteEnum.UnDelete,
                            create_time = DateTime.Now
                        });
                        if (mod_en_model.id > 0)
                        {
                            var up_lucene_model = Service.GetUpLeaveLuceneNode("en", mod_en_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertNodeIndex(up_lucene_model.node_code, mod_en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_node_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,
                                model_type = "",
                                node_type = NodeTypeEnum.ContentNode,
                                node_code = node_code,
                                parent_node = parent_node,
                                node_name = en_node_name,
                                node_tpl = en_node_tpl,
                                list_tpl = en_list_tpl,
                                content_tpl = en_content_tpl,
                                node_pic = en_node_pic,
                                node_desc = en_node_desc,
                                node_remark = en_node_remark,
                                meta_keywords = en_meta_keywords,
                                show_menu = show_menu,
                                show_path = show_path,
                                need_review = need_review,
                                ext_model = ext_model,

                                extend_fields_json = en_extend_fields_json,
                                mate_description = en_mate_description,
                                out_link = en_out_link,
                                node_atlas = en_node_atlas,
                                pwd = pwd,
                                lang_flag = 1,
                                down_list = en_down_list,
                                is_show = is_show
                            });
                        }
                    }
                }
                if (result > 0)
                {
                    #region 修改栏目关联内容修改以及关联栏目文件修改
                    site_content_service.Modify(a => a.node_id == id, a => new Net.Models.site_content() { node_code = node_code });
                    site_doc_type_service.Modify(a => a.node_id == id, a => new Net.Models.site_doc_type() { node_code = node_code });
                    site_doc_service.Modify(a => a.node_id == id, a => new Net.Models.site_doc() { node_code = node_code });
                    #endregion

                    string res_code = $"cms_node_g_{id}";
                    var opeart = sys_resource.GetByOne(a => a.p_module == 5 && a.res_code == res_code);
                    string append_msg = "";
                    if (opeart == null)
                    {
                        #region 栏目相关权限
                        {
                            //一级分类不用创建==parent=0:
                            long module_id = 5;
                            string module_code = "site_node";


                            sys_resource_entity resource = new sys_resource_entity()
                            {
                                p_module = module_id,
                                url = $"",
                                uri = "",
                                enable_status = StatusEnum.Legal,
                                created_time = DateTime.Now,
                                creator_id = "0",
                                creator_name = "",
                                delete_status = DeleteEnum.UnDelete,
                                description = "",
                                last_modified_time = DateTime.Now,
                                last_modifier_id = "0",
                                last_modifier_name = "",
                                n_sort = 1,
                                res_code = $"cms_node_g_{id}",
                                res_name = $"栏目管理--{sc_node_name}({node_code})",
                                status = StatusEnum.Legal
                            };
                            resource = sys_resource.Insert(resource);
                            if (resource.id > 0)
                            {
                                List<sys_operate_entity> operates = new List<sys_operate_entity>();
                                var operate_codes = new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
                            };
                                foreach (var item in operate_codes)
                                {
                                    var operate_model = new sys_operate_entity()
                                    {
                                        is_menu = StatusEnum.Legal,
                                        creator_id = "0",
                                        creator_name = "",
                                        delete_status = DeleteEnum.UnDelete,
                                        description = "",
                                        last_modified_time = DateTime.Now,
                                        last_modifier_id = "0",
                                        last_modifier_name = "",
                                        n_sort = 1,
                                        operate_code = item.Key,
                                        operate_name = item.Value,
                                        p_module = module_id,
                                        api_url = $"",
                                        created_time = DateTime.Now,
                                        enable_status = StatusEnum.Legal,
                                        p_module_code = module_code,
                                        p_res = resource.id,
                                        p_res_code = resource.res_code,
                                        status = StatusEnum.Legal
                                    };
                                    operates.Add(operate_model);
                                }
                                //批量插入权限
                                sys_operate.BulkInsert(operates);
                            }
                        }
                        #endregion
                        #region 内容相关权限
                        {
                            //一级分类不用创建==parent=0:
                            long module_id = 6;
                            string module_code = "site_content";


                            sys_resource_entity resource = new sys_resource_entity()
                            {
                                p_module = module_id,
                                url = $"",
                                uri = "",
                                enable_status = StatusEnum.Legal,
                                created_time = DateTime.Now,
                                creator_id = "0",
                                creator_name = "",
                                delete_status = DeleteEnum.UnDelete,
                                description = "",
                                last_modified_time = DateTime.Now,
                                last_modifier_id = "0",
                                last_modifier_name = "",
                                n_sort = 1,
                                res_code = $"cms_content_g_{id}",
                                res_name = $"内容管理--{sc_node_name}({node_code})",
                                status = StatusEnum.Legal
                            };
                            resource = sys_resource.Insert(resource);
                            if (resource.id > 0)
                            {
                                List<sys_operate_entity> operates = new List<sys_operate_entity>();
                                var operate_codes = new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                                foreach (var item in operate_codes)
                                {
                                    var operate_model = new sys_operate_entity()
                                    {
                                        is_menu = StatusEnum.Legal,
                                        creator_id = "0",
                                        creator_name = "",
                                        delete_status = DeleteEnum.UnDelete,
                                        description = "",
                                        last_modified_time = DateTime.Now,
                                        last_modifier_id = "0",
                                        last_modifier_name = "",
                                        n_sort = 1,
                                        operate_code = item.Key,
                                        operate_name = item.Value,
                                        p_module = module_id,
                                        api_url = $"",
                                        created_time = DateTime.Now,
                                        enable_status = StatusEnum.Legal,
                                        p_module_code = module_code,
                                        p_res = resource.id,
                                        p_res_code = resource.res_code,
                                        status = StatusEnum.Legal
                                    };
                                    operates.Add(operate_model);
                                }
                                //批量插入权限
                                sys_operate.BulkInsert(operates);
                            }
                        }
                        #endregion
                        append_msg = ",并添加该栏目的权限,请权限用户进行授权!";
                    }
                    if (parent_node == 0)
                    {
                        AddLog("site_node", Utils.LogsOperateTypeEnum.Update, $"修改根节点栏目:{sc_node_name}");
                    }
                    else
                    {
                        AddLog($"cms_node_g_{parent_node}", Utils.LogsOperateTypeEnum.Update, $"修改栏目:{sc_node_name}");
                    }


                    return new RestfulData(0, $"修改成功{append_msg}");
                }
                else
                {
                    return new RestfulData(-1, $"修改失败");
                }
            }
        }

        /// <summary>
        /// 栏目排序
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "parent_node":"父级栏目ID",
        ///     "sort_data":[1,24,4]
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 传入parent_node为父级栏目ID需要判断权限,sort_data 为排序的id集合
        /// </param>
        /// <returns></returns>
        [HttpPost("sort")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData Sort(JObject data)
        {
            long parent_node = data["parent_node"].MyToLong();

            //if (parent_node<=0)
            //{
            //    return new RestfulData(-1, "所属父级ID必传,参数错误");
            //}

            
            var list = JsonHelper.ToObject<List<long>>(data["sort_data"].ToString());
            int count = list.Count();
            foreach (var item in list)
            {
                Service.Modify(a => a.id == item, a => new site_node()
                {
                    nsort = count
                });
                count--;


            }
            if (parent_node <= 0)
            {
                AddLog($"cms_node_g_{parent_node}", Utils.LogsOperateTypeEnum.Update, $"子栏目排序");
            }
            else {
                AddLog($"cms_node", Utils.LogsOperateTypeEnum.Update, $"顶级栏目排序");
            }

            return new RestfulData() { code = 0, msg = "ok" };
        }

        /// <summary>
        /// 删除栏目
        /// </summary>
        /// <param name="PermissionCheck"></param>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"删除的栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 此处删除为假删除,只是修改del_flag=1
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("delete")]
        public async Task<RestfulData> DeleteAsync([FromServices] IDynamicPermissionCheck PermissionCheck,JObject data)
        {


            long Id = data["id"].MyToLong();


            if (Id <= 0)
            {
                return new RestfulData (){ code = -1, msg = "参数错误" };
            }



            var check = Service.GetList("", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.parent_node==Id
            });
            if (check.Count() > 0)
            {
                return new RestfulData (){ code = -1, msg = "存在下级栏目无法删除" };
            }
            var model = Service.GetByOne(a=> a.id == Id&&a.lang=="sc"&&a.del_flag==DeleteEnum.UnDelete);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法删除" };
            }
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_node", "cms_node_g_" + model.id, "delete"))
            {
                return new RestfulData (){ code = -403, msg = "forbidden" };
            }
            long parent_node = model.parent_node;
            int flag = Service.Modify(a => a.id == Id, a => new site_node() { del_flag = DeleteEnum.Delete });
            if (flag > 0)
            {

                if (parent_node == 0)
                {
                    AddLog("site_node", Utils.LogsOperateTypeEnum.Delete, $"删除跟节点栏目:{model.node_name}");
                }
                else
                {
                    AddLog($"cms_node_g_{parent_node}", Utils.LogsOperateTypeEnum.Delete, $"删除栏目:{model.node_name}");
                }
                return new RestfulData (){ code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }
        }


        /// <summary>
        /// 设置栏目关联内容的字段不显示以及必填项
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "node_code":"设置栏目的栏目代号",
        ///     "field_list":
        ///     [
        ///         { "value": "is_node", "title": "是否栏目显示" },
        ///         { "value": "is_home", "title": "是否主页显示" },
        ///         { "value": "is_top", "title": "是否置顶" },
        ///         { "value": "is_show", "title": "是否显示" },
        ///         { "value": "n_sort", "title": "排序" },
        ///         { "value": "view_count", "title": "点击数" },
        ///         { "value": "publish_date", "title": "发布日期" },
        ///         { "value": "link", "title": "链接" },
        ///         { "value": "summary", "title": "文章简介" },
        ///         { "value": "chr_content", "title": "文章内容" },
        ///         { "value": "author", "title": "作者" },
        ///         { "value": "source", "title": "来源" },
        ///         { "value": "tag", "title": "标签" },
        ///         { "value": "cover", "title": "封面图片" },
        ///         { "value": "atlas", "title": "图集" },
        ///         { "value": "down_file", "title": "文件" }
        ///    ],
        ///    "field_required":
        ///    [
        ///        { "value": "publish_date", "title": "发布日期" },
        ///        { "value": "link", "title": "链接" },
        ///        { "value": "summary", "title": "文章简介" },
        ///        { "value": "chr_content", "title": "文章內容" },
        ///        { "value": "author", "title": "作者" },
        ///        { "value": "source", "title": "来源" },
        ///        { "value": "cover", "title": "封面图片" },
        ///        { "value": "atlas", "title": "图集" },
        ///        { "value": "down_file", "title": "文件" },
        ///    ]
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 在内容管理添加或修改内容时,会有接口获取该内容关联栏目的字段不显示配置以及配置设置
        /// </param>
        /// <returns></returns>
        [HttpPost("SaveField")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData SaveFieldSet([FromServices] ISiteNodeFieldSetService site_node_fidld, [FromServices] ISiteNodeFieldRequiredSetService site_node_fidld_req, JObject data)
        {

            string node_code = data["node_code"].MyToString();
            var field_list = (data["field_list"] as JArray);
            var field_required = (data["field_required"] as JArray); 
            site_node_fidld.Delete(a => a.node_code.ToLower() == node_code.ToLower());

            if (field_list != null)
            {
                List<site_node_field_set> list = new List<site_node_field_set>();

                foreach (var item in field_list)
                {
                    list.Add(new site_node_field_set()
                    {
                        node_code = node_code,
                        field_code = item["value"].MyToString(),
                        field_name = item["title"].MyToString()
                    });
                }
                site_node_fidld.BulkInsert(list);

            }

            site_node_fidld_req.Delete(a => a.node_code.ToLower() == node_code.ToLower());

            if (field_required != null)
            {
                List<site_node_field_set_required> list = new List<site_node_field_set_required>();

                foreach (var item in field_required)
                {
                    list.Add(new site_node_field_set_required()
                    {
                        node_code = node_code,
                        field_code = item["value"].MyToString(),
                        field_name = item["title"].MyToString()
                    });
                }
                site_node_fidld_req.BulkInsert(list);

            }
            return new RestfulData() { code = 0, msg = "ok" };
        }


        /// <summary>
        /// 获取栏目配置的字段显示以及字段必填配置
        /// </summary>
        /// <param name="site_node_fidld"></param>
        /// <param name="site_node_fidld_req"></param>
        /// <param name="node_id">
        /// 栏目ID
        ///说明:
        ///
        /// - 在内容管理添加或修改时根据关联的栏目ID获取该栏目的字段隐藏配置以及字段必填配置
        /// - 在多语言时sc 则根据required_field验证字段必填,其他语种例如:tc/en 如果title有填写则根据required_field验证字段必填
        /// - 单语言时则只根据required_field验证sc字段
        /// </param>
        /// <returns></returns>
        [HttpGet("NodeFieldSet/{node_id}")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData GetFieldSet([FromServices] ISiteNodeFieldSetService site_node_fidld, [FromServices] ISiteNodeFieldRequiredSetService site_node_fidld_req, long node_id)
        {
            if (node_id <= 0)
            {
                return new RestfulData() { code = -1, msg = "必需传入栏目ID" };
            }
            var model = Service.GetByOne(a => a.id == node_id && a.lang == "sc");
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "栏目不存在" };
            }
            var hidden_field = site_node_fidld.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<site_node_field_set, bool>>>() { a => a.node_code == model.node_code });
            var required_field = site_node_fidld_req.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<site_node_field_set_required, bool>>>() { a => a.node_code == model.node_code });


            return new RestfulData<object>() { code = 0, msg = "ok",data=new { hidden_field , required_field } };
        }

        /// <summary>
        /// 获取栏目树形结构
        /// </summary>
        /// <param name="lang">语种:sc,tc,en,默认不传为sc</param>
        /// <returns></returns>
        [HttpGet("data/list/{lang}")]
        [HttpGet("data/list")]
        [Net.Mvc.Authorize.PermissionCheck]
        public async Task<RestfulData> GetNodeListAsync(string lang="sc")
        {

            var result = await Service.GetTreeDataAsync("node", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);

            
            return new RestfulData<object>(0, "ok", result[0].children);

        }


        /// <summary>
        /// 修改栏目显示状态
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"栏目ID",
        /// "is_show":"1显示,0不显示"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("change_show")]
        public RestfulData changeAsync(JObject data)
        {
            int status = data["is_show"].MyToInt();
            long id = data["id"].MyToLong();
            
            if (status < 0 || id <= 0)
            {
                return new RestfulData (){ code = -1, msg = "参数错误" };
            }
           
            if (status != 0 && status != 1)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }

            var model = Service.GetByOne(a => a.id == id&&a.lang=="sc");
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据错误" };
            }
            int flag = Service.Modify(a => a.id == id, a => new site_node()
            {
                is_show = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_node_g_{model.id}", Utils.LogsOperateTypeEnum.Update, $"修改网站栏目显示状态:{model.node_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return new RestfulData(){ code = 0, msg = "操作成功" };
            }
            else
            {
                return new RestfulData (){ code = -1, msg = "操作失败" };
            }
        }



        /// <summary>
        /// 修改栏目全文索引状态
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"栏目ID",
        /// "lucene_index":"1索引,0不索引"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("change_lucene")]
        public RestfulData Lucene(JObject data)
        {
            int status = data["lucene_index"].MyToInt();
            long id = data["id"].MyToLong();

            if (status < 0 || id <= 0)
            {
                return new() { code = -1, msg = "参数错误" };
            }
            if (status != 0 && status != 1)
            {
                return new () { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == id && a.lang == "sc");
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据错误" };
            }
            int flag = Service.Modify(a => a.id == id, a => new site_node()
            {
                lucene_index = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_node_g_{id}", Utils.LogsOperateTypeEnum.Update, $"修改网站栏目索引状态:{model.node_name}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return new (){ code = 0, msg = "操作成功" };
            }
            else
            {
                return new() { code = -1, msg = "操作失败" };
            }
        }




    }
}
