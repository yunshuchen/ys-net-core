﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("栏目审核管理")]
    public class SiteNodesReviewController : ApiBasicController<site_node, ISiteNodeService>
    {

        public SiteNodesReviewController(ISiteNodeService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }


        /// <summary>
        /// 获取审核栏目列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "page_size":"页大小",
        ///     "page_index","页码",
        ///     "condition":{"node_name":"栏目名称,模糊检索,可不传","lang":"语种:sc,tc,en","review_status":"0待审核,2审核不通过,必传默认为0"}
        /// }
        ///```
        ///
        /// 说明
        /// - 在待审核列表时review_status传入0,审核不通过的列表里面传入2
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_node_review", resource_code = "site_node_review")]
        [HttpPost("list")]
        public RestfulData getNodeList(JObject data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string lang = "sc";
            string node_name = "";
            int review_status = 0;
            if (data.ContainsKey("condition"))
            {
                node_name = data["condition"]["node_name"].MyToString();
                lang = data["condition"]["lang"].MyToString();
                review_status = data["condition"]["review_status"].MyToInt();
            }
            if (lang == "")
            {
                lang = "sc";
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>();
            condition.Add(a => a.review_status == (ReViewStatusEnum)review_status);
            condition.Add(a => a.lang == lang);
            if (node_name.Trim1() != "")
            {
                condition.Add(a => a.node_name.Contains(node_name));
            }

            var result = Service.GetPageList(page_index, page_size,"nsort asc,create_time asc", condition);
            return new RestfulPageData<site_node>(0, "ok", result);
        }

        /// <summary>
        /// 获取单条栏目数据
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 在修改绑定页面时公用字段包括:parent_node/node_ext_model/ext_model/node_code/need_review/show_menu/show_path/nsort/lucene_index/pwd/is_show 直接使用sc.data的数据即可
        /// - 详细字段请看首页
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_node_review", resource_code = "site_node_review")]
        [HttpPost("single")]
        public RestfulData GetSingleNode( [FromServices] ISysExtModelDetailesService ext, JObject data)
        {
            long id = data["id"].MyToLong();
  

            site_node model = new site_node();
            site_node sc_model = new site_node();
            site_node en_model = new site_node();


            if (!string.IsNullOrEmpty(id.MyToString()) && id.MyToLong() > 0)
            {
                model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (model == null || model.id <= 0)
            {
                model = new site_node();

            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_node();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_node();
            }

            List<sys_ext_model_detail> tc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> sc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> en_list = new List<sys_ext_model_detail>();
            if (sc_model.node_ext_model.MyToString() != "")
            {
                sc_list = ext.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() {
                a=>a.p_code==model.node_ext_model
                });

                tc_list = sc_list.Clone();
                en_list = sc_list.Clone();
                if (Convert.ToInt64(string.Concat(sc_model.id, "")) > 0)
                {
                    {
                        if (sc_model.extend_fields != null)
                        {
                            var ext_sc = new List<sys_ext_model_detail>();
                            foreach (var property in sc_model.extend_fields)
                            {
                                ext_sc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            sc_list.ForEach(a =>
                            {
                                var db = ext_sc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }

                    {
                        if (model.extend_fields != null)
                        {
                            var ext_tc = new List<sys_ext_model_detail>();
                            foreach (var property in model.extend_fields)
                            {
                                ext_tc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            tc_list.ForEach(a =>
                            {
                                var db = ext_tc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }

                    {
                        if (en_model.extend_fields != null)
                        {
                            var ext_en = new List<sys_ext_model_detail>();
                            foreach (var property in en_model.extend_fields)
                            {
                                ext_en.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            en_list.ForEach(a =>
                            {
                                var db = ext_en.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }
                }
            }
            return new RestfulData<object>(0, "ok", new { sc = new { data = sc_model, ext_model = sc_list }, tc = new { data = model, ext_model = tc_list }, en = new { data = en_model, ext_model = en_list } });
        }



        /// <summary>
        /// 审核待审核栏目
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"需要审核的栏目ID",
        ///     "review_status":"1通过,2不通过"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 恢复到正常状态
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_node_review", resource_code = "site_node_review", operate_code = "review")]
        [HttpPost("review")]
        public RestfulData Review(JObject data)
        {
            long Id = data["id"].MyToLong();
            int review_status = data["review_status"].MyToInt();
            if (Id <= 0|| review_status<=0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == Id && a.lang == "sc" && a.review_status==ReViewStatusEnum.UnReview);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法审核" };
            }

            int flag = Service.Modify(a => a.id == Id, a => new site_node() {review_status=(ReViewStatusEnum)review_status });
            if (flag > 0)
            {
                AddLog("site_node_review", Utils.LogsOperateTypeEnum.ReView, $"审核栏目:{model.node_name}");
                return new RestfulData() { code = 0, msg = "审核成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "审核失败" };
            }
        }
        /// <summary>
        /// 删除审核不通过的栏目
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"审核不通过的栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 此处删除为彻底删除
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_node_review", resource_code = "site_node_review", operate_code = "delete")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long Id = data["id"].MyToLong();
            if (Id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == Id && a.lang == "sc" && a.review_status==ReViewStatusEnum.UnPass);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法删除" };
            }

            int flag = Service.Delete(a => a.id == Id);
            if (flag > 0)
            {
                AddLog("site_node_review", Utils.LogsOperateTypeEnum.Delete, $"删除审核不通过的栏目:{model.node_name}");
                return new RestfulData() { code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }
        }

    }
}
