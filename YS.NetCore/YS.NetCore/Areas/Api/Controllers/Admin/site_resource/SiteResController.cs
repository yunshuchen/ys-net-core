﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Common;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Options;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Models;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Components.RenderTree;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("静态资源")]
    public class SiteResController : ApiBasicController<sys_template, ISysTemplateService>
    {

        public SiteResController(ISysTemplateService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }
        
        /// <summary>
        /// 获取资源文件列表
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的Path"
        /// }
        /// ```
        /// 说明
        /// - relative_path 根目录则传空"",进入home则传入"home",home里面的banner则传入"home/banner"以此类推
        /// 
        /// 
        /// ```json
        /// {
        ///     "relative_path":"contact/error"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("FileTree")]
        public RestfulData GetTplDetail([FromServices] ISiteFileProvider siteProvider,JObject data)
        {

            string relative_path1 = data["relative_path"].MyToString();
           
            var relative_path= relative_path1.Replace('/', Path.DirectorySeparatorChar);
            template_item_entity t_item = new template_item_entity();
            var path_array = relative_path.MySplit(Path.DirectorySeparatorChar);
            string frist_path = string.Join("/", path_array.Take(path_array.Length - 1));
            if (frist_path == null)
            {
                frist_path = relative_path;
            }
            if (frist_path == relative_path)
            {
                frist_path = "";
            }
            t_item.current_path = relative_path1;
            t_item.parent_path = frist_path;
            var path_name= relative_path == "" ? "根目录" : path_array.LastOrDefault();
            if (path_name == null)
            {
                path_name = "根目录";
            }
            t_item.tpl_code = "";
            t_item.path_name = path_name;
            List<template_file_entity> childrens = new List<template_file_entity>();
            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            GetFileAndDirects(rootPath, childrens, relative_path);
            t_item.childrens = childrens;
            return new RestfulData<template_item_entity>(0, "ok", t_item);
        }
        private void GetFileAndDirects(string path, List<template_file_entity> childrens, string relative_path)
        {
            if (!Directory.Exists(path)) return;
            //目录下的文件、文件夹集合
            string[] diArr = System.IO.Directory.GetDirectories(path, "*", System.IO.SearchOption.TopDirectoryOnly);
            string[] rootfileArr = System.IO.Directory.GetFiles(path);
            //文件夹递归
            for (int i = 0; i < diArr.Length; i++)
            {
                //增加父节点
                string fileName = Path.GetFileName(diArr[i]);
                DirectoryInfo info = new DirectoryInfo(diArr[i]);
                childrens.Add(new template_file_entity()
                {
                    chr_name = fileName,
                    chr_type = 2,
                    relative_path = Path.Combine(relative_path, fileName).TrimStart('\\').TrimStart('/').Replace('\\', '/'),
                    mime_type="",
                    file_ext = ""
                });
            }

            //文件直接添加
            foreach (string var in rootfileArr)
            {
                string fileName = Path.GetFileName(var);
                childrens.Add(new template_file_entity()
                {
                    chr_name = fileName,
                    chr_type = 1,
                    relative_path = Path.Combine(relative_path, fileName).TrimStart('\\').TrimStart('/').Replace('\\', '/'),
                    mime_type=MimeMapping.MimeUtility.GetMimeMapping(fileName),
                    file_ext= fileName.GetFileExt()
                });;
            }

        }
        /// <summary>
        /// 获取文件内容用于编辑
        /// </summary>
        ///  <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的文件Path"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("GetContent")]
        public RestfulData GetSingleFile([FromServices] ISiteFileProvider siteProvider, JObject data)
        {

            string relative_path1 = data["relative_path"].MyToString();
        
            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);

            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            if (!System.IO.File.Exists(rootPath))
            {
                return new RestfulData(-1, "文件不存在");
            }

            var source = Utils.Extend.ExtFile.ReadFile(rootPath);

            return new RestfulData<object>(0, "ok", new
            {
                path_name = relative_path1,
                source = source,
                mime_type = MimeMapping.MimeUtility.GetMimeMapping(relative_path1),
                file_ext= relative_path1.GetFileExt()
            });
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="siteProvider"></param>
        /// <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的文件Path",
        ///     "source":"文件内容"
        /// }
        /// ```
        /// 说明
        /// .txt,.html,.cshtml,.css,.js,.json,.md 可用于在线编辑
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("SaveContent")]
        public RestfulData SaveFileContent([FromServices] ISiteFileProvider siteProvider, JObject data)
        {

            string relative_path1 = data["relative_path"].MyToString();
            string source = data["source"].MyToString();

            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);

            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            Utils.Extend.ExtFile.WriteFile(rootPath, source, System.Text.Encoding.UTF8);

            AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"修改资源文件:{relative_path}");
            return new RestfulData(0, "ok");
        }
        /// <summary>
        /// 添加文件
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的Path",
        ///     "file_name":"文件名称ps:abc_121.txt",
        ///     "source":"文件内容"
        /// }
        /// ```
        /// 说明
        /// - file_name 只能以英文、数字、下划线 命名；类型要求：.txt,.html,.cshtml,.css,.js,.json,.md
        /// </param>
        /// <param name="siteProvider"></param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("addFile")]
        public RestfulData AddFile([FromServices] Microsoft.Extensions.Options.IOptions<AllowFileOptions> _options,[FromServices] ISiteFileProvider siteProvider, JObject data)
        {

            string relative_path1 = data["relative_path"].MyToString();
            string source = data["source"].MyToString();
            string file_name = data["file_name"].MyToString();
        
            string chr_name = file_name.Substring(0, file_name.LastIndexOf('.'));
            if (!System.Text.RegularExpressions.Regex.IsMatch(chr_name, Utils.Constant.RegularExpression.LettersAndNumberAndLine))
            {
                return new RestfulData(-1, "文件名只能包含:英文,数字,下划线");
            }
            string ext = file_name.GetFileExt().ToLower();
            List<string> exts = _options.Value.OnlineAllowEditFile;
            if (!exts.Contains(ext))
            {
                return new RestfulData(-1, "文件格式错误");
            }
            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);
            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            if (System.IO.File.Exists(Path.Combine(rootPath, file_name)))
            {
                return new RestfulData(-1, "文件已经存在无法新增");
            }
            Utils.Extend.ExtFile.WriteFile(Path.Combine(rootPath, file_name), source, System.Text.Encoding.UTF8);
            if (System.IO.File.Exists(Path.Combine(rootPath, file_name)))
            {
                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"新增资源文件:{relative_path}/{file_name}");
                return new RestfulData(0, "ok");
            }
            else {
                return new RestfulData(-1, "创建文件失败");
            }
        }

        /// <summary>
        /// 添加文件夹
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的Path",
        ///     "directory_name":"文件夹名称ps:abc_121.txt"
        /// }
        /// ```
        /// 说明
        /// - directory_name 只能以英文、数字、下划线 命名；
        /// </param>
        /// <param name="siteProvider"></param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("addDirectory")]
        public RestfulData AddDirectory([FromServices] ISiteFileProvider siteProvider, JObject data)
        {

            string relative_path1 = data["relative_path"].MyToString();

            string directory_name = data["directory_name"].MyToString();
         
            if (!System.Text.RegularExpressions.Regex.IsMatch(directory_name, Utils.Constant.RegularExpression.LettersAndNumberAndLine))
            {
                return new RestfulData(-1, "文件夹名只能包含:英文,数字,下划线");
            }
           
            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);
            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            string path = Path.Combine(rootPath, directory_name);
            if (System.IO.Directory.Exists(path))
            {
                return new RestfulData(-1, "文件夹已经存在无法新增");
            }
            Directory.CreateDirectory(path);
            if (Directory.Exists(path))
            {
                AddLog("site_tpl", Utils.LogsOperateTypeEnum.Update, $"新增资源文件夹:{relative_path}/{directory_name}");
                return new RestfulData(0, "ok");
            }
            else {
                return new RestfulData(-1, "创建目录失败");
            }
        }
        /// <summary>
        /// 表单上传文件
        /// </summary>
        /// <param name="data">
        ///     "relative_path":"相对于根路径的Path",
        ///     "file":"file对象"
        /// 说明
        /// - 类型要求：
        /// .txt, .html, .cshtml, .js, .css, .json, .flv, .swf, .mkv, .avi, .rm, .rmvb, .mpeg, .mpg, .ogg, .ogv, .mov, .wmv, .mp4, .webm, .mp3, .wav, .mid,.png,.jpg,.jpeg,.gif,.bmp,.webp,.mid,.rar,.zip,.tar,.gz,.7z,.bz2,.cab,.iso, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .md, .xml
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("upload")]
        public RestfulData UploadTplFile([FromServices] Microsoft.Extensions.Options.IOptions<AllowFileOptions> _options, [FromServices] ISiteFileProvider siteProvider, IFormCollection data)
        {

            string relative_path1 = data["relative_path"].MyToString();

       
            if (data.Files.Count <= 0)
            {
                return new RestfulData(-1, "请选择要上传的文件");
            }
            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);
            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            string file_name = data.Files[0].FileName;
            string ext = file_name.GetFileExt();
            List<string> exts = _options.Value.FileAllowFiles;
            if (!exts.Contains(ext))
            {
                return new RestfulData(-1, "文件格式错误");
            }

            file_name = file_name.Substring(0, file_name.LastIndexOf('.'));
            string newfile_yinpin = file_name.ToPinYingGuid();
            string new_name = newfile_yinpin + ext;
            while (true)
            {
                if (!System.IO.File.Exists(Path.Combine(BasePath, new_name)))
                {
                    break;
                }
                else
                {
                    new_name = newfile_yinpin + "_" + DateTime.Now.GetTimestamp() + ext;
                }
            }
            string save_path = Path.Combine(rootPath, new_name);
            data.Files[0].SaveAs(save_path);
            return new RestfulData(0, "ok");

        }


        /// <summary>
        /// 删除文件或文件夹
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "relative_path":"相对于根路径的Path",
        ///     "chr_type":"1文件 2目录"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_tpl", resource_code = "site_tpl")]
        [HttpPost("delete")]
        public RestfulData DeleteFileOrFolder([FromServices] ISiteFileProvider siteProvider, JObject data)
        {


            int chr_type = data["chr_type"].MyToInt();//1文件 2目录

            string relative_path1 = data["relative_path"].MyToString();

       
            if (chr_type != 1 && chr_type != 2)
            {
                return new RestfulData(-1, "参数错误");
            }
            var relative_path = relative_path1.Replace('/', Path.DirectorySeparatorChar);

            string BasePath = siteProvider.WwwrootsePath;
            string rootPath = Path.Combine(BasePath, relative_path);
            if (chr_type == 1)
            {
                if (!System.IO.File.Exists(rootPath))
                {
                    return new RestfulData() { code = -1, msg = "文件不存在" };
                }
                try
                {
                    System.IO.File.Delete(rootPath);
                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Delete, $"删除资源文件:{relative_path}");
                    return new RestfulData() { code = 0, msg = "ok" };
                }
                catch (Exception ex)
                {
                    return new RestfulData() { code = -1, msg = "删除失败" };
                }
            }
            else {
                if (!Directory.Exists(rootPath))
                {
                    return new RestfulData() { code = -1, msg = "目录不存在" };
                }

                try
                {

                    
                    System.IO.Directory.Delete(rootPath, true);
                    AddLog("site_tpl", Utils.LogsOperateTypeEnum.Delete, $"删除资源文件目录:{relative_path}");
                    return new RestfulData() { code = 0, msg = "ok" };
                }
                catch (Exception ex)
                {

                    return new RestfulData() { code = -1, msg = "删除失败" };
                }
            }
        }
    }

}
