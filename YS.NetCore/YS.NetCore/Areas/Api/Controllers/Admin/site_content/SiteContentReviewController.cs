﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("内容审核管理")]
    public class SiteContentReviewController : ApiBasicController<site_content, ISiteContentService>
    {

        public SiteContentReviewController(ISiteContentService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }


        /// <summary>
        /// 获取审核内容列表
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "page_size":"页大小",
        ///     "page_index","页码",
        ///     "condition":{"title":"内容标题,模糊检索,可不传","lang":"语种:sc,tc,en","review_status":"0待审核,2审核不通过,必传默认为0"}
        /// }
        ///```
        ///
        /// 说明
        /// - 在待审核列表时review_status传入0,审核不通过的列表里面传入2
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_content_review", resource_code = "site_content_review")]
        [HttpPost("list")]
        public RestfulData getNodeList(JObject data)
        {
            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string lang = "sc";
            string node_name = "";
            int review_status = 0;
            if (data.ContainsKey("condition"))
            {
                node_name = data["condition"]["title"].MyToString();
                lang = data["condition"]["lang"].MyToString();
                review_status = data["condition"]["review_status"].MyToInt();
            }
            if (lang == "")
            {
                lang = "sc";
            }
            var condition = new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>();
            condition.Add(a => a.review_status == (ReViewStatusEnum)review_status);
            condition.Add(a => a.lang == lang);
            if (node_name.Trim1() != "")
            {
                condition.Add(a => a.title.Contains(node_name));
            }

            var result = Service.GetPageList(page_index, page_size,"is_top desc,n_sort desc,create_time desc", condition);
            return new RestfulPageData<site_content>(0, "ok", result);
        }

        /// <summary>
        /// 获取单条内容数据
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 在修改绑定页面时公用字段包括:node_id/is_node/is_home/is_top/n_sort/view_count/publish_date/is_show 直接使用sc.data的数据即可
        /// - 详细字段请看首页
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_content_review", resource_code = "site_content_review")]
        [HttpPost("single")]
        public RestfulData GetSingleContent([FromServices] ISiteNodeService site_node_service, [FromServices] ISysExtModelDetailesService ext, JObject data)
        {
            long id = data["id"].MyToLong();


            site_content tc_model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();


            if (!string.IsNullOrEmpty(id.MyToString()) && id.MyToLong() > 0)
            {
                tc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (tc_model == null || tc_model.id <= 0)
            {
                tc_model = new site_content();

            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_content();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_content();
            }

            List<sys_ext_model_detail> tc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> sc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> en_list = new List<sys_ext_model_detail>();
            if (sc_model.node_id > 0)
            {

                var node_model = site_node_service.GetByOne(a => a.id == sc_model.node_id);

                if (node_model.node_ext_model.MyToString() != "")
                {
                    sc_list = ext.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() {
                        a=>a.p_code==node_model.node_ext_model
                    });

                    tc_list = tc_list.Clone();
                    en_list = tc_list.Clone();
                    if (Convert.ToInt64(string.Concat(sc_model.id, "")) > 0)
                    {
                        {
                            var ext_sc = new List<sys_ext_model_detail>();
                            foreach (var property in sc_model.extend_fields)
                            {
                                ext_sc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            sc_list.ForEach(a =>
                            {
                                var db = ext_sc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }

                        {
                            var ext_tc = new List<sys_ext_model_detail>();
                            foreach (var property in tc_model.extend_fields)
                            {
                                ext_tc.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            tc_list.ForEach(a =>
                            {
                                var db = ext_tc.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }

                        {
                            var ext_en = new List<sys_ext_model_detail>();
                            foreach (var property in en_model.extend_fields)
                            {
                                ext_en.Add(new sys_ext_model_detail()
                                {
                                    key = property.key.ToString(),
                                    value = property.value.ToString()
                                });
                            }
                            en_list.ForEach(a =>
                            {
                                var db = ext_en.Where(b => b.key == a.key).FirstOrDefault();
                                if (db != null)
                                {
                                    a.value = db.value;
                                }
                            });
                        }
                    }
                }
            }
            return new RestfulData<object>(0, "ok", new { sc = new { data = sc_model, ext_model = sc_list }, tc = new { data = tc_model, ext_model = tc_list }, en = new { data = en_model, ext_model = en_list } });
        }




        /// <summary>
        /// 审核待审核栏目
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"需要审核的栏目ID",
        ///     "review_status":"1通过,2不通过"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 恢复到正常状态
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_content_review", resource_code = "site_content_review", operate_code = "review")]
        [HttpPost("review")]
        public RestfulData Review(JObject data)
        {
            long Id = data["id"].MyToLong();
            int review_status = data["review_status"].MyToInt();
            if (Id <= 0|| review_status<=0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == Id && a.lang == "sc" && a.review_status==ReViewStatusEnum.UnReview);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法审核" };
            }

            int flag = Service.Modify(a => a.id == Id, a => new site_content() {review_status=(ReViewStatusEnum)review_status });
            if (flag > 0)
            {
                AddLog("site_content_review", Utils.LogsOperateTypeEnum.ReView, $"审核内容:{model.title}");
                return new RestfulData() { code = 0, msg = "审核成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "审核失败" };
            }
        }
        /// <summary>
        /// 删除审核不通过的内容
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"审核不通过的内容ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 此处删除为彻底删除
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck(module_code = "site_content_review", resource_code = "site_content_review", operate_code = "delete")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long Id = data["id"].MyToLong();
            if (Id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == Id && a.lang == "sc" && a.review_status==ReViewStatusEnum.UnPass);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法删除" };
            }

            int flag = Service.Delete(a => a.id == Id);
            if (flag > 0)
            {
                AddLog("site_content_review", Utils.LogsOperateTypeEnum.Delete, $"删除审核不通过的内容:{model.title}");
                return new RestfulData() { code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }
        }

    }
}
