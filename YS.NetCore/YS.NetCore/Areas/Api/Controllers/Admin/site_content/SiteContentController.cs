﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("内容管理")]
    public class SiteContentController : ApiBasicController<site_content, ISiteContentService>
    {

        public SiteContentController(ISiteContentService service
            , IApplicationContextAccessor _httpContextAccessor)
        : base(service, _httpContextAccessor)
        {



        }
        /// <summary>
        /// 获取内容管理左侧树形结构
        /// </summary>
        /// <param name="data">
        ///```json
        ///{ 
        ///     "lang":"语种:sc简体,tc繁体,en英文"
        ///} 
        ///```
        ///权限判断:
        ///
        /// - add_disable true则没有添加权限,false则有添加权限
        /// - mod_disable true则没有修改权限,false则有修改权限
        /// - del_disable true则没有删除权限,false则有删除权限
        /// - 点击树形结构时,根据点击节点的add_disable判断此点击节点是否有添加按钮的出现
        /// - 此节点加载的列表(list接口)数据的ID匹配tree节点的ID匹配判断mod_disable/del_disable判断修改/删除按钮的出现
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("tree")]
        public async Task<RestfulData> GetNodeTreeAsync([FromServices] ISiteNodeService site_node_service, JObject data)
        {
            string lang = data["lang"].MyToString();
            var result = await site_node_service.GetTreeDataAsync("content", new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete
            }, lang);
            return new RestfulData<object>(0, "ok", result);
        }

        /// <summary>
        /// 获取内容列表
        /// </summary>
        /// <param name="data">
        ///```json
        ///{
        ///     "page_size":"页大小",
        ///     "page_index","页码",
        ///     "condition":{"node_id":"关联栏目的ID,即点击栏目tree节点的ID","lang":"语种:sc,tc,en"}
        /// }
        ///```
        ///
        ///权限判断:
        ///
        /// - add_disable true则没有添加权限,false则有添加权限
        /// - mod_disable true则没有修改权限,false则有修改权限
        /// - del_disable true则没有删除权限,false则有删除权限
        /// - 点击树形结构时,根据点击节点的add_disable判断此点击节点是否有添加按钮的出现
        /// - 此节点加载的列表数据的ID匹配tree节点的ID匹配判断mod_disable/del_disable判断修改/删除按钮的出现
        /// - 排序为:is_top desc,n_sort desc,id desc,row_key desc,置顶->排序倒序->id倒序->主键倒序
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("list")]
        public RestfulData getContentList(JObject data)
        {

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            long pid = 0;
            string lang = "sc";

            if (data.ContainsKey("condition"))
            {
                pid = data["condition"]["node_id"].MyToLong();
                lang = data["condition"]["lang"].MyToString();
            }

            var condition = new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>();
            condition.Add(a => a.node_id == pid);
            condition.Add(a => a.lang == lang && a.review_status == ReViewStatusEnum.Pass && a.del_flag == DeleteEnum.UnDelete);

            var result = Service.GetPageList(page_index, page_size, "is_top desc,n_sort desc,id desc,row_key desc", condition);
            return new RestfulData<object>(0, "ok", result);
        }

        /// <summary>
        /// 获取单条内容数据
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"栏目ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 在修改查看绑定页面时公用字段包括:node_id/is_node/is_home/is_top/n_sort/view_count/publish_date/is_show 直接使用sc.data的数据即可
        /// - 详细字段请看首页
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("single")]
        public async Task<RestfulData> GetSingleContentAsync([FromServices] IDynamicPermissionCheck PermissionCheck, [FromServices] ISiteNodeService site_node_service, [FromServices] ISysExtModelDetailesService ext, JObject data)
        {
            long id = data["id"].MyToLong();
    

            site_content tc_model = new site_content();
            site_content sc_model = new site_content();
            site_content en_model = new site_content();


            if (!string.IsNullOrEmpty(id.MyToString()) && id.MyToLong() > 0)
            {
                tc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "tc");
                sc_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "sc");
                en_model = Service.Get("row_key asc", a => a.id == id.MyToLong() && a.lang == "en");
            }
            if (tc_model == null || tc_model.id <= 0)
            {
                tc_model = new site_content();

            }
            if (sc_model == null || sc_model.id <= 0)
            {
                sc_model = new site_content();

            }
            if (en_model == null || en_model.id <= 0)
            {
                en_model = new site_content();
            }

            List<sys_ext_model_detail> tc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> sc_list = new List<sys_ext_model_detail>();

            List<sys_ext_model_detail> en_list = new List<sys_ext_model_detail>();
            if (sc_model.node_id > 0)
            {



                var node_model = site_node_service.GetByOne(a => a.id == sc_model.node_id && a.lang == "sc");

                if (node_model.ext_model.MyToString() != "")
                {
                    sc_model.ext_model_code = node_model.ext_model.MyToString();
                    tc_model.ext_model_code = node_model.ext_model.MyToString();
                    en_model.ext_model_code = node_model.ext_model.MyToString();
                    sc_list = ext.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() {
                        a=>a.p_code==node_model.ext_model
                    });

                    tc_list = sc_list.Clone();
                    en_list = sc_list.Clone();
                    if (Convert.ToInt64(string.Concat(sc_model.id, "")) > 0)
                    {
                        {
                            if (sc_model.extend_fields != null)
                            {
                                var ext_sc = new List<sys_ext_model_detail>();
                                foreach (var property in sc_model.extend_fields)
                                {
                                    ext_sc.Add(new sys_ext_model_detail()
                                    {
                                        key = property.key.ToString(),
                                        value = property.value.ToString()
                                    });
                                }
                                sc_list.ForEach(a =>
                                {
                                    var db = ext_sc.Where(b => b.key == a.key).FirstOrDefault();
                                    if (db != null)
                                    {
                                        a.value = db.value;
                                    }
                                });
                            }
                        }

                        {
                            if (tc_model.extend_fields != null)
                            {
                                var ext_tc = new List<sys_ext_model_detail>();
                                foreach (var property in tc_model.extend_fields)
                                {
                                    ext_tc.Add(new sys_ext_model_detail()
                                    {
                                        key = property.key.ToString(),
                                        value = property.value.ToString()
                                    });
                                }
                                tc_list.ForEach(a =>
                                {
                                    var db = ext_tc.Where(b => b.key == a.key).FirstOrDefault();
                                    if (db != null)
                                    {
                                        a.value = db.value;
                                    }
                                });
                            }
                        }

                        {
                            if (en_model.extend_fields != null)
                            {
                                var ext_en = new List<sys_ext_model_detail>();
                                foreach (var property in en_model.extend_fields)
                                {
                                    ext_en.Add(new sys_ext_model_detail()
                                    {
                                        key = property.key.ToString(),
                                        value = property.value.ToString()
                                    });
                                }
                                en_list.ForEach(a =>
                                {
                                    var db = ext_en.Where(b => b.key == a.key).FirstOrDefault();
                                    if (db != null)
                                    {
                                        a.value = db.value;
                                    }
                                });
                            }
                        }
                    }
                }
            }
            return new RestfulData<object>(0, "ok", new { sc = new { data = sc_model, ext_model = sc_list }, tc = new { data = tc_model, ext_model = tc_list }, en = new { data = en_model, ext_model = en_list } });
        }

        /// <summary>
        /// 新增保存/修改保存 内容
        /// </summary>
        /// <param name="tableid"></param>
        /// <param name="modify_mark"></param>
        /// <param name="node_mark"></param>
        /// <param name="searcher"></param>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"内容ID,为0时则表示新增,否则则表示修改此内容"
        ///     "node_id":"*关联栏目ID",
        ///     "is_node":"是否栏目页处显示:1栏目页处显示,0栏目页处不显示",
        ///     "is_home":"是否首页显示,1首页显示,0不首页显示",
        ///     "is_top":"是否置顶,1置顶,0不置顶",
        ///     "n_sort":"排序号,可不填,后台自动生成",
        ///     "view_count":"查看/点击次数",
        ///     "publish_date":"发布日期,可为空,日期格式yyyy-MM-dd",
        ///     "is_show":"是否显示:1显示,0不显示"
        ///     "sc":
        ///     {
        ///         "title":"*内容标题",
        ///         "link":"链接",
        ///         "summary":"简介",
        ///         "chr_content":"内容",
        ///         "cover":"图片",
        ///         "atlas":"图集",
        ///         "tag":"标签",
        ///         "author":"作者",
        ///         "source":"来源",
        ///         "down_file  :"文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        ///     "tc":
        ///     {
        ///         "title":"内容标题",
        ///         "link":"链接",
        ///         "summary":"简介",
        ///         "chr_content":"内容",
        ///         "cover":"图片",
        ///         "atlas":"图集",
        ///         "tag":"标签",
        ///         "author":"作者",
        ///         "source":"来源",
        ///         "down_file  :"文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        ///     "en":
        ///     {
        ///         "title":"内容标题",
        ///         "link":"链接",
        ///         "summary":"简介",
        ///         "chr_content":"内容",
        ///         "cover":"图片",
        ///         "atlas":"图集",
        ///         "tag":"标签",
        ///         "author":"作者",
        ///         "source":"来源",
        ///         "down_file  :"文件",
        ///         "ext_model":[{"key":"key扩展栏目的Key","value":"扩展栏目设置的值"}]
        ///     },
        /// }
        ///```
        ///
        ///说明:
        /// - 详细字段请看首页
        /// - post 的 sc/tc/en 结构必须提交,就算是单语言也必须提交
        /// - post data 根公共树形取数据时从single接口的data.sc节点获取
        /// </param>
        /// <returns></returns>
        [HttpPost("save")]
        [Net.Mvc.Authorize.PermissionCheck]
        public async Task<RestfulData> SaveAsync(
            [FromServices] IDynamicPermissionCheck PermissionCheck
       , [FromServices] ISysTableidService tableid
       , [FromServices] ISiteModifyMarksService modify_mark
       , [FromServices] ISiteContentMarksService node_mark
       , [FromServices] ISiteNodeService site_node_service
       , [FromServices] Net.MyLucene.ISearchService searcher
       , JObject data)
        {
            long id = data["id"].MyToLong();
            long node_id = data["node_id"].MyToLong();

            string node_code = "";
            if (node_id <= 0)
            {
                return new RestfulData(-1, "请选择栏目");
            }
            StatusEnum is_node = (StatusEnum)data["is_node"].MyToInt();
            StatusEnum is_home = (StatusEnum)data["is_home"].MyToInt();
            StatusEnum is_top = (StatusEnum)data["is_top"].MyToInt();
            StatusEnum is_show = (StatusEnum)data["is_show"].MyToInt();
            int n_sort = data["n_sort"].MyToInt();
            int view_count = data["view_count"].MyToInt();
            string publish_date = data["publish_date"].MyToString();


            if (!data.ContainsKey("sc") || !data.ContainsKey("tc") || !data.ContainsKey("en"))
            {
                return new RestfulData(-1, "参数错误");
            }


            if (id > 0)
            {
                if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + node_id, "update"))
                {
                    return new RestfulData(-403, "认证不通过/Status401Unauthorized");
                }
            }
            else
            {
                if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + node_id, "add"))
                {
                    return new RestfulData(-403, "认证不通过/Status401Unauthorized");
                }

            }

            var node_model = site_node_service.GetByOne(a => a.id == node_id && a.lang == "sc");

            node_code = node_model.node_code;

            string sc_title = data["sc"]["title"].MyToString();
            string sc_link = data["sc"]["link"].MyToString();
            string sc_summary = data["sc"]["summary"].MyToString();
            string sc_chr_content = data["sc"]["chr_content"].MyToString();
            string sc_cover = data["sc"]["cover"].MyToString();
            string sc_atlas = data["sc"]["atlas"].MyToString();
            string sc_tag = data["sc"]["tag"].MyToString();
            string sc_author = data["sc"]["author"].MyToString();
            string sc_source = data["sc"]["source"].MyToString();
            string sc_down_file = data["sc"]["down_file"].MyToString();

            string sc_extend_fields_json = data["sc"]["ext_model"].MyToString();
            if (sc_extend_fields_json == "")
            {
                sc_extend_fields_json = "[]";
            }


            string tc_title = data["tc"]["title"].MyToString();
            string tc_link = data["tc"]["link"].MyToString();
            string tc_summary = data["tc"]["summary"].MyToString();
            string tc_chr_content = data["tc"]["chr_content"].MyToString();
            string tc_cover = data["tc"]["cover"].MyToString();
            string tc_atlas = data["tc"]["atlas"].MyToString();
            string tc_tag = data["tc"]["tag"].MyToString();
            string tc_author = data["tc"]["author"].MyToString();
            string tc_source = data["tc"]["source"].MyToString();
            string tc_down_file = data["tc"]["down_file"].MyToString();

            string tc_extend_fields_json = data["tc"]["ext_model"].MyToString();
            if (tc_extend_fields_json == "")
            {
                tc_extend_fields_json = "[]";
            }

            string en_title = data["en"]["title"].MyToString();
            string en_link = data["en"]["link"].MyToString();
            string en_summary = data["en"]["summary"].MyToString();
            string en_chr_content = data["en"]["chr_content"].MyToString();
            string en_cover = data["en"]["cover"].MyToString();
            string en_atlas = data["en"]["atlas"].MyToString();
            string en_tag = data["en"]["tag"].MyToString();
            string en_author = data["en"]["author"].MyToString();
            string en_source = data["en"]["source"].MyToString();
            string en_down_file = data["en"]["down_file"].MyToString();

            string en_extend_fields_json = data["en"]["ext_model"].MyToString();
            if (en_extend_fields_json == "")
            {
                en_extend_fields_json = "[]";
            }
            if (sc_title == "")
            {
                return new RestfulData(-1, "参数错误,简体内容标题必填");
            }
            var user = httpContextAccessor.Current.CurrentUser;
            if (id <= 0)
            {
                int count = n_sort;
                if (n_sort <= 0)
                {
                    count = Service.Count(a => a.node_id == node_id);
                    count = count + 1;
                }
                long new_id = tableid.getNewTableId<site_content>();
                if (new_id <= 0)
                {
                    return new RestfulData(-1, "参数错误");
                }
                site_content result = new site_content();
                result = Service.Add(new site_content()
                {
                    id = new_id,
                    node_id = node_id,
                    is_node = is_node,
                    is_home = is_home,
                    is_top = is_top,
                    n_sort = n_sort,
                    view_count = view_count,
                    publish_date = publish_date,
                    node_code=node_code,
                    is_show = is_show,
                    title = sc_title,
                    link = sc_link,
                    summary = sc_summary,
                    chr_content = sc_chr_content,
                    extend_fields_json = sc_extend_fields_json,
                    cover = sc_cover,
                    atlas = sc_atlas,
                    tag = sc_tag,
                    author = sc_author,
                    source = sc_source,
                    down_file = sc_down_file,

                    status = StatusEnum.Legal,
                    review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                    lang = "sc",
                    lang_flag = 1,
                    creator = user.id,
                    creator_name = user.alias_name,
                    del_flag = DeleteEnum.UnDelete,
                    create_time = DateTime.Now
                });
                if (result.id > 0 && node_model.lucene_index == StatusEnum.Legal)
                {
                    var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(result.lang, node_model.node_code);
                    if (up_lucene_model != null)
                    {
                        searcher.InsertContentIndex(up_lucene_model.node_code, result);
                    }
                }
                if (tc_title.Trim() != "")
                {
                    result = Service.Add(new site_content()
                    {

                        id = new_id,
                        node_id = node_id,
                        is_node = is_node,
                        is_home = is_home,
                        is_top = is_top,
                        n_sort = n_sort,
                        view_count = view_count,
                        publish_date = publish_date,
                        node_code = node_code,
                        title = tc_title,
                        link = tc_link,
                        summary = tc_summary,
                        chr_content = tc_chr_content,
                        extend_fields_json = tc_extend_fields_json,
                        cover = tc_cover,
                        atlas = tc_atlas,
                        tag = tc_tag,
                        author = tc_author,
                        source = tc_source,
                        down_file = tc_down_file,

                        status = StatusEnum.Legal,
                        review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        lang = "tc",
                        lang_flag = 1,
                        creator = user.id,
                        creator_name = user.alias_name,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0 && node_model.lucene_index == StatusEnum.Legal)
                    {
                        var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(result.lang, node_model.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (en_title.Trim() != "")
                {
                    result = Service.Add(new site_content()
                    {

                        id = new_id,
                        node_id = node_id,
                        is_node = is_node,
                        is_home = is_home,
                        is_top = is_top,
                        n_sort = n_sort,
                        view_count = view_count,
                        publish_date = publish_date,
                        node_code = node_code,
                        title = en_title,
                        link = en_link,
                        summary = en_summary,
                        chr_content = en_chr_content,
                        extend_fields_json = en_extend_fields_json,
                        cover = en_cover,
                        atlas = en_atlas,
                        tag = en_tag,
                        author = en_author,
                        source = en_source,
                        down_file = en_down_file,

                        status = StatusEnum.Legal,
                        review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        lang = "en",
                        lang_flag = 1,
                        creator = user.id,
                        creator_name = user.alias_name,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (result.id > 0 && node_model.lucene_index == StatusEnum.Legal)
                    {
                        var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(result.lang, node_model.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, result);
                        }
                    }
                }
                if (result.id > 0)
                {
                    AddLog($"cms_content_g_{node_id}", Utils.LogsOperateTypeEnum.Add, $"新增{node_model.node_name}栏目下内容:{sc_title}");

                    return new RestfulData() { code = 0, msg = "新增成功" };
                }
                else
                {
                    return new RestfulData() { code = -1, msg = "新增失败" };
                }
            }
            else
            {

                if (n_sort <= 0)
                {
                    n_sort = Service.Count(a => a.node_id == node_id);
                    n_sort = n_sort + 1;
                }

                var result = 0;
                var modify = modify_mark.Insert(new site_modify_marks()
                {
                    chr_type = 2,
                    create_id = httpContextAccessor.Current.CurrentUser.id,
                    operate_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                    create_time = DateTime.Now,
                    create_name = user.alias_name
                });


                var sc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "sc");
                if (sc_model != null)
                {
                    result = Service.Modify(a => a.id == id && a.lang == "sc", a => new site_content()
                    {
                        node_code = node_code,
                        node_id = node_id,
                        is_node = is_node,
                        is_home = is_home,
                        is_top = is_top,
                        n_sort = n_sort,
                        view_count = view_count,
                        publish_date = publish_date,
                        is_show = is_show,
                        title = sc_title,
                        link = sc_link,
                        summary = sc_summary,
                        chr_content = sc_chr_content,
                        extend_fields_json = sc_extend_fields_json,
                        cover = sc_cover,
                        atlas = sc_atlas,
                        tag = sc_tag,
                        author = sc_author,
                        source = sc_source,
                        down_file = sc_down_file


                    });
                    var new_sc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "sc");


                    if (result > 0 && node_model.lucene_index == StatusEnum.Legal)
                    {
                        var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(sc_model.lang, node_model.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.UpdateContentIndex(new_sc_model.node_code, new_sc_model);
                        }
                    }
                    //留痕
                    if (modify.id > 0)
                    {
                        //插入旧数据
                        node_mark.Insert(new site_content_marks()
                        {
                            lang = "sc",
                            lang_flag = 1,
                            data_id = id,
                            mark_id = modify.id,
                            data_type = 1,


                            is_node = sc_model.is_node,
                            is_home = sc_model.is_home,
                            is_top = sc_model.is_top,
                            publish_date = sc_model.publish_date,
                            is_show = sc_model.is_show,
                            title = sc_model.title,
                            link = sc_model.link,
                            summary = sc_model.summary,
                            chr_content = sc_model.chr_content,
                            extend_fields_json = sc_model.extend_fields_json,
                            cover = sc_model.cover,
                            atlas = sc_model.atlas,
                            tag = sc_model.tag,
                            author = sc_model.author,
                            source = sc_model.source,
                            down_file = sc_model.down_file,
                        });
                        //插入新数据
                        node_mark.Insert(new site_content_marks()
                        {
                            lang = "sc",
                            data_id = id,
                            mark_id = modify.id,
                            data_type = 2,
                            is_node = new_sc_model.is_node,
                            is_home = new_sc_model.is_home,
                            is_top = new_sc_model.is_top,
                            publish_date = new_sc_model.publish_date,
                            is_show = new_sc_model.is_show,
                            title = new_sc_model.title,
                            link = new_sc_model.link,
                            summary = new_sc_model.summary,
                            chr_content = new_sc_model.chr_content,
                            extend_fields_json = new_sc_model.extend_fields_json,
                            cover = new_sc_model.cover,
                            atlas = new_sc_model.atlas,
                            tag = new_sc_model.tag,
                            author = new_sc_model.author,
                            source = new_sc_model.source,
                            down_file = new_sc_model.down_file
                        });
                    }
                }

                #region 业务不可能
                else
                {

                    var mod_sc_model = Service.Add(new site_content()
                    {
                        id = id,
                        node_id = node_id,
                        is_node = is_node,
                        is_home = is_home,
                        is_top = is_top,
                        n_sort = n_sort,
                        view_count = view_count,
                        publish_date = publish_date,
                        node_code = node_code,
                        is_show = is_show,
                        title = sc_title,
                        link = sc_link,
                        summary = sc_summary,
                        chr_content = sc_chr_content,
                        extend_fields_json = sc_extend_fields_json,
                        cover = sc_cover,
                        atlas = sc_atlas,
                        tag = sc_tag,
                        author = sc_author,
                        source = sc_source,
                        down_file = sc_down_file,

                        status = StatusEnum.Legal,
                        review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                        lang = "sc",
                        lang_flag = 1,
                        creator = user.id,
                        creator_name = user.alias_name,
                        del_flag = DeleteEnum.UnDelete,
                        create_time = DateTime.Now
                    });
                    if (mod_sc_model.id > 0)
                    {
                        var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(mod_sc_model.lang, node_model.node_code);
                        if (up_lucene_model != null)
                        {
                            searcher.InsertContentIndex(up_lucene_model.node_code, mod_sc_model);
                        }
                    }
                    //留痕
                    if (modify.id > 0)
                    {
                        //插入新数据
                        node_mark.Insert(new site_content_marks()
                        {
                            lang = "sc",
                            data_id = id,
                            mark_id = modify.id,
                            data_type = 2,

                            is_node = mod_sc_model.is_node,
                            is_home = mod_sc_model.is_home,
                            is_top = mod_sc_model.is_top,
                            publish_date = mod_sc_model.publish_date,
                            is_show = mod_sc_model.is_show,
                            title = mod_sc_model.title,
                            link = mod_sc_model.link,
                            summary = mod_sc_model.summary,
                            chr_content = mod_sc_model.chr_content,
                            extend_fields_json = mod_sc_model.extend_fields_json,
                            cover = mod_sc_model.cover,
                            atlas = mod_sc_model.atlas,
                            tag = mod_sc_model.tag,
                            author = mod_sc_model.author,
                            source = mod_sc_model.source,
                            down_file = mod_sc_model.down_file
                        });
                    }
                }
                #endregion


                //繁体修改
                if (tc_title != "")
                {
                    var tc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "tc");
                    if (tc_model != null)
                    {
                        #region 繁体修改
                        result = Service.Modify(a => a.id == id && a.lang == "tc", a => new site_content()
                        {

                            node_code = node_code,
                            node_id = node_id,
                            is_node = is_node,
                            is_home = is_home,
                            is_top = is_top,
                            n_sort = n_sort,
                            view_count = view_count,
                            publish_date = publish_date,
                            is_show = is_show,
                            title = tc_title,
                            link = tc_link,
                            summary = tc_summary,
                            chr_content = tc_chr_content,
                            extend_fields_json = tc_extend_fields_json,
                            cover = tc_cover,
                            atlas = tc_atlas,
                            tag = tc_tag,
                            author = tc_author,
                            source = tc_source,
                            down_file = tc_down_file,


                        });
                        var new_tc_model = Service.Get("row_key asc", a => a.id == id && a.lang == "tc");
                        if (result > 0 && node_model.lucene_index == StatusEnum.Legal)
                        {
                            var up_lucene_model = site_node_service.GetUpLeaveLuceneNode(new_tc_model.lang, node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateContentIndex(up_lucene_model.node_code, new_tc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "tc",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,


                                is_node = tc_model.is_node,
                                is_home = tc_model.is_home,
                                is_top = tc_model.is_top,
                                publish_date = tc_model.publish_date,
                                is_show = tc_model.is_show,
                                title = tc_model.title,
                                link = tc_model.link,
                                summary = tc_model.summary,
                                chr_content = tc_model.chr_content,
                                extend_fields_json = tc_model.extend_fields_json,
                                cover = tc_model.cover,
                                atlas = tc_model.atlas,
                                tag = tc_model.tag,
                                author = tc_model.author,
                                source = tc_model.source,
                                down_file = tc_model.down_file,
                            });
                            //插入新数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,

                                is_node = new_tc_model.is_node,
                                is_home = new_tc_model.is_home,
                                is_top = new_tc_model.is_top,
                                publish_date = new_tc_model.publish_date,
                                is_show = new_tc_model.is_show,
                                title = new_tc_model.title,
                                link = new_tc_model.link,
                                summary = new_tc_model.summary,
                                chr_content = new_tc_model.chr_content,
                                extend_fields_json = new_tc_model.extend_fields_json,
                                cover = new_tc_model.cover,
                                atlas = new_tc_model.atlas,
                                tag = new_tc_model.tag,
                                author = new_tc_model.author,
                                source = new_tc_model.source,
                                down_file = new_tc_model.down_file
                            });
                        }
                        #endregion
                    }
                    else
                    {
                        #region 新插入

                        var mod_tc_model = Service.Add(new site_content()
                        {
                            id = id,
                            node_id = node_id,
                            is_node = is_node,
                            is_home = is_home,
                            is_top = is_top,
                            n_sort = n_sort,
                            view_count = view_count,
                            publish_date = publish_date,
                            node_code = node_code,
                            title = tc_title,
                            link = tc_link,
                            summary = tc_summary,
                            chr_content = tc_chr_content,
                            extend_fields_json = tc_extend_fields_json,
                            cover = tc_cover,
                            atlas = tc_atlas,
                            tag = tc_tag,
                            author = tc_author,
                            source = tc_source,
                            down_file = tc_down_file,
                            status = StatusEnum.Legal,
                            review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                            lang = "tc",
                            lang_flag = 1,
                            creator = user.id,
                            creator_name = user.alias_name,
                            del_flag = DeleteEnum.UnDelete,
                            create_time = DateTime.Now
                        });


                        if (mod_tc_model.id > 0)
                        {
                            var up_lucene_model = site_node_service.GetUpLeaveLuceneNode("tc", node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertContentIndex(up_lucene_model.node_code, mod_tc_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "tc",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,

                                is_node = mod_tc_model.is_node,
                                is_home = mod_tc_model.is_home,
                                is_top = mod_tc_model.is_top,
                                publish_date = mod_tc_model.publish_date,
                                is_show = mod_tc_model.is_show,
                                title = mod_tc_model.title,
                                link = mod_tc_model.link,
                                summary = mod_tc_model.summary,
                                chr_content = mod_tc_model.chr_content,
                                extend_fields_json = mod_tc_model.extend_fields_json,
                                cover = mod_tc_model.cover,
                                atlas = mod_tc_model.atlas,
                                tag = mod_tc_model.tag,
                                author = mod_tc_model.author,
                                source = mod_tc_model.source,
                                down_file = mod_tc_model.down_file
                            });
                        }

                        #endregion
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "tc");
                }


                if (en_title != "")
                {
                    var en_model = Service.Get("row_key asc", a => a.id == id && a.lang == "en");
                    if (en_model != null)
                    {
                        result = Service.Modify(a => a.id == id && a.lang == "en", a => new site_content()
                        {

                            node_code = node_code,
                            node_id = node_id,
                            is_node = is_node,
                            is_home = is_home,
                            is_top = is_top,
                            n_sort = n_sort,
                            view_count = view_count,
                            publish_date = publish_date,
                            is_show = is_show,
                            title = en_title,
                            link = en_link,
                            summary = en_summary,
                            chr_content = en_chr_content,
                            extend_fields_json = en_extend_fields_json,
                            cover = en_cover,
                            atlas = en_atlas,
                            tag = en_tag,
                            author = en_author,
                            source = en_source,
                            down_file = en_down_file,
                        });


                        var new_en_model = Service.Get("row_key asc", a => a.id == id && a.lang == "en");

                        if (result > 0)
                        {
                            var up_lucene_model = site_node_service.GetUpLeaveLuceneNode("en", node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.UpdateContentIndex(up_lucene_model.node_code, new_en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入旧数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                lang_flag = 1,
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 1,


                                is_node = en_model.is_node,
                                is_home = en_model.is_home,
                                is_top = en_model.is_top,
                                publish_date = en_model.publish_date,
                                is_show = en_model.is_show,
                                title = en_model.title,
                                link = en_model.link,
                                summary = en_model.summary,
                                chr_content = en_model.chr_content,
                                extend_fields_json = en_model.extend_fields_json,
                                cover = en_model.cover,
                                atlas = en_model.atlas,
                                tag = en_model.tag,
                                author = en_model.author,
                                source = en_model.source,
                                down_file = en_model.down_file,
                            });
                            //插入新数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,

                                is_node = new_en_model.is_node,
                                is_home = new_en_model.is_home,
                                is_top = new_en_model.is_top,
                                publish_date = new_en_model.publish_date,
                                is_show = new_en_model.is_show,
                                title = new_en_model.title,
                                link = new_en_model.link,
                                summary = new_en_model.summary,
                                chr_content = new_en_model.chr_content,
                                extend_fields_json = new_en_model.extend_fields_json,
                                cover = new_en_model.cover,
                                atlas = new_en_model.atlas,
                                tag = new_en_model.tag,
                                author = new_en_model.author,
                                source = new_en_model.source,
                                down_file = new_en_model.down_file
                            });
                        }
                    }
                    else
                    {

                        var mod_en_model = Service.Add(new site_content()
                        {

                            id = id,
                            node_id = node_id,
                            is_node = is_node,
                            is_home = is_home,
                            is_top = is_top,
                            n_sort = n_sort,
                            view_count = view_count,
                            publish_date = publish_date,
                            node_code = node_code,
                            title = en_title,
                            link = en_link,
                            summary = en_summary,
                            chr_content = en_chr_content,
                            extend_fields_json = en_extend_fields_json,
                            cover = en_cover,
                            atlas = en_atlas,
                            tag = en_tag,
                            author = en_author,
                            source = en_source,
                            down_file = en_down_file,

                            status = StatusEnum.Legal,
                            review_status = node_model.need_review == StatusEnum.Legal ? ReViewStatusEnum.UnReview : ReViewStatusEnum.Pass,
                            lang = "en",
                            lang_flag = 1,
                            creator = user.id,
                            creator_name = user.alias_name,
                            del_flag = DeleteEnum.UnDelete,
                            create_time = DateTime.Now
                        });
                        if (mod_en_model.id > 0)
                        {
                            var up_lucene_model = site_node_service.GetUpLeaveLuceneNode("en", node_model.node_code);
                            if (up_lucene_model != null)
                            {
                                searcher.InsertContentIndex(up_lucene_model.node_code, mod_en_model);
                            }
                        }
                        //留痕
                        if (modify.id > 0)
                        {
                            //插入新数据
                            node_mark.Insert(new site_content_marks()
                            {
                                lang = "en",
                                data_id = id,
                                mark_id = modify.id,
                                data_type = 2,

                                is_node = mod_en_model.is_node,
                                is_home = mod_en_model.is_home,
                                is_top = mod_en_model.is_home,
                                publish_date = mod_en_model.publish_date,
                                is_show = mod_en_model.is_show,
                                title = mod_en_model.title,
                                link = mod_en_model.link,
                                summary = mod_en_model.summary,
                                chr_content = mod_en_model.chr_content,
                                extend_fields_json = mod_en_model.extend_fields_json,
                                cover = mod_en_model.cover,
                                atlas = mod_en_model.atlas,
                                tag = mod_en_model.tag,
                                author = mod_en_model.author,
                                source = mod_en_model.source,
                                down_file = mod_en_model.down_file
                            });
                        }
                    }
                }
                else
                {
                    Service.Delete(a => a.id == id && a.lang == "en");
                }
                if (result > 0)
                {

                    AddLog($"cms_content_g_{node_id}", Utils.LogsOperateTypeEnum.Update, $"修改内容:{sc_title}");
                    return new RestfulData(0, $"修改成功");
                }
                else
                {
                    return new RestfulData(-1, $"修改失败");
                }
            }
        }

        /// <summary>
        /// 内容排序
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///     "node_id":"所属栏目的栏目ID,即tree的ID",
        ///     "sort_data":{"内容ID":"排序号","内容ID":"排序号"}
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 传入node_id点击tree的节点id需要判断权限,sort_data为{id:sort}键值对象
        /// - 因为内容有分页所以sort_data 为ID:sort键值对象
        /// </param>
        /// <returns></returns>
        [HttpPost("sort")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData Sort(JObject data)
        {
            long parent_node = data["node_id"].MyToLong();

            if (parent_node <= 0)
            {
                return new RestfulData(-1, "所属栏目ID必传,参数错误");
            }
            var list = JsonHelper.ToObject<List<long>>(data["sort_data"].ToString());
            int count = list.Count();
            foreach (var item in list)
            {
                Service.Modify(a => a.id == item, a => new site_content()
                {
                    n_sort = count
                });
                count--;
            }
            AddLog($"cms_content_g_{parent_node}", Utils.LogsOperateTypeEnum.Update, $"内容排序");

            return new RestfulData() { code = 0, msg = "ok" };
        }

        /// <summary>
        /// 删除内容
        /// </summary>
        /// <param name="PermissionCheck"></param>
        /// <param name="data">
        ///```json
        /// {
        ///     "id":"删除的内容ID"
        /// }
        ///```
        ///
        ///说明:
        ///
        /// - 此处删除为假删除,只是修改del_flag=1
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("delete")]
        public async Task<RestfulData> DeleteAsync([FromServices] IDynamicPermissionCheck PermissionCheck, JObject data)
        {
            long Id = data["id"].MyToLong();
            if (Id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = Service.GetByOne(a => a.id == Id && a.lang == "sc" && a.del_flag == DeleteEnum.UnDelete);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在无法删除" };
            }
            if (!await PermissionCheck.CheckAsync(HttpContext, "site_content", "cms_content_g_" + model.node_id, "delete"))
            {
                return new RestfulData() { code = -403, msg = "forbidden" };
            }

            int flag = Service.Modify(a => a.id == Id, a => new site_content() { del_flag = DeleteEnum.Delete });
            if (flag > 0)
            {
                AddLog($"cms_content_g_{model.node_id}", Utils.LogsOperateTypeEnum.Delete, $"删除内容:{model.title}");
                return new RestfulData() { code = 0, msg = "删除成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "删除失败" };
            }
        }

        /// <summary>
        /// 修改内容显示状态
        /// </summary>
        /// <param name="data">
        /// {
        /// "id":"内容ID",
        /// "is_show":"1显示,0不显示"
        /// }
        /// </param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpPost("change_show")]
        public RestfulData changeAsync(JObject data)
        {
            int status = data["is_show"].MyToInt();
            long id = data["id"].MyToLong();

            if (status < 0 || id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }

            if (status != 0 && status != 1)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }

            var model = Service.GetByOne(a => a.id == id && a.lang == "sc");
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据错误" };
            }
            int flag = Service.Modify(a => a.id == id, a => new site_content()
            {
                is_show = (StatusEnum)status
            });
            if (flag > 0)
            {
                AddLog($"cms_content_g_{model.node_id}", Utils.LogsOperateTypeEnum.Update, $"修改网站内容显示状态:{model.title}=>{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
                return new RestfulData() { code = 0, msg = "操作成功" };
            }
            else
            {
                return new RestfulData() { code = -1, msg = "操作失败" };
            }
        }

        /// <summary>
        /// 获取内容数据
        /// </summary>
        /// <param name="lang">语种:sc,tc,en,默认不传为sc</param>
        ///  <param name="node_id">关联栏目ID</param>
        /// <returns></returns>
        [HttpGet("data/list/{node_id}/{lang}")]
        [HttpGet("data/list/{node_id}")]
        [Net.Mvc.Authorize.PermissionCheck]
        public RestfulData GetContentList(string node_id, string lang = "sc")
        {
            long node_id_long = node_id.MyToLong();
            if (node_id.IsNullOrEmpty() || node_id_long <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var result = Service.GetList("is_top desc,n_sort desc", new List<System.Linq.Expressions.Expression<Func<site_content, bool>>>() {
                a=>a.review_status==ReViewStatusEnum.Pass&&a.del_flag==DeleteEnum.UnDelete&&a.is_show==StatusEnum.Legal&&a.lang==lang&&a.node_id==node_id_long
            });
            return new RestfulData<object>(0, "ok", result);
        }



        /// <summary>
        /// 获取内容的扩展字段列表
        /// </summary>
        /// <param name="extdetailrservice"></param>
        /// <param name="node_id">内容关联的栏目ID</param>
        /// <returns></returns>
        [Net.Mvc.Authorize.PermissionCheck]
        [HttpGet("data/content/extmodel/{node_id}")]
        public RestfulData GetContentExtModelDetail([FromServices] ISysExtModelDetailesService extdetailrservice,[FromServices] ISiteNodeService nodeService, long node_id)
        {
            long id = node_id.MyToLong();
            if (id <= 0)
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            var model = nodeService.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData() { code = -1, msg = "数据不存在" };
            }
            var fields = extdetailrservice.GetList("sort asc", new List<System.Linq.Expressions.Expression<Func<sys_ext_model_detail, bool>>>() { a => a.p_code == model.ext_model });
            return new RestfulData<object>(0, "ok", new { model, fields });

        }

    }
}
