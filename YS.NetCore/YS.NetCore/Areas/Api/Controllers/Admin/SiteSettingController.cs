﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("站点配置获取")]
    public class SiteSettingController : ApiBasicController<sys_account, ISysAccountService>
    {
        public SiteSettingController(ISysAccountService service
                , IApplicationContextAccessor _httpContextAccessor)
            : base(service, _httpContextAccessor)
        {

        }

        /// <summary>
        /// 获取站点设置
        /// "editor_type": "使用的编辑器:UEditor/EWebEditor",
        /// "en_sitename": "英文站点名称[multi_language=false则忽略]",
        /// "favicon": "favicon",
        /// "logo": "站点logo",
        /// "outer_chain_picture": "是否允许外链",
        /// "sc_sitename": "简体站点名称",
        /// "show_data_lang": "默认数据显示语种",
        /// "tc_site_name": "繁体站点名称[multi_language=false则忽略]",
        /// "tpl_code": "模版代号",
        /// "multi_language": "是否为多语种站点:false单语种只有sc,true多语种包含:sc,en,tc",
        /// "site_meta_key_words": "站点meta关键字",
        /// "site_meta_descption": "站点meta说明"
        /// </summary>
        /// <returns>
        /// </returns>
        //[PermissionCheck]
        [HttpGet("setting")]
        public RestfulData GetSiteSetting([FromServices] IApplicationSettingService applicationSettingService)
        {

            string EditorType = applicationSettingService.Get("EditorType", "");
            string en_SiteName = applicationSettingService.Get("en_SiteName", "");

            string Favicon = applicationSettingService.Get("Favicon", "");
            string Logo = applicationSettingService.Get("Logo", "");
            bool OuterChainPicture = applicationSettingService.Get("OuterChainPicture", "true").MyToBool();
            string sc_SiteName = applicationSettingService.Get("sc_SiteName", "");
            string Show_Data_Lang = applicationSettingService.Get("Show_Data_Lang", "");
            string SiteMetaKeywords = applicationSettingService.Get("SiteMetaKeywords", "");
            
            string tc_SiteName = applicationSettingService.Get("tc_SiteName", "");
            string TplCode = applicationSettingService.Get("TplCode", "");
            string SiteMetaDescption = applicationSettingService.Get("SiteMetaDescption", "");
            bool MultiLanguage = httpContextAccessor.Current.MultiLanguage;

            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = new 
                {
                    editor_type= EditorType,
                    en_sitename= en_SiteName,
                    favicon= Favicon,
                    logo=Logo,
                    outer_chain_picture= OuterChainPicture,
                    sc_sitename= sc_SiteName,
                    show_data_lang= Show_Data_Lang,
                    site_meta_keywords= SiteMetaKeywords,
                    site_meta_descption= SiteMetaDescption,
                    tc_site_name = tc_SiteName,
                    tpl_code= TplCode,
                    multi_language= MultiLanguage
                }
            };
        }

        /// <summary>
        /// 保存站点设置
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "en_sitename":"英文站点名称",
        ///     "sc_sitename":"简体站点名称",
        ///     "tc_sitename":"繁体站点名称",
        ///     "favicon":"站点favicon图标",
        ///     "logo":"站点LOGO",
        ///     "outer_chain_picture":"是否允许图片外链,true/false",
        ///     "show_data_lang":"默认数据显示语种:sc/tc/en",
        ///     "tpl_code":"使用模板代号",
        ///     "editor_type":"编辑器类型,UEditor/EWebEditor",
        ///     "site_meta_key_words":"站点meta关键字",
        ///     "site_meta_descption":"站点meta说明"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpPost("Save")]
        public RestfulData SaveSiteSetting([FromServices]IApplicationSettingService AppSite,JObject data)
        {
            string en_sitename = data["en_sitename"].MyToString();
            string sc_sitename = data["sc_sitename"].MyToString();
            string tc_sitename = data["tc_sitename"].MyToString();
            string favicon = data["favicon"].MyToString();
            string logo = data["logo"].MyToString();
            string outer_chain_picture = data["outer_chain_picture"].MyToString();
            string show_data_lang = data["show_data_lang"].MyToString();
            string tpl_code = data["tpl_code"].MyToString();
            string editor_type = data["editor_type"].MyToString();
            string site_meta_key_words = data["site_meta_key_words"].MyToString();
            string site_meta_descption = data["site_meta_descption"].MyToString();

            AppSite.Modify("tc_SiteName", tc_sitename);
            AppSite.Modify("sc_SiteName", sc_sitename);
            AppSite.Modify("en_SiteName", en_sitename);
            AppSite.Modify("TplCode", tpl_code);
            AppSite.Modify("EditorType", editor_type);
            AppSite.Modify("Favicon", favicon);
            AppSite.Modify("Logo", logo);
            AppSite.Modify("OuterChainPicture", outer_chain_picture);
            AppSite.Modify("show_data_lang", show_data_lang);
            AppSite.Modify("SiteMetaKeywords", site_meta_key_words);
            AppSite.Modify("SiteMetaDescption", site_meta_descption);
            return new RestfulData(0, "ok");
        }
    }
}
