﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using YS.Net;
using YS.Net.Models;
using YS.Net.Mvc;
using YS.Net.Mvc.Authorize;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System.DrawingCore.Imaging;
using System.DrawingCore;

namespace YS.NetCore.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("账号相关")]
    public class AccountController : ApiBasicController<sys_account, ISysAccountService>
    {
        //用户角色关系缓存
        private readonly ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        //private readonly ConcurrentDictionary<long, sys_account> online_User;
        private const string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;



        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;

        public AccountController(ISysAccountService service
            , IApplicationContextAccessor _httpContextAccessor
            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>> cacheManager
            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> _role_operate_cacheManager)
        : base(service, _httpContextAccessor)
        {

            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());
            //角色操作关系表
            role_operate_cahce = _role_operate_cacheManager.GetOrAdd(Net.SessionKeyProvider.SysRoleOperateRelation, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());

        }
        /// <summary>
        /// 后台账号登录
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "account":"账号",
        ///     "pwd":"密码:明文密码Md5 ps:e10adc3949ba59abbe56e057f20f883e",
        ///     "validate_code":"验证码,先不处理",
        ///     "validate_key":"验证码key,在调用ValidateCode接口时返回的validate_key",
        /// }
        /// ```
        /// - 说明
        /// ```json
        /// {
        ///     "code":"0 Ok,-1 参数错误,-2需修改密码,-3验证码错误,-4账号或密码错误,-5账号被禁用请联系管理员,-6登录限制"
        /// }
        /// ```
        /// </param>
        /// <returns>
        /// </returns>
        [AllowAnonymous]
        [HttpPost("login")]
        public RestfulData Login([FromServices] IConfiguration Configuration,
        [FromServices] ISysRoleOperateRelationService RoleOperate,
        [FromServices] ISysCatalogService catalog,
        [FromServices] ISysModuleService module,
        [FromServices] ISysResourceService resource,
        JObject data)
        {
            string account = data["account"].MyToString().Trim();
            string pwd = data["pwd"].MyToString().Trim();
            string validate_code = data["validate_code"].MyToString().Trim();
            string validate_key = data["validate_key"].MyToString().Trim();


            var result = Service.UserLogin(account, pwd, CacheKey.LoginValidateCodeCackeKey, validate_key, validate_code, false);

            if (result.GetModelValue<int>("code") == 0)
            {
                var jWt_Key = Configuration.GetSection("JWTSecret").Get<string>();

                var login_data = result.GetModelValue<sys_account>("user");
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(jWt_Key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(System.Security.Claims.ClaimTypes.Name, login_data.account),
                        new Claim(System.Security.Claims.ClaimTypes.NameIdentifier, login_data.id.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Email, login_data.email.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.GivenName, login_data.alias_name.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Hash, login_data.token)
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                string g_Token = tokenHandler.WriteToken(token);
                login_data.token = g_Token;



                //用户的角色ID集合
                List<sys_user_role_relation_entity> user_role_list = null;
                user_role_cahce.TryGetValue(login_data.account.ToLower(), out user_role_list);
                AddLog("Login", Utils.LogsOperateTypeEnum.Login, "登录系统", login_data.id, login_data.alias_name);

                List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();
                //用户所有的权限
                List<sys_role_operate_relation_entity> list = null;
                user_role_list.ForEach(a =>
                {
                    role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                    if (list == null)
                    {
                        list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                            c=>c.role_id==a.role_id
                        });
                        role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                    }
                    user_operate_list.AddRange(list);
                });
                user_operate_list = user_operate_list.GroupBy(p => new { p.module_code, p.res_code, p.operate_code }).Select(g => g.First()).ToList();
                Dictionary<string, object> catas = new Dictionary<string, object>();

                var catalogs = catalog.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                var modules = module.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                var resources = resource.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                //site_content,site_node,site_doc
                List<string> tree_module = new List<string>() { "site_doc", "site_content", "site_node" };

                catalogs.ForEach(cata =>
                {
                    Dictionary<string, object> user_module = new Dictionary<string, object>();

                    var condtions_modules = modules.Where(a => a.p_catalog == cata.id).ToList();

                    condtions_modules.ForEach(modu =>
                    {
                        Dictionary<string, object> user_res = new Dictionary<string, object>();
                        resources.Where(a => a.p_module == modu.id
                        &&!a.res_code.StartsWith("cms_node_g_") && !a.res_code.StartsWith("site_doc_type_") && !a.res_code.StartsWith("cms_content_g")).ToList()
                        .ForEach(resou =>
                        {
                            var mod_res_operates = user_operate_list.Where(a => a.module_code.ToLower() == modu.module_code.ToLower() && a.res_code.ToLower() == resou.res_code.ToLower()).ToList();
                            if (mod_res_operates.Count() > 0)
                            {
                                if (!catas.ContainsKey(cata.chr_code.ToLower()))
                                {
                                    catas.Add(cata.chr_code.ToLower(), new
                                    {
                                        catalog_code = cata.chr_code,
                                        catalog_name = cata.chr_name,
                                        sort = cata.n_sort
                                    });
                                }
                                if (!user_module.ContainsKey(modu.module_code.ToLower()))
                                {
                                    user_module.Add(modu.module_code.ToLower(), new
                                    {
                                        modu.module_code,
                                        module_name = modu.chr_name
                                    });
                                }
                                user_res.Add(resou.res_code.ToLower(), new
                                {
                                    resou.res_code,
                                    resou.res_name,
                                    resou.uri,
                                    resou.url,
                                    operates = mod_res_operates.Where(a => a.is_menu == Utils.StatusEnum.Legal).Select(a =>
                                    {
                                        return new
                                        {
                                            a.operate_code,
                                            a.operate_name,
                                            a.api_url
                                        };
                                    })
                                });
                            }
                        });
                        if (user_module.ContainsKey(modu.module_code.ToLower()))
                        {
                            if (user_res.Count() > 0)
                            {
                                var item_module_info = user_module[modu.module_code.ToLower()];
                                string module_code = item_module_info.GetModelValue<string>("module_code");
                                string module_name = item_module_info.GetModelValue<string>("module_name");

                                user_module[modu.module_code.ToLower()] = new
                                {
                                    module_code,
                                    module_name,
                                    resorces = user_res.Values
                                };
                            }
                            else
                            {
                                user_module.Remove(modu.module_code.ToLower());
                            }
                        }
                    });
                    if (catas.ContainsKey(cata.chr_code.ToLower()))
                    {
                        if (user_module.Count() > 0)
                        {
                            var item_catalog_info = catas[cata.chr_code.ToLower()];
                            string catalog_code = item_catalog_info.GetModelValue<string>("catalog_code");
                            string catalog_name = item_catalog_info.GetModelValue<string>("catalog_name");
                            int sort = item_catalog_info.GetModelValue<int>("sort");
                            catas[cata.chr_code.ToLower()] = new
                            {
                                catalog_code,
                                catalog_name,
                                sort = sort,
                                modules = user_module.Values
                            };
                        }
                        else
                        {
                            catas.Remove(cata.chr_code.ToLower());
                        }
                    }
                });
                return new RestfulData<object>()
                {
                    code = 0,
                    msg = "ok",
                    data = new
                    {
                        user_data = login_data,
                        roles = user_role_list.Select(a => a.role_id),
                        menus = catas.Values,
                        data_api = user_operate_list.Where(a => a.is_menu == Utils.StatusEnum.UnLegal).Select(a => new { a.module_code, a.res_code, a.operate_code, a.api_url, a.operate_name })

                    }
                };
            }


            return new RestfulData()
            {
                code = result.GetModelValue<int>("code"),
                msg = result.GetModelValue<string>("msg"),
            };

        }

        /// <summary>
        /// 获取验证码登录时使用
        /// </summary>
        /// <returns>
        /// ```json
        /// {
        ///     "code":"",
        ///     "msg":"",
        ///     "data":{
        ///         "validate_key":"验证码key",
        ///         "img":"图片base64"
        ///     }
        /// }
        /// ```
        /// </returns>
        [HttpGet("ValidateCode")]
        [AllowAnonymous]
        public RestfulData GetValidateCode([FromServices] IMyCache cache)
        {

            Response.Clear();

            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            var randValue = rand.Next(0, 10000);
            Bitmap image;
            System.IO.MemoryStream ms = new System.IO.MemoryStream();

            string code = "";
            string code_resilt = "";


            image = Net.Common.ValidateImg.CreateImageCode(code);

            var rdm = randValue % 3;
            if (rdm == 0)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateMyImageCode(code);
            }
            else if (rdm == 1)
            {
                code = Net.Common.ValidateImg.CreateVerifyCode();
                code_resilt = code;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            else
            {
                var result = Net.Common.ValidateImg.CreateVerifyExpression();
                code = result.Item1;
                code_resilt = result.Item2;
                image = Net.Common.ValidateImg.CreateImageCode(code);
            }
            image.Save(ms, ImageFormat.Png);

            string strbaser64 = Convert.ToBase64String(ms.ToArray());
            string validate_key = Guid.NewGuid().ToString("N");
            string cache_key = string.Format(CacheKey.LoginValidateCodeCackeKey, validate_key);
            cache.SetString(cache_key, code_resilt.ToLower(), TimeSpan.FromSeconds(120));

            //_httpContextAccessor.HttpContext.Session.SetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey, code_resilt.ToLower());
            return new RestfulData<object>(0, "ok", new
            {
                validate_key,
                img = $"data:image/png;base64,{strbaser64}"
            });
        }
        /// <summary>
        /// 换取用户Token
        /// </summary>
        /// <param name="RoleOperate"></param>
        /// <param name="catalog"></param>
        /// <param name="module"></param>
        /// <param name="resource"></param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpPost("ResetToken")]
        public RestfulData ResetToken([FromServices] IConfiguration Configuration,
        [FromServices] ISysRoleOperateRelationService RoleOperate,
        [FromServices] ISysCatalogService catalog,
        [FromServices] ISysModuleService module,
        [FromServices] ISysResourceService resource,
        [FromServices] IMyCache cache,
        [FromServices] ISiteBasedataService base_data,
        JObject data)
        {
            string account = httpContextAccessor.Current.CurrentUser.account;
            long account_id = httpContextAccessor.Current.CurrentUser.id;
            var result = Service.UserLogin(account);
            if (result.GetModelValue<int>("code") == 0)
            {
                var login_data = result.GetModelValue<sys_account>("user");

                List<sys_user_role_relation_entity> user_role_list = null;
                user_role_cahce.TryGetValue(login_data.account.ToLower(), out user_role_list);

                if (user_role_list == null || user_role_list.Count() <= 0)
                {

                    user_role_list = Service.GetList("", new List<System.Linq.Expressions.Expression<Func<sys_user_role_relation_entity, bool>>>() {
                        a=>a.user_id==login_data.id
                    });
                    //更新用户角色缓存
                    user_role_cahce.TryUpdate(login_data.account.ToLower(), (List<sys_user_role_relation_entity> value11) => { return user_role_list; });
                }
                AddLog("Login", Utils.LogsOperateTypeEnum.Login, "登录系统", login_data.id, login_data.alias_name);

                List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();
                //用户所有的权限
                List<sys_role_operate_relation_entity> list = null;
                user_role_list.ForEach(a =>
                {
                    role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                    if (list == null)
                    {
                        list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                            c=>c.role_id==a.role_id
                        });
                        role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                    }
                    user_operate_list.AddRange(list);
                });
                user_operate_list = user_operate_list.GroupBy(p => new { p.module_code, p.res_code, p.operate_code }).Select(g => g.First()).ToList();

                

                Dictionary<string, object> catas = new Dictionary<string, object>();

                var catalogs = catalog.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                var modules = module.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                var resources = resource.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
                //site_content,site_node,site_doc
                List<string> tree_module = new List<string>() { "site_doc", "site_content", "site_node" };

                catalogs.ForEach(cata =>
                {
                    Dictionary<string, object> user_module = new Dictionary<string, object>();

                    var condtions_modules = modules.Where(a => a.p_catalog == cata.id).ToList();

                    condtions_modules.ForEach(modu =>
                    {
                        Dictionary<string, object> user_res = new Dictionary<string, object>();
                        resources.Where(a => a.p_module == modu.id
                        && !a.res_code.StartsWith("cms_node_g_") && !a.res_code.StartsWith("site_doc_type_") && !a.res_code.StartsWith("cms_content_g")).ToList()
                        .ForEach(resou =>
                        {
                            var mod_res_operates = user_operate_list.Where(a => a.module_code.ToLower() == modu.module_code.ToLower() && a.res_code.ToLower() == resou.res_code.ToLower()).ToList();
                            if (mod_res_operates.Count() > 0)
                            {
                                if (!catas.ContainsKey(cata.chr_code.ToLower()))
                                {
                                    catas.Add(cata.chr_code.ToLower(), new
                                    {
                                        catalog_code = cata.chr_code,
                                        catalog_name = cata.chr_name,
                                        sort = cata.n_sort
                                    });
                                }
                                if (!user_module.ContainsKey(modu.module_code.ToLower()))
                                {
                                    user_module.Add(modu.module_code.ToLower(), new
                                    {
                                        modu.module_code,
                                        module_name = modu.chr_name
                                    });
                                }
                                user_res.Add(resou.res_code.ToLower(), new
                                {
                                    resou.res_code,
                                    resou.res_name,
                                    resou.uri,
                                    resou.url,
                                    operates = mod_res_operates.Where(a => a.is_menu == Utils.StatusEnum.Legal).Select(a =>
                                    {
                                        return new
                                        {
                                            a.operate_code,
                                            a.operate_name,
                                            a.api_url
                                        };
                                    })
                                });
                            }
                        });
                        if (user_module.ContainsKey(modu.module_code.ToLower()))
                        {
                            if (user_res.Count() > 0)
                            {
                                var item_module_info = user_module[modu.module_code.ToLower()];
                                string module_code = item_module_info.GetModelValue<string>("module_code");
                                string module_name = item_module_info.GetModelValue<string>("module_name");

                                user_module[modu.module_code.ToLower()] = new
                                {
                                    module_code,
                                    module_name,
                                    resorces = user_res.Values
                                };
                            }
                            else
                            {
                                user_module.Remove(modu.module_code.ToLower());
                            }
                        }
                    });
                    if (catas.ContainsKey(cata.chr_code.ToLower()))
                    {
                        if (user_module.Count() > 0)
                        {
                            var item_catalog_info = catas[cata.chr_code.ToLower()];
                            string catalog_code = item_catalog_info.GetModelValue<string>("catalog_code");
                            string catalog_name = item_catalog_info.GetModelValue<string>("catalog_name");
                            int sort = item_catalog_info.GetModelValue<int>("sort");
                            catas[cata.chr_code.ToLower()] = new
                            {
                                catalog_code,
                                catalog_name,
                                sort = sort,
                                modules = user_module.Values
                            };
                        }
                        else
                        {
                            catas.Remove(cata.chr_code.ToLower());
                        }
                    }
                });




                var jWt_Key = Configuration.GetSection("JWTSecret").Get<string>();

                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(jWt_Key);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(System.Security.Claims.ClaimTypes.Name, login_data.account),
                        new Claim(System.Security.Claims.ClaimTypes.NameIdentifier, login_data.id.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Email, login_data.email.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.GivenName, login_data.alias_name.MyToString()),
                        new Claim(System.Security.Claims.ClaimTypes.Hash, login_data.token),
                        new Claim(System.Security.Claims.ClaimTypes.Role, "1"),
                    }),
                    Expires = DateTime.UtcNow.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                string g_Token = tokenHandler.WriteToken(token);
                //RedisHelper.Lock("")
                //删除当前用户的其他登录
                //var keys = RedisHelper.Scan(string.Format(CacheKey.UserLoginInfoDelCackeKey, login_account.id));
                //RedisHelper.Del(keys);
                //RedisHelper.SetAsync(string.Format(CacheKey.UserLoginInfoCackeKey, login_data.id.ToString(), login_data.token), login_data);
                cache.Set<sys_account>(string.Format(CacheKey.UserLoginInfoCackeKey, login_data.id.ToString(), login_data.token), login_data);
                login_data.token = g_Token;
                return new RestfulData<object>()
                {
                    code = 0,
                    msg = "ok",
                    data = new
                    {
                        user_data = login_data,
                        roles = user_role_list.Select(a => a.role_id),
                        menus = catas.Values,
                        data_api = user_operate_list.Where(a => a.is_menu == Utils.StatusEnum.UnLegal).Select(a => new { a.module_code, a.res_code, a.operate_code, a.api_url, a.operate_name }).ToList()
                    }
                };
            }


            return new RestfulData()
            {
                code = result.GetModelValue<int>("code"),
                msg = result.GetModelValue<string>("msg"),
            };

        }

        /// <summary>
        /// 再次获取用户权限,获取用户权限
        /// </summary>
        /// <param name="RoleOperate"></param>
        /// <param name="catalog"></param>
        /// <param name="module"></param>
        /// <param name="resource"></param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpGet("permission")]
        public RestfulData UserPermission([FromServices] ISysRoleOperateRelationService RoleOperate,
        [FromServices] ISysCatalogService catalog,
        [FromServices] ISysModuleService module,
        [FromServices] ISysResourceService resource)
        {
            string account = httpContextAccessor.Current.CurrentUser.account;



            //用户的角色ID集合
            List<sys_user_role_relation_entity> user_role_list = null;
            user_role_cahce.TryGetValue(account.ToLower(), out user_role_list);


            List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();
            //用户所有的权限
            List<sys_role_operate_relation_entity> list = null;
            user_role_list.ForEach(a =>
            {
                role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                if (list == null)
                {
                    list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                            c=>c.role_id==a.role_id
                        });
                    role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                }
                user_operate_list.AddRange(list);
            });
            user_operate_list = user_operate_list.GroupBy(p => new { p.module_code, p.res_code, p.operate_code }).Select(g => g.First()).ToList();
            Dictionary<string, object> catas = new Dictionary<string, object>();

            var catalogs = catalog.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
            var modules = module.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete&&a.module_code!="sys_menus"
                });
            var resources = resource.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>() {
                    a=>a.status==Utils.StatusEnum.Legal&&a.delete_status==Utils.DeleteEnum.UnDelete
                });
            //site_content,site_node,site_doc
            List<string> tree_module = new List<string>() { "site_doc", "site_content", "site_node" };

            catalogs.ForEach(cata =>
            {
                Dictionary<string, object> user_module = new Dictionary<string, object>();

                var condtions_modules = modules.Where(a => a.p_catalog == cata.id).ToList();

                condtions_modules.ForEach(modu =>
                {
                    Dictionary<string, object> user_res = new Dictionary<string, object>();
                    resources.Where(a => a.p_module == modu.id
                    && !a.res_code.StartsWith("cms_node_g_") && !a.res_code.StartsWith("site_doc_type_") && !a.res_code.StartsWith("cms_content_g")).ToList()
                    .ForEach(resou =>
                    {
                        var mod_res_operates = user_operate_list.Where(a => a.module_code.ToLower() == modu.module_code.ToLower() && a.res_code.ToLower() == resou.res_code.ToLower()).ToList();
                        if (mod_res_operates.Count() > 0)
                        {
                            if (!catas.ContainsKey(cata.chr_code.ToLower()))
                            {
                                catas.Add(cata.chr_code.ToLower(), new
                                {
                                    catalog_code = cata.chr_code,
                                    catalog_name = cata.chr_name,
                                    sort = cata.n_sort
                                });
                            }
                            if (!user_module.ContainsKey(modu.module_code.ToLower()))
                            {
                                user_module.Add(modu.module_code.ToLower(), new
                                {
                                    modu.module_code,
                                    module_name = modu.chr_name
                                });
                            }
                            user_res.Add(resou.res_code.ToLower(), new
                            {
                                resou.res_code,
                                resou.res_name,
                                resou.uri,
                                resou.url,
                                operates = mod_res_operates.Where(a => a.is_menu == Utils.StatusEnum.Legal).Select(a =>
                                {
                                    return new
                                    {
                                        a.operate_code,
                                        a.operate_name,
                                        a.api_url
                                    };
                                })
                            });
                        }
                    });
                    if (user_module.ContainsKey(modu.module_code.ToLower()))
                    {
                        if (user_res.Count() > 0)
                        {
                            var item_module_info = user_module[modu.module_code.ToLower()];
                            string module_code = item_module_info.GetModelValue<string>("module_code");
                            string module_name = item_module_info.GetModelValue<string>("module_name");

                            user_module[modu.module_code.ToLower()] = new
                            {
                                module_code,
                                module_name,
                                resorces = user_res.Values
                            };
                        }
                        else
                        {
                            user_module.Remove(modu.module_code.ToLower());
                        }
                    }
                });
                if (catas.ContainsKey(cata.chr_code.ToLower()))
                {
                    if (user_module.Count() > 0)
                    {
                        var item_catalog_info = catas[cata.chr_code.ToLower()];
                        string catalog_code = item_catalog_info.GetModelValue<string>("catalog_code");
                        string catalog_name = item_catalog_info.GetModelValue<string>("catalog_name");
                        int sort = item_catalog_info.GetModelValue<int>("sort");
                        catas[cata.chr_code.ToLower()] = new
                        {
                            catalog_code,
                            catalog_name,
                            sort = sort,
                            modules = user_module.Values
                        };
                    }
                    else
                    {
                        catas.Remove(cata.chr_code.ToLower());
                    }
                }
            });
            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = catas
            };

        }

        /// <summary>
        /// 重置密码
        /// </summary>
        /// <param name="data">
        /// {
        ///  "account":"账号",
        ///  "old_pwd":"旧密码-明文",
        ///  "new_pwd":"新密码-明文",
        ///  "code":"验证码,先不处理"
        /// }
        /// </param>
        /// <returns>
        /// {
        ///     "code":"0 Ok,-1 参数错误,-2原密码与新密码一直,-3验证码错误,-4账号或密码错误"
        /// }
        /// </returns>
        [AllowAnonymous]
        [HttpPost("RestPwd")]
        public RestfulData Save(JObject data)
        {
            string account = data["account"].MyToString().Trim();
            string old_pwd = data["old_pwd"].MyToString().Trim();
            string new_pwd = data["new_pwd"].MyToString().Trim();


            if (account == "" || old_pwd == "" || new_pwd == "")
            {
                return new RestfulData() { code = -1, msg = "非法提交" };
            }
            if (old_pwd == new_pwd)
            {
                return new RestfulData() { code = -1, msg = "新密码与旧密码不能一致" };
            }
            var result = Service.UserModifyPwd(account, old_pwd, new_pwd, "",true);
            if (result.GetModelValue<int>("code") == 1)
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok",
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = result.GetModelValue<int>("code"),
                    msg = result.GetModelValue<string>("msg"),
                };
            }


        }


        /// <summary>
        /// 登录之后定时刷新
        /// </summary>
        /// <param name="online_user_cache"></param>
        /// <param name="httpContextAccessor"></param>
        ///  <param name="data">
        /// ```json
        /// {
        ///     "time":"时间戳"
        /// }
        /// ```
        /// - 说明 返回
        /// ```json
        /// {
        ///    "code":"0:ok,-2:需要重新登录,-403需要登录"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpPost("Refresh")]
        public RestfulData Refresh(
            [FromServices] IOnlineUserCache online_user_cache
            , [FromServices] IHttpContextAccessor httpContextAccessor
            ,JObject data)
        {
            var httpContext = httpContextAccessor.HttpContext;

            
            string user_name = "";
            string account_id = "";
            string token = "";
            account_id = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
            user_name = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Name)?.Value;
            token = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Hash)?.Value;

            string cache_key = $"#{token}#{account_id}#";
            //获取缓存
            var user = online_user_cache.GetOnlineUser(cache_key);
            if (user == null)
            {   
                if (token.MyToString() == "")
                {
                    return new RestfulData(-2, "loginout");//缓存没有此数据
                }
                user=Service.GetByOne(a => a.token == token);
                if (user == null)
                {
                    return new RestfulData(-2, "loginout");//缓存没有此数据
                }
                user.last_refresh = DateTime.Now;
                user.put_out_login = DateTime.MaxValue;
                online_user_cache.UpdateOnlineUser(cache_key, user);
                return new RestfulData(0, "ok");
                //这样做则可以恢复最后一个登录
                //可以从数据库查询一次然后
                //如果满足一定条件则可以存入缓存并更新最后刷新时间
                //如果不满足则把此token放入黑名单
            }
            var last_refresh_s = user.last_refresh.MyToString();

            var last_refresh = DateTime.MinValue;
            DateTime.TryParse(last_refresh_s, out last_refresh);
            DateTime Now = DateTime.Now;
            //已经90秒没有刷新了
            if ((Now - last_refresh).TotalSeconds >= 90)
            {
                return new RestfulData(-2, "loginout");
            }
            //被其他用户踢下线
            var put_out_login_s = user.put_out_login.MyToString();
            var put_out_login = DateTime.MinValue;
            DateTime.TryParse(put_out_login_s, out put_out_login);
            if (Now >= put_out_login)
            {
                return new RestfulData(-2, "loginout");
            }
            //更新缓存最后刷新时间
            user.last_refresh = Now;
            online_user_cache.UpdateOnlineUser(cache_key, user);
            return new RestfulData(0, "ok");
        }

    }
}
