﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Api.Controllers.v1
{

    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("菜单栏目管理")]
    public class SiteSysCatalogController : ApiBasicController<sys_catalog_entity, ISysCatalogService>
    {
        IHttpContextAccessor httpContextAccessor;


        private ISysCatalogService _useService;

        public SiteSysCatalogController(ISysCatalogService useService
            , IApplicationContextAccessor _applicationContextAccessor
            , IHttpContextAccessor _httpContextAccessor) : base(useService, _applicationContextAccessor)
        {
            _useService = useService;
            httpContextAccessor = _httpContextAccessor;
        }

        /// <summary>
        /// 获取菜单栏目详细信息
        /// </summary>
        /// <param name="id">
        /// 主键ID int
        /// 拼接url即可 PS:sysmenus/syscatalog/1 
        /// </param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "view")]
        public RestfulData<sys_catalog_entity> Get(long id)
        {

            if (id <= 0)
            {
                return new RestfulData<sys_catalog_entity>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }
            var model = Service.GetByOne(a => a.id == id);

            return new RestfulData<sys_catalog_entity>()
            {
                code = 0,
                msg = "ok",
                data = model
            };
        }

        /// <summary>
        /// 添加系统栏目
        /// </summary>
        /// <param name="data">
        ///```json
        /// {
        ///    "chr_name": "string",
        ///    "chr_code": "string",
        ///    "status": "int 0 /  1",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "add")]
        [HttpPost("add")]
        public RestfulData<sys_catalog_entity> Add(JObject data)
        {

            if (data["chr_code"].MyToString() == "")
            {
                return new RestfulData<sys_catalog_entity>(-1, "参数错误");
            }
            if (data["chr_name"].MyToString() == "")
            {
                return new RestfulData<sys_catalog_entity>(-1, "参数错误");
            }
            if (data["status"].MyToInt() != 0 && data["status"].MyToInt() != 1)
            {
                return new RestfulData<sys_catalog_entity>(-1, "参数错误");
            }
            var check = Service.GetByOne(a => a.chr_code.ToLower() == data["chr_code"].MyToString().ToLower());

            if (check != null)
            {
                return new RestfulData<sys_catalog_entity>(-1, "菜单栏目代号已经存在");
            }

            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";

            var model = new sys_catalog_entity();
            model.delete_status = DeleteEnum.UnDelete;
            model.chr_name = data["chr_name"].MyToString();
            model.chr_code = data["chr_code"].MyToString();
            model.status = (StatusEnum)data["status"].MyToInt();
            model.enable_status = StatusEnum.Legal;
            model.description = data["description"].MyToString();
            model.n_sort = data["n_sort"].MyToInt();
            model.last_modifier_id = current_id;
            model.last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.last_modified_time = DateTime.Now;
            model.creator_id = current_id;
            model.creator_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.created_time = DateTime.Now;
            var result = Service.Insert(model);


            AddLog("sys_catalog", LogsOperateTypeEnum.Add, $"添加菜单栏目:{data["chr_name"].MyToString()}");
            return new RestfulData<sys_catalog_entity>()
            {
                code = 0,
                msg = "ok",
                data = result
            };
        }

        /// <summary>
        /// 菜单栏目列表
        /// </summary>
        /// <param name="data">
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "chr_name":"string",
        ///         "chr_code":"string",
        ///         "status":"int(-1 全部/0 禁用 /1 启用)"
        ///     }
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "list")]
        [HttpPost("list")]
        public RestfulData<object> List(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>();
            int page_size = data["page_size"].MyToInt();
            int pageNum = data["page_index"].MyToInt();


            if (page_size <= 0 || pageNum <= 0)
            {
                return new RestfulData<object>(-1, "参数错误");
            }
            //string json = data["condition"].ToString();
            //var condition = JsonHelper.ToObject<sys_catalog_entity>(json);

            string chr_name = "";
            string chr_code = "";
            int status = -1;
            if (data.ContainsKey("condition"))
            {

                chr_name = data["condition"]["chr_name"].MyToString();
                chr_code = data["condition"]["chr_code"].MyToString();
                status = data["condition"]["status"].MyToInt(-1);
            }
            predicate.Add(a => a.delete_status == DeleteEnum.UnDelete);
            if (status != -1)
            {
                predicate.Add(a => a.status ==(StatusEnum)status);
            }
            if (chr_name != "")
            {
                predicate.Add(a => a.chr_name.Contains(chr_name));
            }
            if (chr_code != "")
            {
                predicate.Add(a => a.chr_code.Contains(chr_code));
            }
            var result = Service.GetPageList(pageNum, page_size, "id asc", predicate);
            return new RestfulData<object>()

            {
                code = 0,
                msg = "ok",
                data = new
                {
                    list = result.list,
                    total = result.total
                }
            };
        }



        /// <summary>
        /// 改变栏目状态
        /// </summary>
        /// <param name="data">
        /// {
        ///    "id":"int 主键",
        ///    "status":"int  0禁用 /1 启用 "
        /// }
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "change_status")]
        [HttpPost("change_status")]
        public RestfulData ChangeStatus(JObject data)
        {

            long id = data["id"].MyToLong();
            int status = data["status"].MyToInt();
            if (id <= 0 || (status != 0 && status != 1))
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_catalog_entity()
            {
                status = (StatusEnum)status
            });

            AddLog("sys_catalog", LogsOperateTypeEnum.Update, $"修改菜单栏目状态:{EnumExt.GetDescriptionByEnum((StatusEnum)status)}");
            if (flag > 0)
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok"
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "修改失败"
                };
            }
        }

        /// <summary>
        /// 修改菜单栏目
        /// </summary>
        /// <param name="id">更新主键标识</param>
        /// <param name="data">
        /// {
        ///    "chr_name": "string",
        ///    "status": "int 0 /  1",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "modify")]
        [HttpPost("update/{id}")]
        public RestfulData UpdateCatalog(long id, sys_catalog_entity data)
        {
            if (id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = 0;
            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";

            flag = Service.Modify(a => a.id == id, a => new sys_catalog_entity()
            {
                chr_name = data.chr_name,
                status = data.status,
                n_sort = data.n_sort,
                description = data.description,
                last_modifier_id = current_id.MyToString(),
                last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name,
                last_modified_time = DateTime.Now,
            });

            if (flag > 0)
            {

                AddLog("sys_catalog", LogsOperateTypeEnum.Update, $"修改菜单栏目:{data.chr_name}");
                return new RestfulData(0, "修改成功");
            }
            else
            {
                return new RestfulData(-1, "修改失败");
            }
        }
        /// <summary>
        /// 删除菜单栏目
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// 拼接url即可 PS:sysmenus/syscatalog/delete/1 ,不需要 POST BODY 
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_catalog", operate_code = "delete")]
        [HttpPost("delete/{id}")]
        public RestfulData Delete(long id)
        {
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }

            var module_s=Service.GetList<sys_module_entity>("id desc", new List<Expression<Func<sys_module_entity, bool>>>() {
                a=>a.p_catalog==id
            });

            //删除关系
            foreach (var item in module_s)
            {
                Service.Delete<sys_role_operate_relation_entity>(a => a.module_code == item.module_code);
                Service.Delete<sys_operate_entity>(a => a.p_module == item.id);
                Service.Delete<sys_resource_entity>(a => a.p_module == item.id);
                Service.Delete<sys_module_entity>(a => a.id == item.id);
            }
            //删除栏目
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {

                AddLog("sys_catalog", LogsOperateTypeEnum.Update, $"删除菜单栏目:{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败");
            }
        }

        /// <summary>
        /// 获取菜单栏目键值
        /// 一般用于下拉选择
        /// </summary>
        /// <param name="delete_status">
        /// 删除状态查询
        /// -1 全部
        /// 1  未删除
        /// 0  已删除
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpGet("data/list")]
        [HttpGet("data/list/{delete_status}")]
        public RestfulData<object> CatalogKeyValue(DeleteEnum delete_status=DeleteEnum.UnDelete)
        {
            List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_catalog_entity, bool>>>();
            if (delete_status != DeleteEnum.ALl)
            {
                predicate.Add(a => a.delete_status == delete_status);
            }
            var list = Service.GetList("n_sort asc", predicate);
            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = list.Select(a => new { a.id, a.chr_code, a.chr_name })
            };
        }


    }
}