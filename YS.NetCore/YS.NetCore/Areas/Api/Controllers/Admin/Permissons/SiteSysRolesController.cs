﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Api.Controllers.v1
{
    /// <summary>
    /// 系统用户管理
    /// </summary>
    [ApiVersion("1.0")]
    [CommentControllerAttribute("系统角色管理")]
    public class SiteSysRolesController : ApiBasicController<sys_role_entity, ISysRoleService>
    {
        IHttpContextAccessor httpContextAccessor;


     

        private readonly IApplicationContextAccessor applicationContextAccessor;
        private ISysRoleOperateRelationService role_config;
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;


        private ISysModuleService module;
        private ISysResourceService resource;
        private ISysOperateService Operate;

        public SiteSysRolesController(ISysRoleService userService,
            ISysRoleOperateRelationService _role_config,
            ISysResourceService _resource,
            ISysOperateService _Operate,
            ISysModuleService _module,
            IHttpContextAccessor _httpContextAccessor,
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> _role_operate_cacheManager,
            IApplicationContextAccessor _applicationContextAccessor) : base(userService, _applicationContextAccessor)
        {

            httpContextAccessor = _httpContextAccessor;

            module = _module;
            role_config = _role_config;
            resource = _resource;
            Operate = _Operate;
            applicationContextAccessor = _applicationContextAccessor;

            //角色操作关系表
            role_operate_cahce = _role_operate_cacheManager.GetOrAdd("Sys_Role_Operate_Relation", new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());

        }
        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="id">
        /// 主键ID int
        /// </param>
        /// <returns></returns>
        [HttpGet("single/{id}")]
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "view")]
        public RestfulData Get(long id)
        {

            if (id <= 0)
            {
                return new RestfulData<object>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }



            sys_role_entity model = new sys_role_entity();
            if ( id.MyToLong() > 0)
            {

                model = Service.GetByOne(a => a.id == id.MyToLong());
                return new RestfulData<sys_role_entity>()
                {
                    code = -1,
                    msg = "数据不存在",
                    data= model
                };
            }
            return new RestfulData<object>()
            {
                code = -1,
                msg = "数据不存在"
            };
        }
        class OpClass
        {

            public long id { get; set; }
            public long pid { get; set; }
            public string name { get; set; }
            public string module_code { get; set; }
            public string res_code { get; set; }
            public string op_code { get; set; }
            public int check { get; set; }

            public List<OpClass> operate { get; set; }

            public List<OpClass> children { get; set; }
        }
        /// <summary>
        /// 获取角色信息
        /// </summary>
        /// <param name="roleid">
        /// 角色ID
        /// </param>
        /// <returns></returns>
        [HttpGet("role/operate/{roleid}")]
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "view")]
        public RestfulData RoleOpSetting([FromServices] ISiteNodeService node, long roleid)
        {

            if (roleid.MyToLong() <= 0)
            {
                return new RestfulData(-1,"参数错误");
            }

            //
            var configList = role_config.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                a=>a.role_id==roleid.MyToLong()
            });
            var module_list = module.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_module_entity, bool>>>() {


            a=>a.delete_status==DeleteEnum.UnDelete
            });
            var resList = resource.GetList("n_sort asc,id asc", new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>() {
                a=>a.delete_status==DeleteEnum.UnDelete&&a.res_code.StartsWith("cms_node_g_")==false&&a.res_code.StartsWith("cms_content_g_")==false
            });

            var OpList = Operate.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>>() {
            a=>a.delete_status==DeleteEnum.UnDelete
            });
            List<object> result = new List<object>();
            foreach (var module_item in module_list)
            {
                List<object> res_list = new List<object>();

                var _resList = resList.Where(a => a.p_module == module_item.id).ToList();
                foreach (var item in _resList)
                {
                    var Operat = OpList.Where(a => a.p_res == item.id).ToList().Select(a =>
                    {
                        return new
                        {
                            Op_Code = a.operate_code,
                            Op_Name = a.operate_name,
                            Checked = configList.Where(b => b.res_code == a.p_res_code && b.operate_code == a.operate_code && b.module_code == module_item.module_code).Count()
                        };
                    });
                    var res = new
                    {
                        Res_Code = item.res_code,
                        Res_Name = item.res_name,
                        Op_List = Operat
                    };
                    res_list.Add(res);
                }

                var module = new
                {
                    Module_Code = module_item.module_code,
                    Module_Name = module_item.chr_name,
                    Res_List = res_list
                };
                result.Add(module);
            }

            List<OpClass> node_list = new List<OpClass>();

            {
                string type = "node";

                var node_result = node.GetAllLevelData(new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.lang=="sc"
            });
                var node_configList = role_config.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                a=>a.role_id==roleid.MyToLong()
            });


                var node_codes = type == "node" ? new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
            } : new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                long before = type == "node" ? 10000 : 20000;

                foreach (var item in node_result)
                {
                    var res_one = new OpClass();
                    res_one.id = before + item.id;
                    res_one.module_code = $"site_{type}";
                    res_one.res_code = $"cms_{type}_g_{item.id}";
                    res_one.op_code = "";
                    res_one.name = item.title;
                    res_one.pid = 0;
                    res_one.operate = new List<OpClass>();
                    res_one.check = 0;
                    res_one.children = new List<OpClass>();
                    foreach (var op in node_codes)
                    {
                        res_one.operate.Add(new OpClass
                        {
                            id = item.id,
                            module_code = $"site_{type}",
                            res_code = $"cms_{type}_g_{item.id}",
                            op_code = op.Key,
                            name = op.Value,
                            pid = 0,
                            check = node_configList.Where(b => b.res_code == $"cms_{type}_g_{item.id}" && b.operate_code == op.Key && b.module_code == $"site_{type}").Count()
                        });

                    }
                    ProcssChildren(res_one, item, node_codes, node_configList, type);

                    node_list.Add(res_one);
                }
            }
            List<OpClass> content_list = new List<OpClass>();
            {
                string type = "content";

                var node_result = node.GetAllLevelData(new List<System.Linq.Expressions.Expression<Func<site_node, bool>>>() {
                a=>a.lang=="sc"
            });
                var node_configList = role_config.GetList("id asc", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                a=>a.role_id==roleid.MyToLong()
            });


                var node_codes = type == "node" ? new Dictionary<string, string>() {
                                { "add","添加"},
                                { "update","修改"},
                                { "delete","删除"},
                                { "sort","本级子栏目排序"},
                                { "view","查看"}
            } : new Dictionary<string, string>() {
                                { "add","添加内容"},
                                { "update","修改内容"},
                                { "delete","删除内容"},
                                { "view","查看内容"}
                            };
                long before = type == "node" ? 10000 : 20000;

                foreach (var item in node_result)
                {
                    var res_one = new OpClass();
                    res_one.id = before + item.id;
                    res_one.module_code = $"site_{type}";
                    res_one.res_code = $"cms_{type}_g_{item.id}";
                    res_one.op_code = "";
                    res_one.name = item.title;
                    res_one.pid = 0;
                    res_one.operate = new List<OpClass>();
                    res_one.check = 0;
                    res_one.children = new List<OpClass>();
                    foreach (var op in node_codes)
                    {
                        res_one.operate.Add(new OpClass
                        {
                            id = item.id,
                            module_code = $"site_{type}",
                            res_code = $"cms_{type}_g_{item.id}",
                            op_code = op.Key,
                            name = op.Value,
                            pid = 0,
                            check = node_configList.Where(b => b.res_code == $"cms_{type}_g_{item.id}" && b.operate_code == op.Key && b.module_code == $"site_{type}").Count()
                        });

                    }
                    ProcssChildren(res_one, item, node_codes, node_configList, type);

                    content_list.Add(res_one);
                }
            }
            return new RestfulData<object>(0, "ok", new { result, node_operate = node_list, content_operate = content_list });
        }

        private void ProcssChildren(OpClass parent, SiteNodesTree currnt_nodes, Dictionary<string, string> node_codes, List<sys_role_operate_relation_entity> configList, string type)
        {
            long before = type == "node" ? 10000 : 20000;
            foreach (var item in currnt_nodes.children)
            {
                var res_one = new OpClass();
                res_one.id = before + item.id;
                res_one.module_code = $"site_{type}";
                res_one.res_code = $"cms_{type}_g_{item.id}";
                res_one.op_code = "";
                res_one.name = item.title;
                res_one.pid = before + item.pid;
                res_one.operate = new List<OpClass>();
                res_one.check = 0;
                res_one.children = new List<OpClass>();
                foreach (var op in node_codes)
                {
                    res_one.operate.Add(new OpClass
                    {
                        id = item.id,
                        module_code = $"site_{type}",
                        res_code = $"cms_{type}_g_{item.id}",
                        op_code = op.Key,
                        name = op.Value,
                        pid = 0,
                        check = configList.Where(b => b.res_code == $"cms_{type}_g_{item.id}" && b.operate_code == op.Key && b.module_code == $"site_{type}").Count()
                    });

                }
                ProcssChildren(res_one, item, node_codes, configList, type);

                parent.children.Add(res_one);
            }
        }
        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "role_name": "string",
        ///    "is_sys": "int,1系统角色,0非系统角色",
        ///    "status": "int 0 /  1",
        ///    "n_sort": "int,排序",
        ///    "description": "说明"
        ///}
        ///```json
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "add,modify,update")]
        [HttpPost("Save")]
        public RestfulData Add(JObject data)
        {
            long id = data["id"].MyToLong();
            string chr_name = data["role_name"].MyToString();
            string description = data["description"].MyToString();

            int status = data["status"].MyToInt();
            int n_sort = data["n_sort"].MyToInt();
            int is_sys = data["is_sys"].MyToInt();
            if (chr_name == "")
            {
                return (new RestfulData() { code = -1, msg = "参数错误" });
            }
            if (status != 1 && status != 0)
            {
                return (new RestfulData() { code = -1, msg = "参数错误" });
            }
            if (id <= 0)
            {
                var user = applicationContextAccessor.Current.CurrentUser;
                Service.Insert(new sys_role_entity()
                {

                    role_name = chr_name,
                    status = (StatusEnum)status,
                    is_sys = (StatusEnum)is_sys,
                    n_sort = n_sort,
                    description = description,
                    delete_status = DeleteEnum.UnDelete,
                    enable_status = StatusEnum.Legal,
                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now,
                    creator_id = user.id.MyToString(),
                    creator_name = user.alias_name,
                    created_time = DateTime.Now
                });

                AddLog("sys_roles", LogsOperateTypeEnum.Add, $"新增角色:{chr_name}");
            }
            else
            {
                var user = applicationContextAccessor.Current.CurrentUser;
                Service.Modify(a => a.id == id, a => new sys_role_entity()
                {
                    role_name = chr_name,
                    status = (StatusEnum)status,
                    is_sys = (StatusEnum)is_sys,
                    n_sort = n_sort,
                    description = description,

                    last_modifier_id = user.id.MyToString(),
                    last_modifier_name = user.alias_name,
                    last_modified_time = DateTime.Now


                });

                AddLog("sys_roles", LogsOperateTypeEnum.Update, $"修改角色:{chr_name}");
            }

            return new RestfulData() { code =0, msg = "保存成功" };
        }

        /// <summary>
        /// 角色列表
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "role_name":"名称 模糊检索 string",
        ///         "status":"状态 int(-1 全部/0 禁用 /1 启用)"
        ///     }
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles")]
        [HttpPost("list")]
        public RestfulData<object> List(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>();
            int page_size = data["page_size"].MyToInt();
            int pageNum = data["page_index"].MyToInt();
            if (page_size <= 0 || pageNum <= 0)
            {
                return new RestfulData<object>(-1, "参数错误");
            }
            //string json = data["condition"].ToString();
            //var conditions = JsonHelper.ToObject<sys_role_entity>(json);
            predicate.Add(a => a.delete_status == Utils.DeleteEnum.UnDelete);
            string role_name = "";
            int status = -1;
            if (data.ContainsKey("condition"))
            {
                role_name = data["condition"]["role_name"].MyToString();
                status = data["condition"]["status"].MyToInt();
            }
            if (status !=-1)
            {
                predicate.Add(a => a.status == (StatusEnum)status);
            }
            if (role_name != "")
            {
                predicate.Add(a => a.role_name.Contains(role_name));
            }

            var result = Service.GetPageList(pageNum, page_size, "id asc", predicate);
            return new RestfulData<object>()

            {
                code = 0,
                msg = "ok",
                data = new
                {
                    list = result.list,
                    total = result.total
                }
            };
        }


        /// <summary>
        /// 改变角色状态
        /// </summary>
        /// <param name="data">
        /// {
        ///    "id":"int 主键",
        ///    "status":"int  0禁用 /1 启用 "
        /// }
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles")]
        [HttpPost("change_status")]
        public RestfulData ChangeStatus(JObject data)
        {
            
            long id = data["id"].MyToLong();
            int status = data["status"].MyToInt();
            if (id <= 0 || (status != 0 && status != 1))
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_role_entity()
            {
                status = (Utils.StatusEnum)status
            });
            if (flag > 0)
            {

                AddLog("sys_role", Utils.LogsOperateTypeEnum.Update, $"修改角色状态:{EnumExt.GetDescriptionByEnum((Utils.StatusEnum)status)}");
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok"
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "修改失败"
                };
            }
        }


        /// <summary>
        /// 修改角色操作权限信息
        /// </summary>
        /// <param name="id">更新主键标识</param>
        /// <param name="data">
        /// ```json
        /// {
        /// "role_id":"角色ID",
        /// "role_operate":
        ///     [
        ///       {"module_code":"string 模块代号","res_code":"string 资源代号","operate_code":"操作代号"},
        ///       {"module_code":"string 模块代号","res_code":"string 资源代号","operate_code":"操作代号"}
        ///     ]
        /// }
        /// ```json
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles")]
        [HttpPost("setroleoperate")]
        public RestfulData UpdateRoleModule([FromServices]    ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> _role_operate_cacheManager, JObject data)
        {
            long id = data["role_id"].MyToLong();

            if (id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }

            var login_user = base.httpContextAccessor.Current.CurrentUser;
            var current_id = login_user.id.MyToString();
            var operates = JsonHelper.ToObject<List<sys_role_operate_relation_entity>>(data["role_operate"].MyToString());

            operates.ForEach(a => a.role_id = id);
            role_config.Delete(a => a.role_id == id);
            role_config.BulkInsert(operates);
            AddLog("sys_role", Utils.LogsOperateTypeEnum.Update, $"修改角色权限");

            role_operate_cahce = _role_operate_cacheManager.GetOrAdd(Net.SessionKeyProvider.SysRoleOperateRelation, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());




            var new_list = role_config.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                         c=>c.role_id==id
                    });
            role_operate_cahce.TryUpdate(id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return new_list; });

            return new RestfulData(0, "修改成功");
        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// 拼接url即可 PS:sysroles/sysroles/delete/1,POST BODY 不传</param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_roles", resource_code = "sys_roles", operate_code = "delete")]
        [HttpPost("delete/{id}")]
        public RestfulData Delete(long id)
        {
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id&&a.is_sys==StatusEnum.UnLegal, a => new sys_role_entity()
            {
                delete_status = DeleteEnum.Delete
            });

            if (flag > 0)
            {

                AddLog("sys_role", Utils.LogsOperateTypeEnum.Delete, $"删除角色{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败,请检查该角色是否存在或者为系统角色?");
            }
        }



        /// <summary>
        /// 角色下拉
        /// </summary>
        /// <returns></returns>
        [PermissionCheck]
        [HttpGet("data/list")]
        public RestfulData DataList()
        {
            List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>();
           
            predicate.Add(a => a.delete_status == Utils.DeleteEnum.UnDelete);
            
            var result = Service.GetList( "id asc", predicate);
            return new RestfulArray<sys_role_entity>(0, "ok", result);
        }
    }
}
