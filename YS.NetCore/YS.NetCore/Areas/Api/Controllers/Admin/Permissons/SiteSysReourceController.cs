﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Api.Controllers.v1
{
    /// <summary>
    /// 系统用户管理
    /// </summary>
    [ApiVersion("1.0")]
    [CommentControllerAttribute("菜单资源管理")]
    public class SiteSysReourceController : ApiBasicController<sys_resource_entity, ISysResourceService>
    {
        IHttpContextAccessor httpContextAccessor;
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;

        private ISysResourceService _useService;

        public SiteSysReourceController(ISysResourceService useService
            , IHttpContextAccessor _httpContextAccessor
            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> _role_operate_cacheManager
            , IApplicationContextAccessor _applicationContextAccessor) : base(useService, _applicationContextAccessor)
        {
            //角色操作关系表
            role_operate_cahce = _role_operate_cacheManager.GetOrAdd("Sys_Role_Operate_Relation", new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());

            _useService = useService;
            httpContextAccessor = _httpContextAccessor;
        }
        /// <summary>
        /// 获取菜单资源详细信息
        /// </summary>
        /// <param name="id">
        /// 主键ID int
        /// 拼接url即可 PS:sysmenus/sysresorce/1
        /// </param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "view")]
        public RestfulData<sys_resource_entity> Get(long id)
        {

            if (id <= 0)
            {
                return new RestfulData<sys_resource_entity>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }
            var model = Service.GetByOne(a => a.id == id);

            return new RestfulData<sys_resource_entity>()
            {
                code = 0,
                msg = "ok",
                data = model
            };
        }

        /// <summary>
        /// 添加菜单资源
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "res_name": "string",
        ///    "res_code": "string",
        ///    "p_module":"int 关联栏目ID",
        ///    "uri":"string 前端资源路径",
        ///    "url":"string 前端连接路径",
        ///    "status": "int 0 /  1",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "add")]
        [HttpPost("add")]
        public RestfulData<sys_resource_entity> Add(JObject data)
        {

            if (data["res_name"].MyToString() == "")
            {
                return new RestfulData<sys_resource_entity>(-1, "参数错误");
            }
            if (data["res_code"].MyToString() == "")
            {
                return new RestfulData<sys_resource_entity>(-1, "参数错误");
            }
            if (data["status"].MyToInt()!=0&& data["status"].MyToInt() != 1)
            {
                return new RestfulData<sys_resource_entity>(-1, "参数错误");
            }
            if (data["p_module"].MyToInt() <=0)
            {
                return new RestfulData<sys_resource_entity>(-1, "参数错误");
            }
            if (data["is_sys"].MyToInt() !=0&& data["is_sys"].MyToInt() != 1)
            {
                return new RestfulData<sys_resource_entity>(-1, "参数错误");
            }

            var check = Service.GetByOne(a => a.res_code.ToLower() == data["res_code"].MyToString().ToLower() && a.p_module == data["p_module"].MyToLong());

            if (check != null)
            {
                return new RestfulData<sys_resource_entity>(-1, "该模块下模块代号已经存在");
            }


            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";

            var model = new sys_resource_entity();
            model.delete_status = Utils.DeleteEnum.UnDelete;
            model.res_name = data["res_name"].MyToString();
            model.res_code = data["res_code"].MyToString();
            model.p_module = data["p_module"].MyToLong();
            model.url= data["url"].MyToString();
            model.uri = data["uri"].MyToString();
            model.status  = (StatusEnum)data["status"].MyToInt();
            model.enable_status = StatusEnum.Legal;
            model.description = data["description"].MyToString();
            model.n_sort = data["n_sort"].MyToInt();
            model.last_modifier_id = current_id;
            model.last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.last_modified_time = DateTime.Now;
            model.creator_id = current_id;
            model.creator_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.created_time = DateTime.Now;
            var result = Service.Insert(model);
            role_operate_cahce.Clear();
            AddLog("sys_resource", Utils.LogsOperateTypeEnum.Add, $"修改菜单资源:{model.res_name}");
            return new RestfulData<sys_resource_entity>()
            {
                code = 0,
                msg = "ok",
                data = result
            };
        }

        /// <summary>
        /// 菜单资源列表
        /// </summary>
        /// <param name="data">
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "res_name":"string",
        ///         "res_code":"string",
        ///         "p_module":"int,父级模块ID",
        ///         "status":"int(-1 全部/0 禁用 /1 启用)"
        ///     }
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "list")]
        [HttpPost("list")]
        public RestfulData<object> List(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>();
            int page_size = data["page_size"].MyToInt();
            int pageNum = data["page_index"].MyToInt();
            if (page_size <= 0 || pageNum <= 0)
            {
                return new RestfulData<object>(-1, "参数错误");
            }

            long p_module = 0;

            string res_name = "";
            string res_code = "";
            int status = -1;
            if (data.ContainsKey("condition"))
            {
                p_module = data["condition"]["p_module"].MyToLong();
                res_name = data["condition"]["res_name"].MyToString();
                res_code = data["condition"]["res_code"].MyToString();
                status = data["condition"]["status"].MyToInt(-1);
            }

     

          
            predicate.Add(a => a.delete_status == Utils.DeleteEnum.UnDelete);
            if (status != -1)
            {
                predicate.Add(a => a.status ==(StatusEnum)status);
            }
            if (res_name != "")
            {
                predicate.Add(a => a.res_name.Contains(res_name));
            }
            if (res_code != "")
            {
                predicate.Add(a => a.res_code.Contains(res_code));
            }

            if (p_module > 0)
            {
                predicate.Add(a => a.p_module == p_module);
            }
            var result = Service.GetPageList(pageNum, page_size, "id asc", predicate);
            return new RestfulData<object>()

            {
                code = 0,
                msg = "ok",
                data = new
                {
                    list = result.list,
                    total = result.total
                }
            };
        }


        /// <summary>
        /// 改变菜单资源状态
        /// </summary>
        /// <param name="data">
        /// {
        ///    "id":"int 主键",
        ///    "status":"int  0禁用 /1 启用 "
        /// }
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "change_status")]
        [HttpPost("change_status")]
        public RestfulData ChangeStatus(JObject data)
        {
            
            long id = data["id"].MyToLong();
            int status = data["status"].MyToInt();
            if (id <= 0 || (status != 0 && status != 1))
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_resource_entity()
            {
                status = (Utils.StatusEnum)status
            });
            if (flag > 0)
            {

                AddLog("sys_resource", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源状态:{EnumExt.GetDescriptionByEnum((Utils.StatusEnum)status)}");
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok"
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "修改失败"
                };
            }
        }

        /// <summary>
        /// 修改栏目信息
        /// </summary>
        /// <param name="id">更新主键标识</param>
        /// <param name="data">
        /// {
        ///    "res_name": "string",
        ///    "uri":"string 前端资源路径",
        ///    "url":"string 前端连接路径",
        ///    "status": "int 0 /  1",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "modify")]
        [HttpPost("update/{id}")]
        public RestfulData UpdateModule(long id, JObject data)
        {
            if (id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = 0;
            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";
        
                flag = Service.Modify(a => a.id == id, a => new sys_resource_entity()
                {
                    res_name = data["res_name"].MyToString(),
                    status = (StatusEnum) data["status"].MyToInt(),
                    n_sort= data["n_sort"].MyToInt(),
                    uri=data["uri"].MyToString(),
                    url = data["url"].MyToString(),
                    description = data["description"].MyToString(),
                    last_modifier_id = current_id.MyToString(),
                    last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name,
                    last_modified_time = DateTime.Now,
                });
            role_operate_cahce.Clear();
            if (flag > 0)
            {

                AddLog("sys_resource", Utils.LogsOperateTypeEnum.Update, $"修改菜单资源:{data["res_name"].MyToString()}");
                return new RestfulData(0, "修改成功");
            }
            else {
                return new RestfulData(-1, "修改失败");
            }
        }
        /// <summary>
        /// 删除菜单模块
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// 拼接url即可 PS:sysmenus/sysresorce/delete/1,POST BODY 为空
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_resource", operate_code = "delete")]
        [HttpPost("delete/{id}")]
        public RestfulData Delete(long id)
        {
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }

            //int flag = Service.Modify(a => a.id == id, a => new sys_resource_entity()
            //{
            //    delete_status = DeleteEnum.Delete
            //});

            //删除关系
            var model = Service.GetByOne("id asc", a => a.id == id);
            var module_model = Service.GetByOne<sys_module_entity>("id asc", a => a.id == model.p_module);
            Service.Delete<sys_role_operate_relation_entity>(a => a.res_code == model.res_code&&a.module_code== module_model.module_code);
            Service.Delete<sys_operate_entity>(a => a.p_res == id);
            //删除栏目
            int flag = Service.Delete(a => a.id == id);

            if (flag > 0)
            {
                role_operate_cahce.Clear();
                AddLog("sys_resource", Utils.LogsOperateTypeEnum.Delete, $"删除菜单资源:{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败");
            }
        }

        /// <summary>
        /// 获取菜单资源键值列表
        /// </summary>
        /// <param name="delete_status">
        /// -1 全部
        /// 0 未删除
        /// 1 已删除
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpGet("data/list")]
        [HttpGet("data/list/{delete_status}")]
        public RestfulData<object> CatalogKeyValue(DeleteEnum delete_status=DeleteEnum.UnDelete)
        {
            List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_resource_entity, bool>>>();
            if (delete_status != DeleteEnum.ALl)
            {
                predicate.Add(a => a.delete_status == delete_status);
            }
            var list = Service.GetList("n_sort asc", predicate);
            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = list.Select(a => new { a.id, chr_code=a.res_code, chr_name=a.res_name })
            };
        }

      
    }
}