﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;
using YS.Utils.Extend;
using Microsoft.AspNetCore.Authentication;
using YS.Utils.Mvc.Authorize;

namespace YS.NetCore.Areas.Api.Controllers.v1
{
    /// <summary>
    /// 系统用户管理
    /// </summary>
    [ApiVersion("1.0")]
    [CommentControllerAttribute("系统账号策略")]
    public class SiteSysStrategyController : ApiBasicController<sys_strategy, ISysStrategyService>
    {

        public SiteSysStrategyController(ISysStrategyService useService
            , IApplicationContextAccessor _applicationContextAccessor) : base(useService, _applicationContextAccessor)
        {

        }

        /// <summary>
        /// 系统策略列表
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "chr_title":"策略名称,模糊检索",
        ///         "chr_type":"ip IP策略,time 登录时间,power_pwd 密码强度,mod_pwd 密码修改天数,login_cooling 连续登录中间冷却分钟数",
        ///         "mobile_phone":"手机号码,模糊检索,不传则不根据此查询"
        ///     }
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [HttpPost("list")]
        [PermissionCheck(module_code = "sys_strategy", resource_code = "sys_strategy")]
        public RestfulData gv_List(JObject data)
        {

            int page_size = data["page_size"].MyToInt();
            int page_index = data["page_index"].MyToInt();
            string chr_title = "";
            string chr_type = "";
            if (data.ContainsKey("condition"))
            {
                chr_title = data["condition"]["chr_title"].MyToString();
                chr_type = data["condition"]["chr_type"].MyToString();
            }

            List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>> condition = new List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>>();

            if (chr_title.Trim() != "")
            {
                condition.Add(a => a.chr_title.Contains(chr_title));
            }

            if (chr_type.Trim() != "")
            {
                condition.Add(a => a.chr_type == chr_type);
            }
            var list = Service.GetPageList(page_index, page_size, "id desc", condition);
            return new RestfulPageData<sys_strategy>(0, "ok", list);
        }
        /// <summary>
        /// 获取单个系统策略信息
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// </param>
        /// <returns></returns>
        [HttpGet("single/{id}")]
        [PermissionCheck]
        public RestfulData Get(long id)
        {
            if (id <= 0)
            {
                return new RestfulData()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }
            var model = Service.GetByOne(a => a.id == id);
            if (model == null)
            {
                return new RestfulData()
                {
                    code = -1,
                    msg = "数据不存在"
                };
            }

            return new RestfulData<sys_strategy>(0, "ok", model);
        }

        /// <summary>
        /// 删除系统策略
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id":"系统策略主键"
        /// }
        /// ```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_strategy", resource_code = "sys_strategy")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long id = data["id"].MyToLong();
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }
            int flag = Service.Delete(a => a.id == id);
            if (flag > 0)
            {
                Service.Delete<sys_user_strategy>(a => a.strategy_id == id);
                AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Delete, $"删除系统策略{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败");
            }
        }

        /// <summary>
        /// 添加系统账户
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id":"主键,,新增保存是传入0,修改是传入修改的策略ID",
        ///     "chr_title":"策略标题",
        ///     "chr_type":"策略类型ip IP策略,time 登录时间,power_pwd 密码强度,mod_pwd 密码修改天数,login_cooling 连续登录中间冷却分钟数,
        ///     "begin_condition":"",
        ///     "end_condition":""
        ///}
        ///```
        /// - 说明
        /// - chr_type=ip
        /// begin_condition=IP开始段,end_condition=IP结束段
        /// - chr_type=time
        /// begin_condition=开始时间段(HH:mm:ss),end_condition=结束时间段(HH:mm:ss)
        /// - chr_type=power_pwd
        /// begin_condition=密码最低位数(数字类型),end_condition=""
        /// - chr_type=mod_pwd
        /// begin_condition=密码必须修改的天数(数字类型),end_condition=""
        /// - chr_type=login_cooling
        /// begin_condition=同个账号连续登录中间间隔分钟数(数字类型),end_condition=""
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_strategy", resource_code = "sys_strategy", operate_code = "add,mod")]
        [HttpPost("Save")]
        public RestfulData Add(JObject data)
        {
            sys_strategy strategy = JsonHelper.ToObject<sys_strategy>(data.MyToString());

            if (strategy.chr_title == "" || strategy.chr_type == "" || (strategy.begin_condition == "" && strategy.end_condition == ""))
            {
                return new RestfulData() { code = -1, msg = "参数错误" };
            }
            strategy.chr_type = strategy.chr_type.ToLower();
            if (strategy.chr_type == "ip")
            {
                if (!RegexCheck.IsIPv4(strategy.begin_condition) && !RegexCheck.IsIPv6(strategy.begin_condition))
                {
                    return new RestfulData() { code = -1, msg = "IP参数错误" };
                }

                if (!RegexCheck.IsIPv4(strategy.end_condition) && !RegexCheck.IsIPv6(strategy.end_condition))
                {
                    return new RestfulData() { code = -1, msg = "IP参数错误" };
                }
            }
            else if (strategy.chr_type == "time")
            {
                if (!RegexCheck.IsDateTime(strategy.begin_condition,81))
                {
                    return new RestfulData() { code = -1, msg = "Time参数错误" };
                }

                if (!RegexCheck.IsDateTime(strategy.end_condition,81))
                {
                    return new RestfulData() { code = -1, msg = "Time参数错误" };
                }
            }
            else if (strategy.chr_type == "power_pwd")
            {
                if (!RegexCheck.IsNum(strategy.begin_condition))
                {
                    return new RestfulData() { code = -1, msg = "密码强度参数错误" };
                }
                strategy.end_condition = "";
            }
            else if (strategy.chr_type == "mod_pwd")
            {
                if (!RegexCheck.IsNum(strategy.begin_condition))
                {
                    return new RestfulData() { code = -1, msg = "密码强制修改参数错误" };
                }
                strategy.end_condition = "";
            }
            else if (strategy.chr_type == "login_cooling")
            {
                if (!RegexCheck.IsNum(strategy.begin_condition))
                {
                    return new RestfulData() { code = -1, msg = "连续登录冷却参数错误" };
                }
                strategy.end_condition = "";
            }
            else
            {
                return new RestfulData() { code = -1, msg = "未知策略类型" };
            }
            long id = strategy.id.MyToLong();
            long new_id = id;
            if (id <= 0)
            {
                var model = Service.Insert(strategy);
                new_id = model.id;
                AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Add, $"新增系统策略{strategy.chr_title}");
            }
            else
            {
                bool flag = false;
                flag = Service.Modify(a => a.id == id, a => new sys_strategy()
                {
                    chr_title = strategy.chr_title,
                    chr_type = strategy.chr_type,
                    begin_condition = strategy.begin_condition,
                    end_condition = strategy.end_condition,
                }) > 0;
                if (!flag)
                {
                    new_id = 0;
                }
                AddLog("sys_strategy", Utils.LogsOperateTypeEnum.Update, $"修改系统策略,{strategy.chr_title}");
            }

            if (new_id > 0)
            {
                return new RestfulData() { code = 0, msg = "操作成功" };
            }
            return new RestfulData() { code = -1, msg = "操作失败" };
        }

    }
}