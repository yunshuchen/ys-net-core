﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;
using YS.Utils.Extend;
using Microsoft.AspNetCore.Authentication;
using YS.Utils.Mvc.Authorize;

namespace YS.NetCore.Areas.Api.Controllers.v1
{
    /// <summary>
    /// 系统用户管理
    /// </summary>
    [ApiVersion("1.0")]
    [CommentControllerAttribute("系统账号管理")]
    public class SiteSysAccountController : ApiBasicController<sys_account, ISysAccountService>
    {
        IHttpContextAccessor httpContextAccessor;
        private ISysUserRoleRelationService accountrole;
        ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role;
        private ISysAccountService _useService;

        private ISysRoleService role;
        IMd5Hash hashmd5;
        public SiteSysAccountController(ISysAccountService useService
            , IHttpContextAccessor _httpContextAccessor
            , ISysUserRoleRelationService _accountrole
            , ISysRoleService _role
            , IApplicationContextAccessor _applicationContextAccessor

            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>> _user_role
            , IMd5Hash _hashmd5) : base(useService, _applicationContextAccessor)
        {
            //用户角色
            user_role = _user_role.GetOrAdd(Net.SessionKeyProvider.SysUserRoleRelation, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>()); ;
            accountrole = _accountrole;
            hashmd5 = _hashmd5;
            role = _role;
            _useService = useService;
            httpContextAccessor = _httpContextAccessor;
        }
        /// <summary>
        /// 获取单个账户信息
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// </param>
        /// <returns></returns>
        [HttpGet("single/{id}")]
        [PermissionCheck]
        public RestfulData<object> Get(
            [FromServices] ISysStrategyService sys_strategy
            , [FromServices] ISysUserStrategyService sys_user_strategy,long id)
        {


            if (id <= 0)
            {
                return new RestfulData<object>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }

            var model = Service.GetByOne(a => a.id == id);
            model.password = "";
            var list = role.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>() {
                  a=> a.status ==StatusEnum.Legal
                });
            List<object> result = new List<object>();

            var strategys = sys_strategy.GetList("", new List<System.Linq.Expressions.Expression<Func<sys_strategy, bool>>>());

            var user_strategy_condition = new List<System.Linq.Expressions.Expression<Func<sys_user_strategy, bool>>>();


          


                user_strategy_condition.Add(a => a.user_id == id.MyToLong());


            var user_strategy = sys_user_strategy.GetList("", user_strategy_condition);

            List<object> strategys_list = new List<object>();
            foreach (var item in strategys)
            {
                strategys_list.Add(new
                {
                    strategy_id = item.id,
                    strategy_name = item.chr_title,
                    selected = user_strategy.Where(b => b.strategy_id == item.id).Count()
                });
            }
            if (id.MyToLong() > 0)
            {
                var UserRole = accountrole.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==id.MyToLong()
                });
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        selected = UserRole.Where(a => a.role_id == item.id).Count()
                    });
                }
            }
            else
            {
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        selected = 0
                    });
                }
            }



            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = new { account_data = model, roles = result, strategys_list }
            };
        }
        /// <summary>
        /// 获取账号的角色
        /// </summary>
        /// <param name="id">
        /// 账号ID 主键
        /// </param>
        /// <returns></returns>
        [HttpGet("role/{id}")]
        [PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "viewrole")]
        public RestfulData<object> GetAccountRole(long id)
        {

            if (id <= 0)
            {
                return new RestfulData<object>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }

            var list = role.GetList("n_sort asc", new List<System.Linq.Expressions.Expression<Func<sys_role_entity, bool>>>() {
                  a=> a.status ==StatusEnum.Legal&&a.delete_status==DeleteEnum.UnDelete
            });

            List<object> result = new List<object>();
            if (id.MyToLong() > 0)
            {
                var UserRole = accountrole.GetList("id desc", new List<System.Linq.Expressions.Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==id.MyToLong()
                });
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        selected = UserRole.Where(a => a.role_id == item.id).Count()
                    });
                }
            }
            else
            {
                foreach (var item in list)
                {
                    result.Add(new
                    {
                        role_id = item.id,
                        chr_name = item.role_name,
                        selected = 0
                    });
                }
            }
            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = result
            };
        }
        ///// <summary>
        ///// 设置账号角色
        ///// </summary>
        ///// <param name="id">账号ID 主键</param>
        ///// <param name="data">
        ///// {
        /////     "roleid":"string  多个用,分割 ps:1,3,8,可以从角色接口获取sitesysrole/data/list"
        ///// }
        ///// </param>
        ///// <returns></returns>
        //[HttpPost("set_roles/{id:int}")]
        //[PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "set_role")]
        //public RestfulData SetRoles(long id, JObject data)
        //{

        //    if (id <= 0)
        //    {
        //        return new RestfulData(-1, "参数错误");
        //    }

        //    string roles_ids = data["ids"].MyToString();
        //    List<long> roles = roles_ids.Split(',').Select(a => a.MyToLong()).Where(a => a > 0).ToList();
        //    accountrole.Delete(a => a.user_id == id);

        //    var usermodel=Service.GetByOne(a => a.id == id);
        //    List<sys_user_role_relation_entity> b_insert = new List<sys_user_role_relation_entity>();
        //    foreach (var item in roles)
        //    {
        //        b_insert.Add(new sys_user_role_relation_entity()
        //        {
        //            user_id = id,
        //            role_id = item
        //        });
        //    }
        //    accountrole.BulkInsert(b_insert);
        //    AddLog("sysaccount", Utils.LogsOperateTypeEnum.Update, $"修改用户角色,用户名称:{usermodel?.alias_name}");
        //    return new RestfulData(0, "保存成功");
        //}



        /// <summary>
        /// 添加系统账户
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "id":"账号ID,新增保存是传入0,修改是传入修改的账户ID"
        ///     "account":"账号"
        ///     "password":"密码:明文,修改不传入则不修改密码" ,
        ///     "alias_name":"昵称" ,
        ///     "chr_name":"姓名",
        ///     "email":"邮件",
        ///     "user_status":"user_status",
        ///     "roles":"1,3,5 多个以逗号隔开,可以从角色接口获取sitesysrole/data/list 获取",
        ///     "mobile_phone":"手机号码",
        ///     "strategy":"账号使用策略,多个用,分割:1,3,5"
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "add")]
        [HttpPost("Save")]
        public RestfulData Add([FromServices] ISysUserStrategyService sys_user_strateg, [FromServices] ISysUserPwdLogService sys_user_pwd,[FromServices]IMd5Hash md5Hash,JObject data)
        {
            long id = data["id"].MyToLong();
            string account = data["account"].MyToString();
            string alias_name = data["alias_name"].MyToString();
            string pwd = data["password"].MyToString();
            string email = data["email"].MyToString();
            string mobile_phone = data["mobile_phone"].MyToString();
            string chr_name = data["chr_name"].MyToString(); 
            int status = data["user_status"].MyToInt();
            string roles = data["roles"].MyToString();
            string strategy = data["strategy"].MyToString();
            if (account == "" || alias_name == "" || (status != 0 && status != 1))
            {
                return new RestfulData (){ code = -1, msg = "参数错误" };
            }
            long new_id = id;
            if (id <= 0)
            {
                if (pwd.Trim().Length <= 0)
                {
                    return (new RestfulData() { code = -1, msg = "参数错误" });
                }
                var check = Service.GetByOne("", a => a.account == account);
                if (check != null)
                {
                    return (new RestfulData() { code = -1, msg = "账号已存在" });
                }
                bool flag = false;
                var new_md5 = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(pwd));
                var model = Service.Insert(new sys_account()
                {
                    account = account,
                    password = new_md5,
                    alias_name = alias_name,
                    chr_name= chr_name,
                    user_status = (StatusEnum)status,
                    create_time = DateTime.Now,
                    login_count = 0,
                    last_login_ip = "",
                    creator_name = "",
                    email = email,
                    mobile_phone = mobile_phone,
                    last_login_time = DateTime.MinValue,
                    token = "",
                    last_online_time = DateTime.MinValue,
                    next_set_pwd = DateTime.MinValue
                });
                new_id = model.id;
                flag = new_id > 0;
                sys_user_pwd.Insert(new sys_user_pwd_log()
                {
                    mod_time = DateTime.Now,
                    new_pwd = model.password,
                    user_id = model.id
                });
                AddLog("sys_account", Utils.LogsOperateTypeEnum.Add, $"新增账号,account:{account},别名:{chr_name}");
            }
            else
            {
                bool flag = false;
                if (pwd.Trim().Length > 0)
                {
                    var new_md5 = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(pwd));
                    flag = Service.Modify(a => a.id == id, a => new sys_account()
                    {
                        alias_name = alias_name,
                        chr_name= chr_name,
                        email = email,
                        mobile_phone = mobile_phone,
                        user_status = (StatusEnum)status,
                        password = new_md5
                    }) > 0;
                    var model = Service.GetByOne("id asc", a => a.id == id);
                    sys_user_pwd.Insert(new sys_user_pwd_log()
                    {
                        mod_time = DateTime.Now,
                        new_pwd = model.password,
                        user_id = id
                    });
                    AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改账号信息与密码,account:{account},别名:{chr_name}");
                }
                else
                {
                    flag = Service.Modify(a => a.id == id, a => new sys_account()
                    {
                        email = email,
                        alias_name = alias_name,
                        chr_name = chr_name,
                        mobile_phone = mobile_phone,
                        user_status = (StatusEnum)status,
                    }) > 0;
                    AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改账号信息,account:{account},别名:{chr_name}");
                }
            }
            if (new_id > 0)
            {
                //删除以前的
                accountrole.Delete(a => a.user_id == new_id);
                var roles_arr = roles.Split(',');
                List<sys_user_role_relation_entity> b_insert = new List<sys_user_role_relation_entity>();
                foreach (var item in roles_arr)
                {
                    b_insert.Add(new sys_user_role_relation_entity()
                    {
                        user_id = new_id,
                        role_id = item.MyToLong()
                    });
                }
                accountrole.BulkInsert(b_insert);
                sys_user_strateg.Delete(a => a.user_id == new_id);
                var strategy_arr = strategy.MySplit(',');
                List<sys_user_strategy> s_insert = new List<sys_user_strategy>();
                foreach (var item in strategy_arr)
                {
                    s_insert.Add(new sys_user_strategy()
                    {
                        user_id = new_id,
                        strategy_id = item.MyToLong()
                    });
                }
                sys_user_strateg.BulkInsert(s_insert);
                return  new RestfulData() { code = 0, msg = "操作成功" };
            }
            return  new RestfulData() { code = -1, msg = "操作失败" };
        }

        /// <summary>
        /// 系统账户列表
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "account":"账号,模糊检索",
        ///         "user_status":"状态,不传为全部(-1 全部/0 禁用 /1 启用)",
        ///         "mobile_phone":"手机号码,模糊检索,不传则不根据此查询"
        ///     }
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "list")]
        [HttpPost("list")]
        public RestfulData<object> List(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_account, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_account, bool>>>();
            int page_size = data["page_size"].MyToInt();
            int pageNum = data["page_index"].MyToInt();
            if (page_size <= 0 || pageNum <= 0)
            {
                return new RestfulData<object>(-1, "参数错误");
            }
            //string json = data["condition"].ToString();
            //var conditions = JsonHelper.ToObject<sys_account>(json);
            string account = "";
            string mobile_phone = "";
            int user_status = -1;
            if (data.ContainsKey("condition"))
            {

                account = data["condition"]["account"].MyToString();
                mobile_phone = data["condition"]["mobile_phone"].MyToString();
                user_status = data["condition"]["user_status"].MyToInt(-1);
            }
            if (user_status !=-1)
            {
                predicate.Add(a => a.user_status ==(StatusEnum)user_status);
            }
            if (account != "")
            {
                predicate.Add(a => a.account.Contains(account));
            }


            if (mobile_phone.Trim().Length > 0)
            {
                predicate.Add(a => a.mobile_phone.Contains(mobile_phone));
            }


            var result = Service.GetUserRoleList(pageNum, page_size, "id asc", predicate);


            return new RestfulData<object>()

            {
                code = 0,
                msg = "ok",
                data = result.data
            };
        }


        /// <summary>
        /// 改变系统账号状态
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "id":"int 主键",
        ///    "user_status":"int  0禁用 /1 启用 "
        /// }
        /// ```
        /// /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "change_status")]
        [HttpPost("change_status")]
        public RestfulData ChangeStatus(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_account, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_account, bool>>>();
            long id = data["id"].MyToLong();
            int status = data["user_status"].MyToInt();

            AddLog("sys_account", Utils.LogsOperateTypeEnum.Update, $"修改用户{id}状态:{EnumExt.GetDescriptionByEnum((Utils.StatusEnum)status)}");
            if (id <= 0 || (status != 0 && status != 1))
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_account()
            {
                user_status = (Utils.StatusEnum)status
            });
            if (flag > 0)
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok"
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "修改失败"
                };
            }
        }

       
        /// <summary>
        /// 删除系统账号
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// 拼接url即可 delete/1 , POST BODY 为空
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_account", resource_code = "sys_account", operate_code = "delete")]
        [HttpPost("delete")]
        public RestfulData Delete(JObject data)
        {
            long id = data["id"].MyToLong();
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }

            int flag = Service.Delete(a => a.id == id);
  
            if (flag > 0)
            {
                Service.Delete<sys_user_role_relation_entity>(a => a.user_id == id);
                Service.Delete<sys_user_strategy>(a => a.user_id == id);
                AddLog("sys_account", Utils.LogsOperateTypeEnum.Delete, $"删除账号{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败");
            }
        }

      
    }
}