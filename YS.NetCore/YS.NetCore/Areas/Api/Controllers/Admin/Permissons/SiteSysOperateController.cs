﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Mvc;
using YS.Net;
using YS.Utils.Encrypt;
using YS.Net.Mvc.Authorize;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using YS.Utils;
using YS.Utils.Cache;
using System.Linq.Expressions;
using YS.Net.Mvc;

namespace YS.NetCore.Areas.Api.Controllers.v1
{
    /// <summary>
    /// 操作管理
    /// </summary>
    [ApiVersion("1.0")]
    [CommentControllerAttribute("菜单操作管理")]
    public class SiteSysOperateController : ApiBasicController<sys_operate_entity, ISysOperateService>
    {
        IHttpContextAccessor httpContextAccessor;


        private ISysOperateService _useService;

        public SiteSysOperateController(ISysOperateService useService
            , IApplicationContextAccessor _applicationContextAccessor
            , IHttpContextAccessor _httpContextAccessor) : base(useService, _applicationContextAccessor)
        {

            _useService = useService;
            httpContextAccessor = _httpContextAccessor;
        }
        /// <summary>
        /// 获取操作详细信息
        /// </summary>
        /// <param name="id">
        /// 主键ID int
        /// 拼接url即可 PS:sysmenus/sysoperate/1
        /// </param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "view")]
        public RestfulData<sys_operate_entity> Get(long id)
        {

            if (id <= 0)
            {
                return new RestfulData<sys_operate_entity>()
                {
                    code = -1,
                    msg = "参数错误"
                };
            }
            var model = Service.GetByOne(a => a.id == id);

            return new RestfulData<sys_operate_entity>()
            {
                code = 0,
                msg = "ok",
                data = model
            };
        }

        /// <summary>
        /// 添加操作
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///    "operate_name": "string",
        ///    "operate_code": "string",
        ///    "p_res":"int 关联资源ID",
        ///    "api_url":"string api 路径",
        ///    "is_menu": "int 0 数据接口 /1 菜单接口",
        ///    "status": "int  0 禁用 /1 启用",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        ///```
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "add")]
        [HttpPost("add")]
        public RestfulData<sys_operate_entity> Add([FromServices]ISysResourceService sysResourceService,[FromServices]ISysModuleService sysModuleService, JObject data)
        {

            if (data["operate_code"].MyToString() == "")
            {
                return new RestfulData<sys_operate_entity>(-1, "参数错误");
            }
            if (data["operate_name"].MyToString() == "")
            {
                return new RestfulData<sys_operate_entity>(-1, "参数错误");
            }
            if (data["status"].MyToInt() != 0 && data["status"].MyToInt() != 1)
            {
                return new RestfulData<sys_operate_entity>(-1, "参数错误");
            }
            if (data["p_res"].MyToInt() <= 0)
            {
                return new RestfulData<sys_operate_entity>(-1, "参数错误");
            }
            long p_res = data["p_res"].MyToInt();
            long p_module = 0;
            string p_module_code = "";
            string p_res_code = "";
            var resModel = sysResourceService.GetByOne(a => a.id == p_res);

            if (resModel != null)
            {
                p_res_code = resModel.res_code;
                p_module = resModel.p_module;

                if (p_module > 0)
                {
                    var moduleModel = sysModuleService.GetByOne(a => a.id == p_module);

                    if (moduleModel != null)
                    {
                        p_module_code = moduleModel.module_code;
                    }
                        
                }
            }

            
            if (data["is_sys"].MyToInt() != 0 && data["is_sys"].MyToInt() != 1)
            {
                return new RestfulData<sys_operate_entity>(-1, "参数错误");
            }
            var check = Service.GetByOne(a => a.operate_code.ToLower() == data["operate_code"].MyToString().ToLower()
               && a.p_res == p_res);
            if (check != null)
            {
                return new RestfulData<sys_operate_entity>(-1, "该资源下该操作代号已经存在");
            }
            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";

            var model = new sys_operate_entity();
            model.delete_status = Utils.DeleteEnum.UnDelete;
            model.operate_name = data["operate_name"].MyToString();
            model.operate_code = data["operate_code"].MyToString();
            model.p_module = p_module;
            model.p_module_code = p_module_code;

            model.p_res = data["p_res"].MyToLong();
            model.p_res_code = p_res_code;
            model.api_url = data["api_url"].MyToString();

            model.status = (StatusEnum)data["status"].MyToInt();
            model.enable_status = StatusEnum.Legal;
            model.description = data["description"].MyToString();
            model.n_sort = data["n_sort"].MyToInt();
            model.last_modifier_id = current_id;
            model.last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.last_modified_time = DateTime.Now;
            model.creator_id = current_id;
            model.creator_name = httpContextAccessor.HttpContext.User.Identity.Name;
            model.created_time = DateTime.Now;
            var result = Service.Insert(model);

            AddLog("sys_operate", Utils.LogsOperateTypeEnum.Add, $"添加{model.p_res_code}操作:{model.operate_name}");
            return new RestfulData<sys_operate_entity>()
            {
                code = 0,
                msg = "ok",
                data = result
            };
        }

        /// <summary>
        /// 操作列表
        /// </summary>
        /// <param name="data">
        /// {
        ///    "page_index": "int",
        ///    "page_size": "int",
        ///    "condition": {
        ///         "operate_name":"string",
        ///         "operate_code":"string",
        ///         "p_res":"int,父级资源ID",
        ///         "status":"int(-1 全部/0 禁用 /1 启用)"
        ///     }
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "list")]
        [HttpPost("list")]
        public RestfulData<object> List(JObject data)
        {
            List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>>();
            int page_size = data["page_size"].MyToInt();
            int pageNum = data["page_index"].MyToInt();
            if (page_size <= 0 || pageNum <= 0)
            {
                return new RestfulData<object>(-1, "参数错误");
            }
            long p_res = 0;




            string operate_name = "";
            string operate_code = "";
            int status = -1;
            if (data.ContainsKey("condition"))
            {

                p_res = data["condition"]["p_res"].MyToLong();
                operate_name = data["condition"]["operate_name"].MyToString();
                operate_code = data["condition"]["operate_code"].MyToString();
                status = data["condition"]["status"].MyToInt(-1);
            }
            //string json = data["conditions"].ToString();
            //var conditions = JsonHelper.ToObject<sys_operate_entity>(json);
            predicate.Add(a => a.delete_status == Utils.DeleteEnum.UnDelete);
            if (status != -1)
            {
                predicate.Add(a => a.status == (StatusEnum)status);
            }
            if (operate_name != "")
            {
                predicate.Add(a => a.operate_name.Contains(operate_name));
            }
            if (operate_code != "")
            {
                predicate.Add(a => a.operate_code.Contains(operate_code));
            }
            if (p_res > 0)
            {
                predicate.Add(a => a.p_res == p_res);
            }
            var result = Service.GetPageList(pageNum, page_size, "id asc", predicate);
            return new RestfulData<object>()

            {
                code = 0,
                msg = "ok",
                data = new
                {
                    list = result.list,
                    total = result.total
                }
            };
        }


        /// <summary>
        /// 改变菜单资源状态
        /// </summary>
        /// <param name="data">
        /// {
        ///    "id":"int 主键",
        ///    "status":"int  0禁用 /1 启用 "
        /// }
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "change_status")]
        [HttpPost("change_status")]
        public RestfulData ChangeStatus(JObject data)
        {
            
            long id = data["id"].MyToLong();
            int status = data["status"].MyToInt();
            if (id <= 0 || (status != 0 && status != 1))
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = Service.Modify(a => a.id == id, a => new sys_operate_entity()
            {
                status = (Utils.StatusEnum)status
            });
            if (flag > 0)
            {

                AddLog("sys_operate", Utils.LogsOperateTypeEnum.Update, $"修改操作状态:{EnumExt.GetDescriptionByEnum((Utils.StatusEnum)status)}");
                return new RestfulData()
                {
                    code = 0,
                    msg = "ok"
                };
            }
            else
            {
                return new RestfulData()
                {
                    code = 0,
                    msg = "修改失败"
                };
            }
        }

        /// <summary>
        /// 修改栏目信息
        /// </summary>
        /// <param name="id">更新主键标识</param>
        /// <param name="data">
        /// {
        ///    "operate_name": "string",
        ///    "api_url":"string api 路径",
        ///    "is_menu": "int 0 数据接口 /1 菜单接口",
        ///    "status": "int  0 禁用 /1 启用",
        ///    "n_sort": "int",
        ///    "description": "string"
        ///}
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "modify")]
        [HttpPost("update/{id}")]
        public RestfulData UpdateModule(long id, JObject data)
        {
            if (id <= 0)
            {
                return new RestfulData(-1, "参数错误");
            }

            int flag = 0;
            var login_user = httpContextAccessor.HttpContext.User.Claims;
            var current_id = login_user.Where(a => a.Type == System.Security.Claims.ClaimTypes.NameIdentifier).FirstOrDefault()?.Value ?? "";

            flag = Service.Modify(a => a.id == id, a => new sys_operate_entity()
            {
                operate_name = data["operate_name"].MyToString(),
                status = (StatusEnum)data["status"].MyToInt(),
                n_sort = data["api_url"].MyToInt(),
                api_url = data["uri"].MyToString(),
                description = data["description"].MyToString(),
                last_modifier_id = current_id.MyToString(),
                last_modifier_name = httpContextAccessor.HttpContext.User.Identity.Name,
                last_modified_time = DateTime.Now,
            });
            if (flag > 0)
            {
                AddLog("sys_operate", Utils.LogsOperateTypeEnum.Update, $"修改操作:{data["operate_name"].MyToString()}");
                return new RestfulData(0, "修改成功");
            }
            else
            {
                return new RestfulData(-1, "修改失败");
            }
        }
        /// <summary>
        /// 删除操作模块
        /// </summary>
        /// <param name="id">
        /// 主键ID
        /// 拼接url即可 PS:sysmenus/sysoperate/delete/1 ,POST BODY 不传
        /// </param>
        /// <returns></returns>
        [PermissionCheck(module_code = "sys_menus", resource_code = "sys_operate", operate_code = "delete")]
        [HttpPost("delete/{id}")]
        public RestfulData Delete(long id)
        {
            if (id <= 0)
            {
                return new RestfulData(0, "参数错误");
            }

            //int flag = Service.Modify(a => a.id == id, a => new sys_operate_entity()
            //{
            //    delete_status = DeleteEnum.Delete
            //});

            //删除关系
            var model = Service.GetByOne("id asc", a => a.id == id);
            Service.Delete<sys_role_operate_relation_entity>(a => a.res_code == model.p_res_code && a.module_code == model.p_module_code && a.operate_code == model.operate_code);
            //删除栏目
            int flag = Service.Delete(a => a.id == id);

            if (flag > 0)
            {

                AddLog("sys_operate", Utils.LogsOperateTypeEnum.Delete, $"删除操作:{id}");
                return new RestfulData(0, "删除成功");
            }
            else
            {
                return new RestfulData(-1, "删除失败");
            }
        }

        /// <summary>
        /// 获取操作键值列表
        /// </summary>
        /// <param name="delete_status">
        /// -1 全部
        /// 0 未删除
        /// 1 已删除
        /// </param>
        /// <returns></returns>
        [PermissionCheck]
        [HttpGet("data/list")]
        [HttpGet("data/list/{delete_status}")]
        public RestfulData<object> CatalogKeyValue(DeleteEnum delete_status=DeleteEnum.UnDelete)
        {
            List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>> predicate = new List<System.Linq.Expressions.Expression<Func<sys_operate_entity, bool>>>();
            if (delete_status != DeleteEnum.ALl)
            {
                predicate.Add(a => a.delete_status == delete_status);
            }
            var list = Service.GetList("n_sort asc", predicate);
            return new RestfulData<object>()
            {
                code = 0,
                msg = "ok",
                data = list.Select(a => new { a.id, chr_code = a.operate_code, chr_name = a.operate_name })
            };
        }

    }
}