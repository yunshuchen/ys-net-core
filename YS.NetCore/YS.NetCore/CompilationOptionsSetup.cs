﻿using YS.Utils.Mvc.Plugin;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Hosting;

namespace YS.NetCore
{
    public class CompilationOptionsSetup : ConfigureOptions<MvcRazorRuntimeCompilationOptions>
    {
        public CompilationOptionsSetup(IWebHostEnvironment webHostEnvironment) : base(optoins =>
        {
            if (webHostEnvironment.IsDevelopment())
            {
                optoins.FileProviders.Add(new DeveloperViewFileProvider(webHostEnvironment));
            }            
        })
        {
        }
    }
}
