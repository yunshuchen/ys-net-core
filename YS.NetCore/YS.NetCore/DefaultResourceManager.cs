
using YS.Utils.Mvc.Resource;
using System;

namespace YS.NetCore
{
    public class DefaultResourceManager : ResourceManager
    {
        const string LibraryPath = "~/lib";
        const string ScriptPath = "~/js";
        const string StylePath = "~/css";
        public override string Name { get; }

        protected override void InitScript(Func<string, ResourceHelper> script)
        {

            script("jQuery").Include($"{ScriptPath}/jquery.min.js", $"{ScriptPath}/jquery.js"); //.RequiredAtHead();
            ////script("LayUI").Include($"{LibraryPath}/layui/layui.js", $"{LibraryPath}/layui/layui.js").RequiredAtHead();
            ////script("xadmin").Include($"{ScriptPath}/xadmin.js", $"{ScriptPath}/xadmin.js").RequiredAtHead();
            ////script("Public").Include($"{ScriptPath}/public.js", $"{ScriptPath}/public.js").RequiredAtHead();
            ////script("Regs").Include($"{ScriptPath}/Regs.js", $"{ScriptPath}/Regs.js").RequiredAtHead();
            script("EWebEditor")
                .Include($"{LibraryPath}/ewebeditor/ewebeditor.js", $"{LibraryPath}/ewebeditor/ewebeditor.js");
                //.RequiredAtHead();
            script("UEditor").Include($"{LibraryPath}/UEditor/ueditor.config.js", $"{LibraryPath}/UEditor/ueditor.config.js")
                .Include($"{LibraryPath}/UEditor/ueditor.all.min.js", $"{LibraryPath}/UEditor/ueditor.all.min.js")
                .Include($"{LibraryPath}/UEditor/lang/zh-cn/zh-cn.js", $"{LibraryPath}/UEditor/lang/zh-cn/zh-cn.js");
                //.RequiredAtHead();


        }

        protected override void InitStyle(Func<string, ResourceHelper> style)
        {
      
            style("LayUI")
                .Include($"{LibraryPath}/layui/css/layui.css", $"{LibraryPath}/layui/css/layui.css").RequiredAtHead();
            style("xadmin")
                .Include("~/css/xadmin.css", "~/css/xadmin.css").RequiredAtHead();

        }
    }
}