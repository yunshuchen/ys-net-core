﻿$(window).scroll(function() {
    var winTop = $(window).scrollTop();
    var winHeight = $(window).height();
    var headerH = $("header").height();
    var bannerH = $(".banner").height();
    if (winTop > 600) {
        $("#top").show();
    } else {
        $("#top").hide();
    };
    if (winTop - headerH - bannerH > 0) {
        $(".right-flow").addClass("on");
    } else {
        $(".right-flow").removeClass("on");
    }
})


$("body").on("click", ".open-nav", function() {
    $(".nav-content").show();
}).on("click", ".close-nav", function() {
    $(".nav-content").hide();
}).on("click", ".nav-content .sub-nav-tit", function(event) {
    event.stopPropagation();
    if ($(this).hasClass("on")) {
        $(this).removeClass("on").siblings(".side-nav-sub").stop().slideUp(300);
    } else {
        $(".nav-content .sub-nav-tit").removeClass("on");
        $(".nav-content li .side-nav-sub").stop().slideUp(300);
        $(this).addClass("on").siblings(".side-nav-sub").stop().slideDown(300);
    }
}).on("click", "#top", function() {
    $("body,html").animate({
        "scrollTop": 0,
    }, 300);
}).on("click", ".open-site", function() {
    if ($(window).width() < 1200) {
        return false;
    } else {
        $("#site-map,.cover").show();
    }
}).on("click", ".close-site", function() {
    $("#site-map,.cover").hide();
}).on("click", ".building", function() {
    if ($(window).width() < 1200) {
        return false;
    } else {
        $("#building-content,.cover").show();
    }
}).on("click", ".close-site", function() {
    $("#building-content,.cover").hide();
})

$("nav li").each(function() {
    var sb = $(this).find(".sub-nav"),
        n = sb.length;
    if (n > 0) {
        $(this).hover(function() {
            sb.stop().show();
            $(".nav-bg").stop().show();
        }, function() {
            sb.stop().hide();
            $(".nav-bg").stop().hide();
        })
    }
})

$(".sub-nav").each(function() {
    var x = $(this);
    $(this).find(".sub-nav-right-item").each(function(e) {
        var y = $(this);
        y.mouseenter(function() {
            y.addClass("on").siblings().removeClass("on");
            x.find(".sub-nav-left-item").hide().eq(e).show();
        })
    })
})



function getBodySrollTop() {
    return document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
}


//用firefox变量表示火狐代理
var firefox = navigator.userAgent.indexOf('Firefox') != -1;

function MouseWheel(e) { //阻止事件冒泡和默认行为的完整兼容性代码
    e = e || window.event;
    if (e.stopPropagation) { //这是取消冒泡
        e.stopPropagation();
    } else {
        e.cancelBubble = true;
    };
    if (e.preventDefault) { //这是取消默认行为，要弄清楚取消默认行为和冒泡不是一回事
        e.preventDefault();
    } else {
        e.returnValue = false;
    };
}




