function fixBanner() {
    var winh = $(window).height(),
        winw = $(window).width(),
        headerh = $("header").height();
    if (winh - headerh < 750 && winw > 1230) {
        $(".banner").height(winh - headerh);
    } else {
        $(".banner").attr("style", "");
    }
}
fixBanner();
$(window).resize(function() {
    fixBanner();
})

$(function() {

    //banner图   
    var swiper1 = new Swiper('.banner .swiper-container', {
        pagination: {
            el: '.banner .swiper-pagination',
            clickable: true,
        },
        watchOverflow: true,
        effect: 'fade',
        fadeEffect: {
            crossFade: true,
        },
        speed: 1600,
        autoplay: {
            disableOnInteraction: false,
            delay: 6000,
        },
        loop: true,
        updateOnImagesReady: true,
    });

    //滚动新闻  
    var swiper2 = new Swiper('.banner-news .swiper-container', {
        loop: true,
        direction: 'vertical',
        on: {
            resize: function() {
                this.update();
            }
        },
        autoplay: {
            disableOnInteraction: false,
            delay: 4000,
        },
        slidesPerView: 1,
        speed: 600,
    });

    //展览  
    var swiper3 = new Swiper('.exhibition-swiper .swiper-container', {
        watchOverflow: true,
        speed: 800,
        // loop: true,
        // loopAdditionalSlides: 3,
        // roundLengths: true,
        autoplay: {
            disableOnInteraction: false,
            delay: 4000,
        },
        breakpoints: {
            1230: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        },
        navigation: {
            nextEl: '.exhibition-swiper-next',
            prevEl: '.exhibition-swiper-prev',
        },
        updateOnImagesReady: true,
        on: {
            resize: function() {
                this.update();
            }
        },
    });
    $(".exhibition-swiper").on({
        "mouseenter": function() {
            swiper3.autoplay.stop();
        },
        "mouseleave": function() {
            swiper3.autoplay.start();
        }
    })


    //活动  
    var swiper4 = new Swiper('.activity-swiper .swiper-container', {
        watchOverflow: true,
        speed: 600,
        slidesPerView: 1,
        spaceBetween: 0,
        on: {
            resize: function() {
                this.update();
            }
        },
        autoplay: {
            disableOnInteraction: false,
            delay: 4000,
        },
        breakpoints: {
            1230: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 30,
            }
        },
        updateOnImagesReady: true,
        // navigation: {
        //     nextEl: '.exhibition-swiper-next',
        //     prevEl: '.exhibition-swiper-prev',
        // },
        //loopAdditionalSlides: 3,
    });
    $(".activity-swiper").on({
        "mouseenter": function() {
            swiper4.autoplay.stop();
        },
        "mouseleave": function() {
            swiper4.autoplay.start();
        }
    })



})