﻿//js  string to byte
function stringToBytes(str) {

    var ch, st, re = [];
    for (var i = 0; i < str.length; i++) {
        ch = str.charCodeAt(i);  // get char
        st = [];                 // set up "stack"
        do {
            st.push(ch & 0xFF);  // push byte to stack
            ch = ch >> 8;          // shift value down by 1 byte
        }
        while (ch);
        // add stack contents to result
        // done because chars have "wrong" endianness
        re = re.concat(st.reverse());
    }
    // return an array of bytes
    return re;
}
/**
*String To Byte
*/
function stringToByte(str) {
    var bytes = new Array();
    var len, c;
    len = str.length;
    for (var i = 0; i < len; i++) {
        c = str.charCodeAt(i);
        if (c >= 0x010000 && c <= 0x10FFFF) {
            bytes.push(((c >> 18) & 0x07) | 0xF0);
            bytes.push(((c >> 12) & 0x3F) | 0x80);
            bytes.push(((c >> 6) & 0x3F) | 0x80);
            bytes.push((c & 0x3F) | 0x80);
        } else if (c >= 0x000800 && c <= 0x00FFFF) {
            bytes.push(((c >> 12) & 0x0F) | 0xE0);
            bytes.push(((c >> 6) & 0x3F) | 0x80);
            bytes.push((c & 0x3F) | 0x80);
        } else if (c >= 0x000080 && c <= 0x0007FF) {
            bytes.push(((c >> 6) & 0x1F) | 0xC0);
            bytes.push((c & 0x3F) | 0x80);
        } else {
            bytes.push(c & 0xFF);
        }
    }
    return bytes;


}

/**
*byte To String
*/
function byteToString(arr) {
    if (typeof arr === 'string') {
        return arr;
    }
    var str = '',
    _arr = arr;
    for (var i = 0; i < _arr.length; i++) {
        var one = _arr[i].toString(2),
            v = one.match(/^1+?(?=0)/);
        if (v && one.length == 8) {
            var bytesLength = v[0].length;
            var store = _arr[i].toString(2).slice(7 - bytesLength);
            for (var st = 1; st < bytesLength; st++) {
                store += _arr[st + i].toString(2).slice(2);
            }
            str += String.fromCharCode(parseInt(store, 2));
            i += bytesLength - 1;
        } else {
            str += String.fromCharCode(_arr[i]);
        }
    }
    return str;
}
var writeUTF = function (str, isGetBytes) {
    var back = [];
    var byteSize = 0;
    for (var i = 0; i < str.length; i++) {
        var code = str.charCodeAt(i);
        if (0x00 <= code && code <= 0x7f) {
            byteSize += 1;
            back.push(code);
        } else if (0x80 <= code && code <= 0x7ff) {
            byteSize += 2;
            back.push((192 | (31 & (code >> 6))));
            back.push((128 | (63 & code)))
        } else if ((0x800 <= code && code <= 0xd7ff)
        || (0xe000 <= code && code <= 0xffff)) {
            byteSize += 3;
            back.push((224 | (15 & (code >> 12))));
            back.push((128 | (63 & (code >> 6))));
            back.push((128 | (63 & code)))
        }
    }
    for (i = 0; i < back.length; i++) {
        back[i] &= 0xff;
    }
    if (isGetBytes) {
        return back
    }
    if (byteSize <= 0xff) {
        return [0, byteSize].concat(back);
    } else {
        return [byteSize >> 8, byteSize & 0xff].concat(back);
    }
}


var readUTF = function (arr) {
    if (typeof arr === 'string') {
        return arr;
    }
    var UTF = '', _arr = arr;
    for (var i = 0; i < _arr.length; i++) {
        var one = _arr[i].toString(2),
        v = one.match(/^1+?(?=0)/);
        if (v && one.length == 8) {
            var bytesLength = v[0].length;
            var store = _arr[i].toString(2).slice(7 - bytesLength);
            for (var st = 1; st < bytesLength; st++) {
                store += _arr[st + i].toString(2).slice(2)
            }
            UTF += String.fromCharCode(parseInt(store, 2));
            i += bytesLength - 1
        } else {
            UTF += String.fromCharCode(_arr[i])
        }
    }
    return UTF
}



/**
*string转换Hex
*str String 需要转换的
*@return Hex16进制字符串
*/
var toUTF8Hex = function (str) {
    var charBuf = writeUTF(str, true);
    var re = '';
    for (var i = 0; i < charBuf.length; i++) {
        var x = (charBuf[i] & 0xFF).toString(16);
        if (x.length === 1) {
            x = '0' + x;
        }
        re += x;
    }
    return re;
}

/**
*Hex转换string
*str hex String 需要转换的16进制
*@return string字符串
*/
var utf8HexToStr = function (str) {
    var buf = [];
    for (var i = 0; i < str.length; i += 2) {
        buf.push(parseInt(str.substring(i, i + 2), 16));
    }
    return readUTF(buf);
}
/**
*加密密字符串
*@str 需要加密的字符串
*@key 加密的key
*return加密后的字符串Hex
*/
function EncryptStr(encrypt_input, encrypt_key) {
    var key = encrypt_key.split(''); //Can be any chars, and any size array
    console.log("原文==>", encrypt_input);
    var input = toUTF8Hex(encrypt_input);
    console.log("原文==>Hex", input);
    var output = [];
    for (var i = 0; i < input.length; i++) {
        var charCode = input.charCodeAt(i) ^ key[i % key.length].charCodeAt(0);
        output.push(String.fromCharCode(charCode));
    }
    var decrypt = output.join("");
    console.log("密文==>", decrypt);
    var decryptHex = toUTF8Hex(output.join(""));
    console.log("密文==>Hex", decryptHex);
    return decryptHex;
}

/**
*解密密字符串
*@str 需要解密的Hex
*@key 解密的key
*return解密后的字符串
*/
function DecryptStr(decrypt_Hex, encrypt_key) {
    var key = encrypt_key.split(''); //Can be any chars, and any size array
    console.log("密文==>Hex", decrypt_Hex);

    var input = utf8HexToStr(decrypt_Hex);
    console.log("密文==>", input);
 
    var output = [];

    for (var i = 0; i < input.length; i++) {
        var charCode = input.charCodeAt(i) ^ key[i % key.length].charCodeAt(0);
        output.push(String.fromCharCode(charCode));
    }
    var decryptHex = output.join("");
    console.log("原文==>HEX", decryptHex);
    var decrypt = utf8HexToStr(decryptHex);
    console.log("原文==>", decrypt);

    return decrypt;
}
/**
* 1:成功 -1:错误 0:等待  2:提示
*/
function Message(type, content) {

    if (type == 1)
        return layer.msg(content, { time: 5000, icon: 1 });
    else if (type == -1)
        return layer.msg(content, { time: 5000, icon: 2 });
    else if (type == 0) {
        //自定页
        return layer.load(3, { shade: 0.2 }); //添加laoding,0-2两种方式
    }
    else {
        return layer.msg(content, { time: 5000, icon: 5 });
    }
}
var GridView = function (url, containerId, params, callBack) {

    var obj = new Object();
    obj.page_index = 1;
    obj.page_size = 20;
    obj.url = url;
    obj.params = params;
    obj.fn = callBack;

    obj.LoadData = function (func) {
        layui.use(['layer', 'element', 'jquery'], function () {
            var manager = Message(0, '正在加载数据,请稍候...');
            if (obj.params == null || obj.params == "") {
                obj.params = {};
            }

            $(obj.params).attr("page_index", obj.page_index);
            $(obj.params).attr("page_size", obj.page_size);

            $.ajax({
                url: obj.url,
                cache: false,
                async: true,
                dataType: 'html',
                data: obj.params,
                type: 'post',
                success: function (data) {
                    document.getElementById(containerId).innerHTML = data;
                    //分页事件绑定
                    var pagerObj = $("#" + containerId).find("div.PageBtns");
                    var pageTotal = $(pagerObj).attr("page_count");//总页数

                    //首页
                    $(pagerObj).find("a[tag='frist']").bind("click", function () {
                        if (obj.page_index != 1) {
                            obj.page_index = 1;
                            obj.LoadData();
                        } else {
                            $(pagerObj).find("a[tag='first']").unbind("click");
                        }
                    });

                    //上一页
                    if (parseInt(obj.page_index) > 1) {
                        $(pagerObj).find("a[tag='prev']").bind("click", function () {
                            obj.page_index -= 1;
                            obj.LoadData();
                        });
                    } else {
                        $(pagerObj).find("a[tag='prev']").unbind("click");
                    }

                    //下一页
                    if (parseInt(obj.page_index) < parseInt(pageTotal)) {
                        $(pagerObj).find("a[tag='next']").bind("click", function () {
                            obj.page_index += 1;
                            obj.LoadData();
                        });
                    } else {
                        $(pagerObj).find("a[tag='next']").unbind("click");
                    }


                    $(pagerObj).find("a[tag='number']").bind("click", function () {
                        obj.page_index = parseInt($(this).html());
                        obj.LoadData();
                    });


                    //未页
                    $(pagerObj).find("a[tag='last']").bind("click", function () {
                        if (parseInt(obj.page_index) < parseInt(pageTotal)) {
                            obj.page_index = $(this).attr("val");
                            obj.LoadData();
                        } else {
                            $(pagerObj).find("a[tag='last']").unbind("click");
                        }
                    });
                    //分页大小
                    $(pagerObj).find("a[tag='page_size']").bind("click", function () {
                        obj.page_size = $(this).attr("val");
                        obj.LoadData();
                    });

                    //重设iframe高度
                    ///reSetIframeHeight();
                    layer.close(manager);
                    if (obj.fn != null) {
                        obj.fn();
                    }

                },
                error: function (e) {
                    layer.close(manager);
                },
                complete: function () {
                    if (func) {
                        func.call();
                    }
                    if (obj.fn) {
                        obj.fn.call();
                    }
                }
            });
        });

    }

    return obj;
}

$(document).on("click", '[data-toggle="searchModal"]',
    function (a) {
        a.preventDefault();
        window.searchType = $(this).data("type") == undefined ? "0" : $(this).data("type");
        window.searchFun = $(this).data("fun") == undefined ? "" : $(this).data("fun");

        window.target_dom = $(this).attr("target-dom") == undefined ? "" : $(this).attr("target-dom");
        var obj = this;
        var d = $(obj);
        var e = d.attr("data-remote") || d[0].href;
        if (e.indexOf("?") != -1) {
            e = e + "&ajax=1";
        } else {
            e = e + "?ajax=1";
        }
        window.open(e, 'newwindow', 'width=' + (window.screen.availWidth - 10) + ',height=' + (window.screen.availHeight - 30) + ',top=0,left=0,toolbar=no,menubar=no,scrollbars=no, resizable=no,location=no, status=no')

        return false;
    });


function WebEditor(id) {//createCar工厂
    var obj = {};//或obj = {} 原材料阶段
    var dom = $("#editor_container__" + id);
    obj.editor_id = dom.data("editor_id");
    obj.editor_type = dom.data("editor_type");
    obj.editor_input_id = dom.data("editor_input_id");
    obj.editor = null;

    if (obj.editor_type == "UEditor") {
        obj.editor = UE.getEditor(obj.editor_input_id);
        obj.editor.options.Exrends = { "data_id": 0, "data_type": "content", "chr_title": "" };
    }

    obj.GetContentHtml = function () {
        if (obj.editor_type == "EWebEditor") {
            if (!obj.editor) {
                obj.editor = EWEBEDITOR.Instances[this.editor_input_id]; //来访问已创建编辑器的接口对象。
            }
            var result = obj.editor.getHTML();

            return result;
        } else {
            var result = obj.editor.getContent();
            return result;
        }
    };
    obj.SetContentHtml = function (html) {
        if (obj.editor_type == "EWebEditor") {
            if (!obj.editor) {
                obj.editor = EWEBEDITOR.Instances[this.editor_input_id]; //来访问已创建编辑器的接口对象。
            }
             obj.editor.setHTML(html);
        } else {
             obj.editor.setContent(html);
        }
    };
    obj.hasContents = function () {
        if (obj.editor_type == "EWebEditor") {
            if (!obj.editor) {
                obj.editor = EWEBEDITOR.Instances[this.editor_input_id]; //来访问已创建编辑器的接口对象。
            }
            var text = obj.editor.getText();
            return text.replace(/\s+/g, '').length > 0
        } else {
            return obj.editorhasContents();
        }
    }

    return obj;//输出产品
}