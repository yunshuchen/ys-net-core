using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace YS.NetCore
{
    public class Program
    {
        public static void Main(string[] args)
        {   // 设置读取指定位置的nlog.config文件
            NLogBuilder.ConfigureNLog("NLog.Config");
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var config = new ConfigurationBuilder()
      .SetBasePath(Directory.GetCurrentDirectory())
      .AddJsonFile("hosting.json", optional: false, reloadOnChange: false)
      .Build();
            string urls = config.GetValue<string>("server.urls");
         return   Host.CreateDefaultBuilder(args)

                    .UseServiceProviderFactory(new AutofacServiceProviderFactory())
                        .ConfigureWebHostDefaults(webBuilder =>
                        {
                            webBuilder.UseUrls(urls);
                        webBuilder.UseKestrel(configure =>
                  
                            {
                                //configure.AllowSynchronousIO = false;
                            });
                            webBuilder.UseStartup<Startup>();

                            webBuilder.ConfigureLogging((hostingContext, logging) =>
                            {
                                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                                logging.AddConsole();
                                logging.AddDebug();
                                logging.AddEventSourceLogger();

                                logging.AddFilter("Microsoft.AspNetCore.SignalR", LogLevel.Debug);
                                logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
                            });
                        }).UseNLog();// 配置使用NLog
        }
    }
}
