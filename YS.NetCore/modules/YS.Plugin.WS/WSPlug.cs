﻿using YS.Net;
using YS.Plugin.WS.Hubs;
using YS.Plugin.WS.Models;
using YS.Utils.Mvc.Resource;
using YS.Utils.Mvc.Route;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Plugin.WS
{
    public class WSPlug : PluginBase
    {
        public override IEnumerable<AdminMenu> AdminMenu()
        {
            return new List<AdminMenu>();
        }


        public override void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR()
                    .AddMessagePackProtocol();

            services.AddSingleton<StockTicker>();

            services.AddCors(options => options.AddPolicy("SignalR",
           builder =>
           {
               builder.AllowAnyMethod()
                      .AllowAnyHeader()
                      .SetIsOriginAllowed(str => true)
                      .AllowCredentials();
           }));
        }
        public override void ConfigureApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<StockTickerHub>("/ws/stocks",op=> {
                    op.Transports = HttpTransportType.WebSockets | HttpTransportType.LongPolling;
                });
                endpoints.MapHub<ChatHub>("/ws/chat");

            });
         
            app.UseCors("SignalR");
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapHub<StockTickerHub>("/stocks", options =>
            //    {
            //        options.Transports =
            //            HttpTransportType.WebSockets |
            //            HttpTransportType.LongPolling;
            //    });
            //});
        }
        public override IEnumerable<RouteDescriptor> RegistRoute()
        {
            return new List<RouteDescriptor>();
        }

        protected override void InitScript(Func<string, ResourceHelper> script)
        {
           
        }
        protected override void InitStyle(Func<string, ResourceHelper> style)
        {
            
        }



    }
}
