﻿using Autofac;
using YS.Net;
using YS.Net.Quartz;
using YS.Net.Services;
using YS.Utils.DI;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace YS.Plugin.QuartzJob.Jobs
{
    [CustomJob(CronExpression: "0/5 * * * * ? ", Description:"定时执行")]
    [ClassTypeDI]
    [SingletonTypeDI]
    public class CheckRentJob : IJob
    {
        private readonly ILogger<CheckRentJob> _logger;
        private readonly IServiceScopeFactory serviceScopeFactory;
        //private readonly IContainer AutofacContainer;
        public CheckRentJob(ILogger<CheckRentJob> logger, IServiceScopeFactory _serviceScopeFactory/*, ILifetimeScope lifetimeScope*/)
        {
            
   
            _logger = logger;
            serviceScopeFactory = _serviceScopeFactory;
        }

        public Task Execute(IJobExecutionContext context)
        {
            //var abc = AutofacContainer.Resolve<ISiteSurveyService>();

            using (var scope = serviceScopeFactory.CreateScope())
            {

            }

            Console.WriteLine($"{DateTime.Now.ToString()}检查完毕");
            return Task.CompletedTask;
        }

    }
}
