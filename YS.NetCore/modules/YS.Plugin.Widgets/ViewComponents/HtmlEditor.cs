﻿using YS.Net;
using YS.Net.Common;
using YS.Net.Setting;
using YS.Plugin.Widgets.Models;
using YS.Utils;
using YS.Utils.Extend;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Plugin.Widgets.ViewComponents
{
    [ViewComponent(Name = "CMSHtmlEditor")]
    public class HtmlEditor : ViewComponent
    {


        private readonly ISiteFileProvider SiteFile;
        private readonly IApplicationContextAccessor applicationContextAccessor;
        private readonly IApplicationSettingService applicationSettingService;
        public HtmlEditor(ISiteFileProvider _SiteFile
            ,IApplicationSettingService _applicationSettingService
            , IApplicationContextAccessor _applicationContextAccessor) {
            SiteFile = _SiteFile;
            applicationContextAccessor = _applicationContextAccessor;
            applicationSettingService = _applicationSettingService;

             
        }

        /// <summary>
        /// 生成html编辑器
        /// </summary>
        /// <param name="html_content"></param>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync(string html_content,string id,string data_id)
        {
            //string TplBasePath = SiteFile.TemplateRelativeBasePath;
            string tpl_name = applicationContextAccessor.Current.SiteTplName;

            string TplBasePath = $"{tpl_name}/modules/{this.GetType().Assembly.GetName().Name}/Components/{this.ViewComponentContext.ViewComponentDescriptor.ShortName}/Default.cshtml";


            string EWebEditor = applicationSettingService.Get("EditorType", "EWebEditor");
   

            return View(ViewComponentExtensions.GetViewPath(this, TplBasePath), new EditorOpetions()
            {
                data_id = data_id,
                editor_type= EWebEditor,
                editor_licence = "",
                html_content = html_content,
                id = id
            }) ;


        }
    }
}
