﻿using YS.Net;
using YS.Net.Common;
using YS.Utils;
using YS.Utils.Mvc;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Plugin.Widgets.ViewComponents
{
    [ViewComponent(Name = "CMSPagination")]
    public class Pagination : ViewComponent
    {


        private readonly ISiteFileProvider SiteFile;
        private readonly IApplicationContextAccessor applicationContextAccessor;
        public Pagination(ISiteFileProvider _SiteFile, IApplicationContextAccessor _applicationContextAccessor) {
            SiteFile = _SiteFile;
            applicationContextAccessor = _applicationContextAccessor;
        }
        /// <summary>
        /// 分页数据
        /// </summary>
        /// <param name="UrlFomart">分页url格式</param>
        /// <param name="PageSize">页大小</param>
        /// <param name="IPage">页码</param>
        /// <param name="Total">总计记录数</param>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync(
            string UrlFomart
            , int PageSize
            , int IPage
            , int Total)
        {
            //string TplBasePath = SiteFile.TemplateRelativeBasePath;
            string tpl_name = applicationContextAccessor.Current.SiteTplName;

            string TplBasePath = $"{tpl_name}/modules/{this.GetType().Assembly.GetName().Name}/Components/{this.ViewComponentContext.ViewComponentDescriptor.ShortName}/Default.cshtml";


            var result = new PageOf<object>()
            {
                page_index = IPage,
                PagePostUrl = UrlFomart,
                page_size = PageSize,
                total = Total
            };

            return View(ViewComponentExtensions.GetViewPath(this, TplBasePath), result);


        }
    }
}
