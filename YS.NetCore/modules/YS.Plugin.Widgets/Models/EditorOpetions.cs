﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Plugin.Widgets.Models
{
    public class EditorOpetions
    {

        /// <summary>
        /// 编辑器ID
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// 编辑器初始化内容
        /// </summary>
        public string html_content { get; set; }

        public string editor_type { get; set; }
        /// <summary>
        /// 编辑器授权文件
        /// </summary>
        public string editor_licence { get; set; }


        public string data_id { get; set; }
    }
}
