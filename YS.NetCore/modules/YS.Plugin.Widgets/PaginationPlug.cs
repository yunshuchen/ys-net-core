﻿using YS.Net;
using YS.Utils.Mvc.Resource;
using YS.Utils.Mvc.Route;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Plugin.Widgets
{
    public class PaginationPlug : PluginBase
    {
        public override IEnumerable<AdminMenu> AdminMenu()
        {
            return new List<AdminMenu>();
        }


        public override void ConfigureServices(IServiceCollection serviceCollection)
        {
 
        }
        public override void ConfigureApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //base.ConfigureApplication(app, env);    
        }
        public override IEnumerable<RouteDescriptor> RegistRoute()
        {
            return new List<RouteDescriptor>();
        }

        protected override void InitScript(Func<string, ResourceHelper> script)
        {
           
        }
        protected override void InitStyle(Func<string, ResourceHelper> style)
        {
            
        }



    }
}
