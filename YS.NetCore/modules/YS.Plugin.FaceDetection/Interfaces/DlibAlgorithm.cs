﻿
using FaceRecognitionDotNet;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace YS.Plugin.FaceDetection.Interfaces
{
    public class DlibAlgorithm : IDetectFace
    {
        private readonly FaceRecognition _faceRecognition;
        public DlibAlgorithm(Utils.Mvc.Plugin.IPluginLoader loader)
        {
            var type = this.GetType();
            string currentDirectory = Path.GetDirectoryName(type.Assembly.Location);

            _faceRecognition = FaceRecognition.Create(Path.Combine(currentDirectory,loader.PluginResurceFolderName(), "FaceDetection", "Classifiers", "dlib-model"));

        }

        public bool CompareFace(string knownFaceEncoding, string faceEncodingToCheck, double tolerance = 0.6)
        {
            var knownImage = FaceRecognitionDotNet.FaceRecognition.LoadImage(YS.Utils.Extend.ExtImage.FileToBitmap(knownFaceEncoding));
            var unknownImage = FaceRecognitionDotNet.FaceRecognition.LoadImage(YS.Utils.Extend.ExtImage.FileToBitmap(faceEncodingToCheck));

            var known_encoding = _faceRecognition.FaceEncodings(knownImage);
            var unknown_encoding = _faceRecognition.FaceEncodings(unknownImage);

            if (unknown_encoding.Count() <= 0 || known_encoding.Count() <= 0)
            {
                return false;
            }
            var result1 = FaceRecognitionDotNet.FaceRecognition.CompareFace(known_encoding.FirstOrDefault(), unknown_encoding.FirstOrDefault());
            return result1;

        }



    }
}
