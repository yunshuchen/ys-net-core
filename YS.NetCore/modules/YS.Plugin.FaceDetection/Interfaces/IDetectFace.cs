﻿using FaceRecognitionDotNet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace YS.Plugin.FaceDetection.Interfaces
{
    public interface IDetectFace
    {
        public bool CompareFace(string knownFaceEncoding, string faceEncodingToCheck, double tolerance = 0.6);
        
    }
}
