﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using YS.Utils.Cache;
using YS.Utils.Encrypt;
using YS.Net.Setting;
using Microsoft.Extensions.Localization;
using YS.Net.Mvc;
using YS.Net.Models;
using YS.Net.Services;
using YS.Net;
using YS.Utils;
using Microsoft.AspNetCore.Authorization;
using YS.Utils.Mvc;
using Newtonsoft.Json.Linq;
using YS.Net.Common;
using System.IO;

namespace YS.Plugin.FaceDetection.Controllers
{
    [Microsoft.AspNetCore.Mvc.ApiVersion("1.0")]
    [CommentControllerAttribute("插件-人脸比对")]
    [Route("api/v{version:apiVersion}/plugins/[controller]")]
    [ApiController]
    public class FaceDetectionController : Net.Mvc.ApiBasicController<sys_account, ISysAccountService>
    {


        public FaceDetectionController(
            ISysAccountService userService
            , IApplicationContextAccessor _httpContextAccessor) : base(userService, _httpContextAccessor)
        {
        }


        /// <summary>
        /// 人脸比对接口
        /// </summary>
        /// <param name="data">
        /// ```json
        /// {
        ///     "base_image_url":"原始图片路径",
        ///     "check_image_url":"比对的图片路径"
        /// }
        /// ```
        /// </param>
        /// <returns>
        /// </returns>
        [AllowAnonymous]
        [HttpPost("ToCheck")]
        public RestfulData ToCheck([FromServices] ISiteFileProvider siteFileProvider, [FromServices] Interfaces.IDetectFace detectFace
            , JObject data)
        {
            string base_image_url = data["base_image_url"].MyToString();
            string check_image_url = data["check_image_url"].MyToString();
            if (base_image_url.StartsWith("/"))
            {
                base_image_url = base_image_url.Substring(1);
            }
            if (check_image_url.StartsWith("/"))
            {
                check_image_url = check_image_url.Substring(1);
            }
            base_image_url = base_image_url.Replace('/', System.IO.Path.DirectorySeparatorChar);
            check_image_url = check_image_url.Replace('/', System.IO.Path.DirectorySeparatorChar);
            base_image_url = System.IO.Path.Combine(Directory.GetCurrentDirectory(), base_image_url);
            check_image_url = System.IO.Path.Combine(Directory.GetCurrentDirectory(), check_image_url);
            var result = detectFace.CompareFace(base_image_url, check_image_url);
            return new RestfulData<object>(0, "ok", new { result });
        }
     
    }



}
