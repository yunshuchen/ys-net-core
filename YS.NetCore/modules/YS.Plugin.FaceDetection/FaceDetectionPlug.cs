﻿using YS.Net;
using YS.Plugin.FaceDetection.Interfaces;
using YS.Utils.Mvc.Resource;
using YS.Utils.Mvc.Route;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.IO;

namespace YS.Plugin.FaceDetection
{
    public class FaceDetectionPlug : PluginBase
    {
        public override IEnumerable<AdminMenu> AdminMenu()
        {
            return new List<AdminMenu>();
        }


        public override void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.TryAddSingleton<IDetectFace, DlibAlgorithm>();

            serviceCollection.ConfigureSwaggerGen(c => {
                var basePath = Path.GetDirectoryName(typeof(Program).Assembly.Location);//获取应用程序所在目录（绝对，不受工作目录影响，建议采用此方法获取路径）
                var xmlPath = Path.Combine(basePath, "YS.Plugin.FaceDetection.xml");
                c.IncludeXmlComments(xmlPath);
            });
        }
        public override void ConfigureApplication(IApplicationBuilder app, IWebHostEnvironment env)
        {

        }
        public override IEnumerable<RouteDescriptor> RegistRoute()
        {
            

            yield return new RouteDescriptor
            {
                RouteName = "FaceDetection",
                Template = "api/v{version:apiVersion}/plugins/facedetection/{action}",
                Defaults = new { controller = "facedetection", action = "Index" },
                Priority = 11
            };
        }

        protected override void InitScript(Func<string, ResourceHelper> script)
        {

        }
        protected override void InitStyle(Func<string, ResourceHelper> style)
        {

        }



    }
}
