﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using YS.Utils.Extend;

namespace YS.Plugin.RabbitMQ.BackgroundServices
{
    public class MqProducerService : BackgroundService
    {

        private readonly string QueueName = "task_queue";
        private readonly IConnection connection;
        public IServiceScopeFactory serviceScopeFactory;
        public MqProducerService(IServiceScopeFactory _serviceScopeFactory)
        {
            serviceScopeFactory = _serviceScopeFactory;
            //创建连接工厂
            var factory = new ConnectionFactory() { HostName = "127.0.0.1", UserName = "dev", Password = "dev123321" };
            //创建连接 
            connection = factory.CreateConnection();
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Factory.StartNew(async () =>
              {
                //创建通道
                using (var channel = connection.CreateModel())
                {
                      while (!stoppingToken.IsCancellationRequested)
                      {
                        //声明一个队列
                        channel.QueueDeclare(queue: QueueName,//队列名称
                                          durable: true,//否持久化。true 则为持久化，队列元数据落盘，重启不丢失。
                                          exclusive: false,//是否排他的。true 则仅对首次声明它的 Connection 可见，并在连接断开时自动删除。也就是说，一个 Connection 的多个 Channel 都是可见的；首次 指一个连接如果已经声明了排他队列，则不允许其他连接建立同名的排他队列。注意：即使声明为持久化，只要连接断开，就会自动删除
                                          autoDelete: false,//是否自动删除。含义与交换器的参数类似，至少有一个消费者连接到这个队列，之后全部断开，才会自动删除，
                                          arguments: null);//更多参数，诸如消息存活时间、过期时间、最大长度、死信队列相关参数、优先级等等。

                        var message = $"测试--{DateTime.Now.ToUnix()}";
                          var body = Encoding.UTF8.GetBytes(message);

                          var properties = channel.CreateBasicProperties();
                          properties.Persistent = true;

                        //发布消息
                        channel.BasicPublish(exchange: "",
                                               routingKey: QueueName,
                                               basicProperties: properties,
                                               body: body);
                          Console.WriteLine(" [producer] Sent {0}", message);
                      }
                    //删除队列
                    channel.QueueDelete(queue: QueueName,
                          ifUnused: false,
                          ifEmpty: false//是否在队列为空（没有消息积压）时才删除
                          );

                      await Task.Delay(1000);
                  }

              });

            connection.Close();
        }
    }
}
