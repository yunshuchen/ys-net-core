﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using YS.Utils.Extend;

namespace YS.Plugin.RabbitMQ.BackgroundServices
{
    public class MqConsumerService : IHostedService
    {

        private readonly string QueueName = "task_queue";
        private readonly IConnection connection;
        public IServiceScopeFactory serviceScopeFactory;
        private readonly IModel channel;
        public MqConsumerService(IServiceScopeFactory _serviceScopeFactory)
        {
            serviceScopeFactory = _serviceScopeFactory;
            //创建连接工厂
            var factory = new ConnectionFactory() { HostName = "127.0.0.1", UserName = "dev", Password = "dev123321" };
            //创建连接 
            connection = factory.CreateConnection();

            channel = connection.CreateModel();

        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Register();
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.connection.Close();
            return Task.CompletedTask;
        }

        private void Register()
        {

            //while (!stoppingToken.IsCancellationRequested)
            //{
            //声明一个队列
            channel.QueueDeclare(queue: QueueName,//队列名称
                              durable: true,//否持久化。true 则为持久化，队列元数据落盘，重启不丢失。
                              exclusive: false,//是否排他的。true 则仅对首次声明它的 Connection 可见，并在连接断开时自动删除。也就是说，一个 Connection 的多个 Channel 都是可见的；首次 指一个连接如果已经声明了排他队列，则不允许其他连接建立同名的排他队列。注意：即使声明为持久化，只要连接断开，就会自动删除
                              autoDelete: false,//是否自动删除。含义与交换器的参数类似，至少有一个消费者连接到这个队列，之后全部断开，才会自动删除，
                              arguments: null);//更多参数，诸如消息存活时间、过期时间、最大长度、死信队列相关参数、优先级等等。

            channel.QueueDeclare(queue: QueueName,
                     durable: true,
                     exclusive: false,
                     autoDelete: false,
                     arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);//每次取出一个

            Console.WriteLine(" [consumer] Waiting for messages.");

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();//ReadOnlyMemory to byte[]
                                             //var body = ea.Body.Span//ReadOnlyMemory to byte[]
                    var message = Encoding.UTF8.GetString(body);
                Console.WriteLine("[consumer] Received {0}", message);

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            channel.BasicConsume(queue: QueueName,
                                 autoAck: false,//是否自动确认
                                 consumer: consumer,//消费者标签
                                 exclusive: true,//是否排他的消费者
                                 noLocal: false//为true，则不能在同一个Connection 中 生产者发送的消息发送给消费者。
                                 );

            //    await Task.Delay(1000);
            //}

        }

    }
}
