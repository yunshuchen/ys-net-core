﻿using System;
using System.Collections.Generic;

namespace YS.Blazor.Web.Client.Json.Caching
{
    public interface IResourceNamesCache
    {
        IList<string> GetOrAdd(string name, Func<string, IList<string>> valueFactory);
    }
}
