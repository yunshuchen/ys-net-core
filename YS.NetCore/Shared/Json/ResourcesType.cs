﻿namespace YS.Blazor.Web.Client.Json
{
    public enum ResourcesType
    {
        CultureBased,
        TypeBased
    }
}