﻿using System.IO;
using System.Reflection;

namespace YS.Blazor.Web.Client.Json.Internal
{
    public static class PathHelpers
    {
        public static string GetApplicationRoot()
        {
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            System.Console.WriteLine(path);
            return string.Concat(path,"");        }
    }
}