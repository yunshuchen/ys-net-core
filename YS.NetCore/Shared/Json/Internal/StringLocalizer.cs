﻿using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Microsoft.Extensions.Localization;

namespace YS.Blazor.Web.Client.Json.Internal
{
    internal class StringLocalizer : IStringLocalizer
    {
        private JsonStringLocalizer _localizer;

        public StringLocalizer(IStringLocalizerFactory factory)
        {
            var type = typeof(StringLocalizer);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = factory.Create(string.Empty, assemblyName.FullName) as JsonStringLocalizer;
        }

        public LocalizedString this[string name] => _localizer[name];

        public LocalizedString this[string name, params object[] arguments] => _localizer[name, arguments];

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures) => _localizer.GetAllStrings(includeParentCultures);

        public IStringLocalizer WithCulture(CultureInfo culture) => _localizer.WithCulture(culture);
    }
}