﻿using Microsoft.Extensions.Localization;

namespace YS.Blazor.Web.Client.Json
{
    public class JsonLocalizationOptions : LocalizationOptions
    {
        public ResourcesType ResourcesType { get; set; } = ResourcesType.TypeBased;
    }
}