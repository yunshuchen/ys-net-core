
using YS.Utils.Mvc.Plugin;
using YS.Utils.Cache;
using YS.Utils.Extend;
using YS.Utils.Mvc.RazorPages;
using YS.Utils.Options;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using YS.Utils.Notification;
using YS.Utils.Logging;
using YS.Utils.Encrypt;
using YS.Utils.Mvc.ValueProvider;
using Microsoft.AspNetCore.ResponseCompression;

namespace YS.Utils
{
    public static class Builder
    {

        public static IServiceCollection RegisterGzip(this IServiceCollection services)
        {
            
            services.Configure<GzipCompressionProviderOptions>(options =>
            options.Level = System.IO.Compression.CompressionLevel.Fastest);
            services.AddResponseCompression(options =>
            {
                options.MimeTypes = new[]
                {
                    // Default
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json",
                    // Custom
                    "image/svg+xml",
                    "font/woff2",
                    "application/font-woff",
                    "application/font-ttf",
                    "application/font-eot",
                    "image/jpeg",
                    "image/png"
                };
            });

            return services;
        }
        public static void UseEasyFrameWork(this IServiceCollection services, IConfiguration configuration)
        {
            //浏览器诊断
            services.AddBrowserDetection();

            services.TryAddEnumerable(ServiceDescriptor.Singleton<IConfigureOptions<RazorViewEngineOptions>, PluginRazorViewEngineOptionsSetup>());
            services.TryAddSingleton<IPluginLoader, Loader>();
            services.TryAddTransient<ICookie, Cookie>();

            services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All));
            services.TryAddTransient<IEncryptService, EncryptService>();
            services.TryAddTransient<IMd5Hash, Md5Hash>();
            //Razor 模版渲染服务
            services.AddTransient<IViewRenderService, ViewRenderService>();

            //通知服务
            services.AddTransient<INotificationManager, NotificationManager>();
            services.AddTransient<INotifyService, EmailNotifyService>();
            services.AddTransient<INotifyService, MailKitEmailNotify>();
            services.AddTransient<INotifyService, RazorEmailNotifyService>();


            services.AddTransient<IPluginLoader, Loader>();
            services.AddSingleton<ICacheProvider, DefaultCacheProvider>();
            services.Configure<CDNOption>(configuration.GetSection("CDN"));

            services.Configure<CultureOption>(configuration.GetSection("Culture"));



        }

        public static void ConfigureMetaData<TEntity, TMetaData>(this IServiceCollection service)
        {
            // service.AddSingleton<ViewMetaData<TEntity>, TMetaData>();
        }

        public static IEnumerable<IPluginStartup> LoadAvailablePlugins(this IServiceCollection services)
        {
            var pluginStartup = ActivatorUtilities.GetServiceOrCreateInstance<IPluginLoader>(services.BuildServiceProvider());
            return pluginStartup.LoadEnablePlugins(services);
        }

        public static IApplicationBuilder UsePluginStaticFile(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<PluginStaticFileMiddleware>();
            return builder;
        }
        public static void UseFileLog(this ILoggerFactory loggerFactory, IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            loggerFactory.AddProvider(new FileLoggerProvider(env, httpContextAccessor));
        }


 
    }
}