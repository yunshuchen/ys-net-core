﻿
using Microsoft.CodeAnalysis;
using Microsoft.CSharp;
using SimpleExpressionEvaluator;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;

namespace YS.Utils.LINQ
{
    /// <summary>
    /// 处理表达试运算---动态生成数学表达式并计算其值
    /// 表达式使用 C# 语法，可带一个的自变量(x)。
    /// 表达式的自变量和值均为(double)类型。
    /// </summary>
    /// <example>
    /// <code>
    /// Expression expression = new Expression("Math.Sin(x)"); 
    /// Console.WriteLine(expression.Compute(Math.PI / 2)); 
    /// expression = new Expression("double u = Math.PI - x;" + 
    /// "double pi2 = Math.PI * Math.PI;" + 
    /// "return 3 * x * x + Math.Log(u * u) / pi2 / pi2 + 1;"); 
    /// Console.WriteLine(expression.Compute(0)); 
    /// 
    /// Expression expression = new Expression("return 10*(5+5)/10;");
    /// Response.Write(expression.Compute(0));
    /// Response.End();
    /// </code>
    /// </example>
    public class EvalExpression
    {
        private string expression;


        /// <summary>
        /// 表达试运算
        /// </summary>
        /// <param name="expression">表达试</param>
        public EvalExpression(string _expression)
        {
            this.expression = _expression;

        }


        
        /// <summary>
        /// 处理数据
        /// </summary>
        /// <param name="x"></param>
        /// <returns>返回计算值</returns>
        public int Compute()
        {
            dynamic dynamicEngine = new ExpressionEvaluator();

            return (int)dynamicEngine.Evaluate(expression);
        }
    }

}
