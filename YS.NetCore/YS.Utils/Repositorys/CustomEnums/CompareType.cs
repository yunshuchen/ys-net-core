﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils
{
    public enum CompareType
    {
        /// <summary>
        /// ==
        /// </summary>
        eq = 1,
        /// <summary>
        /// !=
        /// </summary>
        neq = 2,
        /// <summary>
        /// &gt;
        /// </summary>
        gt = 3,
        /// <summary>
        /// >=
        /// </summary>
        gte = 4,
        /// <summary>
        /// &lt;
        /// </summary>
        lt = 5,
        /// <summary>
        /// &lt;=
        /// </summary>
        lte = 6
    }
}
