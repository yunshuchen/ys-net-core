﻿using YS.Utils.DI;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.MySql
{
    [SingletonDI]
    public class MySqlMethodCallTranslatorPlugin : IMethodCallTranslatorPlugin
    {
        public IEnumerable<IMethodCallTranslator> Translators { get; }

        public MySqlMethodCallTranslatorPlugin()
        {
            Translators = new List<IMethodCallTranslator>
            {
                new ComparisonTranslator()
            };
        }
    }
}
