﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.MySql
{
    public class MySqlDbContextOptionsExtensionInfo : DbContextOptionsExtensionInfo
    {
        private readonly IDbContextOptionsExtension _extension;

        public MySqlDbContextOptionsExtensionInfo(IDbContextOptionsExtension extension) : base(extension)
        {
            _extension = extension;
        }

        public override long GetServiceProviderHashCode()
        {
            return _extension.GetHashCode();
        }

        public override void PopulateDebugInfo(IDictionary<string, string> debugInfo)
        {

        }

        public override bool IsDatabaseProvider { get; }
        public override string LogFragment { get; } = "'StringCompareSupport'=true";
    }
}
