﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.Oracle
{
    public class OracleDbContextOptionsExtension : IDbContextOptionsExtension
    {
        public OracleDbContextOptionsExtension()
        {
            Info = new OracleDbContextOptionsExtensionInfo(this);
        }

        public void ApplyServices(IServiceCollection services)
        {
            services.AddSingleton<IMethodCallTranslatorPlugin, OracleMethodCallTranslatorPlugin>();
        }

        public void Validate(IDbContextOptions options)
        {
        }

        public DbContextOptionsExtensionInfo Info { get; }
    }
}
