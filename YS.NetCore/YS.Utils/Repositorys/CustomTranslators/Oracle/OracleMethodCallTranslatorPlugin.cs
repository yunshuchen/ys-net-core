﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.Oracle
{
    public class OracleMethodCallTranslatorPlugin : IMethodCallTranslatorPlugin
    {
        public IEnumerable<IMethodCallTranslator> Translators { get; }

        public OracleMethodCallTranslatorPlugin()
        {
            Translators = new List<IMethodCallTranslator>
            {
                new ComparisonTranslator()
            };
        }
    }
}
