﻿using YS.Utils.DI;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.SqlServer
{
    [SingletonDI]
    public class SqlServerMethodCallTranslatorPlugin : IMethodCallTranslatorPlugin
    {
        public IEnumerable<IMethodCallTranslator> Translators { get; }

        public SqlServerMethodCallTranslatorPlugin(ISqlExpressionFactory sqlExpressionFactory)
        {
            Translators = new List<IMethodCallTranslator>
            {
                new ComparisonTranslator(),
                new MathTranslator(sqlExpressionFactory)
            };
        }
    }
}
