﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.SqlServer
{
    public static class SqlServerDbContextOptionsBuilderExtensions
    {
        public static SqlServerDbContextOptionsBuilder AddSqlServerStringCompareSupport(
            this SqlServerDbContextOptionsBuilder sqlServerOptionsBuilder)
        {
            var infrastructure = (IRelationalDbContextOptionsBuilderInfrastructure)
                sqlServerOptionsBuilder;
            var builder = (IDbContextOptionsBuilderInfrastructure)
                infrastructure.OptionsBuilder;

            // if the extension is registered already then we keep it 
            // otherwise we create a new one
            var extension = infrastructure.OptionsBuilder.Options
                                .FindExtension<SqlServerDbContextOptionsExtension>()
                            ?? new SqlServerDbContextOptionsExtension();
            builder.AddOrUpdateExtension(extension);

            return sqlServerOptionsBuilder;
        }
    }
}
