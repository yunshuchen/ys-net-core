﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace YS.Utils.Repositorys.CustomTranslators.SqlServer
{
    public class MathTranslator : IMethodCallTranslator
    {
        ISqlExpressionFactory Sql { get; }
        public MathTranslator(ISqlExpressionFactory sqlExpressionFactory)
        {
            Sql = sqlExpressionFactory;
        }
        public SqlExpression Translate(SqlExpression instance, MethodInfo method, IReadOnlyList<SqlExpression> arguments, IDiagnosticsLogger<DbLoggerCategory.Query> logger)
        {
            
            if (arguments.Count != 2 || method.DeclaringType != typeof(Math)) return null;
            return method.Name switch
            {
                nameof(Math.Min) => IIF(Sql.LessThan(arguments[0], arguments[1]), arguments[0], arguments[1]),
                nameof(Math.Max) => IIF(Sql.GreaterThan(arguments[0], arguments[1]), arguments[0], arguments[1]),
                _ => null,
            };
            CaseExpression IIF(SqlExpression test, SqlExpression whenTrue, SqlExpression whenFalse) =>
                Sql.Case(new[] { new CaseWhenClause(test, whenTrue) }, whenFalse);
        }
    }
}
