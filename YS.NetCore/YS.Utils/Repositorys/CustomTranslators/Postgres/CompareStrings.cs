﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace YS.Utils.Repositorys.CustomTranslators.Postgres
{

 

    public class CompareStrings : IMethodCallTranslator
    {
        public SqlExpression Translate(SqlExpression instance, MethodInfo method, IReadOnlyList<SqlExpression> arguments, IDiagnosticsLogger<DbLoggerCategory.Query> logger)
        {
            ExpressionType type = ExpressionType.GreaterThan;
            if (method.ReturnType == typeof(bool))
            {
                SqlExpression left = null;
                SqlExpression right = null;
                // if (method.Name == nameof(Text.Compare)
                if(method.DeclaringType == typeof(YS.Utils.Repositorys.CustomTranslators.Text)
                     && arguments.Count == 3
                    && arguments[0].Type.UnwrapNullableType() == arguments[2].Type.UnwrapNullableType())
                {
                    CompareType origin_type = CompareType.gt;
                    
                    if (arguments[1] is SqlConstantExpression constant)
                    {
                       origin_type = (CompareType)constant.Value;
                        //var pro = typeof(CompareType).GetProperty("origin_type_name");
                        //origin_type = (CompareType)pro.GetConstantValue();
                    }
                    switch (origin_type)
                    {
                        case CompareType.eq:
                            type = ExpressionType.Equal;
                            break;
                        case CompareType.neq:
                            type = ExpressionType.NotEqual;
                            break;
                        case CompareType.gt:
                            type = ExpressionType.GreaterThan;
                            break;
                        case CompareType.gte:
                            type = ExpressionType.GreaterThanOrEqual;
                            break;
                        case CompareType.lt:
                            type = ExpressionType.LessThan;
                            break;
                        case CompareType.lte:
                            type = ExpressionType.LessThanOrEqual;
                            break;
                    }


                    left = arguments[0];
                    right = arguments[2];
                }
                else if (method.Name == nameof(string.CompareTo)
                    && arguments.Count == 1
                    && instance != null
                    && instance.Type.UnwrapNullableType() == arguments[0].Type.UnwrapNullableType())
                {
                    left = instance;
                    right = arguments[0];
                }

                if (left != null
                    && right != null)
                {
                    return new SqlBinaryExpression(type, left, right, method.ReturnType, null);
                }
            }

            return null;
        }
    }
}
