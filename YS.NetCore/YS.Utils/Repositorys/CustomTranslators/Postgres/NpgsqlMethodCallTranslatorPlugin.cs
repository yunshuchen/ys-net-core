﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Repositorys.CustomTranslators.Postgres
{
    public class NpgsqlMethodCallTranslatorPlugin : IMethodCallTranslatorPlugin
    {
        public IEnumerable<IMethodCallTranslator> Translators { get; }

        public NpgsqlMethodCallTranslatorPlugin()
        {
            Translators = new List<IMethodCallTranslator>
            {
                new CompareStrings()
            };
        }
    }
}
