﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using Z.BulkOperations;

namespace YS.Utils.Repositorys
{
    /// <summary>
    /// 基础仓储实现
    /// https://docs.microsoft.com/zh-cn/ef/core/providers/sql-server/functions
    /// </summary>
    public partial class EfRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region 参数
        private readonly IDbContext _context;

        //定义数据访问上下文对象

        //private readonly Lazy<MSNopObjectContext> _dbMaster = new Lazy<MSNopObjectContext>(() => new DbContextFactory().CreateWriteDbContext());

        //private readonly Lazy<MSNopObjectContext> _dbSlave = new Lazy<MSNopObjectContext>(() => new DbContextFactory().CreateReadDbContext());


        private DbSet<TEntity> _entities;
        #endregion

        #region 构造函数
        public EfRepository(IDbContext context)
        {
            this._context = context;
        }
        #endregion

        #region 公共方法
        /// <summary>
        /// 实体更改的回滚并返回完整的错误消息
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>Error message</returns>
        protected string GetFullErrorTextAndRollbackEntityChanges(DbUpdateException exception)
        {
            //回滚实体
            if (_context is DbContext dbContext)
            {
                var entries = dbContext.ChangeTracker.Entries()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified).ToList();

                entries.ForEach(entry => entry.State = EntityState.Unchanged);
            }
            _context.SaveChanges();
            return exception.ToString();
        }
        #endregion

        #region 方法
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="OrderBy">排序字符串</param>
        /// <param name="predicate">条件Linq表达式</param>
        /// <returns>Entity</returns>
        public virtual TEntity GetOne(string OrderBy, Expression<Func<TEntity, bool>> predicate)
        {
            //var predicate1 = LinqKit.PredicateBuilder.New<TEntity>(true);
            //predicate1.

            if (OrderBy.Trim() != "")
            {
                return Entities.Where(predicate).OrderBy(OrderBy).AsNoTracking().FirstOrDefault();
            }
            else
            {
                return Entities.Where(predicate).AsNoTracking().FirstOrDefault();
            }
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="predicate">条件Linq表达式</param>
        /// <returns>记录数</returns>
        public virtual int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return Entities.Where(predicate).AsNoTracking().Count();
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="ExpressionsSql">Sql 语句</param>
        /// <returns>记录数</returns>
        public virtual int Count(string ExpressionsSql)
        {
            return Count(ExpressionsSql, null);
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="ExpressionsSql">Sql 语句,变量以:@0,@1....</param>
        /// <param name="ExpressionPartams">Sql 语句条件</param>
        /// <returns>记录数</returns>
        public virtual int Count(string ExpressionsSql, params object[] ExpressionPartams)
        {
            if (ExpressionPartams != null && ExpressionPartams.Length > 0)
            {
                return Entities.Where(ExpressionsSql, ExpressionPartams).AsNoTracking().Count();
            }
            else
            {
                return Entities.Where(ExpressionsSql).AsNoTracking().Count();
            }
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual bool IsExists(Expression<Func<TEntity, bool>> predicate)
        {
            return Entities.Any(predicate);
        }

        public virtual List<TEntity> GetList(string OrderBy, List<Expression<Func<TEntity, bool>>> expressions)
        {
            IQueryable<TEntity> order_result= Entities.AsQueryable();
            if (OrderBy != "")
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.OrderBy(OrderBy).AsNoTracking().ToList();
            }
            else {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.AsNoTracking().ToList();
            }
        }


        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=123</param>
        /// <returns></returns>
        public virtual List<TEntity> GetList(string OrderBy, string Expressions)
        {
            return GetList(OrderBy, Expressions, null);
        }
        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        public virtual List<TEntity> GetList(string OrderBy, string Expressions, params object[] ExpressionPartams) {
            IQueryable<TEntity> order_result = Entities;
            if (OrderBy != "")
            {
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions);
                }
                return order_result.OrderBy(OrderBy).AsNoTracking().ToList();
            }
            else
            {
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions);
                }
                return order_result.AsNoTracking().ToList();
            }
        }

        public virtual PageOf<TEntity> GetPageList(int iPage, int PageSize, string OrderBy, List<Expression<Func<TEntity, bool>>> expressions)
        {
            if (OrderBy != "")
            {
                IQueryable<TEntity> order_result = Entities.OrderBy(OrderBy);
                
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                var result = order_result.PageResult(iPage, PageSize);
                return new PageOf<TEntity>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
            else
            {
                IQueryable<TEntity> order_result = Entities;
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                var result = order_result.PageResult(iPage, PageSize);
                return new PageOf<TEntity>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
        }


        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <returns></returns>
        public virtual PageOf<TEntity> GetPageList(int iPage, int PageSize, string OrderBy, string Expressions)
        {
            return GetPageList(iPage, PageSize,OrderBy, Expressions, null);
        }
        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        public virtual PageOf<TEntity> GetPageList(int iPage, int PageSize, string OrderBy, string Expressions, params object[] ExpressionPartams)
        {
            if (OrderBy != "")
            {
                IQueryable<TEntity> order_result = Entities.OrderBy(OrderBy);
           
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                var result = order_result.AsNoTracking().PageResult(iPage, PageSize);
                return new PageOf<TEntity>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
            else
            {
                IQueryable<TEntity> order_result = Entities;
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                var result = order_result.AsNoTracking().PageResult(iPage, PageSize);
                return new PageOf<TEntity>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
        }
        #region 删除
        /// <summary>
        /// 按id获取实体
        /// </summary>
        /// <param name="predicate">删除条件</param>
        public virtual int Delete(Expression<Func<TEntity, bool>> predicate)
        {
           return  Entities.Where(predicate).DeleteFromQuery();
           // _context.SaveChanges();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            try
            {
                Entities.Remove(entity);
                _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                Entities.BulkDelete(entities);
                //_context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        #endregion


        #region 添加
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual TEntity Insert(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            try
            {
                var result = Entities.Add(entity).Entity;
                _context.SaveChanges();
                return result;
            }

            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }



        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void BulkInsert(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                Entities.BulkInsert(entities);
               // _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        #endregion

        #region 修改
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual TEntity Update(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            try
            {
                var result=Entities.Update(entity).Entity;
                _context.SaveChanges();
                return result;
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }


     

        /// <summary>
        /// 批量修改
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void Update(IEnumerable<TEntity> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                Entities.BulkUpdate(entities);
               // _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        /// <summary>
        /// 根据条件修改
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="updator"></param>
        public virtual int Update(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TEntity>> updator)
        {
            try
            {
                return Entities.Where(predicate).UpdateFromQuery(updator);
                //_context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }

        }

        public void BulkMerge(IEnumerable<TEntity> entities, Action<BulkOperation<TEntity>> options)
        {
            try
            {
                Entities.BulkMerge(entities, options);
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }

        }
        public void BulkSynchronize(IEnumerable<TEntity> entities, Action<BulkOperation<TEntity>> options)
        {
            try
            {
                Entities.BulkSynchronize(entities, options);
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

       
        public IQueryable<TEntity> ToJoin(string OrderBy, List<Expression<Func<TEntity, bool>>> expressions)
        {
            IQueryable<TEntity> order_result = Entities;
            if (OrderBy != "")
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.OrderBy(OrderBy);
            }
            else
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result;
            }
        }

        #endregion


        #endregion



        #region 属性
        /// <summary>
        /// 获取表
        /// </summary>
        public virtual IQueryable<TEntity> Table => Entities;

        /// <summary>
        /// 获取一个启用“no tracking”(EF特性)的表，仅当您仅为只读操作加载记录时才使用它
        /// </summary>
        public virtual IQueryable<TEntity> TableNoTracking => Entities.AsNoTracking();

        /// <summary>
        /// 获取设置模板
        /// </summary>
        public virtual DbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();

                return _entities;
            }
        }


        #endregion


        #region 方法T1
        /// <summary>
        /// 获取单个实体
        /// </summary>
        /// <param name="OrderBy">排序字符串</param>
        /// <param name="predicate">条件Linq表达式</param>
        /// <returns>Entity</returns>
        public virtual T1 GetOne<T1>(string OrderBy, Expression<Func<T1, bool>> predicate) where T1 : class
        {
            // var predicate1 = LinqKit.PredicateBuilder.New<T1>(false);
            //predicate1.And(a => a.id == 1);

            if (OrderBy.Trim() != "")
            {
                return OtherTable<T1>().Where(predicate).OrderBy(OrderBy).AsNoTracking().FirstOrDefault();
            }
            else
            {
                return OtherTable<T1>().Where(predicate).AsNoTracking().FirstOrDefault();
            }
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="predicate">条件Linq表达式</param>
        /// <returns>记录数</returns>
        public virtual int Count<T1>(Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return OtherTable<T1>().Where(predicate).AsNoTracking().Count();
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="ExpressionsSql">Sql 语句</param>
        /// <returns>记录数</returns>
        public virtual int Count<T1>(string ExpressionsSql) where T1 : class
        {
            return Count<T1>(ExpressionsSql, null);
        }

        /// <summary>
        /// 获取记录数量
        /// </summary>
        /// <param name="ExpressionsSql">Sql 语句,变量以:@0,@1....</param>
        /// <param name="ExpressionPartams">Sql 语句条件</param>
        /// <returns>记录数</returns>
        public virtual int Count<T1>(string ExpressionsSql, params object[] ExpressionPartams) where T1 : class
        {
            if (ExpressionPartams != null && ExpressionPartams.Length > 0)
            {
                return OtherTable<T1>().Where(ExpressionsSql, ExpressionPartams).AsNoTracking().Count();
            }
            else
            {
                return OtherTable<T1>().Where(ExpressionsSql).AsNoTracking().Count();
            }
        }

        /// <summary>
        /// 判断是否存在
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public virtual bool IsExists<T1>(Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return OtherTable<T1>().Any(predicate);
        }

        public virtual List<T1> GetList<T1>(string OrderBy, List<Expression<Func<T1, bool>>> expressions) where T1 : class
        {
            IQueryable<T1> order_result = OtherTable<T1>();
            if (OrderBy != "")
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.OrderBy(OrderBy).AsNoTracking().ToList();
            }
            else
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.AsNoTracking().ToList();
            }
        }


        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=123</param>
        /// <returns></returns>
        public virtual List<T1> GetList<T1>(string OrderBy, string Expressions) where T1 : class
        {
            return GetList<T1>(OrderBy, Expressions, null);
        }
        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        public virtual List<T1> GetList<T1>(string OrderBy, string Expressions, params object[] ExpressionPartams) where T1 : class
        {
            IQueryable<T1> order_result = OtherTable<T1>();
            if (OrderBy != "")
            {
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions);
                }
                return order_result.OrderBy(OrderBy).AsNoTracking().ToList();
            }
            else
            {
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions);
                }
                return order_result.AsNoTracking().ToList();
            }
        }

        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, List<Expression<Func<T1, bool>>> expressions) where T1 : class
        {
            if (OrderBy != "")
            {
                IQueryable<T1> order_result = OtherTable<T1>().OrderBy(OrderBy);

                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                var result = order_result.PageResult(iPage, PageSize);
                return new PageOf<T1>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
            else
            {
                IQueryable<T1> order_result = OtherTable<T1>();
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                var result = order_result.PageResult(iPage, PageSize);
                return new PageOf<T1>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
        }


        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <returns></returns>
        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string Expressions) where T1 : class
        {
            return GetPageList<T1>(iPage, PageSize, OrderBy, Expressions, null);
        }
        /// <summary>
        /// 查询列表根据Sql条件
        /// 
        /// </summary>
        /// <param name="OrderBy">排序字符串Ps:id asc</param>
        /// <param name="Expressions">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string Expressions, params object[] ExpressionPartams) where T1 : class
        {
            if (OrderBy != "")
            {
                IQueryable<T1> order_result = OtherTable<T1>().OrderBy(OrderBy);
            
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                var result = order_result.AsNoTracking().PageResult(iPage, PageSize);
                return new PageOf<T1>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
            else
            {
                IQueryable<T1> order_result = OtherTable<T1>();
                if (ExpressionPartams != null && ExpressionPartams.Length > 0)
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                else
                {
                    order_result = order_result.Where(Expressions, ExpressionPartams);
                }
                var result = order_result.AsNoTracking().PageResult(iPage, PageSize);
                return new PageOf<T1>()
                {
                    list = result.Queryable.ToList(),
                    page_index = result.CurrentPage,
                    page_size = result.PageSize,
                    total = result.RowCount
                };
            }
        }
        #region 删除
        /// <summary>
        /// 按id获取实体
        /// </summary>
        /// <param name="predicate">删除条件</param>
        public virtual int Delete<T1>(Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return OtherTable<T1>().Where(predicate).DeleteFromQuery();
            // _context.SaveChanges();
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual void Delete<T1>(T1 entity) where T1 : class
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            try
            {
                OtherSet<T1>().Remove(entity);
                _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void Delete<T1>(IEnumerable<T1> entities) where T1 : class
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                OtherSet<T1>().BulkDelete(entities);
                //_context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        #endregion


        #region 添加
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual T1 Insert<T1>(T1 entity) where T1 : class
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            try
            {
                var result = OtherSet<T1>().Add(entity).Entity;
                _context.SaveChanges();
                return result;
            }

            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }



        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void BulkInsert<T1>(IEnumerable<T1> entities) where T1 : class
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                OtherSet<T1>().BulkInsert(entities);
                // _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        #endregion

        #region 修改
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="entity">Entity</param>
        public virtual T1 Update<T1>(T1 entity) where T1 : class
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            try
            {
                var result = OtherSet<T1>().Update(entity).Entity;
                _context.SaveChanges();
                return result;
            }
            catch (DbUpdateException exception)
            {
                //ensure that the detailed error text is saved in the Log
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }




        /// <summary>
        /// 批量修改
        /// </summary>
        /// <param name="entities">Entities</param>
        public virtual void Update<T1>(IEnumerable<T1> entities) where T1 : class
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            try
            {
                OtherSet<T1>().BulkUpdate(entities);
                // _context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }
        /// <summary>
        /// 根据条件修改
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="updator"></param>
        public virtual int Update<T1>(Expression<Func<T1, bool>> predicate, Expression<Func<T1, T1>> updator) where T1 : class
        {
            try
            {
                return OtherSet<T1>().Where(predicate).UpdateFromQuery(updator);
                //_context.SaveChanges();
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }

        }

        public void BulkMerge<T1>(IEnumerable<T1> entities, Action<BulkOperation<T1>> options) where T1 : class
        {
            try
            {
                OtherSet<T1>().BulkMerge(entities, options);
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }

        }
        public void BulkSynchronize<T1>(IEnumerable<T1> entities, Action<BulkOperation<T1>> options) where T1 : class
        {
            try
            {
                OtherSet<T1>().BulkSynchronize(entities, options);
            }
            catch (DbUpdateException exception)
            {
                throw new Exception(GetFullErrorTextAndRollbackEntityChanges(exception), exception);
            }
        }



        public IQueryable<T1> ToJoin<T1>(string OrderBy, List<Expression<Func<T1, bool>>> expressions) where T1 : class
        {
            IQueryable<T1> order_result = OtherTable<T1>();
            if (OrderBy != "")
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result.OrderBy(OrderBy);
            }
            else
            {
                foreach (var item in expressions)
                {
                    order_result = order_result.Where(item);
                }
                return order_result;
            }
        }

        #endregion

        #endregion

        #region 扩展
        private Dictionary<string, object> OtherTableSet = new Dictionary<string, object>();
        public IQueryable<TOther> OtherTable<TOther>() where TOther : class
        {
            string SetCacheKeyName = typeof(TOther).ToString();
            object result = null;

            OtherTableSet.TryGetValue(SetCacheKeyName, out result);


            if (result == null)
            {
                result = _context.Set<TOther>();
                OtherTableSet.TryAdd(SetCacheKeyName, result);
            }
            return result as IQueryable<TOther>;
        }


        public DbSet<TOther> OtherSet<TOther>() where TOther : class
        {
            string SetCacheKeyName = typeof(TOther).ToString();
            object result = null;

            OtherTableSet.TryGetValue(SetCacheKeyName, out result);


            if (result == null)
            {
                result = _context.Set<TOther>();
                OtherTableSet.TryAdd(SetCacheKeyName, result);
            }
            return result as DbSet<TOther>;
        }


        public virtual DbContext dbContext => _context.dbContext;
        #endregion
    }
}
