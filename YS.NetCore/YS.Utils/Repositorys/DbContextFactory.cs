﻿//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Microsoft.Extensions.Logging.Console;
//using System;
//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using System.Threading.Tasks;

//namespace YS.Utils.Repositorys
//{
//    public class DbContextFactory
//    {
//        private readonly int dbcount;
//        private readonly YS.Utils.Options.DatabaseMasterSlaveOption database_master_slave;
//        public DbContextFactory()
//        {
//            var builder = new ConfigurationBuilder();
//            // 方式1
//            var _configuration = builder
//                 .AddJsonFile("configs/database_master_slave.json", false, true)
//                 .Build();
//            database_master_slave = _configuration.GetSection("MasterSlaveSetting").Get<YS.Utils.Options.DatabaseMasterSlaveOption>();


//            dbcount = database_master_slave.DbCount;
//        }




//        /// <summary>
//        /// 获取DbContext的Options
//        /// </summary>
//        /// <param name="writeRead"></param>
//        /// <returns></returns>
//        public DbContextOptions<MSNopObjectContext> GetOptions(WriteAndRead writeRead)
//        {


//            //随机选择读数据库节点
//            var optionsBuilder = new DbContextOptionsBuilder<MSNopObjectContext>();
//            YS.Utils.Options.DatabaseOption databaseOption = database_master_slave.database[0];
//            if (writeRead == WriteAndRead.Read)
//            {
//                Random r = new Random();
//                int i = r.Next(1, dbcount);
//                databaseOption = database_master_slave.database[i];
//            }
//            if (databaseOption.DbType == "MsSql")
//            {
//                optionsBuilder.UseLazyLoadingProxies()
//                .UseSqlServer(connectionString: databaseOption.ConnectionString);

//            }
//            else if (databaseOption.DbType == "MySql")
//            {

//                optionsBuilder.UseLazyLoadingProxies()
//                .UseMySql(connectionString: databaseOption.ConnectionString, serverVersion: Microsoft.EntityFrameworkCore.ServerVersion.FromString(databaseOption.DBVersion))
//                 .UseLoggerFactory(LoggerFactory.Create(b => b
//                .AddConsole()
//                .AddFilter(level => level >= LogLevel.Information)))
//            .EnableSensitiveDataLogging()
//            .EnableDetailedErrors();
//            }
//            else if (databaseOption.DbType == "NpgSql")
//            {
//                optionsBuilder.UseLazyLoadingProxies()
//                  .UseNpgsql(connectionString: databaseOption.ConnectionString);
//            }
//            else if (databaseOption.DbType == "Oracle")
//            {
//                optionsBuilder.UseLazyLoadingProxies()
//                .UseOracle(connectionString: databaseOption.ConnectionString, b => b.UseOracleSQLCompatibility(databaseOption.DBVersion));
//            }
//            else
//            {
//                throw new Exception("DB Config Error");
//            }

//            return optionsBuilder.Options;
//        }

//        /// <summary>
//        /// 创建ReadDbContext实例
//        /// </summary>
//        /// <returns></returns>
//        public MSNopObjectContext CreateReadDbContext()
//        {
//            //先从线程获取实例，保证线程安全
//            MSNopObjectContext dbContext = (MSNopObjectContext)CallContext.GetData("ReadDbContext");
//            if (dbContext == null)
//            {
//                if (dbcount == 1)//如果数据库数量为1，则不启用读写分离
//                {
//                    dbContext = new MSNopObjectContext(WriteAndRead.Write);
//                }
//                else
//                {
//                    dbContext = new MSNopObjectContext(WriteAndRead.Read);
//                }
//                CallContext.SetData("ReadDbContext", dbContext);
//            }
//            return dbContext;
//        }

//        /// <summary>
//        /// 创建WriteDbContext实例
//        /// </summary>
//        /// <returns></returns>
//        public MSNopObjectContext CreateWriteDbContext()
//        {
//            //先从线程获取实例，保证线程安全
//            MSNopObjectContext dbContext = (MSNopObjectContext)CallContext.GetData("WriteDbContext");
//            if (dbContext == null)
//            {
//                dbContext = new MSNopObjectContext(WriteAndRead.Write);
//                CallContext.SetData("WriteDbContext", dbContext);
//            }
//            return dbContext;
//        }


//    }

//    /// <summary>
//    /// 从线程获取实例
//    /// </summary>
//    public class CallContext
//    {
//        static ConcurrentDictionary<string, AsyncLocal<object>> state = new ConcurrentDictionary<string, AsyncLocal<object>>();

//        public static void SetData(string name, object data) =>
//        state.GetOrAdd(name, _ => new AsyncLocal<object>()).Value = data;

//        public static object GetData(string name) =>
//        state.TryGetValue(name, out AsyncLocal<object> data) ? data.Value : null;
//    }
//}