﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Reflection;
using System.Text;

namespace YS.Utils
{
    public static  class EnumExt
    {

        /// <summary>
        /// 获取所有的枚举
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>
        /// [{value,desc}]
        /// </returns>
        public static List<dynamic> GetEnumByValueDescription<T>()  where T : Enum
        {
            List<dynamic> list = new List<dynamic>();
            System.Reflection.FieldInfo[] fields = typeof(T).GetFields();
            foreach (FieldInfo field in fields)
            {
                if (!field.FieldType.IsEnum)
                {
                    continue;
                }
                int value = (int)field.GetValue(null);
                string desc = fields.ToString();
                object[] objs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);    //获取描述属性
                if (objs.Length > 0)
                {
                    desc = ((DescriptionAttribute)objs[0]).Description;
                }
                dynamic Customer = new ExpandoObject(); 
                Customer.value = value; 
                Customer.desc = desc; 
                
                list.Add(Customer);
            }
            return list;
        }


        /// <summary>
        /// 获取枚举desc
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static string GetDescriptionByEnum(Enum enumValue)
        {
            string value = enumValue.ToString();
            System.Reflection.FieldInfo field = enumValue.GetType().GetField(value);
            object[] objs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);    //获取描述属性
            if (objs.Length == 0)    //当描述属性没有时，直接返回名称
                return value;
            DescriptionAttribute descriptionAttribute = (DescriptionAttribute)objs[0];
            return descriptionAttribute.Description;
        }
    }
}
