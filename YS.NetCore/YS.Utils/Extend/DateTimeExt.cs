﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils.Extend
{
    public static class DateTimeExt
    {
        /// <summary>
        /// DateTime To Unix
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public static long ToUnix(this DateTime now)
        {
            DateTime startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0), TimeZoneInfo.Local);  // 当地时区
            long timeStamp = (long)((now - startTime).TotalMilliseconds); // 相差毫秒数
            return timeStamp;
        }

        /// <summary>
        /// Unix To DateTime
        /// </summary>
        /// <param name="unix_time"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this long unix_time)
        {
            System.DateTime startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0), TimeZoneInfo.Local);//TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            string time_str = unix_time.ToString();
            string Millisecond = "0";
            if (time_str.Length > 10)
            {
                Millisecond = time_str.Substring(10);
                time_str = time_str.Substring(0, 10);
            }
            DateTime TranslateDate = startTime.AddSeconds(double.Parse(time_str)).AddMilliseconds(double.Parse(Millisecond));
            return TranslateDate;
        }

        /// <summary>
        /// 设置常量最小时间
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static DateTime ToMinValue(this DateTime obj)
        {
            return DateTime.Parse("1900-01-01 00:00:00.000");

        }

    }
}
