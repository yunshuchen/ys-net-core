using System.Collections.Generic;
using System.Runtime.Serialization;

namespace YS.Utils
{

    public class PageOf<T>
    {
        /// <summary>
        /// 信息对象列表
        /// </summary>
        public IList<T> list { get; set; }

        /// <summary>
        /// 总数
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int page_index { get; set; }

        /// <summary>
        /// 页大小
        /// </summary>
        public int page_size { get; set; }
        public string PagePostUrl { get; set; }

        public int page_count
        {
            get
            {
                if (total % page_size != 0)
                {
                    return (int)(total / page_size) + 1;
                }
                else
                {
                    return (int)(total / page_size);
                }
            }
        }
    }
}
