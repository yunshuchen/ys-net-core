﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace YS.Utils
{
    public enum StatusEnum:int
    {
        All = -1,
        /// <summary>
        ///启用
        /// </summary>
        [Description("启用")]
        Legal =1,
        /// <summary>
        /// 禁用
        /// </summary>
        [Description("禁用")]
        UnLegal =0
    }

    /// <summary>
    /// 审核状态
    /// </summary>
    public enum ReViewStatusEnum:int
    {
        ALl=-1,
        UnReview=0,
        Pass=1,
        UnPass=2
    }

    /// <summary>
    /// 删除
    /// </summary>
    public enum DeleteEnum:int
    {
        ALl = -1,
        /// <summary>
        /// 未删除
        /// </summary>
        UnDelete = 0,
        /// <summary>
        /// 已删除
        /// </summary>
        Delete = 1
    }

    /// <summary>
    /// 栏目类型
    /// </summary>
    public enum NodeTypeEnum :int{
        /// <summary>
        /// 内容栏目
        /// </summary>
        ContentNode=1,
        /// <summary>
        /// 会员栏目
        /// </summary>
        MemberNode=2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GenderEnum:int
    {

        Unknown = 0,
        Man = 1,
        Wonan = 2
    }
}
