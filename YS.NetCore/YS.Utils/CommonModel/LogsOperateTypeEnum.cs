﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace YS.Utils
{
    public enum LogsOperateTypeEnum
    {
        /// <summary>
        /// 登录
        /// </summary>
        [Description("登录")]
        Login = 1,
        /// <summary>
        /// 添加
        /// </summary>
        [Description("添加")]
        Add = 2,
        /// <summary>
        /// 修改
        /// </summary>
        [Description("修改")]
        Update = 3,
        /// <summary>
        /// 删除
        /// </summary>
        [Description("删除")]
        Delete = 5,
        /// <summary>
        ///  下载
        /// </summary>
        [Description("下载")]
        Down = 6,

        /// <summary>
        ///  审核
        /// </summary>
        [Description("审核")]
        ReView = 7,

        /// <summary>
        ///  恢复
        /// </summary>
        [Description("恢复")]
        Recovery = 8,
        /// <summary>
        ///  打印
        /// </summary>
        [Description("打印")]
        Print =9,

        /// <summary>
        ///  导入
        /// </summary>
        [Description("导入")]
        Import = 10,

        /// <summary>
        ///  导出
        /// </summary>
        [Description("导出")]
        Export = 11
    }
}
