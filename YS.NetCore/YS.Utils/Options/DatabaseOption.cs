using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Options
{
    public class DatabaseOption
    {
        public string DbType { get; set; }
        public string ConnectionString { get; set; }

        public string DBVersion { get; set; }
    }


    public class DatabaseMasterSlaveOption
    {
        public List<DatabaseOption> database { get; set; }
        public int DbCount { get; set; }
    }
}
