using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Options
{
    public class SessionOptions
    {
        /// <summary>
        /// SqlServer/Redis/Memory
        /// </summary>
        public string SessionType { get; set; }
        /// <summary>
        /// Session 存储链接:如Memory则链接忽略
        /// </summary>
        public string SessionConnectionString { get; set; }

        /// <summary>
        /// Session 超时分钟数
        /// </summary>
        public double SessionTimeOut { get; set; }

    }
}
