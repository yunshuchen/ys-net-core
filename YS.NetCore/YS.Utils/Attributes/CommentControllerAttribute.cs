﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils
{
    /// <summary>
    /// 根据结算代号调用具体的结算函数
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CommentControllerAttribute : SwaggerTagAttribute
    {
        /// 备注
        /// </summary>
        public string Comment { get; private set; }


        /// <summary>
        /// Commamd标记特性
        /// 提供给远程端来调用
        /// </summary>    
        /// <param name="Comment">备注</param> 
        public CommentControllerAttribute(string Comment) : base(Comment)
        {
            this.Comment = Comment;
        }

    }


}