

namespace YS.Utils.SMTP
{
    public class SmtpSetting
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Email { get; set; }
        public string PassWord { get; set; }
        public bool EnableSsl { get; set; }
    }
  
}
