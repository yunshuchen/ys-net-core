﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Mvc
{
    /// <summary>
    ///
    /// </summary>
    public class RestfulData
    {
        public RestfulData()
        {
        }
        public RestfulData(int code, string msg)
        {
            this.code = code;
            this.msg = msg;
        }
        /// <summary>
        /// <![CDATA[错误码]]>
        /// </summary>
        public int code { get; set; }

        /// <summary>
        ///<![CDATA[消息]]>
        /// </summary>
        public string msg { get; set; }



    }

    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RestfulData<T> : RestfulData
    {
        public RestfulData()
        {
        }

        public RestfulData(int code, string msg)
        {
            this.code = code;
            this.msg = msg;
        }


        public RestfulData(int code, string msg,T value)
        {
            this.code = code;
            this.msg = msg;
            this.data = value;
        }
        /// <summary>
        /// <![CDATA[数据]]>
        /// </summary>
        public virtual T data { get; set; }
    }

    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RestfulPageData<T> : RestfulData
    {
        private int total = 0;
        private List<T> value = null;
        public RestfulPageData()
        {
        }

        public RestfulPageData(int code, string msg)
        {
            this.code = code;
            this.msg = msg;
        }
        public RestfulPageData(int code, string msg,PageOf<T> page_data)
        {
            this.code = code;
            this.msg = msg;
            this.data = new
            {
                list = page_data.list,
                total = page_data.total
            };
        }

        public RestfulPageData(int code, string msg, List<T> value, int total)
        {
            this.code = code;
            this.msg = msg;
            this.total = total;
            this.value = value;
            this.data = new
            {
                list = value,
                total = total
            };
        }
        /// <summary>
        /// <![CDATA[数据]]>
        /// </summary>
        public virtual object data { get; set; }
    }

    /// <summary>
    /// <![CDATA[返回数组]]>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RestfulArray<T> : RestfulData<IEnumerable<T>>
    {
        public RestfulArray(int code, string msg, IEnumerable<T> data)
        {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }
    }
}
