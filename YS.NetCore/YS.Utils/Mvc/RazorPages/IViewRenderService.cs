using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Mvc.RazorPages
{
    public interface IViewRenderService
    {
        string Render(string viewPath);
        string Render<Model>(string viewPath, Model model);
        string Render<TModel>(string pluginPath, string viewPath, TModel model);


    }
}
