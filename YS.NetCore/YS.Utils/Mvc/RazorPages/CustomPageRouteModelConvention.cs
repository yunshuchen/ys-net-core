﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YS.Utils.RazorPages
{
    public class CustomPageRouteModelConvention : IPageRouteModelConvention
    {
        public void Apply(PageRouteModel model)
        {
            foreach (var selector in model.Selectors.ToList())
            {

   
                model.Selectors.Add(new SelectorModel
                {
                    AttributeRouteModel = new AttributeRouteModel
                    {
                        Order = 1,
                        Template = AttributeRouteModel.CombineTemplates(
                            selector.AttributeRouteModel.Template,
                            "{globalTemplate?}"),
                    }
                });
                //selector.AttributeRouteModel.Template = "{tenant}/" + selector.AttributeRouteModel.Template;
            }
        }
    }
}
