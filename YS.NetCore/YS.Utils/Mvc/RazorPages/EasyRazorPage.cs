using Microsoft.AspNetCore.Mvc.Razor;
using YS.Utils.Mvc.Resource;
using Microsoft.AspNetCore.Html;
using YS.Utils.Mvc.Resource.Enums;
using System.Linq;
using YS.Utils.Extend;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using YS.Utils.Options;
using Microsoft.Extensions.Options;
using System.Text.RegularExpressions;
using System;

namespace YS.Utils.Mvc.RazorPages
{
    public abstract class EasyRazorPage<TModel> : RazorPage<TModel>
    {
        private List<ResourceCollection> _requiredScripts
        {
            get
            {
                const string key = "Requied_Scripts";
                if (ViewContext.HttpContext.Items.ContainsKey(key))
                {
                    return ViewContext.HttpContext.Items[key] as List<ResourceCollection>;
                }
                var scripts = new List<ResourceCollection>();
                ViewContext.HttpContext.Items.Add(key, scripts);
                return scripts;
            }
        }
        private List<ResourceCollection> _requiredStyles
        {
            get
            {
                const string key = "Requied_Styles";
                if (ViewContext.HttpContext.Items.ContainsKey(key))
                {
                    return ViewContext.HttpContext.Items[key] as List<ResourceCollection>;
                }
                var styles = new List<ResourceCollection>();
                ViewContext.HttpContext.Items.Add(key, styles);
                return styles;
            }
        }
        public IHtmlContent ScriptAtHead(bool includeRequired = true)
        {
            return GetResource(includeRequired, ResourceType.Script, ResourcePosition.Head);
        }
       

        public IHtmlContent ScriptAtFoot(bool includeRequired = true)
        {
            return GetResource(includeRequired, ResourceType.Script, ResourcePosition.Foot);
        }

        public IHtmlContent StyleAtHead(bool includeRequired = true)
        {
            return GetResource(includeRequired, ResourceType.Style, ResourcePosition.Head);
        }


        public IHtmlContent StyleAtFoot(bool includeRequired = true)
        {
            return GetResource(includeRequired, ResourceType.Style, ResourcePosition.Foot);
        }

        #region html标签相关

        /// <summary>
        /// 获取语言
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public LocalizeString L(string content)
        {
            string plugin_name = ((Microsoft.AspNetCore.Mvc.Controllers.ControllerActionDescriptor)this.ViewContext.ActionDescriptor).ControllerTypeInfo.Assembly.GetName().Name;
            return new LocalizeString(content, plugin_name, this.ViewContext.HttpContext);
        }
        /// <summary>
        /// 获取语言
        /// </summary>
        /// <param name="content"></param>
        /// <param name="plugin_name"></param>
        /// <returns></returns>
        public LocalizeString L(string content, string plugin_name)
        {
            return new LocalizeString(content, plugin_name, this.ViewContext.HttpContext);
        }
        /// <summary>
        /// 清除Html标签
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <returns></returns>
        public string ClearHTML(string Htmlstring)
        {
            Htmlstring = Regex.Replace(Htmlstring, @"<script[\s\S]*?</script>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<noscript[\s\S]*?</noscript>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<style[\s\S]*?</style>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<.*?>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", " ", RegexOptions.IgnoreCase);
            return Htmlstring;
        }
        /// <summary>
        /// 截取文字
        /// </summary>
        /// <param name="content">需要截取的文字</param>
        /// <param name="start">开始位置</param>
        /// <param name="length">截取长度</param>
        /// <returns></returns>
        public string Sub(string content, int start, int length)
        {
            content = string.Concat(content, "");

            if (content.Length < length)
            {
                length = content.Length;
            }

            return content.Substring(start, length);
        }
        /// <summary>
        /// 截取Html文字
        /// </summary>
        /// <param name="HtmlContent">html文字</param>
        /// <param name="start">开始位置</param>
        /// <param name="length">截取长度</param>
        /// <returns></returns>
        public string SubHtml(string HtmlContent, int start, int length)
        {
            HtmlContent = string.Concat(HtmlContent, "");
            HtmlContent = ClearHTML(HtmlContent);
            if (HtmlContent.Length < length)
            {
                length = HtmlContent.Length;
            }

            return HtmlContent.Substring(start, length);
        }
        /// <summary>
        /// 判断是否手机浏览器 true(Mobile/Tablet),false(desktop)
        /// </summary>
        /// <returns></returns>
        public bool CheckMobileBrowser()
        {
            var service = ViewContext.HttpContext.RequestServices.GetService<Shyjus.BrowserDetection.IBrowserDetector>();
            return service.Browser.DeviceType == "Mobile" || service.Browser.DeviceType == "Tablet";
        }
        /// <summary>
        /// 浏览器名称:Chrome,Edge,Internet Explorer 11,Opera,Safari,Firefox
        /// </summary>
        /// <returns></returns>
        public string GetBrowserName()
        {
            var service = ViewContext.HttpContext.RequestServices.GetService<Shyjus.BrowserDetection.IBrowserDetector>();
            return service.Browser.Name;
        }
        /// <summary>
        /// 浏览器操作系统:Windows,Mac OS,iOS,Android,OSX
        /// </summary>
        /// <returns></returns>
        public string GetOperatingSystemName()
        {
            var service = ViewContext.HttpContext.RequestServices.GetService<Shyjus.BrowserDetection.IBrowserDetector>();
            return service.Browser.OS;
        }

        /// <summary>
        /// 格式化时间
        /// </summary>
        /// <param name="date_str">要格式化的时间字符串</param>
        /// <param name="format">yyyy-MM-dd HH:mm:ss</param>
        /// <returns></returns>
        public string FormatDate(string date_str, string format)
        {
            try
            {
                return DateTime.Parse(date_str).ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);
            }
            catch (Exception)
            {

                return "";
            }
            
        }
        /// <summary>
        /// 格式化时间
        /// </summary>
        /// <param name="date_str">要格式化的时间字符串</param>
        /// <param name="format">yyyy-MM-dd HH:mm:ss</param>
        /// <returns></returns>
        public string FormatDate(DateTime date_time, string format)
        {
            try
            {
                return date_time.ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);
            }
            catch (Exception)
            {

                return "";
            }

        }
        #endregion


        private IHtmlContent GetResource(bool includeRequired, ResourceType type, ResourcePosition position)
        {
            var builder = new HtmlContentBuilder();
            IUrlHelper urlHelper = Context.RequestServices.GetService<IUrlHelperFactory>().GetUrlHelper(ViewContext);
            IWebHostEnvironment hostingEnvironment = Context.RequestServices.GetService<IWebHostEnvironment>();
            IOptions<CDNOption> options = Context.RequestServices.GetService<IOptions<CDNOption>>();
            switch (type)
            {
                case ResourceType.Script:
                    {
                        if (includeRequired)
                        {
                            ResourceHelper.ScriptSource.Where(m => m.Value.Required && m.Value.Position == position)
                                                    .Each(m => m.Value.Each(r =>
                                                    {
                                                        builder.AppendHtml(r.ToSource(urlHelper, hostingEnvironment, options));
                                                    }));
                        }

                        _requiredScripts.Where(m => m.Position == position).Each(m => m.Each(r =>
                        builder.AppendHtml(r.ToSource(urlHelper, hostingEnvironment, options))));
                        break;
                    }

                case ResourceType.Style:
                    {
                        if (includeRequired)
                        {
                            ResourceHelper.StyleSource.Where(m => m.Value.Required && m.Value.Position == position)
                                                        .Each(m => m.Value.Each(r =>
                                                        {
                                                            builder.AppendHtml(r.ToSource(urlHelper, hostingEnvironment, options));
                                                        }));
                        }

                        _requiredStyles.Where(m => m.Position == position).Each(m => m.Each(r =>
                        {
                            builder.AppendHtml(r.ToSource(urlHelper, hostingEnvironment, options));
                        }));
                        break;
                    }
            }
            return builder;
        }

        public ScriptRegister Script
        {
            get
            {
                return new ScriptRegister(this, RegistScript);
            }
        }

        public StyleRegister Style
        {
            get
            {
                return new StyleRegister(this, RegistStyle);
            }
        }
        private void RegistStyle(ResourceCollection resource)
        {
            if (_requiredStyles.All(m => m.Name != resource.Name))
            {
                _requiredStyles.Add(resource);
            }
        }
        private void RegistScript(ResourceCollection resource)
        {
            if (_requiredScripts.All(m => m.Name != resource.Name))
            {
                _requiredScripts.Add(resource);
            }
        }
        public IApplicationContext ApplicationContext
        {
            get
            {
                return ViewContext.HttpContext.RequestServices.GetService<IApplicationContext>();
            }
        }
       
    }
}