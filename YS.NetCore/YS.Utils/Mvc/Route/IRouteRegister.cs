using System.Collections.Generic;

namespace YS.Utils.Mvc.Route
{
    public interface IRouteRegister
    {
        IEnumerable<RouteDescriptor> RegistRoute();
    }
}
