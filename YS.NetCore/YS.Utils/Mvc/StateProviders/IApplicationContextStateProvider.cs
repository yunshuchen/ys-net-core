﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Mvc.StateProviders
{
    public interface IApplicationContextStateProvider
    {
        string Name { get; }
        Func<IApplicationContext, T> Get<T>();
    }
}
