﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils
{
    public static class ExtSession
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) :
                                  JsonConvert.DeserializeObject<T>(value);
        }

        public static string GetStringValue(this ISession session, string key)
        {
            var value = session.GetString(key);
            return string.Concat(value, "");
        }
        public static bool GetBoolValue(this ISession session, string key,bool default_value)
        {
            var value = session.GetString(key);
            bool result = default_value;
            bool.TryParse(value, out result);
            return result;
        }
    }
}
