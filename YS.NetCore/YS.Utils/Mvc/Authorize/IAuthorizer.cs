
using YS.Utils.Models;

namespace YS.Utils.Mvc.Authorize
{
    public interface IAuthorizer
    {
        bool Authorize(string permission);
        bool Authorize(string permission, IUser user);
    }
}