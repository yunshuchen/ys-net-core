﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Mvc
{
    public static class ViewComponentExtensions
    {
        public static string GetViewPath(this ViewComponent viewComponent, string base_path)
        {


            var viewPath = $"/wwwroot/templates/{base_path}";

            var viewEngine = viewComponent.ViewContext.HttpContext.RequestServices.GetRequiredService<ICompositeViewEngine>();
            //var result = viewEngine.FindView(viewComponent.ViewContext, themeViewPath, isMainPage: false);
            var result = viewEngine.GetView("", viewPath, isMainPage: false);
            if (result.Success)
            {
                viewPath = viewPath;
            }
            return viewPath;
        }

        public static string GetViewPath(this Microsoft.AspNetCore.Http.HttpContext httpcontext, string base_path)
        {



            string viewPath = base_path;
            var viewEngine = httpcontext.RequestServices.GetRequiredService<ICompositeViewEngine>();
            //*var result = viewEngine.FindView(viewComponent.ViewContext, themeViewPath, isMainPage: false);
            //var result = viewEngine.GetView("", viewPath, isMainPage: false);
            //if (result.Success)
            //{
            //    base_path = base_path;
            //}
            return viewPath;
        }
    }
}
