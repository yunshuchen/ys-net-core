﻿//using YS.Utils.RepositoryPattern;
//using Microsoft.AspNetCore.Mvc;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace YS.Utils.Mvc
//{
//    /// <summary>
//    /// 基本控制器，增删改查
//    /// </summary>
//    /// <typeparam name="TEntity">实体类型</typeparam>
//    /// <typeparam name="TPrimarykey">主键类型</typeparam>
//    /// <typeparam name="TService">Service类型</typeparam>
//    [Area("Admin")]
//    [Route("Admin/[controller]/{action=Index}")]
//    [ApiExplorerSettings(IgnoreApi = true)]

//    public class BasicController<TEntity, TService> : Controller
//        where TEntity : class
//        where TService : IService<TEntity>
//    {
//        public TService Service;
//        public BasicController(TService service)
//        {
//            Service = service;
            
//        }

        
//        protected override void Dispose(bool disposing)
//        {
//            Service.Dispose();
//            base.Dispose(disposing);
//        }
//    }
//}
