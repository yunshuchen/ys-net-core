using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils
{
    public static class TypeUtils
    {
        public static bool IsGenericSubclassOf(this Type type, Type superType)
        {
            if (type.BaseType != null
                && !type.BaseType.Equals(typeof(object)))
            {
                if (type.BaseType.Equals(superType))
                {
                    return true;
                }
                return type.BaseType.IsGenericSubclassOf(superType);
            }

            return false;
        }
    }
}
