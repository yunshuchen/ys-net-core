using CacheManager.Core;
using Microsoft.AspNetCore.Http;
using System.Collections.Concurrent;

namespace YS.Utils.Cache
{
    public class DefaultCacheProvider : ICacheProvider
    {
        private readonly ConcurrentDictionary<string, object> _allCacheManager;
        public DefaultCacheProvider()
        {
            _allCacheManager = new ConcurrentDictionary<string, object>();
        }
        public ISkyCacheManager<T> Build<T>()
        {
            return (ISkyCacheManager<T>)_allCacheManager.GetOrAdd(typeof(T).FullName, k =>
            {
                 return new DefaultCacheManager<T>(CacheFactory.Build<T>(setting =>
                   {
                       setting.WithDictionaryHandle(k);
                       
                   }));
             });
        }
    }
}
