namespace YS.Utils.Cache
{
    public interface ICacheProvider
    {
        ISkyCacheManager<T> Build<T>();
    }
}
