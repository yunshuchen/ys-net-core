
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Notification
{
    public interface INotificationManager
    {
        void Send(Notice notice);
    }
}
