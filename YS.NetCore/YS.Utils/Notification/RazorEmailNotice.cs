
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Notification
{
    public class RazorEmailNotice : EmailNotice
    {
        public object Model { get; set; }
        /// <summary>
        /// Content root view path: ~/EmailTemplates/ResetPassword.cshtml.
        /// </summary>
        public string TemplatePath { get; set; }
    }
}
