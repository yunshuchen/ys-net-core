
using YS.Utils.SMTP;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace YS.Utils.Notification
{
    public interface ISmtpProvider
    {
        SmtpClient Get();

        SmtpSetting GetSetting();
    }
}
