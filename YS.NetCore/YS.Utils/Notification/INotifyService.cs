
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Notification
{
    public interface INotifyService
    {
        Type SupportType { get; }
        void Send(Notice notice);
    }
}
