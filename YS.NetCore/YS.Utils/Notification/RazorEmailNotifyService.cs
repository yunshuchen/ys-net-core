
using YS.Utils.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Notification
{
    /// <summary>
    /// Razor ģ����� �ʼ�����
    /// </summary>
    public class RazorEmailNotifyService : MailKitEmailNotify
    {
        private readonly IViewRenderService _viewRenderService;
        public RazorEmailNotifyService(IViewRenderService viewRenderService, ISmtpProvider smtpProvider, ILogger<MailKitEmailNotify> logger)
            : base(smtpProvider, logger)
        {
            _viewRenderService = viewRenderService;
        }
        public override Type SupportType => typeof(RazorEmailNotice);
        public override void Send(Notice notice)
        {
            RazorEmailNotice razorEmailNotice = notice as RazorEmailNotice;
            razorEmailNotice.IsHtml = true;
            razorEmailNotice.Content = _viewRenderService.Render(razorEmailNotice.TemplatePath, razorEmailNotice.Model);
            base.Send(razorEmailNotice);
        }
    }
}
