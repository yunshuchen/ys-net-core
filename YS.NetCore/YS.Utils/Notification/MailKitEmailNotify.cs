﻿using YS.Utils.Extend;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace YS.Utils.Notification
{
    public class MailKitEmailNotify : INotifyService
    {
        private readonly ISmtpProvider _smtpProvider;
        private readonly ILogger<MailKitEmailNotify> _logger;

        public MailKitEmailNotify(ISmtpProvider smtpProvider, ILogger<MailKitEmailNotify> logger)
        {
            _smtpProvider = smtpProvider;
            _logger = logger;
        }
        public virtual Type SupportType => typeof(EmailNotice);

        public virtual void Send(Notice notice)
        {

            var message = new MimeMessage();


            var email = notice as EmailNotice;
            var config_client = _smtpProvider.GetSetting();
            if (email.From.IsNullOrWhiteSpace())
            {
                email.From = config_client.Email;
            }
            message.From.Add(new MailboxAddress(email.DisplayName ?? email.From, email.From));
            if (email.To != null)
            {
                foreach (var item in email.To)
                {
                    message.To.Add(new MailboxAddress(item, item));
                }
            }
            if (email.Cc != null)
            {
                foreach (var item in email.Cc)
                {
                    message.Cc.Add(new MailboxAddress(item, item));
                }
            }

            if (email.Bcc != null)
            {
                foreach (var item in email.Bcc)
                {
                    message.Bcc.Add(new MailboxAddress(item, item));
                }
            }
            List<FileStream> list = null;
            message.Subject = email.Subject;
            Multipart multipart = new Multipart("mixed");
            if (email.IsHtml)
            {
                multipart.Add(new MimeKit.TextPart(TextFormat.Html)
                {
                    Text = email.Content
                });
            }
            else
            {
                multipart.Add(new MimeKit.TextPart(TextFormat.Plain)
                {
                    Text = email.Content
                });
            }
            if (email.Attachments != null)
            {
                list = new List<FileStream>(email.Attachments.Length);
                foreach (var path in email.Attachments)
                {
                    if (!File.Exists(path))
                    {
                        throw new FileNotFoundException("文件未找到", path);
                    }
                    var fileName = Path.GetFileName(path);
                    var fileType = MimeTypes.GetMimeType(path);
                    var contentTypeArr = fileType.Split('/');
                    var contentType = new ContentType(contentTypeArr[0], contentTypeArr[1]);
                    MimePart attachment = null;
                    var fs = new FileStream(path, FileMode.Open);
                    list.Add(fs);
                    attachment = new MimePart(contentType)
                    {
                        Content = new MimeContent(fs),
                        ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                        ContentTransferEncoding = ContentEncoding.Base64,
                    };
                    var charset = "GB18030";
                    attachment.ContentType.Parameters.Add(charset, "name", fileName);
                    attachment.ContentDisposition.Parameters.Add(charset, "filename", fileName);

                    foreach (var param in attachment.ContentDisposition.Parameters)
                        param.EncodingMethod = ParameterEncodingMethod.Rfc2047;
                    foreach (var param in attachment.ContentType.Parameters)
                        param.EncodingMethod = ParameterEncodingMethod.Rfc2047;
                    multipart.Add(attachment);

                }
            }
            message.Body = multipart;
            using (SmtpClient client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
        
                //Smtp服务器
                client.Connect(config_client.Host, config_client.Port, config_client.EnableSsl);
                //登录，发送
                //特别说明，对于服务器端的中文相应，Exception中有编码问题，显示乱码了
                client.Authenticate(config_client.Email, config_client.PassWord);
                client.Send(message);
                client.MessageSent += (object sender, MailKit.MessageSentEventArgs e) =>
                {
                    if (list != null && list.Count > 0)
                    {
                        //发送完毕销毁文件对象
                        foreach (var fs in list)
                        {
                            fs.Dispose();//手动高亮
                        }
                    }
                    //断开
                    client.Disconnect(true);
                    Console.WriteLine("发送邮件成功");
                };
            }
        }

    }
}
