using YS.Utils.Models;
using Microsoft.AspNetCore.Hosting;
using System;

namespace YS.Utils
{
    public interface IApplicationContext
    {
        IUser CurrentUser { get; }
        IUser CurrentCustomer { get; }

        ISite_Member MemberCustomer { get; }

        IWebHostEnvironment HostingEnvironment { get; }
        bool IsAuthenticated { get; }
        T As<T>() where T : class, IApplicationContext;
        T Get<T>(string name);
        void Set(string name, object value);
    }
}
