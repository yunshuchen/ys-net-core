﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.Encrypt
{
    public interface IMd5Hash
    {
        string MemberUserPwd(string Account, string pwd);
        string AdminUserPwd(string Account, string pwd);

        string GetMd5String(string Str);
    }
}
