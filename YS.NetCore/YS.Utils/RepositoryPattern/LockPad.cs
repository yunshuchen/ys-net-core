﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils
{
    public class LockPad
    {
        public string p_id { get; private set; }

        public LockPad(string key)
        {
            p_id = key;
        }

        public override string ToString()
        {
            return "lock of " + p_id;
        }
    }
}
