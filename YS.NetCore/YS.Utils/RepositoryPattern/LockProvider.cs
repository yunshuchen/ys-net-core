﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Utils
{


    /// <summary>
    /// 线程lock提供对象
    /// </summary>
    public class LockProvider
    {
        private static readonly ConcurrentDictionary<string, LockPad> padLocks = new ConcurrentDictionary<string, LockPad>();



        public static LockPad LookupPadLockByKey(string key)
        {
            if (!padLocks.ContainsKey(key))
            {
                var obj = new LockPad(key);
                padLocks.TryAdd(key, obj);
            }
            LockPad result = null;
            padLocks.TryGetValue(key, out result);
            return result;
        }

        public static void RemoveLockKey(string key)
        {
            LockPad result = null;
            padLocks.TryRemove(key, out result);
        }

   




        /// <summary>
        /// 根据key 锁定对象
        /// </summary>
        /// <returns></returns>
        public static LockPad LookUp(string key)
        {
            return LookupPadLockByKey(key);
        }
    }
}
