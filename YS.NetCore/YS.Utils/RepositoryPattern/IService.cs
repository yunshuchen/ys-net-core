using System;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace YS.Utils.RepositoryPattern
{
    public interface IService<T> : IDisposable
        where T : class
    {


        T GetByOne(string OrderBy, Expression<Func<T, bool>> predicate);
        T GetByOne(Expression<Func<T, bool>> predicate);
        int Count(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">123</param>
        /// <returns></returns>
        int Count(string ExpressionsSql, params object[] ExpressionPartams);

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">查询条件</param>
        /// <param name="ExpressionPartams">查询条件</param>
        /// <returns></returns>
        int Count(string ExpressionsSql);

        List<T> GetList(string keySelector, List<Expression<Func<T, bool>>> predicate);
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        List<T> GetList(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams);
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <returns></returns>
        List<T> GetList(string OrderBy, string ExpressionsSql);


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, List<Expression<Func<T, bool>>> predicate);

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql);
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <param name="ExpressionPartams"></param>
        /// <returns></returns>
        PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams);


        /// <summary>
        /// 修改实体
        /// 修改依据为主键
        /// 修改项为主键之外的所有字段
        /// </summary>
        /// <param name="content"></param>
        void Modify(T content);
        void Modify(IEnumerable<T> entities);
        int Modify(Expression<Func<T, bool>> condition, Expression<Func<T, T>> content);


        int Delete(Expression<Func<T, bool>> condition);

        int Delete(T model);
        int Delete(IEnumerable<T> entities);

        T Insert(T item);
        void BulkInsert(IEnumerable<T> model);

        #region OtherTable


        #region OtherTable

 
        T1 GetByOne<T1>(string OrderBy, Expression<Func<T1, bool>> predicate) where T1 : class;
        T1 GetByOne<T1>(Expression<Func<T1, bool>> predicate) where T1 : class;
        int Count<T1>(Expression<Func<T1, bool>> predicate) where T1 : class;

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">123</param>
        /// <returns></returns>
        int Count<T1>(string ExpressionsSql, params object[] ExpressionPartams) where T1 : class;

        /// <summary>
        /// 获取数量
        /// </summary>
        /// <param name="ExpressionsSql">查询条件</param>
        /// <param name="ExpressionPartams">查询条件</param>
        /// <returns></returns>
        int Count<T1>(string ExpressionsSql) where T1 : class;

        List<T1> GetList<T1>(string keySelector, List<Expression<Func<T1, bool>>> predicate) where T1 : class;
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <param name="ExpressionPartams">125</param>
        /// <returns></returns>
        List<T1> GetList<T1>(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams) where T1 : class;
        /// <summary>
        /// 查询列表根据Sql条件
        /// </summary>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql">id=@0</param>
        /// <returns></returns>
        List<T1> GetList<T1>(string OrderBy, string ExpressionsSql) where T1 : class;


        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, List<Expression<Func<T1, bool>>> predicate) where T1 : class;

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <returns></returns>
        PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string ExpressionsSql) where T1 : class;
        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="OrderBy"></param>
        /// <param name="ExpressionsSql"></param>
        /// <param name="ExpressionPartams"></param>
        /// <returns></returns>
        PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams) where T1 : class;


        /// <summary>
        /// 修改实体
        /// 修改依据为主键
        /// 修改项为主键之外的所有字段
        /// </summary>
        /// <param name="content"></param>
        void Modify<T1>(T1 content) where T1 : class;
        void Modify<T1>(IEnumerable<T1> entities) where T1 : class;
        int Modify<T1>(Expression<Func<T1, bool>> condition, Expression<Func<T1, T1>> content) where T1 : class;


        int Delete<T1>(Expression<Func<T1, bool>> condition) where T1 : class;

        int Delete<T1>(T1 model) where T1 : class;
        int Delete<T1>(IEnumerable<T1> entities) where T1 : class;

        T1 Insert<T1>(T1 item) where T1 : class;
        void BulkInsert<T1>(IEnumerable<T1> model) where T1 : class;
        #endregion
        #endregion
        IQueryable<TOther> OtherTable<TOther>() where TOther : class;

        IQueryable<T> Table { get; }


        IQueryable<T> ToJoin(string OrderBy, List<Expression<Func<T, bool>>> predicate);

        DbContext dbContext { get; }
    }
}