
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using YS.Utils.Repositorys;

namespace YS.Utils.RepositoryPattern
{

    public abstract class ServiceBase<T, LogT> : IService<T>
        where T : class
        where LogT : Microsoft.Extensions.Logging.ILogger<T>
    {
        #region 
        private readonly Microsoft.Extensions.Logging.ILogger<T> log;

        private readonly IRepository<T> useService;
        #endregion

        public ServiceBase(IRepository<T> _useService, Microsoft.Extensions.Logging.ILogger<T> _log)
        {
            log = _log;
            useService = _useService;
        }

        public IQueryable<T> Table => useService.Table;
        
  
        public DbContext dbContext => useService.dbContext;



        public virtual void BulkInsert(IEnumerable<T> model)
        {
            useService.BulkInsert(model);
        }

        public virtual int Count(Expression<Func<T, bool>> predicate)
        {
            return useService.Count(predicate);
        }

        public virtual int Count(string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.Count(ExpressionsSql, ExpressionPartams);
        }

        public virtual int Count(string ExpressionsSql)
        {
            return useService.Count(ExpressionsSql);
        }
        public virtual int Delete(T model)
        {
            useService.Delete(model);
            return 1;
        }


        public virtual int Delete(IEnumerable<T> entities)
        {
            useService.Delete(entities);
            return 1;
        }

        public virtual int Delete(Expression<Func<T, bool>> condition)
        {
            return useService.Delete(condition);
        }

        public void Dispose()
        {

        }

        public virtual T GetByOne(string OrderBy, Expression<Func<T, bool>> predicate)
        {
            return useService.GetOne(OrderBy, predicate);
        }

        public virtual T GetByOne(Expression<Func<T, bool>> predicate)
        {
            return useService.GetOne("", predicate);
        }

        public virtual List<T> GetList(string OrderBy, List<Expression<Func<T, bool>>> predicate)
        {
            return useService.GetList(OrderBy, predicate);
        }

        public virtual List<T> GetList(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.GetList(OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public virtual List<T> GetList(string OrderBy, string ExpressionsSql)
        {
            return useService.GetList(OrderBy, ExpressionsSql);
        }

        public virtual PageOf<T> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<T, bool>>> predicate)
        {
            return useService.GetPageList(iPage, PageSize, keySelector, predicate);
        }

        public virtual PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql)
        {
            return useService.GetPageList(iPage, PageSize, OrderBy, ExpressionsSql);
        }

        public virtual PageOf<T> GetPageList(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams)
        {
            return useService.GetPageList(iPage, PageSize, OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public virtual T Insert(T item)
        {
            return useService.Insert(item);
        }

        public virtual int Modify(Expression<Func<T, bool>> condition, Expression<Func<T, T>> content)
        {
            return useService.Update(condition, content);
        }

        public virtual void Modify(T content)
        {
            useService.Update(content);
        }

        public virtual void Modify(IEnumerable<T> entities)
        {
            useService.Update(entities);
        }

        public virtual IQueryable<TOther> OtherTable<TOther>() where TOther : class
        {
            return useService.OtherTable<TOther>();
        }

        public void Test()
        {

            //log LEFT JOIN res
            //var joinResults = useService.OtherTable<sys_log>()
            //                .GroupJoin(useService.OtherTable<sys_resource>(), log => log.log_module, res => res.res_code, (log, res) => new { log, res })
            //                .SelectMany(combin => combin.res.DefaultIfEmpty(), (log, res) => new
            //                {
            //                    log.log.id,
            //                    log.log.log_module,
            //                    res.res_name,
            //                    diff_ddat = Microsoft.EntityFrameworkCore.EF.Functions.DateDiffDay(DateTime.Now, log.log.log_time)
            //                }).ToList();



            //return useService.OtherTable<sys_log>().Join(useService.OtherTable<sys_resource>(), log => log.log_module, res => res.res_code, (log, res) => new
            //{
            //    log.id,
            //    log.log_module,
            //    res.res_name
            //}).ToList();

            //var result = from l in useService.OtherTable<sys_log>()
            //             join r in useService.OtherTable<sys_resource>()
            //             on l.log_module equals r.res_code
            //             select new
            //             {
            //                 l.id,
            //                 l.log_module,
            //                 r.res_name
            //             };

            //result.ToList();
        }

        public IQueryable<T> ToJoin(string OrderBy, List<Expression<Func<T, bool>>> predicate)
        {
            return useService.ToJoin(OrderBy, predicate);
        }

        #region OtherTable



        public virtual void BulkInsert<T1>(IEnumerable<T1> model) where T1 : class
        {
            useService.BulkInsert<T1>(model);
        }

        public virtual int Count<T1>(Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return useService.Count<T1>(predicate);
        }

        public virtual int Count<T1>(string ExpressionsSql, params object[] ExpressionPartams) where T1 : class
        {
            return useService.Count<T1>(ExpressionsSql, ExpressionPartams);
        }

        public virtual int Count<T1>(string ExpressionsSql) where T1 : class
        {
            return useService.Count<T1>(ExpressionsSql);
        }
        public virtual int Delete<T1>(T1 model) where T1 : class
        {
            useService.Delete<T1>(model);
            return 1;
        }


        public virtual int Delete<T1>(IEnumerable<T1> entities) where T1 : class
        {
            useService.Delete<T1>(entities);
            return 1;
        }

        public virtual int Delete<T1>(Expression<Func<T1, bool>> condition) where T1 : class
        {
            return useService.Delete<T1>(condition);
        }



        public virtual T1 GetByOne<T1>(string OrderBy, Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return useService.GetOne<T1>(OrderBy, predicate);
        }

        public virtual T1 GetByOne<T1>(Expression<Func<T1, bool>> predicate) where T1 : class
        {
            return useService.GetOne<T1>("", predicate);
        }

        public virtual List<T1> GetList<T1>(string OrderBy, List<Expression<Func<T1, bool>>> predicate) where T1 : class
        {
            return useService.GetList<T1>(OrderBy, predicate);
        }

        public virtual List<T1> GetList<T1>(string OrderBy, string ExpressionsSql, params object[] ExpressionPartams) where T1 : class
        {
            return useService.GetList<T1>(OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public virtual List<T1> GetList<T1>(string OrderBy, string ExpressionsSql) where T1 : class
        {
            return useService.GetList<T1>(OrderBy, ExpressionsSql);
        }

        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string keySelector, List<Expression<Func<T1, bool>>> predicate) where T1 : class
        {
            return useService.GetPageList<T1>(iPage, PageSize, keySelector, predicate);
        }

        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string ExpressionsSql) where T1 : class
        {
            return useService.GetPageList<T1>(iPage, PageSize, OrderBy, ExpressionsSql);
        }

        public virtual PageOf<T1> GetPageList<T1>(int iPage, int PageSize, string OrderBy, string ExpressionsSql, params object[] ExpressionPartams) where T1 : class
        {
            return useService.GetPageList<T1>(iPage, PageSize, OrderBy, ExpressionsSql, ExpressionPartams);
        }

        public virtual T1 Insert<T1>(T1 item) where T1 : class
        {
            return useService.Insert<T1>(item);
        }

        public virtual int Modify<T1>(Expression<Func<T1, bool>> condition, Expression<Func<T1, T1>> content) where T1 : class
        {
            return useService.Update<T1>(condition, content);
        }

        public virtual void Modify<T1>(T1 content) where T1 : class
        {
            useService.Update<T1>(content);
        }

        public virtual void Modify<T1>(IEnumerable<T1> entities) where T1 : class
        {
            useService.Update<T1>(entities);
        }


        public IQueryable<T1> ToJoin<T1>(string OrderBy, List<Expression<Func<T1, bool>>> predicate) where T1 : class
        {
            return useService.ToJoin<T1>(OrderBy, predicate);
        }

    
        #endregion


            public Expression<Func<T1, bool>> GetSitePredicate<T1>()
        {


            var predicate1 = LinqKit.PredicateBuilder.New<T1>(true);

            long site_id = 0;// ApplicationContext.site_id;
            if (site_id >= 0)
            {

                if (typeof(T1).IsGenericSubclassOf(typeof(Models.IUser)))
                {
                    var p = Expression.Parameter(typeof(T1), "x");

                    var e = (Expression)System.Linq.Dynamic.Core.DynamicExpressionParser.ParseLambda(new[] { p }, null, $"x.site_id={site_id}");

                    var typedExpression = (Expression<Func<T1, bool>>)e;


                    //// t
                    //var parameter = Expression.Parameter(typeof(T1), "t");

                    //// t.Total
                    //var propertyExpression = Expression.PropertyOrField(parameter, "site_id");

                    //// 100.00M
                    //var constant = Expression.Constant(site_id, typeof(long));

                    //// t.Total == 100.00M 
                    //var equalExpression = Expression.Equal(propertyExpression, constant);

                    //// t => t.Total == 100.00M
                    //var lambda = Expression.Lambda(equalExpression, parameter);
                    //var c = (Func<T1, bool>)lambda.Compile();

                    //return c;
                    return typedExpression;
                }

            }
            return predicate1;

        }


    }
}