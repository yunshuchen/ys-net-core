
namespace YS.Utils.Constant
{
    public enum SourceType
    {
        None = 1,
        Dictionary = 2,
        ViewData = 3
    }
    public enum RecordStatus
    {
        /// <summary>
        /// �
        /// </summary>
        Active = 1,
        /// <summary>
        /// ��Ч
        /// </summary>
        InActive = 2
    }
    public enum ActionType
    {
        Create = 1,
        Update = 2,
        Delete = 3,
        Design = 4,
        Publish = 5,
        Unattached = 6,
        Continue = 7,
        UnAttach = 8
    }
    public enum UserType
    {
        Administrator = 1,
        Customer = 2
    }
}
