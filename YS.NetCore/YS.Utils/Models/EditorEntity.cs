

using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace YS.Utils.Models
{
    public class EditorEntity
    {

        public virtual string Title { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// 是否通过
        /// </summary>
        public virtual StatusEnum? Status { get; set; }
        /// <summary>
        /// 创建人ID
        /// </summary>
        public virtual string CreateBy { get; set; }
        /// <summary>
        /// 创建人
        /// </summary>
        public virtual string CreatebyName { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        public virtual DateTime? CreateDate { get; set; }
        /// <summary>
        /// 修改人ID
        /// </summary>
        public virtual string LastUpdateBy { get; set; }
        /// <summary>
        /// 最后修改人
        /// </summary>
        public virtual string LastUpdateByName { get; set; }
        /// <summary>
        /// 最后更新日期
        /// </summary>
        public virtual DateTime? LastUpdateDate { get; set; }
        [NotMapped, JsonIgnore]
        public virtual Constant.ActionType? ActionType { get; set; }
    }

    /// <summary>
    /// 多语言实体类
    /// </summary>
    public class MultiLangEntity
    {
        public long row_key { get; set; }
        public long id { get; set; }

        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        public int lang_flag { get; set; }
    }

}
