


namespace YS.Utils.Models
{
    public interface IImage
    {
        string ImageUrl { get; set; }
        string ImageThumbUrl { get; set; }
    }
}
