
using System;
using System.Collections.Generic;


namespace YS.Utils.Models
{
    public class IPlatform_site_manager
    {

        /// <summary>
        /// 主键自增长
        /// </summary>
       public  long id { get; set; }
        /// <summary>
        /// 站点名称
        /// </summary>

        public string site_name { get; set; }
        /// <summary>
        /// 站点说明
        /// </summary>

        public string site_desc { get; set; }
        /// <summary>
        /// 模版代号
        /// </summary>

        public string tpl { get; set; }

        /// <summary>
        /// 过期状态,1长期有效,0时间內有效
        /// </summary>

        public StatusEnum exp_status { get; set; }
        /// <summary>
        /// 过期时间
        /// </summary>

        public string exp_time { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>

        public DateTime create_time { get; set; }
        /// <summary>
        /// 域名多个用;隔开
        /// </summary>

        public string site_domain { get; set; }


        /// <summary>
        /// 联系人名称
        /// </summary>

        public string contact_name { get; set; }


        /// <summary>
        /// 联系人电话
        /// </summary>

        public string tel { get; set; }

        /// <summary>
        /// 电子邮件
        /// </summary>

        public string email { get; set; }


        /// <summary>
        /// 站点标识
        /// 用于站点上传与站点资源文件
        /// Guid.NewGuid().ToString("N")
        /// </summary>

        public string site_guid { get; set; }

        /// <summary>
        /// 用户设置是否启用
        /// </summary>

        public StatusEnum status { get; set; }
        /// <summary>
        /// 平台设置是否启用
        /// </summary>

        public StatusEnum platform_status { get; set; }

        /// <summary>
        /// 平台设置是否启用
        /// </summary>

        public bool multi_language { get; set; }
    }
}
