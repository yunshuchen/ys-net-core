
using System;
using System.Collections.Generic;


namespace YS.Utils.Models
{
    public interface IUser
    {

         long id { get; set; }
         string account { get; set; }
         string password { get; set; }
         string alias_name { get; set; }

        string chr_name { get; set; }
        StatusEnum user_status { get; set; }
         string creator_name { get; set; }
         string mobile_phone { get; set; }
        string email { get; set; }
        DateTime create_time { get; set; }
         DateTime last_login_time { get; set; }
         string last_login_ip { get; set; }
         int login_count { get; set; }
         string token { get; set; }
         DateTime last_online_time { get; set; }
         DateTime next_set_pwd { get; set; }
        int account_type { get; set; }
        /// <summary>
        /// 最后刷新时间
        /// </summary>
        DateTime last_refresh { get; set; }


         DateTime put_out_login { get; set; }
        /// <summary>
        /// Authentication 类型
        /// </summary>

         string AuthenticationType { get; set; }


         bool IsAuthenticated { get; set; }
  
         string Name { get { return account; } }

         string ResetToken { get; set; }

         DateTime? ResetTokenDate { get; set; }
    }
}
