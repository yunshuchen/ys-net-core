﻿using System;
using System.Linq;
using System.Collections.Generic;

using YS.Utils;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace YS.Utils.Models
{

    public interface ISite_Member
    {

        [Description("主键")]
        long id { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [Description("昵称")]
        string nick_name { get; set; }
        /// <summary>
        /// 账号
        /// </summary>
        [Description("账号")]
        string chr_account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Description("密码")]
        string chr_pwd { get; set; }
        

        /// <summary>
        /// 头像图片
        /// </summary>
        [Description("头像图片")]
        string avatar_url { get; set; }
        /// <summary>
        /// 0 未知;1男;2女
        /// </summary>
        [Description("0 未知;1男;2女")]
        GenderEnum gender { get; set; }
        /// <summary>
        /// 国家
        /// </summary>
        [Description("国家")]
        string country { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [Description("省份")]
        string province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        [Description("城市")]
        string city { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Description("电话")]
        string tel_no { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string open_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string union_id { get; set; }

        /// <summary>
        /// 登录的sessionKey
        /// </summary>
        /// </summary>
        string session_key { get; set; }


        [Description("登录的sessionKey")]
        string session_id { get; set; }


        /// <summary>
        /// 是否已补全信息
        /// </summary>
        /// </summary>

        StatusEnum comple_userinfo { get; set; }
        /// <summary>
        /// 最后补全信息时间
        DateTime last_comple_date { get; set; }


        /// <summary>
        /// 最后登录时间
        /// </summary>
        DateTime last_login_time { get; set; }


        /// <summary>
        /// JWT Token
        /// </summary>
        string token { get; set; }

        /// <summary>
        /// 是否需要补全用户信息
        /// </summary>

        StatusEnum comple_status { get; set; }

        public string tags { get; set; }





        /// <summary>
        /// 员工的标签属性
        /// </summary>
        List<string> Tag_Codes
        {
            get;
        }
        /// <summary>
        /// 最后补全信息时间
        /// </summary>
        public ReViewStatusEnum review_status { get; set; }
        /// <summary>
        /// 身份证号码
        /// </summary>
         string id_card { get; set; }
        /// <summary>
        /// 权益卡号
        /// </summary>
         string card_no { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
         string real_name { get; set; }
    }
}
