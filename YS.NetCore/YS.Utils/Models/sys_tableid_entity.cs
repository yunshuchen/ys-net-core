﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChinaSky.CMS.Net.Utils
{
    [Serializable]
    [Table("sys_tableid")]
    public class sys_tableid_entity
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string table_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long max_id { get; set; }
    }
}
