﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Utils.DBFunctions
{
    //https://www.itdos.com/CSharp/20150408/0127321.html
    public class CustomFuncations
    {
        [DbFunction(Name = "GetDistance", Schema = "dbo")]
        public static int GetDistance(double lngBegin, double latBegin, double lngEnd, double latEnd)
        {
            throw new NotSupportedException("Direct calls are not supported.");
        }

        /// <summary>
        /// mysql 时间格式化
        /// </summary>
        /// <param name="value"></param>
        /// <param name="format">%Y-%m-%d</param>
        /// <returns></returns>
        [DbFunction("DATE_FORMAT", Schema = null)]
        public static string DATE_FORMAT(DateTime value, string format)
        {
            throw new NotImplementedException();
        }
    }
}
