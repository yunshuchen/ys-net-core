
namespace YS.Net.Filter
{
    public enum PageViewMode
    {
        Publish = 1,
        Design = 2,
        PreView = 3,
        ViewOnly = 4
    }
}
