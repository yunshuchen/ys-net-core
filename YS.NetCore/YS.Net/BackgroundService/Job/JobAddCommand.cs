﻿namespace YS.Net.BackgroundService
{
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;
    using Queue;

    public class JobAddCommand : IRequestHandler<JobAddContext,JobVewModel>
    {
        private readonly IBJobStore _jobStore;
        private readonly IJobQueue _jobQueue; 

        public JobAddCommand(IBJobStore jobStore, IJobQueue jobQueue)
        {
            _jobStore = jobStore;
            _jobQueue = jobQueue;
        }

        public async Task<JobVewModel> Handle(JobAddContext jobContext, CancellationToken cancellationToken)
        {
            var job = new Job
            {

                JobStatus = JobStatus.Created,
                data = jobContext.data,
                action_type = jobContext.action_type

            };
            job = await _jobStore.Save(job);

            var jobTask = new JobTask
            {
                id = job.id
            };
            await _jobQueue.Enqueue(jobTask);

            job.JobStatus = JobStatus.Queued;
            job = await _jobStore.Save(job);

            var jobVewModel = new JobVewModel
            {
                id = job.id,
                status = job.JobStatus.ToString()
            };

            return jobVewModel;
        }
    }
}
