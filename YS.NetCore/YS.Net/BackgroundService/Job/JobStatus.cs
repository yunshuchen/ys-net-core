﻿namespace YS.Net.BackgroundService
{
    public enum JobStatus
    {
        Created,
        Queued,
        InProgress,
        Compleated,
        Failed
    }
}
