﻿namespace YS.Net.BackgroundService
{
    using System.Collections.Generic;
    using System.ComponentModel;

    using MediatR;
    public class JobAddContext : IRequest<JobVewModel>
    {

        public JobAddContext()
        {

        }
        public object data { get; set; }
        public string action_type { get; set; }
    }
}
