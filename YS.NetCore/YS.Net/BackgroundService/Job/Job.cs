﻿namespace YS.Net.BackgroundService
{
    using System.Collections.Generic;

    public class Job
    {
        public string id { get; set; }

        

        public JobStatus JobStatus { get; set; }


        public string messages { get; set; }

        public string action_type { get; set; }
        public object data { get; set; }

    }
}
