﻿using System.Collections.Generic;

namespace YS.Net.BackgroundService
{
    public class JobVewModel
    {
        public string id { get; set; }
        public string status { get; set; }

        public string messages { get; set; }

        public string action_type { get; set; }

        public object data { get; set; }

    }
}
