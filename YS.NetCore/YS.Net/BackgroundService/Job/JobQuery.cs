﻿namespace YS.Net.BackgroundService
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using MediatR;

    public class JobQuery : IRequestHandler<JobQueryContext, JobVewModel>
    {
        private readonly IBJobStore _jobStore;

        public JobQuery(IBJobStore jobStore)
        {
            _jobStore = jobStore;
        }

        public async Task<JobVewModel> Handle(JobQueryContext jobQueryContext, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(jobQueryContext.Id))
            {
                throw new ArgumentException($"Parameter {nameof(jobQueryContext.Id)} is emty.");
            }
            var job = await _jobStore.Get(jobQueryContext.Id);
            var jobViewModel = new JobVewModel
            {
                id = job.id,
                status = job.JobStatus.ToString(),
                messages = job.messages,
                action_type = job.action_type,
                data=job.data
            };
            return jobViewModel;
        }
    }
}
