﻿namespace YS.Net.BackgroundService.Queue
{
    using MediatR;
    public class EnqueueJobContext : IRequest
    {
        public string id { get; set; }
    }
}
