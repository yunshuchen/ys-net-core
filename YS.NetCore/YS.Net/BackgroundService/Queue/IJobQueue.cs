﻿namespace YS.Net.BackgroundService.Queue
{
    using System.Threading.Tasks;

    public interface IJobQueue
    {
        Task Enqueue(JobTask jobTask);
        Task<JobTask> Dequeue();
    }
}
