﻿namespace YS.Net.BackgroundService
{
    using System.Threading.Tasks;

    public interface IBJobStore
    {
        Task<Job> Save(Job job);
        Task<Job> Get(string id);
    }
}
