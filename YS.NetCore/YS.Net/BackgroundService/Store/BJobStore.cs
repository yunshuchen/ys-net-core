﻿namespace YS.Net.BackgroundService
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;
    using Exceptions;

    public class BJobStore : IBJobStore
    {
        private readonly ConcurrentDictionary<string, Job> _jobs;

        public BJobStore()
        {
            _jobs = new ConcurrentDictionary<string, Job>();
        }

        public async Task<Job> Save(Job job)
        {
            if (job == null)
            {
                throw new ArgumentNullException(nameof(job));
            }

            if (string.IsNullOrWhiteSpace(job.id))
            {
                job.id = Guid.NewGuid().ToString();
            }

            _jobs.AddOrUpdate(job.id, job, (key, existingJob) => job);

            return job;
        }

        public async Task<Job> Get(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException(nameof(id));
            }

            if (_jobs.TryGetValue(id, out var job))
            {
                return job;
            }

            throw new NotFoundException($"Could not found {nameof(Job)} with {nameof(id)} = {id}");
        }
    }
}
