﻿namespace YS.Net.BackgroundService.Worker
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using System.Threading.Tasks;
    using YS.Net.Common;
    using YS.Net.Services;

    using Microsoft.Extensions.Hosting;
    using Queue;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using System.Data;

    using YS.Utils;
    using YS.Net.Models;

    using System.Collections.Concurrent;
    using YS.Utils.Models;
    using Microsoft.Extensions.Logging;

    public class MyBackgroundWorker : BackgroundService
    {

        private const int RunItervalInSeconds = 5;
        private const int DownloadParallelismDegree = 3;
        private readonly IJobQueue _jobQueue;
        private readonly IBJobStore _jobStore;



        private ISiteFileProvider fileProdider;

        public IServiceScopeFactory serviceScopeFactory;

        private IApplicationContextAccessor httpContextAccessor;
        public MyBackgroundWorker(
            IJobQueue jobQueue
            , IBJobStore jobStore
            , IServiceScopeFactory _serviceScopeFactory
            )
        {
            serviceScopeFactory = _serviceScopeFactory;


            _jobQueue = jobQueue;
            _jobStore = jobStore;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var jobTask = await _jobQueue.Dequeue();
                if (jobTask != null)
                {
                    var job = await _jobStore.Get(jobTask.id);
                    job.JobStatus = JobStatus.InProgress;
                    job = await _jobStore.Save(job);
                    
                  

                    if (job.action_type == "send_notice")
                    {
                        var WxAppId = job.data.GetModelValue<string>("WxAppId");
                        var template_id = job.data.GetModelValue<string>("template_id");
                        var msg_data = job.data.GetModelValue<object>("msg_data");
                        var notice = job.data.GetModelValue<site_notice>("notice");
                        var url = job.data.GetModelValue<string>("url");
                        string recipient=notice.recipient;
                        var condition = new List<System.Linq.Expressions.Expression<Func<site_member, bool>>>();
                        if (recipient == "all")
                        {
                        }
                        else if (recipient == "member")
                        {
                            condition.Add(a => a.review_status == ReViewStatusEnum.Pass);
                        }
                        else
                        {
                            var tags = recipient.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                            var predicate = PredicateBuilder2.False<site_member>();
                            foreach (var item in tags)
                            {
                                
                                if (item.StartsWith("t"))
                                {

                                    predicate = predicate.Or(a => ("," + a.tags + ",").Contains("," + item + ","));
                                }
                                else if (item.StartsWith("p"))
                                {
                                    long id = item.Replace("p", "").MyToLong();
                                    if (id > 0)
                                    {
                                        predicate = predicate.Or(a => a.id == id);
                                    }
                                }
                            }
                            condition.Add(predicate);

                        }

                        using (var scope = serviceScopeFactory.CreateScope())
                        {
                            var memberservice = scope.ServiceProvider.GetService<ISiteMemberService>();

                            var result = memberservice.GetList("id desc", condition);

                            foreach (var item in result)
                            {if (item.open_id != "")
                                {
                                    var send_result = Senparc.Weixin.MP.AdvancedAPIs.TemplateApi.SendTemplateMessageAsync(WxAppId, item.open_id, template_id, url, msg_data);
                                }
                            }
         
                        }

                    }

                    
                  
                }

                await Task.Delay(TimeSpan.FromSeconds(RunItervalInSeconds), stoppingToken);


            }

        }

    }
}
