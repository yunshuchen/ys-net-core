﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace YS.Net
{
    public interface IMyCache
    {

        string GetString(string key, string default_value = "");

        int GetInt(string key, int default_value = 0);
        T Get<T>(string key) where T : class;

        void Remove(string key);

        void RemoveAsync(string key);

        void SetString(string key, string value);


        void SetString(string key, string value,TimeSpan ts);


        Task SetStringAsync(string key, string value, CancellationToken token = default);

        void SetInt(string key, int value);

        Task SetIntAsync(string key, int value, CancellationToken token = default);


        void Set<T>(string key, T value) where T : class;


        void Set<T>(string key, T value, TimeSpan ts) where T : class;



        Task SetAsync<T>(string key, T value, CancellationToken token = default) where T : class;


        //void SetBytes<T>(string key, T value, TimeSpan? ts) where T : class;
        //T GetBytes<T>(string key) where T : class;

    }
}
