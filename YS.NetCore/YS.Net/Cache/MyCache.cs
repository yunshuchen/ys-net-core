﻿using YS.Utils;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using YS.Utils.DI;
using System.IO;

namespace YS.Net
{
    [SingletonDI]
    public class MyCache : IMyCache
    {
        
        private readonly IDistributedCache _cache;

        public MyCache(IDistributedCache cache)
        {
            _cache = cache;
        }

        public string GetString(string key,string default_value="")
        {
            var byte_value = _cache.Get(key);
            if (byte_value == null)
            {
                return default_value;
            }
            string value = Encoding.UTF8.GetString(byte_value);
            return value;
        }

        public int GetInt(string key,int default_value= 0)
        {
            var byte_value = _cache.Get(key);
            if (byte_value == null)
            {
                return default_value;
            }
            string value = Encoding.UTF8.GetString(byte_value);
            return value.MyToInt();
        }
        public T Get<T>(string key) where T : class
        {
            var byte_value = _cache.Get(key);
            if (byte_value == null)
            {
                return default(T);
            }
            string value = Encoding.UTF8.GetString(byte_value);
            if (value.MyToString() == "")
            {
                return default(T);
            }
            return JsonHelper.ToObject<T>(value);
        }
      
        public void Remove(string key)
        {
            _cache.Remove(key);
        }
        public void RemoveAsync(string key)
        {
            _cache.RemoveAsync(key);
        }
        public void SetString(string key, string value)
        {
            
            _cache.Set(key, Encoding.UTF8.GetBytes(value), new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddDays(1)
            });
        }
        public Task SetStringAsync(string key, string value, CancellationToken token = default)
        {
            _cache.SetAsync(key, Encoding.UTF8.GetBytes(value), new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddDays(1)
            }, token);
            return Task.CompletedTask;
        }
        public void SetInt(string key, int value)
        {
            SetString(key, value.MyToString());
        }

        public Task SetIntAsync(string key, int value,  CancellationToken token = default)
        {
            return SetStringAsync(key, value.MyToString(), token);
        }

        public void Set<T>(string key, T value) where T :class
        {
            _cache.Set(key, Encoding.UTF8.GetBytes(JsonHelper.ToJson(value)), new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddDays(1)
            });
        }

        public Task SetAsync<T>(string key, T value, CancellationToken token = default) where T : class
        {
            _cache.SetAsync(key, Encoding.UTF8.GetBytes(JsonHelper.ToJson(value)), new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now.AddDays(1),
                
            }, token);
            return Task.CompletedTask;
        }

        public void SetString(string key, string value, TimeSpan ts)
        {
            _cache.Set(key, Encoding.UTF8.GetBytes(value), new DistributedCacheEntryOptions()
            {
              
                AbsoluteExpirationRelativeToNow=ts
            });
        }

        public void Set<T>(string key, T value, TimeSpan ts) where T : class
        {
            _cache.Set(key, Encoding.UTF8.GetBytes(JsonHelper.ToJson(value)), new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = ts
            });
        }

        //public void SetBytes<T>(string key, T value, TimeSpan? ts) where T : class
        //{
        //    if (ts == null)
        //    {
        //        ts = TimeSpan.FromDays(60);
        //    }
        //    MemoryStream stream = new MemoryStream();
        //    var binFormat = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        //    binFormat.Serialize(stream, value);
        //    var bytes = stream.ToArray();
        //    _cache.Set(key, bytes, new DistributedCacheEntryOptions()
        //    {
        //        AbsoluteExpirationRelativeToNow = ts
        //    });
        //}

        //public T GetBytes<T>(string key) where T : class
        //{

        //    var bytes = _cache.Get(key);
        //    if (bytes == null)
        //    {
        //        return default;
        //    }
        //    var binFormat = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

        //    var ms = new MemoryStream(bytes);
        //    var result = (T)binFormat.Deserialize(ms);
        //    return result;
        //}
    }
}
