﻿using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace YS.Net
{
    public static class FPage
    {



        public static IHtmlContent ExtSelect(this IHtmlHelper Html, string formart_value, string select_value, string tag, string id, string name, string className)
        {
            var formart_values = formart_value.MyToString().Split(new string[] { "|#|" }, StringSplitOptions.None).Select(a => a.Split(new string[] { "|,|" }, StringSplitOptions.None));

            Dictionary<string, string> dic = formart_values.Where(a => a.Length == 2).ToDictionary(a => a[0], a => a[1]);
            StringBuilder TempHtml = new StringBuilder();
            TempHtml.AppendLine($"<select tag=\"{tag}\" lay-search=\"\" name=\"{name}\" id=\"{id}\" class=\"{className}\">");

            foreach (var item in dic)
            {
                if (item.Key.ToLower() == select_value.ToLower())
                {
                    TempHtml.AppendLine($"<option selected=\"selected\" value=\"{item.Key.ToLower()}\">{item.Value}</option>");
                }
                else
                {
                    TempHtml.AppendLine($"<option value=\"{item.Key.ToLower()}\">{item.Value}</option>");
                }
            }
            TempHtml.AppendLine("</select>");
            return new HtmlString(TempHtml.ToString());
        }

        public static IHtmlContent ExtRadio(this IHtmlHelper Html, string formart_value, string select_value, string tag, string name, string className)
        {
            var formart_values = formart_value.MyToString().Split(new string[] { "|#|" }, StringSplitOptions.None).Select(a => a.Split(new string[] { "|,|" }, StringSplitOptions.None));
            Dictionary<string, string> dic = formart_values.Where(a => a.Length == 2).ToDictionary(a => a[0], a => a[1]);
            StringBuilder TempHtml = new StringBuilder();
            TempHtml.AppendLine($"<div tag=\"{tag}\" class=\"{className}\">");
            foreach (var item in dic)
            {
                if (item.Key.ToLower() == select_value.ToLower())
                {
                    TempHtml.AppendLine($"<input type=\"radio\" name=\"{name}\" value=\"{item.Key.ToLower()}\"  title=\"{item.Value}\" checked=\"\">");
                }
                else
                {
                    TempHtml.AppendLine($"<input type=\"radio\" name=\"{name}\" value=\"{item.Key.ToLower()}\"  title=\"{item.Value}\" >");
                }
            }
            TempHtml.AppendLine("</div>");
            return new HtmlString(TempHtml.ToString());
        }

        public static IHtmlContent ExtCheckBox(this IHtmlHelper Html, string formart_value, string select_value, string tag, string name, string className)
        {
            var formart_values = formart_value.MyToString().Split(new string[] { "|#|" }, StringSplitOptions.None).Select(a => a.Split(new string[] { "|,|" }, StringSplitOptions.None));
            Dictionary<string, string> dic = formart_values.Where(a => a.Length == 2).ToDictionary(a => a[0], a => a[1]);
            StringBuilder TempHtml = new StringBuilder();
            TempHtml.AppendLine($"<div tag=\"{tag}\" class=\"{className}\">");
            var select_values = select_value.Split(new string[] { "|,|" }, StringSplitOptions.None).ToList();
            foreach (var item in dic)
            {
                if (select_values.ContainsIgnoreCase(item.Key))
                {
                    TempHtml.AppendLine($"<input type=\"checkbox\"  lay-skin=\"primary\" name=\"{name}\" value=\"{item.Key.ToLower()}\"  title=\"{item.Value}\" checked=\"\">");
                }
                else
                {
                    TempHtml.AppendLine($"<input type=\"checkbox\"  lay-skin=\"primary\" name=\"{name}\" value=\"{item.Key.ToLower()}\"  title=\"{item.Value}\" >");
                }
            }
            TempHtml.AppendLine("</div>");
            return new HtmlString(TempHtml.ToString());
        }

        /// <summary>
        /// 获取扩展字段
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="key"></param>
        /// <param name="defaultvalue"></param>
        /// <returns></returns>
        public static string GetExtModelField(this List<Models.sys_ext_model_detail> list, string key, string defaultvalue)
        {
            if (list == null)
            {
                return defaultvalue;
            }
            else
            {
                var pic = list.Where(a => a.key == key).FirstOrDefault();

                if (pic != null)
                {
                    return pic.value;
                }
                return defaultvalue;
            }
        }
        public static IQueryable<T> GetPageList<T>(IOrderedQueryable<T> List, int PageIndex, int PageSize)
        {
            int PageCount = GetPageCount(PageSize, List.Count());
            PageIndex = CheckPageIndex(PageIndex, PageCount);
            return List.Skip((PageIndex - 1) * PageSize).Take(PageSize);
        }

        public static int GetPageCount(int PageSize, int recordCount)
        {
            int PageCount = recordCount % PageSize == 0 ? recordCount / PageSize : recordCount / PageSize + 1;
            if (PageCount < 1) PageCount = 1;
            return PageCount;
        }

        public static int CheckPageIndex(int PageIndex, int PageCount)
        {
            if (PageIndex > PageCount) PageIndex = PageCount;
            if (PageIndex < 1) PageIndex = 1;
            return PageIndex;
        }

        public enum FPageMode { Normal, Numeric, GroupNumeric }





        public static IHtmlContent ShowFPage(this IHtmlHelper Html, string urlFormat, int PageIndex, int PageSize, int recordCount, FPageMode Mode)
        {
            urlFormat = urlFormat.Replace("%7B0%7D", "{0}");
            int PageCount = GetPageCount(PageSize, recordCount);

            StringBuilder TempHtml = new StringBuilder();

            TempHtml.AppendLine($"<div class=\"data_totle\"><b>合计：</b><i>{recordCount}</i></div><div class=\"PageBtns\" pagecount=\"{PageCount}\">");
            if (PageIndex == 1)
            {
                TempHtml.AppendLine($"<a class=\"pgEmpty\" tag=\"frist\" pageCount=\"{PageCount}\" >首页</a><a class=\"pgEmpty\"  tag=\"prev\">上一页</a>");
            }
            else
            {
                TempHtml.AppendLine($"<a  class=\"pgNext\"  tag=\"frist\" pageCount=\"{PageCount}\">首页</a>")
                    .AppendLine($"<a class=\"pgNext\" tag=\"prev\" pageCount=\"{PageCount}\">上一页</a>");
            }
            // 数字分页
            switch (Mode)
            {
                case FPageMode.Numeric:
                    TempHtml.AppendLine(GetNumericPage(urlFormat, PageIndex, PageSize, PageCount));
                    break;
                case FPageMode.GroupNumeric:
                    TempHtml.AppendLine(GetGroupNumericPage(urlFormat, PageIndex, PageSize, PageCount));
                    break;
            }

            if (PageIndex == PageCount)
            {
                TempHtml.AppendLine($"<a class=\"pgEmpty\" tag=\"next\" pageCount=\"{PageCount}\">下一页</a><a class=\"pgEmpty\" tag=\"last\">末页</a>");
            }
            else
            {
                TempHtml.AppendLine($"<a class=\"pgNext\" tag=\"next\" val=\"{(PageIndex + 1)}\" pageCount=\"{PageCount}\">下一页</a>")
                    .AppendLine($"<a class=\"pgNext\" tag=\"last\" val=\"{PageCount}\" pageCount=\"{PageCount}\">末页</a>");
            }
            TempHtml.Append($"</div>");
            return new HtmlString(TempHtml.ToString());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="urlFormat"></param>
        /// <param name="PageIndex"></param>
        /// <param name="recordCount"></param>
        /// <returns></returns>
        public static IHtmlContent ShowFPage(this IHtmlHelper Html, string urlFormat, int PageIndex, int PageSize, int recordCount)
        {
            FPageMode Mode = FPageMode.Numeric;
            return ShowFPage(Html, urlFormat, PageIndex, PageSize, recordCount, Mode);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="urlFormat"></param>
        /// <param name="PageIndex"></param>
        /// <param name="recordCount"></param>
        /// <returns></returns>
        public static IHtmlContent ShowFPage(this IHtmlHelper Html, string urlFormat, int PageIndex, int recordCount)
        {
            int PageSize = 10;

            FPageMode Mode = FPageMode.Numeric;
            return ShowFPage(Html, urlFormat, PageIndex, PageSize, recordCount, Mode);
        }

        /// <summary>
        /// 分组数字分页
        /// </summary>
        /// <param name="urlFormat"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public static string GetGroupNumericPage(string urlFormat, int pageIndex, int pageSize, int pageCount)
        {
            int GroupChildCount = 5; // 分组显示个数
            int DGroup = pageIndex / GroupChildCount; //当前组
            int GroupCount = pageCount / GroupChildCount;      //组数

            //如果正好是当前组最后一页 不进入下一组
            if (pageIndex % GroupChildCount == 0) DGroup--;

            //当前组数量
            int GroupSpan = (DGroup == GroupCount) ? pageCount % GroupChildCount : GroupChildCount;

            StringBuilder TempHtml = new StringBuilder();
            for (int i = DGroup * GroupChildCount + 1; i <= DGroup * GroupChildCount + GroupSpan; i++)
            {
                if (i == pageIndex)
                    TempHtml.AppendFormat("<span class=\"current\" tag=\"current\">{0}</span>", i);
                else
                    TempHtml.AppendFormat("<a class=\"pgNext\" tag=\"number\" >{1}</a>", string.Format(urlFormat, i), i);
            }
            return TempHtml.ToString();
        }

        /// <summary>
        /// 数字分页
        /// </summary>
        /// <param name="urlFormat"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public static string GetNumericPage(string urlFormat, int pageIndex, int pageSize, int pageCount)
        {
            int SpanNum = 10;//显示个数

            int BeginNum = 0;
            if (pageIndex < 7)
                BeginNum = 1;
            else
                BeginNum = pageIndex - 5;

            if (BeginNum < 1) BeginNum = 1;
            int EndNum = BeginNum + (SpanNum - 1);
            if (EndNum > pageCount)
            {
                EndNum = pageCount;
                BeginNum = pageCount - (SpanNum - 1);
                if (BeginNum < 1) BeginNum = 1;
            }


            StringBuilder TempHtml = new StringBuilder();
            for (int i = BeginNum; i <= EndNum; i++)
            {
                if (i == pageIndex)
                    TempHtml.AppendFormat("<span  class=\"current\" tag=\"current\">{0}</span>", i);
                else
                    TempHtml.AppendFormat("<a class=\"pgNext\" tag=\"number\">{1}</a>", string.Format(urlFormat, i), i);
            }
            return TempHtml.ToString();
        }
    }
}