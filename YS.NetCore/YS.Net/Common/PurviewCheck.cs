﻿using System.Linq;
using System.Text;

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Concurrent;
using YS.Net.Models;
using YS.Utils.Cache;
using YS.Net.Services;

using Microsoft.Extensions.DependencyInjection;
using System.Linq.Expressions;
using YS.Utils;

namespace YS.Net
{
    public static class PurviewCheck
    {



        /// <summary>
        /// 判断多个模块是否有权限
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="modules">多块代号 多个用,号分割</param>
        /// <returns></returns>
        public static bool Check(this IHtmlHelper Html, string modules)
        {
            return checkPermission(Html, modules, "", "");
        }

        /// <summary>
        /// 判断多个模块资源是否有权限
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="modules">多块模块代号 多个用,号分割</param>
        /// <param name="resources">多块资源代号 多个用,号分割</param>
        /// <returns></returns>
        public static bool Check(this IHtmlHelper Html, string modules, string resources)
        {
            return checkPermission(Html, modules, resources,"");

        }

        /// <summary>
        /// 判断多个模块资源是否有权限
        /// </summary>
        /// <param name="Html"></param>
        /// <param name="modules">多块模块代号 多个用,号分割</param>
        /// <param name="resources">多块资源代号 多个用,号分割</param>
        /// <param name="operates">多块操作代号 多个用,号分割</param>
        /// <returns></returns>
        public static bool Check(this IHtmlHelper Html, string modules, string resources, string operates)
        {

            return checkPermission(Html, modules, resources, operates);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user_name"></param>
        /// <param name="module_code"></param>
        /// <param name="resource_code"></param>
        /// <param name="operate_code"></param>
        private static bool checkPermission(IHtmlHelper Html,  string module_code, string resource_codes, string operate_codes)
        {

            var user = Html.ViewContext.HttpContext.RequestServices.GetService<IApplicationContext>().CurrentUser;
            if (user.account == "admin")
            {
                return true;
            }
            //用user户角色关系缓存
            ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
            string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;

            //角色操作关系缓存
            ISysRoleOperateRelationService RoleOperate;

            ISysUserRoleRelationService user_role_service;
            //角色操作关系缓存
            ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;
            string SysRoleOperateNodes = Net.SessionKeyProvider.SysRoleOperateRelation;

            ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>
            cacheManager = Html.ViewContext.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>>();
            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());


            ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>
                role_operate_cacheManager = Html.ViewContext.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>>();
            role_operate_cahce = role_operate_cacheManager.GetOrAdd(SysRoleOperateNodes, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());


            RoleOperate = Html.ViewContext.HttpContext.RequestServices.GetService<ISysRoleOperateRelationService>();

            user_role_service = Html.ViewContext.HttpContext.RequestServices.GetService<ISysUserRoleRelationService>();

      

            if (user == null)
            {
                return false;
            }
            List<sys_user_role_relation_entity> user_role = null;
            user_role_cahce.TryGetValue(user.account.ToLower(), out user_role);
            if (user_role == null)
            {
                List<sys_user_role_relation_entity> cache_out = null;
                user_role_cahce.TryRemove(user.account.ToLower().ToLower(), out cache_out);
                user_role = user_role_service.GetList("", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==user.id
                });
                //更新用户角色缓存
                user_role_cahce.TryUpdate(user.account.ToLower().ToLower(), (List<sys_user_role_relation_entity> value11) => { return user_role; });
            }
            List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();
            //用户所有的权限
            List<sys_role_operate_relation_entity> list = null;
            user_role.ForEach(a =>
            {
                role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                if (list == null)
                {
                    list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                        c=>c.role_id==a.role_id
                    });
                    role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                }
                user_operate_list.AddRange(list);
            });

            if (module_code.Length > 0)
            {

                var module_codes = module_code.Split(',').Select(a=>a.ToLower());
                var modules = user_operate_list.Where(a => module_codes.Contains(a.module_code.ToLower()));

                if (modules.Count() <= 0)
                {
                    return false;
                }
                if (resource_codes.Trim().Length > 0)
                {
                    var resource_code = resource_codes.Split(',').Select(a => a.ToLower());
                    var resource = modules.Where(a => resource_code.Contains(a.res_code.ToLower()));
                    if (resource.Count() <= 0)
                    {
                        return false;
                    }

                    if (operate_codes.Trim().Length > 0)
                    {
                        var operate_code = operate_codes.Split(',').Select(a => a.ToLower());
                        var operate = resource.Where(a => operate_code.Contains(a.operate_code.ToLower()));
                        if (operate.Count() <= 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

    }
}
