﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using NPOI.XSSF.UserModel;
using System.DrawingCore;
using NPOI.SS.Util;

namespace YS.Net.Exrend
{
    public class NpoiExcelExportHelper
    {
        private static NpoiExcelExportHelper _exportHelper;

        public static NpoiExcelExportHelper _
        {
            get => _exportHelper ?? (_exportHelper = new NpoiExcelExportHelper());
            set => _exportHelper = value;
        }

        /// <summary>
        /// TODO:先创建行，然后在创建对应的列
        /// 创建Excel中指定的行
        /// </summary>
        /// <param name="sheet">Excel工作表对象</param>
        /// <param name="rowNum">创建第几行(从0开始)</param>
        /// <param name="rowHeight">行高</param>
        public HSSFRow CreateRow(ISheet sheet, int rowNum, float rowHeight)
        {
            HSSFRow row = (HSSFRow)sheet.CreateRow(rowNum); //创建行
            row.HeightInPoints = rowHeight; //设置列头行高
            return row;
        }

        /// <summary>
        /// 创建行内指定的单元格
        /// </summary>
        /// <param name="row">需要创建单元格的行</param>
        /// <param name="cellStyle">单元格样式</param>
        /// <param name="cellNum">创建第几个单元格(从0开始)</param>
        /// <param name="cellValue">给单元格赋值</param>
        /// <returns></returns>
        public HSSFCell CreateCells(HSSFRow row, HSSFCellStyle cellStyle, int cellNum, string cellValue)
        {
            HSSFCell cell = (HSSFCell)row.CreateCell(cellNum); //创建单元格
            cell.CellStyle = cellStyle; //将样式绑定到单元格
            if (!string.IsNullOrWhiteSpace(cellValue))
            {
                //单元格赋值
                cell.SetCellValue(cellValue);
            }

            return cell;
        }


        /// <summary>
        /// 行内单元格常用样式设置
        /// </summary>
        /// <param name="workbook">Excel文件对象</param>
        /// <param name="hAlignment">水平布局方式</param>
        /// <param name="vAlignment">垂直布局方式</param>
        /// <param name="fontHeightInPoints">字体大小</param>
        /// <param name="isAddBorder">是否需要边框</param>
        /// <param name="boldWeight">字体加粗 (None = 0,Normal = 400，Bold = 700</param>
        /// <param name="fontName">字体（仿宋，楷体，宋体，微软雅黑...与Excel主题字体相对应）</param>
        /// <param name="isAddBorderColor">是否增加边框颜色</param>
        /// <param name="isItalic">是否将文字变为斜体</param>
        /// <param name="isLineFeed">是否自动换行</param>
        /// <param name="isAddCellBackground">是否增加单元格背景颜色</param>
        /// <param name="fillPattern">填充图案样式(FineDots 细点，SolidForeground立体前景，isAddFillPattern=true时存在)</param>
        /// <param name="cellBackgroundColor">单元格背景颜色（当isAddCellBackground=true时存在）</param>
        /// <param name="fontColor">字体颜色</param>
        /// <param name="underlineStyle">下划线样式（无下划线[None],单下划线[Single],双下划线[Double],会计用单下划线[SingleAccounting],会计用双下划线[DoubleAccounting]）</param>
        /// <param name="typeOffset">字体上标下标(普通默认值[None],上标[Sub],下标[Super]),即字体在单元格内的上下偏移量</param>
        /// <param name="isStrikeout">是否显示删除线</param>
        /// <returns></returns>
        public HSSFCellStyle CreateStyle(HSSFWorkbook workbook, HorizontalAlignment hAlignment, VerticalAlignment vAlignment, short fontHeightInPoints, bool isAddBorder, short boldWeight, string fontName = "宋体", bool isAddBorderColor = true, bool isItalic = false, bool isLineFeed = false, bool isAddCellBackground = false, FillPattern fillPattern = FillPattern.NoFill, short cellBackgroundColor = HSSFColor.Yellow.Index, short fontColor = HSSFColor.Black.Index, FontUnderlineType underlineStyle =
            FontUnderlineType.None, FontSuperScript typeOffset = FontSuperScript.None, bool isStrikeout = false)
        {
            HSSFCellStyle cellStyle = (HSSFCellStyle)workbook.CreateCellStyle(); //创建列头单元格实例样式
            cellStyle.Alignment = hAlignment; //水平居中
            cellStyle.VerticalAlignment = vAlignment; //垂直居中
            cellStyle.WrapText = isLineFeed;//自动换行

            //背景颜色，边框颜色，字体颜色都是使用 HSSFColor属性中的对应调色板索引，关于 HSSFColor 颜色索引对照表，详情参考：https://www.cnblogs.com/Brainpan/p/5804167.html

            //TODO：引用了NPOI后可通过ICellStyle 接口的 FillForegroundColor 属性实现 Excel 单元格的背景色设置，FillPattern 为单元格背景色的填充样式

            //TODO:十分注意，要设置单元格背景色必须是FillForegroundColor和FillPattern两个属性同时设置，否则是不会显示背景颜色
            if (isAddCellBackground)
            {
                cellStyle.FillForegroundColor = cellBackgroundColor;//单元格背景颜色
                cellStyle.FillPattern = fillPattern;//填充图案样式(FineDots 细点，SolidForeground立体前景)
            }


            //是否增加边框
            if (isAddBorder)
            {
                //常用的边框样式 None(没有),Thin(细边框，瘦的),Medium(中等),Dashed(虚线),Dotted(星罗棋布的),Thick(厚的),Double(双倍),Hair(头发)[上右下左顺序设置]
                cellStyle.BorderBottom = BorderStyle.Thin;
                cellStyle.BorderRight = BorderStyle.Thin;
                cellStyle.BorderTop = BorderStyle.Thin;
                cellStyle.BorderLeft = BorderStyle.Thin;
            }

            //是否设置边框颜色
            if (isAddBorderColor)
            {
                //边框颜色[上右下左顺序设置]
                cellStyle.TopBorderColor = HSSFColor.DarkGreen.Index;//DarkGreen(黑绿色)
                cellStyle.RightBorderColor = HSSFColor.DarkGreen.Index;
                cellStyle.BottomBorderColor = HSSFColor.DarkGreen.Index;
                cellStyle.LeftBorderColor = HSSFColor.DarkGreen.Index;
            }

            /**
             * 设置相关字体样式
             */
            var cellStyleFont = (HSSFFont)workbook.CreateFont(); //创建字体

            //假如字体大小只需要是粗体的话直接使用下面该属性即可
            //cellStyleFont.IsBold = true;

            cellStyleFont.Boldweight = boldWeight; //字体加粗
            cellStyleFont.FontHeightInPoints = fontHeightInPoints; //字体大小
            cellStyleFont.FontName = fontName;//字体（仿宋，楷体，宋体 ）
            cellStyleFont.Color = fontColor;//设置字体颜色
            cellStyleFont.IsItalic = isItalic;//是否将文字变为斜体
            cellStyleFont.Underline = underlineStyle;//字体下划线
            cellStyleFont.TypeOffset = typeOffset;//字体上标下标
            cellStyleFont.IsStrikeout = isStrikeout;//是否有删除线

            cellStyle.SetFont(cellStyleFont); //将字体绑定到样式
            return cellStyle;
        }


        public DataSet ReadXlsx(string path)
        {
            DataSet result = new DataSet();

            XSSFWorkbook hssfworkbook;
            #region//初始化信息
            try
            {
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    hssfworkbook = new XSSFWorkbook(file);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            #endregion
            int sheet_count = hssfworkbook.Count;

            for (int j = 0; j < sheet_count; j++)
            {

                NPOI.SS.UserModel.ISheet sheet = hssfworkbook.GetSheetAt(j);
                string SheetName = sheet.SheetName;

                DataTable table = new DataTable(SheetName);
                if (sheet.LastRowNum <= 0)
                {
                    continue;
                }

                IRow headerRow = sheet.GetRow(0);//第一行为标题行

                if (headerRow.LastCellNum <= 0)
                {
                    continue;
                }
                int cellCount = headerRow.LastCellNum;//LastCellNum = PhysicalNumberOfCells
                int rowCount = sheet.LastRowNum;//LastRowNum = PhysicalNumberOfRows - 1

                //handling header.
                for (int i = headerRow.FirstCellNum; i < cellCount; i++)
                {
                    DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue.Trim());
                    table.Columns.Add(column);
                }
                for (int i = (sheet.FirstRowNum + 1); i <= rowCount; i++)
                {
                    IRow row = sheet.GetRow(i);
                    DataRow dataRow = table.NewRow();

                    if (row != null)
                    {
                        for (int k = row.FirstCellNum; k < cellCount; k++)
                        {
                            if (row.GetCell(k) != null)
                                dataRow[k] = GetCellValue(row.GetCell(k));
                        }
                    }

                    table.Rows.Add(dataRow);
                }
                result.Tables.Add(table);
            }
            return result;
        }

        public DataSet ReadXls(string path)
        {
            DataSet result = new DataSet();

            HSSFWorkbook hssfworkbook;
            #region//初始化信息
            try
            {
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    hssfworkbook = new HSSFWorkbook(file);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            #endregion
            int sheet_count = hssfworkbook.Count;

            for (int j = 0; j < sheet_count; j++)
            {

                NPOI.SS.UserModel.ISheet sheet = hssfworkbook.GetSheetAt(j);
                string SheetName = sheet.SheetName;

                DataTable table = new DataTable(SheetName);
                IRow headerRow = sheet.GetRow(0);//第一行为标题行
                int cellCount = headerRow.LastCellNum;//LastCellNum = PhysicalNumberOfCells
                int rowCount = sheet.LastRowNum;//LastRowNum = PhysicalNumberOfRows - 1

                //handling header.
                for (int i = headerRow.FirstCellNum; i < cellCount; i++)
                {
                    DataColumn column = new DataColumn(headerRow.GetCell(i).StringCellValue.Trim());
                    table.Columns.Add(column);
                }
                for (int i = (sheet.FirstRowNum + 1); i <= rowCount; i++)
                {
                    IRow row = sheet.GetRow(i);
                    DataRow dataRow = table.NewRow();

                    if (row != null)
                    {
                        for (int k = row.FirstCellNum; k < cellCount; k++)
                        {
                            if (row.GetCell(k) != null)
                                dataRow[k] = GetCellValue(row.GetCell(k));
                        }
                    }

                    table.Rows.Add(dataRow);
                }
                result.Tables.Add(table);
            }
            return result;
        }
        /// <summary>
        /// 根据Excel列类型获取列的值
        /// </summary>
        /// <param name="cell">Excel列</param>
        /// <returns></returns>
        private string GetCellValue(ICell cell)
        {
            if (cell == null)
                return string.Empty;
            switch (cell.CellType)
            {
                case CellType.Blank:
                    return string.Empty;
                case CellType.Boolean:
                    return cell.BooleanCellValue.ToString();
                case CellType.Error:
                    return cell.ErrorCellValue.ToString();
                case CellType.Numeric:
                case CellType.Unknown:
                default:
                    return cell.ToString();//This is a trick to get the correct value of the cell. NumericCellValue will return a numeric value no matter the cell value is a date or a number
                case CellType.String:
                    return cell.StringCellValue;
                case CellType.Formula:
                    try
                    {
                        HSSFFormulaEvaluator e = new HSSFFormulaEvaluator(cell.Sheet.Workbook);
                        e.EvaluateInCell(cell);
                        return cell.ToString();
                    }
                    catch
                    {
                        return cell.NumericCellValue.ToString();
                    }
            }
        }


        public IWorkbook CancelMergedRegion(IWorkbook WorkBooks)
        {
            //IWorkbook WorkBooks;
            //FileStream FStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            //if (filePath.IndexOf(".xlsx") > 0)
            //{
            //    WorkBooks = new XSSFWorkbook(FStream);
            //}
            //else
            //{
            //    WorkBooks = new HSSFWorkbook(FStream);
            //}



            //IWorkbook wk = WorkBooks;
            ////获取后缀名
            //string extension = filePath.Substring(filePath.LastIndexOf(".")).ToString().ToLower();
            //Dictionary<string, string> targetvalue = new Dictionary<string, string>();

            //    //获取第一个sheet
            ISheet sheet = WorkBooks.GetSheetAt(0);
            int MergedCount = sheet.NumMergedRegions;
            for (int i = MergedCount - 1; i >= 0; i--)
            {


                /**CellRangeAddress对象属性有：FirstColumn，FirstRow，LastColumn，LastRow 进行操作 取消合并单元格 **/

                var temp = sheet.GetMergedRegion(i);

                int first_row = temp.FirstRow;
                int last_row = temp.LastRow;
                int first_column = temp.FirstColumn;
                int last_column = temp.LastColumn;
                List<string> indexs = new List<string>();
                string value = "";
                for (int row = first_row; row <= last_row; row++)
                {
                    for (int column = first_column; column <= last_column; column++)
                    {
                        if (value == "")
                        {
                            ICell cell = sheet.GetRow(row).GetCell(column);
                            value = GetCellValue(cell);
                            break;
                        }
                        indexs.Add($"{row}_{column}");


                    }
                }

                sheet.RemoveMergedRegion(i);
                for (int row = first_row; row <= last_row; row++)
                {
                    for (int column = first_column; column <= last_column; column++)
                    {

                        sheet.GetRow(row).GetCell(column).SetCellValue(value);

                    }
                }
            }

            return WorkBooks;


        }


        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public DataTable RoadCancelMergedDt(string filePath, int header_row_index = 0)
        {

            IWorkbook WorkBooks;
            FileStream FStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            if (filePath.IndexOf(".xlsx") > 0)
            {
                WorkBooks = new XSSFWorkbook(FStream);
            }
            else
            {
                WorkBooks = new HSSFWorkbook(FStream);
            }


            DataTable dt = new DataTable();
            IWorkbook wk = WorkBooks;

            wk = CancelMergedRegion(wk);
            //获取后缀名
            string extension = filePath.Substring(filePath.LastIndexOf(".")).ToString().ToLower();
            //判断是否是excel文件
            if (extension == ".xlsx" || extension == ".xls")
            {
                //获取第一个sheet
                ISheet sheet = wk.GetSheetAt(0);
                //获取第一行
                IRow headrow = sheet.GetRow(header_row_index);
                //创建列
                for (int i = headrow.FirstCellNum; i < headrow.Cells.Count; i++)
                {
                    string ColumnName = (i + 1).ToNumberSystem26();
                    ICell cell = headrow.GetCell(i);
                    dt.Columns.Add(cell.ToString() + "-" + ColumnName);
                }
                //读取每行,从第二行起
                for (int r = header_row_index + 1; r <= sheet.LastRowNum; r++)
                {
                    bool result = false;
                    DataRow dr = dt.NewRow();
                    //获取当前行
                    IRow row = sheet.GetRow(r);
                    //读取每列
                    for (int j = 0; j < row.Cells.Count; j++)
                    {
                        ICell cell = row.GetCell(j); //一个单元格
                        if (cell == null)
                        {
                            continue;
                        }
                        if (cell.IsMergedCell && r > 1)  //检测列的单元格是否合并
                        {
                            var cellValue = GetCellValue(cell);
                            if (string.IsNullOrEmpty(cellValue))
                            {
                                dr[j] = dt.Rows[r - 2 - header_row_index][j];
                            }
                            else
                            {
                                dr[j] = cellValue; //获取单元格的值
                                if (string.IsNullOrWhiteSpace(dr[j].ToString()) && j > 0)
                                {
                                    dr[j] = dr[j - 1];
                                }
                            }
                        }
                        else
                        {
                            dr[j] = GetCellValue(cell); //获取单元格的值
                            if (string.IsNullOrWhiteSpace(dr[j].ToString()) && j > 0)
                            {
                                dr[j] = dr[j - 1];
                            }
                        }
                        if (dr[j].ToString() != "")//全为空则不取
                        {
                            result = true;
                        }
                    }
                    if (result == true)
                    {
                        dt.Rows.Add(dr); //把每行追加到DataTable
                    }
                }
            }
            return dt;
        }

        /// <summary>
        /// 导入
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public DataTable RoadDt(string filePath, int header_row_index = 0)
        {

            IWorkbook WorkBooks;
            FileStream FStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            if (filePath.IndexOf(".xlsx") > 0)
            {
                WorkBooks = new XSSFWorkbook(FStream);
            }
            else
            {
                WorkBooks = new HSSFWorkbook(FStream);
            }


            DataTable dt = new DataTable();
            IWorkbook wk = WorkBooks;
            //获取后缀名
            string extension = filePath.Substring(filePath.LastIndexOf(".")).ToString().ToLower();
            //判断是否是excel文件
            if (extension == ".xlsx" || extension == ".xls")
            {
                //获取第一个sheet
                ISheet sheet = wk.GetSheetAt(0);
                //获取第一行
                IRow headrow = sheet.GetRow(header_row_index);
                //创建列
                for (int i = headrow.FirstCellNum; i < headrow.Cells.Count; i++)
                {
                    string ColumnName = (i + 1).ToNumberSystem26();
                    ICell cell = headrow.GetCell(i);
                    dt.Columns.Add(cell.ToString() + "-" + ColumnName);
                }
                //读取每行,从第二行起
                for (int r = header_row_index + 1; r <= sheet.LastRowNum; r++)
                {
                    bool result = false;
                    DataRow dr = dt.NewRow();
                    //获取当前行
                    IRow row = sheet.GetRow(r);
                    //读取每列
                    for (int j = 0; j < row.Cells.Count; j++)
                    {
                        ICell cell = row.GetCell(j); //一个单元格
                        if (cell == null)
                        {
                            continue;
                        }
                        if (cell.IsMergedCell && r > 1)  //检测列的单元格是否合并
                        {
                            var cellValue = GetCellValue(cell);
                            if (string.IsNullOrEmpty(cellValue))
                            {
                                dr[j] = dt.Rows[r - 2 - header_row_index][j];
                            }
                            else
                            {
                                dr[j] = cellValue; //获取单元格的值
                                if (string.IsNullOrWhiteSpace(dr[j].ToString()) && j > 0)
                                {
                                    dr[j] = dr[j - 1];
                                }
                            }
                        }
                        else
                        {
                            dr[j] = GetCellValue(cell); //获取单元格的值
                            if (string.IsNullOrWhiteSpace(dr[j].ToString()) && j > 0)
                            {
                                dr[j] = dr[j - 1];
                            }
                        }
                        if (dr[j].ToString() != "")//全为空则不取
                        {
                            result = true;
                        }
                    }
                    if (result == true)
                    {
                        dt.Rows.Add(dr); //把每行追加到DataTable
                    }
                }
            }
            return dt;
        }
        /// <summary>
        /// 判断是否是合并单元格
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private bool IsMergeCell(ISheet sheet, int rowIndex, int colIndex, out Point start, out Point end)
        {
            //定义返回结果
            bool result = false;
            start = new Point(0, 0);
            end = new Point(0, 0);
            //如果没有行数，返回false
            if ((rowIndex < 0) || (colIndex < 0)) return result;
            //获取合并单元格数
            int regionsCount = sheet.NumMergedRegions;
            for (int i = 0; i < regionsCount; i++)
            {
                CellRangeAddress range = sheet.GetMergedRegion(i);
                if (rowIndex >= range.FirstRow && rowIndex <= range.LastRow && colIndex >= range.FirstColumn && colIndex <= range.LastColumn)
                {
                    start = new Point(range.FirstRow, range.FirstColumn);
                    end = new Point(range.LastRow, range.LastColumn);
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// 获取合并单元格的值
        /// </summary>
        /// <param name="sheet">第一个sheet</param>
        /// <param name="row">初始行</param>
        /// <param name="column">初始列</param>
        /// <returns></returns>
        private object GetMergedRegionStr(ISheet sheet, int row, int column)
        {
            //初始化坐标
            Point start = new Point(0, 0);
            Point end = new Point(0, 0);
            //获取合并单元格的数量
            int regionsCount = sheet.NumMergedRegions;
            //初始化单元格的值
            object cellValue = new object();
            //判断坐标
            for (int j = 0; j < regionsCount; j++)
            {
                //获得不同坐标
                CellRangeAddress range = sheet.GetMergedRegion(j);
                //判读参数坐标是否存在
                if (row >= range.FirstRow && row <= range.LastRow && column >= range.FirstColumn && column <= range.LastColumn)
                {
                    //获取坐标
                    start = new Point(range.FirstRow, range.FirstColumn);
                    end = new Point(range.LastRow, range.LastColumn);
                    //坐标x=row,y=colum
                    int x = start.X;//获取所在坐标行号
                    int y = start.Y;//获取所在坐标列号
                    //获取所在行
                    IRow fRow = sheet.GetRow(x);
                    //获取所在列
                    ICell fCell = fRow.GetCell(y);
                    //判断cell的类型
                    if (fCell.CellType == CellType.String)//string类型
                    {
                        cellValue = fCell.StringCellValue;
                    }
                    else if (fCell.CellType == CellType.Numeric)//数字类型
                    {
                        //判断是否是时间类型
                        if (HSSFDateUtil.IsCellDateFormatted(fCell))
                        {
                            cellValue = fCell.DateCellValue;
                        }
                        else
                        {
                            cellValue = fCell.NumericCellValue;
                        }

                    }
                    else if (fCell.CellType == CellType.Formula)//单元格有公式类型
                    {
                        cellValue = fCell.NumericCellValue;
                    }
                    else
                    {
                        cellValue = "";
                    }
                    break;
                }
            }
            return cellValue;
        }




    }
}

