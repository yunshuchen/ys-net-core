﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using YS.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using YS.Utils.DI;

namespace YS.Net.Common
{
    [SingletonDI]
    public class SiteFileProvider : ISiteFileProvider
    {
        private static IWebHostEnvironment hostingEnvironment;
        private static IConfiguration configuration;

        private static string TemplatBasePath = @"wwwroot\\templates\\";
        private static string _lucene_index = @"lucene_index";
        private static string config_StaticBasePath = @"static";

        private static string config_UploadStaticContentPath = @"uploadfiles";
        private static string config_TemplateBasePath = @"~/wwwroot/templates";
        public SiteFileProvider(
            IWebHostEnvironment _hostingEnvironment, IConfiguration _configuration)
        {
            hostingEnvironment = _hostingEnvironment;
            configuration = _configuration;
            string _TemplatBasePath = configuration.GetSection("TemplatBasePath").Get<string>().MyToString();

            string _UploadStaticPath = configuration.GetSection("UploadStaticPath").Get<string>().MyToString();


            string _UploadStaticContentPath = configuration.GetSection("UploadStaticContentPath").Get<string>().MyToString();
            string _config_TemplateBasePath = configuration.GetSection("TemplateRelativeBasePath").Get<string>().MyToString();
            if (_config_TemplateBasePath.Length > 0)
            {
                config_TemplateBasePath = _config_TemplateBasePath;
            }
            string __lucene_index = configuration.GetSection("lucene_index").Get<string>().MyToString();
            if (_TemplatBasePath.Length > 0)
            {
                TemplatBasePath = _TemplatBasePath;
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                TemplatBasePath = TemplatBasePath.Replace("/", "\\");
            }
            else
            {
                TemplatBasePath = TemplatBasePath.Replace("\\", "/");
                TemplatBasePath = TemplatBasePath.Replace(@"\", "/");
            }

            if (_UploadStaticPath.Length > 0)
            {
                config_StaticBasePath = _UploadStaticPath;
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                config_StaticBasePath = config_StaticBasePath.Replace("/", "\\");
            }
            else
            {
                config_StaticBasePath = config_StaticBasePath.Replace("\\", "/");
                config_StaticBasePath = config_StaticBasePath.Replace(@"\", "/");
            }

            if (_UploadStaticContentPath.Length > 0)
            {
                config_UploadStaticContentPath = _UploadStaticContentPath;
            }
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                config_UploadStaticContentPath = config_UploadStaticContentPath.Replace("/", "\\");
            }
            else
            {
                config_UploadStaticContentPath = config_UploadStaticContentPath.Replace("\\", "/");
                config_UploadStaticContentPath = config_UploadStaticContentPath.Replace(@"\", "/");
            }

            if (__lucene_index.Length > 0)
            {
                _lucene_index = __lucene_index;
            }

        }

        /// <summary>
        /// 静态文件的目录名
        /// upload
        /// </summary>
       public string StaticDirectory { get { return config_StaticBasePath; } }


        /// <summary>
        /// [WebSite]/wwwroot
        /// </summary>
        public string WwwrootsePath
        {
            get => Path.Combine(new DirectoryInfo(hostingEnvironment.ContentRootPath).FullName, "wwwroot");

        }
        /// <summary>
        /// 静态文件的目录名
        /// uploadfiles
        /// </summary>
        public string StaticContentDirectory { get { return config_UploadStaticContentPath; } }


        /// <summary>
        /// 内容关联上传的文件目录
        /// [WebSite]\upload\uploadfiles
        /// </summary>
        public string UploadStaticContentPath
        {
            get => Path.Combine(new DirectoryInfo(hostingEnvironment.ContentRootPath).FullName, config_StaticBasePath, config_UploadStaticContentPath);
        }

        /// <summary>
        /// 内容关联上传的文件目录
        /// [WebSite]\upload
        /// </summary>
        public string  SiteWebRootPath
        {
            get => Path.Combine(new DirectoryInfo(hostingEnvironment.ContentRootPath).FullName, config_StaticBasePath);
        }
        /// <summary>
        /// 上传附件的日期目录:yyyy-MM-dd
        /// </summary>
        public string ContentUploadBuildPath
        {

            get => DateTime.Now.MyToDate();//.Replace('-', Path.DirectorySeparatorChar);

        }
        public T GetAppSettingValue<T>(string key)
        {
            return configuration.GetSection(key).Get<T>();
        }

        /// <summary>
        /// 模板基础目录
        /// [WebSite]/wwwroot/templates/
        /// </summary>
        public string TemplateBasePath
        {
            get => Path.Combine(new DirectoryInfo(hostingEnvironment.ContentRootPath).FullName, TemplatBasePath);

        }
        /// <summary>
        /// 静态文件目录
        /// [WebSite]\upload
        /// </summary>
        public string StaticBasePath
        {
            get => Path.Combine(new DirectoryInfo(hostingEnvironment.ContentRootPath).FullName, config_StaticBasePath);

        }
        /// <summary>
        /// 是否未windows平台
        /// </summary>
        public bool IsWindows { get => RuntimeInformation.IsOSPlatform(OSPlatform.Windows); }
   
        /// <summary>
        /// 获取模型类型
        /// </summary>
        /// <param name="ModelCode"></param>
        /// <returns></returns>
        public string GetModelTypeName(string ModelCode)
        {
            switch (ModelCode.ToLower())
            {
                case "single":
                    return "单页";
                case "news":
                    return "文章";
                case "product":
                    return "产品";
                default:
                    break;
            }
            return "未知模型";
        }

        /// <summary>
        /// 静态文件的目录名
        /// upload
        /// </summary>
        public string TemplateRelativeBasePath { get { return config_TemplateBasePath; } }



        public string lucene_index => _lucene_index;
    }
}
