﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace YS.Net.Common
{
    public interface ISiteFileProvider
    {


        /// <summary>
        /// 模板基础目录
        /// [WebSite]\wwwroot\templates\
        /// </summary>
        string TemplateBasePath { get; }


        /// <summary>
        /// 静态文件目录
        /// [WebSite]\upload
        /// </summary>
        string StaticBasePath { get; }

        /// <summary>
        /// 静态文件的目录名
        /// upload
        /// </summary>
        string StaticDirectory { get; }


        /// <summary>
        /// 静态文件的目录名
        /// uploadfiles
        /// </summary>
        string StaticContentDirectory { get; }


        /// <summary>
        /// 内容关联的上传目录
        /// [WebSite]\upload\uploadfiles
        /// </summary>
        string UploadStaticContentPath { get; }




        /// <summary>
        /// 内容关联的上传目录
        /// [WebSite]\upload
        /// </summary>
        string SiteWebRootPath { get; }

        /// <summary>
        /// 是否为windows平台
        /// </summary>
        bool IsWindows { get;  }


        string GetModelTypeName(string ModelCode);

        /// <summary>
        /// 上传附件的日期目录:yyyy-MM-dd
        /// </summary>
        string ContentUploadBuildPath { get;  }

        /// <summary>
        /// 模板存放的相对目录
        /// ~/wwwroot/templates
        /// </summary>
        string TemplateRelativeBasePath { get; }


        /// <summary>
        /// Lucene 索引目录
        /// </summary>
        string lucene_index { get; }

        /// <summary>
        /// [WebSite]/wwwroot
        /// </summary>
        string WwwrootsePath { get; }

        /// <summary>
        /// 获取配置设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetAppSettingValue<T>(string key);

    }
}
