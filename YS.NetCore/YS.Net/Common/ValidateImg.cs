﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using YS.Utils.LINQ;

namespace YS.Net.Common
{
    public  class ValidateImg
    {
        #region 验证码长度(默认4个验证码的长度)
        private static int length = 4;
        public static int Length
        {
            get { return length; }
            set { length = value; }
        }
        #endregion

        #region 验证码字体大小(为了显示扭曲效果，默认40像素，可以自行修改)
        private static int fontSize = 40;
        public static int FontSize
        {
            get { return fontSize; }
            set { fontSize = value; }
        }
        #endregion

        #region 边框补(默认1像素)
        private static int padding = 2;
        public static int Padding
        {
            get { return padding; }
            set { padding = value; }
        }
        #endregion

        #region 是否输出燥点(默认不输出)
        private static bool chaos = true;
        public static bool Chaos
        {
            get { return chaos; }
            set { chaos = value; }
        }
        #endregion

        #region 输出燥点的颜色(默认灰色)
        private static Color chaosColor = Color.LightGray;
        public static Color ChaosColor
        {
            get { return chaosColor; }
            set { chaosColor = value; }
        }
        #endregion

        #region 自定义背景色(默认白色)
        private static Color backgroundColor = Color.White;
        public static Color BackgroundColor
        {
            get { return backgroundColor; }
            set { backgroundColor = value; }
        }
        #endregion

        #region 自定义随机颜色数组
        private static Color[] colors = { Color.Black, Color.Red, Color.DarkBlue, Color.Green, Color.Orange, Color.Brown, Color.DarkCyan, Color.Purple };
        public static Color[] Colors
        {
            get { return colors; }
            set { colors = value; }
        }
        #endregion

        #region 自定义字体数组
        private static string[] fonts = { "Arial", "Georgia" };
        public static string[] Fonts
        {
            get { return fonts; }
            set { fonts = value; }
        }
        #endregion

        #region 自定义随机码字符串序列(使用逗号分隔)
        private static string codeSerial = "1,2,3,4,5,6,7,8,9,0,A,B,C,D,E,F,G,H,J,K,M,N,P,Q,R,S,T,U,V,W,X,Y,Z";
        //private static string codeSerial = "阿,保,持,的,法,规,和,东,三,省,问,我,惹,你,诶,没,改,变,揍,屁,股,吧";
        public static string CodeSerial
        {
            get { return codeSerial; }
            set { codeSerial = value; }
        }
        #endregion

        #region 产生波形滤镜效果

        private const double PI = 3.1415926535897932384626433832795;
        private const double PI2 = 6.283185307179586476925286766559;

        /// <summary>
        /// 正弦曲线Wave扭曲图片（Edit By 51aspx.com）
        /// </summary>
        /// <param name="srcBmp">图片路径</param>
        /// <param name="bXDir">如果扭曲则选择为True</param>
        /// <param name="dMultValue">波形的幅度倍数，越大扭曲的程度越高，一般为3</param>
        /// <param name="dPhase">波形的起始相位，取值区间[0-2*PI)</param>
        /// <returns></returns>
        public static Bitmap TwistImage(Bitmap srcBmp, bool bXDir, double dMultValue, double dPhase)
        {
            Bitmap destBmp = new Bitmap(srcBmp.Width, srcBmp.Height);

            // 将位图背景填充为白色
            Graphics graph = Graphics.FromImage(destBmp);
            graph.FillRectangle(new SolidBrush(Color.White), 0, 0, destBmp.Width, destBmp.Height);
            graph.Dispose();

            double dBaseAxisLen = bXDir ? (double)destBmp.Height : (double)destBmp.Width;

            for (int i = 0; i < destBmp.Width; i++)
            {
                for (int j = 0; j < destBmp.Height; j++)
                {
                    double dx = 0;
                    dx = bXDir ? (PI2 * (double)j) / dBaseAxisLen : (PI2 * (double)i) / dBaseAxisLen;
                    dx += dPhase;
                    double dy = Math.Sin(dx);

                    // 取得当前点的颜色
                    int nOldX = 0, nOldY = 0;
                    nOldX = bXDir ? i + (int)(dy * dMultValue) : i;
                    nOldY = bXDir ? j : j + (int)(dy * dMultValue);

                    Color color = srcBmp.GetPixel(i, j);
                    if (nOldX >= 0 && nOldX < destBmp.Width
                     && nOldY >= 0 && nOldY < destBmp.Height)
                    {
                        destBmp.SetPixel(nOldX, nOldY, color);
                    }
                }
            }
            return destBmp;
        }
        #endregion

        #region 生成校验码图片
        public static Bitmap CreateMyImageCode(string code)
        {
            int fSize = FontSize;
            int fWidth = fSize + Padding;

            int imageWidth = (int)(code.Length * fWidth) + 30 + Padding * 2;
            int imageHeight = fSize * 2 + Padding;

            Bitmap image = new Bitmap(imageWidth, imageHeight);
            using (Graphics g = Graphics.FromImage(image))
            {
                g.Clear(Color.FromArgb(255, 133, 2));
                Font font = new Font(new FontFamily("Arial"), 14, FontStyle.Bold);
                SolidBrush brush = new SolidBrush(Color.White);
                int left = 0, top = 0, top1 = 1, top2 = 1;
                Random rand = new Random();
                int n1 = (imageHeight - FontSize - Padding * 2);
                int n2 = n1 / 4;
                top1 = n2;
                top2 = n2 * 2;

                Font f;
                Brush b;
                int cindex, findex;
                //随机字体和颜色的验证码字符
                for (int i = 0; i < code.Length; i++)
                {
                    cindex = rand.Next(Colors.Length - 1);
                    findex = rand.Next(Fonts.Length - 1);

                    f = new Font(Fonts[findex], fSize, FontStyle.Bold);
                    b = new SolidBrush(Colors[cindex]);

                    if (i % 2 == 1)
                    {
                        top = top2;
                    }
                    else
                    {
                        top = top1;
                    }

                    left = i * fWidth;

                    g.DrawString(code.Substring(i, 1), f, b, left, top);
                }

                //g.DrawString(code, font, brush, 3, 2);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                image.Save(ms, ImageFormat.Png);
            }
            return image;
        }

        public static Bitmap CreateImageCode(string code)
        {
            int fSize = FontSize;
            int fWidth = fSize + Padding;

            int imageWidth = (int)(code.Length * fWidth) + 30 + Padding * 2;
            int imageHeight = fSize * 2 + Padding;

            Bitmap image = new Bitmap(imageWidth, imageHeight);

            using (Graphics g = Graphics.FromImage(image))
            {

                g.Clear(BackgroundColor);

                Random rand = new Random();

                //给背景添加随机生成的燥点
                if (Chaos)
                {

                    Pen pen = new Pen(ChaosColor, 0);
                    int c = Length * 10;

                    for (int i = 0; i < c; i++)
                    {
                        int x = rand.Next(image.Width);
                        int y = rand.Next(image.Height);

                        g.DrawRectangle(pen, x, y, 1, 1);
                    }
                }

                int left = 0, top = 0, top1 = 1, top2 = 1;

                int n1 = (imageHeight - FontSize - Padding * 2);
                int n2 = n1 / 4;
                top1 = n2;
                top2 = n2 * 2;

                Font f;
                Brush b;

                int cindex, findex;

                //随机字体和颜色的验证码字符
                for (int i = 0; i < code.Length; i++)
                {
                    cindex = rand.Next(Colors.Length - 1);
                    findex = rand.Next(Fonts.Length - 1);

                    f = new Font(Fonts[findex], fSize, FontStyle.Bold);
                    b = new SolidBrush(Colors[cindex]);

                    if (i % 2 == 1)
                    {
                        top = top2;
                    }
                    else
                    {
                        top = top1;
                    }

                    left = i * fWidth;

                    g.DrawString(code.Substring(i, 1), f, b, left, top);
                }

                //画一个边框 边框颜色为Color.Gainsboro
                g.DrawRectangle(new Pen(Color.Gainsboro, 0), 0, 0, image.Width - 1, image.Height - 1);
                g.Dispose();

                //产生波形（Add By 51aspx.com）
                image = TwistImage(image, true, 8, 4);

          
            }
            return image;
        }
        #endregion

        #region 生成随机字符码
        public static string CreateVerifyCode(int codeLen)
        {
            if (codeLen == 0)
            {
                codeLen = Length;
            }

            string[] arr = CodeSerial.Split(',');

            string code = "";

            int randValue = -1;

            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));

            for (int i = 0; i < codeLen; i++)
            {
                randValue = rand.Next(0, arr.Length - 1);

                code += arr[randValue];
            }

            return code;
        }
        public static (string, string) CreateVerifyExpression()
        {
            string _codeSerial = "1,2,3,4,5,6,7,8,9";
            string expression_code = "+,-,*";
            string expression_name = "＋,－,×";


            string[] arr = _codeSerial.Split(',');



            string[] code = expression_code.Split(',');

            string[] name = expression_name.Split(',');

            List<string> nums = new List<string>();

            List<string> codes = new List<string>();
            Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            int randValue = -1;
            for (int i = 0; i < 3; i++)
            {
                randValue = rand.Next(0, arr.Length - 1);
                nums.Add(arr[randValue]);
            }
            List<string> codes_name = new List<string>();
            for (int i = 0; i < 2; i++)
            {
                randValue = rand.Next(0, 10000);
                randValue = randValue % 3;
                codes.Add(code[randValue]);
                codes_name.Add(name[randValue]);
            }

            string v_code = nums[0] + codes_name[0] + nums[1] + codes_name[1] + nums[2]+"=?";
            string v_expression_code = nums[0] + codes[0] + nums[1] + codes[1] + nums[2];

            EvalExpression expression = new EvalExpression($"{v_expression_code}");
            string result = expression.Compute().MyToString();
            return (v_code, result);
        }
        public static string CreateVerifyCode()
        {
            return CreateVerifyCode(0);
        }
        #endregion
    }
}
