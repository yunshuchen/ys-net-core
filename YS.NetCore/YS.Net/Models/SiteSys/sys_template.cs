﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;


namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_templates")]
    public class sys_template
    {
        /// <summary>        
        ///
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }

        /// <summary>
        /// 模板名称
        /// </summary>
        public string tpl_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string tpl_code { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string tpl_desc { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string tpl_thumbnail { get; set; }

    }
}
