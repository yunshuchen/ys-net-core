using YS.Utils;
using YS.Utils.Models;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Security.Principal;
using Newtonsoft.Json;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_account")]
    public class sys_account: IIdentity,IUser
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public string account { get; set; }

        [JsonIgnore]
        public string password { get; set; }
        public string alias_name { get; set; }

        public string chr_name { get; set; }
        public StatusEnum user_status { get; set; }
        public string creator_name { get; set; }

        public string mobile_phone { get; set; }
        public string email { get; set; }


        public DateTime create_time { get; set; }
        public DateTime last_login_time { get; set; }
        public string last_login_ip { get; set; }
        public int login_count { get; set; }
        public int account_type { get; set; }
        public string token { get; set; }
        public DateTime last_online_time { get; set; }
        public DateTime next_set_pwd { get; set; }

        /// <summary>
        /// 最后刷新时间
        /// </summary>
        [NotMappedAttribute]
        [JsonIgnore]
        public DateTime last_refresh { get; set; }
        /// <summary>
        /// 设置下线时间
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public DateTime put_out_login { get; set; }
        /// <summary>
        /// Authentication 类型
        /// </summary>
        [NotMappedAttribute]
        [JsonIgnore]
        public string AuthenticationType { get; set; }

        [NotMappedAttribute]
        [JsonIgnore]
        public bool IsAuthenticated { get; set; }
        [NotMappedAttribute]
        [JsonIgnore]
        public string Name { get { return account; } }
        [NotMappedAttribute]
        [JsonIgnore]
        public string ResetToken { get; set; }
        [NotMappedAttribute]
        [JsonIgnore]
        public DateTime? ResetTokenDate { get; set; }
    }
}
