﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Models
{
    public class template_file
    {
        public string tplPathCode { get; set; }
        public string pathName { get; set; }
        public tpl_directory_info Node { get; set; }
        public List<tpl_directory_info> Childrens
        {
            get
            {
                return Node.Nodes;
            }
        }
        public string CurrentPath { get; set; }


        /// <summary>
        /// 上一级目录
        /// </summary>
        public string PrePath { get; set; }

    }

    public class tpl_directory_info
    {

        /// <summary>
        /// 文件或者目录名
        /// </summary>
        public string ChrName { get; set; }

        ///// <summary>
        ///// 全路径
        ///// </summary>
        //public string FullPath { get; set; }

        /// <summary>
        /// 类型
        /// 1文件
        /// 2目录
        /// </summary>
        public int ChrType { get; set; }

        /// <summary>
        /// 下级目录
        /// </summary>
        public List<tpl_directory_info> Nodes { get; set; }


        public string FullName { get; set; }

        /// <summary>
        /// 文件或者目录的上级目录名称
        /// </summary>
        public string ParentPath { get; set; }
    }


    public class template_item_entity { 
        /// <summary>
        /// 模版代号
        /// </summary>
        public string tpl_code { get; set; }
        /// <summary>
        /// 路径名称
        /// </summary>
        public string path_name { get; set; }
        //当前路径
        public string current_path { get; set; }
        /// <summary>
        /// 父级路径
        /// </summary>
        public string parent_path { get; set; }

        public List<template_file_entity> childrens { get; set; }
    }

    public class template_file_entity {
        /// <summary>
        /// 文件或者目录名
        /// </summary>
        public string chr_name { get; set; }

        /// <summary>
        /// 类型
        /// 1文件
        /// 2目录
        /// </summary>
        public int chr_type { get; set; }

        public string relative_path { get; set; }

        public string mime_type { get; set; }

        public string file_ext { get; set; }

    }
}
