﻿using YS.Utils;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_ext_model")]
    public class sys_ext_model
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        public string chr_name { get; set; }
        public string chr_code { get; set; }

        public StatusEnum status { get; set; }
    }
}
