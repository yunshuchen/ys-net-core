﻿using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_ext_model_detail")]
    public class sys_ext_model_detail
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; } 

        public string p_code { get; set; }
        /// <summary>
        /// 标识
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// 文字
        /// </summary>
        public string chr_name { get; set; }

        /// <summary>
        /// 类型
        /// text/textarea/upload/uploadlist
        /// </summary>
       // [JsonIgnore]
        public string type { get; set; }
        /// <summary>
        /// 类型
        /// text/textarea/upload/uploadlist
        /// </summary>
        //[NotMapped]
        //public string chr_type { get { return type; } }
        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string defaultvalue { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int sort { get; set; }


        /// <summary>
        /// 选项值
        /// </summary>
        public string formart_value { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string remark { get; set; }

    }
}
