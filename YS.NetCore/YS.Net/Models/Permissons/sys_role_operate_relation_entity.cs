using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_role_operate_relation")]
    public class sys_role_operate_relation_entity
    {
        /// <summary>        
        ///
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public long role_id { get; set; }


        public string module_code { get; set; }

        public string res_code { get; set; }

        public string operate_code { get; set; }

        [NotMapped]
        public string operate_name { get; set; }


        [NotMapped]
        public string api_url { get; set; }

        [NotMapped]
        public StatusEnum is_menu { get; set; }


    }
}
