using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_user_role_relation")]
    public class sys_user_role_relation_entity
    {
        /// <summary>        
        ///
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public long role_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public long user_id { get; set; }
    }
}
