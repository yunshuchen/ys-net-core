﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_catalog")]
    public class sys_catalog_entity
    {
        /// <summary>        
        ///
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string chr_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string chr_code { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string description { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DeleteEnum delete_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public StatusEnum enable_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public StatusEnum status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime last_modified_time { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime created_time { get; set; }
    }
}
