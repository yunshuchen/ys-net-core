﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    /// <summary>
    /// 用户修改密码的日志记录
    /// </summary>
    [Serializable]
    [Table("sys_user_pwd_log")]
    public class sys_user_pwd_log
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long user_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string new_pwd { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime mod_time { get; set; }
    }
}
