﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    /// <summary>
    /// 安全策略表
    /// </summary>
    [Serializable]
    [Table("sys_strategy")]
    public class sys_strategy
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string chr_title { get; set; }
        /// <summary>
        /// 类型:ip,time,power_pwd,mod_pwd,login_cooling
        /// </summary>
        public string chr_type { get; set; }
        /// <summary>
        /// 开始条件
        /// </summary>
        public string begin_condition { get; set; }
        /// <summary>
        /// 结束条件
        /// </summary>
        public string end_condition { get; set; }
    }
}
