﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    /// <summary>
    /// 用户策略关系表
    /// </summary>
    [Serializable]
    [Table("sys_user_strategy")]
    public class sys_user_strategy
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long user_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long strategy_id { get; set; }
    }
}
