using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_operate")]
    public class sys_operate_entity
    {
        /// <summary>        
        ///
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public long p_res { get; set; }

        public string p_res_code { get; set; }


        public long p_module { get; set; }

        public string p_module_code { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string operate_code { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string operate_name { get; set; }


        public string api_url { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string description { get; set; }

        /// <summary>
        /// 是否未菜单权限
        /// Legal 菜单
        /// UnLegal 数据权限
        /// </summary>
        public StatusEnum is_menu { get; set; }

        /// <summary>        
        ///
        /// </summary>
        public DeleteEnum delete_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public StatusEnum enable_status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public StatusEnum status { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string last_modifier_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime last_modified_time { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_id { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public string creator_name { get; set; }
        /// <summary>        
        ///
        /// </summary>
        public DateTime created_time { get; set; }
    }
}
