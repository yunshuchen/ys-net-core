﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Models
{

    public enum LuceneContentType
    {
        Node = 1,
        Content = 2,
        Doc = 3,
        Fund_Doc = 4
    }
    public class LuceneModel
    {

        public string identity_key { get => string.Concat(chr_type.ToString(), "_", this.id, "_", this.lang); }

        /// <summary>
        /// 主键ID
        /// </summary>
        public long row_key { get; set; }
        /// <summary>
        /// 数据类型
        /// </summary>
        public LuceneContentType chr_type { get; set; }
        /// <summary>
        /// 数据ID
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 分类代号
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 分类代号
        /// </summary>
        public string link { get; set; }
        /// <summary>
        /// 语种
        /// tc
        /// sc
        /// en
        /// </summary>
        public string lang { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string desc { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string chr_content { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string file_path { get; set; }
        /// <summary>
        /// 发布时间
        /// </summary>
        public DateTime publish_date { get; set; }
    }
}
