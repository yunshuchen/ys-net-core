﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Net.Models
{
    public class JResult
    {
        public static CommResult Error(string errMsg)
        {
            var result= new CommResult();
            result.code = -1;
            result.message = errMsg;
            result.result = false;
            return result;
        }

        public static CommResult Success()
        {
            var result= new CommResult();
            result.result = true;
            result.code = 0;
            return result;
        }


        public static CommResult Success(object resultData)
        {
            var result = new CommResult();
            result.result = true;
            result.code = 0;
            result.data = resultData;
            return result;
        }


        public static CommResult Success(string v)
        {
            var result = new CommResult();
            result.result = true;
            result.code = 0;
            result.message = v;
            return result;
        }
    }

    public class CommResult
    {

        public bool result { get; set; }

        public object data { get; set; }

        public int code { get; set; }

        public string message { get; set; }   

    }
}
