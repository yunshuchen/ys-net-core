﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using Newtonsoft.Json;
using YS.Utils.Models;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_content")]
    public class site_content 
    {
        /// <summary>
        /// 列标识,自增长,唯一标识
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 记录ID
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 语种标识：tc，en，sc
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        public int lang_flag { get; set; }
        /// <summary>
        /// 栏目ID
        /// </summary>
        public long node_id { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        public string link { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        public string summary { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string chr_content { get; set; }
        /// <summary>
        /// 扩展模型以json格式存储
        /// </summary>
        [JsonIgnore]
        public string extend_fields_json { get; set; }


        //[NotMapped]
        //private dynamic _dynamic_extend_fields = null;
        //[NotMapped]
        //public dynamic dynamic_extend_fields
        //{
        //    get
        //    {
        //        if (_dynamic_extend_fields == null)
        //        {
        //            _dynamic_extend_fields = new System.Dynamic.ExpandoObject();
                    

        //            if (this.extend_fields_json.MyToString() != "")
        //            {
        //                if (_extend_fields == null)
        //                {
        //                    var temp = JsonHelper.ToObject<dynamic>(this.extend_fields_json.MyToString());

        //                    foreach (var property in temp)
        //                    {
        //                        _dynamic_extend_fields[property.key.ToString()] = property.value.ToString();
                            
        //                    }
        //                }
        //            }
        //        }
        //        return _dynamic_extend_fields;
        //    }

        //}
        [NotMapped]
        private List<sys_ext_model_detail> _extend_fields = null;
        /// <summary>        
        ///扩展字段
        ///{[key:"",value:"",sort:12],多个分开}
        ///GetExtModelField("key","defaultvalue")
        /// </summary>
        [NotMapped]

        public List<sys_ext_model_detail> extend_fields
        {
            get
            {
                if (this.extend_fields_json.MyToString() != "")
                {
                    if (_extend_fields == null)
                    {
                        var temp = JsonHelper.ToObject<dynamic>(this.extend_fields_json.MyToString());

                        List<sys_ext_model_detail> tc_list = new List<sys_ext_model_detail>();

                        foreach (var property in temp)
                        {
                            tc_list.Add(new sys_ext_model_detail()
                            {
                                key = property.key.ToString(),
                                value = property.value.ToString()
                            });
                        }

                        _extend_fields = tc_list;
                    }
                }
                return _extend_fields;

            }
        }
        [NotMapped]
        public string ext_model_code { get; set; }
        /// <summary>
        /// 封面
        /// </summary>
        public string cover { get; set; }
        /// <summary>
        /// 图集
        /// </summary>
        public string atlas { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public ReViewStatusEnum review_status { get; set; }


        /// <summary>
        /// 排序
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>
        /// 是否首页
        /// </summary>
        public StatusEnum is_home { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public StatusEnum is_top { get; set; }
        /// <summary>
        /// 是否栏目页
        /// </summary>
        public StatusEnum is_node { get; set; }
        /// <summary>
        /// 查看数
        /// </summary>
        public int view_count { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        public StatusEnum status { get; set; }


        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [JsonIgnore]
        public long creator { get; set; }
        /// <summary>
        /// 创建者名
        /// </summary>
        public string creator_name { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        public string tag { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        public string author { get; set; }
        /// <summary>
        /// 来源
        /// </summary>
        public string source { get; set; }
        /// <summary>
        /// 删除状态
        /// </summary>
        public DeleteEnum del_flag { get; set; }
        /// <summary>
        /// 下载文件
        /// </summary>
        public string down_file { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public StatusEnum is_show { get; set; }

        /// <summary>
        /// 发布日期
        /// </summary>
        public string publish_date { get; set; }
    }
}
