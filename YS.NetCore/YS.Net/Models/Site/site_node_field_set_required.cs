﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_node_field_set_required")]
    public class site_node_field_set_required
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string field_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string field_code { get; set; }
    }
}
