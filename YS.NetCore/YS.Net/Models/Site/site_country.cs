﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_country")]
    public class site_country
    {
        /// <summary>
        /// 
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int lang_flag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string country_code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime create_time { get; set; }
    }
}
