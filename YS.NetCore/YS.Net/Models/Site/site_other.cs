﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_other")]
    public class site_other
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 主键自增长
        /// </summary>
        [Key]
        [Description("主键自增长")]
        public long id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Description("名称")]
        public string chr_title { get; set; }
        /// <summary>
        /// 代号
        /// </summary>
        [Description("代号")]
        public string chr_code { get; set; }
        /// <summary>
        /// 语种
        /// </summary>
        [Key]
        [Description("语种")]
        public string lang { get; set; }
        /// <summary>
        /// 语言标识
        /// </summary>
        [Description("语言标识")]
        public int lang_flag { get; set; }

        public StatusEnum is_show { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Description("内容")]
        public string chr_content { get; set; }
        /// <summary>
        /// 是否长期有效:1 长期有效,0时间段內有效
        /// </summary>
        [Description("是否长期有效")]
        public StatusEnum off_status { get; set; }


        /// <summary>
        /// 有效开始时间
        /// </summary>
        [Description("有效开始时间")]
        public string begin_time { get; set; }

        /// <summary>
        /// 有效结束时间
        /// </summary>
        [Description("有效结束时间")]
        public string end_time { get; set; }

    }
}
