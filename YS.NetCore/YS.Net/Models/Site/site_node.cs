﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using Newtonsoft.Json;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_node")]
    public class site_node 
    {
        /// <summary>
        /// 资增长ID
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 语种标识：tc，en，sc
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        public int lang_flag { get; set; }
        /// <summary>
        /// 模型类型
        /// </summary>
        [JsonIgnore]
        public string model_type { get; set; }
        /// <summary>
        /// 栏目类型 1文章,2链接
        /// </summary>
        [JsonIgnore]
        public NodeTypeEnum node_type { get; set; }
        /// <summary>
        /// 链接地址
        /// </summary>
        public string out_link { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 父级栏目
        /// </summary>
        public long parent_node { get; set; }


        /// <summary>
        /// [NotMapping]父级栏目
        /// </summary>
        [NotMapped]
        public string parent_node_name { get; set; }
        /// <summary>
        /// 级别路径
        /// </summary>
        public string leave_path { get; set; }
        /// <summary>
        /// 栏目名称
        /// </summary>
        public string node_name { get; set; }
        /// <summary>
        /// 栏目模板
        /// </summary>
        public string node_tpl { get; set; }
        /// <summary>
        /// 列表模板
        /// </summary>
        public string list_tpl { get; set; }
        /// <summary>
        /// 内容模板
        /// </summary>
        public string content_tpl { get; set; }
        /// <summary>
        /// 栏目图片
        /// </summary>
        public string node_pic { get; set; }
        /// <summary>
        /// 栏目图集
        /// </summary>
        public string node_atlas { get; set; }
        /// <summary>
        /// 栏目说明
        /// </summary>
        public string node_desc { get; set; }
        /// <summary>
        /// 栏目备注
        /// </summary>
        public string node_remark { get; set; }
        /// <summary>
        /// 是否需要审核
        /// </summary>
        public StatusEnum need_review { get; set; }
        /// <summary>
        /// mate关键字
        /// </summary>
        public string meta_keywords { get; set; }
        /// <summary>
        /// mate说明
        /// </summary>
        public string mate_description { get; set; }
        /// <summary>
        /// 展示菜单
        /// </summary>
        public StatusEnum show_menu { get; set; }
        /// <summary>
        /// 展示导航
        /// </summary>
        public StatusEnum show_path { get; set; }


        /// <summary>
        /// 排序
        /// </summary>
        public int nsort { get; set; }
        /// <summary>
        /// 扩展模型内容
        /// </summary>
        [JsonIgnore]
        public string extend_fields_json { get; set; }



        /// <summary>        
        ///NotMapped
        ///扩展字段
        ///{[key:"",value:"",sort:12],多个分开}
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public List<sys_ext_model_detail> extend_fields
        {
            get
            {
                if (extend_fields_json.MyToString() == "")
                {
                    return null;
                }
                return JsonHelper.ToObject<List<sys_ext_model_detail>>(this.extend_fields_json.MyToString());
            }
        }




        /// <summary>
        /// 栏目使用的扩展字段模型代号
        /// </summary>
        public string node_ext_model { get; set; }

        /// <summary>
        /// 栏目内容使用的扩展字段模型代号
        /// </summary>
        public string ext_model { get; set; }

        /// <summary>
        /// 进入安全密码
        /// </summary>
        public string pwd { get; set; }

        /// <summary>
        /// 下载文件的集合
        /// </summary>
        public string down_list { get; set; }

        /// <summary>
        /// NotMapped是否有下级数据
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public bool hava_child { get; set; }

        /// <summary>
        /// NotMapped同级的栏目数
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public int same_level_count { get; set; }

        /// <summary>
        /// 审核状态
        /// </summary>
        public ReViewStatusEnum review_status { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public StatusEnum is_show { get; set; }

        /// <summary>
        /// 是否进行全文索引
        /// </summary>
        public StatusEnum lucene_index { get; set; }

        /// <summary>
        /// 是否删除
        /// </summary>
        public DeleteEnum del_flag { get; set; }

        public DateTime create_time { get; set; }

    }
}
