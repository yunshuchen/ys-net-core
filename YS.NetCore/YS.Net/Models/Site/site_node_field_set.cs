﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_node_field_set")]
    public class site_node_field_set
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 字段代号
        /// </summary>
        public string field_code { get; set; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string field_name { get; set; }
    }
}
