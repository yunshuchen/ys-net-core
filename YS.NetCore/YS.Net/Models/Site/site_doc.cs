﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_doc")]
    public class site_doc
    {
        /// <summary>
        /// 
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int lang_flag { get; set; }
        /// <summary>
        /// 模块分类
        /// </summary>
        public string module { get; set; }

        /// <summary>
        /// 栏目id
        /// </summary>
        public long node_id { get; set; }

        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 说明
        /// </summary>
        public string chr_desc { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string chr_content { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string img { get; set; }
        /// <summary>
        /// 文件路径
        /// </summary>
        public string file_link { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        public string link { get; set; }

        


        /// <summary>
        /// 作者名称
        /// </summary>
        public string author { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        public string source { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        public StatusEnum is_top { get; set; }
        /// <summary>
        /// 是否是新
        /// </summary>
        public StatusEnum is_new { get; set; }
        /// <summary>
        /// 是否需要登录
        /// </summary>
        public StatusEnum is_login { get; set; }
        /// <summary>
        /// 日期
        /// </summary>
        public string date { get; set; }
       
        /// <summary>
        /// 创建者用户ID
        /// </summary>
        public long create_id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        public string create_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>
        /// 是否审核
        /// </summary>
        public ReViewStatusEnum valid { get; set; }
        /// <summary>
        /// 是否审核
        /// </summary>
        public StatusEnum is_show { get; set; }
    }
}
