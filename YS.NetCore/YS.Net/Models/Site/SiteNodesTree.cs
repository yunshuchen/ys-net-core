﻿using YS.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Models
{
    public class SiteNodesTree
    {

        public long id { get; set; }

        public long pid { get; set; }
        public string title { get; set; }

        public string node_code { get; set; }

        public bool spread { get; set; }

      

        public string model_type { get; set; }
        /// <summary>
        /// 是否需要审核
        /// </summary>
        public StatusEnum need_review { get; set; }


        public bool view_disable { get; set; }

        public bool add_disable { get; set; }


        public bool mod_disable { get; set; }
        public bool del_disable { get; set; }

        public string out_link { get; set; }


        public string node_pic { get; set; }
        public string node_atlas { get; set; }

        public string node_desc { get; set; }
        public string node_remark
        {
            get; set;

        }
        public List<SiteNodesTree> children { get; set; }
    }
    public class ParentSiteNode
    {
        public site_node model { get; set; }
        public ParentSiteNode parent_node { get; set; }
    }


        public class SiteWebNodesTree
    {

        public long id { get; set; }

        public long pid { get; set; }
        public string title { get; set; }

        public string node_code { get; set; }
        public string node_name { get; set; }

        public bool spread { get; set; }

        public List<SiteWebNodesTree> children { get; set; }

        public string model_type { get; set; }
        /// <summary>
        /// 是否需要审核
        /// </summary>
        public StatusEnum need_review { get; set; }


        public bool view_disable { get; set; }

        public bool add_disable { get; set; }

        public string out_link { get; set; }

        public string node_pic { get; set; }
        public string node_atlas { get; set; }

        public string node_desc { get; set; }
        public string node_remark { get; set; }

    }
}
