﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_notice")]
    public class site_notice
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        [Description("标题")]
        public string chr_title { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Description("简介")]
        public string chr_desc { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Description("内容")]
        public string chr_content { get; set; }
        /// <summary>
        /// 接受者
        /// </summary>
        [Description("接受者")]
        public string recipient { get; set; }

        /// <summary>
        /// 是否新数据
        /// </summary>
        [NotMapped]
        public int is_new { get; set; }
        /// <summary>
        /// 已查看会员
        /// </summary>
        [Description("已查看会员")]
        public string read_user { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        [Description("创建者ID")]
        public long create_user { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        [Description("创建者名称")]
        public string create_username { get; set; }
    }
}
