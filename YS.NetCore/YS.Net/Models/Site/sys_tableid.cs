﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("sys_tableid")]
    public class sys_tableid
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string table_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long max_id { get; set; }
    }
}
