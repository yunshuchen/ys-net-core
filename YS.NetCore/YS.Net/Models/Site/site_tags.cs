﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_tags")]
    public class site_tags
    {
        /// <summary>
        /// 列标识,自增长,唯一标识
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 记录ID
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 语种标识：tc，en，sc
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        public int lang_flag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string first_letter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tag_link { get; set; }
    }
}
