﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_doc_type")]
    public class site_doc_type
    {


        /// <summary>
        /// 
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int lang_flag { get; set; }

        /// <summary>
        /// 栏目ID 
        /// </summary>
        public long node_id { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        public string node_code { get; set; }




        /// <summary>
        /// 栏目名称
        /// </summary>
        public string node_name { get; set; }
        /// <summary>
        /// 代号
        /// </summary>
        public string module { get; set; }
        /// <summary>
        /// 标题
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 排序
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time { get; set; }
        /// <summary>
        /// 创建者ID
        /// </summary>
        public long create_id { get; set; }
        /// <summary>
        /// 创建者名称
        /// </summary>
        public string create_name { get; set; }
    }
}
