﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_basedata")]
    public class site_basedata
    {
        /// <summary>
        /// 
        /// </summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long row_key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public string lang { get; set; }
        /// <summary>
        /// 
        /// </summary>
        
        public int lang_flag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string module { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long parent { get; set; }


        public string parent_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string chr_desc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string chr_content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string img { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string thumb { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string link { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string file_link { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int n_sort { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime create_time { get; set; }
    }
}
