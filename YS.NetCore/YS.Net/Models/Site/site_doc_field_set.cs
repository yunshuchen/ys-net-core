﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_doc_field_set")]
    public class site_doc_field_set
    {
        /// <summary>
        /// 
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string doc_type_module { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string field_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string field_code { get; set; }
    }
}
