﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_content_marks")]
    public class site_content_marks
    {
        /// <summary>
        /// 列标识,自增长,唯一标识
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("列标识,自增长,唯一标识")]
        public long id { get; set; }
        /// <summary>
        /// 关联留痕记录ID
        /// </summary>
        [Description("关联留痕记录ID")]
        public long mark_id { get; set; }
        /// <summary>
        /// 1旧数据,2新数据
        /// </summary>
        [Description("1旧数据,2新数据")]
        public int data_type { get; set; }
        /// <summary>
        /// 记录ID
        /// </summary>
        [Description("记录ID")]
        public long data_id { get; set; }
        /// <summary>
        /// 语种标识：tc，en，sc
        /// </summary>
        [Description("语种标识：tc，en，sc")]
        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        [Description("1此记录只用于繁体 2表示此记录既用于繁体有用于简体s")]
        public int lang_flag { get; set; }
        
        /// <summary>
        /// 标题
        /// </summary>
        [Description("标题")]
        public string title { get; set; }
        /// <summary>
        /// 链接
        /// </summary>
        [Description("链接")]
        public string link { get; set; }
        /// <summary>
        /// 简介
        /// </summary>
        [Description("简介")]
        public string summary { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        [Description("内容")]
        public string chr_content { get; set; }
        /// <summary>
        /// 扩展模型以json格式存储
        /// </summary>
        [Description("扩展模型以json格式存储")]
        public string extend_fields_json { get; set; }
        /// <summary>
        /// 封面
        /// </summary>
        [Description("封面")]
        public string cover { get; set; }
        /// <summary>
        /// 图集
        /// </summary>
        [Description("图集")]
        public string atlas { get; set; }

        /// <summary>
        /// 是否首页
        /// </summary>
        [Description("是否首页")]
        public StatusEnum is_home { get; set; }
        /// <summary>
        /// 是否置顶
        /// </summary>
        [Description("是否置顶")]
        public StatusEnum is_top { get; set; }
        /// <summary>
        /// 是否栏目页
        /// </summary>
        [Description("是否栏目页")]
        public StatusEnum is_node { get; set; }
        /// <summary>
        /// 标签
        /// </summary>
        [Description("标签")]
        public string tag { get; set; }
        /// <summary>
        /// 作者
        /// </summary>
        [Description("作者")]
        public string author { get; set; }
        /// <summary>
        /// 来源
        /// </summary>
        [Description("来源")]
        public string source { get; set; }

        /// <summary>
        /// 下载文件
        /// </summary>
        [Description("下载文件")]
        public string down_file { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        [Description("是否显示")]
        public StatusEnum is_show { get; set; }

        /// <summary>
        /// 发布时间
        /// </summary>
        [Description("发布时间")]
        public string publish_date { get; set; }
    }
}
