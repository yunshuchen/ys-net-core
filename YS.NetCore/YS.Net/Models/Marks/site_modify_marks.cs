﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_modify_marks")]
    public class site_modify_marks
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("主键")]
        public long id { get; set; }
        /// <summary>
        /// 类型:1栏目,2内容
        /// </summary>
        [Description("类型:1栏目,2内容")]
        public int chr_type { get; set; }
        /// <summary>
        /// 操作时间
        /// </summary>
        [Description("操作时间")]
        public DateTime create_time { get; set; }
        /// <summary>
        /// 操作人ID
        /// </summary>
        [Description("操作人ID")]
        public long create_id { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Description("操作人")]
        public string create_name { get; set; }
        /// <summary>
        /// 操作IP
        /// </summary>
        [Description("操作IP")]
        public string operate_ip { get; set; }
    }
}
