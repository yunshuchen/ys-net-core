﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
namespace YS.Net.Models
{
    [Serializable]
    [Table("site_node_marks")]
    public class site_node_marks
    {
        /// <summary>
        /// 列标识,自增长,唯一标识
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("列标识,自增长,唯一标识")]
        public long id { get; set; }
        /// <summary>
        /// 关联留痕记录ID
        /// </summary>
        [Description("关联留痕记录ID")]
        public long mark_id { get; set; }
        /// <summary>
        /// 1旧数据,2新数据
        /// </summary>
        [Description("1旧数据,2新数据")]
        public int data_type { get; set; }
        /// <summary>
        /// 关联数据ID
        /// </summary>
        [Description("关联数据ID")]
        public long data_id { get; set; }
        /// <summary>
        /// 语种标识：tc，en，sc
        /// </summary>
        [Description("语种标识：tc，en，sc")]
        public string lang { get; set; }
        /// <summary>
        /// 1此记录只用于繁体 2表示此记录既用于繁体有用于简体s
        /// </summary>
        [Description("1此记录只用于繁体 2表示此记录既用于繁体有用于简体s")]
        public int lang_flag { get; set; }
        /// <summary>
        /// 模型类型
        /// </summary>
        [Description("模型类型")]
        public string model_type { get; set; }
        /// <summary>
        /// 栏目类型 1文章,2链接
        /// </summary>
        [Description("栏目类型 1文章,2链接")]
        public NodeTypeEnum node_type { get; set; }
        /// <summary>
        /// 链接地址
        /// </summary>
        [Description("链接地址")]
        public string out_link { get; set; }
        /// <summary>
        /// 栏目代号
        /// </summary>
        [Description("栏目代号")]
        public string node_code { get; set; }
        /// <summary>
        /// 父级栏目
        /// </summary>
        [Description("父级栏目")]
        public long parent_node { get; set; }

        /// <summary>
        /// 栏目名称
        /// </summary>
        [Description("栏目名称")]
        public string node_name { get; set; }
        /// <summary>
        /// 栏目模板
        /// </summary>
        [Description("栏目模板")]
        public string node_tpl { get; set; }
        /// <summary>
        /// 列表模板
        /// </summary>
        [Description("列表模板")]
        public string list_tpl { get; set; }
        /// <summary>
        /// 内容模板
        /// </summary>
        [Description("内容模板")]
        public string content_tpl { get; set; }
        /// <summary>
        /// 栏目图片
        /// </summary>
        [Description("栏目图片")]
        public string node_pic { get; set; }
        /// <summary>
        /// 栏目图集
        /// </summary>
        [Description("栏目图集")]
        public string node_atlas { get; set; }
        /// <summary>
        /// 栏目说明
        /// </summary>
        [Description("栏目说明")]
        public string node_desc { get; set; }
        /// <summary>
        /// 栏目备注
        /// </summary>
        [Description("栏目备注")]
        public string node_remark { get; set; }
        /// <summary>
        /// 是否需要审核
        /// </summary>
        [Description("是否需要审核")]
        public StatusEnum need_review { get; set; }
        /// <summary>
        /// mate关键字
        /// </summary>
        [Description("mate关键字")]
        public string meta_keywords { get; set; }
        /// <summary>
        /// mate说明
        /// </summary>
        [Description("mate说明")]
        public string mate_description { get; set; }
        /// <summary>
        /// 展示菜单
        /// </summary>
        [Description("展示菜单")]
        public StatusEnum show_menu { get; set; }
        /// <summary>
        /// 展示导航
        /// </summary>
        [Description("展示导航")]
        public StatusEnum show_path { get; set; }

        /// <summary>
        /// 扩展模型内容
        /// </summary>
        [Description("扩展模型内容")]
        public string extend_fields_json { get; set; }
        /// <summary>
        /// 扩展模型代号
        /// </summary>
        [Description("扩展模型代号")]
        public string ext_model { get; set; }
        /// <summary>
        /// 进入安全密码
        /// </summary>
        [Description("进入安全密码")]
        public string pwd { get; set; }
        /// <summary>
        /// 下载文件的集合
        /// </summary>
        [Description("下载文件的集合")]
        public string down_list { get; set; }
        /// <summary>
        /// 是否显示
        /// </summary>
        [Description("是否显示")]
        public StatusEnum is_show { get; set; }
    }
}
