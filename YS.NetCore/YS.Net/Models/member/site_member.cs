﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
 using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Linq;
using YS.Utils.Models;

namespace YS.Net.Models
{
    [Serializable]
    [Table("site_member")]
    public class site_member : ISite_Member
    {
        /// <summary>
        /// 主键
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Description("主键")]
        public long id { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        [Description("昵称")]
        public string nick_name { get; set; }

        /// <summary>
        /// 账号
        /// </summary>
        [Description("账号")]
        public string chr_account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [Description("密码")]
        [JsonIgnore]
        public string chr_pwd { get; set; }
        /// <summary>
        /// 头像图片
        /// </summary>
        [Description("头像图片")]
        public string avatar_url { get; set; }
        /// <summary>
        /// 0 未知;1男;2女
        /// </summary>
        [Description("0 未知;1男;2女")]
        public GenderEnum gender { get; set; }
        /// <summary>
        /// 国家
        /// </summary>
        [Description("国家")]
        public string country { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        [Description("省份")]
        public string province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        [Description("城市")]
        public string city { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Description("电话")]
        public string tel_no { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string open_id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string union_id { get; set; }

        /// <summary>
        /// 登录的sessionKey
        /// </summary>
        /// </summary>
        [Description("登录的sessionId")]
        public string session_id { get; set; }

        [Description("登录的sessionKey")]
        [JsonIgnore]
        public string session_key { get; set; }

        /// <summary>
        /// 是否已补全信息
        /// </summary>
        /// </summary>
        [JsonIgnore]
        public StatusEnum comple_userinfo { get; set; }
        /// <summary>
        /// 最后补全信息时间
        /// </summary>
        [JsonIgnore]
        public DateTime last_comple_date { get; set; }
        

        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime last_login_time { get; set; }


        /// <summary>
        /// JWT Token
        /// </summary>
        [NotMapped]
        public string token { get; set; }

        /// <summary>
        /// 是否需要补全用户信息
        /// </summary>
        [NotMapped]
        public StatusEnum comple_status { get; set; }

        /// <summary>
        /// t1,t2...tN
        /// </summary>
        public string tags { get; set; }


        /// <summary>
        /// 员工的标签属性
        /// </summary>
        [NotMapped]
        public List<string> Tag_Codes {
            get
            {
                List<string> result = new List<string>() { "public", "all" };
                if (this.review_status == ReViewStatusEnum.Pass)
                {
                    result.Add("member");
                }
                result.Add($"p{id}");
                //result.AddRange(tags.MyToString().Split(',').Select(a => $"t{a}"));
                result.AddRange(tags.MyToString().MySplit(','));
                return result;
            }
        }
        /// <summary>
        /// 最后补全信息时间
        /// </summary>
        [Description("审核状态")]
        public ReViewStatusEnum review_status { get; set; }


        /// <summary>
        /// 身份证号码
        /// </summary>
        public string id_card { get; set; }
        /// <summary>
        /// 权益卡号
        /// </summary>
        public string card_no { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string real_name { get; set; }

    }
}
