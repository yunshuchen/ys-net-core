

using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Route
{
    public interface IRouteDataProvider
    {
        int Order { get; }
        string ExtractVirtualPath(string path, RouteValueDictionary values);
    }
}
