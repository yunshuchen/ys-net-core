using YS.Utils.Extend;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using YS.Net.Route;
using System.Linq;

namespace YS.Net
{
    public class PageRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (routeDirection == RouteDirection.UrlGeneration) return false;

            const string start = "/";
            string path = start;
           
            path = $"{start}{values[routeKey]}";

            if (path != start)
            {
                var routeDataProviders = httpContext.RequestServices.GetService<IEnumerable<IRouteDataProvider>>();
                foreach (var item in routeDataProviders.OrderBy(m => m.Order))
                {
                    path = item.ExtractVirtualPath(path, values);
                }
            }
            values[routeKey] = path.ToLower();
            if (path == start) return true;
            //return httpContext.RequestServices.GetService<IPageService>().IsExists(path);
            return false;
        }
    }
}