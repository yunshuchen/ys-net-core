﻿using YS.Net.Mvc.Authorize;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using YS.Net.Services;
using YS.Utils;
using System.Collections.Concurrent;
using YS.Net.Models;
using YS.Utils.Cache;
using Microsoft.Extensions.Localization;

namespace YS.Net.Mvc
{
    /// <summary>
    /// 基本控制器，增删改查
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    /// <typeparam name="TPrimarykey">主键类型</typeparam>
    /// <typeparam name="TService">Service类型</typeparam>
    [Area("SiteAdmin")]
    [Route("SiteAdmin/[controller]/{action=Index}")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [DefaultAuthorize]
    public class SiteAdminController<TEntity, TService> : PluginBasicController<TEntity, TService>
        where TEntity : class
        where TService : IService<TEntity>
    {
        public SiteAdminController(TService service, IApplicationContextAccessor _httpContextAccessor) : base(service, _httpContextAccessor)
        {

        }
    }

}
