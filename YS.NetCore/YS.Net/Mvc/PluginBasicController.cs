﻿using YS.Net.Mvc.Authorize;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using YS.Net.Services;
using YS.Utils;
using System.Collections.Concurrent;
using YS.Net.Models;
using YS.Utils.Cache;
using Microsoft.Extensions.Localization;

namespace YS.Net.Mvc
{
    /// <summary>
    /// 插件的基本控制器，增删改查
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    /// <typeparam name="TService">Service类型</typeparam>
    [ApiExplorerSettings(IgnoreApi = true)]

    public class PluginBasicController<TEntity, TService> : Controller
        where TEntity : class
        where TService : IService<TEntity>
    {
        public IApplicationContextAccessor httpContextAccessor;
        public TService Service;

        private const string Sys_Res_Cache_Key = SessionKeyProvider.SysResourceCacheKey;


        private IStringLocalizerFactory localizerFactory = null;
        private IStringLocalizer string_localizer = null;


        private readonly ConcurrentDictionary<string, sys_resource_entity> sys_res_cache;
        public PluginBasicController(TService service, IApplicationContextAccessor _httpContextAccessor)

        {
            Service = service;
            httpContextAccessor = _httpContextAccessor;
            var cacheManager = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, sys_resource_entity>>>();
            sys_res_cache = cacheManager.GetOrAdd(Sys_Res_Cache_Key, new ConcurrentDictionary<string, sys_resource_entity>());

            if (sys_res_cache.Count <= 0)
            {
                var res_service = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISysResourceService>();
                res_service.ReloadCahce();

                sys_res_cache = cacheManager.GetOrAdd(Sys_Res_Cache_Key, new ConcurrentDictionary<string, sys_resource_entity>());
            }
        }

        private ISysLogsService _Logs = null;
        public ISysLogsService Logs
        {
            get
            {
                _Logs = _Logs == null ? httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISysLogsService>() : _Logs;
                return _Logs;
            }
        }
        public string L(string content)
        {
            if (localizerFactory == null)
            {
                localizerFactory = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<IStringLocalizerFactory>();
                string_localizer = localizerFactory.Create(null);
            }

            return string_localizer[content].Value;
        }

        public string L<T>(string content)
        {
            if (localizerFactory == null)
            {
                string plugin_name = typeof(T).Assembly.GetName().Name;
                localizerFactory = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<IStringLocalizerFactory>();
                string_localizer = localizerFactory.Create("", plugin_name);
            }
 
            return string_localizer[content].Value;
        }

        public string L(string content,string plugin_namespace)
        {
            if (localizerFactory == null)
            {
                localizerFactory = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<IStringLocalizerFactory>();
                string_localizer = localizerFactory.Create("", plugin_namespace);
            }

            return string_localizer[content].Value;
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        public void AddLog(string module, LogsOperateTypeEnum log_type, string remark)
        {
            string log_module_name = "";

            sys_resource_entity model = null;
            sys_res_cache.TryGetValue(module.ToLower(), out model);
            if (model != null)
            {
                log_module_name = model.res_name;
            }

            Logs.Insert(new sys_logs()
            {
                log_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                log_module = module,
                log_time = DateTime.Now,
                log_user = httpContextAccessor.Current.CurrentUser.id,
                log_user_name = httpContextAccessor.Current.CurrentUser.alias_name,
                operate_type = log_type,
                remark = remark,
                operate_type_name = EnumExt.GetDescriptionByEnum(log_type),
                log_module_name = log_module_name
            });
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        public void AddLog(string module, LogsOperateTypeEnum log_type, string remark, string module_name, long user_id, string user_name)
        {
            string log_module_name = module_name;


            Logs.Insert(new sys_logs()
            {
                log_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                log_module = module,
                log_time = DateTime.Now,
                log_user = user_id,
                log_user_name = user_name,
                operate_type = log_type,
                remark = remark,
                operate_type_name = EnumExt.GetDescriptionByEnum(log_type),
                log_module_name = log_module_name
            });
        }
        protected override void Dispose(bool disposing)
        {
            Service.Dispose();
            base.Dispose(disposing);
        }
    }
}
