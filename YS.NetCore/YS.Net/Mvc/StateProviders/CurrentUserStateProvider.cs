﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using YS.Utils.Mvc.StateProviders;
using YS.Utils.Models;
using YS.Utils;
using YS.Utils.Extend;
using YS.Net.Services;

namespace YS.Net.Mvc.StateProviders
{
    public class CurrentUserStateProvider : IApplicationContextStateProvider
    {

        private readonly IHttpContextAccessor _httpContextAccessor;
        public CurrentUserStateProvider(IHttpContextAccessor httpContextAccessor)
        {

            _httpContextAccessor = httpContextAccessor;
        }
        public string Name => "CurrentUser";
        IUser _currentUser;
        public Func<IApplicationContext, T> Get<T>()
        {
            //return context =>
            //{
            //    if (_currentUser != null)
            //    {
            //        return (T)_currentUser;
            //    }
            //    var httpContext = _httpContextAccessor.HttpContext;
            //    if (httpContext != null && httpContext.User.Identity.IsAuthenticated && httpContext.User.Identity.Name.IsNotNullAndWhiteSpace())
            //    {
            //        var currentUser = _httpContextAccessor.HttpContext.Session.Get<Models.sys_account>(Net.SessionKeyProvider.AdminLoginLoginUser);
            //        if (currentUser != null)
            //        {
            //            _currentUser = currentUser;
            //            return (T)_currentUser;
            //        }
            //        using (var userService = httpContext.RequestServices.GetService<ISysAccountService>())
            //        {
            //            _currentUser = userService.Get("",a=>a.account.ToLower()==httpContext.User.Identity.Name.ToLower());
            //            return (T)_currentUser;
            //        }

            //    }
            //    return default(T);
            //};


            return context =>
            {
                if (_currentUser != null)
                {
                    return (T)_currentUser;
                }
                var httpContext = _httpContextAccessor.HttpContext;
                if (httpContext != null && httpContext.User.Identity.IsAuthenticated && httpContext.User.Identity.Name.IsNotNullAndWhiteSpace())
                {
                    string chr_account = httpContext.User.Identity.Name.MyToString();
                    string account_id = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;

                    string login_token = "";
                    login_token = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Hash)?.Value;
                    if (chr_account.MyToString() == "" || account_id.MyToString() == "" || login_token.MyToString() == "")
                    {
                        return default(T);
                    }

                    var cache = httpContext.RequestServices.GetService<IMyCache>();
                    _currentUser = cache.Get<Models.sys_account>(string.Format(CacheKey.UserLoginInfoCackeKey, account_id, login_token));
                    if (_currentUser == null)
                    {
                        using (var userService = httpContext.RequestServices.GetService<ISysAccountService>())
                        {
                            _currentUser = userService.Get("", a => a.account.ToLower() == chr_account.ToLower() && a.token == login_token);
                            if (_currentUser != null)
                            {
                                cache.SetAsync(string.Format(CacheKey.UserLoginInfoCackeKey, account_id, login_token), _currentUser);
                            }
                        }
                    }
                    return (T)_currentUser;
                }
                return default(T);
            };
        }
    }
}
