﻿
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using YS.Utils.Mvc.StateProviders;
using YS.Utils.Models;
using YS.Utils;
using YS.Utils.Extend;
using YS.Net.Services;

namespace YS.Net.Mvc.StateProviders
{
    public class MemberCustomerStateProvider : IApplicationContextStateProvider
    {

        private readonly IHttpContextAccessor _httpContextAccessor;
        public MemberCustomerStateProvider(IHttpContextAccessor httpContextAccessor)
        {
           
            _httpContextAccessor = httpContextAccessor;
        }
        public string Name => "MemberCustomer";
        ISite_Member _currentUser;
        public Func<IApplicationContext, T> Get<T>()
        {
            return context =>
            {
                if (_currentUser != null)
                {
                    return (T)_currentUser;
                }
                var httpContext = _httpContextAccessor.HttpContext;
                if (httpContext != null && httpContext.User.Identity.IsAuthenticated && httpContext.User.Identity.Name.IsNotNullAndWhiteSpace())
                {
                    using (var userService = httpContext.RequestServices.GetService<ISiteMemberService>())
                    {
                        _currentUser = userService.GetByOne("id asc", a => a.open_id == httpContext.User.Identity.Name); return (T)_currentUser;
                    }
                }
                return default(T);
            };
        }
    }
}
