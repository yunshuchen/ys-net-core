﻿using YS.Net.Services;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using YS.Net;
using YS.Utils;
using YS.Net.Models;
using System.Collections.Concurrent;
using YS.Utils.Cache;

namespace YS.Net.Mvc
{
    /// <summary>
    /// 基本控制器，增删改查
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    /// <typeparam name="TService">Service类型</typeparam>
    [Area("api")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ApiBasicController<TEntity, TService> : ControllerBase
        where TEntity : class
        where TService : IService<TEntity>
    {
        public TService Service;
     
        public IApplicationContextAccessor httpContextAccessor;
        private const string Sys_Res_Cache_Key = SessionKeyProvider.SysResourceCacheKey;
        private readonly ConcurrentDictionary<string, sys_resource_entity> sys_res_cache;
        public ApiBasicController(TService service, IApplicationContextAccessor _httpContextAccessor)
        {
            httpContextAccessor = _httpContextAccessor;
            Service = service;

            var cacheManager = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, sys_resource_entity>>>();
            sys_res_cache = cacheManager.GetOrAdd(Sys_Res_Cache_Key, new ConcurrentDictionary<string, sys_resource_entity>());

            if (sys_res_cache.Count <= 0)
            {
                var res_service = httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISysResourceService>();
                res_service.ReloadCahce();

               sys_res_cache = cacheManager.GetOrAdd(Sys_Res_Cache_Key, new ConcurrentDictionary<string, sys_resource_entity>());
            }

        }
        private ISysLogsService _Logs = null;
        public ISysLogsService Logs
        {
            get
            {
                _Logs = _Logs == null ? httpContextAccessor.Current.HttpContextAccessor.HttpContext.RequestServices.GetService<ISysLogsService>() : _Logs;
                return _Logs;
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void AddLog(string res_code, LogsOperateTypeEnum log_type, string remark)
        {
            string log_module_name = "";
            if (res_code.ToLower() == "login")
            {
                log_module_name = "系统后台登录";
            }
            else
            {
                sys_resource_entity model = null;
                sys_res_cache.TryGetValue(res_code.ToLower(), out model);
                if (model != null)
                {
                    log_module_name = model.res_name;
                }
            }
            Logs.Insert(new sys_logs()
            {
                log_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                log_module = res_code,
                log_module_name = log_module_name,
                operate_type_name = EnumExt.GetDescriptionByEnum(log_type),
                log_time = DateTime.Now,
                log_user = httpContextAccessor.Current.CurrentUser.id,
                log_user_name = httpContextAccessor.Current.CurrentUser.alias_name,
                operate_type = log_type,
                remark = remark
            });
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public void AddLog(string res_code, LogsOperateTypeEnum log_type, string remark,long log_user,string log_user_name)
        {
            string log_module_name = "";
            if (res_code.ToLower() == "login")
            {
                log_module_name = "系统后台登录";
            }
            else
            {
                sys_resource_entity model = null;
                sys_res_cache.TryGetValue(res_code.ToLower(), out model);
                if (model != null)
                {
                    log_module_name = model.res_name;
                }
            }
            Logs.Insert(new sys_logs()
            {
                log_ip = httpContextAccessor.Current.HttpContextAccessor.IP(),
                log_module = res_code,
                log_module_name= log_module_name,
                operate_type_name = EnumExt.GetDescriptionByEnum(log_type),
                log_time = DateTime.Now,
                log_user = log_user,
                log_user_name = log_user_name,
                operate_type = log_type,
                remark = remark
            });
        }
    }
}
