using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;

namespace YS.Net.Mvc.Routing
{
    public class HomeRouteTransformer : DynamicRouteValueTransformer
    {

        private string lang = "sc";
        public HomeRouteTransformer()
        {
  
        }
        public HomeRouteTransformer(string _lang)
        {
            lang = _lang;
        }
        public override async ValueTask<RouteValueDictionary> TransformAsync(HttpContext httpContext, RouteValueDictionary values)
        {
          
            string path = values["path"].MyToString().ToLower();
            if (path.StartsWith("/"))
            {
                path = path.Substring(1);
            }
            if (!path.Contains("sc/") 
                && !path.Contains("tc/") 
                && !path.Contains("en/")
                &&!path.Contains("api") 
                //&& !path.Contains("docviewer")
                //&& !path.Contains("contact_process")
                //&& !path.Contains("appointment_process")
                && !path.Contains("captcha")
                && !path.Contains("swagger") 
                && !path.Contains("admin")
                && !path.Contains("siteadmin")
                && !path.Contains("static") 
                && !path.Contains("upload")
                &&!path.Contains("weixin")
                && !path.Contains("ewebeditor")
                && !Path.HasExtension(path)
                && !path.Contains("uploadfiles"))
            {

                List<string> HomeControllerPix = new List<string>()
                {
                    "",
                    "index",
                    "default"
                };

                List<string> Searcher = new List<string>()
                {
                    "search"
                };
                List<string> Error_Prx = new List<string>()
                {
                    "error"
                };
                List<string> DocViewer_Prx = new List<string>()
                {
                    "docviewer"
                };
                //
                //string SiteNodeRex = @"^\w+$";

                //string ListRex1 = @"^\w+/list$";
                //string ListRex2 = @"^\w+/list/\w+$";

                //string DetaileRex2 = @"^\w+/detail";
                //string DetaileRex1 = @"^\w+/detail/\w+$";

                //string DetaileJsonRex1 = @"^\w+/content_json";
                //string DetaileJsonRex2 = @"^\w+/content_json/\w+$";

                //string Search_list = @"^search/list/[1-9]\d/*$";
                //string Search_list0 = @"^search/list/[1-9]\d*$";
                //string Search_list1 = @"^search/list/$";
                //string Search_list2 = @"^search/list$";

                string SiteNodeRex = @"^[0-9a-zA-Z-_]{1,}$";
                string SiteNodeRex1 = @"[0-9a-zA-Z-_]{1,}/$";
                string ListRex = @"^[0-9a-zA-Z-_]{1,}/list/$";
                string ListRex1 = @"^[0-9a-zA-Z-_]{1,}/list$";
                string ListRex2 = @"^[0-9a-zA-Z-_]{1,}/list/[0-9a-zA-Z-_]{1,}$";

                string DetaileRex2 = @"^[0-9a-zA-Z-_]{1,}/detail";
                string DetaileRex1 = @"^[0-9a-zA-Z-_]{1,}/detail/[0-9a-zA-Z-_]{1,}$";

                string DetaileJsonRex1 = @"^[0-9a-zA-Z-_]{1,}/content_json";
                string DetaileJsonRex2 = @"^[0-9a-zA-Z-_]{1,}/content_json/[0-9a-zA-Z-_]{1,}$";

                string Search_list = @"^search/list/[1-9]\d/*$";
                string Search_list0 = @"^search/list/[1-9]\d*$";
                string Search_list1 = @"^search/list/$";
                string Search_list2 = @"^search/list$";

                var controller = "Home";
                var action = "Index";

                if (HomeControllerPix.Contains(path))
                {
                    action = "Index";
                    values["NodeCode"] = "index";
                }
                else if (Error_Prx.Contains(path))
                {
   
                    values["controller"] = "Home";
                    values["lang"] = lang;
                    values["action"] = "Error";
                    return values;
                }
                else if (Searcher.Contains(path))
                {
                    action = "Search";
                    values["NodeCode"] = "search";
                    values["page"] = 1;

                }
                else if (DocViewer_Prx.Contains(path))
                {
                    action = "Docviewer";
                    values["NodeCode"] = "DocViewer";
                    values["page"] = 1;
                }
                else if (RegexCheck.IsMatch(path, Search_list0)|| RegexCheck.IsMatch(path, Search_list))
                {
                    action = "Search";
                    values["NodeCode"] = "search";
                    values["page"] = path.Replace("search/list/","").Replace("search/list", "").Replace("/","");
                }
                else if (RegexCheck.IsMatch(path, Search_list1)|| RegexCheck.IsMatch(path, Search_list2))
                {
                    action = "Search";
                    values["NodeCode"] = "search";
                    values["page"] = 1;
                }
                else
                {

                    if (RegexCheck.IsMatch(path, SiteNodeRex)|| RegexCheck.IsMatch(path, SiteNodeRex1))
                    {

                        values["NodeCode"] = path.Replace("/","");
                        action = "Node";
                    }
                    else
                    {
                        List<string> paths = path.Split('/').ToList();
                        if (paths.Count >= 2)
                        {
                            if ((RegexCheck.IsMatch(path, ListRex1) || RegexCheck.IsMatch(path, ListRex2) || RegexCheck.IsMatch(path, ListRex))
                                && paths[1] == "list")
                            {

                                values["NodeCode"] = paths[0];
                                if (paths.Count == 2)
                                {
                                    values["Params"] = "";
                                }
                                else
                                {
                                    values["Params"] = string.Join('/', paths.Skip(2));
                                }
                                action = "List";
                            }
                            else if (RegexCheck.IsMatch(path, DetaileRex2) || RegexCheck.IsMatch(path, DetaileRex1)
                                && paths[1] == "detail")
                            {
                                values["NodeCode"] = paths[0];
                                if (paths.Count == 2)
                                {
                                    values["Params"] = "";
                                }
                                else
                                {
                                    values["Params"] = string.Join('/', paths.Skip(2));
                                }
                                action = "Detail";
                            }
                            else if (RegexCheck.IsMatch(path, DetaileJsonRex1) || RegexCheck.IsMatch(path, DetaileJsonRex2)
                               && paths[1] == "content_json")
                            {
                                values["NodeCode"] = paths[0];
                                if (paths.Count == 2)
                                {
                                    values["Params"] = "";
                                }
                                else
                                {
                                    values["Params"] = string.Join('/', paths.Skip(2));
                                }
                                action = "JsonDetail";
                            }
                        }
                    }
                }
                values["controller"] = controller;
                values["lang"] = lang;
                values["action"] = action;
                return values;
            }
            else
            {
                return values;
            }
          
        }
    }
}
