﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mvc.Authorize
{
    public interface IDynamicPermissionCheck
    {


        System.Threading.Tasks.Task<bool> CheckAsync(HttpContext httpContext, string module_code, string resource_code, string operate_code);
        System.Threading.Tasks.Task<bool> CheckAsync(HttpContext httpContext, string module_code, string resource_code);
        System.Threading.Tasks.Task<bool> CheckAsync(HttpContext httpContext, string module_code);
    }
}
