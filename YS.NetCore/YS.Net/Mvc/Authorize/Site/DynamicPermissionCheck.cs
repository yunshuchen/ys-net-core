﻿using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Cache;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using YS.Utils.DI;

namespace YS.Net.Mvc.Authorize
{
    [ScopedDI]
    public class DynamicPermissionCheck : IDynamicPermissionCheck
    {

        private readonly IAuthorizationHandler authorization;
        //用户角色关系
        private ISysUserRoleRelationService user_role_service;

        //角色操作关系缓存
        private ISysRoleOperateRelationService RoleOperate;

        //用户角色关系缓存
        private ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        private const string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;


        //角色操作关系缓存
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;
        private const string SysRoleOperateNodes = Net.SessionKeyProvider.SysRoleOperateRelation;

        private IApplicationContext context;
        public DynamicPermissionCheck(IAuthorizationHandler _authorization
            ,ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>> cacheManager
            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>> role_operate_cacheManager
            , ISysRoleOperateRelationService _RoleOperate
            ,IApplicationContext _context
            , ISysUserRoleRelationService _user_role_service
            )
        {
            context = _context;
            authorization = _authorization;
            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());
            role_operate_cahce = role_operate_cacheManager.GetOrAdd(SysRoleOperateNodes, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());
            RoleOperate = _RoleOperate;
            user_role_service = _user_role_service;
        }
        public async System.Threading.Tasks.Task<bool> CheckAsync(HttpContext httpContext, string module_code, string resource_code, string operate_code)
        {
           var user = context.CurrentUser;

           /* var current_authorization = (authorization as PermissionCheckPolicyHandler);
            var Schemes = await current_authorization.Schemes.GetAllSchemesAsync();
            var defaultAuthenticate = Schemes.Where(a => a.Name.ToLower() == "DefaultAuthenticationScheme".ToLower()).FirstOrDefault();

            if (defaultAuthenticate == null)
            {
                return false;
            }*/
          
            string user_name = httpContext.User.Identity.Name.MyToString();
            if (user_name == ""|| user.id.MyToLong()<=0)
            {
                return false;
            }
            //var result = await httpContext.AuthenticateAsync(defaultAuthenticate.Name);
            //if (!result.Succeeded)
            //{
            //    return false;
            //}

            try
            {
                checkPermission(user.id, user_name, module_code, resource_code, operate_code);
            }
            catch (Exception)
            {

                return false;
            }
            return true;
        }

  

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user_name"></param>
        /// <param name="module_code"></param>
        /// <param name="resource_code"></param>
        /// <param name="operate_code"></param>
        private void checkPermission(long user_id, string user_name, string module_code, string resource_codes, string operate_codes)
        {
            if(user_name=="admin")
            {
                return;
            }
            List<sys_user_role_relation_entity> user_role = null;
            user_role_cahce.TryGetValue(user_name, out user_role);
            if (user_role == null)
            {
                List<sys_user_role_relation_entity> cache_out = null;
                user_role_cahce.TryRemove(user_name.ToLower(), out cache_out);
                user_role = user_role_service.GetList("", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==user_id
                });
                //更新用户角色缓存
                user_role_cahce.TryUpdate(user_name.ToLower(), (List<sys_user_role_relation_entity> value11) => { return user_role; });

            }

            List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();


            //用户所有的权限
            List<sys_role_operate_relation_entity> list = null;
            user_role.ForEach(a => {
                role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                if (list == null)
                {
                    list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                         c=>c.role_id==a.role_id
                    });
                    role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                }
                user_operate_list.AddRange(list);
            });



            if (module_code.Length > 0)
            {
                var modules = user_operate_list.Where(a => a.module_code.ToLower() == module_code.ToLower());

                if (modules.Count() <= 0)
                {
                    throw new Exception("不具备操作权限");
                }
                if (resource_codes.Trim().Length > 0)
                {
                    var resource_code = resource_codes.Split(',').Select(a => a.ToLower());
                    var resource = modules.Where(a => resource_code.Contains(a.res_code.ToLower()));
                    if (resource.Count() <= 0)
                    {
                        throw new Exception("不具备操作权限");
                    }

                    if (operate_codes.Trim().Length > 0)
                    {
                        var operate_code = operate_codes.Split(',').Select(a => a.ToLower());
                        var operate = resource.Where(a => operate_code.Contains(a.operate_code.ToLower()));
                        if (operate.Count() <= 0)
                        {
                            throw new Exception("不具备操作权限");
                        }
                    }
                }
            }
        }

        public async Task<bool> CheckAsync(HttpContext httpContext, string module_code, string resource_code)
        {
            return await CheckAsync(httpContext, module_code, resource_code, "");

        }
        public async Task<bool> CheckAsync(HttpContext httpContext, string module_code)
        {
            return await CheckAsync(httpContext, module_code, "", "");

        }
        
    }
}
