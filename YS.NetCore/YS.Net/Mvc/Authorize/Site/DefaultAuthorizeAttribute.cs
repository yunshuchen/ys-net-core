using YS.Net.Models;
using YS.Net.Services;
using YS.Utils;
using YS.Utils.Cache;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace YS.Net.Mvc.Authorize
{
    public class DefaultAuthorizeAttribute : AuthorizeAttribute, IActionFilter
    {

        public string module_code { get; set; }
        public string resource_code { get; set; }
        public string operate_code { get; set; }
        public object JsonRequestBehavior { get; private set; }

        public DefaultAuthorizeAttribute(string module_code, string resource_code, string operate_code) : base()
        {
            this.module_code = module_code;
            this.resource_code = resource_code;
            this.operate_code = operate_code;
        }

        public DefaultAuthorizeAttribute(string module_code, string resource_code) : base()
        {
            this.module_code = module_code;
            this.resource_code = resource_code;
            this.operate_code = "";
        }


        public DefaultAuthorizeAttribute(string module_code) : base()
        {

            this.AuthenticationSchemes = DefaultAuthenticationScheme;
            this.module_code = module_code;
            this.resource_code = "";
            this.operate_code = "";
        }
        public const string DefaultAuthenticationScheme = "DefaultAuthenticationScheme";
        public DefaultAuthorizeAttribute()
        {
            this.AuthenticationSchemes = DefaultAuthenticationScheme;

            this.module_code = "";
            this.resource_code = "";
            this.operate_code = "";
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        //用户角色关系缓存
        private ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        private string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;

        //角色操作关系缓存
        private ISysRoleOperateRelationService RoleOperate;
        //角色操作关系缓存
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;
        private string SysRoleOperateNodes = Net.SessionKeyProvider.SysRoleOperateRelation;


        private ISysUserRoleRelationService user_role_service;
        public void OnActionExecuting(ActionExecutingContext context)
        {
            //context.ActionDescriptor
            var user = context.HttpContext.RequestServices.GetService<IApplicationContext>().CurrentUser;
            if (context.HttpContext.User.Identity.IsAuthenticated && user == null)
            {
                context.HttpContext.SignOutAsync(AuthenticationSchemes).Wait();
                context.Result = new RedirectResult("/admin/login");
                return;
            }
            if (module_code == "" && resource_code == "" && operate_code == "")
            {
                return;
            }
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>
                 cacheManager = context.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>>();
            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>
                role_operate_cacheManager = context.HttpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>>();
            role_operate_cahce = role_operate_cacheManager.GetOrAdd(SysRoleOperateNodes, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());
            RoleOperate = context.HttpContext.RequestServices.GetService<ISysRoleOperateRelationService>();

            user_role_service = context.HttpContext.RequestServices.GetService<ISysUserRoleRelationService>();



            bool have_error = true;


            have_error = this.checkPermission(user.id, user.account.ToLower(), module_code, resource_code, operate_code);

            if (!have_error)
            {
                //context.Result = new JsonResult(new { Code = 401, Msg = "会话失效，请重新登录" });
                context.Result = new RedirectResult("/admin/error/forbidden");
                return;
            }



        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="user_name"></param>
        /// <param name="module_code"></param>
        /// <param name="resource_code"></param>
        /// <param name="operate_code"></param>
        private bool checkPermission(long user_id, string user_name, string module_code, string resource_codes, string operate_codes)
        {
            if (user_name == "admin")
            {
                return true;
            }
            List<sys_user_role_relation_entity> user_role = null;
            user_role_cahce.TryGetValue(user_name, out user_role);
            if (user_role == null)
            {
                List<sys_user_role_relation_entity> cache_out = null;
                user_role_cahce.TryRemove(user_name.ToLower(), out cache_out);
                user_role = user_role_service.GetList("", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==user_id
                });
                //更新用户角色缓存
                user_role_cahce.TryUpdate(user_name.ToLower(), (List<sys_user_role_relation_entity> value11) => { return user_role; });
            }
            List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();
            //用户所有的权限
            List<sys_role_operate_relation_entity> list = null;
            user_role.ForEach(a =>
            {
                role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                if (list == null)
                {
                    list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                        c=>c.role_id==a.role_id
                    });
                    role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                }
                user_operate_list.AddRange(list);
            });

            if (module_code.Length > 0)
            {
                var modules = user_operate_list.Where(a => a.module_code.ToLower() == module_code.ToLower());

                if (modules.Count() <= 0)
                {
                    return false;
                }
                if (resource_codes.Trim().Length > 0)
                {
                    var resource_code = resource_codes.Split(',').Select(a => a.ToLower());
                    var resource = modules.Where(a => resource_code.Contains(a.res_code.ToLower()));
                    if (resource.Count() <= 0)
                    {
                        return false;
                    }

                    if (operate_codes.Trim().Length > 0)
                    {
                        var operate_code = operate_codes.Split(',').Select(a => a.ToLower());
                        var operate = resource.Where(a => operate_code.Contains(a.operate_code.ToLower()));
                        if (operate.Count() <= 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}