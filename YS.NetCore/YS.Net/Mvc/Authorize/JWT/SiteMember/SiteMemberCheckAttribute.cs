﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Cache;
using System.Linq.Expressions;

namespace YS.Net.Mvc.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class SiteMemberCheckAttribute : AuthorizeAttribute
    {
        
        public SiteMemberCheckAttribute() : base("SiteMember")
        {

        }
        
    }

    public class MemberCheckPolicyRequirement : IAuthorizationRequirement
    {
        public MemberCheckPolicyRequirement()
        {

        }
    }


    public class MemberCheckPolicyHandler : AttributeAuthorizationHandler<MemberCheckPolicyRequirement, SiteMemberCheckAttribute>
    {
        public IAuthenticationSchemeProvider Schemes { get; set; }
        private readonly IHttpContextAccessor httpContextAccessor;
        public MemberCheckPolicyHandler(IHttpContextAccessor _httpContextAccessor
    , IAuthenticationSchemeProvider schemes)
        {
            Schemes = schemes;
            httpContextAccessor = _httpContextAccessor;
        }
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext authoriazationContext, MemberCheckPolicyRequirement requirement, IEnumerable<SiteMemberCheckAttribute> attributes)
        {
            var httpContext = httpContextAccessor.HttpContext;
            //获取授权方式
            var defaultAuthenticate = await Schemes.GetDefaultAuthenticateSchemeAsync();
            //var Schemes = await current_authorization.Schemes.GetAllSchemesAsync();
            // var defaultAuthenticate = Schemes.Where(a => a.Name.ToLower() == "bearer").FirstOrDefault();
            if (defaultAuthenticate == null)
            {
                authoriazationContext.Fail();
                return;
            }
            string user_name = "";
            string account_id = "";
            account_id = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
            user_name = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Name)?.Value;

            if (user_name == "" || account_id == null || account_id == "")
            {
                authoriazationContext.Fail();
                return;
            }
            //验证签发的用户信息
            var result = await httpContext.AuthenticateAsync(defaultAuthenticate.Name);
            if (!result.Succeeded)
            {
                authoriazationContext.Fail();
                return;
            }
            var context = authoriazationContext.Resource as AuthorizationFilterContext;
            
            authoriazationContext.Succeed(requirement);
            return;
        }
    }
}


