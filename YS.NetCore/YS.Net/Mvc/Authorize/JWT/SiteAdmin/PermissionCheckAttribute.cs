﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using YS.Net.Models;
using YS.Net.Services;
using YS.Utils.Cache;
using System.Linq.Expressions;

namespace YS.Net.Mvc.Authorize
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class PermissionCheckAttribute : AuthorizeAttribute
    {
        public string module_code { get; set; }
        public string resource_code { get; set; }
        public string operate_code { get; set; }
        public PermissionCheckAttribute(string module_code, string resource_code, string operate_code) : base("Permission")
        {
            this.module_code = module_code;
            this.resource_code = resource_code;
            this.operate_code = operate_code;
        }

        public PermissionCheckAttribute(string module_code, string resource_code) : base("Permission")
        {
            this.module_code = module_code;
            this.resource_code = resource_code;
            this.operate_code = "";
        }

        public PermissionCheckAttribute() : base("Permission")
        {
            this.module_code = "";
            this.resource_code = "";
            this.operate_code = "";
        }
        public PermissionCheckAttribute(string module_code) : base("Permission")
        {
            this.module_code = module_code;
            this.resource_code = "";
            this.operate_code = "";
        }
    }

    public class PermissionCheckPolicyRequirement : IAuthorizationRequirement
    {
        public PermissionCheckPolicyRequirement()
        {

        }
    }


    public class PermissionCheckPolicyHandler : AttributeAuthorizationHandler<PermissionCheckPolicyRequirement, PermissionCheckAttribute>
    {


        //用户角色关系缓存
        private ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        private const string SysUserRoleNodes = SessionKeyProvider.SysUserRoleRelation;


        //角色操作关系缓存
        private ConcurrentDictionary<string, List<sys_role_operate_relation_entity>> role_operate_cahce;
        private const string SysRoleOperateNodes = SessionKeyProvider.SysRoleOperateRelation;

        //角色操作关系缓存
        private ISysRoleOperateRelationService RoleOperate;
        private ISysUserRoleRelationService user_role_service;


        public IAuthenticationSchemeProvider Schemes { get; set; }
        private readonly IHttpContextAccessor httpContextAccessor;

        public PermissionCheckPolicyHandler(IHttpContextAccessor _httpContextAccessor
            , IAuthenticationSchemeProvider schemes)
        {
            Schemes = schemes;
            httpContextAccessor = _httpContextAccessor;
        }
        private static IEnumerable<PermissionCheckAttribute> GetAttributes(MemberInfo memberInfo)
        {
            return memberInfo.GetCustomAttributes(typeof(PermissionCheckAttribute), false).Cast<PermissionCheckAttribute>();
        }
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext authoriazationContext, PermissionCheckPolicyRequirement requirement, IEnumerable<PermissionCheckAttribute> attributes)
        {
            var httpContext = httpContextAccessor.HttpContext;

            //用户角色缓存
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>
                 cacheManager = httpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>>();
            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());

            //角色操作缓存
            ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>
            role_operate_cacheManager = httpContext.RequestServices.GetService<ISkyCacheManager<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>>();
            role_operate_cahce = role_operate_cacheManager.GetOrAdd(SysRoleOperateNodes, new ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>());

            var online_user_cache = httpContext.RequestServices.GetService<IOnlineUserCache>();
             
            RoleOperate = httpContext.RequestServices.GetService<ISysRoleOperateRelationService>();

            user_role_service = httpContext.RequestServices.GetService<ISysUserRoleRelationService>();

            //获取授权方式
            var defaultAuthenticate = await Schemes.GetDefaultAuthenticateSchemeAsync();
            //var Schemes = await current_authorization.Schemes.GetAllSchemesAsync();
            // var defaultAuthenticate = Schemes.Where(a => a.Name.ToLower() == "bearer").FirstOrDefault();
            if (defaultAuthenticate == null)
            {
                
                authoriazationContext.Fail();
                return;
            }
            string user_name = "";
            string account_id = "";
            string token = "";
            account_id = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier)?.Value;
            user_name = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Name)?.Value;
            token = httpContext.User.FindFirst(System.Security.Claims.ClaimTypes.Hash)?.Value;

            if (user_name == ""|| account_id==null|| account_id==""|| token.MyToString()=="")
            {
                authoriazationContext.Fail();
                return;
            }


            //验证签发的用户信息
            var result = await httpContext.AuthenticateAsync(defaultAuthenticate.Name);
            if (!result.Succeeded)
            {
                authoriazationContext.Fail();
                return;
            }
            string cache_key = $"#{token}#{account_id}#";
            //获取缓存
            var cache_user = online_user_cache.GetOnlineUser(cache_key);
            if (cache_user == null)
            {
                //if (token.MyToString() == "")
                //{
                //    authoriazationContext.Fail();
                //    return;
                //}
                //cache_user = RoleOperate.GetByOne<SysAccountEntity>(a => a.token == token);
                //if (cache_user == null)
                //{
                //    authoriazationContext.Fail();
                //    return;
                //}
                //cache_user.last_refresh = DateTime.Now;
                //cache_user.put_out_login = DateTime.MaxValue;
                //online_user_cache.UpdateOnlineUser(cache_key, cache_user);
            }
            //var last_refresh_s = cache_user.last_refresh.MyToString();
            //var last_refresh = DateTime.MinValue;
            //DateTime.TryParse(last_refresh_s, out last_refresh);
            //DateTime Now = DateTime.Now;
            ////已经90秒没有刷新了
            //if ((Now - last_refresh).TotalSeconds >= 90)
            //{
            //    authoriazationContext.Fail();
            //    return;
            //}
            ////被其他用户踢下线
            //var put_out_login_s = cache_user.put_out_login.MyToString();
            //var put_out_login = DateTime.MinValue;
            //DateTime.TryParse(put_out_login_s, out put_out_login);
            //if (Now >= put_out_login)
            //{
            //    authoriazationContext.Fail();
            //    return;
            //}
            var context = authoriazationContext.Resource as AuthorizationFilterContext;
            bool have_error = false;
            foreach (var permissionAttribute in attributes)
            {
                try
                {
                    this.checkPermission(context, user_name.ToLower(), account_id.MyToLong(),permissionAttribute.module_code, permissionAttribute.resource_code, permissionAttribute.operate_code);
                }
                catch (Exception)
                {
                    have_error = true;
                    break;
                }
            }
            if (!have_error)
            {
                authoriazationContext.Succeed(requirement);
            }
            else
            {
                authoriazationContext.Fail();
            }
            authoriazationContext.Succeed(requirement);
            return;


        }
        private void checkPermission(AuthorizationFilterContext context, string user_name, long account_id, string module_code, string resource_codes, string operate_codes)
        {
            List<sys_user_role_relation_entity> user_role = null;
            user_role_cahce.TryGetValue(user_name, out user_role);
            if (user_role == null)
            {
                var user_db_list = user_role_service.GetList("", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                    a=>a.user_id==account_id
                });
                user_role = user_db_list;
                //更新用户角色缓存
                user_role_cahce.TryUpdate(user_name.ToLower(), (List<sys_user_role_relation_entity> value11) => { return user_db_list; });
                //  throw new Exception("不具备操作权限");
            }

            List<sys_role_operate_relation_entity> user_operate_list = new List<sys_role_operate_relation_entity>();



            //用户所有的权限
            List<sys_role_operate_relation_entity> list = null;
            user_role.ForEach(a =>
            {
                role_operate_cahce.TryGetValue(a.role_id.MyToString(), out list);
                if (list == null)
                {
                    list = RoleOperate.GetRoleOperateList("", new List<System.Linq.Expressions.Expression<Func<sys_role_operate_relation_entity, bool>>>() {
                         c=>c.role_id==a.role_id
                    });
                    role_operate_cahce.TryUpdate(a.role_id.MyToString(), (List<sys_role_operate_relation_entity> value11) => { return list; });
                }
                user_operate_list.AddRange(list);
            });



            if (module_code.Length > 0)
            {
                var modules = user_operate_list.Where(a => a.module_code.ToLower() == module_code.ToLower());

                if (modules.Count() <= 0)
                {
                    throw new Exception("不具备操作权限");
                }
                if (resource_codes.Trim().Length > 0)
                {
                    var resource_code = resource_codes.Split(',').Select(a => a.ToLower());
                    var resource = modules.Where(a => resource_code.Contains(a.res_code.ToLower()));
                    if (resource.Count() <= 0)
                    {
                        throw new Exception("不具备操作权限");
                    }

                    if (operate_codes.Trim().Length > 0)
                    {
                        var operate_code = operate_codes.Split(',').Select(a => a.ToLower());
                        var operate = resource.Where(a => operate_code.Contains(a.operate_code.ToLower()));
                        if (operate.Count() <= 0)
                        {
                            throw new Exception("不具备操作权限");
                        }
                    }
                }
            }
        }
    }
}


