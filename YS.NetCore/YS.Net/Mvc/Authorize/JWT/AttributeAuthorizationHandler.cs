﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace YS.Net.Mvc.Authorize
{
    public abstract class AttributeAuthorizationHandler<TRequirement, TAttribute> : AuthorizationHandler<TRequirement> where TRequirement : IAuthorizationRequirement where TAttribute : Attribute
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement)
        {
            var require = requirement as PermissionCheckPolicyRequirement;
            if (require != null)
            {

                var attributes = new List<TAttribute>();
                if (context.Resource is Endpoint endpoint)
                {
                    var cad = endpoint.Metadata.OfType<ControllerActionDescriptor>().FirstOrDefault();
                    attributes.AddRange(GetAttributes(cad.ControllerTypeInfo.UnderlyingSystemType));
                    attributes.AddRange(GetAttributes(cad.MethodInfo));
                }
                return HandleRequirementAsync(context, requirement, attributes);
            }
            else
            {
                var attributes = new List<TAttribute>();
                if (context.Resource is Endpoint endpoint)
                {
                    var cad = endpoint.Metadata.OfType<ControllerActionDescriptor>().FirstOrDefault();
                    attributes.AddRange(GetAttributes(cad.ControllerTypeInfo.UnderlyingSystemType));
                    attributes.AddRange(GetAttributes(cad.MethodInfo));
                }
                return HandleRequirementAsync(context, requirement, attributes);
            }
        }

        protected abstract Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement, IEnumerable<TAttribute> attributes);

        private static IEnumerable<TAttribute> GetAttributes(MemberInfo memberInfo)
        {
            return memberInfo.GetCustomAttributes(typeof(TAttribute), false).Cast<TAttribute>();
        }
    }
}
