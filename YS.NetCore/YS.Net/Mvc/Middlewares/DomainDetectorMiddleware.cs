﻿using YS.Utils;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace YS.Net.Mvc.Middlewares
{
    public class DomainDetectorMiddleware
    {

		private readonly RequestDelegate _next;

		public DomainDetectorMiddleware(RequestDelegate next, IWebHostEnvironment env)
		{
			HostingEnvironment = env;
			this._next = next;
		}

		public IWebHostEnvironment HostingEnvironment { get; set; }


		public async Task Invoke(HttpContext context)
		{
			HttpRequest Request = context.Request;
			HttpResponse Response = context.Response;
			long site_admin = context.Session.Get<long>("site_id");
			if (site_admin == 0)
			{
				//Request.Host
			}
			await _next.Invoke(context);



		}


		private string GetContentType()
		{
			return "text/plain";
		}
	}
}
