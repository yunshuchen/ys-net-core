﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YS.Net.Mvc.Middlewares
{
    public class RequestResponseLog
    {
        public string Url { get; set; }
        public IDictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
        public IDictionary<string, string> ResponseHeaders { get; set; } = new Dictionary<string, string>();
        public string Protocol { get; set; }
        public string Method { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public DateTime ExcuteStartTime { get; set; }
        public DateTime ExcuteEndTime { get; set; }
        public override string ToString()
        {
          
            return JsonHelper.ToJson(this);
        }
    }
}
