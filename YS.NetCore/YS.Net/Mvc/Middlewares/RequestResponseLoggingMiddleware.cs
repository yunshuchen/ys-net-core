﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace YS.Net.Mvc.Middlewares
{

    public class RequestResponseLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private RequestResponseLog _logInfo;
        private readonly ILogger<RequestResponseLoggingMiddleware> logger;
        public RequestResponseLoggingMiddleware(RequestDelegate next, ILogger<RequestResponseLoggingMiddleware> _logger)
        {
            _next = next;
            logger = _logger;
        }

        public async Task Invoke(HttpContext context)
        {
            _logInfo = new RequestResponseLog();




            await LogRequest(context, _logInfo);

            await this.LogResponse(context, _logInfo);

            Console.WriteLine(_logInfo.ToString());



        }

        protected virtual async Task LogRequest(HttpContext context, RequestResponseLog _logInfo)
        {
            context.Request.EnableBuffering();

            HttpRequest request = context.Request;
            _logInfo.Url = request.Path.ToString();
            _logInfo.Protocol = context.Request.Protocol;
            _logInfo.Headers = request.Headers.ToDictionary(k => k.Key, v => string.Join(";", v.Value.ToList()));
            _logInfo.Method = request.Method;
            _logInfo.ExcuteStartTime = DateTime.Now;
            using (StreamReader reader = new StreamReader(context.Request.Body, Encoding.UTF8, true, -1, true))
            {
                context.Request.Body.Position = 0L;
                string endAsync = await reader.ReadToEndAsync();
                context.Request.Body.Position = 0L;
                _logInfo.RequestBody = endAsync;
            }
        }

        protected virtual async Task LogResponse(HttpContext context, RequestResponseLog _logInfo)
        {
            _logInfo.Headers = context.Response.Headers.ToDictionary(k => k.Key, v => string.Join(";", v.Value.ToList()));
         
            Stream originalBody = context.Response.Body;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    context.Response.Body = (Stream)stream;
                    await this._next(context);
                    stream.Position = 0L;
                    string body = "";
                    using (StreamReader reader = new StreamReader((Stream)stream, Encoding.UTF8, true, -1, true))
                    {
                        body = await reader.ReadToEndAsync();
                    }
                    stream.Position = 0L;
                    await stream.CopyToAsync(originalBody);
                    string message = "StatusCode:" + context.Response.StatusCode.ToString() + " " + ((HttpStatusCode)context.Response.StatusCode).ToString() + "\r\n" + body;
                    _logInfo.ResponseBody = message;
                    body = (string)null;
                }
            }
            finally
            {
                context.Response.Body = originalBody;
            }
            originalBody = (Stream)null;
        }

    }

    public static class RequestResponseLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestResponseLogging(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestResponseLoggingMiddleware>();

        }
    }
}
