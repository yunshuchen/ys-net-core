
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Notification.ViewModels
{
    public class ResetPasswordViewModel
    {
        public string Link { get; set; }
    }
}
