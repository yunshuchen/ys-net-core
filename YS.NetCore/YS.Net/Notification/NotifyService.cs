
using System;
using System.Collections.Generic;
using System.Text;

using YS.Utils.Notification;
using Microsoft.AspNetCore.Http;
using YS.Net.Notification.ViewModels;
using Microsoft.AspNetCore.DataProtection;
using YS.Utils.Models;
using YS.Net.Models;
using YS.Utils;
using YS.Net.Setting;
using YS.Utils.DI;

namespace YS.Net.Notification
{
    [TransientDI]
    public class NotifyService : INotifyService
    {
        private readonly INotificationManager _notificationManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
      //  private readonly IDataProtectionProvider _dataProtectionProvider;
        private readonly IApplicationSettingService app_setting;
        public NotifyService(
            INotificationManager notificationManager
            , IHttpContextAccessor httpContextAccessor
            //, IDataProtectionProvider dataProtectionProvider
            , IApplicationSettingService _app_setting
            )
        {
            _notificationManager = notificationManager;
            _httpContextAccessor = httpContextAccessor;
          //  _dataProtectionProvider = dataProtectionProvider;
            app_setting = _app_setting;
        }
        public void SendAppointmentNotify(object appointment)
        {
        
            //List<string> to_mail = new List<string>();
            //if (appointment.type_1 == 1)
            //{
            //    string key=app_setting.Get("mail_type1", "");
            //    if (key != "" )
            //    {
            //        var email_list = key.Split(';');
            //        foreach (var item in email_list)
            //        {
            //            if (RegexCheck.IsEmail(key))
            //            {
            //                to_mail.Add(key);
            //            }
            //        }
            //    }
            //}

            //if (appointment.type_2 == 1)
            //{
            //    string key = app_setting.Get("mail_type2", "");
            //    if (key != "")
            //    {
            //        var email_list = key.Split(';');
            //        foreach (var item in email_list)
            //        {
            //            if (RegexCheck.IsEmail(key))
            //            {
            //                to_mail.Add(key);
            //            }
            //        }
            //    }
            //}
            //if (appointment.type_3 == 1)
            //{
            //    string key = app_setting.Get("mail_type3", "");
            //    if (key != "")
            //    {
            //        var email_list = key.Split(';');
            //        foreach (var item in email_list)
            //        {
            //            if (RegexCheck.IsEmail(key))
            //            {
            //                to_mail.Add(key);
            //            }
            //        }
            //    }
            //}
            //if (appointment.type_4 == 1)
            //{
            //    string key = app_setting.Get("mail_type4", "");
            //    if (key != "")
            //    {
            //        var email_list = key.Split(';');
            //        foreach (var item in email_list)
            //        {
            //            if (RegexCheck.IsEmail(key))
            //            {
            //                to_mail.Add(key);
            //            }
            //        }
            //    }
            //}
            //if (appointment.type_5 == 1)
            //{
            //    string key = app_setting.Get("mail_type5", "");
            //    if (key != "")
            //    {
            //        var email_list = key.Split(';');
            //        foreach (var item in email_list)
            //        {
            //            if (RegexCheck.IsEmail(key))
            //            {
            //                to_mail.Add(key);
            //            }
            //        }
            //    }
            //}
            //_notificationManager.Send(new RazorEmailNotice
            //{
            //    Subject = $"预约开户申请提醒--{appointment.date.ToString("yyyy-MM-dd")}",
            //    To = to_mail.ToArray(),
            //    Model = appointment,
            //    TemplatePath = "~/wwwroot/templates/tpl1/notify/appointment.cshtml"
            //});
        }
    }
}
