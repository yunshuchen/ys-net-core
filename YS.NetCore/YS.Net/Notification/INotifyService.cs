using YS.Net.Models;
using YS.Utils.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Notification
{
    public interface INotifyService
    {
        void SendAppointmentNotify(object appointment);
    }
}
