
using YS.Utils.Notification;
using System.Net;
using System.Net.Mail;
using YS.Utils.Extend;
using System;
using YS.Net.Setting;
using YS.Utils.SMTP;
using YS.Utils;
using YS.Utils.DI;

namespace YS.Net.SMTP
{
    [TransientDI]
    public class SmtpProvider : ISmtpProvider
    {

        private readonly IApplicationSettingService _applicationSettingService;
        public SmtpProvider(IApplicationSettingService applicationSettingService)
        {
            _applicationSettingService = applicationSettingService;
        }
        public SmtpClient Get()
        {

            var setting_str = _applicationSettingService.Get("stmp_client","");
            var setting = JsonHelper.ToObject<SmtpSetting>(setting_str);
            if (setting_str==""||setting.Host.IsNullOrWhiteSpace() || setting.Email.IsNullOrWhiteSpace())
            {
                throw new Exception("SMTP Server is not ready");
            }
            SmtpClient client = null;
            if (setting.Port > 0)
            {
                client = new SmtpClient(setting.Host, setting.Port);
            }
            else
            {
                client = new SmtpClient(setting.Host);
            }
            
            client.UseDefaultCredentials = true;
            client.EnableSsl = setting.EnableSsl;
            client.Credentials = new NetworkCredential(setting.Email, setting.PassWord);
            return client;
        }

        public SmtpSetting GetSetting()
        {
            var setting_str = _applicationSettingService.Get("stmp_client", "");
            var setting = JsonHelper.ToObject<SmtpSetting>(setting_str);
            if (setting_str == "" || setting.Host.IsNullOrWhiteSpace() || setting.Email.IsNullOrWhiteSpace())
            {
                throw new Exception("SMTP Server is not ready");
            }

            return setting;
        }
    }
}
