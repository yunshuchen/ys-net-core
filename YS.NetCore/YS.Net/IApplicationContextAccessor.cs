using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net
{
    public interface IApplicationContextAccessor
    {
        CMSApplicationContext Current { get; }
    }
}
