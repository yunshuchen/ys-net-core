﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Web;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace YS.Net
{
    public static class TypeConverters
    {
        public static DateTime CustomMinValue
        {
            get
            {
                return DateTime.Parse("1900-01-01 00:00:00.000");
            }
        }

        public static bool ParerIP(this string ip, string IP1, string IP2)
        {
            List<long> ips = new List<long>() {
            IP2Long(IP1),
              IP2Long(IP2),
            };
            long ipAddress = IP2Long(ip);
            bool inRange = (ipAddress >= ips.Min() && ipAddress <= ips.Max());
            return inRange;

        }

        private static long IP2Long(string ip) {
            string[] ipBytes;
            double num = 0;
            if (!string.IsNullOrEmpty(ip))
            {
                ipBytes = ip.Split('.');
                for (int i = ipBytes.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(ipBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                }
            }
            return (long)num;
        }
        /// <summary>
        /// Clones the specified list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="List">The list.</param>
        /// <returns>List{``0}.</returns>
        public static List<T> Clone<T>(this List<T> List)
        {
            using (Stream objectStream = new MemoryStream())
            {
                IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                formatter.Serialize(objectStream, List);
                objectStream.Seek(0, SeekOrigin.Begin);
                return formatter.Deserialize(objectStream) as List<T>;
            }
        }
        public static string IP(this IHttpContextAccessor httpContextAccessor) {
            //先获取代理设定的IP
            var realIp = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.MyToString();

            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey("X-Real-IP"))
            {
                realIp = string.Concat(httpContextAccessor.HttpContext.Request.Headers["X-Real-IP"], "");
            }

            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                realIp=  string.Concat(httpContextAccessor.HttpContext.Request.Headers["X-Forwarded-For"], "");
 
            }
            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey("HTTP_X_FORWARDED_FOR"))
            {
                realIp = string.Concat(httpContextAccessor.HttpContext.Request.Headers["HTTP_X_FORWARDED_FOR"], "");

            }
            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey("X_REAL_IP"))
            {
                realIp = string.Concat(httpContextAccessor.HttpContext.Request.Headers["X_REAL_IP"], "");

            }
            return realIp;
        }
        public static string ServerName(this IHttpContextAccessor httpContextAccessor)
        {
            if (httpContextAccessor.HttpContext.Request.Headers.ContainsKey("SERVER_NAME"))
            {
                return  string.Concat(httpContextAccessor.HttpContext.Request.Headers["SERVER_NAME"], "");
            }
            return "";
        }
       
        public static bool IsMobileDevice(string UserAgent)
        {

            String[] mobileAgents = { "iphone", "android", "phone", "mobile", "wap", "netfront", "java", "opera mobi", "opera mini", "ucweb", "windows ce", "symbian", "series", "webos", "sony", "blackberry", "dopod", "nokia", "samsung", "palmsource", "xda", "pieplus", "meizu", "midp", "cldc", "motorola", "foma", "docomo", "up.browser", "up.link", "blazer", "helio", "hosin", "huawei", "novarra", "coolpad", "webos", "techfaith", "palmsource", "alcatel", "amoi", "ktouch", "nexian", "ericsson", "philips", "sagem", "wellcom", "bunjalloo", "maui", "smartphone", "iemobile", "spice", "bird", "zte-", "longcos", "pantech", "gionee", "portalmmm", "jig browser", "hiptop", "benq", "haier", "^lct", "320x320", "240x320", "176x220", "w3c ", "acs-", "alav", "alca", "amoi", "audi", "avan", "benq", "bird", "blac", "blaz", "brew", "cell", "cldc", "cmd-", "dang", "doco", "eric", "hipt", "inno", "ipaq", "java", "jigs", "kddi", "keji", "leno", "lg-c", "lg-d", "lg-g", "lge-", "maui", "maxo", "midp", "mits", "mmef", "mobi", "mot-", "moto", "mwbp", "nec-", "newt", "noki", "oper", "palm", "pana", "pant", "phil", "play", "port", "prox", "qwap", "sage", "sams", "sany", "sch-", "sec-", "send", "seri", "sgh-", "shar", "sie-", "siem", "smal", "smar", "sony", "sph-", "symb", "t-mo", "teli", "tim-", "tosh", "tsm-", "upg1", "upsi", "vk-v", "voda", "wap-", "wapa", "wapi", "wapp", "wapr", "webc", "winw", "winw", "xda", "xda-", "Googlebot-Mobile" };
            Boolean isMoblie = false;
            if (UserAgent.ToString().ToLower() != null)
            {
                for (int i = 0; i < mobileAgents.Length; i++)
                {
                    if (UserAgent.ToString().ToLower().IndexOf(mobileAgents[i]) >= 0)
                    {
                        isMoblie = true;
                        break;
                    }
                }
            }
            if (isMoblie)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


       


        public static bool TryUpdate<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict,
  TKey key,
  Func<TValue, TValue> updateFactory)
        {

            if (!dict.ContainsKey(key))
            {
                var NewValue = updateFactory(default(TValue));
                dict.TryAdd(key, NewValue);
                return true;
            }
            TValue curValue;
            while (dict.TryGetValue(key, out curValue))
            {
                if (!dict.ContainsKey(key))
                {
                    dict.TryAdd(key, curValue);
                    return true;
                }
                if (dict.TryUpdate(key, updateFactory(curValue), curValue))
                    return true;
                // if we're looping either the key was removed by another thread,
                // or another thread changed the value, so we start again.
            }
            return false;
        }

//        public static bool TryReMove<TKey, TValue>(this ConcurrentDictionary<TKey, TValue> dict,
//Func<TValue, bool> predicat)
//        {

//            var remove_condtion = dict.Values.Where(predicat).ToList();
            
//            return false;
//        }

        /// <summary>
        /// 获取文件的后缀名
        /// 包含 . 
        /// </summary>
        /// <param name="file_name"></param>
        /// <returns></returns>
        public static string GetFileExt(this string file_name)
        {
            string result = "";
            if (file_name.Trim().Length > 0)
            {
                var index = file_name.LastIndexOf('.');

                result = file_name.Substring(index);
            }
            return result;
        }
        /// <summary>
        /// 获取类中的属性值
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T GetModelValue<T>(this object obj, string FieldName)
        {
            if (null == obj)
            {
                return default(T);
            }
            Type Ts = obj.GetType();

            object o = null;
            try
            {
                o = Ts.GetProperty(FieldName).GetValue(obj, null);
                
            }
            catch (Exception)
            {
            }
            return (T)o;

        }

        /// <summary>
        /// 设置类中的属性值
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="Value"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool SetModelValue<T>(this object obj, string FieldName, T Value)
        {
            try
            {
                Type Ts = obj.GetType();
                object v = Convert.ChangeType(Value, Ts.GetProperty(FieldName).PropertyType);
                Ts.GetProperty(FieldName).SetValue(obj, v, null);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string GetObjValue(this PropertyInfo property, object obj)
        {
            return string.Concat(property.GetValue(obj, null), "");
        }
        /// <summary>
        /// 安全转换为string类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string MyToString(this Object obj)
        {
            return string.Concat(obj, "");
        }

        /// <summary>
        /// 安全转换为long类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static long MyToLong(this Object obj)
        {
            return MyToLong(obj,0);
        }     /// <summary>
              /// 安全转换为long类型
              /// </summary>
              /// <param name="obj"></param>
              /// <returns></returns>
        public static long MyToLong(this Object obj, long default_value = 0)
        {
            try
            {
                return Convert.ToInt64(string.Concat(obj, ""));
            }
            catch (Exception)
            {

                return default_value;
            }
        }
        /// <summary>
        /// 安全转换为int类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int MyToInt(this Object obj)
        {
            return MyToInt(obj, 0);
        }
        /// <summary>
        /// 安全转换为int类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int MyToInt(this Object obj,int default_value=0)
        {
            try
            {
                return Convert.ToInt32(string.Concat(obj, ""));
            }
            catch (Exception)
            {

                return default_value;
            }
        }
        /// <summary>
        /// Int 补位格式化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string MyToStringF(this int obj)
        {
            try
            {
                return obj <= 9 ? "0" + obj : obj.ToString();
            }
            catch (Exception)
            {

                return "";
            }
        }

        /// <summary>
        /// 安全转换为double类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static double MyToDouble(this Object obj)
        {
            try
            {
                return Convert.ToDouble(string.Concat(obj, ""));
            }
            catch (Exception)
            {

                return 0;
            }
        }

        /// <summary>
        /// 安全转换为double类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool MyToBool(this Object obj)
        {
            try
            {
                return Convert.ToBoolean(string.Concat(obj, ""));
            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// 安全转换为double类型
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static decimal MyToDecimal(this Object obj)
        {
            try
            {
                return Convert.ToDecimal(string.Concat(obj, ""));
            }
            catch (Exception)
            {

                return 0;
            }
        }
        /// <summary>
        /// 时间格式化yyyy-MM-dd
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string MyToDate(this DateTime obj, string format = "yyyy-MM-dd")
        {
            return obj.ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);

        }
        /// <summary>
        /// DateTime 时间格式化
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string MyToDateTime(this DateTime obj, string format = "yyyy-MM-dd HH:mm:ss")
        {
            return obj.ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);
        }

        /// <summary>
        /// string 时间格式化
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string MyToDateTimeF1(this string obj, string format = "yyyy-MM-dd HH:mm:ss")
        {


            return DateTime.Parse(obj).ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);

        }

        /// <summary>
        /// string 时间格式化
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string MyToDateTimeF1(this string obj, string format = "yyyy-MM-dd HH:mm:ss", string defaultvalue="")
        {
            DateTime conv = DateTime.MinValue;

            DateTime.TryParse(obj, out conv);

            if (conv != DateTime.MinValue)
            {
             return   conv.ToString(format, System.Globalization.DateTimeFormatInfo.InvariantInfo);
            }
            return defaultvalue;

        }
    }
}
