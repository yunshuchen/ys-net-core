using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class sys_resource_entity_mapping : NopEntityTypeConfiguration<sys_resource_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_resource_entity> builder)
        {
            
            builder.ToTable("sys_resource");
           // builder.Property(e => e.created_time).HasColumnType("datetime").HasColumnName("created_time");
            builder.HasKey(sysFun => sysFun.id);
            base.Configure(builder);
        }
    }
}

