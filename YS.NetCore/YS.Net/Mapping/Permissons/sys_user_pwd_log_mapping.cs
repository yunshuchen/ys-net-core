﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    /// <summary>
    /// 用户修改密码的日志记录
    /// </summary>
    public class sys_user_pwd_log_mapping : NopEntityTypeConfiguration<sys_user_pwd_log>
    {

        public override void Configure(EntityTypeBuilder<sys_user_pwd_log> builder)
        {

            builder.ToTable("sys_user_pwd_log");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
