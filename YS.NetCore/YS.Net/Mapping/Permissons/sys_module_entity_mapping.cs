using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{
   
    public class sys_module_entity_mapping : NopEntityTypeConfiguration<sys_module_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_module_entity> builder)
        {

            builder.ToTable("sys_module");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
