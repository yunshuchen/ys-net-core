using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{

    public class sys_operate_entity_mapping : NopEntityTypeConfiguration<sys_operate_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_operate_entity> builder)
        {

            builder.ToTable("sys_operate");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
