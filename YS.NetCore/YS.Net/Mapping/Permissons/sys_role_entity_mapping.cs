using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{

    public class sys_role_entity_mapping : NopEntityTypeConfiguration<sys_role_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_role_entity> builder)
        {
            // builder.ToTable(nameof(site_member));
            builder.ToTable("sys_role");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }

    }
}
