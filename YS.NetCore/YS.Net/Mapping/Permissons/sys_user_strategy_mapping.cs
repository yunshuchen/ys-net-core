﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    /// <summary>
    /// 用户策略关系表
    /// </summary>
    public class sys_user_strategy_mapping : NopEntityTypeConfiguration<sys_user_strategy>
    {

        public override void Configure(EntityTypeBuilder<sys_user_strategy> builder)
        {

            builder.ToTable("sys_user_strategy");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
