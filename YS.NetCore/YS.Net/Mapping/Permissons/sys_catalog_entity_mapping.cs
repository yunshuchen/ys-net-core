﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{

    public class sys_catalog_entity_mapping : NopEntityTypeConfiguration<sys_catalog_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_catalog_entity> builder)
        {

            builder.ToTable("sys_catalog");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
