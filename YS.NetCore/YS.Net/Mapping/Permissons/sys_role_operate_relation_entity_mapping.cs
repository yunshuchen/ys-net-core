using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{
 
    public class sys_role_operate_relation_entity_mapping : NopEntityTypeConfiguration<sys_role_operate_relation_entity>
    {
        public override void Configure(EntityTypeBuilder<sys_role_operate_relation_entity> builder)
        {

            builder.ToTable("sys_role_operate_relation");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }

    }
}
