﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{
    /// <summary>
    /// 安全策略表
    /// </summary>
    public class sys_strategy_mapping : NopEntityTypeConfiguration<sys_strategy>
    {
        public override void Configure(EntityTypeBuilder<sys_strategy> builder)
        {

            builder.ToTable("sys_strategy");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
