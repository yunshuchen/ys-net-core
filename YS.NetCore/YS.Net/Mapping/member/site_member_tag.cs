﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{
    public class site_member_tag_mapping : NopEntityTypeConfiguration<site_member_tag>
    {

        public override void Configure(EntityTypeBuilder<site_member_tag> builder)
        {
            builder.ToTable(nameof(site_member_tag));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
    
}
