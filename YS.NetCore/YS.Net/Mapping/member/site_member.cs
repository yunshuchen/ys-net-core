﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{
    public class site_member_mapping : NopEntityTypeConfiguration<site_member>
    {

        public override void Configure(EntityTypeBuilder<site_member> builder)
        {
            builder.ToTable(nameof(site_member));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
  
}
