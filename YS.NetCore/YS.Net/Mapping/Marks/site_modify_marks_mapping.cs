﻿
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using YS.Net.Models;

namespace YS.Net.Mapping
{
    public class site_modify_marks_mapping : NopEntityTypeConfiguration<site_modify_marks>
    {

        public override void Configure(EntityTypeBuilder<site_modify_marks> builder)
        {
            builder.ToTable(nameof(site_modify_marks));
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }

}
