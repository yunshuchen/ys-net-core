﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    public class site_node_marks_mapping : NopEntityTypeConfiguration<site_node_marks>
    {

        public override void Configure(EntityTypeBuilder<site_node_marks> builder)
        {
            builder.ToTable(nameof(site_node_marks));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}