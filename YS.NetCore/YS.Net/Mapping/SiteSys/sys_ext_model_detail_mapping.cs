﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    public class sys_ext_model_detail_mapping : NopEntityTypeConfiguration<sys_ext_model_detail>
    {

        public override void Configure(EntityTypeBuilder<sys_ext_model_detail> builder)
        {
            builder.ToTable(nameof(sys_ext_model_detail));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}