﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    public class sys_ext_model_mapping : NopEntityTypeConfiguration<sys_ext_model>
    {

        public override void Configure(EntityTypeBuilder<sys_ext_model> builder)
        {
            builder.ToTable(nameof(sys_ext_model));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}