using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Mapping
{
    public class sys_account_mapping : NopEntityTypeConfiguration<sys_account>
    {

        public override void Configure(EntityTypeBuilder<sys_account> builder)
        {
            builder.ToTable(nameof(sys_account));
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}