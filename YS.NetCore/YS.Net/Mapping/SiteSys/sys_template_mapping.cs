﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using YS.Net.Setting;

namespace YS.Net.Mapping
{
    public class sys_template_mapping : NopEntityTypeConfiguration<sys_template>
    {

        public override void Configure(EntityTypeBuilder<sys_template> builder)
        {
            builder.ToTable("sys_templates");
            //builder.ToTable("sys_logs");
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}
