using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using YS.Net.Setting;

namespace YS.Net.Mapping
{
    public class site_application_setting_mapping : NopEntityTypeConfiguration<ApplicationSetting>
    {

        public override void Configure(EntityTypeBuilder<ApplicationSetting> builder)
        {

            builder.ToTable("site_application_setting");
            builder.HasKey(sysFun => sysFun.SettingKey);


            base.Configure(builder);
        }
    }
}