﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_basedata_type_mapping : NopEntityTypeConfiguration<site_basedata_type>
    {
        public override void Configure(EntityTypeBuilder<site_basedata_type> builder)
        {

            builder.ToTable(nameof(site_basedata_type));
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}

