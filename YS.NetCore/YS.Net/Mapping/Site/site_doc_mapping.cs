﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_doc_mapping : NopEntityTypeConfiguration<site_doc>
    {
        public override void Configure(EntityTypeBuilder<site_doc> builder)
        {

            builder.ToTable(nameof(site_doc));
            builder.HasKey(sysFun => new { sysFun.id, sysFun.lang });


            base.Configure(builder);
        }
    }
}

