﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_notice_mapping : NopEntityTypeConfiguration<site_notice>
    {
        public override void Configure(EntityTypeBuilder<site_notice> builder)
        {

            builder.ToTable(nameof(site_notice));
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}

