﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_doc_field_set_mapping : NopEntityTypeConfiguration<site_doc_field_set>
    {
        public override void Configure(EntityTypeBuilder<site_doc_field_set> builder)
        {

            builder.ToTable(nameof(site_doc_field_set));
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}