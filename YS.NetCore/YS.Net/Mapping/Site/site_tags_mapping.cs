﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_tags_mapping : NopEntityTypeConfiguration<site_tags>
    {
        public override void Configure(EntityTypeBuilder<site_tags> builder)
        {

            builder.ToTable(nameof(site_tags));
            builder.HasKey(sysFun => new { sysFun.id, sysFun.lang });


            base.Configure(builder);
        }
    }
}



