﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_basedata_mapping : NopEntityTypeConfiguration<site_basedata>
    {
        public override void Configure(EntityTypeBuilder<site_basedata> builder)
        {

            builder.ToTable(nameof(site_basedata));
            builder.HasKey(sysFun => new { sysFun.id, sysFun.lang });


            base.Configure(builder);
        }
    }
}

