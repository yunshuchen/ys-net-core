﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class sys_tableid_mapping : NopEntityTypeConfiguration<sys_tableid>
    {
        public override void Configure(EntityTypeBuilder<sys_tableid> builder)
        {

            builder.ToTable(nameof(sys_tableid));
            builder.HasKey(sysFun => sysFun.id);


            base.Configure(builder);
        }
    }
}


