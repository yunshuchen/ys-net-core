﻿using YS.Net.Models;
using YS.Utils.RepositoryConfiguration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
namespace YS.Net.Mapping
{

    public class site_content_mapping : NopEntityTypeConfiguration<site_content>
    {
        public override void Configure(EntityTypeBuilder<site_content> builder)
        {

            builder.ToTable(nameof(site_content));
            builder.HasKey(sysFun => new { sysFun.id, sysFun.lang });


            base.Configure(builder);
        }
    }
}
