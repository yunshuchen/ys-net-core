﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Net
{

    public class CacheKey
    {
      
        /// <summary>
        /// 换取登录的信息的Code
        /// </summary>
        public const string Code2LoginCackeKey = "Cache:Code2Login:Code:{0}";

        /// <summary>
        /// 登录验证码的缓存Key
        /// </summary>
        public const string LoginValidateCodeCackeKey = "Cache:Login:ValidateCode:Key:{0}";

        /// <summary>
        /// 用户登录缓存
        /// </summary>
        public const string UserLoginInfoDelCackeKey = "Cache:User:Login:Info:Key:{0}:";
        /// <summary>
        /// 用户登录缓存{id}{token}
        /// </summary>
        public const string UserLoginInfoCackeKey = "Cache:User:Login:Info:Key:{0}:{1}";



        /// <summary>
        /// 忘记密码找回界面的验证码
        /// </summary>
        public const string ForgetPasswordValidateCodeCackeKey = "Cache:ForgetPassword:ValidateCode:Key:{0}";


        /// <summary>
        /// 忘记密码重置密码用到的Key
        /// </summary>
        public const string ForgetAndResetPasswordCackeKey = "Cache:ForgetPassword:Reset:Key:{0}";




      
    }
}
