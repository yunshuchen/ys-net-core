﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net
{
    public class WeChatConstants
    {

        /// <summary>
        /// 微信自动登录前后端分离使用的缓存Key{标识}
        /// 
        /// </summary>
        public const string WeChatMPAutoLoginKey = "wechat:mp:autologin:{0}";

    }
}
