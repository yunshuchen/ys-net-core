using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Options
{
    public class AllowFileOptions
    {
        /// <summary>
        /// 上传图片格式显示
        /// </summary>
        public List<string> ImageAllowFiles { get; set; }
        /// <summary>
        /// 上传文件允许的格式
        /// </summary>
        public List<string> FileAllowFiles { get; set; }
        /// <summary>
        /// 允许在线编辑的文件
        /// </summary>
        public List<string> OnlineAllowEditFile { get; set; }

        /// <summary>
        /// 允许多媒体上传文件
        /// </summary>
        public List<string> MediaAllowFiles { get; set; }
    }
}
