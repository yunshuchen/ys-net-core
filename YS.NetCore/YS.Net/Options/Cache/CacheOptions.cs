using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Options
{
    public class CacheOptions
    {
        public string CacheType { get; set; }
        public string CacheConnectionString { get; set; }
        public int CacheDefaultTimeOut { get; set; }
    }
}
