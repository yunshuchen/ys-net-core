using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Options
{
    public class SurveySettingOption
    {
        public string Scope { get; set; }

        public string OAuthPage { get; set; }
        public string OAuthBack { get; set; }
        public string SuccessRedirect { get; set; }

        public string WxTemplateMsgIds { get; set; }

        public string WxReviewMsgId { get; set; }
    }
}
