﻿using YS.Utils.Mvc.Route;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net
{
    public class RouteProvider : IRouteProvider
    {
        public IEnumerable<RouteDescriptor> GetRoutes()
        {
            foreach (var item in RouteDescriptors.Routes)
            {
                yield return item;
            }
        }
    }
}
