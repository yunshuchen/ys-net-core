﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YS.Net.JsonLocalizer
{
    public class RouteValueRequestCultureProvider : RequestCultureProvider
    {

        public int IndexOfCulture;
        public int IndexofUICulture;

        public override Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {

            if (httpContext == null)
                throw new ArgumentNullException(nameof(httpContext));

            string uiCulture = null;
            var admin_lang = "";
            httpContext.Request.Cookies.TryGetValue("admin_lang", out admin_lang);
            string culture = "";
            culture=admin_lang == "" ? this.GetDefaultCultureCode() : admin_lang;

     
            var twoLetterCultureName = httpContext.Request.Path.Value.Split('/').Where(a => a.Trim() != "").FirstOrDefault();
            var twoLetterUICultureName = httpContext.Request.Path.Value.Split('/').Where(a => a.Trim() != "").FirstOrDefault();

            if (twoLetterCultureName == "en")
                culture = uiCulture = "en";
            if (twoLetterUICultureName == "tc")
                culture = uiCulture = "tc";
            else if (twoLetterUICultureName == "sc")
                culture = uiCulture = "sc";


            if (culture == "tc")
            {
                culture = "zh-HK";
                uiCulture = "zh-HK";
            }
            else if (culture == "en")
            {
                culture = "en-US";
                uiCulture = "en-US";
            }
            else {
                culture = "zh-CN";
                uiCulture = "zh-CN";
            }
            var providerResultCulture = new ProviderCultureResult(culture, uiCulture);

            return Task.FromResult(providerResultCulture);
        }
        private string GetDefaultCultureCode()
        {
            return this.Options.DefaultRequestCulture.Culture.TwoLetterISOLanguageName;
        }

        private bool CheckCultureCode(string cultureCode)
        {
            return this.Options.SupportedCultures.Select(c => c.TwoLetterISOLanguageName).Contains(cultureCode);
        }
    }
}
