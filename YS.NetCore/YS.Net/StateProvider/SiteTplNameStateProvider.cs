﻿using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc.StateProviders;
using System;
using System.Collections.Generic;
using System.Text;


namespace YS.Net.StateProvider
{
    public class SiteTplNameStateProvider : IApplicationContextStateProvider
    {
        private readonly IApplicationSettingService _applicationSettingService;
        public SiteTplNameStateProvider(IApplicationSettingService applicationSettingService)
        {
            _applicationSettingService = applicationSettingService;
        }
        public string Name => "SiteTplName";

        public Func<IApplicationContext, T> Get<T>()
        {
            string result = _applicationSettingService.Get("TplCode", "tpl1");
            return (context) =>
            {
                return (T)(object)result;
            };
        }
    }
}
