﻿using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc.StateProviders;
using System;
using System.Collections.Generic;
using System.Text;


namespace YS.Net.StateProvider
{
    public class ShowDataLangStateProvider : IApplicationContextStateProvider
    {
        private readonly IApplicationSettingService _applicationSettingService;
        public ShowDataLangStateProvider(IApplicationSettingService applicationSettingService)
        {
            _applicationSettingService = applicationSettingService;
        }
        public string Name => "Show_Data_Lang";

        public Func<IApplicationContext, T> Get<T>()
        {
            string result = _applicationSettingService.Get(Name, "tc");
            return (context) =>
            {
                return (T)(object)result;
            };
        }
    }
}
