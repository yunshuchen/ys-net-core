﻿using YS.Net.Setting;
using YS.Utils;
using YS.Utils.Mvc.StateProviders;
using System;
using System.Collections.Generic;
using System.Text;


namespace YS.Net.StateProvider
{
    public class OuterChainPictureStateProvider : IApplicationContextStateProvider
    {
        private readonly IApplicationSettingService _applicationSettingService;
        public OuterChainPictureStateProvider(IApplicationSettingService applicationSettingService)
        {
            _applicationSettingService = applicationSettingService;
        }
        public string Name => "OuterChainPicture";

        public Func<IApplicationContext, T> Get<T>()
        {
            bool result = _applicationSettingService.Get(Name, "false") == "true";
            return (context) =>
            {
                return (T)(object)result;
            };
        }
    }
}
