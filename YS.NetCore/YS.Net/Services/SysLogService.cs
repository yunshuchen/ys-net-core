﻿//using YS.Net.Models;
//using YS.Utils.RepositoryPattern;
//using Microsoft.EntityFrameworkCore;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using YS.Utils.DI;
//using YS.Utils.Repositorys;

//namespace YS.Net.Services
//{
//    [ScopedDI]
//    public class SysLogService :  ServiceBase<sys_logs, Microsoft.Extensions.Logging.ILogger<sys_logs>>, ISysLogService
//    {

//        public SysLogService(IRepository<sys_logs> _useService, Microsoft.Extensions.Logging.ILogger<sys_logs> _log)
//            : base(_useService, _log)
//        {

//        }

//        public object TestJoin()
//        {

//            return Table.Join(base.OtherTable<sys_resource_entity>(), log => log.log_module, res => res.res_code, (log, res) => new
//            {
//                log.id,
//                log.log_module,
//                res.res_name
//            }).ToList();
//        }

//        public object TestJoin1()
//        {

//var query = from b in context.Set<Blog>()
//            join p in context.Set<Post>()
//                on b.BlogId equals p.BlogId into grouping
//            from p in grouping.DefaultIfEmpty()
//            select new { b, p };
//            var result = from l in Table
//                         join r in OtherTable<sys_resource_entity>()
//                         on l.log_module equals r.res_code
//                         select new
//                         {
//                             l.id,
//                             l.log_module,
//                             r.res_name
//                         };

//            return result.ToList();
//        }

//        public object TestLeftJoin()
//        {
//            //log LEFT JOIN res
//            var joinResults = base.Table
//                            .GroupJoin(base.OtherTable<sys_resource_entity>(), log => log.log_module, res => res.res_code, (log, res) => new { log, res })
//                            .SelectMany(combin => combin.res.DefaultIfEmpty(), (log, res) => new
//                            {
//                                log.log.id,
//                                log.log.log_module,
//                                res.res_name
//                            }).ToList();
//            return joinResults;
//        }
//    }
//}
