﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using System.ComponentModel.DataAnnotations.Schema;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysTableidService : ServiceBase<sys_tableid, Microsoft.Extensions.Logging.ILogger<sys_tableid>>, ISysTableidService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysTableidService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
           IRepository<sys_tableid> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_tableid> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }

        public long getNewTableId<T>() where T : class
        {
            long result = -1;

            var table_name = "";


            var item = typeof(T).GetCustomAttributes(typeof(TableAttribute), false).FirstOrDefault();

            if (item != null)
            {
                table_name = (item as TableAttribute).Name;
            }

            if (table_name == "")
            {
                throw new Exception("请为实体类添加TableAttribute标记");
            }
            long max_id = 1;
            lock (LockProvider.LookUp(table_name))
            {
                var table_id = GetByOne(a => a.table_name.ToLower() == table_name.ToLower());
                if (table_id == null)
                {
                    base.Insert(new sys_tableid()
                    {
                        max_id = 1,
                        table_name = table_name.ToLower()
                    });
                }
                else
                {
                    max_id = table_id.max_id + 1;
                    Modify(a => a.id == table_id.id, a => new sys_tableid()
                    {
                        max_id = max_id
                    });
                }
            }
            result = max_id;

            return result;


        }
    }
}
