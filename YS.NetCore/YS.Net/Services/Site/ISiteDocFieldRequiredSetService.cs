﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteDocFieldRequiredSetService : IService<site_doc_field_set_required>
    {

    }
}
