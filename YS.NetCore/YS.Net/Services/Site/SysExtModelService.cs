﻿

using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysExtModelService : ServiceBase<sys_ext_model, Microsoft.Extensions.Logging.ILogger<sys_ext_model>>, ISysExtModelService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysExtModelService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor
            , IRepository<sys_ext_model> _useService
            ,Microsoft.Extensions.Logging.ILogger<sys_ext_model> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
