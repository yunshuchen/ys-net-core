﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Net.Mvc.Authorize;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteOtherService : ServiceBase<site_other, Microsoft.Extensions.Logging.ILogger<site_other>>, ISiteOtherService
    {

        private const string SysCmsNodes = "SysMuliteSiteOtherCache";

        private readonly ConcurrentDictionary<string, site_other> _NodeCache;
        IHttpContextAccessor httpContextAccessor;
        public SiteOtherService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor, IRepository<site_other> _useService,
            ISkyCacheManager<ConcurrentDictionary<string, site_other>> cacheManager,
            Microsoft.Extensions.Logging.ILogger<site_other> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;

            _NodeCache = cacheManager.GetOrAdd(SysCmsNodes, new ConcurrentDictionary<string, site_other>());
        }
        public override int Modify(Expression<Func<site_other, bool>> condition, System.Linq.Expressions.Expression<Func<site_other, site_other>> content)
        {
            int falg = base.Modify(condition, content);
            ReloadCache();
            return falg;
        }
        public override site_other Insert(site_other item)
        {
            var falg = base.Insert(item);
            ReloadCache();
            return falg;
        }
        public override int Delete(Expression<Func<site_other, bool>> predicat)
        {

            int flag = base.Delete(predicat);
            if (flag > 0)
            {
                ReloadCache();
            }
            return flag;
        }

        public List<site_other> GetList()
        {
            List<site_other> list = _NodeCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = base.GetList("row_key asc", new List<Expression<Func<site_other, bool>>>());
                                list.ForEach(a =>
                {
                    _NodeCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_other value11) => { return a; });
                });
            }

            return list;
        }

        public override site_other GetByOne(string OrderBy, Expression<Func<site_other, bool>> predicate)
        {
            List<site_other> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            site_other result = new site_other();
            IEnumerable<site_other> condition = list;
            if (predicate != null)
            {

                condition = condition.Where(predicate.Compile());

            }
            if (OrderBy != null && OrderBy != "")
            {
                result = condition.MyOrderBy(OrderBy).FirstOrDefault();
            }
            else
            {
                result = condition.FirstOrDefault();
            }
            return result;
        }


        public override PageOf<site_other> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<site_other, bool>>> predicate)
        {
            List<site_other> list = _NodeCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = GetList();
            }


            List<site_other> result = new List<site_other>();
            IEnumerable<site_other> condition = list;
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }

            }
            result = condition.OrderBy(a => a.row_key).ToList();
            var page_list = result.Skip((iPage - 1) * PageSize).Take(PageSize).ToList();
            return new PageOf<site_other>()
            {
                list = page_list,
                total = result.Count,
                page_index = iPage,
                page_size = PageSize

            };
        }


        public override site_other GetByOne(Expression<Func<site_other, bool>> predicate)
        {
            List<site_other> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            site_other result = new site_other();
            IEnumerable<site_other> condition = list;
            if (predicate != null)
            {

                condition = condition.Where(predicate.Compile());

            }
            result = condition.FirstOrDefault();
            return result;
        }


        public  site_other GetSiteByOne(Expression<Func<site_other, bool>> predicate)
        {
            List<site_other> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            site_other result = new site_other();
            IEnumerable<site_other> condition = list;
            if (predicate != null)
            {

                condition = condition.Where(predicate.Compile());

            }
            DateTime now = DateTime.Now;
            condition = condition.Where(a=>
            (a.off_status == StatusEnum.Legal || (a.off_status == StatusEnum.UnLegal && now>=DateTime.Parse(a.begin_time) && now <= DateTime.Parse(a.end_time)))
            );
            result = condition.FirstOrDefault();
            return result;
        }
        private void ReloadCache()
        {
            _NodeCache.Clear();
            var list = base.GetList("row_key asc", new List<Expression<Func<site_other, bool>>>());
            list.ForEach(a =>
            {
                _NodeCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_other value11) => { return a; });
            });

        }

        public string GetContentByCode(string code, string lang)
        {
            string now = DateTime.Now.MyToDateTime();
            string result = "";
            Expression<Func<site_other, bool>> expression = a => a.chr_code.ToLower() == code.ToLower() && a.lang == lang;

            if (lang == "sc")
            {
                expression = a => a.chr_code.ToLower() == code.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) ;
            }
            else
            {
                expression = a => a.chr_code.ToLower() == code.ToLower() && a.lang == lang ;
            }

            var result1 = GetSiteByOne(expression)?.chr_content;
            return result1 ?? result.Trim();

        }


        public string GetContentByCode(string code, string lang, StatusEnum is_show)
        {
            string now = DateTime.Now.MyToDateTime();
            string result = "";
            Expression<Func<site_other, bool>> expression = a => a.chr_code.ToLower() == code.ToLower() && a.lang == lang&&a.is_show== is_show;

            if (lang == "sc")
            {
                expression = a => a.chr_code.ToLower() == code.ToLower() && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2) && a.is_show == is_show) ;
            }

            var result1 = GetSiteByOne(expression)?.chr_content;
            return result1 ?? result.Trim();
        }
    }
}