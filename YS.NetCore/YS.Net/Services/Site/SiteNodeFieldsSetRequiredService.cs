﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteNodeFieldRequiredSetService : ServiceBase<site_node_field_set_required, Microsoft.Extensions.Logging.ILogger<site_node_field_set_required>>, ISiteNodeFieldRequiredSetService
    {

        IHttpContextAccessor httpContextAccessor;
        public SiteNodeFieldRequiredSetService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor, IRepository<site_node_field_set_required> _useService,
            Microsoft.Extensions.Logging.ILogger<site_node_field_set_required> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
