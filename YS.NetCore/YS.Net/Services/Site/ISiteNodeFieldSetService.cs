﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteNodeFieldSetService : IService<site_node_field_set>
    {

    }
}
