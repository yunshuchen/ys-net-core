﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteDocService : ServiceBase<site_doc, Microsoft.Extensions.Logging.ILogger<site_doc>>, ISiteDocService
    {
        private readonly ConcurrentDictionary<string, site_doc> _ConetntCache;
        private const string SysCmsNodes = "SysMuliteCmsDocCache";
        IHttpContextAccessor httpContextAccessor;
        public SiteDocService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor, IRepository<site_doc> _useService,
            ISkyCacheManager<ConcurrentDictionary<string, site_doc>> cacheManager,
            Microsoft.Extensions.Logging.ILogger<site_doc> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
            _ConetntCache = cacheManager.GetOrAdd(SysCmsNodes, new ConcurrentDictionary<string, site_doc>());
        }



        public override site_doc Insert(site_doc item)
        {
            var result = base.Insert(item);
            ReloadCache();
            return result;
        }
        /// <summary>
        /// 系统配置添加
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override int Delete(Expression<Func<site_doc, bool>> predicat)
        {

            int flag = base.Delete(predicat);
            if (flag > 0)
            {
                ReloadCache();
            }
            return flag;
        }


        public override site_doc GetByOne(Expression<Func<site_doc, bool>> predicate)
        {
            List<site_doc> list = _ConetntCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            site_doc result = new site_doc();
            IEnumerable<site_doc> condition = list;
            if (predicate != null)
            {
                condition = condition.Where(predicate.Compile());
            }
            result = condition.FirstOrDefault();
            return result;
        }
        public override int Count(Expression<Func<site_doc, bool>> predicate)
        {
            List<site_doc> list = _ConetntCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            return list.Count(predicate.Compile());
        }

        public override void BulkInsert(IEnumerable<site_doc> model)
        {
            base.BulkInsert(model);
            ReloadCache();
        }

        public List<site_doc> GetList()
        {

            List<site_doc> list = null;
            if (list == null)
            {
                list = base.GetList("id asc", new List<Expression<Func<site_doc, bool>>>());

            }

            list.ForEach(a =>
            {
                _ConetntCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_doc value11) => { return a; });
            });
            return list;
        }
        private void ReloadCache()
        {
            _ConetntCache.Clear();
            var list = base.GetList("id asc", new List<Expression<Func<site_doc, bool>>>());
            list.ForEach(a =>
            {
                _ConetntCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_doc value11) => { return a; });
            });
        }

        public override site_doc GetByOne(string OrderBy, Expression<Func<site_doc, bool>> predicate)
        {
            List<site_doc> list = _ConetntCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            site_doc result = new site_doc();
            IEnumerable<site_doc> condition = list;
            if (predicate != null)
            {
                condition = condition.Where(predicate.Compile());
            }
            if (OrderBy != null && OrderBy != "")
            {
                result = condition.MyOrderBy(OrderBy).FirstOrDefault();
            }
            else
            {
                result = condition.FirstOrDefault();
            }
            return result;
        }
        public override List<site_doc> GetList(string keySelector, List<Expression<Func<site_doc, bool>>> predicate)
        {
            List<site_doc> list = _ConetntCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            IEnumerable<site_doc> condition = list;
            List<site_doc> result = new List<site_doc>();
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }
            }
            if (keySelector != null && keySelector != "")
            {
                result = condition.MyOrderBy(keySelector).ToList();
            }
            else
            {
                result = condition.ToList();

            }
            return result;
        }
        public override PageOf<site_doc> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<site_doc, bool>>> predicate)
        {
            List<site_doc> list = _ConetntCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = GetList();
            }


            List<site_doc> result = new List<site_doc>();
            IEnumerable<site_doc> condition = list;
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }

            }
            if (keySelector == "")
            {
                result = condition.OrderByDescending(a => a.id).ThenBy(a => a.n_sort).ToList();
            }
            else
            {
                result = condition.MyOrderBy(keySelector).ToList();
            }
            var page_list = result.Skip((iPage - 1) * PageSize).Take(PageSize).ToList();
            return new PageOf<site_doc>()
            {
                list = page_list,
                total = result.Count,
                page_index = iPage,
                page_size = PageSize

            };
        }
        public override int Modify(Expression<Func<site_doc, bool>> condition, System.Linq.Expressions.Expression<Func<site_doc, site_doc>> content)
        {
            int falg = base.Modify(condition, content);
            ReloadCache();
            return falg;
        }
    }
}
