﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteNodeService : IService<site_node>
    {

        site_node Get(string OrderBy, Expression<Func<site_node, bool>> predicate);

        
        site_node Add(site_node item);

        List<site_node> GetList();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        System.Threading.Tasks.Task<List<SiteNodesTree>> GetTreeDataAsync(string type, List<Expression<Func<site_node, bool>>> conditions,string lang="tc");


        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        System.Threading.Tasks.Task<List<SiteNodesTree>> GetSiteTreeDataAsync(string type, List<Expression<Func<site_node, bool>>> conditions, string lang = "tc");
        List<SiteNodesTree> GetAllLevelData(List<Expression<Func<site_node, bool>>> predicat);


        List<SiteWebNodesTree> GetPathChildNodes(string node_code,string lang);
        List<SiteWebNodesTree> GetPathChildNodes(string node_code, string lang, List<Expression<Func<site_node, bool>>> predicat);
        /// <summary>
        /// 获取自己或上级允许索引得栏目
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        site_node GetUpLeaveLuceneNode(string lang,string node_code);

        /// <summary>
        /// 获取所有父级栏目
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="node_code"></param>
        /// <returns></returns>
        ParentSiteNode GetAllLevelParentNode(string lang, string node_code);


    }
}
