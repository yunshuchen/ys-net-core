﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISysTemplateService : IService<sys_template>
    {
        sys_template Get(string OrderBy, Expression<Func<sys_template, bool>> predicate);
       
    }
}
