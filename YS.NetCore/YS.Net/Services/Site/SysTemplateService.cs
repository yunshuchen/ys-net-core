﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.Encrypt;
using YS.Utils.Mvc.Authorize;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysTemplateService : ServiceBase<sys_template, Microsoft.Extensions.Logging.ILogger<sys_template>>, ISysTemplateService
    {
        IHttpContextAccessor httpContextAccessor;
        public SysTemplateService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            IRepository<sys_template> _useService,
             Microsoft.Extensions.Logging.ILogger<sys_template> _logger) : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;

        }


        /// <summary>
        /// 根据Key获取系统配置
        /// </summary>
        /// <param name="settingKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public sys_template Get(string OrderBy, Expression<Func<sys_template, bool>> predicate)
        {

            if (OrderBy.MyToString() == "")
            {
                return base.GetByOne(predicate);
            }
            else
            {
                return base.GetByOne(OrderBy, predicate);
            }
        }
    }

       
        
       
    
}
