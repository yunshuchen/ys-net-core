﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Net.Mvc.Authorize;
using System.Dynamic;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteNodeService : ServiceBase<site_node, Microsoft.Extensions.Logging.ILogger<site_node>>, ISiteNodeService
    {
        private readonly ConcurrentDictionary<string, site_node> _NodeCache;

        private const string SysCmsNodes = "SysMuliteCmsNodes";
        IHttpContextAccessor httpContextAccessor;
        IDynamicPermissionCheck PermissionCheck;
        public SiteNodeService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            ISkyCacheManager<ConcurrentDictionary<string, site_node>> cacheManager,
            IDynamicPermissionCheck _PermissionCheck, IRepository<site_node> _useService,
            Microsoft.Extensions.Logging.ILogger<site_node> _logger)
            : base(_useService, _logger)
        {
            PermissionCheck = _PermissionCheck;
            _NodeCache = cacheManager.GetOrAdd(SysCmsNodes, new ConcurrentDictionary<string, site_node>());
            httpContextAccessor = _httpContextAccessor;
        }


        public override int Modify(Expression<Func<site_node, bool>> condition, System.Linq.Expressions.Expression<Func<site_node, site_node>> content)
        {
            int falg = base.Modify(condition, content);
            ReloadCache();
            return falg;
        }
        /// <summary>
        /// 系统配置添加
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public site_node Add(site_node item)
        {

            var result = base.Insert(item);
            ReloadCache();
            return result;
        }


        public override site_node Insert(site_node item)
        {
            var result = base.Insert(item);
            ReloadCache();
            return result;
        }
        /// <summary>
        /// 系统配置添加
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override int Delete(Expression<Func<site_node, bool>> predicat)
        {

            int flag = base.Delete(predicat);
            if (flag > 0)
            {
                ReloadCache();
            }
            return flag;
        }
        /// <summary>
        /// 根据Key获取系统配置
        /// </summary>
        /// <param name="settingKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public site_node Get(string OrderBy, Expression<Func<site_node, bool>> predicate)
        {
            List<site_node> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            site_node result = new site_node();
            IEnumerable<site_node> condition = list;
            if (predicate != null)
            {

                condition = condition.Where(predicate.Compile());

            }
            if (OrderBy != null && OrderBy != "")
            {
                result = condition.MyOrderBy(OrderBy).FirstOrDefault();
            }
            else
            {
                result = condition.FirstOrDefault();
            }
            return result;


        }

        public override site_node GetByOne(Expression<Func<site_node, bool>> predicate)
        {
            List<site_node> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            site_node result = new site_node();
            IEnumerable<site_node> condition = list;
            if (predicate != null)
            {
                condition = condition.Where(predicate.Compile());
            }
            result = condition.FirstOrDefault();
            return result;
        }

        public override site_node GetByOne(string OrderBy, Expression<Func<site_node, bool>> predicate)
        {
            List<site_node> list = _NodeCache.Values.ToList();
            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            site_node result = new site_node();
            IEnumerable<site_node> condition = list;
            if (predicate != null)
            {
                condition = condition.Where(predicate.Compile());
            }
            if (OrderBy != null && OrderBy != "")
            {
                result = condition.MyOrderBy(OrderBy).FirstOrDefault();
            }
            else
            {
                result = condition.FirstOrDefault();
            }
            return result;
        }
        public override List<site_node> GetList(string keySelector, List<Expression<Func<site_node, bool>>> predicate)
        {
            List<site_node> list = _NodeCache.Values.ToList();

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            IEnumerable<site_node> condition = list;
            List<site_node> result = new List<site_node>();
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }
            }
            if (keySelector != null && keySelector != "")
            {
                result = condition.MyOrderBy(keySelector).ToList();
            }
            else
            {
                result = condition.ToList();

            }
            return result;
        }

        public List<site_node> GetList()
        {
            List<site_node> list = _NodeCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = base.GetList("nsort asc", new List<Expression<Func<site_node, bool>>>());
                list.ForEach(a =>
                {
                    string parent_name = "根栏目";

                    if (a.parent_node != 0)
                    {
                        var parent_node_item = list.Where(b => b.id == a.parent_node && b.lang == a.lang).FirstOrDefault() ;
                        parent_name = parent_node_item?.node_name;
                    }
                    a.hava_child = list.Where(b => b.parent_node == a.id).Count() > 0;
                    a.same_level_count = list.Where(b => b.parent_node == a.parent_node).Count();
                    a.parent_node_name = parent_name;
                    

                    _NodeCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_node value11) => { return a; });
                });
            }

            return list;
        }

        private void ReloadCache()
        {
            _NodeCache.Clear();
            var list = base.GetList("nsort asc", new List<Expression<Func<site_node, bool>>>());
            list.ForEach(a =>
            {
                string parent_name = "根栏目";

                if (a.parent_node != 0)
                {
                    var parent_node_item = list.Where(b => b.id == a.parent_node && b.lang == a.lang).FirstOrDefault();
                    parent_name = parent_node_item?.node_name;
                }
                a.hava_child = list.Where(b => b.parent_node == a.id).Count() > 0;
                a.same_level_count = list.Where(b => b.parent_node == a.parent_node).Count();
                a.parent_node_name = parent_name;
                _NodeCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_node value11) => { return a; });
            });

        }

        public override PageOf<site_node> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<site_node, bool>>> predicate)
        {
            List<site_node> list = _NodeCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = GetList();
            }


            List<site_node> result = new List<site_node>();
            IEnumerable<site_node> condition = list;
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }

            }

            if (keySelector != null && keySelector != "")
            {
                result = condition.MyOrderBy(keySelector).ThenBy(a => a.nsort).ToList();
            }
            else
            {
                result = condition.OrderBy(a=>a.nsort).ToList();

            }
            var page_list = result.Skip((iPage - 1) * PageSize).Take(PageSize).ToList();
            return new PageOf<site_node>()
            {
                list = page_list,
                total = result.Count,
                page_index = iPage,
                page_size = PageSize

            };
        }

        public async System.Threading.Tasks.Task<List<SiteNodesTree>> GetSiteTreeDataAsync(string type, List<Expression<Func<site_node, bool>>> conditions, string lang = "tc")
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            if (conditions != null)
            {
                IEnumerable<site_node> query = null;
                foreach (var item in conditions)
                {
                    query = list.Where(item.Compile());
                }

                list = query.ToList();
            }
            if (lang == "sc")
            {
                list = list.Where(a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2))).MyOrderBy("nsort asc").ToList();
            }
            else
            {
                list = list.Where(a => a.lang == lang).OrderBy(a => a.nsort).ToList();
            }





            SiteNodesTree result = new SiteNodesTree();
            result.children = new List<SiteNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.title = "栏目列表";
            result.out_link = "";
            result.view_disable = true;
            result.add_disable = true;
            result.mod_disable = true;
            result.del_disable = true;
            result.node_pic = "";
            result.node_atlas = "";
            result.node_desc = "";
            result.node_remark = "";

            GetSiteChildNodes(result, list, 0, type, true, lang);

            List<SiteNodesTree> rs = new List<SiteNodesTree>();
            rs.Add(result);
            return rs;
        }

        private void GetSiteChildNodes(SiteNodesTree result, List<site_node> list, long pid, string type, bool spread = false, string lang = "tc")
        {
            list.Where(a => a.parent_node == pid).OrderBy(a => a.nsort).ToList().ForEach(async a =>
            {
                SiteNodesTree child = new SiteNodesTree();
                child.children = new List<SiteNodesTree>();
                child.id = a.id;
                child.model_type = a.model_type;
                child.need_review = a.need_review;
                child.pid = pid;
                child.node_code = a.node_code;
                child.spread = spread;
                child.title = a.node_name;
                child.view_disable = true;
                child.add_disable = true;


                child.mod_disable = true;
                child.del_disable = true;
                child.out_link = a.out_link;
                child.node_pic = a.node_pic;
                child.node_atlas = a.node_atlas;
                child.node_desc = a.node_desc;
                child.node_remark = a.node_remark;

                GetSiteChildNodes(child, list, a.id, type, false, lang);
                result.children.Add(child);
            });

        }
        public async System.Threading.Tasks.Task<List<SiteNodesTree>> GetTreeDataAsync(string type, List<Expression<Func<site_node, bool>>> conditions, string lang="tc")
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            if (conditions != null)
            {
                IEnumerable<site_node> query = null;
                foreach (var item in conditions)
                {
                    query = list.Where(item.Compile());
                }

                list = query.ToList();
            }
            if (lang == "sc")
            {
                list = list.Where(a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2))).MyOrderBy("nsort asc").ToList();
            }
            else
            {
                list = list.Where(a => a.lang == lang).OrderBy(a => a.nsort).ToList();
            }





            SiteNodesTree result = new SiteNodesTree();
            result.children = new List<SiteNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.title = "栏目列表";
            result.out_link = "";
            result.view_disable = true;
            result.add_disable = true;
            result.mod_disable = true;
            result.del_disable = true;
            result.node_pic = "";
            result.node_atlas ="";
            result.node_desc = "";
            result.node_remark = "";
            if (type == "node")
            {
                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", "site_node"))
                {
                    result.view_disable = false;
                }

                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", "site_node", "add"))
                {
                    result.add_disable = false;
                }
                //if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", "site_node", "del"))
                //{
                //    result.del_disable = false;
                //}
                //if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", "site_node", "mod"))
                //{
                //    result.mod_disable = false;
                //}
            }
            else if(type == "data")
            {
                result.view_disable = false;
                result.add_disable = false;
                result.mod_disable = false;
                result.del_disable = false;
            }
            else
            {
                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", "site_content"))
                {
                    result.view_disable = false;
                }

                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", "site_content", "add"))
                {
                    result.add_disable = false;
                }

                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", "site_content", "del"))
                {
                    result.del_disable = false;
                }
                if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", "site_content", "mod"))
                {
                    result.mod_disable = false;
                }
            }
            GetChildNodes(result, list, 0, type,true,lang);

            List<SiteNodesTree> rs = new List<SiteNodesTree>();
            rs.Add(result);
            return rs;
        }

        private void GetChildNodes(SiteNodesTree result, List<site_node> list, long pid, string type,bool spread=false,string lang="tc")
        {
            list.Where(a => a.parent_node == pid).OrderBy(a => a.nsort).ToList().ForEach(async a =>
            {
                SiteNodesTree child = new SiteNodesTree();
                child.children = new List<SiteNodesTree>();
                child.id = a.id;
                child.model_type = a.model_type;
                child.need_review = a.need_review;
                child.pid = pid;
                child.node_code = a.node_code;
                child.spread = spread;
                child.title = a.node_name;
                child.view_disable = true;
                child.add_disable = true;


                child.mod_disable = true;
                child.del_disable = true;
                child.out_link = a.out_link;
                child.node_pic = a.node_pic;
                child.node_atlas = a.node_atlas;
                child.node_desc = a.node_desc;
                child.node_remark = a.node_remark;
                if (type == "node")
                {
                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", $"cms_node_g_{a.id}"))
                    {
                        child.view_disable = false;

                    }
                    else {
                        return;
                    }

                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", $"cms_node_g_{a.id}", "add"))
                    {
                        child.add_disable = false;
                    }
                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", $"cms_node_g_{a.id}", "delete"))
                    {
                        child.del_disable = false;
                    }
                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_node", $"cms_node_g_{a.id}", "update"))
                    {
                        child.mod_disable = false;
                    }
                }
                else if (type == "data")
                {
                    result.view_disable = false;
                    result.add_disable = false;
                    result.mod_disable = false;
                    result.del_disable = false;
                }
                else
                {

                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", $"cms_content_g_{a.id}"))
                    {
                        child.view_disable = false;

                    }
                    else {
                        return;
                    }

                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", $"cms_content_g_{a.id}", "add"))
                    {
                        child.add_disable = false;
                    }

                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", $"cms_content_g_{a.id}", "delete"))
                    {
                        child.del_disable = false;
                    }
                    if (await PermissionCheck.CheckAsync(httpContextAccessor.HttpContext, "site_content", $"cms_content_g_{a.id}", "update"))
                    {
                        child.mod_disable = false;
                    }
                }
                GetChildNodes(child, list, a.id, type, false, lang) ;
                result.children.Add(child);
            });

        }
        private void GetChildNodes(SiteNodesTree result, List<site_node> list, long pid, bool spread = true)
        {
            list.Where(a => a.parent_node == pid).OrderBy(a => a.nsort).ToList().ForEach(async a =>
            {
                SiteNodesTree child = new SiteNodesTree();
                child.children = new List<SiteNodesTree>();
                child.id = a.id;
                child.model_type = a.model_type;
                child.need_review = a.need_review;
                child.pid = pid;
                child.node_code = a.node_code;
                child.out_link = a.out_link;
                child.spread = spread;
                child.title = a.node_name;
                child.view_disable = false;
                child.add_disable = false;
                child.node_pic = a.node_pic;
                child.node_atlas = a.node_atlas;
                child.node_desc = a.node_desc;
                child.node_remark = a.node_remark;
                GetChildNodes(child, list, a.id,false);
                result.children.Add(child);
            });

        }



        public List<SiteNodesTree> GetAllLevelData(List<Expression<Func<site_node, bool>>> predicat)
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            list = list.OrderBy(a=>a.nsort).ToList();

            List<site_node> condtion_result = new List<site_node>();
            IEnumerable<site_node> condition = list;
            if (predicat != null && predicat.Count > 0)
            {

                foreach (var item in predicat)
                {
                    condition = condition.Where(item.Compile());
                }

            }
            condtion_result = condition.ToList();

            SiteNodesTree result = new SiteNodesTree();
            result.children = new List<SiteNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.out_link = "";
            result.title = "栏目列表";
            result.node_pic = "";
            result.node_atlas = "";

            result.node_desc = "";
            result.node_remark = "";
            GetChildNodes(result, condtion_result, 0);
            return result.children;
            //List<SiteNodesTree> rs = new List<SiteNodesTree>();
            //rs.Add(result);
            //return rs;
        }

        public List<SiteWebNodesTree> GetPathChildNodes(string node_code,string lang)
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            long parent_id = 0;
            if (lang == "sc")
            {
                var mode=GetByOne(a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) && a.node_code.ToLower() == node_code.ToLower());
                if (mode != null)
                {
                    parent_id = mode.id;
                }
            }
            else
            {
                var mode = GetByOne(a => a.lang ==lang && a.node_code.ToLower() == node_code.ToLower());
                if (mode != null)
                {
                    parent_id = mode.id;
                }
            }
            if (parent_id == 0)
            {
                return new List<SiteWebNodesTree>();
            }
            SiteWebNodesTree result = new SiteWebNodesTree();
            result.children = new List<SiteWebNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.out_link = "";
            result.title = "栏目列表";
            result.node_pic = "";
            result.node_atlas = "";
            result.node_name = "栏目列表";
            result.node_desc = "";
            result.node_remark = "";
            GetWebTreeChildNodes(list,result, parent_id, lang);


            return result.children;
        }


        private void GetWebTreeChildNodes(List<site_node> list, SiteWebNodesTree result, long pid, string lang)
        {
            Func<site_node, bool> condtion = a => a.parent_node == pid && a.lang == lang && a.show_path == StatusEnum.Legal;
            if (lang == "sc")
            {
                condtion = a => a.parent_node == pid && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) && a.show_path == StatusEnum.Legal;
            }
            list.Where(condtion).OrderBy(a => a.nsort).ToList().ForEach(async a =>
            {
                SiteWebNodesTree child = new SiteWebNodesTree();
                child.children = new List<SiteWebNodesTree>();
                child.id = a.id;
                child.model_type = a.model_type;
                child.need_review = a.need_review;
                child.pid = pid;
                child.node_code = a.node_code;
                child.spread = false;
                child.title = a.node_name;
                child.view_disable = true;
                child.add_disable = true;
                child.out_link = a.out_link;
                child.node_pic = a.node_pic;
                child.node_atlas = a.node_atlas;
                child.node_name = a.node_name;
                child.node_desc = a.node_desc;
                child.node_remark = a.node_remark;
                GetWebTreeChildNodes(list, child, a.id, lang);
                result.children.Add(child);
            });
        }

        public List<SiteWebNodesTree> GetPathChildNodes(string node_code, string lang, List<Expression<Func<site_node, bool>>> predica)
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }

            long parent_id = 0;
            if (lang == "sc")
            {
                var mode = GetByOne(a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2)) && a.node_code.ToLower() == node_code.ToLower());
                if (mode != null)
                {
                    parent_id = mode.id;
                }
            }
            else
            {
                var mode = GetByOne(a => a.lang == lang && a.node_code.ToLower() == node_code.ToLower());
                if (mode != null)
                {
                    parent_id = mode.id;
                }
            }
            if (parent_id == 0)
            {
                return new List<SiteWebNodesTree>();
            }
            SiteWebNodesTree result = new SiteWebNodesTree();
            result.children = new List<SiteWebNodesTree>();
            result.id = 0;
            result.model_type = "";
            result.node_code = "";
            result.spread = true;
            result.need_review = StatusEnum.UnLegal;
            result.pid = 0;
            result.out_link = "";
            result.title = "栏目列表";
            result.node_pic = "";
            result.node_atlas = "";
            result.node_name = "栏目列表";
            result.node_desc = "";
            result.node_remark = "";
            GetWebTreeChildNodes(list, result, parent_id, lang, predica);


            return result.children;
        }



        private void GetWebTreeChildNodes(List<site_node> list, SiteWebNodesTree result, long pid, string lang, List<Expression<Func<site_node, bool>>> predicat)
        {
            Func<site_node, bool> condtion = a => a.parent_node == pid ;
            if (lang == "sc")
            {
                condtion = a => a.parent_node == pid && (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            IEnumerable<site_node> condition_list = list.Where(condtion);
            if (predicat != null && predicat.Count > 0)
            {

                foreach (var item in predicat)
                {
                    condition_list = condition_list.Where(item.Compile());
                }

            }

            condition_list.OrderBy(a => a.nsort).ToList().ForEach(a =>
            {
                SiteWebNodesTree child = new SiteWebNodesTree();
                child.children = new List<SiteWebNodesTree>();
                child.id = a.id;
                child.model_type = a.model_type;
                child.need_review = a.need_review;
                child.pid = pid;
                child.node_code = a.node_code;
                child.spread = false;
                child.title = a.node_name;
                child.view_disable = true;
                child.add_disable = true;
                child.out_link = a.out_link;
                child.node_pic = a.node_pic;
                child.node_atlas = a.node_atlas;
                child.node_name = a.node_name;
                child.node_desc = a.node_desc;
                child.node_remark = a.node_remark;
                GetWebTreeChildNodes(list, child, a.id, lang, predicat);
                result.children.Add(child);
            });
        }


        public site_node GetUpLeaveLuceneNode(string lang, string node_code)
        {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            Func<site_node, bool> condtion = a => a.lang == lang;
            if (lang == "sc")
            {
                condtion = a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            list=list.Where(condtion).ToList();

            site_node current = list.Where(a => a.node_code.ToLower() == node_code.ToLower()).FirstOrDefault();

            if (current == null)
            {
                return null;
            }
           
            if (current.lucene_index == StatusEnum.Legal)
            {
                return current;
            }
            if (current.parent_node == 0)
            {
                return null;
            }
            return RecursionParentNode(list, current.parent_node);
        }

        public site_node RecursionParentNode(List<site_node> list, long parent_id)
        {
            site_node current = list.Where(a => a.id==parent_id).FirstOrDefault();

            if (current == null)
            {
                return null;
            }
           

            if (current.lucene_index == StatusEnum.Legal)
            {
                return current;
            }
            if (current.parent_node == 0)
            {
                return null;
            }
            return RecursionParentNode(list, current.parent_node);
        }


        /// <summary>
        /// 获取所有父级栏目
        /// </summary>
        /// <param name="lang"></param>
        /// <param name="node_code"></param>
        /// <returns></returns>
        public ParentSiteNode GetAllLevelParentNode(string lang, string node_code) {
            List<site_node> list = _NodeCache.Values as List<site_node>;

            if (list == null || list.Count <= 0)
            {
                list = GetList();
            }
            Func<site_node, bool> condtion = a => a.lang == lang;
            if (lang == "sc")
            {
                condtion = a => (a.lang == "sc" || (a.lang == "tc" && a.lang_flag == 2));
            }
            list = list.Where(condtion).ToList();

            site_node current = list.Where(a => a.node_code.ToLower() == node_code.ToLower()).FirstOrDefault();
            ParentSiteNode obj = new ParentSiteNode();
            if (current == null)
            {
                return obj;
            }
            obj.model = current;
            if (current.parent_node == 0)
            {
                return obj;
            }
            ParentNode(obj, list, current.parent_node);
            
            return obj;
        }

        public ParentSiteNode ParentNode(ParentSiteNode result,List<site_node> list, long parent_id)
        {
            checked { 
            
            }
            ParentSiteNode obj = new ParentSiteNode();
            site_node current = list.Where(a => a.id == parent_id).FirstOrDefault();
            if (current == null)
            {
                return null;
            }
            obj.model = current;
            var parent = new ParentSiteNode();
            if (current.parent_node != 0)
            {
                 ParentNode(obj, list, current.parent_node);
            }
            else {
                parent = null;
            }
            obj.parent_node = parent;
            result.parent_node = obj;
            return result;
        }
    }
}
