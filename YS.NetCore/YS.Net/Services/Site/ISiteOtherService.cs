﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteOtherService : IService<site_other>
    {

        public string GetContentByCode(string code, string lang);

        string GetContentByCode(string code, string lang, StatusEnum is_show);
    }
}
