﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteMemberService : ServiceBase<site_member, Microsoft.Extensions.Logging.ILogger<site_member>>, ISiteMemberService
    {

        IHttpContextAccessor httpContextAccessor;
        public SiteMemberService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor, IRepository<site_member> _useService,
            Microsoft.Extensions.Logging.ILogger<site_member> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
