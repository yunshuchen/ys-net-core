﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteContentService : IService<site_content>
    {


        site_content Get(string OrderBy, Expression<Func<site_content, bool>> predicate);
        site_content Add(site_content item);
        List<site_content> GetList();
   
        object GetNodeContentList(int iPage, int PageSize, long Pid, List<Expression<Func<site_content, bool>>> predicate,string lang="tc");
    }
}
