﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using System.Linq.Dynamic.Core;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteContentService : ServiceBase<site_content, Microsoft.Extensions.Logging.ILogger<site_content>>, ISiteContentService
    {
        private readonly ConcurrentDictionary<string, site_content> _ConetntCache;
        private const string SysCmsNodes = "SysMuliteCmsContent";
        IHttpContextAccessor httpContextAccessor;
        public SiteContentService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            ISkyCacheManager<ConcurrentDictionary<string, site_content>> cacheManager,
           IRepository<site_content> _useService,
            Microsoft.Extensions.Logging.ILogger<site_content> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;

           // _ConetntCache = cacheManager.GetOrAdd(SysCmsNodes, new ConcurrentDictionary<string, site_content>());
        }

        private void ReloadCache()
        {
            //_ConetntCache.Clear();
            //var list = base.GetList("id asc", new List<Expression<Func<site_content, bool>>>());
            //list.ForEach(a =>
            //{
            //    _ConetntCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_content value11) => { return a; });
            //});

        }

        //public override site_content Insert(site_content item)
        //{
        //    var result = base.Insert(item);
        //    ReloadCache();
        //    return result;
        //}
        ///// <summary>
        ///// 系统配置添加
        ///// </summary>
        ///// <param name="item"></param>
        ///// <returns></returns>
        //public override int Delete(Expression<Func<site_content, bool>> predicat)
        //{

        //    int flag = base.Delete(predicat);
        //    if (flag > 0)
        //    {
        //        ReloadCache();
        //    }
        //    return flag;
        //}


        //public override site_content GetByOne(Expression<Func<site_content, bool>> predicate)
        //{

        //    return GetByOne("", predicate);
        //}
        //public override int Count(Expression<Func<site_content, bool>> predicate)
        //{
        //    List<site_content> list = _ConetntCache.Values.ToList();
        //    if (list == null || list.Count <= 0)
        //    {
        //        list = GetList();
        //    }
        //    return list.Count(predicate.Compile());
        //}
        
        //public override void BulkInsert(List<site_content> model)
        //{
        //    base.BulkInsert(model);
        //    ReloadCache();
        //}
        //public override site_content GetByOne(string OrderBy, Expression<Func<site_content, bool>> predicate)
        //{
        //    List<site_content> list = _ConetntCache.Values.ToList();
        //    if (list == null || list.Count <= 0)
        //    {
        //        list = GetList();
        //    }
        //    site_content result = new site_content();
        //    IEnumerable<site_content> condition = list;
        //    if (predicate != null)
        //    {
        //        condition = condition.Where(predicate.Compile());
        //    }
        //    if (OrderBy != null && OrderBy != "")
        //    {
        //        result = condition.OrderBy(OrderBy).FirstOrDefault();
        //    }
        //    else
        //    {
        //        result = condition.FirstOrDefault();
        //    }
            
        //    return result;
        //}
        //public override List<site_content> GetList(string keySelector, List<Expression<Func<site_content, bool>>> predicate)
        //{
        //    List<site_content> list = _ConetntCache.Values.ToList();

        //    if (list == null || list.Count <= 0)
        //    {
        //        list = GetList();
        //    }

        //    IEnumerable<site_content> condition = list;
        //    List<site_content> result = new List<site_content>();
        //    if (predicate != null && predicate.Count > 0)
        //    {

        //        foreach (var item in predicate)
        //        {
        //            condition = condition.Where(item.Compile());
        //        }
        //    }
        //    if (keySelector != null && keySelector != "")
        //    {
        //        result = condition.OrderBy(keySelector).ToList();
        //    }
        //    else
        //    {
        //        result = condition.ToList();

        //    }
        //    return result;
        //}
        //public override PageOf<site_content> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<site_content, bool>>> predicate)
        //{
        //    List<site_content> list = _ConetntCache.Values.ToList();
        //    if (list == null || list.Count() <= 0)
        //    {
        //        list = GetList();
        //    }


        //    List<site_content> result = new List<site_content>();
        //    IEnumerable<site_content> condition = list;
        //    if (predicate != null && predicate.Count > 0)
        //    {

        //        foreach (var item in predicate)
        //        {
        //            condition = condition.Where(item.Compile());
        //        }

        //    }
        //    if (keySelector == "")
        //    {
        //        result = condition.OrderByDescending(a => a.id).ThenBy(a => a.n_sort).ToList();
        //    }
        //    else {
        //        result = condition.OrderBy(keySelector).ToList();
        //    }
        //    var page_list = result.Skip((iPage - 1) * PageSize).Take(PageSize).ToList();
        //    return new PageOf<site_content>()
        //    {
        //        list = page_list,
        //        Total = result.Count,
        //        PageIndex = iPage,
        //        PageSize = PageSize

        //    };
        //}


        //public override int Modify(Expression<Func<site_content, bool>> condition, System.Linq.Expressions.Expression<Func<site_content, site_content>> content)
        //{
        //    int falg = base.Modify(condition, content);
        //    ReloadCache();
        //    return falg;
        //}
        public site_content Add(site_content item)
        {
            //var result = base.Insert(item);
            //ReloadCache();
            //return result;
            return base.Insert(item);
        }

        public site_content Get(string OrderBy, Expression<Func<site_content, bool>> predicate)
        {
            return base.GetByOne(OrderBy, predicate);
        }

        public List<site_content> GetList()
        {

            //List<site_content> list = null;
            //if (list == null)
            //{
               return base.GetList("id asc", new List<Expression<Func<site_content, bool>>>());

            //}

            //list.ForEach(a =>
            //{
            //    _ConetntCache.TryUpdate(a.id.MyToString() + "_" + a.lang, (site_content value11) => { return a; });
            //});
            //return list;
        }

        public object GetNodeContentList(int iPage, int PageSize, long pid, List<Expression<Func<site_content, bool>>> predicate, string lang = "tc")
        {

            var query = from mm in ToJoin("", predicate)
                        join cm in OtherTable<site_node>()
                        on new { node_id = mm.node_id, lang = mm.lang} equals new { node_id = cm.id, lang = cm.lang} into b_join
                        from cm in b_join.DefaultIfEmpty()
                        where mm.lang==lang && mm.node_id== pid
                        orderby mm.is_top descending
                        orderby mm.n_sort descending
                        orderby mm.create_time descending
                        select new
                        {
                            mm.row_key,
                            mm.id,
                            mm.lang,
                            mm.lang_flag,
                            mm.node_id,
                            mm.node_code,
                            mm.title,
                            mm.link,
                            mm.summary,
                            mm.chr_content,
                            mm.extend_fields_json,
                            mm.cover,
                            mm.atlas,
                            mm.n_sort,
                            mm.is_home,
                            mm.is_top,
                            mm.is_node,
                            mm.view_count,
                            mm.status,
                            mm.review_status,
                            mm.create_time,
                            mm.creator,
                            mm.creator_name,
                            mm.tag,
                            mm.is_show,
                            mm.author,
                            mm.source,
                            mm.del_flag,
                            mm.down_file,
                            cm.node_name,
                            pid = cm.parent_node,
                            hava_child = false,
                            same_level_count = 1
                        };
            var result = query.PageResult(iPage, PageSize);


            var list = result.Queryable.ToList();
            List<IDictionary<string, object>> new_list = new List<IDictionary<string, object>>();
            list.ForEach(a =>
            {
                string test = JsonHelper.ToJson(a);
                var dic = JsonHelper.ToObject<IDictionary<string, object>>(test);
                dic["hava_child"] = list.Where(b => b.pid == a.node_id).Count() > 0;
                dic["same_level_count"] = list.Where(b => b.node_id == a.node_id).Count();

                new_list.Add(dic);
            });
            return new
            {
                list = new_list,
                total= result.RowCount,
                page_index = iPage,
                page_size=PageSize
            };



        }
    }
}
