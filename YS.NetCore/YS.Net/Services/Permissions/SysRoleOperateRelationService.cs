

using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysRoleOperateRelationService : ServiceBase<sys_role_operate_relation_entity, Microsoft.Extensions.Logging.ILogger<sys_role_operate_relation_entity>>, ISysRoleOperateRelationService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysRoleOperateRelationService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
                  IRepository<sys_role_operate_relation_entity> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_role_operate_relation_entity> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }

        public List<sys_role_operate_relation_entity> GetRoleOperateList(string keySelector, List<Expression<Func<sys_role_operate_relation_entity, bool>>> predicate)
        {
        //    using (Chloe.IDbContext context = base.DbContext.Provider)
        //    {

        //        var query = base.ToJoin(context, keySelector, predicate);
        //        var Ijoin_query = query.InnerJoin<sys_operate_entity>((r, o) =>
        //        r.operate_code.ToLower() == o.operate_code.ToLower()
        //        && r.res_code.ToLower() == o.p_res_code.ToLower()
        //        && r.module_code.ToLower() == o.p_module_code.ToLower());
        //        var query_map = Ijoin_query.Select((r, o) => new { r, o });  /* 投影成匿名对象 */
        //        var select2 = query_map.Select(a => new
        //        {
        //            id = a.r.id,
        //            role_id = a.r.role_id,
        //            module_code = a.r.module_code,
        //            res_code = a.r.res_code,
        //            operate_code = a.r.operate_code,
        //            operate_name = a.o.operate_name,
        //            api_url = a.o.api_url,
        //            is_menu = a.o.is_menu
        //        }).ToList();
        //        var list = JsonHelper.ToObject<List<sys_role_operate_relation_entity>>(JsonHelper.ToJson(select2));
        //        return list;
        //    }

            var query = from mm in ToJoin(keySelector, predicate)
                        join cm in OtherTable<sys_operate_entity>()
                        on new { operate_code = mm.operate_code, res_code = mm.res_code, module_code = mm.module_code } equals new { operate_code = cm.operate_code, res_code = cm.p_res_code, module_code = cm.p_module_code } into b_join
                        from cm in b_join.DefaultIfEmpty()
                       
                        select new
                        {
                            id = mm.id,
                            role_id = mm.role_id,
                            module_code = mm.module_code,
                            res_code = mm.res_code,
                            operate_code = mm.operate_code,
                            operate_name = cm.operate_name,
                            api_url = cm.api_url,
                            is_menu = cm.is_menu
                        };
            var result = query.ToList();

            var list = JsonHelper.ToObject<List<sys_role_operate_relation_entity>>(JsonHelper.ToJson(result));
            return list;

        }
    }
}
