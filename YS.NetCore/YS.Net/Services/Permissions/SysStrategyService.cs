﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysStrategyService : ServiceBase<sys_strategy, Microsoft.Extensions.Logging.ILogger<sys_strategy>>, ISysStrategyService
    {
        private readonly ConcurrentDictionary<string, sys_strategy> _NodeCache;
        private const string cache_key = "sys_account_strategy";
        IHttpContextAccessor httpContextAccessor;
        public SysStrategyService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
               ISkyCacheManager<ConcurrentDictionary<string, sys_strategy>> cacheManager,
               IRepository<sys_strategy> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_strategy> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
            _NodeCache = cacheManager.GetOrAdd(cache_key, new ConcurrentDictionary<string, sys_strategy>());
        }

        
        public override int Modify(Expression<Func<sys_strategy, bool>> condition, Expression<Func<sys_strategy, sys_strategy>> content)
        {
            int result = base.Modify(condition, content);
            if (result > 0)
            {
                ReloadCache();
            }
            return result;
        }
        public override sys_strategy Insert(sys_strategy item)
        {
            var model= base.Insert(item);
            if (model.id > 0)
            {
                ReloadCache();
            }
            return model;
        }
        public override int Delete(Expression<Func<sys_strategy, bool>> predicat)
        {

            int flag = base.Delete(predicat);
            if (flag > 0)
            {
                ReloadCache();
            }
            return flag;
        }

        public override sys_strategy GetByOne(string OrderBy, Expression<Func<sys_strategy, bool>> predicate)
        {
            sys_strategy result = new sys_strategy();
            List<sys_strategy> list = _NodeCache.Values.ToList();
            IEnumerable<sys_strategy> condition = list;
            if (predicate != null)
            {

                condition = condition.Where(predicate.Compile());
            }
            if (OrderBy != null && OrderBy != "")
            {
                result = condition.MyOrderBy(OrderBy).FirstOrDefault();
            }
            else
            {
                result = condition.FirstOrDefault();
            }
            return result;

        }

        public override sys_strategy GetByOne(Expression<Func<sys_strategy, bool>> predicate)
        {
            return this.GetByOne("",predicate);
        }


        public override List<sys_strategy> GetList(string keySelector, List<Expression<Func<sys_strategy, bool>>> predicate)
        {
            List<sys_strategy> list = _NodeCache.Values.ToList();

            if (list == null || list.Count <= 0)
            {
                list = ReloadCache();
            }

            IEnumerable<sys_strategy> condition = list;
            List<sys_strategy> result = new List<sys_strategy>();
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }
            }
            if (keySelector != null && keySelector != "")
            {
                result = condition.MyOrderBy(keySelector).ToList();
            }
            else
            {
                result = condition.ToList();

            }
            return result;
        }


        public override PageOf<sys_strategy> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<sys_strategy, bool>>> predicate)
        {
            List<sys_strategy> list = _NodeCache.Values.ToList();
            if (list == null || list.Count() <= 0)
            {
                list = ReloadCache();
            }


            List<sys_strategy> result = new List<sys_strategy>();
            IEnumerable<sys_strategy> condition = list;
            if (predicate != null && predicate.Count > 0)
            {

                foreach (var item in predicate)
                {
                    condition = condition.Where(item.Compile());
                }
            }
            result = condition.OrderBy(a => a.id).ToList();
            var page_list = result.Skip((iPage - 1) * PageSize).Take(PageSize).ToList();
            return new PageOf<sys_strategy>()
            {
                list = page_list,
                total = result.Count,
                page_index = iPage,
                page_size = PageSize

            };
        }

        private List<sys_strategy> ReloadCache()
        {
            _NodeCache.Clear();
            var list = base.GetList("id asc", new List<Expression<Func<sys_strategy, bool>>>());
            list.ForEach(a => {
                _NodeCache.TryAdd(a.id.MyToString(), a);
            });

            return list;

        }
    }
}
