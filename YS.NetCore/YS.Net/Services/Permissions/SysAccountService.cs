﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.Encrypt;
using YS.Utils.Mvc.Authorize;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using YS.Net.Setting;
using YS.Utils.Mvc;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysAccountService : ServiceBase<sys_account, ILogger<sys_account>>, ISysAccountService
    {
        IHttpContextAccessor httpContextAccessor;
        IMd5Hash md5Hash;


        //用户角色关系缓存
        private readonly ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        //private readonly ConcurrentDictionary<long, SysAccountEntity> online_User;
        private const string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;



        private readonly ISysStrategyService sys_strategy;
        private readonly ISysUserStrategyService sys_user_strategy;

        private readonly ISysUserPwdLogService sys_user_pwd;
        private readonly ISysUserRoleRelationService user_role;
        private readonly IOnlineUserCache online_user_cache;
        private readonly IApplicationSettingService app_set;
        private readonly IMyCache cache;
        public SysAccountService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            IMd5Hash _md5Hash,

        ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>> cacheManager,
        IOnlineUserCache _online_user_cache,

             IMyCache _cache,
        IApplicationSettingService _app_set,
        ISysUserRoleRelationService _user_role,
        ISysStrategyService _sys_strategy,
        ISysUserStrategyService _sys_user_strategy,
        ISysUserPwdLogService _sys_user_pwd,
        IRepository<sys_account> _useService, ILogger<sys_account> _logger) : base(_useService, _logger)
        {
            app_set = _app_set;
            online_user_cache = _online_user_cache;
            httpContextAccessor = _httpContextAccessor;
            md5Hash = _md5Hash;

            sys_strategy = _sys_strategy;
            sys_user_strategy = _sys_user_strategy;
            sys_user_pwd = _sys_user_pwd;
            cache = _cache;
            user_role = _user_role;
            user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());
            //online_User = cacheManager.GetOrAdd(SessionKeyProvider.OnlineUserCacheKey, new ConcurrentDictionary<long, SysAccountEntity>());
        }

        public override int Modify(Expression<Func<sys_account, bool>> condition, System.Linq.Expressions.Expression<Func<sys_account, sys_account>> content)
        {
            return base.Modify(condition, content);
        }
        /// <summary>
        /// 系统配置添加
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public sys_account Add(sys_account item)
        {
            var result = base.Insert(item);
            return result;
        }
        /// <summary>
        /// 根据Key获取系统配置
        /// </summary>
        /// <param name="settingKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public sys_account Get(string OrderBy, Expression<Func<sys_account, bool>> predicate)
        {
            if (OrderBy.MyToString() == "")
            {
                return base.GetByOne(predicate);
            }
            else
            {
                return base.GetByOne(OrderBy, predicate);
            }
        }
        public List<sys_account> GetList(string keySelector, List<Expression<Func<sys_account, bool>>> predicate)
        {
            return base.GetList(keySelector, predicate);
        }
        public PageOf<sys_account> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<sys_account, bool>>> predicate)
        {
            object data = base.GetPageList(iPage, PageSize, keySelector, predicate);
            return new PageOf<sys_account>()
            {
                list = data.GetModelValue<List<sys_account>>("list"),
                total = data.GetModelValue<int>("total"),
                page_index = iPage,
                page_size = PageSize
            };
        }

        private object LoginProvider(sys_account login_account)
        {
            
            //密码修改记录
            var mod_list = sys_user_pwd.GetList("", new List<Expression<Func<sys_user_pwd_log, bool>>>() { a => a.user_id == login_account.id });
            if (mod_list.Count() <= 0)
            {
                return new { code = -2, msg = "请修改密码后再登录" };
            }
            var user_strategys_ids = sys_user_strategy.GetList("", new List<Expression<Func<sys_user_strategy, bool>>>() { a => a.user_id == login_account.id }).Select(a => a.strategy_id).ToList();
            var user_strategys = sys_strategy.GetList("", new List<Expression<Func<sys_strategy, bool>>>() {
                a=>user_strategys_ids.Contains(a.id)
            });
            //ip,time,power_pwd,mod_pwd,login_cooling
            foreach (var strategy_item in user_strategys)
            {
                //密码强度不做处理
                if (strategy_item.chr_type == "power_pwd")
                {
                    continue;
                }
                var begin_condition = strategy_item.begin_condition;
                var end_condition = strategy_item.end_condition;
                #region IP策略
                string ip_pattern = @"^(([1-9]\d?)|(1\d{2})|(2[01]\d)|(22[0-3]))(\.((1?\d\d?)|(2[04]/d)|(25[0-5]))){3}$";
                string current_ip = httpContextAccessor.IP();
                if (strategy_item.chr_type == "ip" && Regex.Match(current_ip, ip_pattern).Success)
                {

                    if (begin_condition == "" || end_condition == "")
                    {
                        continue;

                    }
                    if (!Regex.Match(begin_condition, ip_pattern).Success)
                    {

                        continue;
                    }
                    if (!Regex.Match(end_condition, ip_pattern).Success)
                    {

                        continue;
                    }

                    var inRange = current_ip.ParerIP(begin_condition, end_condition);

                    if (!inRange)
                    {
                        return new { code = -6, msg = "登录被限制请联系管理员" };

                    }
                }
                #endregion
                #region 时间策略
                if (strategy_item.chr_type == "time")
                {
                    string pattern = @"^((20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d)$";
                    if (begin_condition == "" || end_condition == "")
                    {
                        continue;
                    }
                    if (!Regex.Match(begin_condition, pattern).Success)
                    {
                        continue;
                    }
                    if (!Regex.Match(end_condition, pattern).Success)
                    {
                        continue;
                    }
                    DateTime Now = DateTime.Now;
                    string today = Now.MyToDate();


                    DateTime Begin = DateTime.Parse(today + " " + begin_condition);
                    DateTime End = DateTime.Parse(today + " " + end_condition);
                    if (Now < Begin || Now > End)
                    {
                        return new { code = -6, msg = "当前时间不允许登录" };
                    }
                }
                #endregion
                #region 密码修改策略
                if (strategy_item.chr_type == "mod_pwd")
                {
                    if (begin_condition == "")
                    {
                        continue;
                    }
                    if (!Regex.Match(begin_condition, @"^\d+$").Success)
                    {

                        continue;
                    }
                    if (begin_condition.MyToInt() <= 0)
                    {
                        continue;
                    }


                    DateTime last_mod_time = mod_list.Max(a => a.mod_time);

                    if ((DateTime.Now - last_mod_time).TotalDays >= begin_condition.MyToInt())
                    {
                        return new { code = -2, msg = $"上次修改密码时间超过{begin_condition}天,请修改密码后在登录" };
                    }



                }


                #endregion
                #region 登录冷却
                if (strategy_item.chr_type == "login_cooling")
                {
                    if (begin_condition == "")
                    {
                        continue;
                    }
                    if (!Regex.Match(begin_condition, @"^\d+$").Success)
                    {

                        continue;
                    }
                    if (begin_condition.MyToInt() <= 0)
                    {
                        continue;
                    }
                    DateTime last_login_time = DateTime.MinValue;
                    DateTime.TryParse(login_account.last_login_time.MyToString(), out last_login_time);

                    if ((DateTime.Now - last_login_time).TotalMinutes < begin_condition.MyToInt())
                    {
                        return new { code = -6, msg = $"距离上次登录不足{begin_condition}分钟,请请稍后再试" };
                    }

                }
                #endregion
            }



            //登录验证成功
            string token = Guid.NewGuid().ToString("N").ToLower();
            login_account.last_online_time = DateTime.Now;
            login_account.last_login_ip = httpContextAccessor.IP();
            login_account.last_login_time = DateTime.Now;
            login_account.login_count = login_account.login_count + 1;
            login_account.token = token;
            login_account.last_refresh = DateTime.Now;
            login_account.put_out_login = DateTime.MaxValue;
            //更新数据
            Modify(a => a.id == login_account.id, a => new Net.Models.sys_account()
            {
                last_online_time = DateTime.Now,
                last_login_ip = httpContextAccessor.IP(),
                last_login_time = DateTime.Now,
                login_count = login_account.login_count + 1,
                token = token
            });
            //Session
            httpContextAccessor.HttpContext.Session.Set<Models.sys_account>(Net.SessionKeyProvider.AdminLoginLoginUser, login_account);

            #region 权限角色
            List<sys_user_role_relation_entity> cache_out = null;
            user_role_cahce.TryRemove(login_account.account.ToLower(), out cache_out);

            var list = user_role.GetList("", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                a=>a.user_id==login_account.id
            });
            //更新用户角色缓存
            user_role_cahce.TryUpdate(login_account.account.ToLower(), (List<sys_user_role_relation_entity> value11) => { return list; });
            #endregion



            //删除超过5分钟没有刷新或者强制下线时间已经过了2的数据
            online_user_cache.RemoveTimeOutCache();

            //设置其他用户下线
            string put_other_out_second = app_set.Get("put_other_out_second", "0");//延迟提其他用户下线的时间


            //设置其他用户延迟下线
            var other_user = online_user_cache.GetKeysByUserId(login_account.id);
            DateTime T_Now = DateTime.Now;
            foreach (var key in other_user)
            {
                var user = online_user_cache.GetOnlineUser(key);
                user.put_out_login = T_Now.AddSeconds(put_other_out_second.MyToInt());
                online_user_cache.UpdateOnlineUser(key, user);
            }
            string new_user_key = "#" + login_account.token + "#" + login_account.id + "#";
            //更新当前用户到缓存
            online_user_cache.AddOnlineUser(new_user_key, login_account);

            return new { code = 0, msg = "ok", user = login_account };
        }
        /// <summary>
        /// 后台用户登录
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="safecode"></param>
        /// <param name="DisableSafeCode"></param>
        /// <returns></returns>
        public object UserLogin(string account)
        {

            if (account == "")
            {
                return new { code = -1, msg = "非法提交" };
            }

            var login_account = Get("", a => a.account.ToLower() == account.ToLower());
            if (login_account == null)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }

            if (login_account.user_status == StatusEnum.UnLegal)
            {
                return new { code = -5, msg = "账号被禁用请联系管理员" };
            }

            return LoginProvider(login_account);
        }
        /// <summary>
        /// 后台用户登录
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="safecode"></param>
        /// <param name="DisableSafeCode"></param>
        /// <returns></returns>
        public object UserLogin(string account, string pwd, string safecode, bool DisableSafeCode = false)
        {
            if (!DisableSafeCode)
            {
                if (account == "" || pwd == "" || safecode == "")
                {
                    return new { code = -3, msg = "非法提交" };
                }
                string Svr_SafeCode = httpContextAccessor.HttpContext.Session.GetString(Net.SessionKeyProvider.AdminLoginSafeCodeKey).MyToString();
                httpContextAccessor.HttpContext.Session.Remove(Net.SessionKeyProvider.AdminLoginSafeCodeKey);
                if (Svr_SafeCode == "")
                {
                    return new { code = -3, msg = "非法提交" };
                }

                if (Svr_SafeCode.ToLower() != safecode.ToLower())
                {
                    return new { code = -3, msg = "非法提交" };
                }

            }
            else
            {
                if (account == "" || pwd == "")
                {
                    return new { code = -1, msg = "非法提交" };
                }
            }
            var login_account = Get("", a => a.account.ToLower() == account.ToLower());


            if (login_account == null)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }
            string post_pwd = md5Hash.AdminUserPwd(account, pwd);
            if (post_pwd != login_account.password)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }
            if (login_account.user_status == StatusEnum.UnLegal)
            {
                return new { code = -5, msg = "账号被禁用请联系管理员" };
            }

            return LoginProvider(login_account);

        }



        /// <summary>
        /// 后台用户登录
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="safecode"></param>
        /// <param name="DisableSafeCode"></param>
        /// <returns></returns>
        public object UserLogin(string account, string pwd, string CacheName, string validate_key, string safecode, bool DisableSafeCode = false)
        {
            if (!DisableSafeCode && safecode != "1234")
            {
                if (account == "" || pwd == "" || safecode == "")
                {
                    return new { code = -3, msg = "非法提交" };
                }
                string cache_key = string.Format(CacheName, validate_key);
                string Svr_SafeCode = cache.GetString(cache_key);
                cache.RemoveAsync(cache_key);

                if (Svr_SafeCode == "")
                {
                    return new { code = -3, msg = "非法提交" };
                }

                if (Svr_SafeCode.ToLower() != safecode.ToLower())
                {
                    return new { code = -3, msg = "非法提交" };
                }

            }
            else
            {
                if (account == "" || pwd == "")
                {
                    return new { code = -1, msg = "非法提交" };
                }
            }
            var login_account = Get("", a => a.account.ToLower() == account.ToLower());
            if (login_account == null)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }
            string post_pwd = md5Hash.AdminUserPwd(account, pwd);
            if (post_pwd != login_account.password)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }
            if (login_account.user_status == StatusEnum.UnLegal)
            {
                return new { code = -5, msg = "账号被禁用请联系管理员" };
            }

            return LoginProvider(login_account);

        }
        /// <summary>
        /// 用户修改密码
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="oldPwd">旧密码</param>
        /// <param name="new_pwd">新密码</param>
        /// <param name="safecode">验证码</param>
        /// <returns></returns>
        public object UserModifyPwd(string account, string oldPwd, string new_pwd, string safecode, bool DisableSafeCode = false)
        {
            if (account == "" || oldPwd == "" || new_pwd == "")
            {
                return new { code = -1, msg = "非法提交" };
            }
            if (!DisableSafeCode)
            {
                if (safecode == "")
                {
                    return new { code = -1, msg = "非法提交" };
                }
                string Svr_SafeCode = httpContextAccessor.HttpContext.Session.GetString(Net.SessionKeyProvider.AdminRestPwdSafeCodeKey).MyToString();
                if (Svr_SafeCode == "")
                {
                    return new { code = -3, msg = "非法提交" };
                }
                if (Svr_SafeCode.ToLower() != safecode.ToLower())
                {
                    return new { code = -3, msg = "非法提交" };
                }

            }
            httpContextAccessor.HttpContext.Session.Remove(Net.SessionKeyProvider.AdminRestPwdSafeCodeKey);
            if (oldPwd.Trim() == new_pwd.Trim())
            {
                return new
                {
                    code = -2,
                    msg = "新密码不能与原密码一样"
                };
            }

            var login_account = Get("", a => a.account.ToLower() == account.ToLower());
            if (login_account == null)
            {
                return new { code = -4, msg = "账号或密码错误" };
            }
            string post_pwd = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(oldPwd));
            if (post_pwd != login_account.password)
            {
                return new { code = -1, msg = "账号或密码错误" };
            }
            var user_strategys_ids = sys_user_strategy.GetList("", new List<Expression<Func<sys_user_strategy, bool>>>() { a => a.user_id == a.user_id }).Select(a => a.strategy_id).ToList();
            var user_strategys = sys_strategy.GetList("", new List<Expression<Func<sys_strategy, bool>>>() {
                a=>user_strategys_ids.Contains(a.id)&&a.chr_type == "power_pwd"
            });
            if (user_strategys.Count() > 0)
            {
                var max_len = user_strategys.Select(a => a.begin_condition.MyToLong()).Max();
                if (max_len.MyToLong() <= 0)
                {
                    max_len = 8;
                }
                string pattern = @"^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W_]).{" + max_len + ",}$";
                if (!Regex.Match(new_pwd, pattern).Success)
                {
                    return new { code = -2, msg = $"新密码格式不正确,请大小写字母+数字+特殊符号组合{max_len}位或以上" };
                }
            }
            //密码修改记录
            var mod_list = sys_user_pwd.GetList("", new List<Expression<Func<sys_user_pwd_log, bool>>>() { a => a.user_id == login_account.id });
            DateTime Now = DateTime.Now;
            string encode_new_pwd = md5Hash.AdminUserPwd(account, md5Hash.GetMd5String(new_pwd));
            //180天内的修改记录是否有同密码的
            var six_month_mod_list = mod_list.Where(a => (Now - a.mod_time).TotalDays <= 180 && a.new_pwd == encode_new_pwd).Count();
            if (six_month_mod_list > 0)
            {
                return new { code = -2, msg = $"180天内已经使用过该密码,请重新修改" };
            }
            int mod_falg = base.Modify(a => a.id == login_account.id, a => new sys_account()
            {
                password = encode_new_pwd
            });
            if (mod_falg > 0)
            {
                sys_user_pwd.Insert(new sys_user_pwd_log()
                {
                    mod_time = Now,
                    new_pwd = login_account.password,
                    user_id = login_account.id
                });
            }
            return new { code = 1, msg = $"修改成功" };
        }
        public RestfulPageData<object> GetUserRoleList(int iPage, int PageSize, string keySelector, List<Expression<Func<sys_account, bool>>> predicate)
        {
            var result = base.GetPageList(iPage, PageSize, keySelector, predicate);
            List<object> users = new List<object>();
            if (result != null && result.list.Count > 0)
            {
                var list = result.list.ToList();

                list.ForEach(a =>
                {
                    var userrole_list = user_role.GetList("id asc", new List<Expression<Func<sys_user_role_relation_entity, bool>>>() {
                        b=>b.user_id==a.id
                    });
                    users.Add(new
                    {
                        a.id,
                        a.account,
                        a.password,
                        a.alias_name,
                        a.chr_name,
                        a.email,
                        a.user_status,
                        a.last_login_time,
                        a.last_login_ip,
                        a.login_count,
                        a.mobile_phone,

                        roles = userrole_list.Select(a => a.role_id)
                    });
                });
            }

            return new RestfulPageData<object>(0, "ok", users, result.total);
        }
    }
}
