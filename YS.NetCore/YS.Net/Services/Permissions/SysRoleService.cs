

using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysRoleService : ServiceBase<sys_role_entity, Microsoft.Extensions.Logging.ILogger<sys_role_entity>>, ISysRoleService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysRoleService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
                              IRepository<sys_role_entity> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_role_entity> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
