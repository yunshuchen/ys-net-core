

using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysResourceService : ServiceBase<sys_resource_entity, Microsoft.Extensions.Logging.ILogger<sys_resource_entity>>, ISysResourceService
    {
        private const string Sys_Res_Cache_Key = SessionKeyProvider.SysResourceCacheKey;
        private readonly ConcurrentDictionary<string, sys_resource_entity> sys_res_cache;
        IHttpContextAccessor httpContextAccessor;
        public SysResourceService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            ISkyCacheManager<ConcurrentDictionary<string, sys_resource_entity>> cacheManager,
            IRepository<sys_resource_entity> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_resource_entity> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;

            sys_res_cache = cacheManager.GetOrAdd(Sys_Res_Cache_Key, new ConcurrentDictionary<string, sys_resource_entity>());
        }

        public void ReloadCahce()
        {
            var list = base.GetList("", new List<Expression<Func<sys_resource_entity, bool>>>() { });
            sys_res_cache.Clear();
            list.ForEach(a =>
            {
                sys_res_cache.TryUpdate(a.res_code.ToLower(), (sys_resource_entity value11) => { return a; });
            });

        }
    }
}
