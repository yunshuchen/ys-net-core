﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.Mvc;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISysAccountService : IService<sys_account>
    {
        sys_account Get(string OrderBy, Expression<Func<sys_account, bool>> predicate);

        int Modify(Expression<Func<sys_account, bool>> condition, System.Linq.Expressions.Expression<Func<sys_account, sys_account>> content);
        sys_account Add(sys_account item);

        System.Collections.Generic.List<sys_account> GetList(string keySelector,
            List<System.Linq.Expressions.Expression<Func<sys_account, bool>>> predicate);
        /// <summary>
        /// 分页通用函数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="iPage"></param>
        /// <param name="PageSize"></param>
        /// <param name="predicate"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        PageOf<sys_account> GetPageList(int iPage, int PageSize, string keySelector, List<Expression<Func<sys_account, bool>>> predicate);

        /// <summary>
        /// OAuth Code2Login
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        object UserLogin(string account);
        object UserLogin(string account, string pwd, string CacheName, string validate_key, string safecode, bool DisableSafeCode = false);
        object UserLogin(string account, string pwd, string safecode, bool DisableSafeCode = false);


        /// <summary>
        /// 用户修改密码
        /// </summary>
        /// <param name="account">账号</param>
        /// <param name="oldPwd">旧密码</param>
        /// <param name="new_pwd">新密码</param>
        /// <param name="safecode">验证码</param>
        /// <returns></returns>
        object UserModifyPwd(string account, string oldPwd, string new_pwd, string safecode, bool DisableSafeCode = false);
        RestfulPageData<object> GetUserRoleList(int iPage, int PageSize, string keySelector, List<Expression<Func<sys_account, bool>>> predicate);

    }
}
