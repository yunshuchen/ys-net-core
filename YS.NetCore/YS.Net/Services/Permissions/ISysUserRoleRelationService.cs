using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;


namespace YS.Net.Services
{
    public interface ISysUserRoleRelationService : IService<sys_user_role_relation_entity>
    {
        public List<sys_user_role_relation_entity> GetUserRoles(string userName);


 
    }

}
