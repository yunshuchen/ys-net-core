﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysUserPwdLogService : ServiceBase<sys_user_pwd_log, Microsoft.Extensions.Logging.ILogger<sys_user_pwd_log>>, ISysUserPwdLogService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysUserPwdLogService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor
            ,IRepository<sys_user_pwd_log> _useService
            ,Microsoft.Extensions.Logging.ILogger<sys_user_pwd_log> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
