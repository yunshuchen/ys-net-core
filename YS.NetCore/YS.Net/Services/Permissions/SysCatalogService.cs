﻿
using YS.Utils.DI;
using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysCatalogService : ServiceBase<sys_catalog_entity, Microsoft.Extensions.Logging.ILogger<sys_catalog_entity>>, ISysCatalogService
    {

        IHttpContextAccessor httpContextAccessor;
        public SysCatalogService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            IRepository<sys_catalog_entity> _useService,
            Microsoft.Extensions.Logging.ILogger<sys_catalog_entity> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }
    }
}
