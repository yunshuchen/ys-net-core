using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;


namespace YS.Net.Services
{
    public interface ISysRoleOperateRelationService : IService<sys_role_operate_relation_entity>
    {
        public List<sys_role_operate_relation_entity> GetRoleOperateList(string keySelector, List<Expression<Func<sys_role_operate_relation_entity, bool>>> predicate);
    }
}
