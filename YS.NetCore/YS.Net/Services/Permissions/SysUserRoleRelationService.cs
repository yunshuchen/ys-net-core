

using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SysUserRoleRelationService : ServiceBase<sys_user_role_relation_entity, Microsoft.Extensions.Logging.ILogger<sys_user_role_relation_entity>>, ISysUserRoleRelationService
    {
        //用户角色关系缓存
        private ConcurrentDictionary<string, List<sys_user_role_relation_entity>> user_role_cahce;
        private const string SysUserRoleNodes = Net.SessionKeyProvider.SysUserRoleRelation;
        IHttpContextAccessor httpContextAccessor;
        public SysUserRoleRelationService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            Microsoft.Extensions.Logging.ILogger<sys_user_role_relation_entity> _logger
             ,IRepository<sys_user_role_relation_entity> _useService
            , ISkyCacheManager<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>> cacheManager)
            : base(_useService, _logger)
        {
             user_role_cahce = cacheManager.GetOrAdd(SysUserRoleNodes, new ConcurrentDictionary<string, List<sys_user_role_relation_entity>>());
            httpContextAccessor = _httpContextAccessor;
        }

        public List<sys_user_role_relation_entity> GetUserRoles(string user_name)
        {
            List<sys_user_role_relation_entity> result = null;


            user_role_cahce.TryGetValue(user_name, out result);
            return result;
        }
    }
}
