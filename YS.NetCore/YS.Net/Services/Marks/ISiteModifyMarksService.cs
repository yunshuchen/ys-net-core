﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace YS.Net.Services
{
    public interface ISiteModifyMarksService : IService<site_modify_marks>
    {
        object GetNodeModifyMarkPage(int iPage, int PageSize, List<Expression<Func<site_modify_marks, bool>>> predicate, string node_name, string lang = "tc");


        object GetContentModifyMarkPage(int iPage, int PageSize, List<Expression<Func<site_modify_marks, bool>>> predicate,string chr_title, string lang = "tc");
    }
}
