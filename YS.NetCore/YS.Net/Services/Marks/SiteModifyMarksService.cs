﻿using YS.Net.Models;
using YS.Utils;
using YS.Net;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YS.Utils.DI;
using YS.Utils.Repositorys;
using System.Linq.Dynamic.Core;

namespace YS.Net.Services
{
    [ScopedDI]
    public class SiteModifyMarksService : ServiceBase<site_modify_marks, Microsoft.Extensions.Logging.ILogger<site_modify_marks>>, ISiteModifyMarksService
    {

        IHttpContextAccessor httpContextAccessor;
        public SiteModifyMarksService(IApplicationContext applicationContext,
            IHttpContextAccessor _httpContextAccessor,
            IRepository<site_modify_marks> _useService,
            Microsoft.Extensions.Logging.ILogger<site_modify_marks> _logger)
            : base(_useService, _logger)
        {
            httpContextAccessor = _httpContextAccessor;
        }

        public object GetContentModifyMarkPage(int iPage, int PageSize, List<Expression<Func<site_modify_marks, bool>>> predicate, string chr_title, string lang = "tc")
        {
            var query = from mm in Table
                        join cm in (from icm in OtherTable<site_content_marks>()
                                    where icm.data_type == 1
                                    select new { icm.data_id, icm.mark_id, icm.lang })
                        on mm.id equals cm.mark_id into b_join
                        from cm in b_join.DefaultIfEmpty()
                        join c in OtherTable<site_content>()
                        on new { cm.data_id, cm.lang } equals new { data_id = c.id, c.lang } into temp1
                        from u in temp1.DefaultIfEmpty()
                        orderby mm.create_time descending
                        select new
                        {
                            mm.id,
                            mm.chr_type,
                            mm.create_time,
                            mm.create_id,
                            mm.create_name,
                            mm.operate_ip,
                            u.title,
                            u.node_code
                        };
            var result = query.PageResult(iPage, PageSize);


            return new
            {
                list = result.Queryable.ToList(),
                page_index = result.CurrentPage,
                page_size = result.PageSize,
                total = result.RowCount
            };


           

        }

        public object GetNodeModifyMarkPage(int iPage, int PageSize, List<Expression<Func<site_modify_marks, bool>>> predicate, string node_name, string lang = "tc")
        {

            var query = from mm in Table
                        join cm in (from icm in OtherTable<site_node_marks>()
                                    where icm.data_type == 1
                                    select new { icm.data_id, icm.mark_id, icm.lang,icm.node_name,icm.node_code })
                        on mm.id equals cm.mark_id into b_join
                        from cm in b_join.DefaultIfEmpty()
                        
                        orderby mm.create_time descending
                        select new
                        {
                            mm.id,
                            mm.chr_type,
                            mm.create_time,
                            mm.create_id,
                            mm.create_name,
                            mm.operate_ip,
                            cm.node_name,
                            cm.node_code
                        };
            var result = query.PageResult(iPage, PageSize);


            return new
            {
                list = result.Queryable.ToList(),
                page_index = result.CurrentPage,
                page_size = result.PageSize,
                total = result.RowCount
            };


        }
    }
}
