﻿using YS.Net.Models;
using YS.Utils;
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using YS.Utils.DI;
namespace YS.Net.Services
{

    [ScopedDI]
    public interface ISiteMemberTagService : IService<site_member_tag>
    {

    }
}
