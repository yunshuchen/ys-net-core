﻿

using YS.Net.Common;
using YS.Net.Models;
using YS.Net.MyLucene.Models;
using YS.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using YS.Utils.DI;

namespace YS.Net.MyLucene
{
    [SingletonDI]
    public class SearchService : ISearchService
    {
        private readonly  Index tc_index;

        private readonly Index sc_index;

        private readonly Index en_index;
        public SearchService(ISiteFileProvider site_provider)
        {
            string tc_path = System.IO.Path.Combine(site_provider.lucene_index, "tc");
            string sc_path = System.IO.Path.Combine(site_provider.lucene_index, "sc");
            string en_path = System.IO.Path.Combine(site_provider.lucene_index, "en");
            tc_index = new Index(tc_path);
            sc_index = new Index(sc_path);
            en_index = new Index(en_path);
        }
        private Index GetIndex(string lang)
        {
            switch (lang)
            {
                case "tc":
                    return tc_index;
                case "sc":
                    return sc_index;
                case "en":
                    return en_index;
                default:
                    break;
            }
            throw new Exception("语言代号错误");
        }

        public void InitBuildIndex(string lang, List<LuceneModel> User)
        {
            GetIndex(lang).Build(User);
        }
        public void UpdateIndex(string lang, LuceneModel User)
        {
            GetIndex(lang).UpdateIndex(User);
        }
        public SearchResponse Search(string lang, SearchRequest request) => GetIndex(lang).Search(request.iPage, request.PageSize, request.Query, request.Order, request.Begin, request.End, request.node_code);

        public void RemoveIndexAll(string lang)
        {
            GetIndex(lang).RemoveIndex();
        }

        public void InsertNodeIndex(string node_code, site_node node)
        {
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Node,
                chr_content = node.node_desc,
                code = node.node_code,
                node_code = node_code,
                desc = node.node_remark,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_list,
                publish_date = node.create_time,
                title = node.node_name
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void UpdateNodeIndex(string node_code, site_node node)
        {
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Node,
                chr_content = node.node_desc,
                code = node.node_code,
                node_code = node_code,
                desc = node.node_remark,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_list,
                publish_date = node.create_time,
                title = node.node_name
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void InsertContentIndex(string node_code, site_content node)
        {
            DateTime publish_date = DateTime.Now;
            DateTime.TryParse(node.publish_date, out publish_date);
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Content,
                chr_content = node.chr_content,
                code = node.node_code,
                node_code = node_code,
                desc = node.summary,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_file,
                publish_date = publish_date,
                title = node.title
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void UpdateContentIndex(string node_code, site_content node)
        {
            DateTime publish_date = DateTime.Now;
            DateTime.TryParse(node.publish_date, out publish_date);
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Content,
                chr_content = node.chr_content,
                code = node.node_code,
                node_code = node_code,
                desc = node.summary,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_file,
                publish_date = publish_date,
                title = node.title
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void InsertDocIndex(site_doc node)
        {
            DateTime publish_date = DateTime.Now;
            DateTime.TryParse(node.date, out publish_date);
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Doc,
                chr_content = node.chr_content,
                code = node.module,
                node_code = node.node_code,
                desc = node.chr_desc,
                id = node.id,
                row_key = node.row_key,
                file_path = node.file_link,
                publish_date = publish_date,
                title = node.name
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void UpdateDocIndex(site_doc node)
        {
            DateTime publish_date = DateTime.Now;
            DateTime.TryParse(node.date, out publish_date);
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Doc,
                chr_content = node.chr_content,
                code = node.module,
                node_code = node.node_code,
                desc = node.chr_desc,
                id = node.id,
                row_key = node.row_key,
                file_path = node.file_link,
                publish_date = publish_date,
                title = node.name
            };
            GetIndex(node.lang).UpdateIndex(model);
        }

        public void DeleteNodeIndex(site_node node)
        {
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Node,
                chr_content = node.node_desc,
                code = node.node_code,
                desc = node.node_remark,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_list,
                publish_date = node.create_time,
                title = node.node_name
            };

            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_tc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_sc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_en"));
        }
        public void DeleteContentIndex(site_content node)
        {
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Content,
                chr_content = node.chr_content,
                code = node.node_code,
                desc = node.summary,
                id = node.id,
                row_key = node.row_key,
                file_path = node.down_file,
                title = node.title
            };
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_tc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_sc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_en"));
        }
        public void DeleteDocIndex(site_doc node)
        {
            LuceneModel model = new LuceneModel()
            {
                lang = node.lang,
                chr_type = LuceneContentType.Doc,
                chr_content = node.chr_content,
                code = node.module,
                node_code = node.node_code,
                desc = node.chr_desc,
                id = node.id,
                row_key = node.row_key,
                file_path = node.file_link,
                title = node.name
            };
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_tc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_sc"));
            GetIndex(node.lang).RemoveIndex(string.Concat(model.chr_type.ToString(), "_", model.id, "_en"));
        }

    }
}
