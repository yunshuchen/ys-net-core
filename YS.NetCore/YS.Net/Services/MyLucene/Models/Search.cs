﻿
using YS.Net.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.MyLucene.Models
{
    public class SearchRequest
    {
        public string Query { get; set; }

        public int iPage { get; set; }
        public int PageSize { get; set; }

        public string Order { get; set; }

        public string Begin { get; set; }

        public string End { get; set; }

        public string node_code { get; set; }
    }

    public class SearchResponse
    {
        public SearchResponse() => datas = new List<LuceneModel>();
        public int TotalCount { get; set; }
        public List<LuceneModel> datas { get; set; }

        public int iPage { get; set; }
        public int PageSize { get; set; }

        public long usage_ms { get; set; }

        public int PageCount
        {
            get
            {
                if (TotalCount % PageSize != 0)
                {
                    return (int)(TotalCount / PageSize) + 1;
                }
                else
                {
                    return (int)(TotalCount / PageSize);
                }
            }
        }
    }
}
