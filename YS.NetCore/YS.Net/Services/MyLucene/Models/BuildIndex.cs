﻿
using YS.Net.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.MyLucene.Models
{
    public class BuildIndexRequest
    {
        public IEnumerable<LuceneModel> Users;
    }
}
