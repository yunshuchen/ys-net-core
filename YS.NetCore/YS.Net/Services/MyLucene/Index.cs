﻿using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Core;
using Lucene.Net.Analysis.Miscellaneous;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Lucene.Net.Util;
using System;
using System.Collections.Generic;
using jieba.NET;
using JiebaNet.Segmenter;
using Lucene.Net.QueryParsers.Classic;
using Lucene.Net.Search.Highlight;
using System.IO;
using YS.Net.Models;
using YS.Net.MyLucene.Models;
using System.Diagnostics;

namespace YS.Net.MyLucene
{
    internal class Index : IDisposable
    {
        private const LuceneVersion MATCH_LUCENE_VERSION = LuceneVersion.LUCENE_48;
        private const int SNIPPET_LENGTH = 100;
        private readonly IndexWriter writer;
        private readonly Analyzer analyzer;
        private readonly QueryParser queryParser;
        private readonly SearcherManager searchManager;

        public Index(string indexPath)
        {
            analyzer = SetupAnalyzer();


            queryParser = SetupQueryParser(analyzer);
            writer = new IndexWriter(FSDirectory.Open(indexPath), new IndexWriterConfig(MATCH_LUCENE_VERSION, analyzer));

            searchManager = new SearcherManager(writer, true, null);
        }

        private Analyzer SetupAnalyzer()
        {
            return (Analyzer)new JieBaAnalyzer(TokenizerMode.Search);
        }

        private QueryParser SetupQueryParser(Analyzer analyzer)
        {
            return new MultiFieldQueryParser(
                MATCH_LUCENE_VERSION,
                SearchFields.ToArray(),
                analyzer
            );
        }

        public void Build(IEnumerable<LuceneModel> users)
        {

            foreach (var user in users)
            {
                try
                {
                    if (user != null)
                        writer.UpdateDocument(new Term("identity_key", user.identity_key.ToString()), BuildDocument(user));
                }
                catch (Exception)
                {

                }

            }
            writer.Flush(true, true);
            writer.Commit();
        }

        public void ModifyIndex(LuceneModel user)
        {
            try
            {
                if (user != null)
                    writer.DeleteDocuments(new Term("identity_key", user.identity_key.ToString()));
                writer.UpdateDocument(new Term("identity_key", user.identity_key.ToString()), BuildDocument(user));
            }
            catch (Exception)
            {

            }

            writer.Flush(true, true);
            writer.Commit();
        }
        public void UpdateIndex(LuceneModel user)
        {
            try
            {
                if (user != null)
                    writer.UpdateDocument(new Term("identity_key", user.identity_key.ToString()), BuildDocument(user));
            }
            catch (Exception)
            {

            }

            writer.Flush(true, true);
            writer.Commit();

        }
        public void RemoveIndex(string row_key)
        {

            try
            {
                writer.DeleteDocuments(new Term("identity_key", row_key.ToString()));
            }
            catch (Exception)
            {
            }

            writer.Flush(true, true);
            writer.Commit();
        }
        public void RemoveIndex()
        {

            try
            {
                writer.DeleteAll();
            }
            catch (Exception)
            {
            }

            writer.Flush(true, true);
            writer.Commit();
        }


        private Document BuildDocument(LuceneModel user)
        {


            DateTime Now = DateTime.Now;

            DateTime.TryParse(user.publish_date.MyToString(), out Now);

            string publish_date = Now.MyToDateTime();
            int publish_date_format = Now.MyToDateTime("yyyyMMdd").MyToInt();
            Document doc = new Document
            {
                new StringField("identity_key", user.identity_key.MyToString(),Field.Store.YES),
                new StringField("row_key", user.row_key.MyToString(),Field.Store.YES),
                new StringField("id", user.id.MyToString(),Field.Store.YES),
                new StringField("chr_type", ((int)user.chr_type).MyToString(), Field.Store.YES),
                new StringField("code", user.code.MyToString(), Field.Store.YES),
                new StringField("node_code", user.node_code.MyToString(), Field.Store.YES),
                new StringField("link", user.link.MyToString(), Field.Store.YES),
                new StringField("lang", user.lang.MyToString(), Field.Store.YES),
                new StringField("title", user.title.MyToString(), Field.Store.YES),
                new StringField("desc", user.desc.MyToString(), Field.Store.YES),
                new TextField("chr_content", user.chr_content.MyToString(), Field.Store.YES),
                new StringField("file_path", user.file_path.MyToString(), Field.Store.YES),
                new StringField("publish_date", publish_date, Field.Store.YES),
                new StringField("publish_date_format", publish_date_format.ToString(), Field.Store.YES)
            };
            return doc;
        }

        public SearchResponse Search(int iPage, int PageSize, string queryString, string order = "auto", string begin = "", string end = "", string node_code = "")
        {
            Stopwatch myWatch = new Stopwatch();
            myWatch.Start();
            if (string.IsNullOrEmpty(queryString.Replace("*", "").Replace("?", "")))
            {
                return new SearchResponse() { TotalCount = 0, datas = new List<LuceneModel>() };
            }

            TopScoreDocCollector collector = TopScoreDocCollector.Create(iPage * PageSize, false);
            Query query = BuildQuery(queryString);
            if (begin.MyToString() != "" && end.MyToString() != "")
            {

                BytesRef bLeft = new BytesRef(begin);

                BytesRef eLeft = new BytesRef(end);
                TermRangeQuery querybetween = new TermRangeQuery("publish_date_format", bLeft, eLeft, true, true);
                (query as BooleanQuery).Add(querybetween, Occur.MUST);


            }
            if (node_code.MyToString() != "")
            {
                TermQuery tn = new TermQuery(new Term("node_code", node_code));
                (query as BooleanQuery).Add(tn, Occur.MUST);
            }
            TermQuery tq = new TermQuery(new Term("chr_type", ((int)LuceneContentType.Content).MyToString()));
            (query as BooleanQuery).Add(tq, Occur.MUST);
            //配置高亮器
            //以红色字体标记关键词
            SimpleHTMLFormatter formatter = new SimpleHTMLFormatter("<font color='red'>", "</font>");
            QueryScorer scorer = new QueryScorer(query);
            //创建一个高亮器
            Highlighter highlighter = new Highlighter(formatter, scorer);
            //设置文本摘要大小
            SimpleFragmenter fragmenter = new SimpleFragmenter(100);
            highlighter.TextFragmenter = fragmenter;
            searchManager.MaybeRefreshBlocking();
            IndexSearcher searcher = searchManager.Acquire();
            try
            {   //按发布时间降序
                Sort sort = new Sort(SortField.FIELD_SCORE);
                if (order.MyToString() != "auto")
                {
                    //按发布时间降序
                    sort = new Sort(new SortField("publish_date_format", SortFieldType.INT32, true), SortField.FIELD_SCORE);
                }
                TopDocs docs = searcher.Search(query, null, 10000, sort);
                //searcher.Search(query, collector);
                SearchResponse sr = CompileResults(iPage, PageSize, searcher, collector, docs, highlighter);
                myWatch.Stop();
                long myUseTime = myWatch.ElapsedMilliseconds;

                sr.usage_ms = myUseTime;

                return sr;

            }
            finally
            {
                searchManager.Release(searcher);
                searcher = null;
            }
        }

        private SearchResponse CompileResults(int iPage
            , int PageSize
            , IndexSearcher searcher
            , TopScoreDocCollector collector
            , TopDocs docs
            , Highlighter highlighter)
        {
            int hits_count = docs.TotalHits;// collector.TotalHits;
            var searchResults = new SearchResponse() { TotalCount = hits_count };

            searchResults.PageSize = PageSize;
            searchResults.iPage = iPage;

            if (hits_count <= 0)
            {
                searchResults.datas = new List<LuceneModel>();
                return searchResults;
            }



            int startIndex = (iPage - 1) * PageSize;
            int endIndex = iPage * PageSize;

            for (int i = startIndex; i < endIndex && i < hits_count; i++)
            {
                Document document = searcher.Doc(docs.ScoreDocs[i].Doc);
                long row_key = document.GetField("row_key").GetStringValue().MyToString().MyToLong();
                long id = document.GetField("id").GetStringValue().MyToString().MyToLong();

                LuceneContentType chr_type = (LuceneContentType)document.GetField("chr_type")?.GetStringValue().MyToInt();
                string code = document.GetField("code")?.GetStringValue().MyToString();
                string node_code = document.GetField("node_code")?.GetStringValue().MyToString();
                string link = document.GetField("link")?.GetStringValue().MyToString();

                string lang = document.GetField("lang")?.GetStringValue().MyToString();
                string temp_title = document.GetField("title")?.GetStringValue().MyToString();
                TokenStream tokenStream = analyzer.GetTokenStream("title", new StringReader(temp_title));
                string highLightText = highlighter.GetBestFragment(tokenStream, temp_title);

                string title = highlighter.GetBestFragment(analyzer, "title", temp_title);
                if (title.MyToString() == "")
                {
                    title = temp_title;
                }
                string desc = document.GetField("desc")?.GetStringValue().MyToString();

                string chr_content = document.GetField("chr_content").GetStringValue().MyToString();
                string file_path = document.GetField("file_path").GetStringValue().MyToString();

                DateTime publish_date = DateTime.Now;
                DateTime.TryParse(document.GetField("publish_date").GetStringValue().MyToString(), out publish_date);


                var searchResult = new LuceneModel()
                {
                    id = id,
                    row_key = row_key,
                    chr_type = chr_type,
                    code = code,
                    node_code = node_code,
                    lang = lang,
                    title = title,
                    desc = desc,
                    chr_content = chr_content,
                    file_path = file_path,
                    publish_date = publish_date,
                    link = link
                };
                searchResults.datas.Add(searchResult);

            }

            //ScoreDoc[] hits = collector.GetTopDocs(start, limit).ScoreDocs;

            //foreach (var result in hits)
            //{


            //    var document = searcher.Doc(result.Doc);
            //    long row_key = document.GetField("row_key").GetStringValue().MyToString().MyToLong();
            //    long id = document.GetField("id").GetStringValue().MyToString().MyToLong();

            //    LuceneContentType chr_type = (LuceneContentType)document.GetField("chr_type")?.GetStringValue().MyToInt();
            //    string code = document.GetField("code")?.GetStringValue().MyToString();
            //    string node_code = document.GetField("node_code")?.GetStringValue().MyToString();
            //    string link = document.GetField("link")?.GetStringValue().MyToString();

            //    string lang = document.GetField("lang")?.GetStringValue().MyToString();
            //    string temp_title = document.GetField("title")?.GetStringValue().MyToString();
            //    TokenStream tokenStream = analyzer.GetTokenStream("title", new StringReader(temp_title));
            //    string highLightText = highlighter.GetBestFragment(tokenStream, temp_title);

            //    string title = highlighter.GetBestFragment(analyzer, "title", temp_title);
            //    if (title.MyToString() == "")
            //    {
            //        title = temp_title;
            //    }
            //    string desc = document.GetField("desc")?.GetStringValue().MyToString();

            //    string chr_content = document.GetField("chr_content").GetStringValue().MyToString();
            //    string file_path = document.GetField("file_path").GetStringValue().MyToString();

            //    DateTime publish_date = DateTime.Now;
            //    DateTime.TryParse(document.GetField("publish_date").GetStringValue().MyToString(), out publish_date);


            //    var searchResult = new LuceneModel()
            //    {
            //        id = id,
            //        row_key = row_key,
            //        chr_type = chr_type,
            //        code = code,
            //        node_code = node_code,
            //        lang = lang,
            //        title = title,
            //        desc = desc,
            //        chr_content = chr_content,
            //        file_path = file_path,
            //        publish_date = publish_date,
            //        link = link
            //    };
            //    searchResults.datas.Add(searchResult);
            //}
            return searchResults;
        }
        private SearchResponse CompileResults(int iPage
            , int PageSize
            , IndexSearcher searcher
            , TopDocs topdDocs
            , Highlighter highlighter)
        {
            var searchResults = new SearchResponse() { TotalCount = topdDocs.TotalHits };


            int start = PageSize * (iPage - 1);
            int end = iPage * PageSize;
            end = topdDocs.TotalHits < end ? topdDocs.TotalHits : end;


            searchResults.PageSize = PageSize;
            searchResults.iPage = iPage;


            //foreach (var result in topdDocs.ScoreDocs)
            //{
            for (var i = start; i < end; i++)
            {
                var result = topdDocs.ScoreDocs[i];
                var document = searcher.Doc(result.Doc);
                long row_key = document.GetField("row_key").GetStringValue().MyToString().MyToLong();
                long id = document.GetField("id").GetStringValue().MyToString().MyToLong();

                LuceneContentType chr_type = (LuceneContentType)document.GetField("chr_type")?.GetStringValue().MyToInt();
                string code = document.GetField("code")?.GetStringValue().MyToString();
                string node_code = document.GetField("node_code")?.GetStringValue().MyToString();

                string link = document.GetField("link")?.GetStringValue().MyToString();

                string lang = document.GetField("lang")?.GetStringValue().MyToString();
                string title = document.GetField("title")?.GetStringValue().MyToString();
                title = highlighter.GetBestFragment(analyzer, "title", title);

                string desc = document.GetField("desc")?.GetStringValue().MyToString();

                string chr_content = document.GetField("chr_content").GetStringValue().MyToString();
                string file_path = document.GetField("file_path").GetStringValue().MyToString();

                DateTime publish_date = DateTime.Now;
                DateTime.TryParse(document.GetField("publish_date").GetStringValue().MyToString(), out publish_date);


                var searchResult = new LuceneModel()
                {
                    id = id,
                    row_key = row_key,
                    chr_type = chr_type,
                    code = code,
                    node_code = node_code,
                    lang = lang,
                    title = title,
                    desc = desc,
                    chr_content = chr_content,
                    file_path = file_path,
                    publish_date = publish_date,
                    link = link
                };
                searchResults.datas.Add(searchResult);
            }
            return searchResults;
        }


        public List<string> SearchFields = new List<string>() {
                        "title",
                        "chr_content",
                        "desc",
                        "file_path",
                        "link"
        };
        private Query BuildQuery(string queryString)
        {
            /*
            *  var segmenter = new JiebaSegmenter();
               var segments = segmenter.Cut("我来到北京清华大学", cutAll: true);
               var resultWords = new List<string> {"我", "来到", "北京", "清华", "清华大学", "华大", "大学"};
               Compared(segments, resultWords);

               segments = segmenter.Cut("我来到北京清华大学"); 
               resultWords = new List<string> { "我","来到", "北京", "清华大学"};
               Compared(segments, resultWords);
              
               segments = segmenter.Cut("他来到了网易杭研大厦");  // 默认为精确模式，同时也使用HMM模型
               resultWords = new List<string> {"他", "来到", "了", "网易", "杭研", "大厦"};
               Compared(segments, resultWords);
              
               segments = segmenter.CutForSearch("小明硕士毕业于中国科学院计算所，后在日本京都大学深造"); // 搜索引擎模式
               resultWords = new List<string> {"小明","硕士" ,"毕业","于","中国" ,"科学","学院", "科学院" ,"中国科学院","计算", "计算所","，" , "后"
                   ,"在" ,"日本","京都" ,"大学", "日本京都大学" ,"深造"};
               Compared(segments, resultWords);
              
               segments = segmenter.Cut("结过婚的和尚未结过婚的");
               resultWords = new List<string> {"结过婚","的" ,"和" ,"尚未" ,"结过婚","的"};
              
               Compared(segments, resultWords);
              
               segments = segmenter.Cut("快奔三", false, false);
               resultWords = new List<string> {"快","奔三"};
            * */
            var booleanQuery = new BooleanQuery();
            //分词查询
            var segmenter = new JiebaSegmenter();
            var segments = segmenter.Cut(queryString.Trim());
            foreach (var seachField in SearchFields)
            {
                Query base_query = new WildcardQuery(new Term(seachField, "*" + queryString.Trim() + "*"));
                booleanQuery.Add(base_query, Occur.SHOULD);
                foreach (string word in segments)
                {
                    Query query = new WildcardQuery(new Term(seachField, "*" + word + "*"));
                    booleanQuery.Add(query, Occur.SHOULD);
                }
            }

            //不模糊分词查询
            //foreach (var seachField in SearchFields)
            //{
            //    Query query = new WildcardQuery(new Term(seachField, "*" + queryString.Trim() + "*"));
            //    booleanQuery.Add(query, Occur.SHOULD);
            //}
            // booleanQuery.Add(new WildcardQuery(new Term("is_show", "1")), Occur.MUST);

            //不分词查询
            //Lucene.Net.QueryParsers.Analyzing.AnalyzingQueryParser parser = new Lucene.Net.QueryParsers.Analyzing.AnalyzingQueryParser(MATCH_LUCENE_VERSION, "title", analyzer);
            //booleanQuery.Add(parser.Parse(queryString), Occur.SHOULD);

            //parser = new Lucene.Net.QueryParsers.Analyzing.AnalyzingQueryParser(MATCH_LUCENE_VERSION, "chr_content", analyzer);
            //booleanQuery.Add(parser.Parse(queryString), Occur.SHOULD);

            //parser = new Lucene.Net.QueryParsers.Analyzing.AnalyzingQueryParser(MATCH_LUCENE_VERSION, "desc", analyzer);
            //booleanQuery.Add(parser.Parse(queryString), Occur.SHOULD);

            return booleanQuery;
            //return queryParser.Parse(Sanitize(queryString));
        }

        private string Sanitize(string queryString)
        {
            string[] removed = { "*", "?", "%", "+" };
            string[] spaces = { "-" };
            foreach (var r in removed)
            {
                queryString = queryString.Replace(r, string.Empty);
            }
            foreach (var s in spaces)
            {
                queryString = queryString.Replace(s, " ");
            }
            return queryString;
        }

        public void Dispose()
        {
            searchManager?.Dispose();
            analyzer?.Dispose();
            writer?.Dispose();

        }
    }
}
