﻿
using YS.Net.Models;
using YS.Net.MyLucene.Models;
using System;
using System.Collections.Generic;
using System.Text;


namespace YS.Net.MyLucene
{
    public interface ISearchService
    {
        void InitBuildIndex(string lang, List<LuceneModel> User);

        void UpdateIndex(string lang, LuceneModel User);



        void RemoveIndexAll(string lang);
        SearchResponse Search(string lang, SearchRequest request);



        /// <summary>
        /// 初始化栏目索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void InsertNodeIndex(string node_code,site_node node);
        /// <summary>
        /// 更新栏目索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void UpdateNodeIndex(string node_code, site_node node);

        void DeleteNodeIndex(site_node node);
        /// <summary>
        /// 初始化栏目内容索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void InsertContentIndex(string node_code, site_content node);
        /// <summary>
        /// 更新栏目内容索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void UpdateContentIndex(string node_code, site_content node);

        void DeleteContentIndex(site_content node);

        /// <summary>
        /// 初始化栏目文档索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void InsertDocIndex(site_doc node);
        /// <summary>
        /// 更新栏目文档索引
        /// </summary>
        /// <param name="node_code"></param>
        /// <param name="node"></param>
        void UpdateDocIndex(site_doc node);


        void DeleteDocIndex(site_doc node);
   
    }
}
