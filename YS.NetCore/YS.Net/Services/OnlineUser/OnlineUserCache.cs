﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using YS.Net;
using YS.Utils;
using YS.Net.Models;
using YS.Utils.Cache;
using YS.Utils.DI;
namespace YS.Net.Services
{
    [ScopedDI]
    public class OnlineUserCache : IOnlineUserCache
    {
        private readonly ConcurrentDictionary<string, sys_account> online_User;
        public OnlineUserCache(ISkyCacheManager<ConcurrentDictionary<string, sys_account>> cacheManager)
        {
            online_User = cacheManager.GetOrAdd(SessionKeyProvider.OnlineUserCacheKey, new ConcurrentDictionary<string, sys_account>());
        }
        public void AddOnlineUser(string token, sys_account model)
        {
            online_User.TryUpdate(token, (sys_account value11) => { return model; });
        }

        public sys_account GetOnlineUser(string token)
        {
            sys_account user = new sys_account() { id = 0 };
            online_User.TryGetValue(token,out user);
            return user ?? new sys_account() { id = 0, token = "", last_refresh = DateTime.MinValue, put_out_login = DateTime.MinValue };

        }

        public void RemoveOnlineUser(string token)
        {
            sys_account user = new sys_account() { id = 0 };
            online_User.TryRemove(token, out user);
        }

        public void UpdateOnlineUser(string token, sys_account model)
        {
            online_User.TryUpdate(token, (sys_account value11) => { return model; });
        }

        public List<string> GetKeysByUserId(long user_id)
        {
            var keys = online_User.Keys;
            var resutlt = keys.Where(a => a.EndsWith(string.Format("#{0}#", user_id))).ToList();
            return resutlt;
        }

        public void RemoveTimeOutCache()
        {
            DateTime Now = DateTime.Now;
            var keys = online_User.Values.ToList();
            var resutlt = keys.Where(a => (Now - a.last_refresh).TotalSeconds >= 300 || (Now - a.put_out_login).TotalSeconds >= 120).Select(a => string.Format("#{0}#{1}#", a.token, a.id)).ToList();
            foreach (var key in resutlt)
            {
                RemoveOnlineUser(key);
            }
        }
      
    }
}
