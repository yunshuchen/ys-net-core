﻿using YS.Net.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Services
{
    public interface IOnlineUserCache
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token">#token#user_id#</param>
        /// <param name="model"></param>
        void AddOnlineUser(string token, sys_account model);


        sys_account GetOnlineUser(string token);


        void UpdateOnlineUser(string token, sys_account model);


        void RemoveOnlineUser(string token);


        List<string> GetKeysByUserId(long user_id);


        void RemoveTimeOutCache();
    }
}
