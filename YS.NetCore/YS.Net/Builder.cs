﻿using YS.Utils;
using YS.Utils.Extend;
using YS.Utils.Mvc.Plugin;
using YS.Utils.Mvc.Resource;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using YS.Net.Route;
using YS.Utils.RazorPages;
using YS.Net.Services;
using YS.Net.Mvc.StateProviders;
using YS.Net.Models;
using System.Threading.Tasks;
using YS.Net.Mvc.Authorize;
using MediatR;
using YS.Net.BackgroundService.Queue;
using YS.Net.BackgroundService.Worker;
using YS.Net.BackgroundService;
using Quartz.Spi;
using YS.Net.Quartz;
using Quartz;
using Quartz.Impl;
using YS.Utils.Repositorys;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using System.IO;
using Microsoft.Extensions.Logging;
using YS.Utils.Repositorys.CustomTranslators.SqlServer;
using Microsoft.EntityFrameworkCore.Infrastructure;
using YS.Utils.Repositorys.CustomTranslators.MySql;
using YS.Net.Mvc.Middlewares;

namespace YS.Net
{
    public static class Builder
    {


        private static void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            //if (options.SameSite == SameSiteMode.None)
            //{
            //    var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
            //    // TODO: Use your User Agent library of choice here.
            //    if (/* UserAgent doesn't support new behavior */)
            //    {
            //        options.SameSite = SameSiteMode.Unspecified;
            //    }
            //}
        }

        /// <summary>
        /// AddSingleton→AddTransient→AddScoped
        /// AddSingleton的生命周期：项目启动-项目关闭 相当于静态类  只会有一个
        /// AddScoped的生命周期：请求开始-请求结束 在这次请求中获取的对象都是同一个
        /// AddTransient的生命周期：请求获取-（GC回收-主动释放） 每一次获取的对象都不是同一个
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void UseJF(this IServiceCollection services, IConfiguration configuration)
        {


            var config = new ConfigurationBuilder()
     .SetBasePath(Directory.GetCurrentDirectory())
     .AddJsonFile("configs/session_config.json", optional: false, reloadOnChange: false)
     .Build();
            YS.Utils.Options.SessionOptions SessionOp = config.GetValue<YS.Utils.Options.SessionOptions>("SessionProvider");

          //  YS.Utils.Options.SessionOptions SessionOp = configuration.GetSection("SessionProvider").Get<YS.Utils.Options.SessionOptions>();

            double IdleTimeout = 20;

            if (SessionOp == null || SessionOp.SessionType.ToLower() == "memory")
            {
                //添加session
                services.AddDistributedMemoryCache(a => a.SizeLimit = 50000);

            }
            else if (SessionOp.SessionType.ToLower() == "sqlserver")
            {

                /*
                     CREATE TABLE [dbo].[sql_sessions_provider](  
                     [Id] [nvarchar](449) NOT NULL,  
                     [Value] [varbinary](max) NOT NULL,  
                     [ExpiresAtTime] [datetimeoffset](7) NOT NULL,  
                     [SlidingExpirationInSeconds] [bigint] NULL,  
                     [AbsoluteExpiration] [datetimeoffset](7) NULL,  
                      CONSTRAINT [pk_Id] PRIMARY KEY CLUSTERED   
                     (  
                         [Id] ASC  
                     )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]  
                     ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]  

                     GO  
                     CREATE NONCLUSTERED INDEX [Index_ExpiresAtTime] ON [dbo].[sql_sessions_provider]  
                     (  
                         [ExpiresAtTime] ASC  
                     )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
                     GO  

                 */
                services.AddDistributedSqlServerCache(options =>
                {
                    options.ConnectionString = SessionOp.SessionConnectionString;
                    options.SchemaName = "dbo";
                    options.TableName = "sql_sessions_provider";
                });
            }
            else if (SessionOp.SessionType.ToLower() == "redis")
            {
                services.AddDistributedRedisCache(options => {
                    options.Configuration = SessionOp.SessionConnectionString;
                    options.InstanceName = "";
                });

            }
            else
            {
                //添加session
                services.AddDistributedMemoryCache(a=> {
                });
            }

            IdleTimeout = SessionOp?.SessionTimeOut ?? 20;

            //添加session
            var CookieSecure = configuration.GetSection("CookieSecure").Get<bool>();
            services.AddSession(opt =>
            {


                opt.IdleTimeout = TimeSpan.FromMinutes(IdleTimeout);
                opt.Cookie.HttpOnly = true;
                opt.Cookie.IsEssential = true;
                if (CookieSecure)
                {
                    opt.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                }
                else
                {
                    opt.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                }


            });
            services.AddDataProtection();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;       //改为false或者直接注释掉，上面的Session才能正常使用
                options.MinimumSameSitePolicy = SameSiteMode.None;
                if (CookieSecure)
                {
                    options.Secure = CookieSecurePolicy.Always;
                }
                else
                {
                    options.Secure = CookieSecurePolicy.SameAsRequest;
                }
                options.HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always;


            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddScoped<IApplicationContextAccessor, ApplicationContextAccessor>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddSingleton<Net.Mvc.Routing.HomeRouteTCTransformer>();
            services.AddSingleton<Net.Mvc.Routing.HomeRouteSCTransformer>();
            services.AddSingleton<Net.Mvc.Routing.HomeRouteENTransformer>();
            services.AddSingleton<Net.Mvc.Routing.HomeRouteTransformer>();



            IMvcBuilder mvcBuilder = services.AddControllersWithViews(option =>
            {
                // option.ModelBinderProviders.Insert(0, new WidgetTypeModelBinderProvider());
                // option.ModelMetadataDetailsProviders.Add(new DataAnnotationsMetadataProvider());
            })
            .AddRazorOptions(opt =>
            {
                opt.ViewLocationExpanders.Clear();
            })
            .AddControllersAsServices()
            .AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            }).SetCompatibilityVersion(CompatibilityVersion.Latest);
            services.AddRazorPages()
                .AddRazorPagesOptions(options =>
            {
                options.Conventions.Add(new CustomPageRouteModelConvention());
            });
            services.AddHealthChecks();
            services.AddRouting(option => option.LowercaseUrls = true);


            services.TryAddScoped<IApplicationContext, CMSApplicationContext>();
            services.TryAddSingleton<IRouteProvider, RouteProvider>();
            services.AddTransient<IRouteDataProvider, PaginationRouteDataProvider>();
            services.AddTransient<IRouteDataProvider, PostIdRouteDataProvider>();
            services.AddTransient<IRouteDataProvider, CategoryRouteDataProvider>();
            services.AddTransient<IRouteDataProvider, HtmlRouteDataProvider>();
            services.TryAddSingleton<IAdminMenuProvider, AdminMenuProvider>();
      

            #region Application类缓存
            //缓存相关注入
            services.ConfigureCache<ConcurrentDictionary<string, object>>();
            #endregion

            #region 站点缓存相关注入
            services.ConfigureCache<ConcurrentDictionary<string, site_node>>();
            services.ConfigureCache<ConcurrentDictionary<string, site_content>>();
            services.ConfigureCache<ConcurrentDictionary<string, site_doc_type>>();
            services.ConfigureCache<ConcurrentDictionary<string, site_doc>>();
            services.ConfigureCache<ConcurrentDictionary<string, site_other>>();
            services.ConfigureCache<ConcurrentDictionary<string, sys_strategy>>();
            //在线用户
            services.ConfigureCache<ConcurrentDictionary<string, sys_account>>();
            //系统菜单资源
            services.ConfigureCache<ConcurrentDictionary<string, sys_resource_entity>>();


            services.ConfigureCache<string>();
            //用户角色关系缓存
            services.ConfigureCache<ConcurrentDictionary<string, List<sys_user_role_relation_entity>>>();
            //角色操作关系缓存
            services.ConfigureCache<ConcurrentDictionary<string, List<sys_role_operate_relation_entity>>>();



            services.TryAddScoped<IOnlineUserCache, OnlineUserCache>();

            #endregion

            #region Application Provider
            services.ConfigureStateProvider<CurrentCustomerStateProvider>();
            services.ConfigureStateProvider<CurrentUserStateProvider>();
            services.ConfigureStateProvider<MemberCustomerStateProvider>();


            services.ConfigureStateProvider<StateProvider.OuterChainPictureStateProvider>();
            services.ConfigureStateProvider<StateProvider.ShowDataLangStateProvider>();
            services.ConfigureStateProvider<StateProvider.SiteTplNameStateProvider>();

            #endregion






            services.UseEasyFrameWork(configuration);


            foreach (IPluginStartup item in services.LoadAvailablePlugins())
            {
                item.Setup(new object[] { services, mvcBuilder });
            }
            services.AddAuthentication(DefaultAuthorizeAttribute.DefaultAuthenticationScheme)
            .AddCookie(DefaultAuthorizeAttribute.DefaultAuthenticationScheme, o =>
              {
                  if (CookieSecure)
                  {
                      o.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                  }
                  else
                  {
                      o.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                  }
                  o.LoginPath = new PathString("/admin/login");
                  o.AccessDeniedPath = new PathString("/error/forbidden");
                  o.Events = new Microsoft.AspNetCore.Authentication.Cookies.CookieAuthenticationEvents()
                  {
                      OnRedirectToAccessDenied = context =>
                      {
                          context.Response.Redirect("/error/forbidden");
                          return Task.CompletedTask;
                      }
                  };

              })
            .AddCookie(CustomerAuthorizeAttribute.CustomerAuthenticationScheme, option =>
            {
                option.Cookie.HttpOnly = true;
                if (CookieSecure)
                {
                    option.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                }
                else
                {
                    option.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                }
                option.LoginPath = new PathString("/admin/login");
                option.AccessDeniedPath = new PathString("/error/forbidden");
            });
       

            #region 后台服务
            //注册后台服务类
            services.AddMediatR(typeof(JobAddContext));
            services.AddSingleton<IBJobStore, BJobStore>();
            services.AddSingleton<IJobQueue, JobQueue>();
            services.AddHostedService<MyBackgroundWorker>();



            services.AddSingleton<IJobFactory,SingletonJobFactory>();
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddJob();
            services.AddHostedService<QuartzHostedService>();

            #endregion
        }
        public static void UseJF(this IApplicationBuilder applicationBuilder, IWebHostEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            if (hostingEnvironment.IsDevelopment())
            {
                applicationBuilder.UsePluginStaticFile();
            }

            applicationBuilder.UseStaticFiles();
            ServiceLocator.Setup(httpContextAccessor);
            applicationBuilder.ConfigureResource();
            applicationBuilder.ConfigurePlugin(hostingEnvironment);

            applicationBuilder.UseRouting();
            applicationBuilder.UseEndpoints(endpoints =>
            {
                applicationBuilder.ApplicationServices.GetService<IRouteProvider>().GetRoutes()
                .OrderByDescending(route => route.Priority)
                .Each(route =>
                {
                    endpoints.MapControllerRoute(
                        name: route.RouteName,
                        pattern: route.Template,
                        defaults: route.Defaults,
                        constraints: route.Constraints,
                        dataTokens: route.DataTokens);
                });
                endpoints.MapRazorPages();
            });
            System.IO.Directory.SetCurrentDirectory(hostingEnvironment.ContentRootPath);
            
            Console.WriteLine("Welcome to use CMS");
        }

        public static void UseDataBase(this IServiceCollection services, Utils.Options.DatabaseOption databaseOption)
        {
            if (databaseOption.DbType == "MsSql")
            {
                services.AddDbContextPool<NopObjectContext>(options => options
                   .UseLazyLoadingProxies()
                   .UseSqlServer(connectionString: databaseOption.ConnectionString, builders => {
                       builders.AddSqlServerStringCompareSupport();
                   }));

            }
            else if (databaseOption.DbType == "MySql")
            {
                //services.AddEntityFrameworkMySql();
                //Pomelo.EntityFrameworkCore.MySql--AddDbContext
                services.AddDbContextPool<NopObjectContext>(options =>
                    options.UseLazyLoadingProxies()
                    .UseMySql(connectionString: databaseOption.ConnectionString, serverVersion: Microsoft.EntityFrameworkCore.ServerVersion.FromString(databaseOption.DBVersion),mySqlOptionsAction: (MySqlDbContextOptionsBuilder builder) => {
                        builder.AddMySqlStringCompareSupport();
                    })
                     .UseLoggerFactory(LoggerFactory.Create(b => b
                    .AddConsole()
                    .AddFilter(level => level >= LogLevel.Information)))
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors()
                
                );

            }
            else if (databaseOption.DbType == "NpgSql")
            {
                services.AddDbContext<NopObjectContext>(options => options
                  .UseLazyLoadingProxies()
                  .UseNpgsql(connectionString: databaseOption.ConnectionString));
            }
            else if (databaseOption.DbType == "Oracle")
            {
                //“11”代表11g，传入“12”代表12c。
                services.AddDbContext<NopObjectContext>(options => options
            .UseLazyLoadingProxies()
            .UseOracle(connectionString: databaseOption.ConnectionString, b => b.UseOracleSQLCompatibility(databaseOption.DBVersion)));
            }
            else
            {
                throw new Exception("DB Config Error");
            }
            services.AddEntityFrameworkProxies();
        }
    }
}
