﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using HtmlAgilityPack;
using System.Linq;
using HtmlAgilityPack.CssSelectors.NetCore;
using YS.Utils.Extend;

namespace YS.Net
{
    /// <summary>
    /// 字符串操作扩展类
    /// </summary>
    public static class StringExtensions
    {
        #region Excel列名转换
        /// <summary>
        /// 将指定的自然数转换为26进制表示。映射关系：[1-26] ->[A-Z]。
        /// </summary>
        /// <param name="n">自然数（如果无效，则返回空字符串）。</param>
        /// <returns>26进制表示。</returns>
        public static string ToNumberSystem26(this int n)
        {
            string s = string.Empty;
            while (n > 0)
            {
                int m = n % 26;
                if (m == 0) m = 26;
                s = (char)(m + 64) + s;
                n = (n - m) / 26;
            }
            return s;
        }


        /// <summary>
        /// 将指定的26进制表示转换为自然数。映射关系：[A-Z] ->[1-26]。
        /// </summary>
        /// <param name="s">26进制表示（如果无效，则返回0）。</param>
        /// <returns>自然数。</returns>
        public static int FromNumberSystem26(this string s)
        {
            if (string.IsNullOrEmpty(s)) return 0;
            int n = 0;
            for (int i = s.Length - 1, j = 1; i >= 0; i--, j *= 26)
            {
                char c = Char.ToUpper(s[i]);
                if (c < 'A' || c > 'Z') return 0;
                n += ((int)c - 64) * j;
            }
            return n;
        }
        #endregion

        #region 全角转换半角以及半角转换为全角

        /// <summary>
        ///转全角的函数(SBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSBC(this string input)
        {
            // 半角转全角：
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 32)
                {
                    array[i] = (char)12288;
                    continue;
                }
                if (array[i] < 127)
                {
                    array[i] = (char)(array[i] + 65248);
                }
            }
            return new string(array);
        }

        /// <summary>
        ///转半角的函数(DBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToDBC(this string input)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 12288)
                {
                    array[i] = (char)32;
                    continue;
                }
                if (array[i] > 65280 && array[i] < 65375)
                {
                    array[i] = (char)(array[i] - 65248);
                }
            }
            return new string(array);
        }
        #endregion

        /// <summary>
        /// String2Base64
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToBase64(this string str)
        {
            byte[] byteArray = str.ToByte();

            string strbaser64 = Convert.ToBase64String(byteArray);
            return strbaser64;
        }

        /// <summary>
        /// Base642String
        /// </summary>
        /// <param name="base64_str"></param>
        /// <returns></returns>
        public static string Base64ToString(this string base64_str)
        {
            byte[] c = Convert.FromBase64String(base64_str);
            return System.Text.Encoding.UTF8.GetString(c);
        }
        /// <summary>
        /// 判断字符串是否为null或""
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }
        /// <summary>
        /// 判断字符串是否为null或""或" "(包含空字符的字符串)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }
        /// <summary>
        /// 切割字符串移除空字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <returns></returns>
        public static string[] MySplit(this string str, params char[] separator)
        { 
            return str.MyToString().Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        public static string[] MySplit(this string str, params string[] separator)
        {
            return str.MyToString().Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }
        /// <summary>
        /// 比较字符串区分大小写
        /// </summary>
        /// <param name="str1">字符串1</param>
        /// <param name="str2">字符串2</param>
        /// <returns></returns>
        public static bool EqualsIgnoreCase(this string str1, string str2)
        {
            return null == str1 ? null == str2 : str1.Equals(str2, StringComparison.CurrentCultureIgnoreCase);
        }
        /// <summary>
        /// 判断字符串1是否包含字符串2（不区分大不写）
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static bool ContainsIgnoreCase(this string str1, string str2)
        {
            return null == str1 || null == str2 ? false : str1.IndexOf(str2, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }
        /// <summary>
        /// 获取当前时间的时间戳
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public static long GetTimestamp(this DateTime now)
        {
            DateTime startTime = TimeZoneInfo.ConvertTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0),TimeZoneInfo.Local);  // 当地时区
            long timeStamp = (long)((now - startTime).TotalMilliseconds); // 相差毫秒数
            return timeStamp;
        }
        /// <summary>
        /// 去除空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string Trim1(this string str)
        {
            return str.IsNullOrEmpty() ? "" : str.Trim();
        }
        /// <summary>
        /// 将字符串转换为GUID
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Guid ToGuid(this string str)
        {
            return Guid.TryParse(str, out Guid guid) ? guid : Guid.Empty;
        }
        /// <summary>
        /// 判断一个字符串是否是GUID
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsGuid(this string str)
        {
            return Guid.TryParse(str, out Guid guid);
        }
        /// <summary>
        /// 判断一个字符串是否是GUID
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsGuid(this string str, out Guid guid)
        {
            return Guid.TryParse(str, out guid);
        }
        /// <summary>
        /// 判断一个字符串是否是字体图标(以fa开头)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsFontIco(this string str)
        {
            return str.Trim1().StartsWith("fa");
        }
        /// <summary>
        /// 判断字符串是否为整数
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsInt(this string str)
        {
            return int.TryParse(str, out int i);
        }
        /// <summary>
        /// 判断字符串是否为整数
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsInt(this string str, out int i)
        {
            return int.TryParse(str, out i);
        }
        /// <summary>
        /// 将字符串转换为整数
        /// </summary>
        /// <param name="str"></param>
        /// <param name="defaultValue">转换失败时的默认值</param>
        /// <returns></returns>
        public static int ToInt(this string str, int defaultValue = int.MinValue)
        {
            return int.TryParse(str, out int i) ? i : defaultValue;
        }
        /// <summary>
        /// 判断字符串是否为数字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDecimal(this string str)
        {
            return decimal.TryParse(str, out decimal d);
        }
        /// <summary>
        /// 判断字符串是否为数字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDecimal(this string str, out decimal d)
        {
            return decimal.TryParse(str, out d);
        }
        /// <summary>
        /// 将字符串转换为数字
        /// </summary>
        /// <param name="str"></param>
        /// <param name="defaultValue">转换失败时的默认值</param>
        /// <returns></returns>
        public static decimal ToDecimal(this string str, decimal defaultValue = decimal.MinValue)
        {
            return decimal.TryParse(str, out decimal d) ? d : defaultValue;
        }
       
        /// <summary>
        /// 判断字符串是否为日期时间
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDateTime(this string str)
        {
            return DateTime.TryParse(str, out DateTime dt);
        }
        /// <summary>
        /// 判断字符串是否为日期时间
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDateTime(this string str, out DateTime dt)
        {
            return DateTime.TryParse(str, out dt);
        }
        /// <summary>
        /// 将字符串转换为日期时间
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string str)
        {
            return DateTime.TryParse(str, out DateTime dt) ? dt : DateTime.MinValue;
        }
        /// <summary>
        /// 验证字符串是否为数字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsDigital(this string str)
        {
            foreach (char c in str.ToCharArray())
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 验证是否为固话号码
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsTelNumber(this string str)
        {
            //去掉-线后全为数字
            return !str.StartsWith("-") && str.Replace("-", "").IsDigital();
        }

        
        /// <summary>
        /// 移出所有空格
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string TrimAll(this string str)
        {
            return Regex.Replace(str, @"\s", "");
        }

        #region 得到汉字拼音
        /// <summary>
        /// 得到汉字拼音(全拼)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToPinYing(this string str)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return "";
            }
            var format = new Utils.Pinyin.format.PinyinOutputFormat(Utils.Pinyin.format.ToneFormat.WITHOUT_TONE,
                Utils.Pinyin.format.CaseFormat.LOWERCASE, Utils.Pinyin.format.VCharFormat.WITH_U_AND_COLON);
            return Utils.Pinyin.Pinyin4Net.GetPinyin(str, format).TrimAll();
        }

        public static string ToPinYingEWebeditor(this string str)
        {
            string new_name = FilterChar(str);
            if (string.IsNullOrWhiteSpace(str))
            {
                return "";
            }
            var format = new Utils.Pinyin.format.PinyinOutputFormat(Utils.Pinyin.format.ToneFormat.WITHOUT_TONE,
                Utils.Pinyin.format.CaseFormat.LOWERCASE, Utils.Pinyin.format.VCharFormat.WITH_U_AND_COLON);
            string result = Utils.Pinyin.Pinyin4Net.GetPinyin(new_name, format).TrimAll();
            if (result.IsNullOrEmpty())
            {
                result = "";
            }
            return result;
        }
        public static string FilterChar(string inputValue)
        {
            // return Regex.Replace(inputValue, "[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;|{}【】；‘’，。/*-+]+", "", RegexOptions.IgnoreCase);
            //if (Regex.IsMatch(inputValue, "[A-Za-z0-9\u4e00-\u9fa5-]+"))
            //{
            //    return Regex.Match(inputValue, "[A-Za-z0-9\u4e00-\u9fa5-]+").Value;
            //}
            //return "";


            inputValue = inputValue.Replace(@"\", "");
            inputValue = Regex.Replace(inputValue, "[ \\[ \\] \\^ \\*×(^)$%~!/@#$…&%￥+=<>《》（）|!！??？:：•`·、。，；,.;\"‘’“”]", "");
            //inputValue = Regex.Replace(inputValue, "[ \\[ \\] \\^ \\-_*×――(^)$%~!/@#$…&%￥—+=<>《》|!！??？:：•`·、。，；,.;\"‘’“”-]", "").ToUpper();
            return inputValue;
            
        }
        /// <summary>
        /// 文件名
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToPinYingGuid(this string str)
        {
            string new_name = FilterChar(str);
            if (string.IsNullOrWhiteSpace(str))
            {
                return Guid.NewGuid().ToString("N");
            }
            var format = new Utils.Pinyin.format.PinyinOutputFormat(Utils.Pinyin.format.ToneFormat.WITHOUT_TONE,
                Utils.Pinyin.format.CaseFormat.LOWERCASE, Utils.Pinyin.format.VCharFormat.WITH_U_AND_COLON);
            string result = Utils.Pinyin.Pinyin4Net.GetPinyin(new_name, format).TrimAll();
            if (result.IsNullOrEmpty())
            {
                result = Guid.NewGuid().ToString("N");
            }
            return result;
        }
        #endregion

        /// <summary>
        /// URL编码
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string MyUrlEncode(this string url)
        {
            return WebUtility.UrlEncode(url);
        }
        /// <summary>
        /// URL解码
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string MyUrlDecode(this string url)
        {
            return WebUtility.UrlDecode(url);
        }
        /// <summary>
        /// HTML编码
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string HtmlEncode(this string str)
        {
            return WebUtility.HtmlEncode(str);
        }
        /// <summary>
        /// HTML解码
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string HtmlDecode(this string str)
        {
            return WebUtility.HtmlDecode(str);
        }
        /// <summary>
        /// 将List拼接为字符串
        /// </summary>
        /// <param name="ts"></param>
        /// <param name="split">分隔符</param>
        /// <param name="prefix">前缀</param>
        /// <param name="suffix">后缀</param>
        /// <returns></returns>
        public static string JoinList<T>(this List<T> ts, string split = ",", string prefix = "", string suffix = "")
        {
            if (null == ts || ts.Count == 0)
            {
                return "";
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var t in ts)
            {
                stringBuilder.Append(prefix);
                stringBuilder.Append(t);
                stringBuilder.Append(suffix);
                stringBuilder.Append(split);
            }
            return stringBuilder.ToString().TrimEnd(split.ToCharArray());
        }
        /// <summary>
        /// 将List转换为SQL in语句
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ts"></param>
        /// <param name="single">是包含单引号</param>
        /// <returns></returns>
        public static string JoinSqlIn<T>(this List<T> ts, bool single = true)
        {
            return ts.JoinList(",", single ? "'" : "", single ? "'" : "");
        }
        /// <summary>
        /// 得到实符串实际长度
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int Size(this string str)
        {
            byte[] strArray =Encoding.Default.GetBytes(str);
            return strArray.Length;
        }
        ///   <summary>   
        ///   去除HTML标记   
        ///   </summary>   
        ///   <param   name="NoHTML">包括HTML的源码   </param>   
        ///   <returns>已经去除后的文字</returns>   
        public static string RemoveHTML(this string Htmlstring)
        {
            //删除脚本   
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML   
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", "   ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);

            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            
            return Htmlstring;
        }
        /// <summary>
        /// 过滤js脚本
        /// </summary>
        /// <param name="strFromText"></param>
        /// <returns></returns>
        public static string RemoveScript(this string html)
        {
            if (html.IsNullOrEmpty()) return string.Empty;
            Regex regex1 = new Regex(@"<script[\s\S]+</script *>", RegexOptions.IgnoreCase);
            Regex regex2 = new Regex(@" href *= *[\s\S]*script *:", RegexOptions.IgnoreCase);
            Regex regex3 = new Regex(@" on[\s\S]*=", RegexOptions.IgnoreCase);
            Regex regex4 = new Regex(@"<iframe[\s\S]+</iframe *>", RegexOptions.IgnoreCase);
            Regex regex5 = new Regex(@"<frameset[\s\S]+</frameset *>", RegexOptions.IgnoreCase);
            html = regex1.Replace(html, ""); //过滤<script></script>标记
            html = regex2.Replace(html, ""); //过滤href=javascript: (<A>) 属性
            html = regex3.Replace(html, " _disibledevent="); //过滤其它控件的on...事件
            html = regex4.Replace(html, ""); //过滤iframe
            html = regex5.Replace(html, ""); //过滤frameset
            return html;
        }
        /// <summary>
        /// 替换页面标签
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string RemovePageTag(this string html)
        {
            if (html.IsNullOrEmpty()) return string.Empty;
            Regex regex0 = new Regex(@"<!DOCTYPE[^>]*>", RegexOptions.IgnoreCase);
            Regex regex1 = new Regex(@"<html\s*", RegexOptions.IgnoreCase);
            Regex regex2 = new Regex(@"<head[\s\S]+</head\s*>", RegexOptions.IgnoreCase);
            Regex regex3 = new Regex(@"<body\s*", RegexOptions.IgnoreCase);
            Regex regex4 = new Regex(@"<form\s*", RegexOptions.IgnoreCase);
            Regex regex5 = new Regex(@"</(form|body|head|html)>", RegexOptions.IgnoreCase);
            html = regex0.Replace(html, ""); //过滤<html>标记
            html = regex1.Replace(html, "<html\u3000 "); //过滤<html>标记
            html = regex2.Replace(html, ""); //过滤<head>属性
            html = regex3.Replace(html, "<body\u3000 "); //过滤<body>属性
            html = regex4.Replace(html, "<form\u3000 "); //过滤<form>属性
            html = regex5.Replace(html, "</$1\u3000>"); //过滤</html></body></head></form>属性
            return html;
        }

        /// <summary>
        /// 取得html中的图片
        /// </summary>
        /// <param name="HTMLStr"></param>
        /// <returns></returns>
        public static string GetImg(this string text)
        {
            string str = string.Empty;
            Regex r = new Regex(@"<img\s+[^>]*\s*src\s*=\s*([']?)(?<url>\S+)'?[^>]*>", //注意这里的(?<url>\S+)是按正则表达式中的组来处理的，下面的代码中用使用到，也可以更改成其它的HTML标签，以同样的方法获得内容！
            RegexOptions.Compiled);
            Match m = r.Match(text.ToLower());
            if (m.Success)
                str = m.Result("${url}").Replace("\"", "").Replace("'", "");
            return str;
        }
        /// <summary>
        /// 取得html中的所有图片
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string[] GetImgs(this string text)
        {
            var doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml("<body>" + text + "</body>");
            var img_nodes = doc.QuerySelectorAll("img");

            Dictionary<string, int> dic = new Dictionary<string, int>();
            foreach (var item in img_nodes)
            {
                HtmlAttribute src = item.Attributes["src"];
                if (src != null)
                {
                    string src_str = src.Value.MyToString();
                    if (src_str != "")
                    {
                        dic.TryAdd(src_str.HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                    }
                }

            }
            var list = doc.DocumentNode.ChildNodes;

            GetCssImgs(list, dic);

            List<string> imgs = new List<string>();
            Regex r = new Regex(@"<img\s+[^>]*\s*src\s*=\s*([']?)(?<url>\S+)'?[^>]*>", RegexOptions.IgnoreCase);
            Match m = r.Match(text);
            while (m.Success)
            {
                dic.TryAdd(m.Result("${url}").HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                m = m.NextMatch();
            }
            Regex r1 = new Regex(@"(?<=\b(?:src\s*=\s*['""]?|background:\s*.*?\burl\b\()(?!http:))/?([^'""\s\)]+)", RegexOptions.IgnoreCase);
            Match m1 = r1.Match(text);
            if (m1.Success)
            {
                foreach (Capture a in m1.Captures)
                {
                    dic.TryAdd(a.Value.HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                }


            }

            Regex r2 = new Regex(@"(?<=\b(?:src\s*=\s*['""]?|background-image:\s*.*?\burl\b\()(?!http:))/?([^'""\s\)]+)", RegexOptions.IgnoreCase);
            Match m2 = r2.Match(text);
            if (m2.Success)
            {
                foreach (Capture a in m2.Captures)
                {
                    dic.TryAdd(a.Value.HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                }
            }

            var reg = new Regex(@"url\s*\((.*)\)", RegexOptions.IgnoreCase);
            Match m3 = reg.Match(text);
            if (m3.Success && m3.Groups.Count > 1)
            {
                dic.TryAdd(m3.Groups[1].Value.HtmlDecode().Replace("\"", "").Replace("'", ""), 1);

            }
            return dic.Keys.ToArray();
        }



        public static void GetCssImgs(HtmlAgilityPack.HtmlNodeCollection nodes, Dictionary<string, int> dic)
        {

            Regex re = new Regex(@"background-image:\s*url\s*\((?<image>.+)\s*\)", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
            Regex re1 = new Regex(@"background:\s*url\s*\((?<image>.+)\s*\)", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
            Regex re2 = new Regex(@":\s*url\s*\((?<image>.+)\s*\)", RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace);
            foreach (var item in nodes)
            {
                if (item.NodeType == HtmlAgilityPack.HtmlNodeType.Element && item.Name.ToLower() != "img")
                {
                    var style = item.Attributes["style"];
                    if (style != null)
                    {
                        string style_str = style.Value.MyToString();
                        if (style_str != "")
                        {
                            Match m3 = re.Match(style_str);
                            if (m3.Success)
                            {
                                dic.TryAdd(m3.Result("${image}").HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                            }
                            Match m1 = re2.Match(style_str);
                            if (m1.Success)
                            {
                                dic.TryAdd(m1.Result("${image}").HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                            }
                            Match m = re1.Match(style_str);
                            if (m.Success)
                            {
                                dic.TryAdd(m.Result("${image}").HtmlDecode().Replace("\"", "").Replace("'", ""), 1);
                            }
                        }

                    }
                }
                var childnodes = item.ChildNodes;
                GetCssImgs(childnodes, dic);
            }
        }
        /// <summary>
        /// 过滤字符串不区分大小写
        /// </summary>
        /// <param name="str"></param>
        /// <param name="oldStr"></param>
        /// <param name="newStr"></param>
        /// <returns></returns>
        public static string ReplaceIgnoreCase(this string str, string oldStr, string newStr)
        {
            return str.IsNullOrWhiteSpace() ? "" : Regex.Replace(str,
                oldStr.IsNullOrEmpty() ? "" : oldStr, newStr.IsNullOrEmpty() ? "" : newStr, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 判断子字符串位置，不区分大小写
        /// </summary>
        /// <param name="str"></param>
        /// <param name="subString"></param>
        /// <returns></returns>
        public static int IndexOfIgnoreCase(this string str, string subString)
        {
            return str.IndexOf(subString, StringComparison.CurrentCultureIgnoreCase);
        }

        /// <summary>
        /// 过滤查询SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static string FilterSelectSql(this string sql)
        {
           
            return sql.IsNullOrWhiteSpace() ? "" 
                : sql.ReplaceIgnoreCase("DELETE ", "")
                .ReplaceIgnoreCase("UPDATE ", "")
                .ReplaceIgnoreCase("INSERT ", "")
                .ReplaceIgnoreCase("TRUNCATE TABLE", "")
                .ReplaceIgnoreCase("DROP", "");
        }
         
    
      
      

        /// <summary>
        /// 从字符串中提取数字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetNumber(this string str)
        {
            if (str.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }
            StringBuilder stringBuilder = new StringBuilder();
            foreach (char c in str.ToCharArray())
            {
                if (int.TryParse(c.ToString(), out int a))
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString().TrimStart('0');
        }

        /// <summary>
        /// 截取字符串，汉字两个字节，字母一个字节
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="len">截取长度</param>
        /// <param name="show">截取后加上符号</param>
        /// <returns></returns>
        public static string CutOut(this string str, int len, string show = "…")
        {
            ASCIIEncoding ascii = new ASCIIEncoding();
            int tempLen = 0;
            string tempString = "";
            byte[] s = ascii.GetBytes(str);
            int oldLen = s.Length;
            for (int i = 0; i < s.Length; i++)
            {
                if ((int)s[i] == 63)
                { tempLen += 2; }
                else
                { tempLen += 1; }
                try
                { tempString += str.Substring(i, 1); }
                catch
                { break; }
                if (tempLen > len) break;
            }
            //如果截过则加上半个省略号 
            if (oldLen > len)
                tempString += show;
            tempString = tempString.Replace("&nbsp;", " ");
            tempString = tempString.Replace("&lt;", "<");
            tempString = tempString.Replace("&gt;", ">");
            tempString = tempString.Replace('\n'.ToString(), "<br>");
            return tempString;
        }

        /// <summary>
        /// 截取副标题
        /// </summary>
        /// <param name="contents"></param>
        /// <param name="len"></param>
        /// <param name="show"></param>
        /// <returns></returns>
        public static string CutSubTitle(this string contents, int len, string show = "…")
        {
            return contents.RemoveHTML().CutOut(len, show);
        }

        /// <summary>
        /// 字符串用逗号分开加上双引号(主要用于oracle查询)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string AddDoubleQuotes(this string str)
        {
            if (str.IsNullOrWhiteSpace())
            {
                return string.Empty;
            }
            string[] strings = str.Split(',');
            StringBuilder stringBuilder = new StringBuilder();
            foreach (string s in strings)
            {
                string oreder = s.ContainsIgnoreCase("asc") ? "ASC" : s.ContainsIgnoreCase("desc") ? "DESC" : "ASC";
                stringBuilder.Append("\"" + s.ReplaceIgnoreCase("asc", "").ReplaceIgnoreCase("desc", "").Trim1() + "\" " + oreder + ",");
            }
            return stringBuilder.ToString().TrimEnd(',');
        }

        /// <summary>
        /// 判断一个字符串LIST中是否包含某个字符串，不区分大小写
        /// </summary>
        /// <param name="list"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool ContainsIgnoreCase(this List<string> list, string str)
        {
            foreach (string s in list)
            {
                if (s.EqualsIgnoreCase(str))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 得到Request.Query字符串
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Querys(this HttpRequest request, string key)
        {
            string str = request.Query[key];
            return str ?? string.Empty;
        }
        /// <summary>
        /// 得到Request.Form字符串
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string Forms(this HttpRequest request, string key)
        {
            string str = request.Form[key];
            return str ?? string.Empty;
        }
        /// <summary>
        /// 得到Request.QueryString
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string UrlQuery(this HttpRequest request)
        {
            QueryString queryString = request.QueryString;
            return queryString.HasValue ? queryString.Value : string.Empty;
        }
        /// <summary>
        /// 得到URL
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static string Url(this HttpRequest request)
        {
            return (request.Path.HasValue ? request.Path.Value : "") + request.UrlQuery();
        }
    }
}
