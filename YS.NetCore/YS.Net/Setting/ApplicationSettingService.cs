using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq.Expressions;
using YS.Utils;
using YS.Utils.Cache;
using YS.Utils.RepositoryPattern;
using Microsoft.Extensions.Logging;
using YS.Utils.DI;
using YS.Utils.Repositorys;

namespace YS.Net.Setting
{
    [ScopedDI]
    public class ApplicationSettingService : ServiceBase<ApplicationSetting, Microsoft.Extensions.Logging.ILogger<ApplicationSetting>>, IApplicationSettingService
    {

        private readonly ConcurrentDictionary<string, object> _settingCache;
        private const string ApplicationSetting = "ApplicationSetting";
        public ApplicationSettingService(IRepository<ApplicationSetting> _useService,
               ISkyCacheManager<ConcurrentDictionary<string, object>> cacheManager,
            Microsoft.Extensions.Logging.ILogger<ApplicationSetting> _log)
            : base(_useService, _log)
        {
            _settingCache = cacheManager.GetOrAdd(ApplicationSetting, new ConcurrentDictionary<string, object>());
        }

        


        public int Modify(string SettingKey, string value)
        {

            
            int flag = base.Modify(a => a.SettingKey.ToLower() == SettingKey.ToLower(), a => new Setting.ApplicationSetting()
            {
                Value = value
            }); ;
            if (flag > 0)
                _settingCache.TryUpdate(SettingKey, (object value11) => { return value; });

            return flag;
        }
        /// <summary>
        /// 系统配置添加
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ApplicationSetting Add(ApplicationSetting item)
        {
            var result  = base.Insert(item);
            if (result.SettingKey == null)
            {
                _settingCache.TryAdd(item.SettingKey, item.Value);

            }
            return result;
        }
        /// <summary>
        /// 根据Key获取系统配置
        /// </summary>
        /// <param name="settingKey"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public string Get(string settingKey, string defaultValue)
        {
            string result = "";
            object get_result = null;
            _settingCache.TryGetValue(settingKey, out get_result);
            if (get_result != null)
            {
                result = get_result.MyToString();
            }
            if (result != "")
            {
                return result;
            }


            var setting = base.GetByOne(a => a.SettingKey.ToLower() == settingKey.ToLower());
            if (setting == null || setting.Value == null)
            {
                setting = Add(new Setting.ApplicationSetting() { SettingKey = settingKey, Value = defaultValue });
                result = defaultValue;
            }
            else
            {
                result = setting.Value.MyToString();
            }
            _settingCache.TryAdd(settingKey, result);
            return result;

        }


        //public override List<ApplicationSetting> GetList(string keySelector, List<Expression<Func<ApplicationSetting, bool>>> predicate)
        //{
        //    return base.GetList(keySelector, predicate);
        //}


    }
}