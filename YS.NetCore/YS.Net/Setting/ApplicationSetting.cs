



using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace YS.Net.Setting
{
    [Table("site_application_setting")]
    public class ApplicationSetting 
    {
        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SettingKey { get; set; }
        public string Value { get; set; }
    }

}