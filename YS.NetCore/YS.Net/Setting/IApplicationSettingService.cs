
using YS.Utils.RepositoryPattern;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;


namespace YS.Net.Setting
{
    public interface IApplicationSettingService : IService<ApplicationSetting>
    {
        string Get(string settingKey, string defaultValue);

        int Modify(string SettingKey, string value);
        ApplicationSetting Add(ApplicationSetting item);




    }
}