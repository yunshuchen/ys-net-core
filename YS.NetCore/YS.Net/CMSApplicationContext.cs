
using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using YS.Net.Filter;
using System.Collections.Generic;
using Microsoft.AspNetCore.Html;
using YS.Utils.Mvc.StateProviders;
using YS.Utils.Mvc;
using YS.Utils.Mvc.Extend;
using Microsoft.Extensions.Options;
using YS.Utils.Options;

namespace YS.Net
{
    public class CMSApplicationContext : ApplicationContext
    {

        private Uri _requestUrl;

        private readonly IOptions<SiteSetting> site_config;
        public CMSApplicationContext(IHttpContextAccessor httpContextAccessor, IOptions<SiteSetting> _site_config) :
            base(httpContextAccessor)
        {
            site_config = _site_config;
            HeaderPart = new List<IHtmlContent>();
            FooterPart = new List<IHtmlContent>();
            Styles = new List<string>();
            Scripts = new List<string>();
        }

        public Uri RequestUrl
        {
            get
            {
                if (_requestUrl == null)
                {
                    if (HttpContextAccessor.HttpContext != null)
                    {
                        _requestUrl = new Uri(HttpContextAccessor.HttpContext.Request.Path);
                    }
                }
                return _requestUrl;
            }
            set { _requestUrl = new Uri(value.AbsoluteUri); }
        }
        public string MapPath(string path)
        {
            if (HttpContextAccessor.HttpContext != null)
            {
                return HttpContextAccessor.HttpContext.Request.MapPath(path);
            }
            return path;
        }
        public PageViewMode PageMode { get; set; }
        public bool IsDesignMode { get { return PageMode == PageViewMode.Design; } }
        /// <summary>
        /// Append to html/head
        /// </summary>
        public List<IHtmlContent> HeaderPart { get; set; }
        /// <summary>
        /// Append the content before body close
        /// </summary>
        public List<IHtmlContent> FooterPart { get; set; }
        /// <summary>
        /// Append system styles to page footer
        /// </summary>
        public List<string> Styles { get; set; }
        /// <summary>
        /// Append system script to page footer
        /// </summary>
        public List<string> Scripts { get; set; }
        public bool OuterChainPicture { get { return Get<bool>(nameof(OuterChainPicture)); } }

        public string SiteTplName { get { return Get<string>(nameof(SiteTplName)); } }
        public string Show_Data_Lang { get { return Get<string>(nameof(Show_Data_Lang)); } }

        public string AppVersion { get { return "v4.1.0"; } }

        /// <summary>
        /// 是否使用多语言
        /// true 使用多语言
        /// false 不使用多语言
        /// </summary>
        public bool MultiLanguage { get { return site_config.Value.MultiLanguage; } }

        /// <summary>
        /// 手机版是否使用不同模板
        /// </summary>
        public bool MobileDiffTpl { get { return site_config.Value.MobileDiffTpl; } }
    }
}
