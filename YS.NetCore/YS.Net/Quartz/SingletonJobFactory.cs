﻿using Autofac;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Quartz
{
    public class SingletonJobFactory : IJobFactory
    {
         private readonly IServiceProvider _serviceProvider;
       //* private readonly IContainer AutofacContainer;
        public SingletonJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {

            //从容器中获取IJob实例
            //在Sartup.cs中Autofac 通过Attribute自动注册Singleton
            return _serviceProvider.GetRequiredService(bundle.JobDetail.JobType) as IJob;
           
        }

        public void ReturnJob(IJob job) { }
    }

}
