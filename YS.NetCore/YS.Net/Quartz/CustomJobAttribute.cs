﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Quartz
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CustomJobAttribute : Attribute
    {
        /// <summary>
        /// 代号名称
        /// </summary>
        public string CronExpression { get; private set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Description { get; private set; }
        /// <summary>
        /// 结算标记特性
        /// 提供给远程端来调用
        /// </summary> 
        public CustomJobAttribute()
        {
        }
        /// <summary>
        /// Commamd标记特性
        /// 提供给远程端来调用
        /// </summary>    
        /// <param name="code">代号</param>  
        public CustomJobAttribute(string CronExpression)
        {
            this.CronExpression = CronExpression;
        }

        /// <summary>
        /// Commamd标记特性
        /// 提供给远程端来调用
        /// </summary>    
        /// <param name="code">代号</param>  
        /// <param name="remark">说明</param> 
        public CustomJobAttribute(string CronExpression, string Description)
        {
            this.CronExpression = CronExpression;
            this.Description = Description;
        }
    }
}
