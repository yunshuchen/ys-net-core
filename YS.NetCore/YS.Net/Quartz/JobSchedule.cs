﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Quartz
{
    public class JobSchedule
    {
        public JobSchedule(Type jobType, string cronExpression,string description)
        {
            JobType = jobType;
            CronExpression = cronExpression;
            Description = description;
        }

        public Type JobType { get; }
        public string CronExpression { get; }
        public string Description { get; }
    }
}
