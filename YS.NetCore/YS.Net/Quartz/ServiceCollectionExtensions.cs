﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace YS.Net.Quartz
{
    public static class ServiceCollectionExtensions
    {

        //Startup.cs 添加注入  builder.RegisterType(typeof(Net.Quartz.Jobs.CheckSurveyJob)).SingleInstance();
        //或者通过Attribute 动态注入Single
        //动态添加Job
        //[HttpGet("DynamicAddJob")]
        //[AllowAnonymous]
        //public async Task<RestfulData> DynamicAddJobAsync([FromServices] Quartz.ISchedulerFactory _schedulerFactory)
        //{
        //    Type jobType = Type.GetType("YS.Net.Quartz.Jobs.CheckSurveyJob,YS.Net");

        //    var jobBuilder = Quartz.JobBuilder
        //        .Create(jobType)
        //        .WithIdentity(jobType.FullName)
        //        .WithDescription(jobType.Name)
        //        .Build();

        //    var trigger = Quartz.TriggerBuilder
        //        .Create()
        //        .WithIdentity($"{jobType.FullName}.trigger")
        //        .WithCronSchedule("*/5 * * * * ?")
        //        .WithDescription(jobType.FullName)
        //        .Build();
        //    var Scheduler = await _schedulerFactory.GetScheduler();
        //    await Scheduler.ScheduleJob(jobBuilder, trigger);
        //    return new RestfulData(0, "ok");
        //}
        public static void AddJob(this IServiceCollection services)
        {

            var _assably = AppDomain.CurrentDomain.GetAssemblies();
            var _needImpl = _assably.Where(c => c.FullName.StartsWith("YS.Net") || c.FullName.StartsWith("YS.Utils") || c.FullName.StartsWith("YS.Plugin")).ToArray();
            var list = _needImpl.Select(a=>a.GetTypes().Where(item => Attribute.IsDefined(item, typeof(CustomJobAttribute)))).ToList();

            foreach (var item in list)
            {
                foreach (var job in item)
                {
                    CustomJobAttribute attribute = (CustomJobAttribute)job.GetCustomAttributes(typeof(CustomJobAttribute), false).FirstOrDefault();
                    services.AddSingleton(new JobSchedule(
                       jobType: job,
                       cronExpression: attribute.CronExpression,description: attribute.Description)); 
                }
          
            }
        }
    }
}
