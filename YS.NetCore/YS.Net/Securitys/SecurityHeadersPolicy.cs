﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Securitys
{
    public class SecurityHeadersPolicy
    {
        public IDictionary<string, string> SetHeaders { get; }
         = new Dictionary<string, string>();

        public ISet<string> RemoveHeaders { get; }
        = new HashSet<string>();
    }
}
