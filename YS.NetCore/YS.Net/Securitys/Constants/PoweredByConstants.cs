﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net.Securitys.Constants
{
    /// <summary>
    /// Server headery-related constants.
    /// </summary>
    public static class ServerConstants
    {
        /// <summary>
        /// The header value for X-Powered-By
        /// </summary>
        public static readonly string Header = "Server";
    }
}
