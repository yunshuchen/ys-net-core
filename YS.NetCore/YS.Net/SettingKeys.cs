﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YS.Net
{
    /// <summary>
    /// 网站设置的相关Key
    /// </summary>
    public class SettingKeys
    {
        /// <summary>
        /// 
        /// </summary>
        public const string Favicon = "Favicon";
        public const string ExpandAllPage = "ExpandAllPage";
        public const string ExecuteScriptWhenChangeTheme = "ExecuteScriptWhenChangeTheme";
    }
}
