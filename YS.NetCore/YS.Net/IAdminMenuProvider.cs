
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YS.Net
{
    public interface IAdminMenuProvider
    {
        IEnumerable<AdminMenu> GetAdminMenus();
    }
}
