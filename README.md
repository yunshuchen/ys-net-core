# YS-NetCore

#### 介绍
特点：
- 1.基于这里是列表文本微软Net 5开发，可部署于Windows、linux等主流操作系统，并能部署在Docker中运行。
- 2.数据库访问采用Entity Framework (EF) Core，可同时支持MS SQL Server、MySQL、Postgresql、Oracle等主流关系统数据库系统。
- 3.内置分布式缓冲机制，目前支持Redis。计划支持memcache.
- 4.内置gRPC支持，可通过gRPC对接第三方系统，并能用多种开发语言来扩展应用。
- 5.内置任务支持，通过多种调度策略调度任务。
- 6.内置对全文检索引擎ElasticSearch的支持。
- 7.通过插件接口，可快速扩展应用。
- 8.内置对Swagger支持，所有业务均提供了api接口，能过bear token方式鉴权。并能通过swagger-ui实现api文档及web方式测试。
- 9.内置RBAC权限模型，可控制对功能、操作、数据的权限控制。
- 10.内置对微信SDK,公众号,小程序,企业微信开放平台的集成，开发人员可不用关心微信或企微相关接口，只关心业务逻辑限可。[https://gitee.com/Senparc](https://gitee.com/Senparc)
- 11. 后台BackgroudService,HostService
- 12. Razor 已支持JsonLocalizer 本地化多语言,另外加入动态路由
- 12. Blazor WebAssembly 正在踩坑,目前处理国际/本地化 :sob: 